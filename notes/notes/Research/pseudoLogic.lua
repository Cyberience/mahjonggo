Sample hand = "b1b1b2b3b4c7c7c7WsWsWsWnWnWn"
	-- or	= "b1b1b2b3b4c7c7c7:WsWsWs:WnWnWn"
	-- or	= "b1b1b2b3b4:c7c7c7:WsWsWs:WnWnWn"
	-- or 	= "b1b1c7c7c7WsWsWsWnWnWn:b2b3b4"
	-- or	= "c1c2c3b4b5b6m7m8m9DrDrDrDbDbDb"
	-- notes: 	c = circles, b = bamboo, m = millions
	--			W = winds, s = south, n = north, e = east, w = west
	--			D = dragons b = Blue dragon, r = Red Dragon, g = Green dragon 
--[[note: 
	get clean pattern, like this->  111223334455
	do a sequence run first. if failed,
	remove 1rst 4 set, do sequence
	remove 2nd 4set do a sequence
	remove 3rd set do a sequence
	remove 4th set 
	repeat with 3 sets.
	repeat with alternat 3-4 sets.
	]]	

		
function checkhand(hand)
	masterhand = migrate(hand)
	if validhand(masterhand['b']) then
	elseif validhand(masterhand['c']) then
	elseif validhand(masterhand['m']) then
	elseif validwinds(masterhand['W']) then
	elseif validwinds(masterhand['D']) then
		win=true
	else
		win = false
	end --if
	return win
end --function
	
function validhand(handarray)
	retval = true  --Any hand left over will return fail, so nothing left will indicate success, so win by default.
	counts = {["streets"]=0,["pungs"]=0,["pongs"]=0, ["kongs"]=0 }
	workarray = handarray.new()
	for interation = 1, 2 do  --how many iterations do we need to try. start with 2 for now
		for i = 1 , #workarray do --go through the workarray
			if workarray[i] = 1 and i < 8 then  --7,8,9 is a street, 8,9 can  not be a 3 run straight
				if workarray[i] == workarray[i+1] and workarray[i] == workarray[i+2] then --we have a street
					counts["streets"] = counts["streets"] + 1
					workarray[i],workarray[i+1],workarray[i+2] = workarray[i]-1,workarray[i+1]-1,workarray[i+2]-1
				else
					retval = false -- failed  -- if its a single, and there is not 3 number run, then its a failed hand
				end
			elseif array[i] = 2 and i < 9 then -- could be a street or pair
			elseif array[i] = 3 -- could be a street or triple or pair
			elseif array[i] = 4 -- could be a quad. street or triple or pair
			end
		end --for arrays
	end -- for interations
	return retval,counts
end --function

function validwinds(handarray)
--winds and dragons can only be 1 pair or sets of 3 or 4,
retval = true

return retval
end


function migrate(hand)
--this function will take the hand, and place the peices into the known working structure, and count up the values
local masterhand = {	['b']={['1']=0,['2']=0,['3']=0,['4']=0,['5']=0,['6']=0,['7']=0,['8']=0,['9']=0},
						['c']={['1']=0,['2']=0,['3']=0,['4']=0,['5']=0,['6']=0,['7']=0,['8']=0,['9']=0},
						['m']={['1']=0,['2']=0,['3']=0,['4']=0,['5']=0,['6']=0,['7']=0,['8']=0,['9']=0},
						['W']={['e']=0,['s']=0,['w']=0,['n']=0},
						['D']={['b']=0,['g']=0,['r']=0},
						['scores']={["streets"]=0,["pungs"]=0,["pongs"]=0, ["kongs"]=0 }
					}
for loop = 1 , string:find(hand,":") , 2 do  --step every 2 through the pairs in 2 upto the declared had set.
	if string.match("bcmWD",hand:sub(loop ,loop)) then --if the char is b c m W od D, then increment the value by one at the position indicated by the following position.
		--					array ID			sub array ID				same here, is a +=1  was available I would use it here.	
		masterhand[hand:sub(loop ,loop)][hand:sub(loop+1 ,loop+1)] = masterhand[hand:sub(loop ,loop)][hand:sub(loop+1 ,loop+1)] + 1
	end --if, should be no failures here.
return masterhand
end