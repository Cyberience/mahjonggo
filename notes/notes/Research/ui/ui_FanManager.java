import java.util.Vector;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class FanManager {
    //一.[88番以上牌型]
    static final short SFDA = 11;//{四方大发} 168番 东东东西西西南南南北北北发发，实际上就是大四喜和青发对的组合

    static final short TH = 12;//{天和} 168番 庄家起牌即和，任何胡牌型均可。

    static final short DH = 13;//{地和} 158番 庄家出第一张后，下家自摸即是地和，牌型不限。

    static final short RH = 14;//{人和} 108番 第一圈内胡牌，牌型不限。

    //二.[88番牌型]

//    static final short SSY = 21; //{十三幺}(又称国士无双) 88番，19w19t19b东南西北中发白，全部13种幺九牌各一张再加上其中任何一种的一张成和

//    static final short DSX = 22; //{大四喜}(又称四喜临门) 88番，东东东西西西南南南北北北33w，东南西北四种风牌各一刻或杠，加上别的牌任何一对

//    static final short DSY = 23; //{大三元}(又称三元及第) 88番，中中中发发发白白白123w88t，中发白三种箭牌的刻或杠，加上任何一顺或刻、杠以及任何一对成和

//    static final short JLBD = 24; //{九连宝灯} 88番，11123456789999w，例中前十三张即为九连宝灯的基本听牌型(九门听)1112345678999加上任何 该种数牌的第十四张即成和，例中是万的九连宝灯胡牌型，索或筒均可，要门清

//    static final short LQD = 25; //{连七对}(又称车轮滚滚) 88番，11223344556677t，同种数牌的相连七对，图例中是筒，万或索均可。胡牌型亦可是2至8或3至9的连对，要门清

//    static final short LYS = 26; //{绿一色}(又称满园春色) 88番 234234666888t发发 23468索和青发六种牌的任何胡牌组合均可

    static final short SGZ = 27; //{四杠子}(又称十八学士) 88番 任何四副杠子(明暗不限)加上任何一对成和

    static final short HG = 28; //{混杠} 88番 四张混开杠

//三.[64番牌型]

//    static final short XSX = 31; //{小四喜} 64番 东东东西西西南南南北北123w 东南西北四种风牌中三种的刻或杠加另一种的一对配上任何其他牌的一顺、刻或杠成和

    static final short XSY = 32; //{小三元} 64番 中中中发发发白白234b99t 中发白三种箭牌中两种的刻或杠，加上另一种的一对，配上任何其他牌的两副顺、刻或杠成和

    static final short QYJ = 33; //{清幺九} 64番 111999w111999t99b 19万、19索、19筒六种幺九牌的胡牌型，其难度远远大于88番中的大三元和绿一色

    static final short YSSLH = 34; //{一色双龙会} 64番 12312355789789b 同种数牌的两副123、两副789顺，配以5一对作将

    static final short ZYS = 35; //{字一色} 64番 东东东南南南北北北发发发白白 东南西北中发白七种字牌的胡牌型，因为有七种牌，难度较清幺九低很多，允许七对胡牌

//    static final short SAK = 36; //{四暗刻} 64番 111w333777t东东东发发(要不吃不碰，即门清) 由任意种类牌的刻或杠组成的门前清胡牌型

//四.[48番牌型]

    static final short YSSJG = 41; //{一色四节高} 48番 222333444555t22b(可碰) 同种数牌的相连四刻或杠，配以任意一对成和

    static final short YSSTS = 42; //{一色四同顺} 48番 123123123123w77t 同种数牌的相同四顺，配以任意一对成和

//五.[32番牌型]

    static final short YSSBG = 51; //{一色四步高} 32番 123345567789w88t 例为同种数牌的123、345、567、789四顺，配以任意一对成和。同类牌型还有123、234、345、456等

    static final short HYJ = 52; //{混幺九} 32番 111999w999b东东东白白(可碰) 由幺九牌组成的胡牌型，可以是七对

    static final short SGZ1 = 53; //{三杠子} 32番 由任意种类牌的三副杠子及任意另外一顺或刻配以任意一对成和

    static final short TT = 54; //{天听} 32番 起牌即上听

//六.[24番牌型]

//    static final short QD = 61; //{七对} 24番 1122w4477t88b东东发发 基本胡牌型的一种，以七种对子成和

    static final short QXBK = 62; //{七星不靠} 24番 14w25t39b东南西北中发白 十三不靠和牌型中，七种字牌齐全即为七星不靠

    static final short QSK = 63; //{全双刻} 24番 222444w666888b22t(可碰) 全部双数牌组成的刻或杠，配以双数牌的对子成和

    static final short QD1 = 64; //{全大} 24番 778899w789t789b88t 全部由789的数牌组成的胡牌型，允许是七对

    static final short QZ = 65; //{全中} 24番 全部由456的数牌组成的胡牌型，允许是七对(见全大)

    static final short QX = 66; //{全小} 24番 全部由123的数牌组成的胡牌型，允许是七对(见全大)

    static final short YSSJG1 = 67; //{一色三节高} 24番 111222333w567t88b(可碰) 同种数牌的相连三刻或杠，配任意种类牌的一顺、刻、杠及任意一对成和

    static final short YSSTS1 = 68; //{一色三同顺} 24番 123123123w789t55b 同种数牌的同样三副顺子，配任意种类牌的一顺、刻、杠及任意一对成和

//    static final short QYS = 69; //{清一色} 24番 12323456767899w 同种数牌组成的胡牌型，允许七对

//七.[16番牌型]

    static final short QL = 71; //{青龙} 16番 123456789w345t88b 同类数牌中123、456、789三顺即为青龙

    static final short YSSBG1 = 72; //{一色三步高} 16番 123345567w567t99b 同种数牌中相同间隔的三顺组成三步，如图例中所示的123、345、567，同理的还有234、456、678或是345、567、789，这种间隔两张牌的共有3种；也可以是123、234、345，同理的有234、345、456或345、456、567或456、567、678以及567、678、789，这种间隔1张牌的一共有5种类型。

    static final short QDW = 73; //{全带五} 16番 345345w567t567b55b 胡牌型中所有的顺、刻、杠、对均带有5万、5索或5筒

    static final short SSSLH = 74; //{三色双龙会} 16番 123789w55t123789b 同两种数牌的123、789两顺配以另一种数牌的一对5组成的胡牌型

//    static final short SAK1 = 75; //{三暗刻} 16番 111333w777t12388b (其中刻子要摸，不能碰) 胡牌型中含有任意三副手中的刻子(暗杠也可)，所谓暗刻即指在手中的，不能是碰别家牌成刻

    static final short STK = 76; //{三同刻} 16番 222w222t222b45688t(可碰) 三种数牌中同数字牌的三副刻子或杠子

//八.[12番牌型]

    static final short SSBK = 81; //{十三不靠} 12番 147w25t36b东南西北中发 基本和牌型之一，由三种数牌中的147258369九种牌和七种字牌中任意十四张牌成和(数牌中并不要求某一门一定是147或258，只要求三门数牌不一样即可，比方万是147，筒是258，则索一定要是369)

    static final short ZHL = 82; //{组合龙} 12番 147w258t369b中南西北中 十三不靠胡牌型中三种数牌的147258369组合配齐，即成为组合龙

    static final short DYW = 83; //{大于五} 12番 667788w789t67899b 所有牌均为6789的数牌，故称大于五，允许七对胡牌型，若不含6即为全大

    static final short XYW = 84; //{小于五} 12番 (同大于五) 所有牌均为1234的数牌，故称小于五，允许七对胡牌型，若不含4即为全小

    static final short SFK = 85; //{三风刻} 12番 东东东南南南北北北123w88b 胡牌型中含有任意三副风牌刻或杠

//九.[8番牌型]

    static final short HL = 91; //{花龙} 8番 123567w456t78955b 由三种数牌的123、456、789各一顺组成

    static final short TBD = 92; //{推不倒} 8番 112233b222456t白白 所谓推不倒指的是把牌倒过来放牌的样子是一样的，推不倒的牌总共有245689索、1234589筒和白板十四种，任意胡牌型均可成和，允许七对胡牌

    static final short SSSTS = 93; //{三色三同顺} 8番 123789w789t55789b 胡牌型中含有三种数牌的相同数码顺子各一副

    static final short SSSJG = 94; //{三色三节高} 8番 123555w666t77788b 三种数牌各差一级大小的刻子或杠子各一副

    static final short WFH = 95; //{无番和} 8番 123w44678t789t东东 胡下来的牌连一番也数不出，不能是门清，自摸类麻将中没有

//    static final short HDL = 96; //{海底捞月} 8番 最后一张牌时捉放铳，自摸类麻将中没有

    static final short MSHC = 97; //{妙手回春} 8番 自摸最后一张牌

    static final short GSKH = 98; //{杠上开花} 8番 开杠时杠得的牌正好成和

    static final short QGH = 99; //{抢杠和} 8番 明杠时开杠的牌正好被别人成和

//十;//{6番牌型}

//    static final short HYS = 101; //{混一色} 6番 123345789东东东发发 由一种数牌和字牌组成的胡牌型，允许七对和牌

//    static final short PPH = 102; //{碰碰和} 6番 111333w444t东东东发发 由四副刻子或杠子配一对作麻将头成和，混幺九、清幺九、四暗刻、四杠子、大四喜、四方大发等均属此，但不作重复计算

    static final short SSSBG = 103; //{三色三步高} 6番 123w234t345b东东东发发 三种数牌的各一副顺子由小至大排列且间隔相同，比如123万、234索、345筒或234筒、456筒、678万等

    static final short WMQ = 104; //{五门齐} 6番 123w345t789b东东东发发 指胡牌型中四组顺或刻以及一对麻将头分别由万、索、筒三种数牌及风、箭两种字牌各一组成

    static final short QQR = 105; //{全求人} 6番 所有牌全部吃碰或明杠露白，且最后单吊捉人放铳和，自摸类麻将中没有

    static final short SAG = 106; //{双暗杠} 6番 胡牌型中含有两副暗杠

//十一;//{4番牌型}

    static final short QDY = 111; //{全带幺} 4番 123w789t111b东东东发发(注意字也算么九牌) 胡牌型中所有的顺、刻、杠和对中至少带有一张幺九牌

//    static final short BQR = 112; //{不求人} 4番 门清且自摸

    static final short HJZ = 113; //{和决张} 4番 河里已打出或已被吃碰露白的同种牌已有3张，和第4张即称和决张

    static final short SMG = 114; //{双明杠} 4番 胡牌型中含有两副明杠

//十二.[2番牌型]

//    static final short PH = 121; //{平和} 2番 由4副顺子及一对数牌作将组成的和牌，边、坎、钓不影响平和

//    static final short DYJ = 122; //{断幺九} 2番 234345w456t55678b(也要无字) 和牌型中没有一、九及字牌

    static final short SGY = 123; //{四归一} 2番111123w345t67899b 和牌型中包含4张相同的牌，但不能是杠子

    static final short LTK = 124; //{两同刻} 2番 2副相同序数大小的数牌刻子或杠子，图例请参见三同刻

    static final short SAK2 = 125; //{双暗刻} 2番 和牌型中包含两副暗刻，图例可参见三暗刻

//    static final short MQQ = 126; //{门前清} 2番 没有吃、碰、明杠，最后捉放铳和。如果自摸和，即是不求人

    static final short MFK = 127; //{门风刻} 2番 与本门风相同的风刻(也就是你所坐的方位相同的风刻)

    static final short QFK = 128; //{圈风刻} 2番 与圈风相同的风刻(那是算圈的)

    static final short JK = 129; //{箭刻} 2番 中、发、白的刻子或杠子

//    static final short AG = 1210; //{暗杠} 2番 自抓4张相同的牌开杠

//十三.[1番牌型]

//    static final short YBG = 131; //{一般高} 1番 112233w456t789b东东 一种数牌的相同两顺

    static final short XXF = 132; //{喜相逢} 1番 123w123t456678b东东 两种数牌序数相同的两顺

    static final short LL = 133; //{连六} 1番 123456w456t678b东东，一种数牌序数相连的六张组成的两副顺子

    static final short LSP = 134; //{老少配} 1番 123789w567t345b发发，一种数牌的123、789两顺

    static final short QYM = 135; //{缺一门} 1番 123345w56767899b，胡牌型中缺少一种花色的数牌

    static final short YJQ = 136; //{幺九刻} 1番 一、九的数牌或字牌的刻子或杠子

//    static final short WZ = 137; //{无字} 1番 胡牌型中没有字牌

//    static final short MG = 138; //{明杠} 1番 手上的刻子用别人打出的牌开杠或已碰出的刻子用第四张开杠

    static final short BZ = 139; //{边张} 1番 12胡3或89胡7

    static final short QZ1 = 1310; //{嵌张} 1番 胡两张牌嵌住的中间那张，如46胡5，79胡8等

//    static final short DD = 1311; //{单钓} 1番 钓单张牌作将成和

//    static final short ZM = 1312; //{自摸} 1番 自摸成和


    public FanManager() {
    }

    public static Vector getFanResult(Pai[] pai,boolean iszimo){
        if(pai == null)return null;

        int[] paivalue = new int[18];

        boolean isopen = false;
        for (int i = 0; i < pai.length; i++) {
            if (pai[i].majong != 0) {
                paivalue[i] = ((pai[i].majong - 1) / 4) + 1;
//                System.out.println(paivalue[i]);
                if (pai[i].isOpened) {
                    isopen = true;
    //                    break;
                }
            }
        }

        byte pengtimes = 0;
        for(int i=0;i < paivalue.length;i++){
            if(i+2 > paivalue.length - 1)break;
            if (paivalue[i] != 0) {
                if (paivalue[i] == paivalue[i+1] && paivalue[i+1] == paivalue[i+2]){
                    pengtimes++;
                    i += 2;
                }
            }
        }

        if (pengtimes > 4)pengtimes = 4;

        Vector vfan = new Vector();
        int typeNum = 0;
        boolean hasBing = false;
        boolean hasTiao = false;
        boolean hasWan = false;
        boolean hasFeng = false;
        for(int i=0;i < paivalue.length;i++){
            if(paivalue[i] != 0){
                if (paivalue[i] >= 1 && paivalue[i] <= 9 ) { //饼
                    hasBing = true;
                } else if (paivalue[i] >= 10 && paivalue[i] <= 18 ) { //条
                    hasTiao = true;
                } else if (paivalue[i] >= 19 && paivalue[i] <= 27) { //万
                    hasWan = true;
                } else if (paivalue[i] >= 28 && paivalue[i] <= 34) { //风
                    hasFeng =true;
                }

            }
        }

        if(hasBing)typeNum ++;
        if(hasTiao)typeNum ++;
        if(hasWan)typeNum ++;
        if(hasFeng)typeNum ++;

//        System.out.println("hasBing "+hasBing);
//        System.out.println("hasTiao "+hasTiao);
//        System.out.println("hasWan "+hasWan);
//        System.out.println("hasFeng "+hasFeng);

        if (iszimo) {
            if (!isopen)
                vfan.addElement(new Fan(4, "不求人"));
            else
                vfan.addElement(new Fan(1, "自摸"));
        } else {
            if (!isopen)
                vfan.addElement(new Fan(2, "门前清"));
        }


        if (is13YaoHu(paivalue)) {
            vfan.addElement(new Fan(88, "十三幺"));
            return vfan;
        }

        if(isDaSiXiHu(paivalue,pengtimes)) {
            vfan.addElement(new Fan(88, "大四喜"));
            return vfan;
        }

        if (isDaSanYuanHu(paivalue,pengtimes)) {
            vfan.addElement(new Fan(88, "大三元"));
            return vfan;
        }

        if (is7LianDuiHu(paivalue)) {
            vfan.addElement(new Fan(88, "七连对"));
            return vfan;
        }

        if (isLvYiSeHu(paivalue)) {
            vfan.addElement(new Fan(88, "绿一色"));
            return vfan;
        }

        if((hasBing && !hasTiao && !hasWan)||
           (!hasBing && hasTiao && !hasWan)||
           (!hasBing && !hasTiao && hasWan)){
            if (is9BaoLianDengHu(paivalue)) {
                vfan.addElement(new Fan(88, "九连宝灯"));
                return vfan;
            }
        }

        if (isXiaoSiXiHu(paivalue,pengtimes)) {
            vfan.addElement(new Fan(64,"小四喜"));
        }
        
        if (pengtimes == 4 && !isopen) {
            vfan.addElement(new Fan(64,"四暗刻"));
        }

        if(is7DuiHu(paivalue))
            vfan.addElement(new Fan(24,"七对"));

        if(typeNum == 1)
            vfan.addElement(new Fan(24,"清一色"));
        else if(typeNum == 2 && hasFeng)
            vfan.addElement(new Fan(6,"混一色"));

        if (pengtimes == 3 && !isopen) {
            vfan.addElement(new Fan(16,"三暗刻"));
        }

        if (pengtimes == 4 && isopen) {
            vfan.addElement(new Fan(6,"碰碰胡"));
        }

//        for(int i=0;i < 14;i++){
//            if (paivalue[13-i] != 0) {
//                if(paivalue[13-i] < 28){
//                    if(paivalue[13] == paivalue[13-i]){
//                        vfan.addElement(new Fan(1,"单钓"));
//                        break;
//                    }
//                }
//            }
//        }

        byte minggang = 0;
        byte angang = 0;
        for(int i=0;i < 4;i++){
            if(pai[14+i].majong != 0){
                if(pai[14+i].action != 3){
                    minggang++;
                }else{
                    angang++;
                }
            }
        }
        if(minggang == 1)vfan.addElement(new Fan(1,"明杠"));
        else if(minggang == 2)vfan.addElement(new Fan(4,"双明杠"));

        if(angang == 1)vfan.addElement(new Fan(2,"暗杠"));
        else if(angang == 2)vfan.addElement(new Fan(6,"双暗杠"));

        if(angang + minggang == 3)vfan.addElement(new Fan(32,"三杠子"));

        boolean hasYao = false;
        for(int i=0;i < 14;i++){
            if(isYao(paivalue[i])){
                hasYao = true;
                break;
            }
        }
        if(!hasYao)
            vfan.addElement(new Fan(2, "断幺九"));
        
        boolean hasZi = false;
        for(int i=0;i < 14;i++){
            if(isZi(paivalue[i])){
            	hasZi = true;
                break;
            }
        }
        if(!hasZi)
            vfan.addElement(new Fan(1, "无字"));
        
        for(int i=0;i < 9;i++){
            if(paivalue[i] == paivalue[i+1] && paivalue[i+2] == paivalue[i+3] && paivalue[i+4] == paivalue[i+5]){
            	for(int m = 0;m < 3;m ++){
	            	if(paivalue[i] > m*9 && paivalue[i] < 8+m*9){
	            		if(paivalue[i+5] > 2+m*9 && paivalue[i+5] < 10+m*9){
	            			if(paivalue[i]+1 == paivalue[i+2] && paivalue[i]+2 == paivalue[i+4]){
	            				vfan.addElement(new Fan(1, "一般高"));
	            				i=9;
	            				break;
	            			}
	            		}
	            	}
            	}
            }
        }
        
        for(int i=0;i < 4;i++){
        	if(paivalue[i]+1 == paivalue[i+1] && paivalue[i]+2 == paivalue[i+2] && paivalue[i]+3 == paivalue[i+3] && paivalue[i]+4 == paivalue[i+4]&&paivalue[i]+5 == paivalue[i+5]){
	        	for(int m = 0;m < 3;m ++){
	        		if(paivalue[i] > m*9 && paivalue[i] < 5+m*9){
	            		if(paivalue[i+5] > 5+m*9 && paivalue[i+5] < 10+m*9){
            				vfan.addElement(new Fan(1, "连六"));
            				i=9;
            				break;
	            		}
	            	}
	        	}
        	}
        }

        return vfan;
    }

    public static boolean isLv(int paivalue){
        if (paivalue == 11 ||
            paivalue == 12 ||
            paivalue == 13 ||
            paivalue == 15 ||
            paivalue == 17 ||
            paivalue == 33 ){
            return true;
        }
        return false;
    }

    public static boolean isYao(int paivalue){
        if(paivalue == 1 ||
           paivalue == 9 ||
           paivalue == 10 ||
           paivalue == 18 ||
           paivalue == 19 ||
           paivalue == 27 ||
           paivalue == 28 ||//东
           paivalue == 29 ||//南
           paivalue == 30 ||//西
           paivalue == 31 ||//北
           paivalue == 32 ||//中
           paivalue == 33 ||//发
           paivalue == 34) {//白
            return true;
        }
        return false;
    }
    
    public static boolean isZi(int paivalue){
        if(paivalue == 28 ||//东
           paivalue == 29 ||//南
           paivalue == 30 ||//西
           paivalue == 31 ||//北
           paivalue == 32 ||//中
           paivalue == 33 ||//发
           paivalue == 34) {//白
            return true;
        }
        return false;
    }

    public static boolean is13YaoHu(int[] paivalue){
        String[] s = {"1","9","10","18","19","27","28","29","30","31","32","33","34"};
        Vector v13Yao = new Vector();
        for(int i=0;i < s.length;i++){
            v13Yao.addElement(s[i]);
        }
        for(int i=0;i < paivalue.length;i++){
            if(paivalue[i] > 0){
                if(!isYao(paivalue[i]))return false;
                for (int j = 0; j < v13Yao.size(); j++) {
                    if (String.valueOf(paivalue[i]).equals(v13Yao.elementAt(j))) {
                        v13Yao.removeElementAt(j);
                    }
                }
            }
        }

        if(v13Yao.size() == 0)
            return true;

        return false;
    }

    public static boolean is9BaoLianDengHu(int[] paivalue) {
        String[] s = {"1", "1", "1", "2", "3", "4", "5", "6", "7", "8",
                     "0", "0", "0"};
        Vector v9BaoLianDeng = new Vector();
        for (int i = 0; i < s.length; i++) {
            v9BaoLianDeng.addElement(s[i]);
        }
        for (int i = 0; i < paivalue.length; i++) {
            if (paivalue[i] > 0) {
                for (int j = 0; j < v9BaoLianDeng.size(); j++) {
                    int tmp = paivalue[i]%9;
                    if (String.valueOf(tmp).equals(v9BaoLianDeng.elementAt(j))) {
                        v9BaoLianDeng.removeElementAt(j);
                    }
                }
            }
        }

        if (v9BaoLianDeng.size() == 0)
            return true;

        return false;
    }

    public static boolean is7LianDuiHu(int[] paivalue){
        if(is7DuiHu(paivalue)){
            if(paivalue[0] % 9 == 1 && paivalue[13] % 9 == 7)return true;
            if(paivalue[0] % 9 == 2 && paivalue[13] % 9 == 8)return true;
            if(paivalue[0] % 9 == 3 && paivalue[13] % 9 == 9)return true;
        }
        return false;
    }

    public static boolean is7DuiHu(int[] paivalue){
    	for (int i = 0; i < 14; i++) {
            int min = paivalue[i];
            for (int j = i+1; j < 14; j++) {
                if(j < 14){
                    if (paivalue[j] < min) {
                        int temp = min;
                        min = paivalue[j];
                        paivalue[j] = temp;
                    }
                }
            }
            paivalue[i] = min;
        }
        if (paivalue[0] == paivalue[1] &&
            paivalue[2] == paivalue[3] &&
            paivalue[4] == paivalue[5] &&
            paivalue[6] == paivalue[7] &&
            paivalue[8] == paivalue[9] &&
            paivalue[10] == paivalue[11] &&
            paivalue[12] == paivalue[13]) {
            return true;
        }
        return false;
    }

    public static boolean isLvYiSeHu(int[] paivalue){
        int lvCounter = 0;
        for(int i=0;i < 14;i++){
            if(isLv(paivalue[i])){
                lvCounter++;
            }
        }
        if (lvCounter == 14) return true;
        return false;
    }

    public static boolean isDaSiXiHu(int[] paivalue, int pengtimes){
        String[] s = {"28", "29", "30", "31","28", "29", "30", "31","28", "29", "30", "31"};
        Vector vFeng = new Vector();
        for (int i = 0; i < s.length; i++) {
            vFeng.addElement(s[i]);
        }
        for (int i = 0; i < paivalue.length; i++) {
            if (paivalue[i] > 0) {
                for (int j = 0; j < vFeng.size(); j++) {
                    if (String.valueOf(paivalue[i]).equals(vFeng.elementAt(j))) {
                        vFeng.removeElementAt(j);
                    }
                }
            }
        }

        if (vFeng.size() == 0 && pengtimes >= 4)
            return true;

        return false;
    }
    
    public static boolean isXiaoSiXiHu(int[] paivalue, int pengtimes){
    	if(pengtimes < 3)return false;
        int[] sixi = {28, 29, 30, 31};
        int[] count = new int[sixi.length];
        for (int i = 0; i < paivalue.length; i++) {
            if (paivalue[i] > 0) {
            	for(int j = 0; j < sixi.length; j++) {
            		if(paivalue[i] == sixi[j]){
            			count[j] ++;
            			break;
            		}
            	}
            }
        }
        
        String[] s = {"3", "3", "3","2"};
        Vector vCount = new Vector();
        for (int i = 0; i < s.length; i++) {
        	vCount.addElement(s[i]);
        }
        for (int i = 0; i < count.length; i++) {
            if (count[i] > 0) {
                for (int j = 0; j < vCount.size(); j++) {
                    if (String.valueOf(count[i]).equals(vCount.elementAt(j))) {
                    	vCount.removeElementAt(j);
                    }
                }
            }else{
            	return false;
            }
        }

        if (vCount.size() == 0)
            return true;

        return false;
    }

    public static boolean isDaSanYuanHu(int[] paivalue, int pengtimes){
        String[] s = {"32", "33", "34","32", "33", "34","32", "33", "34"};
        Vector vSanYuan = new Vector();
        for (int i = 0; i < s.length; i++) {
            vSanYuan.addElement(s[i]);
        }
        for (int i = 0; i < paivalue.length; i++) {
            if (paivalue[i] > 0) {
                for (int j = 0; j < vSanYuan.size(); j++) {
                    if (String.valueOf(paivalue[i]).equals(vSanYuan.elementAt(j))) {
                        vSanYuan.removeElementAt(j);
                    }
                }
            }
        }

        if (vSanYuan.size() == 0 && pengtimes >= 3)
            return true;

        return false;
    }

}
