import javax.microedition.lcdui.*;

public interface Const {
	//106633552
    static final String sms_again_phonenum = "106633552";
    static final String sms_again_message = "T";
    static final String sms_dapai_phonenum = "106633552";
    static final String sms_dapai_message = "T";
    static final String sms_av_phonenum = "106633552";
    static final String sms_av_message = "T";

//    static final int SOFTKEY_LEFT = -21; //left soft key MotoE398
//    static final int SOFTKEY_RIGHT = -22; //right soft key MotoE398

    static final int TOPLEFT = Graphics.TOP | Graphics.LEFT;
    static final int TOPRIGHT = Graphics.TOP | Graphics.RIGHT;
    static final int TOPCENTER = Graphics.TOP | Graphics.HCENTER;
    static final int CENTER = Graphics.VCENTER | Graphics.HCENTER;
    static final int BOTTOMLEFT = Graphics.BOTTOM | Graphics.LEFT;
    static final int BOTTOMRIGHT = Graphics.BOTTOM | Graphics.RIGHT;
    static final int BOTTOMCENTER = Graphics.BOTTOM | Graphics.HCENTER;

    static final int TRANS_NONE = 0;
    static final int TRANS_ROT90 = 5;
    static final int TRANS_ROT180 = 3;
    static final int TRANS_ROT270 = 6;
    static final int TRANS_MIRROR = 2;
    static final int TRANS_MIRROR_ROT90 = 7;
    static final int TRANS_MIRROR_ROT180 = 1;
    static final int TRANS_MIRROR_ROT270 = 4;

    static final int FRAME_LIMIT = 25;
    static final int RES_INDEX_LEN = 4;

    // game state
    static final int GS_LOGO = 0;
    static final int GS_MENU0 = 1;
    static final int GS_ABOUT = 3;
    static final int GS_HELP = 5;
    static final int GS_OPTIONS = 8;
    static final int GS_EXIT = 9;
    static final int GS_LOADING = 10;
    static final int GS_RUN = 11;
    static final int GS_PAUSE = 12;
    static final int GS_GAMEOVER = 13;
    static final int GS_LEVELOVER = 14;
    static final int GS_DEBRIEF = 15;
    static final int GS_DIALOG = 16;
    static final int GS_END = 17;
    static final int GS_RESTART = 18;
    static final int GS_BACK_TO_MAINMENU = 19;
//    static final int GS_DIALOGASK = 20;
    static final int GS_MINIGAME = 21;
    static final int GS_EFFECT = 22;
    static final int GS_TITLE = 23;
    static final int GS_BAIBAO = 24;
    static final int GS_CONTINUE = 25;
    static final int GS_OPSELECT = 26;
    static final int GS_AVSELECT = 27;
    static final int GS_CONTINUEGAME = 29;
    static final int GS_USERPROFILE = 30;
    static final int GS_SHOWFEE1  = 31;
    static final int GS_SHOWFEE2  = 32;
    static final int GS_LOGO2 = 33;
    static final int GS_JM = 34;
    static final int GS_FASONG = 35;

    static final int PLAYERTURN = 0;//玩家思考应该是两个状态13张的时候14张的时候
    static final int PLAYERGET = 1;//玩家抓牌
    static final int COMPUTERGET = 2;//没有用到
    static final int COMPUTERTURN = 3;//没有用到
    static final int COMPUTERTHINK = 4;//电脑思考
    static final int COMWIN = 5;//电脑胜利
    static final int COMGET = 6;//电脑抓牌
    static final int COMTURN = 7;//用到
    static final int PLAYERTHINK = 8;//玩家思考
    static final int PLAYERCHI = 9;//玩家吃牌
    static final int PLAYERWIN = 10;//玩家胜利
    static final int SELECTCHI = 11;//选择吃牌 自己抓的或者电脑出的
    static final int LIUJU = 12;//平局
    static final int SHOWMM = 13;//显示MM
    static final int SHOWTEXIAO = 14;//显示特效
    static final int PAUSE = 15;//暂停
    static final int SELECTZIMOGANGCHUPAI = 16;//选择吃碰杠胡
    static final int PLAYERZIMO = 17;//自摸用
    static final int COMSHOW = 18;
    static final int SHOWBETSTEP0 = 19;
    static final int SHOWBETSTEP1 = 20;
    static final int SHOWBETSTEP2 = 21;
    static final int SHOWBETSTEP3 = 22;


    static final Font FT_SMALL = Font.getFont(Font.FACE_PROPORTIONAL,
                                              Font.STYLE_PLAIN,
                                              Font.SIZE_SMALL);

}
