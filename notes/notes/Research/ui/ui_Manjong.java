import javax.microedition.midlet.MIDlet;
import javax.microedition.lcdui.*;

public class Manjong extends MIDlet {
    ////////////////////////////////////////////////////////
    public static Manjong s_midlet;
    static Game s_game;

    ////////////////////////////////////////////////////////
    public Manjong() {
        s_midlet = this;
        s_game = new Game();
        Display.getDisplay(this).setCurrent(s_game);
        s_game.init();
        s_game.gameState = Const.GS_LOGO;
        s_game.currentGirl = 0;
        new Thread(s_game).start();

    }

    public void startApp() {
        if (s_game == null) {
            s_game = new Game();
        }
        Display.getDisplay(this).setCurrent(s_game);

    }

    public void pauseApp() {
        notifyPaused();
    }

    public void destroyApp(boolean b) {
        s_game = null;
        notifyDestroyed();
//        Display.getDisplay(this).setCurrent(null);
//        s_game = null;
    }
}
