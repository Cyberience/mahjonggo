Sample hand = "b1b1b2b3b4c7c7c7WsWsWsWnWnWn"
	-- or	= "b1b1b2b3b4c7c7c7:WsWsWs:WnWnWn"
	-- or	= "b1b1b2b3b4:c7c7c7:WsWsWs:WnWnWn"
	-- or 	= "b1b1c7c7c7WsWsWsWnWnWn:b2b3b4"
	-- or	= "c1c2c3b4b5b6m7m8m9DrDrDrDbDbDb"
	-- notes: 	c = circles, b = bamboo, m = millions
	--			W = winds, s = south, n = north, e = east, w = west
	--			D = dragons b = Blue dragon, r = Red Dragon, g = Green dragon 
--[[note: 
	get clean pattern, like this->  111223334455
	Start Count, 1 to 154.
	0000
	||||- 123, 234, 345, 456, 567, 678, 789
	|||-- 111. 222, 333, 444, 555, 666, 777, 888, 999
	||--- 1111, 2222, 3333, 4444, 5555, 6666, 7777, 8888, 9999
	|---- 11, 22, 33, 44, 55, 66, 77, 88, 99
	]]


	
function checkhand(hand)
	local masterhand = migrate(hand)
	local test, retvalues = false,{}
	
	test, retvalues = validhand(masterhand['b'])
	if test then 
		masterhand = addvalues(masterhand, retvalues)
		test, retvalues = validhand(masterhand['c']) end
	if test then 
		masterhand = addvalues(masterhand, retvalues)
		test, retvalues = validhand(masterhand['m']) end
	if test then 
		masterhand = addvalues(masterhand, retvalues)
		test, retvalues = validwinds(masterhand['W']) end
	if test then 
		masterhand = addvalues(masterhand, retvalues)
		test, retvalues = validwinds(masterhand['D']) end
	if test then
		masterhand = addvalues(masterhand, retvalues) end

	
	return masterhand.stre + masterhand.trip + masterhand.quad + masterhand.pair
end --function

function addvalues(oval,nval)
		oval.stre = oval.stre + nval.stre
		oval.trip = oval.trip + nval.trip
		oval.quad = oval.quad + nval.quad
		oval.pair = oval.pair + nval.pair
	return oval
end
	
function validhand(suit)
	local found, colv,worklen,retval,base,sets, workhand,chkstr, points = false, 0, 0, true, 3,5, "","",{stre=0, trip=0,quad=0, pair=0}  --Any hand left over will return fail, so nothing left will indicate success, so win by default.

	if suit:len() > 1 then  -- no point doing this if there is no tiles or a stand alone tile.
		local cycles = math.ceil(math.pow(suit:len()/3,base))  --sets how many cycles we need to test every case for the total number of tiles
		for interation = 1, cycles do
			workhand = suit  --reset the hand
			local control = DecToBase(interation,base)  --counts up making is max 2 in each column
			for icol = 1 , sets do --go through the sets, there is 4 sets of 3, plus 1 pair. Always
				colv = tonumber(control:sub(icol,icol))
				worklen = workhand:len()
				found = false
				for icard = 1, worklen do --walk through the string
					if colv == 0 and worklen > 2 and not found then --check for 7,8,9 is a street, can only be 3,
						chkstr = tostring(icard)...tostring(icard+1)...tostring(icard+2)
						workhand, count = string.gsub(workhand,chkstr,"0",1)  --we have a streets replace only 1 occurance
						points.stre = points.stre + count
					elseif colv = 1 and worklen == 3 and not found then -- could be a 3 of a kind
						chkstr = string.rep(icard,3)
						workhand, count = string.gsub(workhand,chkstr,"0",1)  --we have a triple replace all occurances
						points.trip = points.trip + count
					elseif colv = 2 and worklen == 2 and not found then -- check for a pair
						chkstr = string.rep(icard,2)
						tmpstr = workhand
						workhand, count = string.gsub(workhand,chkstr,"0",1)  --we have pairs replace all occurances can only be 1 pair, or all pairs,
						if count == 1 or count == 7 then
							points.pair = points.pair + count
						else workhand = tmpstr end --reset the hand if the pairs are not 1 or 7 sets.
					end
					workhand = string.gsub(workhand,"0","") -- clean up by removing the 0, we could cut them as the string length would throw us out.
					found = (count>0) and true or false --stops the checking of a second hand if one is found on an earlier number.
				end --icard
			end --for icols
			
			if workhand:len() == 0 then -- a perfect hand, might need to continue to see if there is a higher valid hand!! ***
				retval = true
				break
			else --reset working hand for the next combination
				workhand = suit
			end
		end -- for interations
	elseif suit:len() == 1 then  retval = false end --Guaranteed Failure, cos any single card is a totally invalid hand
	else retval = true end  --empty hand is a valid state, must be no tiles in this suit
	end
	
	return retval,points
end --function

function validwinds(handarray)
--winds and dragons can only be 1 pair or sets of 3 or 4,
retval = true

return retval
end


function migrate(hand) --this function will take the hand, and place the peices into the known working structure,
local masterhand = {	['b']='',['c']='',['m']='',['W']='',['D']='',
											['scores']={["streets"]=0,["pungs"]=0,["kongs"]=0}
										}
local loop1,loop2 = 0,0
hand = string.gmatch(hand, "[^|]+")  --get the hand, exlcude declared
--local splithand = ""
-- for splithand in string.gmatch(hand, "[^|]+") do  --get the string and split it at the delimiter
	for loop = 1 , splithand:len() , 2 do  --step every 2 through the pairs in 2 upto the declared had set.
		if string.match("bcmWD",hand:sub(loop ,loop)) then --if the char is b c m W od D, append the value.
			masterhand[hand:sub(loop ,loop)] = masterhand[hand:sub(loop ,loop)]...hand:sub(loop+1 ,loop+1)
		end --if, should be no failures here.
	end -- loop1
--end --for split
--this may be a good place to scor the declared hands.
return masterhand
end