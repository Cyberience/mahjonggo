var canvas = null;
var context = null;
var bufferCanvas = null;
var bufferCanvasCtx = null;
var coinMaster = null;
var loadingMaster =  null;
var coinArray = [];
var coinTimer = null;
var maxCoins = 45;
var loadcount = 0;
var FormMiddle= 0;
var FormCentre= 0;
var ZeroCorner= 0;
var ZeroCenter= 0;
var ScaleWidth= 0;	
var ScaleHeight= 0;	
var debugtxt = '------';
var CaughtCoins = 0;
var clickposX = 0;
var clickposY = 0;
var resetClick	= null;
	
function init() { //The constructor you might say
	//Main Canvas
	canvas = document.getElementById('Canvas');
	context = canvas.getContext("2d");
	context.canvas.style = '-webkit-tap-highlight-color:rgb(0,0,0,0); -webkit-user-select: none; -webkit-touch-callout:none;';
	context.canvas.width = window.innerWidth-5;
	context.canvas.height = window.innerHeight-5;
	document.getElementById('mask').style.width = window.innerWidth-5 + 'px';
	document.getElementById('mask').style.height = window.innerHeight-5 + 'px';
	
	//Buffer for the canvas
	bufferCanvas = document.createElement("canvas");
	bufferCanvasCtx = bufferCanvas.getContext("2d");
	bufferCanvasCtx.canvas.width = context.canvas.width;
	bufferCanvasCtx.canvas.height = context.canvas.height;
	bufferCanvasCtx.canvas.style = '-webkit-tap-highlight-color:rgb(0,0,0,0); -webkit-user-select: none; -webkit-touch-callout:none;';
	
	//login frame
	loginframe = document.getElementById('loginboxwrap');
	loginframe.style.top = ((window.innerHeight/2 - parseInt(loginframe.style.height)/2) -40)+"px";
	regframe = document.getElementById('regboxwrap');
	regframe.style.top = ((window.innerHeight/2 - parseInt(loginframe.style.height)/2) -140)+"px";
	regsuccessframe = document.getElementById('regsuccesswrap');
	regsuccessframe.style.top = ((window.innerHeight/2 - parseInt(regsuccessframe.style.height)/2) -80)+"px";
	
	//Set keyboad handler
	context.canvas.addEventListener("click", mjOnClick, false);
	context.canvas.addEventListener("touchmove", function(e){Ti.App.fireEvent('onTouchMove',{}); return false; }, false);
  
	//Buffer for Coin master Animation (sprite)
	coinMaster = new Image();
	coinMaster.src = './images/coins.png';
		//Buffer for loaing master Animation (sprite)
	loadingMaster = new Image();
	loadingMaster.src = './images/loading.jpg';
		//place the tile center and 2/3 down
	ScaleWidth=window.innerWidth/550;
	ScaleHeight=context.canvas.height/400;
	FormMiddle=context.canvas.width/2;
	FormCentre=context.canvas.height/2;
	animatetimer = setInterval(animate, 150);
	loadtimer = setInterval(loadcounter, 2000);
	// initialize the rects
	coinTimer = setInterval(addCoin, 500);
}
			
function cleanup(){ //Called from resize form to reset the dynamics
	clearInterval(animatetimer);
	clearInterval(loadtimer);
	clearInterval(coinTimer);
	init();
}
			
function stopanimate(){ //Called from resize form to reset the dynamics
	clearInterval(animatetimer);
	clearInterval(loadtimer);
	clearInterval(coinTimer);
}

function Draw(){
	context.save();
	blank();
		//Animation tiles
	ZeroCorner=(FormMiddle-((loadingMaster.width*ScaleWidth)/2));  //tried to put these in init, but seem image not loaded fast enough to see size.
	ZeroCenter=(((context.canvas.height)-FormCentre)/2+FormCentre);
	// Load the count background
	bufferCanvasCtx.drawImage(loadingMaster,0,1,216,42, ZeroCorner, ZeroCenter ,216*ScaleWidth,42*ScaleHeight);
	//now write the count piece, load count is the step.
	bufferCanvasCtx.drawImage(loadingMaster,0+(24 * loadcount) ,42 ,24 ,42 , ZeroCorner+((24*ScaleWidth) * loadcount), ZeroCenter ,24*ScaleWidth,42*ScaleHeight);
	//Coin
	for (var i = 0; i < coinArray.length; i++) {
		bufferCanvasCtx.drawImage(coinMaster,0 + (80 * parseInt(coinArray[i].spin/2)) ,0,80 ,74 ,coinArray[i].x,coinArray[i].y,coinArray[i].width,coinArray[i].height);
		//position of coin and click
			if 	((coinArray[i].x < clickposX &&  coinArray[i].x + coinArray[i].width > clickposX) &&
					(coinArray[i].y < clickposY &&  coinArray[i].y + coinArray[i].height > clickposY)) {
					CaughtCoins = CaughtCoins + parseInt(110/coinArray[i].width);
					document.getElementById('coincount').value = CaughtCoins;
					// then its inside the coin square
					//coinArray[i].y = 0;   //simple approach, reset to the top.
					coinArray.splice(i,1);   //simple approach, delete from array
					ClickLead();							
			} 
	}
	bufferCanvasCtx.textAlign = "right";
	bufferCanvasCtx.textBaseline = "bottom";
	bufferCanvasCtx.font = "20pt Calibri";
	bufferCanvasCtx.fillStyle = (CaughtCoins < 0) ? 'red' : 'yellow' ; //"#ffff00";
	bufferCanvasCtx.fillText("Coins="+CaughtCoins, bufferCanvas.width, bufferCanvas.height);
	bufferCanvasCtx.textAlign = "center";
	bufferCanvasCtx.textBaseline = "bottom";
	bufferCanvasCtx.fillStyle = "blue";
	bufferCanvasCtx.fillText(debugtxt, bufferCanvas.width/2, bufferCanvas.height);
	
	// copy the entire rendered image from the buffer canvas to the visible one
	context.drawImage(bufferCanvas, 0,0,bufferCanvas.width, bufferCanvas.height);
	context.restore();
}
	
function mjOnClick(pos){
	if (pos.pageX != undefined && pos.pageY != undefined) {
		clickposX=pos.pageX;
		clickposY=pos.pageY;
		debugtxt="Db1=X:"+clickposX+"*Y:"+clickposY;
	} else {
		clickposX=pos.clientX;
		clickposY=pos.clientY;
		debugtxt="Db2=X:"+clickposX+"*Y:"+clickposY;
	}
	resetClick = setInterval(ClickLead, 100); //resets the click location value
	CaughtCoins--;
	document.getElementById('coincount').value = CaughtCoins;
}
			
function ClickLead() { //resets the click value after a short time.
	clickposX = 0;
	clickposY = 0;
	clearInterval(resetClick);
}

function Coin() { //creates the properties of a falling coin
    this.x = Math.round(Math.random() * context.canvas.width);
    this.y = -10;
    this.drift = Math.random();
    this.speed = Math.round(Math.random() * 3) + 1;
    this.width = (Math.random() * 40) + 15; //Bigger Coins
    this.height = this.width *1.1;
	this.spin = parseInt((Math.random() * 16) + 0);
}

function loadcounter() {
	loadcount++;
	if (loadcount > 8){
		loadcount = 0;
	}
}
			
function addCoin() {
	if ((Math.random() * 3)  > 2) { //creates a delay by not creating one every cycle.
		if (coinArray.length < maxCoins)  //if playing the game and delete coins we wont to add some back.
			coinArray[coinArray.length] = new Coin();
	}
}

function blank() {
	bufferCanvasCtx.drawImage(document.getElementById("logo"), 0, 0, context.canvas.width, context.canvas.height);
}

function animate() {
	UpdateCoins();
	Draw();
}

function UpdateCoins() {
    for (var i = 0; i < coinArray.length; i++) {
		coinArray[i].x += coinArray[i].drift;
			if (coinArray[i].y < context.canvas.height) 
				coinArray[i].y += coinArray[i].speed;
				if (coinArray[i].y > context.canvas.height) { //Deduct a point is the coin hits the bottom
					coinArray[i].y = 0; 
					CaughtCoins -= (CaughtCoins < 1)? 0: 1;
				}
			if (coinArray[i].x > context.canvas.width)
				coinArray[i].x = 0;
			if (coinArray[i].spin > 30) {  //only 16 frames
				coinArray[i].spin = 0;
			} else {
			  coinArray[i].spin++ ;
			}
    }
}
			
function show_access(obj){
	clear_access();
	//stopanimate();
	document.getElementById(obj).style.display='block';
	document.getElementById(obj+'wrap').style.display='block';
	document.getElementById('mask').style.display='block';
	if (obj == "loginbox"){
		document.getElementById('username').focus();
	}else{
		document.getElementById('regname').focus();
	}
}

function clear_access(){
	document.getElementById('loginboxwrap').style.display='none';
	document.getElementById('regboxwrap').style.display='none';
	document.getElementById('mask').style.display='none';
	document.getElementById('loginbox').style.display='none';
	document.getElementById('regbox').style.display='none';
	document.getElementById('regsuccess').style.display='none';
}

function hide_access(){
	document.getElementById('loginboxwrap').style.display='none';
	document.getElementById('regboxwrap').style.display='none';
	document.getElementById('mask').style.display='none';
	document.getElementById('loginbox').style.display='none';
	document.getElementById('regbox').style.display='none';
	document.getElementById('regsuccess').style.display='none';
	//init();
}

function chkForm(){	
	document.getElementById('perror1').innerHTML="";
	document.getElementById('perror2').innerHTML="";
	document.getElementById('uerror').innerHTML="";
	emailaddress=document.getElementById('regname').value;
	isValid=validateEmail(emailaddress);

	if (!isValid){
		document.getElementById('uerror').innerHTML="x";
		document.getElementById('errmsg').innerHTML="Invalid email";
		document.getElementById('regname').focus();
		return false;
	}
	
	if (document.getElementById('password1').value.length < 4 ){
		document.getElementById('perror1').innerHTML="x";
		document.getElementById('errmsg').innerHTML="Password too short";
		document.getElementById('password1').focus();
		return false;
	}
	
	if (document.getElementById('password1').value != document.getElementById('password2').value){
		document.getElementById('perror2').innerHTML="x";
		document.getElementById('perror1').innerHTML="";
		document.getElementById('errmsg').innerHTML="Password do not match";
		document.getElementById('password2').focus();
		return false;
	}
	if (document.getElementById('recaptcha_response_field').value == '' ){
		document.getElementById('errmsg').innerHTML="Please enter Captcha verfication code";
		document.getElementById('recaptcha_response_field').focus();
		return false;
	}
	return true;
}

function validateEmail(str){ 
 return (str.indexOf(".") > 2) && (str.indexOf("@") > 0);
}