var canvas = null;
var context = null;
var bufferCanvas = null;
var bufferCanvasCtx = null;
var loadingMaster =  null;
var wallpaper = null;
var wallpaperoffset = null;
var wallscroll = 16;
var offsetX = 0;
var offsetY = 0;
var msgArray = [];
var coinTimer = null;
var debugtxt = '------';
var clickposX = 0;
var clickposY = 0;
var resetClick	= null;
var animatetimer=null;
var loadtimer=null;
var scalefactor=0.8;


function init() { //The constructor you might say
	//Main Canvas
	canvas = document.getElementById('Canvas');
	context = canvas.getContext("2d");
	context.canvas.width = window.innerWidth-5;
	context.canvas.height = window.innerHeight-5;                                                                                                                //context.canvas.width = window.innerWidth-5;
	
	//Buffer for the canvas, exact size of image
	bufferCanvas = document.createElement("canvas");
	bufferCanvasCtx = bufferCanvas.getContext("2d");
	bufferCanvasCtx.canvas.width = document.getElementById("logo").width;
	bufferCanvasCtx.canvas.height = document.getElementById("logo").height;

	//to scale the buffer display the fixed buffer to the display buffer, will need this to get the screen position
	scalefactor=context.canvas.height/document.getElementById("logo").height;

	//set the position of the play area on the flexible canvas. actuall hieght is dynamic, so no need to alter hieght position/
	offsetX = (((context.canvas.width-document.getElementById("logo").width*scalefactor)-3)/2);
	offsetY = 0
	if (offsetX < 0) {offsetX=0;}
	
	//Set keyboad handler
	context.canvas.addEventListener("click", GetOnClick, false);

	//load assets (Could be a delayed function to run in background and display a loading image while waiting,
	wallpaper = new Image();
	wallpaper.src = './images/wallpaper.jpg';

	
	//start the animatn timer, here is the frames per second.
	animatetimer = setInterval(animate, 200);
}

function cleanup(){ //Called from resize form to reset the dynamics
	clearInterval(animatetimer);
	clearInterval(loadtimer);
	init();
}

function Draw(){
	context.save();
	blank();

	//write something to the background

	
	// write the x,y stuff for development
	bufferCanvasCtx.textAlign = "center";
	bufferCanvasCtx.textBaseline = "bottom";
	bufferCanvasCtx.strokeStyle = "blue";
	bufferCanvasCtx.font = "13pt Calibri";
	bufferCanvasCtx.fillText(debugtxt, bufferCanvas.width/2, bufferCanvas.height);

	
	// write the msg box
	bufferCanvasCtx.textAlign = "left";
	bufferCanvasCtx.textBaseline = "top";
	bufferCanvasCtx.fillText(msgArray[0], 5, 600);

	
	// copy the entire rendered image from the buffer canvas to the visible one
	context.fillstyle = "AFFF2F";
	context.fillRect(0,0,context.canvas.width, context.canvas.height); 
	context.drawImage(bufferCanvas, offsetX , offsetY ,bufferCanvas.width*scalefactor, bufferCanvas.height*scalefactor);
	context.restore();
}

function GetOnClick(pos){ //need offset for reading input but not for writing to buffer.
  msgbox("---"); // clean the message,,, idea to add exploding click
	if (pos.pageX != undefined && pos.pageY != undefined) {
		clickposX=parseInt((pos.pageX - offsetX)/scalefactor);
		clickposY=parseInt((pos.pageY - offsetY)/scalefactor);
		debugtxt="X:"+clickposX+"*Y:"+clickposY;
	
		if  (((clickposX > 25) && (clickposY > 102)) && ((clickposX < 240) && (clickposY < 344))) { //this is the avatar box
			msgbox('Avatar box');
		}
		if  (((clickposX > 25) && (clickposY > 400)) && ((clickposX < 240) && (clickposY < 450))) { //this is the avatar box
			msgbox('Buy coins');
		}
		if  (((clickposX > 25) && (clickposY > 465)) && ((clickposX < 120) && (clickposY < 500))) { //this is the avatar box
			msgbox('Log out');
		}
		if  (((clickposX > 140) && (clickposY > 465)) && ((clickposX < 240) && (clickposY < 500))) { //this is the avatar box
			msgbox('Help');
		}
		if  (((clickposX > 358) && (clickposY > 135)) && ((clickposX < 475) && (clickposY < 320))) { //this is the avatar box
			msgbox('Door 1');
		}
		if  (((clickposX > 560) && (clickposY > 135)) && ((clickposX < 716) && (clickposY < 320))) { //this is the avatar box
			msgbox('Door 2');
		}
		if  (((clickposX > 830) && (clickposY > 135)) && ((clickposX < 950) && (clickposY < 320))) { //this is the avatar box
			msgbox('Door 3');
		}
		if  (((clickposX > 358) && (clickposY > 445)) && ((clickposX < 475) && (clickposY < 635))) { //this is the avatar box
			msgbox('Door 4');
		}
		if  (((clickposX > 560) && (clickposY > 445)) && ((clickposX < 716) && (clickposY < 635))) { //this is the avatar box
			msgbox('Door 5');
		}
		if  (((clickposX > 830) && (clickposY > 445)) && ((clickposX < 950) && (clickposY < 635))) { //this is the avatar box
			msgbox('Door 6');
		}

	}
}

function blank() { //draw the master background image, good place to animat the background too.
  bufferCanvasCtx.clearRect(0, 0, canvas.width, canvas.height);  //clean
	for (var i = 0; i < bufferCanvasCtx.canvas.height ; i += wallpaper.height /2 ) {
		bufferCanvasCtx.drawImage(wallpaper, 0, 0+wallscroll, 1021 , wallpaper.height /2 ,0 , i, 1021, wallpaper.height /2 );
	}
	wallscroll = (wallscroll >0 )?wallscroll - 1 : 16 ;
	bufferCanvasCtx.drawImage(document.getElementById("logo"), 0, 0);
}

function animate() {
	// do other calulate animations here.
	Draw();
}

function msgbox(msgtxt) {
msgArray[0] = msgtxt;
}