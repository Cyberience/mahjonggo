var canvas = null;
var context = null;
var bufferCanvas = null;
var bufferCanvasCtx = null;
var loadingMaster =  null;
var layer1 = null;
var layer1direction = 1;
var layer3 = null;
var layer3direction = 1;
var layer1timer = null;
var layer3timer = null;
var playbar = null;
var scroll1 = 0;
var scroll2 = 0;
var scroll3 = 0;
var offsetX = 0;
var offsetY = 0;
var msgArray = [];
var coinTimer = null;
var debugtxt = '';
var clickposX = 0;
var clickposY = 0;
var resetClick	= null;
var animatetimer=null;
var scalefactor=1;
var hall = {left:null, bottom:null, banner:null, door1:null, door2:null};
var door = {img:null, bakimage:null, type:1,type1:null,type2:null, open:false, x:0, y:0, width:119, height:190, frame:0, obj:null, display:false};
var bakimage = {d1:null, d2:null, d3:null,d4:null,d5:null,d6:null, bad:null} ;
var user = {name:'Guest' , coins:0, avatar:null} ;
var imgsLoaded=0;
msgArray[0] = '';
var lastMsgid=0;
var keyCapture=1;
var alerttimer = null;
var chattimer = null;
var getusertimer = null;

function init() {
	canvas = document.getElementById('Canvas');
	context = canvas.getContext("2d");
	context.canvas.width = window.innerWidth-5;
	context.canvas.height = window.innerHeight-5;                                                                                                                //context.canvas.width = window.innerWidth-5;
	bufferCanvas = document.createElement("canvas");
	bufferCanvasCtx = bufferCanvas.getContext("2d");
	bufferCanvasCtx.canvas.width = 1024 ;
	bufferCanvasCtx.canvas.height = 768;
	scalefactor=context.canvas.height/bufferCanvasCtx.canvas.height;
	offsetX = (((context.canvas.width-bufferCanvasCtx.canvas.width*scalefactor)-3)/2);
	offsetY = 0
	if (offsetX < 0) {offsetX=0;}
	context.canvas.addEventListener("click", GetOnClick, false);
	context.canvas.addEventListener("mousemove", GetOnMouseMove, false);
	avatar = new Image();
	avatar.src = './includes/get_avatar.php?rnd='+Math.round(new Date().getTime() / 1000) ;
	avatarframe = new Image();
	avatarframe.src = './images/avatarframe2.png' ;
	layer1 = new Image();
	layer1.src = './images/layer1-hk.jpg';
	layer3 = new Image();
	layer3.src = './images/layer2-cboat.png';
	hall.left = new Image();
	hall.left.src = './images/panel_left.png';
	hall.bottom = new Image();
	hall.bottom.src = './images/panel_bot.jpg';
	hall.banner = new Image();
	hall.banner.src = './images/hkbanner.png';
	hall.door1 = new Image();
	hall.door1.src = './images/door1.png';
	hall.door2 = new Image();
	hall.door2.src = './images/door2.png';
	door.type1 = new Image();
	door.type1.src = './images/doortype1.png';
	door.type2 = new Image();
	door.type2.src = './images/doortype2.png';
	bakimage.d1 = new Image();
	bakimage.d1.src = './images/room1.jpg';
	bakimage.d2 = new Image();
	bakimage.d2.src = './images/room2.jpg';
	bakimage.d3 = new Image();
	bakimage.d3.src = './images/room3.jpg';
	bakimage.d4 = new Image();
	bakimage.d4.src = './images/room4.jpg';
	bakimage.d5 = new Image();
	bakimage.d5.src = './images/room5.jpg';
	bakimage.d6 = new Image();
	bakimage.d6.src = './images/room6.jpg';
	bakimage.bad = new Image();
	bakimage.bad.src = './images/toilet.png';
	playbar = new Image();
	playbar.src = './images/playbar.png';
	startAnimations();
}
function startAnimations(){
	//start the animatn timer, here is the frames per second.
	animatetimer = setInterval(animate, 10);
	layer1timer = setInterval(movelayer1, 100);
	layer3timer = setInterval(movelayer3, 70);
	chattimer = setTimeout(pullChat, 3000);
	alerttimer = setTimeout(alerts_pull, 5000);
	getusertimer =	setTimeout(getUser, 2000);
}
function cleanup(){ //Called from resize form to reset the dynamics
	clearInterval(animatetimer);
	clearInterval(layer1timer);
	clearInterval(layer3timer);
	clearTimeout(chattimer);
	clearTimeout(alerttimer);
	clearTimeout(getusertimer);
	init();
}
function Draw(){
	context.save();
	blank(); //draws the background.
	if (door.display) {
	  bufferCanvasCtx.drawImage(door.bakimage, 0 , 0, door.bakimage.width, door.bakimage.height , door.x , door.y , door.width, door.height);
	  bufferCanvasCtx.drawImage( door.img, door.width*door.frame, 0, door.width, door.height , door.x , door.y , door.width, door.height);
	}
	//Avatar Management and frame, picture first,t hen fram in front. before text, after background clean up.
	//bufferCanvasCtx.drawImage(avatar, (avatar.width/2)-100 , 0, 200, 200 , 34, 100, 200,200);
	if ((avatar.width/2)-70 <= 0){avWidth=0;}else{avWidth=(avatar.width/2)-70;}
	bufferCanvasCtx.drawImage(avatar, avWidth , 0, 205-64, 294-102 , 64, 102, 205-64,294-102);
	bufferCanvasCtx.drawImage(avatarframe, 0 , 0, avatarframe.width, avatarframe.height , 2, 55, 264,260);

	//all text shit here
	bufferCanvasCtx.textAlign = "center";
	bufferCanvasCtx.textBaseline = "bottom";
	bufferCanvasCtx.font = "14pt Calibri";
	bufferCanvasCtx.fillStyle = "red";
	bufferCanvasCtx.fillText(debugtxt, bufferCanvas.width/2, bufferCanvas.height);
	bufferCanvasCtx.font = "16pt Calibri";
	bufferCanvasCtx.fillText(user.coins, 140 , 375);
	bufferCanvasCtx.linewidth= 2;
	bufferCanvasCtx.fillStyle = "blue";
	bufferCanvasCtx.font = "16pt Georgia";
	bufferCanvasCtx.fillText(user.name, 130 , 340);
	bufferCanvasCtx.textAlign = "left";
	bufferCanvasCtx.textBaseline = "top";
	bufferCanvasCtx.fillText(msgArray[0], 313, 715);
	bufferCanvasCtx.font = "10pt Calibri";
	bufferCanvasCtx.fillStyle = "black";
	for (var i = 1; i < msgArray.length; i++) {
		bufferCanvasCtx.fillText(msgArray[i], 1, 532 + (11*i));
	}
	
	//move buffer image that was painstakingly built into view.
	context.fillRect(0,0,context.canvas.width, context.canvas.height); 
	context.drawImage(bufferCanvas, offsetX , offsetY ,parseInt(bufferCanvas.width * scalefactor), parseInt(bufferCanvas.height * scalefactor));
	context.restore();
}
function GetOnMouseMove(pos){ //need offset for reading input but not for writing to buffer.
	if (pos.pageX != undefined && pos.pageY != undefined) {
		posX=parseInt((pos.layerX - offsetX)/scalefactor);
		posY=parseInt((pos.layerY - offsetY)/scalefactor);
		debugtxt="X:"+posX+"*Y:"+posY;
		if (posX > 25 && posX < 120 && posY > 465 && posY < 500){ //Logout button
			document.body.style.cursor = 'pointer';
		}else if (posX > 25 && posY > 400 && posX < 240 && posY < 450){ //Buy coins button
			document.body.style.cursor = 'pointer';
		}else if (posX > 140 && posY > 465 && posX < 240 && posY < 500){ // Help Button
			document.body.style.cursor = 'pointer';
		}else if (posX > 25 && posY > 102 && posX < 240 && posY < 344){  // Avatar
			document.body.style.cursor = 'pointer';
		}else if  (((posX > 358) && (posY > 135)) && ((posX < 475) && (posY < 320))) { //this is the door1
			opendoor(1,358,135,1); //door1
		}else if  (((posX > 595) && (posY > 135)) && ((posX < 718) && (posY < 320))) { //this is the avatar box
			opendoor(2,600,134,2); //door2
		}else if  (((posX > 830) && (posY > 135)) && ((posX < 950) && (posY < 320))) { //this is the avatar box
			opendoor(2,840,134,3); //door3
		}else if  (((posX > 358) && (posY > 445)) && ((posX < 475) && (posY < 635))) { //this is the avatar box
			opendoor(2,358,442,4); //door4
		}else if  (((posX > 595) && (posY > 445)) && ((posX < 718) && (posY < 635))) { //this is the avatar box
			opendoor(2,600,442,5); //door5
		}else if  (((posX > 830) && (posY > 445)) && ((posX < 950) && (posY < 635))) { //this is door 6
			opendoor(1,840,442,6); //door6
		}else if  (((posX > 905) && (posY > 711)) && ((posX < 932) && (posY < 738))) { //this is Music Stop
			document.body.style.cursor = 'pointer';
		}else if  (((posX > 939) && (posY > 713)) && ((posX < 959) && (posY < 740))) { //this is Music Play
			document.body.style.cursor = 'pointer';
		}else {
			if (door.open == true){playSound('door-close');}
		    door.open = false; 
			document.body.style.cursor = 'auto';
		}				
	}
}
function opendoor(doortype,posx,posy,position){
	if (door.open == false && door.display == false) {
		playSound('door-open');
		door.type = doortype;
		door.open = true;
		door.display = true;
		door.x = posx;
		door.y = posy;
		door.obj = setInterval(animatedoor, 65);
		door.img = ((door.type==1)?door.type1:door.type2)
		door.width = (doortype==1)?119:120; 
		door.height = (door.type==1)?door.type1.height:door.type2.height;
		if (position == Math.floor(Math.random()*6)) position=0;
		switch(position){
		case 1:	door.bakimage = bakimage.d1;
				break;
		case 2:	door.bakimage = bakimage.d2;
				break;
		case 3:	door.bakimage = bakimage.d3;
				break;
		case 4:	door.bakimage = bakimage.d4;
				break;
		case 5:	door.bakimage = bakimage.d5;
				break;
		case 6:	door.bakimage = bakimage.d6;
				break;
		default: door.bakimage = bakimage.bad;
				}
	}
}
function GetOnClick(pos){ //need offset for reading input but not for writing to buffer.
	if (pos.pageX != undefined && pos.pageY != undefined) {
		clickposX=parseInt((pos.pageX - offsetX)/scalefactor);
		clickposY=parseInt((pos.pageY - offsetY)/scalefactor);
		if  (((clickposX > 25) && (clickposY > 102)) && ((clickposX < 240) && (clickposY < 314))) { //this is the avatar box
			popUploader();
		} else if  (((clickposX > 25) && (clickposY > 315)) && ((clickposX < 240) && (clickposY < 344))) { //this is the set name
			setUserDetailsForm();
		} else if  (((clickposX > 25) && (clickposY > 400)) && ((clickposX < 240) && (clickposY < 450))) { //this is the buy coins

		} else if  (((clickposX > 25) && (clickposY > 465)) && ((clickposX < 120) && (clickposY < 500))) { //this is the logout
			document.body.style.cursor = 'auto';
			message='<br>Logout. Are you sure?<br><br><a href="?a=logout">YES</a> &nbsp; <a href="javascrip: void(0)" onclick="return closeConfirm();">NO</a>';
			openConfirm(message);
		} else if  (((clickposX > 140) && (clickposY > 465)) && ((clickposX < 240) && (clickposY < 500))) { //this is the Help

		} else if  (((clickposX > 358) && (clickposY > 135)) && ((clickposX < 475) && (clickposY < 320))) { //this is Door 1
			nav("http://mj.live.hk.com?p=room&pid=1");
//			alert("room1");
		} else if  (((clickposX > 560) && (clickposY > 135)) && ((clickposX < 716) && (clickposY < 320))) { //this is Door 2
			
			nav("http://mj.live.hk.com?p=room&pid=2");
		} else if  (((clickposX > 830) && (clickposY > 135)) && ((clickposX < 950) && (clickposY < 320))) { //this is Door 3
			nav("http://mj.live.hk.com?p=room&pid=3");
		} else if  (((clickposX > 358) && (clickposY > 445)) && ((clickposX < 475) && (clickposY < 635))) { //this is Door 4
			nav("http://mj.live.hk.com?p=room&pid=4");
		} else if  (((clickposX > 560) && (clickposY > 445)) && ((clickposX < 716) && (clickposY < 635))) { //this is Door 5
			nav("http://mj.live.hk.com?p=room&pid=5");
		} else if  (((clickposX > 830) && (clickposY > 445)) && ((clickposX < 950) && (clickposY < 635))) { //this is Door 6
			nav("http://mj.live.hk.com?p=room&pid=6");
		} else if  (((clickposX > 905) && (clickposY > 711)) && ((clickposX < 932) && (clickposY < 738))) { //this is Music Stop
			pauseSound('bgmusic');
		} else if  (((clickposX > 939) && (clickposY > 713)) && ((clickposX < 959) && (clickposY < 740))) { //this is Music Play
			playSound('bgmusic');
		}
	}
}
function openConfirm(confirmTxt){
	winW=window.innerWidth;
	winH=window.innerHeight;
	document.getElementById("confirmation-box").style.left=((winW/2)-175)+'px';
	document.getElementById("confirmation-box").style.top=((winH/2)-75)+'px';
	document.getElementById('mask').style.width='100%';
	document.getElementById('mask').style.height='100%';
	document.getElementById('mask').style.display='block';
	document.getElementById("confirmation-box").innerHTML=confirmTxt;
	document.getElementById("confirmation-box").style.display='block';
}
function closeConfirm(){
	document.getElementById("confirmation-box").style.display='none';
	document.getElementById('mask').style.display='none';
	return false;
}
function movelayer1(){
	scroll1 = scroll1 + layer1direction;
	if (scroll1 < 0){scroll1=0;}
	if (layer1direction > 0) {
		if (scroll1 > layer1.width-bufferCanvasCtx.canvas.width ){
			layer1direction = -1;
		}
	} else {
		scroll1 = scroll1 + layer1direction;
		if (scroll1 <2 ){
			layer1direction = 1;
		}
	}
}
function movelayer3(){
	scroll3 = scroll3 + layer3direction;
	if (layer1direction >0) {
		if (scroll3 > bufferCanvasCtx.canvas.width ){
			layer3direction = -1;
		}
	} else {
		scroll3 = scroll3 + layer3direction;
		if (scroll3 < -40 ){
			layer3direction = 1;
		}
	}	
}
function blank() {
	bufferCanvasCtx.clearRect(0, 0, canvas.width, canvas.height);  //clean
	bufferCanvasCtx.drawImage(layer1, 1+scroll1, 0, 1021 , 768 ,0 , 0, 1021, 768);
	bufferCanvasCtx.drawImage(layer3, 0, 0, 158, 70, scroll3, 600, 158, 70 );
	bufferCanvasCtx.drawImage(hall.bottom, 0, 682);
	bufferCanvasCtx.drawImage(hall.left, 0, 18);
	bufferCanvasCtx.drawImage(hall.door1, 318, 77);
	bufferCanvasCtx.drawImage(hall.door2, 552, 77);
	bufferCanvasCtx.drawImage(hall.door2, 793, 77);
	bufferCanvasCtx.drawImage(hall.door2, 318, 385);
	bufferCanvasCtx.drawImage(hall.door2, 552, 385);
	bufferCanvasCtx.drawImage(hall.door1, 793, 385);
	bufferCanvasCtx.drawImage(hall.banner, 310, 20);
	bufferCanvasCtx.drawImage(playbar, 0, 0, 94, 32, 870, 710, 94, 32 );
}
function animate() {
	Draw();
}
function animatedoor(){
	if (door.open == true && door.frame < 8) {
		door.frame++; } 
	else { if (door.open == false && door.frame > 1) {
		door.frame--;
		door.frame--;
		}
	};
	if (door.open == false && door.frame < 2) {
		clearInterval(door.obj);
		door.display = false;
	}
}
document.onkeypress = function(evt){
	if (keyCapture == '0'){return true;}
	if (typeof evt == 'undefined') {
		evt = window.event;
	}
	var keyCode = evt.charCode ? evt.charCode: evt.keyCode;
	switch(keyCode) {
		case 13:
			chatMessage=trim(msgArray[0].substring(0,msgArray[0].length-1));
			msgArray[0] = '';
			if (!chatMessage == ''){
				sendChat(chatMessage);
			}
			break;
		case 8:
			if (evt.preventDefault) {
				evt.preventDefault();
			}
			break;
		default:
			if (bufferCanvasCtx.measureText(msgArray[0]).width < 368) {
				msgArray[0] = msgArray[0].substring(0,msgArray[0].length-1);
				msgArray[0] += String.fromCharCode(keyCode)+"_";
			}else{
				playSound('ding');
			}
	}
}
document.onkeydown = function(evt) {
	if (keyCapture == '0'){return true;}
	if (typeof evt == 'undefined') {
	 evt = window.event;
	 }
	 if (evt) {
		var keyCode = evt.charCode ? evt.charCode: evt.keyCode;
		if (keyCode == 8) {
			if (evt.preventDefault) {
				evt.preventDefault();
			}
			msgArray[0] = msgArray[0].substring(0,msgArray[0].length-2);
			msgArray[0] += "_";
		return false;
		} 
	}
}
function msgbox(msgtxt) {
	maxLength=42;
	index2=0;
	lastSpace=0;
    for (index = 0; index < msgtxt.length; index += maxLength) {
		if (maxLength > msgtxt.length - index){
			index2 = index + (msgtxt.length - index);
		}else{
			index2 += maxLength;
			tmpmsg=msgtxt.substring(index, index2);
			lastSpace=strrpos(tmpmsg," ");
			if (lastSpace){
				chopped=tmpmsg.length - lastSpace;
				index2 = index2 - chopped;
			}
		}
		msgArray[msgArray.length] = msgtxt.substring(index, index2);
			if (lastSpace){
				index=index - chopped;
			}			
		if (msgArray.length > 18){
			msgArray.splice(1,1);
		}
	}
}
function sendChat(chatMessage){
	timestamp=new Date().getTime();
	chatPushCall = new XMLHttpRequest();
	chatPushCall.open('get', 'includes/chat_push.php?msg='+chatMessage+"&t="+timestamp);
	chatPushCall.onreadystatechange = chat_push_response(chatMessage);
	chatPushCall.send(null); 
}
function chat_push_response(chatMessage) {
	if(chatPushCall.readyState == 4) {
		chatPushResponse=chatPushCall.responseText.split("|");
		if (chatPushResponse[0] == 1){
			debugtxt="push ok";
		}else{
			msgArray[0]=chatMessage;
			msgbox("Unable to Send. Try again.");
		}
	}
}
function pullChat(){
	timestamp=new Date().getTime();
	chatPullCall = new XMLHttpRequest();
	chatPullCall.open('get', 'includes/chat_pull.php?id='+lastMsgid+"&t="+timestamp);
	chatPullCall.onreadystatechange = chat_pull_response;
	chatPullCall.send(null);
	chattimer = setTimeout(pullChat, 3000);
}
function chat_pull_response() {
	if(chatPullCall.readyState == 4) {
		chatCallResponse=chatPullCall.responseText.split("|");
		if (chatCallResponse[0] == 1){
			for (i=1 ; i < chatCallResponse.length ; i=i+2){
				msgbox(chatCallResponse[i]);
			}
			lastMsgid=chatCallResponse[chatCallResponse.length-1];
		}
	}
}

function alerts_pull(){
	alertCall = new XMLHttpRequest();
	alertCall.open('get', 'includes/alert_pull.php');
	alertCall.onreadystatechange = alert_pull_response;
	alertCall.send(null);
	alerttimer = setTimeout(alerts_pull, 60000);
}
function alert_pull_response(){
	if(alertCall.readyState == 4) {
		alertResponse=alertCall.responseText.split("|");
		if (alertResponse[0] == 1){
			alert(alertResponse[1]);		
		}	
	}
}

function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}
function strrpos (haystack, needle, offset) {
    var i = -1;
    if (offset) {
        i = (haystack + '').slice(offset).lastIndexOf(needle);
        if (i !== -1) {
            i += offset;
        }
    } else {
        i = (haystack + '').lastIndexOf(needle);
    }
    return i >= 0 ? i : false;
}
function playSound(sndObject){
	player=document.getElementById(sndObject);
	player.play();
}
function pauseSound(sndObject , vol){
	player=document.getElementById(sndObject);
	player.pause();
}
function getUser(){
	getUserCall = new XMLHttpRequest();
	getUserCall.open('get', 'includes/get-user.php?t='+Math.round(new Date().getTime() / 1000));
	getUserCall.onreadystatechange = get_user_response;
	getUserCall.send(null); 
}
function get_user_response(){
	if(getUserCall.readyState == 4) {
		userCallResponse=getUserCall.responseText.split("|");
		if (userCallResponse[0] == 1){
			user.coins=userCallResponse[1];
			user.name=userCallResponse[2];
		}
	}
}
function setUserDetailsForm(){
	keyCapture=0;
	winW=window.innerWidth;
	winH=window.innerHeight;
	document.getElementById("details-form").style.left=((winW/2)-175)+'px';
	document.getElementById("details-form").style.top=((winH/2)-100)+'px';
	document.getElementById('mask').style.width='100%';
	document.getElementById('mask').style.height='100%';
	document.getElementById('mask').style.display='block';
	document.getElementById("details-form").style.display='block';
	document.getElementById('user-name').value=user.name;
	document.getElementById('user-name').focus();
}
function setUserDetails(){
	document.getElementById('form-message').innerHTML="Processing ...";
	Uname=document.getElementById('user-name').value;
	setUserCall = new XMLHttpRequest();
	setUserCall.open('get', 'includes/set-name.php?name='+Uname);
	setUserCall.onreadystatechange = set_name_response;
	setUserCall.send(null);
}
function set_name_response(){
	if(setUserCall.readyState == 4) {
		document.getElementById("details-form").style.display='none';
		document.getElementById('mask').style.display='none';
		lastMsgid=0;
		keyCapture=1;
		document.getElementById('form-message').innerHTML="";
		cleanup();
	}
}
function closeUserDetailsForm(){
	document.getElementById("details-form").style.display='none';
	document.getElementById('mask').style.display='none';
	keyCapture=1;
}
function popUploader(){
	document.getElementById("uploader").style.display='block';
	keyCapture=0;
	winW=window.innerWidth;
	winH=window.innerHeight;
	document.getElementById("uploader").style.left=((winW/2)-175)+'px';
	document.getElementById("uploader").style.top=((winH/2)-100)+'px';
	document.getElementById('mask').style.width='100%';
	document.getElementById('mask').style.height='100%';
	document.getElementById('mask').style.display='block';
	document.getElementById("uploader").style.display='block';
}
function closeUploader(){
	document.getElementById("uploader").style.display='none';
	document.getElementById('mask').style.display='none';
	keyCapture=1;
}

function nav(theUrl){
  //Do a zoom into the door here to a black room, then nav to the new page, a light switch effect would be good.
  if (theUrl != ""){
	clearInterval(animatetimer);
	clearInterval(layer1timer);
	clearInterval(layer3timer);
	clearInterval(chattimer);
	clearInterval(alerttimer);
	context.canvas.removeEventListener("click", GetOnClick, false);
	context.canvas.removeEventListener("mousemove", GetOnMouseMove, false);
	location.href = theUrl ;
  }
}
