var canvas = null;
var context = null;
var bufferCanvas = null;
var bufferCanvasCtx = null;
var loadingMaster =  null;
var layer1 = null;
var layer3 = null;
var layer1timer = null;
var layer3timer = null;
var scroll1 = 0;
var scroll2 = 0;
var scroll3 = 0;
var offsetX = 0;
var offsetY = 0;
var msgArray = [];
var coinTimer = null;
var debugtxt = '------';
var clickposX = 0;
var clickposY = 0;
var resetClick	= null;
var animatetimer=null;
var loadtimer=null;
var scalefactor=1;
var board = null;

function init() { //The constructor you might say
	//Main Canvas
	canvas = document.getElementById('Canvas');
	context = canvas.getContext("2d");
	context.canvas.width = window.innerWidth-5;
	context.canvas.height = window.innerHeight-5;                                                                                                                //context.canvas.width = window.innerWidth-5;
	
	//Buffer for the canvas, exact size of image
	bufferCanvas = document.createElement("canvas");
//***	bufferCanvas = document.getElementById('Buffer');
	bufferCanvasCtx = bufferCanvas.getContext("2d");
	bufferCanvasCtx.canvas.width = 1024 ; // document.getElementById("logo").width;
	bufferCanvasCtx.canvas.height = 768; //document.getElementById("logo").height;

	//to scale the buffer display the fixed buffer to the display buffer, will need this to get the screen position
	scalefactor=context.canvas.height/bufferCanvasCtx.canvas.height;

	//set the position of the play area on the flexible canvas. actuall hieght is dynamic, so no need to alter hieght position/
	offsetX = (((context.canvas.width-bufferCanvasCtx.canvas.width*scalefactor)-3)/2);
	offsetY = 0
	if (offsetX < 0) {offsetX=0;}
	
	//Set keyboad handler
	context.canvas.addEventListener("click", GetOnClick, false);
	context.canvas.addEventListener("mousemove", GetOnMouseMove, false);

	//load assets (Could be a delayed function to run in background and display a loading image while waiting,
	layer1 = new Image();
	layer1.src = './images/layer1.jpg';
	layer3 = new Image();
	layer3.src = './images/layer3-objects.png';
	board = new Image();
	board.src = './images/hall.png';

	//start the animatn timer, here is the frames per second.
	animatetimer = setInterval(animate, 10);
	layer1timer = setInterval(movelayer1, 100);
	layer3timer = setInterval(movelayer3, 70);

}

function cleanup(){ //Called from resize form to reset the dynamics
	clearInterval(animatetimer);
	clearInterval(loadtimer);
	clearInterval(layer1timer);
	clearInterval(layer3timer);
	init();
}

function Draw(){
	context.save();
	blank();

	//write something to the background

	
	// write the x,y stuff for development
	bufferCanvasCtx.textAlign = "center";
	bufferCanvasCtx.textBaseline = "bottom";
	bufferCanvasCtx.strokeStyle = "blue";
	bufferCanvasCtx.font = "13pt Calibri";
	bufferCanvasCtx.fillText(debugtxt, bufferCanvas.width/2, bufferCanvas.height);

	
	// write the msg box
	bufferCanvasCtx.textAlign = "left";
	bufferCanvasCtx.textBaseline = "top";
	bufferCanvasCtx.fillText(msgArray[0], 5, 550);

	
	// copy the entire rendered image from the buffer canvas to the visible one
//	context.fillstyle = "AFFF2F";
	context.fillRect(0,0,context.canvas.width, context.canvas.height); 
	context.drawImage(bufferCanvas, offsetX , offsetY ,parseInt(bufferCanvas.width * scalefactor), parseInt(bufferCanvas.height * scalefactor));
	context.restore();
}


function GetOnMouseMove(pos){ //need offset for reading input but not for writing to buffer.
  msgbox("---"); // clean the message,,, idea to add exploding click
	if (pos.pageX != undefined && pos.pageY != undefined) {
		posX=parseInt((pos.layerX - offsetX)/scalefactor);
		posY=parseInt((pos.layerY - offsetY)/scalefactor);
		debugtxt="X:"+posX+"*Y:"+posY;
		msgbox('Mouse Moved');
		
		if (posX > 25 && posX < 120 && posY > 465 && posY < 500){ //Logout button
			document.body.style.cursor = 'pointer';
		}else if (posX > 25 && posY > 400 && posX < 240 && posY < 450){ //Buy coins button
			document.body.style.cursor = 'pointer';
		}else if (posX > 140 && posY > 465 && posX < 240 && posY < 500){ // Help Button
			document.body.style.cursor = 'pointer';
		}else if (posX > 25 && posY > 102 && posX < 240 && posY < 344){  // Avatar
			document.body.style.cursor = 'pointer';
		}else{
			document.body.style.cursor = 'auto';
		}
	}

}

function GetOnClick(pos){ //need offset for reading input but not for writing to buffer.
  msgbox("---"); // clean the message,,, idea to add exploding click
	if (pos.pageX != undefined && pos.pageY != undefined) {
		clickposX=parseInt((pos.pageX - offsetX)/scalefactor);
		clickposY=parseInt((pos.pageY - offsetY)/scalefactor);
		debugtxt="X:"+clickposX+"*Y:"+clickposY;
	
		if  (((clickposX > 25) && (clickposY > 102)) && ((clickposX < 240) && (clickposY < 344))) { //this is the avatar box
			msgbox('Avatar box');
		}
		if  (((clickposX > 25) && (clickposY > 400)) && ((clickposX < 240) && (clickposY < 450))) { //this is the avatar box
			msgbox('Buy coins');
		}
		if  (((clickposX > 25) && (clickposY > 465)) && ((clickposX < 120) && (clickposY < 500))) { //this is the avatar box
			msgbox('Log out');
			document.body.style.cursor = 'auto';
			message='<br>Logout. Are you sure?<br><br><a href="?a=logout">YES</a> &nbsp; <a href="javascrip: void(0)" onclick="return closeConfirm();">NO</a>';
			openConfirm(message);
		}
		if  (((clickposX > 140) && (clickposY > 465)) && ((clickposX < 240) && (clickposY < 500))) { //this is the avatar box
			msgbox('Help');
		}
		if  (((clickposX > 358) && (clickposY > 135)) && ((clickposX < 475) && (clickposY < 320))) { //this is the avatar box
			msgbox('Door 1');
		}
		if  (((clickposX > 560) && (clickposY > 135)) && ((clickposX < 716) && (clickposY < 320))) { //this is the avatar box
			msgbox('Door 2');
		}
		if  (((clickposX > 830) && (clickposY > 135)) && ((clickposX < 950) && (clickposY < 320))) { //this is the avatar box
			msgbox('Door 3');
		}
		if  (((clickposX > 358) && (clickposY > 445)) && ((clickposX < 475) && (clickposY < 635))) { //this is the avatar box
			msgbox('Door 4');
		}
		if  (((clickposX > 560) && (clickposY > 445)) && ((clickposX < 716) && (clickposY < 635))) { //this is the avatar box
			msgbox('Door 5');
		}
		if  (((clickposX > 830) && (clickposY > 445)) && ((clickposX < 950) && (clickposY < 635))) { //this is the avatar box
			msgbox('Door 6');
		}

	}
}

function openConfirm(confirmTxt){
	winW=window.innerWidth;
	winH=window.innerHeight;
	document.getElementById("confirmation-box").style.left=((winW/2)-175)+'px';
	document.getElementById("confirmation-box").style.top=((winH/2)-75)+'px';
	document.getElementById('mask').style.width='100%';
	document.getElementById('mask').style.height='100%';
	document.getElementById('mask').style.display='block';
	document.getElementById("confirmation-box").innerHTML=confirmTxt;
	document.getElementById("confirmation-box").style.display='block';
}

function closeConfirm(){
	document.getElementById("confirmation-box").style.display='none';
	document.getElementById('mask').style.display='none';
	return false;
}

function movelayer1(){
	scroll1 = (scroll1 < layer1.width-bufferCanvasCtx.canvas.width )?scroll1 + 1 : 0 ;
}

function movelayer3(){
	scroll3 = (scroll3 < layer3.width-bufferCanvasCtx.canvas.width )?scroll3 + 1 : 0 ;
}


function blank() { //draw the master background image, good place to animat the background too.
  bufferCanvasCtx.clearRect(0, 0, canvas.width, canvas.height);  //clean
  
	//place layer 1, slow scroll
	bufferCanvasCtx.drawImage(layer1, 1+scroll1, 0, 1021 , layer1.height ,0 , 0, 1021, layer1.height );
	//place layer 3. faster scroll
	bufferCanvasCtx.drawImage(layer3, 1+scroll3, 0, 1021 , layer3.height ,0 , 0, 1021, layer3.height );
	//update movement

	bufferCanvasCtx.drawImage(board, 0, 0);
}

function animate() {
	// do other calulate animations here.
	Draw();
}

function msgbox(msgtxt) {
msgArray[0] = msgtxt;
}