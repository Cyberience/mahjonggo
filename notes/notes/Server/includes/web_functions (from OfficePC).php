<?php

function loadpage($page) {
global $signupmessage;


switch ($page) {
	case 'hall': 
		include_once('includes/hal_functions.php');
		$retval = readFileAll("tpl/hall.html");
		//any variables and tags to replace do it here.
		break;
	
	case 'room1': 
		include_once('includes/room_functions.php');
		$retval = readFileAll("tpl/room.html");
		//any variables and tags to replace do it here.
		break;
	break;
	
	default:
			$retval = readFileAll("tpl/welcome.html");
			
			// **** Add RE_Captcha code **** //
			require_once('includes/recaptchalib.php');
			$publickey = "6Lf9WcgSAAAAAFXafSPujH4h_ur5ahbrXQMIBcSE";
			$retval = str_replace('<!---captcha--->',recaptcha_get_html($publickey),$retval);
			// **** END add RE-Captcha **** //
			
			if ($signupmessage){
				if ($signupmessage == 'success'){
					$jscript="show_access('regsuccess');";
					$retval = str_replace('<!---footerjavascript--->',$jscript,$retval);			
				}else if($signupmessage == 'vsuccess'){
					$jscript= "document.getElementById('regsuccesstxt').innerHTML='<br>Registration complete.<br><br>You may proceed to login<br><br>Please wait, redirecting...';"
							." show_access('regsuccess'); setTimeout(\"location.href='http://mj.live.hk.com'\",4000);";
					$retval = str_replace('<!---footerjavascript--->',$jscript,$retval);
				}else if($signupmessage == 'vfailed'){
					$jscript= "document.getElementById('regsuccesstxt').innerHTML='<br><br>Verification code invalid or expired.';"
							." show_access('regsuccess');";
					$retval = str_replace('<!---footerjavascript--->',$jscript,$retval);
				}else{
					$jscript="document.getElementById('errmsg').innerHTML='".$signupmessage."'; show_access('regbox');"
						."document.getElementById('regname').value='".$_REQUEST['u']."'";
					$retval = str_replace('<!---footerjavascript--->',$jscript,$retval);
				}
			}
	}
return $retval;
}
	
	//==========--------------------=========================================
// Some generic routines supporting templates
function readFileAll($filename){
  $handle = fopen($filename, "r");
  $contents = fread($handle, filesize($filename));
  fclose($handle);
  return $contents;
} 
?>