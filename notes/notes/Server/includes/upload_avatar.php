<?php
session_start();

if (!isset($_SESSION['uid'])){
	die;
}
$uid=$_SESSION['uid'];
session_write_close();

$fn = (isset($_SERVER['HTTP_X_FILENAME']) ? $_SERVER['HTTP_X_FILENAME'] : false);

if ($fn){
	$allowed_filetypes = array('jpg','jpeg');
    if(!in_array(end(explode(".", $fn)), $allowed_filetypes)){
		echo 'Not a .JPG image file?';
		die;
	}
	
	$ext=end(explode(".", $fn));
	file_put_contents('avatars/' . $fn, file_get_contents('php://input')	);
	echo "$fn uploaded";
	
}else{
	// form submit
	$ext='';
	$type=$_FILES['fileselect']['type'];
	$fn = $_FILES['fileselect']['name'];
	
	//if (!$type){ $type = end(explode(".", $fn)); }
	
	$type1="image/pjpeg"; $type2 = "image/jpeg"; $type3 = "jpg"; $type4 = "jpeg";
	if($type==$type1 || $type==$type2 || $type==$type3 || $type==$type4) { $ext="jpg"; }
	if($ext=="") {
		?>
		&nbsp;&nbsp&nbsp&nbspNot an image file?<br><br>&nbsp&nbsp&nbsp&nbspPlease wait....
		<script type='text/javascript'>setTimeout("window.location.href='?a=upload_avatar_form';", 3000);</script>
		<?php die;
	}
	move_uploaded_file($_FILES['fileselect']['tmp_name'], 'avatars/' . $fn);
}

include_once('includes/resize.class.php');
$obj = new Resize("avatars/$fn");
		$obj->setNewImage("avatars/$uid.jpg");
		$obj->setProportionalFlag('V');
		$obj->setProportional(1);
		$obj->setNewSize(192, 192);
		$obj->make();
unlink("avatars/$fn");
?>
echo "Upload complete<script type='text/javascript'>
		function resetForm(){
			parent.closeUploader();
			parent.cleanup();
			window.location.href = 'index.php?a=upload_avatar_form';
		}
		setTimeout('resetForm()', 5000);
		</script>";