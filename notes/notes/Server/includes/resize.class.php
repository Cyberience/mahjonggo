<?php
class Resize {
    function Resize($image) {
        $this->setImage($image);
        return true;
    }
    function setImage($image) {
        $this->image = $image;
            $tmp = getimagesize($this->image);
            $this->width  = $tmp[0];
            $this->height  = $tmp[1];
            $this->imageType = $tmp[2];
    }
	function getImage() {
        return $this->image;
    }
	function setNewImage($newImage) {
        $this->newImage = $newImage;
    }
	function getNewImage() {
        return $this->newImage;
    }
	function setNewSize($newHeight, $newWidth) {
        $this->newHeight = $newHeight;
        $this->newWidth = $newWidth;
    }
    function setProportional($proportional) {
        $this->proportional = $proportional;
    }
    function getProportional() {
        return $this->proportional;
    }
    function setProportionalFlag($proportionalFlag) {
        $this->proportionalFlag = $proportionalFlag;
    }
    function getProportionalFlag() {
        return $this->proportionalFlag;
    }
    function setBgColor($bgColor) {
        $this->bgColor = $bgColor;
    }
    function getBgColor() {
        return $this->bgColor;
    }
   function copyImage() {
        if(!empty($this->image) && !empty($this->newImage)) {
            if(!copy($this->getImage(), $this->getNewImage())) {
                echo"Cannot copy image.";
            }
            else {
                $this->setImage($this->getNewImage());
                $this->setNewImage("");
                return true;
            }
        }
     }
  function make() {
        if(!empty($this->newImage)) {
            $this->copyImage();
        }
        if($this->getProportional() == 1) {
            if($this->getProportionalFlag() == 'H') {
                $this->newHeight = round(($this->newWidth * $this->height) / $this->width);
            }
            elseif($this->getProportionalFlag() == 'V') {
                $this->newWidth = round(($this->newHeight * $this->width) / $this->height);
            }
        }
        elseif($this->getProportional() != 0) {
            echo "Valor incorreto no atributo 'proportional'.";
        }
                $img    = imagecreatefromjpeg($this->getImage());
                $newImg = imagecreatetruecolor($this->newWidth, $this->newHeight);
				imagecopyresized($newImg, $img, 0, 0, 0, 0, $this->newWidth, $this->newHeight, $this->width, $this->height);
           		imagejpeg($newImg, $this->getImage(), 90);
        return true;
    }
}
?>