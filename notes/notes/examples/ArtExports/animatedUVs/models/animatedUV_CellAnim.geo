// Source file: C:/Dev/sdk/main/sdk_examples/ArtAssets/animatedUVs/Maya7/animatedUVs_CellAnim.mb
CIwModel
{
	name "animatedUV_CellAnim"
	CMesh
	{
		name "animatedUVs_CellAnim"
		scale 1.0
		CVerts
		{
			numVerts 4
			v {-250,-250,0}
			v {250,-250,0}
			v {-250,250,0}
			v {250,250,0}
		}
		CVertNorms
		{
			numVertNorms 1
			vn {0,0,1}
		}
		CUVs
		{
			setID 0
			numUVs 4
			uv {0,0.25000}
			uv {0.25000,0.25000}
			uv {0,0}
			uv {0.25000,0}
		}
		CSurface
		{
			material "numbersMat"
			CQuads
			{
				numQuads 1
				q {2,0,2,-1,-1} {3,0,3,-1,-1} {1,0,1,-1,-1} {0,0,0,-1,-1}
			}
		}
	}
}
