// Source file: C:/Dev/sdk/main/sdk_examples/ArtAssets/animatedUVs/Maya7/animatedUVs_ScrollAnim.mb
CIwModel
{
	name "animatedUVs_ScrollAnim"
	CMesh
	{
		name "animatedUVs_CellAnim"
		scale 1.0
		CVerts
		{
			numVerts 10
			v {647.74847,250.00015,228.18497}
			v {-584.52295,250.00009,228.18497}
			v {647.74847,-250.00015,228.18497}
			v {-584.52295,-250.00009,228.18497}
			v {586.88477,250,228.18497}
			v {586.88477,-250,228.18497}
			v {-532.02844,250,228.18497}
			v {-532.02844,-250,228.18497}
			v {-832.65759,0,228.18497}
			v {958.66547,0,228.18497}
		}
		CVertNorms
		{
			numVertNorms 1
			vn {0.00000,0,1}
		}
		CVertCols
		{
			numVertCols 2
			col {0,0,0,1}
			col {1,1,1,1}
		}
		CUVs
		{
			setID 0
			numUVs 10
			uv {0.00000,1}
			uv {0.10000,1}
			uv {0.10000,0}
			uv {0.00000,0}
			uv {0.00494,1}
			uv {0.00494,0}
			uv {0.09574,1}
			uv {0.09574,0}
			uv {0.10000,0.50000}
			uv {0.00000,0.50000}
		}
		CSurface
		{
			material "glowMat"
			CQuads
			{
				numQuads 1
				q {5,0,5,-1,1} {7,0,7,-1,1} {6,0,6,-1,1} {4,0,4,-1,1}
			}
			CTris
			{
				numTris 6
				t {9,0,9,-1,0} {4,0,4,-1,1} {0,0,0,-1,0}
				t {7,0,7,-1,1} {8,0,8,-1,0} {6,0,6,-1,1}
				t {8,0,8,-1,0} {1,0,1,-1,0} {6,0,6,-1,1}
				t {3,0,2,-1,0} {8,0,8,-1,0} {7,0,7,-1,1}
				t {2,0,3,-1,0} {5,0,5,-1,1} {9,0,9,-1,0}
				t {5,0,5,-1,1} {4,0,4,-1,1} {9,0,9,-1,0}
			}
		}
	}
	CMesh
	{
		name "animatedUVs_CellAnim1"
		scale 1.0
		CVerts
		{
			numVerts 4
			v {-1445.55347,-658.69226,-426.20264}
			v {1801.19629,-658.69226,-426.20264}
			v {-1445.55347,658.69226,-426.20264}
			v {1801.19629,658.69226,-426.20264}
		}
		CVertNorms
		{
			numVertNorms 1
			vn {0.00000,0,1}
		}
		CUVs
		{
			setID 0
			numUVs 4
			uv {0.00000,1}
			uv {0.10000,1}
			uv {0.10000,0}
			uv {0.00000,0}
		}
		CSurface
		{
			material "background"
			CQuads
			{
				numQuads 1
				q {2,0,3,-1,-1} {3,0,2,-1,-1} {1,0,1,-1,-1} {0,0,0,-1,-1}
			}
		}
	}
}
