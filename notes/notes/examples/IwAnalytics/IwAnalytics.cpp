/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwAnalytics IwAnalytics Example
 *
 * The following example demonstrates how to use the IwAnalytics API.
 *
 * @include IwAnalytics.cpp
 */

#include "IwAnalytics.h"
#include "IwAnalyticsUtil.h"
#include "ExamplesMain.h"

// Status string to print at bottom of screen
static char g_StatusStr[8192]; // Can be quite a long string!

// Buttons
static Button* g_SendButton = NULL;

// Sets a string to be displayed beneath the buttons
static void SetStatus(const char* statusStr, ...)
{
    va_list args;
    va_start(args, statusStr);
    strcpy(g_StatusStr, "`x000000");
    vsprintf(g_StatusStr+strlen(g_StatusStr), statusStr, args);
    va_end(args);
}

// Appends to the string to be displayed beneath the buttons
static void AddStatus(const char* statusStr, ...)
{
    va_list args;
    va_start(args, statusStr);
    int currentLen = strlen(g_StatusStr);
    g_StatusStr[currentLen] = '\n';
    vsprintf(g_StatusStr + currentLen + 1, statusStr, args);
    va_end(args);
}

void ResetButtons()
{
    DeleteButtons();
    g_SendButton = 0;
}

void ReadyCallback(void* caller, void* data)
{
    s3eDebugTracePrintf("===================================================");
    s3eDebugTracePrintf("IwAnalytics is ready to use");
    s3eDebugTracePrintf("===================================================");
    IwAnalytics::SendSessionStart();
    AddStatus("Analytics ready to use, starting session");
}

void ErrorCallback(void* caller, IwAnalytics::eIwAnalyticsError error, const char* error_data)
{
    s3eDebugTracePrintf("===================================================");
    s3eDebugTracePrintf("IwAnalytics error: %d", error);
    s3eDebugTracePrintf("IwAnalytics error data: %s", error_data);
    s3eDebugTracePrintf("===================================================");
}

void ExampleInit()
{
    SetStatus("IwAnalytics Test");

    if (!IwAnalytics::Init(ReadyCallback, ErrorCallback, "game_key", "secret_key", "version"))
    {
        SetStatus("Analytics not supported on this platform");
        return;
    }
    IwAnalytics::setCurrency("USD");

    // Create buttons
    ResetButtons();
    g_SendButton = NewButton("Send info");
}

void ExampleTerm()
{
    IwAnalytics::SendSessionEnd();
    IwAnalytics::Terminate();
}

bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    if (pressed)
    {
        if (g_SendButton == pressed)
        {
            const char* level = "Level 1";
            IwAnalytics::StartBatch();

            IwAnalytics::SendBusinessEvent("teddy", "TEDDY", "USD", 10, "store", 0);
            IwAnalytics::SendDesignEvent("teddy_event1", 1);
            IwAnalytics::SendErrorEvent(IwAnalytics::eErrorSeverity_Error, "Teddy crashed");
            IwAnalytics::SendProgressionEvent(IwAnalytics::eProgressionStatus_Start, "world1", "stage1", "level1", 1, 100);
            IwAnalytics::SendResourceEvent(IwAnalytics::eFlowType_Source, "toy", "TEDDIES", "teddies", 2);

            IwAnalytics::SendGameStarted(1);
            IwAnalytics::SendGameQuit(level);
            IwAnalytics::SendGameComplete();
            IwAnalytics::SendLevelStart("world 1", "stage 1", level);
            IwAnalytics::SendLevelComplete("world 1", "stage 1", level, 2, 1000);
            IwAnalytics::SendLevelFailed("world 1", "stage 1", level, 500);
            IwAnalytics::SendLevelRestart(level);
            IwAnalytics::SendLevelQuit(level);
            IwAnalytics::SendPlayerDied("ogre");
            IwAnalytics::SendPlayerCollected("item 1");
            IwAnalytics::SendAchievementEarnt("achievement 1");
            IwAnalytics::SendBonusEarnt("bonus 1");
            IwAnalytics::SendItemPurchased("gold", "gold50", 99, "USD", "store", 0);
            IwAnalytics::SendResourceGained("wood", "wood10", "chips", 5);
            IwAnalytics::SendResourceUsed("wood", "wood10", "chips", 1);
            IwAnalytics::SendAdShown("ad 1");
            IwAnalytics::SendAdClicked("ad 1");
            IwAnalytics::SendError("Purchase failed");
            IwAnalytics::SendWarning("Frame rate too low");
            IwAnalytics::SendInfo("Player is happy again");
            IwAnalytics::SendDebug("Debugging enabled");

            IwAnalytics::SendBatch();
        }
    }

    IwAnalytics::Update();

    return true;
}

void ExampleRender()
{
    // Print status string just below the buttons
    s3eDebugPrint(0, GetYBelowButtons(), g_StatusStr, S3E_TRUE);
}
