/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwBilling IwBilling Example
 *
 * The following example demonstrates how to use the IwBilling
 * functionality.
 *
 * @include IwBillingTest.cpp
 */

// Marmalade headers
#include "IwBilling.h"
#include "ExamplesMain.h"

// Android public key - Set the public key here or in app.icf
static const char* g_AndroidPublicKey = NULL;

// Test product
static const char* g_ProductID = "com.company.product.item";

// Status string to print at bottom of screen
static char g_StatusStr[8192]; // Can be quite a long string!

//Turn this to true to test deferred transaction finishing
static bool g_TestDeferredTransactionFinish = false;
static void* g_TestDeferredTransaction = NULL;

// Buttons
static Button* g_ProductInfoButton = NULL;
static Button* g_ProductBuyButton = NULL;
static Button* g_RestoreButton = NULL;
static Button* g_ConsumeButton = NULL;
static Button* g_Init = NULL;
static Button* g_Deinit = NULL;

// Purchase token returned by purchase process that can be used to consume product with Google Play billing
std::string g_PurchaseToken;

// Sets a string to be displayed beneath the buttons
static void SetStatus(const char* statusStr, ...)
{
    va_list args;
    va_start(args, statusStr);
    strcpy(g_StatusStr, "`x000000");
    vsprintf(g_StatusStr+strlen(g_StatusStr), statusStr, args);
    va_end(args);
}

// Appends to the string to be displayed beneath the buttons
static void AddStatus(const char* statusStr, ...)
{
    va_list args;
    va_start(args, statusStr);
    int currentLen = strlen(g_StatusStr);
    g_StatusStr[currentLen] = '\n';
    vsprintf(g_StatusStr + currentLen + 1, statusStr, args);
    va_end(args);
}

void ResetButtons()
{
    DeleteButtons();
    g_ProductInfoButton = 0;
    g_ProductBuyButton = 0;
    g_RestoreButton = 0;
    g_ConsumeButton = 0;
}

void ReadyCallback(void* caller, void* data)
{
    s3eDebugTracePrintf("===================================================");
    s3eDebugTracePrintf("Billing is ready to use");
    s3eDebugTracePrintf("===================================================");
}

void ErrorCallback(void* userdata, IwBilling::CIwBillingErrorData* data)
{
    AddStatus("An error occurred");
    AddStatus("Product ID: %s", data->ProductID);
    AddStatus("Error: %s", IwBilling::getErrorString(data->Error));

    s3eDebugTracePrintf("===================================================");
    s3eDebugTracePrintf("An error occurred");
    s3eDebugTracePrintf("Product ID: %s", data->ProductID);
    s3eDebugTracePrintf("Error: %d", (int)data->Error);
    s3eDebugTracePrintf("===================================================");
}

void ProductInfoAvailableCallback(void* userdata, IwBilling::CIwBillingInfoAvailableData* data)
{
    AddStatus("Product info available");
    AddStatus("Product ID: %s", data->ProductID);
    AddStatus("Title: %s", data->Title);
    AddStatus("Description: %s", data->Description);
    AddStatus("Price: %s", data->Price);
    AddStatus("Currency Code: %s", data->CurrencyCode);

    s3eDebugTracePrintf("===================================================");
    s3eDebugTracePrintf("Product info available");
    s3eDebugTracePrintf("Product ID: %s", data->ProductID);
    s3eDebugTracePrintf("Title: %s", data->Title);
    s3eDebugTracePrintf("Description: %s", data->Description);
    s3eDebugTracePrintf("Price: %s", data->Price);
    s3eDebugTracePrintf("Currency Code: %s", data->CurrencyCode);
    s3eDebugTracePrintf("===================================================");
}

void ReceiptAvailableCallback(void* userdata, IwBilling::CIwBillingReceiptAvailableData* data)
{
    if (data->Restored)
        AddStatus("Product restore successfull");
    else
        AddStatus("Product purchase successfull");
    char* receipt = new char[data->ReceiptLength + 1];
    memcpy(receipt, data->Receipt, data->ReceiptLength);
    receipt[data->ReceiptLength] = 0;
    AddStatus("Product ID: %s", data->ProductID);
    if (receipt != NULL)
        AddStatus("Receipt: %s", receipt);
    if (data->TransactionID != NULL)
        AddStatus("TransactionID: %s", data->TransactionID);

    if (data->PurchaseToken != NULL)
    {
        g_PurchaseToken = data->PurchaseToken;
        g_ConsumeButton->m_Enabled = true;
    }

    s3eDebugTracePrintf("===================================================");
    s3eDebugTracePrintf("Receipt available");
    s3eDebugTracePrintf("Product ID: %s", data->ProductID);
    if (receipt != NULL)
        s3eDebugTracePrintf("Receipt: %s", receipt);
    if (data->TransactionID != NULL)
        s3eDebugTracePrintf("TransactionID: %s", data->TransactionID);
    s3eDebugTracePrintf("===================================================");
    if (!g_TestDeferredTransactionFinish)
        IwBilling::FinishTransaction(data->FinaliseData);
    else
        g_TestDeferredTransaction = data->FinaliseData;
    delete [] receipt;
}

void RefundCallback(void* userdata, IwBilling::CIwBillingRefundData* data)
{
    AddStatus("Product was refunded %s", data->ProductID);
    s3eDebugTracePrintf("===================================================");
    s3eDebugTracePrintf("Product was refunded %s", data->ProductID);
    s3eDebugTracePrintf("===================================================");

    IwBilling::FinishTransaction(data->FinaliseData);
}

void ConsumeCallback(void* userdata, void* data)
{
    AddStatus("Product was consumed %s", g_PurchaseToken.c_str());
    g_ConsumeButton->m_Enabled = false;
}

void ExampleInit()
{
    SetStatus("Billing Test");

    // Create buttons
    ResetButtons();
    g_Init = NewButton("Initialize");
    g_Init->m_Enabled = true;
    g_Deinit = NewButton("Deinitialize");
    g_Deinit->m_Enabled = false;
    g_ProductInfoButton = NewButton("Request product info");
    g_ProductInfoButton->m_Enabled = false;
    g_ProductBuyButton = NewButton("Buy product");
    g_ProductBuyButton->m_Enabled = false;
    g_RestoreButton = NewButton("Restore purchased products");
    g_RestoreButton->m_Enabled = false;
    g_ConsumeButton = NewButton("Consume product");
    g_ConsumeButton->m_Enabled = false;
}

void ExampleTerm()
{
    IwBilling::Terminate();
}

bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    if (pressed)
    {
        if (g_TestDeferredTransaction)
        {
            IwBilling::FinishTransaction(g_TestDeferredTransaction);
            g_TestDeferredTransaction = NULL;
        }
        if (g_Init == pressed)
        {
            // Create platform independent billing
            if (!IwBilling::Init(ReadyCallback, (void*)g_AndroidPublicKey, IwBilling::BILLING_VENDOR_GOOGLE_PLAY))
            {
                SetStatus("Billing not supported on this platform");
                return false;
            }

            // Set up callbacks
            IwBilling::setInfoAvailableCallback(ProductInfoAvailableCallback, 0);
            IwBilling::setErrorCallback(ErrorCallback, 0);
            IwBilling::setReceiptAvailableCallback(ReceiptAvailableCallback, 0);
            IwBilling::setRefundCallback(RefundCallback, 0);
            IwBilling::setConsumeCallback(ConsumeCallback, 0);
            g_Init->m_Enabled = false;
            g_Deinit->m_Enabled = true;
            g_ProductInfoButton->m_Enabled = true;
            g_ProductBuyButton->m_Enabled = true;
            g_RestoreButton->m_Enabled = true;
            g_ConsumeButton->m_Enabled = false;
            SetStatus("Billing initialized");
        }
        else if (g_Deinit == pressed)
        {
            IwBilling::Terminate();
            g_Init->m_Enabled = true;
            g_Deinit->m_Enabled = false;
            g_ProductInfoButton->m_Enabled = false;
            g_ProductBuyButton->m_Enabled = false;
            g_RestoreButton->m_Enabled = false;
            g_ConsumeButton->m_Enabled = false;
            SetStatus("Billing destroyed");
        }
        else if (g_ProductInfoButton == pressed)
        {
            IwBilling::QueryProducts(&g_ProductID, 1);
        }
        else
        if (g_ProductBuyButton == pressed)
        {
            IwBilling::PurchaseProduct(g_ProductID);
        }
        else
        if (g_RestoreButton == pressed)
        {
            IwBilling::RestoreTransactions();
        }
        else
        if (g_ConsumeButton == pressed)
        {
            if (!g_PurchaseToken.empty())
                IwBilling::ConsumeProduct(g_PurchaseToken.c_str());
            else
                AddStatus("There is nothing to consume");
        }
    }

    return true;
}

void ExampleRender()
{
    // Print status string just below the buttons
    s3eDebugPrint(0, GetYBelowButtons(), g_StatusStr, S3E_TRUE);
}
