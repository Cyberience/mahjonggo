/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */


/**
 * @page ExampleIwAnimAnimationRendering IwAnim Animation Rendering Example
 *
 * Demonstrates loading of animation resources, and rendering
 * of the animated model.
 *
 * The main classes used to achieve this are:
 *  - CIwModel
 *  - CIwAnim
 *  - CIwAnimSkel
 *  - CIwAnimSkin
 *  - CIwAnimPlayer
 *
 * The main functions used to achieve this are:
 *  - CIwAnim::IwAnimInit()
 *  - CIwResManager::LoadGroup()
 *  - CIwResManager::GetGroupNamed()
 *  - CIwResGroup::GetResNamed()
 *  - CIwAnimPlayer::SetSkel()
 *  - CIwAnimPlayer::PlayAnim()
 *  - IwAnimSetSkelContext()
 *  - IwAnimSetSkinContext()
 *
 * IwAnimInit() is first called to initialise the IwAnim functionality.
 * The user must have initialised IwGx and IwGraphics before IwAnim
 * can be initialised. The LoadGroup() function is used to load the
 * animation group. The animation group, or the group file, holds all
 * the information that makes up the animation, namely:
 *  - geo file
 *  - skin file
 *  - skel file
 *  - and anim file.
 *
 * Each of the loaded resources is accessed using the GetResNamed()
 * function. The animation player is then instantiated. The animation
 * player combines the character skeleton and the anim file to create
 * the animation. The example plays the animation by calling the PlayAnim()
 * function.
 *
 * The up, down, left and right keys are set to allow you to
 * manipulate the model matrix to move the animation. The 1 and 4 keys
 * allow you to zoom in and out of the animation.
 *
 *
 * @note For more information on Animation, see the "Animation"
 * module overview in the <i>IwAnim API Reference</i> documentation.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwAnimRenderingImage.png
 *
 * @include IwAnimRendering.cpp
 */

#include "IwAnim.h"
#include "IwAnimPlayer.h"
#include "IwAnimSkel.h"
#include "IwAnimSkin.h"
#include "IwGraphics.h"
#include "IwGxPrint.h"
#include "IwModelBuilder.h"
#include "IwResManager.h"
#include "IwTextParserITX.h"
#include "s3eKeyboard.h"
#include "ExamplesMain.h"

// Angles
CIwFVec3    s_Angles;

// Local matrix
CIwFMat      s_ModelMatrix;

// Resources
CIwModel*       s_Model;
CIwAnim*        s_Anim;
CIwAnimSkel*    s_Skel;
CIwAnimSkin*    s_Skin;

// Animation player
CIwAnimPlayer*  s_Player;

void CreateButtonsUI(int w, int h)
{
    // Create the UI layout
    AddButton("Zoom in: 1", 0, 70, 70, 30, s3eKey1);
    AddButton("Zoom out: 2", 0, 110, 70, 30, s3eKey2);
}

void SurfaceChangedCallback()
{
    DeleteButtons();
    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    IwGxRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
    IwGxInit();
    IwGraphicsInit();
    IwAnimInit();

    // Set screen clear colour
    IwGxSetColClear(0x40, 0x40, 0x40, 0x00);

    // Set field of view
    IwGxSetPerspMul(IwGxGetScreenWidth() / 2.0f);

    // Set near and far planes
    IwGxSetFarZNearZ(0x4010, 0x10);

    // Initialise angles
    s_Angles = CIwFVec3(0, 3.0f * PI / 2.0f, PI);

    // Initialise model position
    s_ModelMatrix.t = CIwFVec3(0, 0x200, 0x300);

#ifdef IW_BUILD_RESOURCES
    //Initialise resource templates - This scales Eva up so animation can interpolate smoothly
    if (IwGetResManager()->m_Flags & CIwResManager::BUILDRES_F )
        IwGetTextParserITX()->ParseFile("resource_template.itx");
#endif

    // Parse the GROUP file, which will load the resources
    IwGetResManager()->LoadGroup("IwAnimRendering.group");

    // Get a pointer to the named group
    CIwResGroup* pGroup = IwGetResManager()->GetGroupNamed("ExampleIwAnimRendering");

    // Get and store pointer to the resources
    s_Model = (CIwModel*)pGroup->GetResNamed("combatEva", IW_GRAPHICS_RESTYPE_MODEL);
    s_Skin  = (CIwAnimSkin*)pGroup->GetResNamed("combatEva", IW_ANIM_RESTYPE_SKIN);
    s_Skel  = (CIwAnimSkel*)pGroup->GetResNamed("combatEva_rig", IW_ANIM_RESTYPE_SKELETON);
    s_Anim  = (CIwAnim*)pGroup->GetResNamed("LfStance", IW_ANIM_RESTYPE_ANIMATION);

    // Create animation player
    s_Player = new CIwAnimPlayer;
    s_Player->SetSkel(s_Skel);
    s_Player->PlayAnim(s_Anim, 0.5f, CIwAnimBlendSource::LOOPING_F);

    // Create the UI
    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // Clear up resources
    delete s_Player;

    // Terminate IwAnim
    IwAnimTerminate();
    IwGraphicsTerminate();
    IwGxTerminate();
    IwGxUnRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // Update angles from the arrow keys
    if (CheckCursorState() == EXCURSOR_LEFT )
        s_Angles.y -= PI / 32.0f;
    if (CheckCursorState() == EXCURSOR_RIGHT )
        s_Angles.y += PI / 32.0f;
    if (CheckCursorState() == EXCURSOR_UP )
        s_Angles.x -= PI / 32.0f;
    if (CheckCursorState() == EXCURSOR_DOWN )
        s_Angles.x += PI / 32.0f;

    // Build view matrix rotation from angles
    CIwFMat rotX, rotY, rotZ;
    rotX.SetRotX(s_Angles.x);
    rotY.SetRotY(s_Angles.y);
    rotZ.SetRotZ(PI);
    s_ModelMatrix.CopyRot(rotZ * rotX * rotY);

    // Forward/back
    CIwFVec3 ofs(0, 0, 16);
    if (CheckButton("Zoom out: 2") & S3E_KEY_STATE_DOWN )
        s_ModelMatrix.t += ofs;
    if (CheckButton("Zoom in: 1") & S3E_KEY_STATE_DOWN )
        s_ModelMatrix.t -= ofs;

    // Update animation player
    s_Player->Update(1.0f / 30.0f);

    // Update IwGx state time stamp
    IwGxTickUpdate();
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Start with no lighting
    IwGxLightingOff();

    //-------------------------------------------------------------------------
    // Render the model
    //-------------------------------------------------------------------------
    IwGxSetModelMatrix(&s_ModelMatrix);

    IwAnimSetSkelContext(s_Player->GetSkel());
    IwAnimSetSkinContext(s_Skin);
    s_Model->Render();

    // Tidier to reset these
    IwAnimSetSkelContext(NULL);
    IwAnimSetSkinContext(NULL);

    // Paint the cursor keys buttons and prompt text
    IwGxPrintString(2, IwGxGetScreenHeight() - 60, "Rotate Model: Arrow Keys");
    RenderCursorskeys();

    // End drawing
    IwGxFlush();

    // Swap buffers
    IwGxSwapBuffers();
}
