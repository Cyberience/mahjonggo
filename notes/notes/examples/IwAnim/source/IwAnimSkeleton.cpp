/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwAnimSkeleton IwAnim Skeleton Example
 *
 * The following example demonstrates blending between full-skeleton
 * animations.
 *
 * The main classes used to achieve this are:
 *  - CIwAnim
 *  - CIwAnimPlayer
 *  - CIwAnimBlendSource
 *
 * The main functions used to achieve this are:
 *  - CIwResGroup::GetResNamed()
 *  - CIwResManager::GetGroupNamed()
 *  - CIwAnimPlayer::SetSkel()
 *  - CIwAnimPlayer::PlayAnim()
 *
 * This example builds on Animation Playback Example by adding further
 * controls to allow you to swap between the loaded animations. During
 * the initalisation phase of our example all resources are loaded
 * up using GetGroupNamed(), all three animations are accessed using
 * the GetResNamed() function and playback of the first animation is
 * started using the PlayAnim() function.
 *
 * If the 1, 2 or 3 keys are pressed then the corresponding animations
 * are played. When a new animation is introduced it is blended in
 * on top of the old animation. The blending is done over a set period,
 * and this can be set using the minus and plus keys to increase or
 * decrease the amount of time. The blend time is added to the PlayAnim()
 * function as a fourth parameter.
 *
 * @note For more information on blending between animations, see
 * the Smooth Transitions section of the <i>IwAnim API Documentation</i>.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwAnimSkeletonImage.png
 *
 * @include IwAnimSkeleton.cpp
 */

#include "IwAnim.h"
#include "IwAnimPlayer.h"
#include "IwAnimSkel.h"
#include "IwAnimSkin.h"
#include "IwGraphics.h"
#include "IwGxPrint.h"
#include "IwModelBuilder.h"
#include "IwResManager.h"
#include "IwTextParserITX.h"
#include "s3eKeyboard.h"
#include "ExamplesMain.h"

// Angles
CIwFVec3    s_Angles;

// Local matrix
CIwFMat      s_ModelMatrix;

// Resources
CIwModel*       s_Model;
CIwAnim*        s_Anims[3];
CIwAnimSkel*    s_Skel;
CIwAnimSkin*    s_Skin;

// Animation player
CIwAnimPlayer*  s_Player;

// Blend duration
float   s_BlendDur = 0.25f;

void CreateButtonsUI(int w, int h)
{
    // Create the UI layout
    AddButton("Zoom in: 1", 0, 70, 70, 30, s3eKey1);
    AddButton("Zoom out: 2", 0, 110, 70, 30, s3eKey2);

    AddButton("Duration -: 4", w - 70, 70, 70, 30, s3eKey4);
    AddButton("Duration +: 5", w - 70, 110, 70, 30, s3eKey5);

    AddButton("Sequence 1: 7", 0, h - 90, w/3, 30, s3eKey7);
    AddButton("Sequence 2: 8", w/3, h - 90, w/3, 30, s3eKey8);
    AddButton("Sequence 3: 9", w/3*2, h - 90, w/3, 30, s3eKey9);
}

void SurfaceChangedCallback()
{
    DeleteButtons();
    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    IwGxRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
    IwGxInit();
    IwGraphicsInit();
    IwAnimInit();

    // Set screen clear colour
    IwGxSetColClear(0x40, 0x40, 0x40, 0x00);

    // Set field of view
    IwGxSetPerspMul(IwGxGetScreenWidth() / 2.0f);

    // Set near and far planes
    IwGxSetFarZNearZ(0x4010, 0x10);

    // Initialise angles
    s_Angles = CIwFVec3(0, 3.0f * PI /2.0f, PI);

    // Initialise model position
    s_ModelMatrix.t = CIwFVec3(0, 0x200, 0x300);

#ifdef IW_BUILD_RESOURCES
    //Initialise resource templates - This scales Eva up so animation can interpolate smoothly
    if (IwGetResManager()->m_Flags & CIwResManager::BUILDRES_F )
        IwGetTextParserITX()->ParseFile("resource_template.itx");
#endif

    // Parse the GROUP file, which will load the resources
    IwGetResManager()->LoadGroup("IwAnimSkeleton.group");

    // Get a pointer to the named group
    CIwResGroup* pGroup = IwGetResManager()->GetGroupNamed("ExampleIwAnimSkeleton");

    // Get and store pointer to the resources
    s_Model = (CIwModel*)pGroup->GetResNamed("combatEva", IW_GRAPHICS_RESTYPE_MODEL);
    s_Skin  = (CIwAnimSkin*)pGroup->GetResNamed("combatEva", IW_ANIM_RESTYPE_SKIN);
    s_Skel  = (CIwAnimSkel*)pGroup->GetResNamed("combatEva_rig", IW_ANIM_RESTYPE_SKELETON);

    s_Anims[0]  = (CIwAnim*)pGroup->GetResNamed("LfStance", IW_ANIM_RESTYPE_ANIMATION);
    s_Anims[1]  = (CIwAnim*)pGroup->GetResNamed("RtStance", IW_ANIM_RESTYPE_ANIMATION);
    s_Anims[2]  = (CIwAnim*)pGroup->GetResNamed("LfDwDodge", IW_ANIM_RESTYPE_ANIMATION);

    // Create animation player
    s_Player = new CIwAnimPlayer;
    s_Player->SetSkel(s_Skel);
    s_Player->PlayAnim(s_Anims[0], 1, CIwAnimBlendSource::LOOPING_F, s_BlendDur);

    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());

}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // Clear up resources
    delete s_Player;

    // Terminate IwAnim
    IwAnimTerminate();
    IwGraphicsTerminate();
    IwGxTerminate();
    IwGxUnRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // Update angles from the arrow keys
    if (CheckCursorState() == EXCURSOR_LEFT )
        s_Angles.y -= PI / 32.0f;
    if (CheckCursorState() == EXCURSOR_RIGHT )
        s_Angles.y += PI / 32.0f;
    if (CheckCursorState() == EXCURSOR_UP )
        s_Angles.x -= PI / 32.0f;
    if (CheckCursorState() == EXCURSOR_DOWN )
        s_Angles.x += PI / 32.0f;

    // Build view matrix rotation from angles
    CIwFMat rotX, rotY, rotZ;
    rotX.SetRotX(s_Angles.x);
    rotY.SetRotY(s_Angles.y);
    rotZ.SetRotZ(PI);
    s_ModelMatrix.CopyRot(rotZ * rotX * rotY);

    // Forward/back
    CIwFVec3 ofs(0, 0, 16);
    if (CheckButton("Zoom out: 2") & S3E_KEY_STATE_DOWN )
        s_ModelMatrix.t += ofs;
    if (CheckButton("Zoom in: 1") & S3E_KEY_STATE_DOWN )
        s_ModelMatrix.t -= ofs;

    // Update blend duration
    if (CheckButton("Duration -: 4") & S3E_KEY_STATE_DOWN )
        s_BlendDur = MAX(0x0, s_BlendDur - 1.0f / 8.0f);
    else
    if (CheckButton("Duration +: 5") & S3E_KEY_STATE_DOWN )
        s_BlendDur = MIN(1, s_BlendDur + 1.0f / 8.0f);

    // Change anim
    CIwAnim* pAnim = NULL;
    if (CheckButton("Sequence 1: 7") & S3E_KEY_STATE_PRESSED )
        pAnim = s_Anims[0];
    else
    if (CheckButton("Sequence 2: 8") & S3E_KEY_STATE_PRESSED )
        pAnim = s_Anims[1];
    else
    if (CheckButton("Sequence 3: 9") & S3E_KEY_STATE_PRESSED )
        pAnim = s_Anims[2];

    if (pAnim)
        s_Player->PlayAnim(pAnim, 1, CIwAnimBlendSource::LOOPING_F, s_BlendDur);

    // Update animation player
    s_Player->Update(1.0f / 30.0f);

    // Update IwGx state time stamp
    IwGxTickUpdate();
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Start with no lighting
    IwGxLightingOff();

    //-------------------------------------------------------------------------
    // Render the model
    //-------------------------------------------------------------------------
    IwGxSetModelMatrix(&s_ModelMatrix);

    IwAnimSetSkelContext(s_Player->GetSkel());
    IwAnimSetSkinContext(s_Skin);
    s_Model->Render();

    // Tidier to reset these
    IwAnimSetSkelContext(NULL);
    IwAnimSetSkinContext(NULL);

    // Debug display
    char line[80];
    sprintf(line, "Animation: %s", s_Player->GetSourceCurr()->GetAnim()->DebugGetName());
    IwGxPrintString(2, 42, line);
    sprintf(line, "Blend Dur: %f", s_BlendDur);
    IwGxPrintString(2, 50, line);

    // Paint the cursor keys buttons
    IwGxPrintString(2, IwGxGetScreenHeight() - 60, "Rotate Model: Arrow Keys");
    RenderCursorskeys();

    // Flush and swap
    IwGxFlush();
    IwGxSwapBuffers();
}
