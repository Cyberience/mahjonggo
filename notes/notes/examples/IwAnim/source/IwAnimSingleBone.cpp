/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwAnimSingleBone IwAnim Single-Boned Animation Playback Example
 *
 * The following example demonstrates how to play single-boned animations.
 *
 * The main classes used to achieve this are:
 *  - CIwAnimPlayer
 *
 * The main functions used to achieve this are:
 *  - CIwAnimPlayer::PlayAnim()
 *  - CIwAnimBone::SetModelMatrix();
 *
 * This example builds on Animation Playback Example by substituting a
 * single-boned animation for the multi bone skinned animation.
 *
 * The position of the single bone is applied to a ball to make it bounce down
 * some stairs.
 *
 * @note For more information on adjusting the playing of an animation,
 * see the Animation Player section of the IwAnim API Documentation.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwAnimSingleBoneImage.png
 *
 * @include IwAnimSingleBone.cpp
 */

#include "IwAnim.h"
#include "IwAnimPlayer.h"
#include "IwAnimSkel.h"
#include "IwAnimSkin.h"
#include "IwGraphics.h"
#include "IwGxPrint.h"
#include "IwModelBuilder.h"
#include "IwResManager.h"
#include "IwTextParserITX.h"
#include "s3eKeyboard.h"
#include "ExamplesMain.h"

// Angles
CIwFVec3    s_Angles;

// Local matrix
CIwFMat      s_ModelMatrix;

// Resources
CIwModel*       s_StaticModel;
CIwModel*       s_AnimModel;
CIwAnim*        s_Anim;

// Animation player
CIwAnimPlayer*  s_Player;

float   s_Speed     = 1;
bool    s_Reverse   = false;
bool    s_Looping   = true;

void CreateButtonsUI(int w, int h)
{
    // Create the UI layout
    AddButton("Zoom in: 1", 0, 70, 70, 30, s3eKey1);
    AddButton("Zoom out: 2", 0, 110, 70, 30, s3eKey2);

    AddButton("Speed -: 4", w - 70, 70, 70, 30, s3eKey4);
    AddButton("Speed +: 5", w - 70, 110, 70, 30, s3eKey5);

    AddButton("Reverse: 3", 0, 150, 70, 30, s3eKey3);

    AddButton("Looping: 6",  w - 70, 150, 70, 30, s3eKey6);
}

void SurfaceChangedCallback()
{
    DeleteButtons();
    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    IwGxInit();
    IwGraphicsInit();
    IwAnimInit();

    IwGxRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);


    // Set screen clear colour
    IwGxSetColClear(0x40, 0x40, 0x40, 0x00);

    // Set field of view
    IwGxSetPerspMul(IwGxGetScreenWidth() / 2.0f);

    // Set near and far planes
    IwGxSetFarZNearZ(0x4010, 0x10);

    // Initialise angles
    s_Angles = CIwFVec3(0, 3.0f * PI / 2.0f, PI);

    // Initialise model position
    s_ModelMatrix.t = CIwFVec3(0, 0x400, 0x1480);

#ifdef IW_BUILD_RESOURCES
    //Initialise resource templates - This scales Eva up so animation can interpolate smoothly
    if (IwGetResManager()->m_Flags & CIwResManager::BUILDRES_F )
        IwGetTextParserITX()->ParseFile("resource_template.itx");
#endif

    // Parse the GROUP file, which will load the resources
    IwGetResManager()->LoadGroup("IwAnimSingleBone.group");

    // Get a pointer to the named group
    CIwResGroup* pGroup = IwGetResManager()->GetGroupNamed("IwAnimSingleBone");

    // Get and store pointer to the resources
    s_StaticModel = (CIwModel*)pGroup->GetResNamed("tower", IW_GRAPHICS_RESTYPE_MODEL);
    s_AnimModel = (CIwModel*)pGroup->GetResNamed("ball_01", IW_GRAPHICS_RESTYPE_MODEL);
    s_Anim  = (CIwAnim*)pGroup->GetResNamed("ball_01", IW_ANIM_RESTYPE_ANIMATION);

    // Create animation player
    s_Player = new CIwAnimPlayer;

    // Play the animation... note that no skeleton needs to be set.
    s_Player->PlayAnim(s_Anim, 1, CIwAnimBlendSource::LOOPING_F);

    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // Clear up resources
    delete s_Player;

    IwGxUnRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);

    // Terminate IwAnim
    IwAnimTerminate();
    IwGraphicsTerminate();
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // Update angles from the arrow keys
    if (CheckCursorState() == EXCURSOR_LEFT )
        s_Angles.y -= PI / 32.0f;
    if (CheckCursorState() == EXCURSOR_RIGHT )
        s_Angles.y += PI / 32.0f;
    if (CheckCursorState() == EXCURSOR_UP )
        s_Angles.x -= PI / 32.0f;
    if (CheckCursorState() == EXCURSOR_DOWN )
        s_Angles.x += PI / 32.0f;

    // Build view matrix rotation from angles
    CIwFMat rotX, rotY, rotZ;
    rotX.SetRotX(s_Angles.x);
    rotY.SetRotY(s_Angles.y);
    rotZ.SetRotZ(PI);
    s_ModelMatrix.CopyRot(rotZ * rotX * rotY);

    // Forward/back
    CIwFVec3 ofs(0, 0, 16);
    if (CheckButton("Zoom out: 2") & S3E_KEY_STATE_DOWN )
        s_ModelMatrix.t += ofs;
    if (CheckButton("Zoom in: 1") & S3E_KEY_STATE_DOWN )
        s_ModelMatrix.t -= ofs;

    // Update speed
    bool replay = false;

    if (CheckButton("Speed -: 4") & S3E_KEY_STATE_DOWN )
    {
        s_Speed = MAX(0.0625f, s_Speed / 2.0f);
        replay = true;
    }
    else
    if (CheckButton("Speed +: 5") & S3E_KEY_STATE_DOWN )
    {
        s_Speed = MIN(4, s_Speed * 2.0f);
        replay = true;
    }

    // Toggle backwards status
    if (CheckButton("Reverse: 3") & S3E_KEY_STATE_PRESSED )
    {
        s_Reverse = !s_Reverse;
        replay = true;
    }

    // Toggle looping status
    if (CheckButton("Looping: 6") & S3E_KEY_STATE_PRESSED )
    {
        s_Looping = !s_Looping;
        replay = true;
    }

    if (replay)
        s_Player->PlayAnim(s_Anim, s_Speed * ((s_Reverse == false) ? 1 : -1), (s_Looping == true ? CIwAnimBlendSource::LOOPING_F : 0) | CIwAnimBlendSource::RESET_IF_SAME_F);

    // Update animation player
    s_Player->Update(1.0f / 30.0f);

    // Update IwGx state time stamp
    IwGxTickUpdate();
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Start with no lighting
    IwGxLightingOff();


    //-------------------------------------------------------------------------
    // Render the static model
    //-------------------------------------------------------------------------
    IwGxSetModelMatrix(&s_ModelMatrix);

    // Render the stairs
    s_StaticModel->Render();

    //-------------------------------------------------------------------------
    // Render the animated model
    //-------------------------------------------------------------------------

    // Get the single animated bone from the skeleton and place model space at its position.
    s_Player->GetSkel()->GetBoneFromID(0)->SetModelMatrix(s_ModelMatrix);

    // Now render the ball at that position
    s_AnimModel->Render();


    // Debug display
    char line[80];
    sprintf(line, "Speed: %f", s_Speed);
    IwGxPrintString(2, 32, line);
    sprintf(line, "Reverse: %s", s_Reverse ? "true" : "false");
    IwGxPrintString(2, 40, line);
    sprintf(line, "Looping: %s", s_Looping ? "true" : "false");
    IwGxPrintString(2, 48, line);

    IwGxPrintString(2, IwGxGetScreenHeight() - 60, "Rotate Model: Arrow Keys");
    RenderCursorskeys();

    // Flush and swap
    IwGxFlush();
    IwGxSwapBuffers();
}
