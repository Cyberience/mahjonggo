// Source maya file: C:/Dev/dev/main/gamedemo1/untitled

CAnimStateMachine
{
	name				combatEva	

	//----------------------------------------------------------------------------
	// begin states
	//----------------------------------------------------------------------------

	CAnimStateLoop
	{
		name				LfStance
		anim				LfStance


		transition			{ LfStance_LfRvPnch , hands }
		transition			{ LfStance_RtSnpKikMd , legs }
		transition			{ LfBlock , block }
		transition			{ LfStance_LfDodge , moving , 3 }
		transition			{ LfStance_RtDodge , moving , 4 }
		transition			{ LfStance_FwStep , moving }
		transition			{ LfStance_BwStep , moving , 2 }
	}

	CAnimStateLoop
	{
		name				LfRvPnch
		anim				LfRvPnch


		transition			{ LfRvPnch_LfStance , idle }
		transition			{ LfRvPnch_LfHook , hands }
	}

	CAnimStateLoop
	{
		name				LfHook
		anim				LfHook


		transition			{ LfHook_LfStance , idle }
		transition			{ LfHook_LfRndHseMd , legs }
	}

	CAnimStateLoop
	{
		name				RtSnpKikHi
		anim				RtSnpKikHi


		transition			{ RtSnpKikHi_RtSnpKikMd , legs }
		transition			{ RtSnpKikHi_LfStance , idle }
	}

	CAnimStateLoop
	{
		name				RtSnpKikMd
		anim				RtSnpKikMd


		transition			{ RtSnpKikMd_RtSnpKikHi , legs }
		transition			{ RtSnpKikMd_LfStance , idle }
	}

	CAnimStateLoop
	{
		name				RtStance
		anim				RtStance


		transition			{ RtStance_RtSnpKikHi , legs }
		transition			{ RtStance_RtJab , hands }
		transition			{ RtBlock , block }
		transition			{ RtStance_LfDodge , moving , 3 }
		transition			{ RtStance_RtDodge , moving , 4 }
		transition			{ RtStance_FwStep , moving }
		transition			{ RtStance_BwStep , moving , 2 }
	}

	CAnimStateLoop
	{
		name				LfRndHseMd
		anim				LfRndHseMd


		transition			{ LfRndHseMd_RtStance , Always }
	}

	CAnimStateLoop
	{
		name				LfBlock
		anim				LfBlock


		transition			{ LfStance , block , 0 }
	}

	CAnimStateLoop
	{
		name				RtBlock
		anim				RtBlock
		

		transition			{ RtStance , block , 0 }
	}

	CAnimStateTransition
	{
		name				LfStance_LfRvPnch
		anim				LfStance_LfRvPnch

		signal				{ hit , 12 , 0.01 }
		signal				{ hit , 0 , -0.01 }
		signal				{ punch , 1 , 0.01 }

		transition			{ LfRvPnch , Always }
	}

	CAnimStateTransition
	{
		name				LfRvPnch_LfStance
		anim				LfRvPnch_LfStance

		transition			{ LfStance , Always }
	}

	CAnimStateTransition
	{
		name				LfRvPnch_LfHook
		anim				LfRvPnch_LfHook

		signal				{ hit , 12 , 0.01 }
		signal				{ hit , 0 , -0.01 }
		signal				{ punch , 1 , 0.01 }

		transition			{ LfHook , Always }
	}

	CAnimStateTransition
	{
		name				LfHook_LfStance
		anim				LfHook_LfStance

		transition			{ LfStance , Always }
	}

	CAnimStateTransition
	{
		name				LfHook_LfRndHseMd
		anim				LfHook_LfRndHseMd
		
		signal				{ hit , 4 , 0.01 }
		signal				{ hit , 0 , -0.01 }
		signal				{ kick , 1 , 0.01 }

		transition			{ LfRndHseMd , Always }
	}

	CAnimStateTransition
	{
		name				LfRndHseMd_RtStance
		anim				LfRndHseMd_RtStance

		transition			{ RtStance , Always }
	}

	CAnimStateTransition
	{
		name				RtStance_RtSnpKikHi
		anim				RtStance_RtSnpKikHi

		signal				{ hit , 4 , 0.01 }
		signal				{ hit , 0 , -0.01 }
		signal				{ kick , 1 , 0.01 }

		transition			{ RtSnpKikHi , Always }
	}

	CAnimStateTransition
	{
		name				RtSnpKikHi_RtSnpKikMd
		anim				RtSnpKikHi_RtSnpKikMd
		
		signal				{ hit , 4 , 0.01 }
		signal				{ hit , 0 , -0.01 }
		signal				{ kick , 1 , 0.01 }

		transition			{ RtSnpKikMd , Always }
	}

	CAnimStateTransition
	{
		name				RtSnpKikMd_RtSnpKikHi
		anim				RtSnpKikMd_RtSnpKikHi
		
		signal				{ hit , 4 , 0.01 }
		signal				{ hit , 0 , -0.01 }
		signal				{ kick , 1 , 0.01 }

		transition			{ RtSnpKikHi , Always }
	}

	CAnimStateTransition
	{
		name				RtSnpKikMd_LfStance
		anim				RtSnpKikMd_LfStance

		transition			{ LfStance , Always }
	}

	CAnimStateTransition
	{
		name				LfStance_RtSnpKikMd
		anim				LfStance_RtSnpKikMd
		
		signal				{ hit , 4 , 0.01 }
		signal				{ hit , 0 , -0.01 }
		signal				{ kick , 1 , 0.01 }

		transition			{ RtSnpKikMd , Always }
	}

	CAnimStateTransition
	{
		name				RtSnpKikHi_LfStance
		anim				RtSnpKikHi_LfStance

		transition			{ LfStance , Always }
	}

	CAnimStateTransition
	{
		name				RtStance_RtJab
		anim				RtStance_RtJab
		
		signal				{ hit , 3 , 0.01 }
		signal				{ hit , 0 , -0.01 }
		signal				{ punch , 1 , 0.01 }

		transition			{ RtStance , Always }
	}

	CAnimStateLoop
	{
		name				LfStance_LfDodge
		anim				LfUpDodge

		transition			{ LfStance , moving , 0 }
	}

	CAnimStateLoop
	{
		name				LfStance_RtDodge
		anim				LfDwDodge

		transition			{ LfStance , moving , 0 }
	}

	CAnimStateLoop
	{
		name				LfStance_FwStep
		anim				LfFwShuffle

		transition			{ LfStance , moving , 0 }
	}

	CAnimStateLoop
	{
		name				LfStance_BwStep
		anim				LfBkShuffle

		transition			{ LfStance , moving , 0 }
		transition			{ LfBlock , block }
	}

	CAnimStateLoop
	{
		name				RtStance_LfDodge
		anim				RtUpDodge

		transition			{ RtStance , moving , 0 }
	}

	CAnimStateLoop
	{
		name				RtStance_RtDodge
		anim				RtDwDodge

		transition			{ RtStance , moving , 0 }
	}

	CAnimStateLoop
	{
		name				RtStance_FwStep
		anim				RtFwShuffle

		transition			{ RtStance , moving , 0 }
	}

	CAnimStateLoop
	{
		name				RtStance_BwStep
		anim				RtBkShuffle

		transition			{ RtStance , moving , 0 }
		transition			{ RtBlock , block }
	}

	CAnimStateTransition
	{
		name				Damage
		anim				NtStance_HedDmg

		signal				{ damage_f , 1 , 0.01 }

		transition			{ LfStance , always }
	}

	CAnimStateTransition
	{
		name				Fall
		anim				NtStance_DthEdge

		signal				{ die_f, 1, 0.1 }

		transition			{ Dead , always }
	}

	CAnimStateTransition
	{
		name				Die
		anim				NtStance_Dth

		transition			{ Dead , always }
	}

	CAnimStateLoop
	{
		name				Dead

		transition			{ LfStance , idle }
	}

	//----------------------------------------------------------------------------
	// end states
	//----------------------------------------------------------------------------
}
