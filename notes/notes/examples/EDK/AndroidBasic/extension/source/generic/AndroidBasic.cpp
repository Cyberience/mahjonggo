/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/*
Generic implementation of the AndroidBasic extension.
This file should perform any platform-indepedentent functionality
(e.g. error checking) before calling platform-dependent implementations.
*/

/*
* NOTE: This file was originally written by the extension builder, but will not
* be overwritten (unless --force is specified) and is intended to be modified.
*/

/**
 * @page EDKExtensionAndroidBasicSource Android Basic Extension Source Code and Project Files
 *
 * AndroidBasic.mkf (includes this extension as a subproject):
 * @include AndroidBasic.mkf
 *
 * Internal header (source/h/AndroidBasic_internal.h):
 * @include ../h/AndroidBasic_internal.h
 *
 * CPP source version 1 (see EDK tutorial) (source/generic/AndroidBasicApp.cpp):
 * @include AndroidBasic.cpp
 *
 * CPP source version 2 (see EDK tutorial) (source/generic/AndroidBasicApp.cpp):
 * @include AndroidBasic.cpp
 *
 * Android java source (source/android/AndroidBasic.java):
 * @include ../android/AndroidBasic.java
 */

#include "AndroidBasic_internal.h"
s3eResult AndroidBasicInit()
{
    //Add any generic initialisation code here
    return AndroidBasicInit_platform();
}

void AndroidBasicTerminate()
{
    //Add any generic termination code here
    AndroidBasicTerminate_platform();
}

int AndroidBasicAdd(int a, int b)
{
    return AndroidBasicAdd_platform(a, b);
}
