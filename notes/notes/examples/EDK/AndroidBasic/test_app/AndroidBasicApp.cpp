/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page EDKExtensionAndroidBasicTest Android Basic Test Application
 *
 * The test application demonstrates how to use a simple Android extension.
 *
 * It uses the following functions:
 * - AndroidBasicAdd()
 *
 * MKB:
 * @include AndroidBasicApp.mkb
 *
 * Source:
 * @include AndroidBasicApp.cpp
 */

#include "AndroidBasic.h"
#include "s3eDevice.h"
#include "s3ePointer.h"
#include "s3eSurface.h"
#include "s3eTimer.h"
#include "IwDebug.h"

S3E_MAIN_DECL int main()
{
    while (!s3eDeviceCheckQuitRequest())
    {

        s3eDeviceYield(0);

        if (AndroidBasicAdd(1, 2))
        {
            s3eDebugPrint(0,10,"The AndroidBasic extension is working", 0);
        }

        s3eSurfaceShow();
    }
    return 0;
}
