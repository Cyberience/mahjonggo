/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page EDKExtensionMessageBoxTest Message Box Test Application
 *
 * The test application demonstrates how to include and use the Message Box EDK
 * extension example. It displays a native message box when the users touches
 * the screen.
 *
 * It uses the following functions:
 * - MessageBox()
 *
 * MKB:
 * @include MessageBoxApp.mkb
 *
 * Source:
 * @include MessageBoxApp.cpp
 */

#include "MessageBox.h"
#include "s3eDevice.h"
#include "s3ePointer.h"
#include "s3eSurface.h"
#include "s3eTimer.h"
#include "IwDebug.h"


void TouchButtonCB(s3ePointerEvent* event);
void MultiTouchButtonCB(s3ePointerTouchEvent* event);

S3E_MAIN_DECL int main()
{
    // Set touch event callback handlers, single and multi-touch devices have different callbacks assigned
    bool bTouchAvailable = s3ePointerGetInt(S3E_POINTER_MULTI_TOUCH_AVAILABLE) != 0;

    if (bTouchAvailable)
    {
        s3ePointerRegister(S3E_POINTER_TOUCH_EVENT, (s3eCallback)MultiTouchButtonCB, 0);
    }
    else
    {
        s3ePointerRegister(S3E_POINTER_BUTTON_EVENT, (s3eCallback)TouchButtonCB, 0);
    }

    while (!s3eDeviceCheckQuitRequest())
    {
        s3eDeviceYield(0);
        if (MessageBoxAvailable())
            s3eDebugPrint(0, 30, "Message Box test application", 0);
        else
            s3eDebugPrint(0, 30, "Message Box extension unavailable", 0);
        s3eSurfaceShow();
    }

    if (bTouchAvailable)
    {
        s3ePointerUnRegister(S3E_POINTER_TOUCH_EVENT, (s3eCallback)MultiTouchButtonCB);
    }
    else
    {
        s3ePointerUnRegister(S3E_POINTER_BUTTON_EVENT, (s3eCallback)TouchButtonCB);
    }

    return 0;
}

void ShowMessageBox()
{
    static bool s_MessageState;

    if (s_MessageState)
    {
        MessageBox("Test Message1", "This is a message1!");
    }
    else
    {
        MessageBox("Test Message2", "This is a message2!");
    }

    s_MessageState = !s_MessageState;
}

void TouchButtonCB(s3ePointerEvent* event)
{
    if (event->m_Pressed)
    {
        ShowMessageBox();
    }
}

void MultiTouchButtonCB(s3ePointerTouchEvent* event)
{
    if (event->m_Pressed)
    {
        ShowMessageBox();
    }
}
