/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
/*
C# implementation of the MessageBox extension.

Add wp8-specific functionality here.

These functions are called via Shim class from native code.
*/
/*
 * NOTE: This file was originally written by the extension builder, but will not
 * be overwritten (unless --force is specified) and is intended to be modified.
 */
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using System.Windows;
using s3e_native;
using Windows.UI.Popups;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.Foundation;

namespace MessageBoxManaged
{
    public sealed class MessageBoxManaged : IManagedS3EEDKAPI
    {
        public MessageBoxManaged(IMessageBoxNative native, IMessageBoxShim shim)
        {
            m_Shim = shim;
            m_Native = native;
        }

        public bool ExtRegister(IS3EAPIManager apiManager, object mainPage)
        {
            try
            {
                // Keep a reference to the API Manager in order to call other
                // APIs.
                m_APIManager = apiManager;
                m_MainPage = mainPage as Page;
                m_MainPanel = apiManager.GetRootPanel() as Panel;

                // Add the managed API to the API Manager
                if (!m_APIManager.RegisterManagedAPI("MessageBox", this))
                    throw new System.Exception("Can't register Managed API");

                // Add the native API to the API manager.  Note that we don't own the native
                // interface, the API Manager does.  We want the two notions of managed and
                // native interface to be separate as there may be cases where we only want
                // one not the other.  It's only a matter of convenience that we create both
                // APIs in this ctr
                if (!m_APIManager.RegisterNativeAPI("MessageBox", m_Native))
                    throw new System.Exception("Can't register Native API");

                // Pass the managed interface down
                m_Shim.Init(this);
            }
            catch (System.Exception e)
            {
                m_APIManager = null;
                m_Shim = null;
                MessageBox_managed("MessageBox extention registration error", "Failed to register MessageBox : " + e.Message);
                return false;
            }
            m_Native.Complete();
            return true;
        }
        IS3EAPIManager m_APIManager = null;
        Page m_MainPage = null;
        // hint: add UI Elements as children of m_MainPanel
        Panel m_MainPanel = null;
        IMessageBoxShim m_Shim = null;
        IMessageBoxNative m_Native = null;
        MessageDialog m_Dialog = null;
        IAsyncOperation<IUICommand> m_AsyncOperation = null;

        public bool MessageBox_managed(string title, string text)
        {
            if (m_AsyncOperation != null &&
                m_AsyncOperation.Status == Windows.Foundation.AsyncStatus.Started
                )
            {
                m_AsyncOperation.Cancel();
                m_AsyncOperation = null;
            }

            if (m_Dialog == null)
            {
                m_Dialog = new Windows.UI.Popups.MessageDialog(title, text);
            }
            else
            {
                m_Dialog.Title = title;
                m_Dialog.Content = text;
            }
            m_AsyncOperation = m_Dialog.ShowAsync();
            m_AsyncOperation.Completed = new AsyncOperationCompletedHandler<IUICommand>(this.CommandInvokedHandler);
            return true;
        }

        private void CommandInvokedHandler(IAsyncOperation<IUICommand> cmd, AsyncStatus status)
        {
            m_Native.Complete();
            m_AsyncOperation = null;
        }
    }
}
