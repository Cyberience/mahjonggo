/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
/*
 * wp8-specific implementation of the MessageBox extension.
 * Add any platform-specific functionality here.
 */
/*
 * NOTE: This file was originally written by the extension builder, but will not
 * be overwritten (unless --force is specified) and is intended to be modified.
 */
#include <crtdbg.h>

#define _CRT_PERROR_DEFINED
#include <wrl/client.h>
#include <wrl/module.h>
#undef _CRT_PERROR_DEFINED

#include "MessageBox_internal.h"

#include "MessageBox_shim.h"
#include "IwDebug.h"

using namespace MessageBoxExtension;

void MessageBoxManagedRegister_platform()
{
    MessageBoxShim^ shim = ref new MessageBoxShim();
    MessageBoxNative^ native = ref new MessageBoxNative();
    s3e_native::s3eExtensionsShim::GetInterface()->Register(ref new MessageBoxManaged::MessageBoxManaged(native, shim));
};

s3eResult MessageBoxInit_platform()
{
    MessageBoxManagedRegister_platform();

    if (MessageBoxShim::GetInterface() == nullptr)
    {
        IwTrace(MESSAGEBOX, ("No IManagedS3EMessageBoxAPI interface. Managed part of MessageBox extension failed to register."));
        return S3E_RESULT_ERROR;
    }

    // Add any platform-specific initialisation code here
    return S3E_RESULT_SUCCESS;
}

void MessageBoxTerminate_platform()
{
    // Add any platform-specific termination code here
}

s3eResult MessageBox_platform(const char* title, const char* text)
{
    Platform::String^ title_string = UTF8ToString(title);
    Platform::String^ text_string = UTF8ToString(text);
    if (!MessageBoxShim::GetInterface()->MessageBox_managed(title_string, text_string))
        return S3E_RESULT_ERROR;

    return S3E_RESULT_SUCCESS;
}

void MessageBoxNative::Complete()
{
    if (MESSAGEBOXGLOBALS->m_MessageBoxRunning)
    {
        MessageBoxComplete();
    }
    else
    {
        s3e_native::s3eExtensionsShim::GetInterface()->RegistrationDone();
    }
}
