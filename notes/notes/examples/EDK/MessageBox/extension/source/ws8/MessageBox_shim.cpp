/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
/*
C++ Shim implementation of the MessageBox extension's.

Not intended for modifications

These functions are called by C# to access native interface and from C++ to
access managed interface.
*/
/*
 * NOTE: This file was originally written by the extension builder, but will not
 * be overwritten (unless --force is specified) and is intended to be modified.
 */
#include "MessageBox_shim.h"

using namespace MessageBoxExtension;


MessageBoxManaged::MessageBoxManaged^ MessageBoxShim::s_ManagedInterface = nullptr;

MessageBoxShim::MessageBoxShim()
{
}

MessageBoxShim::~MessageBoxShim()
{
    Terminate();
}

bool MessageBoxShim::Init(MessageBoxManaged::MessageBoxManaged^ managedAPI)
{
    // It's an error to do this twice.
    if (s_ManagedInterface)
        return false;

    s_ManagedInterface = managedAPI;
    return true;
}

bool MessageBoxShim::Terminate()
{
    // It's an error to do this twice.
    if (!s_ManagedInterface)
        return false;

    s_ManagedInterface = nullptr;
    return true;
}

MessageBoxManaged::MessageBoxManaged^ MessageBoxShim::GetInterface()
{
    return s_ManagedInterface;
}
