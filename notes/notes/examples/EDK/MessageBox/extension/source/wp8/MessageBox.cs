/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
/*
C# implementation of the MessageBox extension.

Add wp8-specific functionality here.

These functions are called via Shim class from native code.
*/
/*
 * NOTE: This file was originally written by the extension builder, but will not
 * be overwritten (unless --force is specified) and is intended to be modified.
 */
using System.Windows.Controls;
using Microsoft.Phone.Controls;
using System.Windows.Media;
using System.Windows;

using s3e_native;
using MessageBoxExtension;

namespace MessageBoxManaged
{
    public class MessageBoxManaged: IManagedS3EMessageBoxAPI, IManagedS3EEDKAPI
    {

        public bool ExtRegister(IS3EAPIManager apiManager, object mainPage)
        {
            try
            {
                // Keep a reference to the API Manager in order to call other
                // APIs.
                m_APIManager = apiManager;
                m_MainPage = mainPage as PhoneApplicationPage;
                m_MainPanel = apiManager.GetRootPanel() as Panel;

                // Add the managed API to the API Manager
                if (!m_APIManager.RegisterManagedAPI("MessageBox", this))
                    throw new System.Exception("Can't register Managed API");

                // Add the native API to the API manager.  Note that we don't own the native
                // interface, the API Manager does.  We want the two notions of managed and
                // native interface to be separate as there may be cases where we only want
                // one not the other.  It's only a matter of convenience that we create both
                // APIs in this ctr
                m_Native = new MessageBoxNativeInterface();
                if (!m_APIManager.RegisterNativeAPI("MessageBox", m_Native))
                    throw new System.Exception("Can't register Native API");

                // Create a Shim so we can pass the Managed interface down to native
                m_Shim = new MessageBoxShim();

                // Pass the managed interface down
                m_Shim.Init(this);
            }
            catch (System.Exception e)
            {
                m_APIManager = null;
                m_Shim = null;
                m_Native = null;
                System.Windows.MessageBox.Show("Failed to register MessageBox : " + e.Message);
                return false;
            }

            return true;
        }
        IS3EAPIManager m_APIManager = null;
        PhoneApplicationPage m_MainPage = null;
        // hint: add UI Elements as children of m_MainPanel
        Panel m_MainPanel = null;
        MessageBoxShim m_Shim = null;
        MessageBoxNativeInterface m_Native = null;

        // managed platform functionality


        public bool MessageBox_managed(string title, string text)
        {
            MessageBox.Show(text, title, MessageBoxButton.OK);
            return true;
        }
    }
}
