/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
/*
C++ Shim implementation of the MessageBox extension's.

Not intended for modifications

These functions are called by C# to access native interface and from C++ to
access managed interface.
*/
/*
 * NOTE: This file was originally written by the extension builder, but will not
 * be overwritten (unless --force is specified) and is intended to be modified.
 */

namespace MessageBoxExtension
{

// This is the interface which native code can use to talk to managed code.
[Windows::Foundation::Metadata::WebHostHidden]
public interface class IManagedS3EMessageBoxAPI : public s3e_native::IManagedS3EAPI
{
    // We shouldn't populate these defacto.  We should only add functions here as and when
    // necessary.
    bool MessageBox_managed(Platform::String^ title, Platform::String^ text);
};

// This is the class which managed code can use to talk to native code.
[Windows::Foundation::Metadata::WebHostHidden]
public ref class MessageBoxNativeInterface sealed : s3e_native::INativeS3EAPI
{
    // We shouldn't populate these defacto.  We should only add functions here as and when
    // necessary.
public:
    MessageBoxNativeInterface() {}
};


[Windows::Foundation::Metadata::WebHostHidden]
public ref class MessageBoxShim sealed
{
public:

    MessageBoxShim();

    // We use two stage construction to avoid the use of exceptions.  We
    // don't want to ever register an API more than once (as it's a static
    // below).  If we did this initialization in the ctr then we'd either
    // have to fail silently or throw an exception.
    bool Init(IManagedS3EMessageBoxAPI^ managedAPI);

    // Unregisters the static interface.
    bool Terminate();

    // Need static getters as we can't have public members in the class.
    static IManagedS3EMessageBoxAPI^ GetInterface();
private:

    ~MessageBoxShim();

    // This is the unavoidable point where we need a static.  There's
    // no choice as we're converting to a C-API.
    static IManagedS3EMessageBoxAPI^ s_ManagedInterface;
};

// char* <-> Platform::String^ wrappers
static inline Platform::String^ UTF8ToString(const char* utf8str)
{
    return s3e_native::s3eAPIDataTypeWrapper::CStringToPlatformString(const_cast<char*>(utf8str));
}

static inline void StringToUTF8(char* utf8str, size_t size, Platform::String^ str)
{
    s3e_native::s3eAPIDataTypeWrapper::PlatformStringToCString(utf8str, size, str);
}

}
