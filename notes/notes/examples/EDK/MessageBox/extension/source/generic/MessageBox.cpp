/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
/*
Generic implementation of the MessageBox extension.
This file should perform any platform-indepedentent functionality
(e.g. error checking) before calling platform-dependent implementations.
*/

/*
 * NOTE: This file was originally written by the extension builder, but will not
 * be overwritten (unless --force is specified) and is intended to be modified.
 */

/**
 * @page EDKExtensionMessageBoxSource Message Box Extension Source Code and Project Files
 *
 * MessageBox.mkf (includes this extension as a subproject):
 * @include MessageBox.mkf
 *
 * Internal header (source/h/MessageBox_internal.h):
 * @include ../h/MessageBox_internal.h
 *
 * Generic source (source/generic/MessageBoxApp.cpp):
 * @include MessageBox.cpp
 *
 * Android source (source/android/MessageBox_platform.cpp):
 * @include ../android/MessageBox_platform.cpp
 *
 * Android java source (source/android/MessageBox.java):
 * @include ../android/MessageBox.java
 *
 * iOS source (source/iphone/MessageBox_platform.mm):
 * @include ../iphone/MessageBox_platform.mm
 *
 * Windows source (source/windows/MessageBox_platform.cpp):
 * @include ../windows/MessageBox_platform.cpp
 */

#include "MessageBox_internal.h"
#include "s3eEdk.h"

s3eResult MessageBoxInit()
{
    //Add any generic initialisation code here
    return MessageBoxInit_platform();
}

void MessageBoxTerminate()
{
    //Add any generic termination code here
    MessageBoxTerminate_platform();
}

void MessageBoxComplete()
{
    MESSAGEBOXGLOBALS->m_MessageBoxRunning = false;
}

s3eResult MessageBox(const char* title, const char* text)
{
     MESSAGEBOXGLOBALS->m_MessageBoxRunning = true;

    s3eResult result = (s3eResult)(intptr_t)s3eEdkThreadRunOnOS(
        (s3eEdkThreadFunc)MessageBox_platform, 2, title, text);

    if (result == S3E_RESULT_SUCCESS)
    {
        while (MESSAGEBOXGLOBALS->m_MessageBoxRunning)
        {
            s3eEdkThreadSleep(20);
        }
    }

    return result;
}
