/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
/*
java implementation of the MessageBox extension.

Add android-specific functionality here.

These functions are called via JNI from native code.
*/
/*
 * NOTE: This file was originally written by the extension builder, but will not
 * be overwritten (unless --force is specified) and is intended to be modified.
 */
import com.ideaworks3d.marmalade.LoaderAPI;
import com.ideaworks3d.marmalade.LoaderActivity;

import android.content.DialogInterface;
import android.app.AlertDialog;
import android.app.Dialog;

class MessageBox implements DialogInterface.OnClickListener, DialogInterface.OnDismissListener
{
    static final String TAG = "MessageBox";
    private String m_MessageText;
    private String m_MessageTitle;
    private boolean m_IgnoreFocusLoss;

    public static native void MessageBoxComplete();

    //This is the function that is called from native code
    public int MessageBox(String title, String text)
    {
        m_MessageText = text;
        m_MessageTitle = title;

        if (LoaderActivity.m_Activity.isFinishing())
        {
            android.util.Log.i(TAG, "activity is finishing... skipping messageBox");
            return 0;
        }

        //Don't want to suspend application due to AlertDialog's Window taking focus
        m_IgnoreFocusLoss = LoaderActivity.m_Activity.getIgnoreFocusLoss();
        LoaderActivity.m_Activity.setIgnoreFocusLoss(true);

        AlertDialog.Builder builder = new
            AlertDialog.Builder(LoaderActivity.m_Activity);
        builder.setTitle(m_MessageTitle);
        builder.setMessage(m_MessageText);
        builder.setPositiveButton("OK", this);
        Dialog dialog = builder.create();
        dialog.setOnDismissListener(this);

        dialog.show();

        return 0;
    }

    public void onClick(DialogInterface dialog, int button)
    {
        dialog.dismiss();
    }

    public void onDismiss(DialogInterface dialog)
    {
        // Restore original focus loss behaviour
        LoaderActivity.m_Activity.setIgnoreFocusLoss(m_IgnoreFocusLoss);

        MessageBoxComplete();
    }
}
