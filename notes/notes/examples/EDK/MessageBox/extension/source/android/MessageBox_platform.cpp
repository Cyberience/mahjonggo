/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
/*
 * Android-specific implementation of the MessageBox extension.
 * Add any platform-specific functionality here.
 *
 * NOTE: This file was originally written by the extension builder, but will not
 * be overwritten (unless --force is specified) and is intended to be modified.
 */

#include "MessageBox_internal.h"
#include "s3eEdk.h"
#include "s3eEdk_android.h"
#include <jni.h>
#include "IwDebug.h"

static jobject g_Obj;
static jmethodID g_MessageBox;

static void JNIMessageBoxComplete(JNIEnv* env, jclass cls)
{
    MessageBoxComplete();
}

s3eResult MessageBoxInit_platform()
{
    //Get the environment from the pointer
    JNIEnv* env = s3eEdkJNIGetEnv();
    jobject obj = NULL;
    jmethodID cons = NULL;

    const JNINativeMethod nativeMethods[] =
    {
         { "MessageBoxComplete", "()V", (void*)&JNIMessageBoxComplete},
    };

    //Get the extension class
    jclass cls = s3eEdkAndroidFindClass("MessageBox");
    if (!cls)
        goto fail;

    //Get its constructor
    cons = env->GetMethodID(cls, "<init>", "()V");
    if (!cons)
        goto fail;

    //Construct the java class
    obj = env->NewObject(cls, cons);
    if (!obj)
        goto fail;

    //Get all the extension methods
    g_MessageBox = env->GetMethodID(cls, "MessageBox", "(Ljava/lang/String;Ljava/lang/String;)I");
    if (!g_MessageBox)
        goto fail;

    //Register native methods
    if (env->RegisterNatives(cls, nativeMethods, sizeof(nativeMethods)/sizeof(nativeMethods[0])))
    {
        IwTrace(MESSAGEBOX, ("Register natives failed"));
        goto fail;
    }

fail:
    jthrowable exc = env->ExceptionOccurred();
    if (exc)
    {
        env->ExceptionDescribe();
        env->ExceptionClear();
        IwTrace(MESSAGEBOX, ("One or more java methods could not be found"));
        return S3E_RESULT_ERROR;
    }

    IwTrace(MESSAGEBOX, ("MessageBox init success"));
    g_Obj = env->NewGlobalRef(obj);
    env->DeleteLocalRef(obj);
    env->DeleteGlobalRef(cls);

    // Add any platform-specific initialisation code here
    return S3E_RESULT_SUCCESS;
}

void MessageBoxTerminate_platform()
{
    // Add any platform-specific termination code here
}

s3eResult MessageBox_platform(const char* title, const char* text)
{
    JNIEnv* env = s3eEdkJNIGetEnv();
    jstring title_jstr = env->NewStringUTF(title);
    jstring text_jstr = env->NewStringUTF(text);
    return (s3eResult)env->CallIntMethod(g_Obj, g_MessageBox, title_jstr, text_jstr);
}
