/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
/*
 * Internal header for the MessageBox extension.
 *
 * This file should be used for any common function definitions etc that need to
 * be shared between the platform-dependent and platform-indepdendent parts of
 * this extension.
 *
 * NOTE: This file was originally written by the extension builder, but will not
 * be overwritten (unless --force is specified) and is intended to be modified.
 */
#ifndef MESSAGEBOX_INTERNAL_H
#define MESSAGEBOX_INTERNAL_H

#include "MessageBox.h"
#include "MessageBox_autodefs.h"

struct MessageBoxGlobals
{
    bool m_MessageBoxRunning;
};

/**
 * Initialise the extension.  This is called once then the extension is first
 * accessed by s3eregister.  If this function returns S3E_RESULT_ERROR the
 * extension will be reported as not-existing on the device.
 */
s3eResult MessageBoxInit();

/**
 * Platform-specific initialisation, implemented on each platform
 */
s3eResult MessageBoxInit_platform();

/**
 * Terminate the extension.  This is called once on shutdown, but only if the
 * extension was loader and Init() was successful.
 */
void MessageBoxTerminate();

/**
 * Platform-specific termination, implemented on each platform
 */
void MessageBoxTerminate_platform();

/**
 * Platform-specific code to display message box.
 */
s3eResult MessageBox_platform(const char* title, const char* text);

/**
 * Called by platform code when dialog has been closed.
 */
void MessageBoxComplete();

#endif /* !MESSAGEBOX_INTERNAL_H */
