/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
#include "MessageBox_internal.h"
#import "Foundation/Foundation.h"
#import "AppKit/AppKit.h"
#include "s3eDevice.h"

@interface MessageBoxDelegate : NSObject{
}
- (void) alertDidEnd:(NSAlert *)alert returnCode:(NSInteger)returnCode contextInfo:(void *)contextInfo;
@end

@implementation MessageBoxDelegate
- (void) alertDidEnd:(NSAlert *)alert returnCode:(NSInteger)returnCode contextInfo:(void *)contextInfo
{
    MessageBoxComplete();
}
@end

static MessageBoxDelegate* g_MessageBoxDelegate = nil;

s3eResult MessageBoxInit_platform()
{
    g_MessageBoxDelegate = [[MessageBoxDelegate alloc] init];
    return S3E_RESULT_SUCCESS;
}

void MessageBoxTerminate_platform()
{
    [g_MessageBoxDelegate release];
    g_MessageBoxDelegate = nil;
}

s3eResult MessageBox_platform(const char* title, const char* text)
{
    NSAlert* alert = [[[NSAlert alloc] init] autorelease];
    NSString* message = [NSString stringWithUTF8String:title];

    [alert addButtonWithTitle:@"OK"];
    [alert setMessageText:message];
    [alert beginSheetModalForWindow:[NSApp mainWindow] modalDelegate:g_MessageBoxDelegate
        didEndSelector:@selector(alertDidEnd:returnCode:contextInfo:) contextInfo:nil];

    return S3E_RESULT_SUCCESS;
}
