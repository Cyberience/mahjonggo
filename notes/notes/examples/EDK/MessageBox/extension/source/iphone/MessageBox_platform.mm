/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
/*
 * iphone-specific implementation of the MessageBox extension.
 * Add any platform-specific functionality here.
 *
 * NOTE: This file was originally written by the extension builder, but will not
 * be overwritten (unless --force is specified) and is intended to be modified.
 */

#include "MessageBox_internal.h"
#import <UIKit/UIKit.h>

@interface MessageBoxDelegate : NSObject <UIAlertViewDelegate> {
}
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
@end

@implementation MessageBoxDelegate
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    MessageBoxComplete();
}
@end

static MessageBoxDelegate* g_MessageBoxDelegate = nil;

s3eResult MessageBoxInit_platform()
{
    g_MessageBoxDelegate = [[MessageBoxDelegate alloc] init];
    return S3E_RESULT_SUCCESS;
}

void MessageBoxTerminate_platform()
{
    [g_MessageBoxDelegate release];
    g_MessageBoxDelegate = nil;
}

s3eResult MessageBox_platform(const char* title, const char* text)
{
#ifdef __IPHONE_8_0
    if([UIAlertController class])
    {
        // iOS 8 implementation
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@(title)
                                                                       message:@(text)
                                                                preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK"
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {
                                                                  MessageBoxComplete();
                                                              }];
        [alert addAction: defaultAction];
        UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
        [rootViewController presentViewController: alert animated:YES completion:nil];
    }
    else
    {
#endif
        // pre-iOS 8 implementation
        UIAlertView* alert = [[[UIAlertView alloc] init] autorelease];
        [alert setTitle:@(title)];
        [alert setMessage:@(text)];
        [alert addButtonWithTitle:@"OK"];
        [alert setDelegate:g_MessageBoxDelegate];
        [alert show];
#ifdef __IPHONE_8_0
    }
#endif
    return S3E_RESULT_SUCCESS;
}
