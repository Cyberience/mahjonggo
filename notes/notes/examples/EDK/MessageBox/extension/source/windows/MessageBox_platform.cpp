/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
/*
 * windows-specific implementation of the MessageBox extension.
 * Add any platform-specific functionality here.
 *
 * NOTE: This file was originally written by the extension builder, but will not
 * be overwritten (unless --force is specified) and is intended to be modified.
 */

#include "MessageBox_internal.h"
#include <windows.h>

s3eResult MessageBoxInit_platform()
{
    //Add any platform-specific initialisation code here
    return S3E_RESULT_SUCCESS;
}

void MessageBoxTerminate_platform()
{
    //Add any platform-specific termination code here
}

s3eResult MessageBox_platform(const char* title, const char* text)
{
    MessageBox(0, text, title, 0);
    MessageBoxComplete();
    return S3E_RESULT_SUCCESS;
}
