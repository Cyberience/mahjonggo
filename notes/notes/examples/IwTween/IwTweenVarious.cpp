/*
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This document is protected by copyright, and contains information
 * proprietary to Marmalade.
 *
 * This file consists of source code released by Marmalade under
 * the terms of the accompanying End User License Agreement (EULA).
 * Please do not use this program/source code before you have read the
 * EULA and have agreed to be bound by its terms.
 */

/**
 * @page ExampleIwTweenVarious IwTweem Various Example
 *
 * This example demonstrates various uses of the IwTween API.
 *
 * All of the supported variable types are demonstrated, together with functionality
 * such as 'delay', repeat mode, easing function and callbacks.
 * For desktop builds, press '1' to delete existing tweens and start new tweens.
 *
 * The main classes used to achieve this are:
 *  - IwTween::CTweenManager
 *
 * The main functions used to achieve this are:
 *  - IwTween::CTweenManager::Tween()
 *  - IwTween::CTweenManager::Update()
 *
 * @include IwTweenVarious.cpp
 */

#include "IwTween.h"
#include "IwGx.h"
#include "s3eKeyboard.h"

using namespace IwTween;

// The Tween manager we will use
CTweenManager* g_Tweener = NULL;

// Uncomment these to test various variable types
#define TEST_FLOAT
#define TEST_BOOL
#define TEST_CHAR
#define TEST_SHORT
#define TEST_INT
#define TEST_EXAMPLES

#ifdef TEST_FLOAT
float valFloatTo;
float valFloatToDelta;
float valFloatFrom;
float valFloatFromDelta;
float valFloatToDelay;
float valFloatRepeat;
float valFloatMirror;
float valFloatExpInEVDefault;
float valFloatExpInEV4;

float valFloatCallbacks;
bool onStartFlag;
bool onCompleteFlag;
void onStartCallback(CTween* pTween) { onStartFlag = true; }
void onCompleteCallback(CTween* pTween) { onCompleteFlag = true; }
#endif
#ifdef TEST_BOOL
bool valBoolTo;
bool valBoolFrom;
#endif
#ifdef TEST_CHAR
signed char valByteTo;
signed char valByteFrom;
unsigned char valUByteTo;
unsigned char valUByteFrom;
#endif
#ifdef TEST_SHORT
short valShortTo;
short valShortFrom;
unsigned short valUShortTo;
unsigned short valUShortFrom;
#endif
#ifdef TEST_INT
int valIntTo;
int valIntFrom;
unsigned int valUIntTo;
unsigned int valUIntFrom;
#endif
#ifdef TEST_EXAMPLES
float valExFloatTo;
float valExFloatFromDelay;
char valExCharMirrorSin;
bool valExBoolFlip;
#endif

// Imitate 25 fps - we can do this as we know our Update/Render code is taking
// very little time
#define FORCED_TIME_UPDATE 0.04f

//------------------------------------------------------------------------------
void onStart(CTween* pTween) {}
void onComplete(CTween* pTween) {}
void InitTweens()
{
    if (!g_Tweener)
        g_Tweener = new CTweenManager;

#ifdef TEST_FLOAT
    // float, to (0 to 1)
    valFloatTo = 0.0f;
    g_Tweener->Tween(2.0f,
        FLOAT, &valFloatTo, 1.0f,
        END);

    // float, to, delta (1 to 2)
    valFloatToDelta = 1.0f;
    g_Tweener->Tween(2.0f,
        DELTA,
        FLOAT, &valFloatToDelta, 1.0f,
        END);

    // float, from (1 to 0)
    valFloatFrom = 0.0f;
    g_Tweener->Tween(2.0f,
        FROM,
        FLOAT, &valFloatFrom, 1.0f,
        END);

    // float, from, delta (2 to 1)
    valFloatFromDelta = 1.0f;
    g_Tweener->Tween(2.0f,
        FROM, DELTA,
        FLOAT, &valFloatFromDelta, 1.0f,
        END);

    // float, to (0 to 1, delay of 1 sec)
    valFloatToDelay = 0.0f;
    g_Tweener->Tween(2.0f,
        DELAY, 1.0f,
        FLOAT, &valFloatToDelay, 1.0f,
        END);

    // float, to, repeat (0 to 1, repeat)
    valFloatRepeat = 0.0f;
    g_Tweener->Tween(1.0f,
        REPEAT,
        FLOAT, &valFloatRepeat, 1.0f,
        END);

    // float, to, mirror (0 to 1, mirror)
    valFloatMirror = 0.0f;
    g_Tweener->Tween(1.0f,
        MIRROR,
        FLOAT, &valFloatMirror, 1.0f,
        END);

    // float, to, ease=expIn, easingValue default (0 to 1)
    valFloatExpInEVDefault = 0.0f;
    g_Tweener->Tween(1.0f,
        EASING, Ease::expIn,
        FLOAT, &valFloatExpInEVDefault, 1.0f,
        END);

    // float, to, ease=expIn, easingValue=4 (0 to 1)
    valFloatExpInEV4 = 0.0f;
    g_Tweener->Tween(1.0f,
        EASING, Ease::expIn,
        EASINGVALUE, 4.0f,
        FLOAT, &valFloatExpInEV4, 1.0f,
        END);

    // float, to, callbacks (0 to 1, +1s)
    valFloatCallbacks = 0.0f;
    onStartFlag = false;
    onCompleteFlag = false;
    g_Tweener->Tween(1.0f,
        DELAY, 1.0f,
        ONSTART, onStartCallback,
        ONCOMPLETE, onCompleteCallback,
        FLOAT, &valFloatCallbacks, 1.0f,
        END);
#endif
#ifdef TEST_BOOL
    // bool, to (false to true)
    valBoolTo = false;
    g_Tweener->Tween(1.0f,
        BOOL, &valBoolTo, true,
        END);

    // bool, from (true to false)
    valBoolFrom = false;
    g_Tweener->Tween(1.0f,
        FROM,
        BOOL, &valBoolFrom, true,
        END);
#endif
#ifdef TEST_CHAR
    // byte, to (-10 to 10)
    valByteTo = -10;
    g_Tweener->Tween(2.0f,
        BYTE, &valByteTo, 10,
        END);

    // byte, from (10 to -10)
    valByteFrom = -10;
    g_Tweener->Tween(2.0f,
        FROM,
        BYTE, &valByteFrom, 10,
        END);

    // ubyte, to (200 to 250)
    valUByteTo = 200;
    g_Tweener->Tween(2.0f,
        UBYTE, &valUByteTo, 250,
        END);

    // ubyte, from (250 to 200)
    valUByteFrom = 200;
    g_Tweener->Tween(2.0f,
        FROM,
        UBYTE, &valUByteFrom, 250,
        END);
#endif
#ifdef TEST_SHORT
    // short, to (-10 to 10)
    valShortTo = -10;
    g_Tweener->Tween(2.0f,
        SHORT, &valShortTo, 10,
        END);

    // short, from (30000 to -30000)
    valShortFrom = -30000;
    g_Tweener->Tween(2.0f,
        FROM,
        SHORT, &valShortFrom, 30000,
        END);

    // ushort, to (0 to 60000)
    valUShortTo = 0;
    g_Tweener->Tween(2.0f,
        USHORT, &valUShortTo, 60000,
        END);

    // ushort, from (60000 to 0)
    valUShortFrom = 0;
    g_Tweener->Tween(2.0f,
        FROM,
        USHORT, &valUShortFrom, 60000,
        END);
#endif
#ifdef TEST_INT
    // int, to (-10 to 10)
    valIntTo = -10;
    g_Tweener->Tween(2.0f,
        INT, &valIntTo, 10,
        END);

    // int, from (10 to -10)
    valIntFrom = -10;
    g_Tweener->Tween(2.0f,
        FROM,
        INT, &valIntFrom, 10,
        END);

    // uint, to (40000 to 40100)
    valUIntTo = 40000;
    g_Tweener->Tween(2.0f,
        UINT, &valUIntTo, 40100,
        END);

    // uint, from (40100 to 40000)
    valUIntFrom = 40000;
    g_Tweener->Tween(2.0f,
        FROM,
        UINT, &valUIntFrom, 40100,
        END);
#endif

#ifdef TEST_EXAMPLES
    // EXAMPLES FROM IWTWEEN.H DOCS
    // Tween float from [0..1]
    valExFloatTo = 0.0f;
    g_Tweener->Tween(2.0f,
        FLOAT, &valExFloatTo, 1.0f,
        END);

    // Tween float from [1..0] after delay of 0.5 sec
    valExFloatFromDelay = 0.0f;
    g_Tweener->Tween(1.0f,
        FROM,
        DELAY, 0.5f,
        ONSTART, onStart,
        ONCOMPLETE, onComplete,
        FLOAT, &valExFloatFromDelay, 1.0f,
        END);

    // Tween ubyte value from [0..255] using sin easing
    valExCharMirrorSin = 0;
    g_Tweener->Tween(1.0f,
        MIRROR,
        EASING, Ease::sineInOut,
        UBYTE, &valExCharMirrorSin, 255,
        END);

    // Tween bool value between true and false, using mirrored Ease::zero to create 'square wave'
    valExBoolFlip = false;
    g_Tweener->Tween(0.5f,
        MIRROR,
        EASING, Ease::zero,
        BOOL, &valExBoolFlip, true,
        END);
#endif
}
//------------------------------------------------------------------------------
void UpdateTweens()
{
    g_Tweener->Update(FORCED_TIME_UPDATE);
}
//------------------------------------------------------------------------------
void RenderTweens()
{
    int x = 0x2;
    int y = 0x2;
    char buff[0x80];

    sprintf(buff, "Num Tweens: %d", g_Tweener->GetNumTweens());
    IwGxPrintString(x, y, buff);
    y += 0xc;
#ifdef TEST_FLOAT
    sprintf(buff, "FloatTo (0 to 1):             %f", valFloatTo);
    IwGxPrintString(x, y, buff);
    y += 0x8;
    sprintf(buff, "FloatToDelta (1 to 2):        %f", valFloatToDelta);
    IwGxPrintString(x, y, buff);
    y += 0x8;
    sprintf(buff, "FloatFrom (1 to 0):           %f", valFloatFrom);
    IwGxPrintString(x, y, buff);
    y += 0x8;
    sprintf(buff, "FloatFromDelta (2 to 1):      %f", valFloatFromDelta);
    IwGxPrintString(x, y, buff);
    y += 0x8;
    sprintf(buff, "FloatToDelay (0 to 1, +1s):   %f", valFloatToDelay);
    IwGxPrintString(x, y, buff);
    y += 0x8;
    sprintf(buff, "FloatRepeat (0 to 1, rep):    %f", valFloatRepeat);
    IwGxPrintString(x, y, buff);
    y += 0x8;
    sprintf(buff, "FloatMirror (0 to 1, mir):    %f", valFloatMirror);
    IwGxPrintString(x, y, buff);
    y += 0x8;
    sprintf(buff, "FloatExpInEVDefault (0 to 1): %f", valFloatExpInEVDefault);
    IwGxPrintString(x, y, buff);
    y += 0x8;
    sprintf(buff, "FloatExpInEV4 (0 to 1):       %f", valFloatExpInEV4);
    IwGxPrintString(x, y, buff);
    y += 0xc;
    sprintf(buff, "FloatCallbacks (0 to 1):      %f", valFloatCallbacks);
    IwGxPrintString(x, y, buff);
    y += 0x8;
    sprintf(buff, "OnStartFlag:                  %s", onStartFlag ? "true" : "false");
    IwGxPrintString(x, y, buff);
    y += 0x8;
    sprintf(buff, "OnCompleteFlag:               %s", onCompleteFlag ? "true" : "false");
    IwGxPrintString(x, y, buff);
    y += 0xc;
#endif
#ifdef TEST_BOOL
    sprintf(buff, "BoolTo (F to T):              %s", valBoolTo ? "true" : "false");
    IwGxPrintString(x, y, buff);
    y += 0x8;
    sprintf(buff, "BoolFrom (T to F):            %s", valBoolFrom ? "true" : "false");
    IwGxPrintString(x, y, buff);
    y += 0xc;
#endif
#ifdef TEST_CHAR
    int valByteTo2 = (int)valByteTo; // required because std sprintf can't display signed chars
    sprintf(buff, "ByteTo (-10 to 10):           %i", valByteTo2);
    IwGxPrintString(x, y, buff);
    y += 0x8;
    int valByteFrom2 = (int)valByteFrom; // required because std sprintf can't display signed chars
    sprintf(buff, "ByteFrom (10 to -10):         %i", valByteFrom2);
    IwGxPrintString(x, y, buff);
    y += 0x8;
    unsigned int valUByteTo2 = (unsigned int)valUByteTo; // required because std sprintf can't display unsigned chars
    sprintf(buff, "UByteTo (200 to 250):         %u", valUByteTo2);
    IwGxPrintString(x, y, buff);
    y += 0x8;
    unsigned int valUByteFrom2 = (unsigned int)valUByteFrom;  // required because std sprintf can't display unsigned chars
    sprintf(buff, "UByteFrom (250 to 200):       %u", valUByteFrom2);
    IwGxPrintString(x, y, buff);
    y += 0xc;
#endif
#ifdef TEST_SHORT
    sprintf(buff, "ShortTo (-10 to 10):          %hi", valShortTo);
    IwGxPrintString(x, y, buff);
    y += 0x8;
    sprintf(buff, "ShortFrom (30000 to -30000):  %hi", valShortFrom);
    IwGxPrintString(x, y, buff);
    y += 0x8;
    sprintf(buff, "UShortTo (0 to 60000):        %hu", valUShortTo);
    IwGxPrintString(x, y, buff);
    y += 0x8;
    sprintf(buff, "UShortFrom (60000 to 0):      %hu", valUShortFrom);
    IwGxPrintString(x, y, buff);
    y += 0xc;
#endif
#ifdef TEST_INT
    sprintf(buff, "IntTo (-10 to 10):            %i", valIntTo);
    IwGxPrintString(x, y, buff);
    y += 0x8;
    sprintf(buff, "IntFrom (10 to -10):          %i", valIntFrom);
    IwGxPrintString(x, y, buff);
    y += 0x8;
    sprintf(buff, "UIntTo (40000 to 40100):      %u", valUIntTo);
    IwGxPrintString(x, y, buff);
    y += 0x8;
    sprintf(buff, "UIntFrom (40100 to 40000):    %u", valUIntFrom);
    IwGxPrintString(x, y, buff);
    y += 0xc;
#endif
#ifdef TEST_EXAMPLES
    sprintf(buff, "FloatTo (0 to 1):             %f", valExFloatTo);
    IwGxPrintString(x, y, buff);
    y += 0x8;
    sprintf(buff, "FloatFromDelay (1 to 0):      %f", valExFloatFromDelay);
    IwGxPrintString(x, y, buff);
    y += 0x8;
    unsigned int valCharMirrorSin2 = (unsigned int)valExCharMirrorSin;
    sprintf(buff, "CharMirrorSin:                %u", valCharMirrorSin2);
    IwGxPrintString(x, y, buff);
    y += 0x8;
    sprintf(buff, "BoolFlip:                     %s", valExBoolFlip ? "true" : "false");
    IwGxPrintString(x, y, buff);
    y += 0xc;
#endif
}
//------------------------------------------------------------------------------
void ShutdownTweens()
{
    delete g_Tweener;
}
//------------------------------------------------------------------------------
int main()
{
    IwGxInit();
    IwGxSetColClear(0, 0, 0xff, 0xff);

    InitTweens();

    // Loop forever, until the user or the OS performs some action to quit the app
    while (!s3eDeviceCheckQuitRequest())
    {
        s3eKeyboardUpdate();
        IwGxClear();

        // 1 to restart all tweens
        if (s3eKeyboardGetState(s3eKey1) & S3E_KEY_STATE_PRESSED)
        {
            printf("%s", "Restarting tweens");
            g_Tweener->Clear();
            InitTweens();
        }

        UpdateTweens();
        RenderTweens();

        IwGxFlush();
        IwGxSwapBuffers();
        s3eDeviceYield((int64)(FORCED_TIME_UPDATE * 1000));
    }

    ShutdownTweens();

    IwGxTerminate();
    return 0;
}
