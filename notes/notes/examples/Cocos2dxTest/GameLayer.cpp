/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2013 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
#include "GameLayer.h"

GameLayer::~GameLayer()
{
}

CCScene* GameLayer::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();

    // 'layer' is an autorelease object
    GameLayer *layer = GameLayer::create();

    // Add layer as a child to scene
    scene->addChild(layer);

    // Return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool GameLayer::init()
{
    if (!CCLayer::init())
        return false;

    // Create main loop
    this->schedule(schedule_selector(GameLayer::update));

    // TEST:
    // Create test sprite and add to layer
    CCSize size = CCDirector::sharedDirector()->getWinSize();
    sprite = CCSprite::create("gem1.png");
    sprite->setPosition(ccp(size.width / 2.0f, size.height / 2.0f));
    this->addChild(sprite);
    // Create a floor sprite and add to layer
    floor = CCSprite::create("floor.png");
    floor->setPosition(ccp(size.width / 2.0f, 10));
    this->addChild(floor);

    // Create Box2D world
    world = new b2World(b2Vec2(0, 100));

    // Create test sprite dynamic body
    b2BodyDef body_def;
    body_def.type = b2_dynamicBody;
    body_def.position.Set(sprite->getPositionX(), -sprite->getPositionY());
    spriteBody = world->CreateBody(&body_def);
    b2PolygonShape poly_shape;
    int w = sprite->getTextureRect().size.width;
    int h = sprite->getTextureRect().size.height;
    b2Vec2 cm(0, 0);
    poly_shape.SetAsBox(sprite->getTextureRect().size.width / 2, sprite->getTextureRect().size.height / 2, cm, 0);
    b2FixtureDef fixture;
    fixture.shape = &poly_shape;
    fixture.density = 1.0f;
    fixture.friction = 0.1f;
    fixture.restitution = 0.7f;
    spriteBody->CreateFixture(&fixture);

    // Create floor static body
    body_def;
    body_def.type = b2_staticBody;
    body_def.position.Set(floor->getPositionX(), -floor->getPositionY());
    floorBody = world->CreateBody(&body_def);
    poly_shape.SetAsBox(160,10, cm, 0);
    floorBody->CreateFixture(&fixture);
    // END TEST:

    return true;
}

void GameLayer::draw()
{
}

void GameLayer::update(float dt)
{
    // Update Box2D world
    world->Step(dt, 6, 3);

    // TEST:
    // Update object from box2d coordinates
    sprite->setPosition(ccp(spriteBody->GetPosition().x, -spriteBody->GetPosition().y));
    // END TEST:
}

