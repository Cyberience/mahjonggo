/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2013 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
#include "s3e.h"
#include "cocos2d.h"
#include "AppDelegate.h"

// Main entry point for the application
int main()
{
    AppDelegate* app = new AppDelegate;
    cocos2d::CCApplication::sharedApplication()->Run();

    return 0;
}
