ReduceTextureThreshold <Integer> Textures this size or above (in either axis) will be halved on upload (to reduce the memory footprint on lower-end devices)
ReduceTextureTwoThirds <Integer> Downscale to two-thirds of original size instead of half. Only relevant if ReduceTextureThreshold is set.
