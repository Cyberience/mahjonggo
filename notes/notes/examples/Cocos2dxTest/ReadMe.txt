#category: Graphics
Cocos2dxTest
==============

Cocos2dxTest demonstrates the basics of using the Cocos2D API to render to the
screen. It also demonstrates the basics of using the Box2D API to run a physics
simulation.
