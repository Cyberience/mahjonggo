/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2013 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
#ifndef __GAMELAYER_H__
#define __GAMELAYER_H__

#include "cocos2d.h"
#include "Box2D/Box2D.h"

using namespace cocos2d;

class GameLayer : public cocos2d::CCLayer
{
protected:
    b2World*        world;      // Instance of physics world
    // TEST:
    CCSprite*       sprite;     // Test sprite
    CCSprite*       floor;      // Test floor sprite
    b2Body*         spriteBody; // Test sprite body
    b2Body*         floorBody;  // Test body
    // END TEST:
public:
    ~GameLayer();

    // Init method
    virtual bool init();

    // Draw method
    virtual void draw();

    // Main update loop
    void update(float dt);

    // Create instance of scene
    static CCScene* scene();

    // preprocessor macro for "static create()" constructor ( node() deprecated )
    CREATE_FUNC(GameLayer);

};

#endif // __GAMELAYER_H__

