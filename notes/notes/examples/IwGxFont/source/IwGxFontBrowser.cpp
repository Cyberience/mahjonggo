/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwGxFontBrowser IwGxFont Browser Example
 *
 * This example application demonstrates how prepared text can be formatted.
 *
 * The main functions used to achieve this are:
 *  - IwGxFontSetFormatStart()
 *  - IwGxFontPrepareText()
 *  - CIwGxFontPreparedData::GetNextCharacterPosition()
 *
 * The application reads in the text of a selected html file and divides it up
 * based on the tags that it encounters. Each piece of text is placed in a
 * block and these blocks is wrapped in a CIwGxFontPreparedData object. Based
 * on the tag surrounding the text the CIwGxFontPreparedData object is given
 * specific formatting.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwGxFontBrowserImage.png
 *
 * @include IwGxFontBrowser.cpp
 */

#include "s3e.h"
#include "IwGx.h"
#include "IwGxFont.h"
#include "IwResManager.h"
#include "ExamplesMain.h"

//--------------------------------------------------------------------------------
// CFormatInfo
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
// Class containing information about how to format a block. As it's created each
// block will inherit the g_Current format info which will reflect the current state
// of tag parsing.
//--------------------------------------------------------------------------------
class CFormatInfo
{
public:
    bool m_Underline;
    bool m_Italic;
    bool m_Link;
    CIwColour m_Colour;
};

//--------------------------------------------------------------------------------
// Global state
//--------------------------------------------------------------------------------
CFormatInfo g_Current; // Format information reflecting the current state of parsing - opening/closing tags affects this info.
CFormatInfo g_Last; // Format information reflecting the state of parsing when the current font tag was opened
const char* g_LinkAddr; // The current address to attach to any created blocks
int32 g_LinkLength; // The length of the current address to attach to any created blocks
CIwSVec2 g_FormatPos; // The current 'cursor position' for laying out text
int32 g_Tick; // Keeping count of which frame we're on for link animations
uint32 g_ScreenWidth, g_ScreenHeight; //Remember gx size
char* g_CurrentPageAddress;

CIwArray<class CBlock*> g_Blocks; // A list of all formatted blocks that make up the current page
CIwArray<class CLink*> g_Links; // A list of all links in the current page
int32 g_SelectedLink; // An index into g_Links to indicate the currently selected link.

//--------------------------------------------------------------------------------
// CLink
//
// Class containing information about a link. If a block is created with
// g_Current.m_Link == true a link will be created and added to the current list.
//--------------------------------------------------------------------------------
class CLink
{
public:
    ~CLink()
    {
        delete m_Addr;
    }

    CBlock* m_Block;
    char* m_Addr;
};

//--------------------------------------------------------------------------------
// CBlock
//
// Container for a CIwGxFontPreparedData and a format info to describe how to
// render it.
//--------------------------------------------------------------------------------
class CBlock
{
public:
    void Render()
    {
        // clear all flags
        IwGxFontClearFlags(0xffffffff);

        // if this block is a link...
        if (m_Info.m_Link)
        {
            IwGxFontSetFlags(IW_GX_FONT_UNDERLINE_F); // underline

            //make link flash if it is the currently selected link
            if ((g_Tick & 0x4) != 0 && g_Links[g_SelectedLink]->m_Block == this )
            {
                IwGxFontSetCol(0xffff00ff);
            }
            else
            {
                IwGxFontSetCol(0xffff0000);
            }
        }
        else
        {
            //Set underline flag if block is underlined
            if (m_Info.m_Underline)
                IwGxFontSetFlags(IW_GX_FONT_UNDERLINE_F);
            //Set colour from block
            IwGxFontSetCol(m_Info.m_Colour);
        }


        //Set italic flag if block is underlined
        if (m_Info.m_Italic)
            IwGxFontSetFlags(IW_GX_FONT_ITALIC_F);

        //Render the prepared text
        IwGxFontDrawText(m_Data);
    }

    //Container for formatted text
    CIwGxFontPreparedData m_Data;
    //Format for rendering
    CFormatInfo m_Info;
};

//--------------------------------------------------------------------------------
// Tag functions
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
// The following function sets the the current prepared text to use the italic
// format type.
//--------------------------------------------------------------------------------
void DoItalic(bool open)
{
    g_Current.m_Italic = open;
    if (!open)
        g_FormatPos.x += 5;
}

//--------------------------------------------------------------------------------
// The following function sets the the current prepared text to use the underline
// format type.
//--------------------------------------------------------------------------------
void DoUnderline(bool open)
{
    g_Current.m_Underline = open;
}

//--------------------------------------------------------------------------------
// The following function sets the the current prepared text to use the link
// format type.
//--------------------------------------------------------------------------------
bool DoLink(bool open, const char* pTag, int32 length)
{
    if (!open)
    {
        g_Current.m_Link = false;
        return true;
    }

    //Find '=' in the tag (assumes href is the only property)
    const char* pEq = strchr(pTag, '=');
    if (!pEq)
        return false;

    //+3 here assumes address is quoted
    g_LinkLength = length - (pEq - pTag + 3);
    g_LinkAddr = pEq+2;
    g_Current.m_Link = true;

    return true;
}

//--------------------------------------------------------------------------------
// The following function calls the relevant tag function. The tag is tested to
// see if it is a closing tag, if it is the relevant function is called and passed
// the open bool set to false. If it is an opening tag, the relevant function
// is called and passed the open tag set to true.
//--------------------------------------------------------------------------------
void DoTag(const char* pTag, int32 length)
{
    //Forward slash at tag start indicates close tag
    bool open = pTag[0] != '/';
    if (!open)
    {
        pTag += 1;
        length -= 1;
    }

    //<i> italic
    if (length == 1 && strncmp(pTag, "i", 1) == 0 )
    {
        DoItalic(open);
    }
    //<u> underline
    else if (length == 1 && strncmp(pTag, "u", 1) == 0 )
    {
        DoUnderline(open);
    }
    //<a href="x"> link
    else if (strncmp(pTag, "a", 1) == 0 && DoLink(open, pTag+1, length-1) )
    {
    }
    //<br> newline
    else if (strncmp(pTag, "br", 2) == 0 )
    {
        //Reset cursor to left hand side
        g_FormatPos.x = 0;
        //And line feed
        g_FormatPos.y += (int16)IwGxFontGetFont()->GetHeight();
    }
}
//--------------------------------------------------------------------------------
// AddBlock
//
// The following function creates a new block for each delimited string, recording
// the current parsing state for rendering. It also creates a link entry if the
// current state indicates a link.
//--------------------------------------------------------------------------------
void AddBlock(const char* text, int32 length)
{
    CBlock* pBlock = new CBlock;

    //Set format start to the cursor position. Format start controls where the text begins
    //within the format rect.
    IwGxFontSetFormatStart(g_FormatPos);

    //Prepare the text - this will fill pBlock->m_Data with formatting information for the
    //block for individual glyphs, including position, scale, and applying word wrapping.
    IwGxFontPrepareText(pBlock->m_Data, text, length);

    //Update the format start to point to the last character rendered.
    g_FormatPos = pBlock->m_Data.GetNextCharacterPosition();

    //Copy current parsing state into this block
    pBlock->m_Info = g_Current;

    //Add to the list of blocks
    g_Blocks.push_back(pBlock);

    //If this will be a link add to the global list
    if (g_Current.m_Link)
    {
        CLink* pLink = new CLink;
        //Record the block
        pLink->m_Block = pBlock;

        //Make copy of address
        pLink->m_Addr = (char*)s3eMalloc(g_LinkLength+1);
        memcpy(pLink->m_Addr, g_LinkAddr, g_LinkLength);
        //Zero-terminate
        pLink->m_Addr[g_LinkLength] = 0;

        //Add to the list of links
        g_Links.push_back(pLink);
    }
}
//--------------------------------------------------------------------------------
// Page Management
//
// HTML doesn't repect new lines in the file (only tagged newlines, so we need to
// strip any newline chars).
//--------------------------------------------------------------------------------
int32 RemoveNewLines(char* line)
{
    char* start = line;
    char* out = line;
    while (*line)
    {
        if (*line != '\n')
        {
            *out = *line;
            out++;
        }
        line++;
    }
    *out = 0;

    return out - start;
}

void ClosePage()
{
    for (uint32 b = 0; b < g_Blocks.size(); b++)
        delete g_Blocks[b];
    for (uint32 l = 0; l < g_Links.size(); l++)
        delete g_Links[l];
    g_Blocks.clear();
    g_Links.clear();
}

//--------------------------------------------------------------------------------
// The following function reads in the contents of the specified html file.
// The fomatting rectangle is set using the IwGxFontSetRect() function and other
// formatting information is set using the instantiated CFormatInfo struct.
// Each block of text that is surrounded by a tag is isolated and placed in a
// CBlock item. The DoTag() function is then called to do the required parsing.
//--------------------------------------------------------------------------------
void OpenPage(const char* html)
{
    //Get the contents of the next page
    char* pContents = NULL;
    s3eFile* pFile = s3eFileOpen(html, "rt");
    int32 size = 0;

    //If the file open succeeds, the contents of the file are placed in @a pContents
    //and the buffer is zero terminated.
    if (pFile)
    {
        size = s3eFileGetSize(pFile);
        pContents = (char*)s3eMalloc(size+1);

        //size may be less than file size if NL/CF pairs are converted
        size = s3eFileRead(pContents, 1, size, pFile);

        //Zero-terminate
        pContents[size] = 0;

        size = RemoveNewLines(pContents);

        s3eFileClose(pFile);
    }
    else
    {
        // If page is not found, display error message
        pContents = (char*)s3eMalloc(2048);
        sprintf(pContents, "File '%s' not found", html);
        size = strlen(pContents);
    }

    //Remember gx state and details required to rebuild the page
    if (g_CurrentPageAddress != html)
    {
        if (g_CurrentPageAddress)
            delete g_CurrentPageAddress;

        g_CurrentPageAddress = new char[strlen(html)+1];
        strcpy(g_CurrentPageAddress, html);
    }

    g_ScreenWidth = IwGxGetScreenWidth();
    g_ScreenHeight = IwGxGetScreenHeight();

    //Close old page - NB. This must be done after opening
    //the next page because the next address may be owned by the current page
    ClosePage();

    //Set the format rectangle so text doesn't overlap the Ideaworks logo
    IwGxFontSetRect(CIwRect(10,30,(int16)g_ScreenWidth-10,(int16)g_ScreenHeight-40));

    //Set the default parsing state
    g_Current.m_Colour.Set(0x80,0x80,0x80,0xff);
    g_Current.m_Italic = false;
    g_Current.m_Underline = false;
    g_Current.m_Link = false;
    g_Last = g_Current;

    g_FormatPos = CIwSVec2(0,0);



    //Now iterate the contents, chopping off tags and creating blocks
    char* pCursor = pContents;
    char* pEnd = pContents+size;

    while (pCursor < pEnd)
    {
        //find the continuous span to the next tag
        char* pOpenTag = strchr(pCursor, '<');
        if (!pOpenTag)
        {
            //There are no more tags - create a block for the rest of the text and exit
            AddBlock(pCursor, pEnd - pCursor);
            break;
        }
        else
        {
            //Add a block for the span to the tag's opening
            if ((pOpenTag - pCursor) > 1 )
            {
                AddBlock(pCursor, pOpenTag - pCursor);
            }

            //Find the closing brace of the tag
            char* pCloseTag = strchr(pOpenTag, '>');
            if (!pCloseTag)
            {
                //badly formed tag - end here...
                break;
            }
            else
            {
                //Parse the tag and perform any state changes required for it
                DoTag(pOpenTag + 1, pCloseTag - pOpenTag - 1);
                pCursor = pCloseTag+1;
            }
        }

    }

    // Contents are no longer needed (text now exists as formatted glyphs within CIwGxFontPreparedData classes)
    delete pContents;

    // Default to the first link
    g_SelectedLink = 0;
}

//--------------------------------------------------------------------------------
// Main functions
//--------------------------------------------------------------------------------


void CreateButtonsUI(int w, int h)
{
    // Create the UI layout
    AddButton("Select", 2, h - 10, 42, 10, s3eKeyAbsASK);
}

void SurfaceChangedCallback()
{
    DeleteButtons();
    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}

//--------------------------------------------------------------------------------
// The following function initialises the resource manager and IwGxFont. The
// Resource Manager is used to load the group file which points to the font
// resources.
// The font resources are then used to form the font and this font is set to
// be the current font using the IwGxFontSetFont() function.
// The display text is loaded from a webpage called main.html.
//--------------------------------------------------------------------------------
void ExampleInit()
{
    //Initialise ResManager and IwGxFont (IwGx has already been initialised by example framework)
    IwGxRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
    IwResManagerInit();
    IwGxFontInit();

    //Load the example's font group
    IwGetResManager()->LoadGroup("IwGxFontBrowser.group");

    //Get default font and set it to be the current font
    CIwGxFont* font = (CIwGxFont*)IwGetResManager()->GetResNamed("arial14", "CIwGxFont");
    IwGxFontSetFont(font);

    //Open the first page
    OpenPage("main.html");

    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}

//--------------------------------------------------------------------------------
// The following function closes any open html page, and terminates IwGxFont and
// the Resource Manager on shutdown.
//--------------------------------------------------------------------------------
void ExampleShutDown()
{
    // Clear up
    ClosePage();
    if (g_CurrentPageAddress)
        delete g_CurrentPageAddress;

    // Release array storage
    g_Blocks.clear_optimised();
    g_Links.clear_optimised();

    //Terminate modules
    IwGxFontTerminate();
    IwResManagerTerminate();
    IwGxUnRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
}

//--------------------------------------------------------------------------------
// The following function calls the OpenPage function which opens and reads in the
// contents of the specified html file. The keyboard state is checked to see if any
// of the s3eKeyAbsUp, s3eKeyAbsDown and s3eKeyAbsASK keys have been pressed, as it
// is using these three keys that the user can browse the three supplied webpages.
//--------------------------------------------------------------------------------
bool ExampleUpdate()
{
    if (g_ScreenWidth != IwGxGetScreenWidth() || g_ScreenHeight != IwGxGetScreenHeight() )
        OpenPage(g_CurrentPageAddress);


    //Up/Down select a link
    if (CheckCursorState() == EXCURSOR_UP )
    {
        g_SelectedLink--;
    }
    else if (CheckCursorState() == EXCURSOR_DOWN )
    {
        g_SelectedLink++;
    }
    else if (!g_Links.empty() && CheckButton("Select") & S3E_KEY_STATE_PRESSED )
    {
        //ASK activates the selected link
        OpenPage(g_Links[g_SelectedLink]->m_Addr);
    }

    //Maintains the
    if (g_SelectedLink < 0)
        g_SelectedLink = g_Links.size() - 1;
    else if (g_SelectedLink >= (int32)g_Links.size() )
        g_SelectedLink = 0;

    //Keep track of which frame we're on for link animation
    g_Tick++;

    return true;
}

//--------------------------------------------------------------------------------
// The following function renders each of the blocks that have been discovered on
// the selected web page.
//--------------------------------------------------------------------------------
void ExampleRender()
{
    //Need lighting on to colour text
    IwGxLightingOn();

    //Render all blocks
    for (uint32 b = 0; b < g_Blocks.size(); b++)
    {
        g_Blocks[b]->Render();
    }

    IwGxPrintString(5, IwGxGetScreenHeight() - 70, "Use Up/Down to select a link", false);
    RenderCursorskeys();

    // Flush and show buffer
    IwGxFlush();
    IwGxSwapBuffers();
}
//--------------------------------------------------------------------------------
