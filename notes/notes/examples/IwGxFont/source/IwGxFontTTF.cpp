/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwGxFontTTF IwGxFont TTF Example
 *
 * This example application loads a True Type font and uses this font to display
 * text on the screen.
 *
 * The main functions used to achieve this are:
 * - IwGxFontInit()
 * - IwGetResManager()
 * - IwGxFontSetFont()
 * - IwGxFontSetCol()
 * - IwGxFontSetRect()
 * - IwGxFontDrawText()
 *
 * The example begins by loading the font group file. The group file contains
 * definitions of CIwGxFont resources. These reference a ttf file and a point
 * size to use. The render function sets one of these font resources before
 * printing text.
 *
 * The colour, formatting rectangle and alignment are set for the font and it
 * is then drawn using the IwGxFontDrawText() function.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwGxFontTTFImage.png
 *
 * @include IwGxFontTTF.cpp
 */

//-------------------------------------------------------------------------------


#include "IwGx.h"
#include "IwGxFont.h"
#include "IwResManager.h"

CIwGxFont* s_Font6;
CIwGxFont* s_Font10;

//-------------------------------------------------------------------------------
// The following function initialises the resource manager and IwGxFont. The
// Resource Manager is used to load the group file which points to the font
// resources.
// The font resources are then used to form the font and this font is set to
// be the current font using the IwGxFontSetFont() function.
//-------------------------------------------------------------------------------
void ExampleInit()
{
    //Initialise modules
    IwResManagerInit();
    IwGxFontInit();

    //Load the group containing the example font
    IwGetResManager()->LoadGroup("IwGxFontTTF.group");

    //Get the example font and set it to be the current font
    s_Font6 = (CIwGxFont*)IwGetResManager()->GetResNamed("Serif_6", "CIwGxFont");
    s_Font10 = (CIwGxFont*)IwGetResManager()->GetResNamed("Kaushan_10", "CIwGxFont");
}

//-------------------------------------------------------------------------------
// The following function terminates IwGxFont and the Resource Manager on
// shutdown.
//-------------------------------------------------------------------------------
void ExampleShutDown()
{
    //terminate the modules
    IwGxFontTerminate();
    IwResManagerTerminate();
}


bool ExampleUpdate()
{
    return true;
}

//-------------------------------------------------------------------------------
// The following function sets the font colour using the IwGxFontSetCol()
// function as well as the position and area alloted to the displayed text using
// the IwGxFontSetRect(). The IwGxFontSetRect() function accepts a CIwRect object
// which defines the rectangle the text will exist within.
// The text that is to be displayed on screen is entered using the
// IwGxFontDrawText() function.
//-------------------------------------------------------------------------------
void ExampleRender()
{
    //Lighting is required to colour fonts
    IwGxLightingOn();

    //Set font colour to black
    IwGxFontSetCol(0xffa0a0a0);

    //Set the formatting rect - this controls where the text appears and what it is formatted against
    IwGxFontSetRect(CIwRect(10,40,(int16)IwGxGetScreenWidth()-20,40));

    //Set the alignment within the formatting rect
    IwGxFontSetAlignmentVer(IW_GX_FONT_ALIGN_BOTTOM);

    // Draw title text
    IwGxFontSetFont(s_Font10);
    IwGxFontDrawText("Lorem ipsum dolor sit amet");

    //Change formatting rect
    IwGxFontSetRect(CIwRect(10,90,(int16)IwGxGetScreenWidth()-20,(int16)IwGxGetScreenHeight()-100));

    //Change the alignment
    IwGxFontSetAlignmentVer(IW_GX_FONT_ALIGN_TOP);

    // Draw some more text
    IwGxFontSetFont(s_Font6);
    IwGxFontDrawText("Consectetur adipisicing elit, sed do eiusmod tempor incididunt "
        "ut labore et dolore magna aliqua. Ut enim ad minim veniam.");

    //Flush and show
    IwGxFlush();
    IwGxSwapBuffers();
}
