/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwGxFontClipping IwGxFont Clipping Example
 *
 * This example application loads a font and illustrates how clipping can be
 * used to isolate parts of the display text in order to change their
 * formatting.
 *
 * The main functions used to achieve this are:
 *  - IwGxFontInit()
 *  - IwGxFontSetFont()
 *  - IwGxFontSetAlignmentHor()
 *  - IwGxFontPrepareText()
 *  - CIwGxFontPreparedData::GetHeight()
 *  - IwGxFontSetCol()
 *  - IwGxFontSetRect()
 *  - IwGxSetScissorScreenSpace()
 *  - IwGxFontDrawText()
 *
 * The IwGxFontPrepareText() function is used to add the display text to a
 * CIwPreparedDataText object. The use of prepared text is an optimisation that
 * means the text does not need to be reformated for each render loop.
 *
 * The IwGxFontSetRect() function is used to set the position of the prepared
 * text, which is gradually scrolled vertically.
 *
 * The IwGxSetScissorScreenSpace() function is used to isolate the text into
 * slices which are then rendered interpolating between black and white.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwGxFontClippingImage.png
 *
 * @include IwGxFontClipping.cpp
 */

#include "IwGx.h"
#include "IwGxFont.h"
#include "IwResManager.h"

CIwGxFontPreparedData s_PreparedText;
int32 s_YPos = 0;

//-------------------------------------------------------------------------------
// The following function initialises the resource manager and IwGxFont. The
// Resource Manager is used to load the group file which points to the font
// resources.
// The font resources are then used to form the font and this font is set to
// be the current font using the IwGxFontSetFont() function.
// A callback is set using the IwGxFontSetParameterCallback() function.
// The IwGxFontPrepareText() function adds a text string to an instantiated
// CIwPreparedDataText object.
//-------------------------------------------------------------------------------
void ExampleInit()
{
    //Initialise modules
    IwResManagerInit();
    IwGxFontInit();

    //Load the group containing the example font
    IwGetResManager()->LoadGroup("IwGxFontBasic.group");

    //Get the example font and set it to be the current font
    CIwGxFont* font = (CIwGxFont*)IwGetResManager()->GetResNamed("arial14", "CIwGxFont");
    IwGxFontSetFont(font);


    IwGxFontSetRect(CIwRect(0, 0, (int16)IwGxGetScreenWidth(), 10000));
    IwGxFontSetAlignmentHor(IW_GX_FONT_ALIGN_CENTRE);

    //Prepare text
    IwGxFontPrepareText(s_PreparedText, "This example demonstrates the use of IwGxFontPrepareText, \
IwGxFontSetRect, \
IwGxSetScissorScreenSpace, \
IwGxFontSetCol, \
to repeatedly render the same string with different colour settings/clipping without reformatting \
to create a gradient coloured scrolling field.");

    s_YPos = IwGxGetScreenHeight();
}

//-------------------------------------------------------------------------------
// The following function terminates IwGxFont and the Resource Manager on
// shutdown.
//-------------------------------------------------------------------------------
void ExampleShutDown()
{
    // Release prepared text
    s_PreparedText.Clear();

    //terminate the modules
    IwGxFontTerminate();
    IwResManagerTerminate();
}

//-------------------------------------------------------------------------------
// The following function decrements the verticle position variable for each
// render loop.
//-------------------------------------------------------------------------------
bool ExampleUpdate()
{
    s_YPos -= 1;

    if (s_YPos + s_PreparedText.GetHeight() < 40 )
        s_YPos = IwGxGetScreenHeight();

    return true;
}

//-------------------------------------------------------------------------------
// The following function sets the formatting rectangle for the text. The verticle
// position of the formatting rectangle is set using the @a s_YPos variable
// set in the Update section.
// The screen is then sliced into ten sections. A for loop is used to give each
// slice a colour and isolate each section for rendering in that colour. Each slice
// is isolated using the IwGxSetScissorScreenSpace() function.
//-------------------------------------------------------------------------------
void ExampleRender()
{
    //Lighting is required to colour fonts
    IwGxLightingOn();

    IwGxFontSetRect(CIwRect(0,(int16)s_YPos,0,0));

    const uint32 slices = 10;
    const int32 sliceHeight = (IwGxGetScreenHeight() - 40) / slices;
    for (uint32 y = 0; y < slices; y++)
    {
        //linear interpolate between black and white
        uint32 col = (y + 1) * 0xff / slices;
        CIwColour iwcol;
        iwcol.r = iwcol.g = iwcol.b = (uint8)col;
        iwcol.a = 0xff;
        IwGxFontSetCol(iwcol);


        IwGxSetScissorScreenSpace(0, y * sliceHeight + 40, IwGxGetScreenWidth(), sliceHeight);
        IwGxFontDrawText(s_PreparedText);
    }


    IwGxClearScissorScreenSpace();

    //Flush and show
    IwGxFlush();
    IwGxSwapBuffers();
}
