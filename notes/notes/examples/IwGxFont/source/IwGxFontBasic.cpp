/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwGxFontBasic IwGxFont Basic Example
 *
 * This example application loads a font and uses this font to display text on
 * the screen.
 *
 * The main functions used to achieve this are:
 *  - IwGxFontInit()
 *  - IwGetResManager()
 *  - IwGxFontSetFont()
 *  - IwGxFontSetCol()
 *  - IwGxFontSetRect()
 *  - IwGxFontDrawText()
 *
 * The example begins by loading the font group file, which points the resource
 * manager to the defining GXFONT file. The GXFONT file sets the TGA file,
 * which graphically defines the font, and the character map. The font is then
 * set as the current font so all printing from this point will use the new
 * font.
 *
 * The colour and formatting rectangle are set for the font and it is then
 * drawn using the IwGxFontDrawText() function.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwGxFontBasicImage.png
 *
 * @include IwGxFontBasic.cpp
 */

#include "IwGx.h"
#include "IwGxFont.h"
#include "IwResManager.h"

//-------------------------------------------------------------------------------
// The following function initialises the resource manager and IwGxFont. The
// Resource Manager is used to load the group file which points to the font
// resources.
// The font resources are then used to form the font and this font is set to
// be the current font using the IwGxFontSetFont() function.
//-------------------------------------------------------------------------------
void ExampleInit()
{
    //Initialise modules
    IwResManagerInit();
    IwGxFontInit();

    //Load the group containing the example font
    IwGetResManager()->LoadGroup("IwGxFontBasic.group");

    //Get the example font and set it to be the current font
    CIwGxFont* font = (CIwGxFont*)IwGetResManager()->GetResNamed("arial14", "CIwGxFont");
    IwGxFontSetFont(font);
}

//-------------------------------------------------------------------------------
// The following function terminates IwGxFont and the Resource Manager on
// shutdown.
//-------------------------------------------------------------------------------
void ExampleShutDown()
{
    //terminate the modules
    IwGxFontTerminate();
    IwResManagerTerminate();
}


bool ExampleUpdate()
{
    return true;
}

//-------------------------------------------------------------------------------
// The following function sets the font colour using the IwGxFontSetCol()
// function as well as the position and area alloted to the displayed text using
// the IwGxFontSetRect(). The IwGxFontSetRect() function accepts a CIwRect object
// which defines the rectangle the text will exist within.
// The text that is to be displayed on screen is entered using the
// IwGxFontDrawText() function.
//-------------------------------------------------------------------------------
void ExampleRender()
{
    //Lighting is required to colour fonts
    IwGxLightingOn();

    //Set font colour to grey
    IwGxFontSetCol(0xff808080);

    //Set the formatting rect - this controls where the text appears and what it is formatted against
    IwGxFontSetRect(CIwRect(10,40,(int16)IwGxGetScreenWidth()-10,(int16)IwGxGetScreenHeight()-50));

    //Draw the text
    IwGxFontDrawText("This is text printed using IwGxFont.\nText printed through this module is device-independent and can be used in both SW and HW modes.\nFonts produced this way can be formatted using word wrap and alignment options, and accept utf8 encoded strings for maximum localisability.");

    //Flush and show
    IwGxFlush();
    IwGxSwapBuffers();
}
