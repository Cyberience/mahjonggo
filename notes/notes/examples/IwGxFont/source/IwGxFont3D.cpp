/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwGxFont3D IwGxFont 3D Example
 *
 * This example application loads a font and illustrates how it can be
 * displayed in 3D.
 *
 * The main functions used to achieve this are:
 *  - IwGxFontInit()
 *  - IwGxFontSetFont()
 *  - IwGxFontPrepareText()
 *  - CIwGxFontPreparedData::GetNumChars()
 *  - CIwGxFontPreparedData::GetCharacterArea()
 *  - IwGxFontSetRect()
 *  - IwGxFontDrawText()
 *
 * A set of four vertices is allocated for each glyph in order to map their raw
 * positions. The GetCharacterArea() function is used to find out how much area
 * is taken up by a character.  The IwGxFontSetUVs() function sends a UV stream
 * appropriate for rendering all characters in the prepared text using a
 * GX_QUAD_LIST primitive stream.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwGxFont3DImage.png
 *
 * @include IwGxFont3D.cpp
 */

#include "IwGx.h"
#include "IwGx.h"
#include "IwGxFont.h"
#include "IwMaterial.h"
#include "IwResManager.h"

CIwGxFont* s_Font;
CIwGxFontPreparedData s_PreparedText;
int32 s_Tick;

//-------------------------------------------------------------------------------
// The following function initialises the resource manager and IwGxFont. The
// Resource Manager is used to load the group file which points to the font
// resources.
// The font resources are then used to form the font and this font is set to
// be the current font using the IwGxFontSetFont() function.
// The formatting rectangle to hold the font is then set using the IwGxFontSetRect()
// function.
// A CIwGxFontPreparedData object is handed the display text for the example.
//-------------------------------------------------------------------------------
void ExampleInit()
{
    //Initialise modules
    IwResManagerInit();
    IwGxFontInit();

    //Load the group containing the example font
    IwGetResManager()->LoadGroup("IwGxFont3D.group");

    //Get the example font and set it to be the current font
    s_Font = (CIwGxFont*)IwGetResManager()->GetResNamed("arial24", "CIwGxFont");
    IwGxFontSetFont(s_Font);

    //Set the formatting rect - we only want one long line so we set the region very wide
    IwGxFontSetRect(CIwRect(0,0,10000,1000));

    //Prepare the text - we will then have a continuous line of text
    IwGxFontPrepareText(s_PreparedText, "Using CIwGxFontPrepareText, IwGxFontSetUVs, and IwGxFontGetMaterial you can format text in any fashion/coordinate space you desire!");
}

//-------------------------------------------------------------------------------
// The following function terminates IwGxFont and the Resource Manager on
// shutdown.
//-------------------------------------------------------------------------------
void ExampleShutDown()
{
    //Clear up handle
    s_PreparedText.Clear();

    //terminate the modules
    IwGxFontTerminate();
    IwResManagerTerminate();
}

//-------------------------------------------------------------------------------
// The following function increments a tick on each render loop.
//-------------------------------------------------------------------------------
bool ExampleUpdate()
{
    s_Tick++;
    return true;
}

//-------------------------------------------------------------------------------
// The following function constructs the cone onto which the font is placed.
// The view matrix is then rotated around the y axix so that it looks like the
// text is moving around the cone.
// A set of four vertices is allocated for each glyph in order to map their raw
// positions. The GetCharacterArea() function is used to find out how much area
// is taken up by a character.
// The IwGxFontSetUVs() function sends a UV stream appropriate for rendering all
// characters in the prepared text using a GX_QUAD_LIST primitive stream.
//-------------------------------------------------------------------------------
void ExampleRender()
{
    //Rotate around the cone we're constructing at a regular pace
    CIwFMat view;
    view.SetRotY((float)(-s_Tick*10*PI/0x800+PI));
    view.t = view.RotateVec(CIwFVec3(0,0,-7000));
    view.LookAt(view.t, CIwFVec3(0,0,0), CIwFVec3(0,-0x1000, 0));
    IwGxSetViewMatrix(&view);

    //Lighting is required to colour fonts
    IwGxLightingOn();
    IwGxSetColStream(NULL);

    for (int32 m = 0; m < s_Font->GetNumberMaterials(); m++)
    {
        //IwGxFontSetUVs sets a stream corresponding to the prepared text
        uint32* charIDs;
        uint32 chars = IwGxFontSetUVs(s_PreparedText, -1, m, &charIDs);

        uint32 numberVerts = chars*4;
        CIwFVec3* pVerts = IW_GX_ALLOC(CIwFVec3, numberVerts);
        CIwFVec3* pVert = pVerts;

        //Collect the raw positions that the glyphs were placed at...
        uint32 i;
        for (i = 0; i < chars; i++)
        {
            CIwRect area = s_PreparedText.GetCharacterArea(charIDs[i]);

            *pVert = CIwFVec3(area.x, area.y, 0); pVert++;
            *pVert = CIwFVec3(area.x, (float)(area.y+area.h), 0); pVert++;
            *pVert = CIwFVec3((float)(area.x+area.w), (float)(area.y+area.h), 0); pVert++;
            *pVert = CIwFVec3((float)(area.x+area.w), area.y, 0); pVert++;
        }

        //Now we transform them onto a very high cone...
        pVert = pVerts;
        for (i = 0; i < numberVerts; i++)
        {
            float along = pVert->x;
            CIwFMat rot(CIwFMat::g_Identity);
            rot.PostRotateZ(along*10*PI/0x800);
            float radius = (pVert->y+100-(along/20));
            CIwFVec3 v(0, (radius*2), 0);
            v = rot.RotateVec(v);

            *pVert = CIwFVec3(v.x*20, -(radius*50-4500), v.y*20);
            pVert++;
        }

        //Set the verts in world space
        IwGxSetVertStreamWorldSpace(pVerts, numberVerts);

        //Create a material from the font's material to make it darker -
        //note we could do anything else we like to the material
        CIwMaterial* mat = IW_GX_ALLOC_MATERIAL();
        mat->Copy(*IwGxFontGetFont()->GetMaterial(m));
        mat->SetColEmissive(0x80,0x80,0x80,0xff);
        IwGxSetMaterial(mat);

        //Finally draw the glyphs
        IwGxDrawPrims(IW_GX_QUAD_LIST, NULL, numberVerts);
    }

    //Flush and show
    IwGxFlush();
    IwGxSwapBuffers();
}
