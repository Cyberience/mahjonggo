/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwGxFontParameters IwGxFont Parameters Example
 *
 * This example application loads a font and uses this font to display text on
 * the screen.  The text to be displayed contains tokens. A callback is used to
 * substitute a string for each token that is encountered before the text is
 * displayed.
 *
 * The main functions used to achieve this are:
 *  - IwGxFontSetFont()
 *  - IwGxFontSetParameterCallback()
 *  - IwGxFontDrawText()
 *
 * The example begins by loading the font. This is done in the exact same way
 * as is described in our first example, @ref ExampleIwGxFontBasic "IwGxFont
 * Basic Example".
 *
 * A callback is registered that will be called whenever a token (a string
 * wrapped in parenthasis, i.e., '{','}') is encountered in the display text.
 *
 * The callback will return a string from an array of possible strings based on
 * keyboard input.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwGxFontParametersImage.png
 *
 * @include IwGxFontParameters.cpp
 */

#include "s3e.h"
#include "IwGx.h"
#include "IwGxFont.h"
#include "IwResManager.h"
#include "ExamplesMain.h"

const char* s_Items[] =
{
    "the quick brown fox",
    "the lazy dog",
    "the man in the moon",
    "the dish and the spoon",
};

int32 s_SubjectIndex = 0;
int32 s_ObjectIndex = 1;

//-----------------------------------------------------------------------------
// This example callback substitutes the tokens {subject} or {object} with the
// relevant entry from s_Items.
//-----------------------------------------------------------------------------
const char * ExampleCallback(const char * in_start, int in_length, char* temp_buffer)
{
    // note we cannot use strcmp because in_start is a pointer into our text buffer
    // (and thus will not be zero-terminated)

    if (in_length == 7 && memcmp(in_start, "subject", 7)==0)
    {
        strcpy(temp_buffer, s_Items[s_SubjectIndex]);
        return temp_buffer;
    }
    else if (in_length == 6 && memcmp(in_start, "object", 6)==0)
    {
        strcpy(temp_buffer, s_Items[s_ObjectIndex]);
        return temp_buffer;
    }
    else
        return "UNKNOWN PARAMETER";
}

void CreateButtonsUI(int w, int h)
{
    // Create the UI layout
    AddButton("Text 1 : 1", 10, h-30, 70, 30, s3eKey1);
    AddButton("Text 2 : 2", 90, h-30, 70, 30, s3eKey2);
}

void SurfaceChangedCallback()
{
    DeleteButtons();
    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}

//-----------------------------------------------------------------------------
// The following function initialises the resource manager and IwGxFont. The
// Resource Manager is used to load the group file which points to the font
// resources.
// The font resources are then used to form the font and this font is set to
// be the current font using the IwGxFontSetFont() function.
// A call back is set using the IwGxFontSetParameterCallback() function.
//-----------------------------------------------------------------------------
void ExampleInit()
{
    //Initialise modules
    IwGxRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
    IwResManagerInit();
    IwGxFontInit();

    //Load the group containing the example font
    IwGetResManager()->LoadGroup("IwGxFontBasic.group");

    //Get the example font and set it to be the current font
    CIwGxFont* font = (CIwGxFont*)IwGetResManager()->GetResNamed("arial14", "CIwGxFont");
    IwGxFontSetFont(font);

    //Set the callback
    IwGxFontSetParameterCallback(ExampleCallback);

    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}

//-----------------------------------------------------------------------------
// The following function terminates IwGxFont and the Resource Manager on
// shutdown.
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    //terminate the modules
    IwGxFontTerminate();
    IwResManagerTerminate();
    IwGxUnRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
}

//-----------------------------------------------------------------------------
// The following function tests for keyboard input. Pressing of s3eKey1 causes
// the subject index to be incremented and pressing of s3eKey2 causes the
// object index to be incremented.
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // On key 1 or 2 change the subject or object item
    int32* pIndex = NULL;
    if (CheckButton("Text 1 : 1") & S3E_KEY_STATE_PRESSED )
        pIndex = &s_SubjectIndex;
    else if (CheckButton("Text 2 : 2") & S3E_KEY_STATE_PRESSED )
        pIndex = &s_ObjectIndex;

    // increment the chosen index
    if (pIndex)
    {
        *pIndex = (*pIndex + 1) % (sizeof(s_Items) / sizeof(const char*));
    }

    return true;
}

//-----------------------------------------------------------------------------
// The following function sets the font colour using the IwGxFontSetCol()
// function as well as the position and area alloted to the displayed text using
// the IwGxFontSetRect(). The IwGxFontSetRect() function accepts a CIwRect object
// which defines the rectangle the text will exist within.
// The text that is to be displayed on screen is entered using the
// IwGxFontDrawText() function. The text to be displayed has two words which are
// wrapped in parenthesis. Each set of parenthesis encountered in the input of
// IwGxFontDrawText() results in the callback registered using
// IwGxFontSetParameterCallback() being called.
//-----------------------------------------------------------------------------
void ExampleRender()
{
    //Lighting is required to colour fonts
    IwGxLightingOn();

    //Set font colour to grey
    IwGxFontSetCol(0xff808080);

    //Set the formatting rect - this controls where the text appears and what it is formatted against
    IwGxFontSetRect(CIwRect(10,40,(int16)IwGxGetScreenWidth()-10,(int16)IwGxGetScreenHeight()-50));

    //Draw the text - note we cannot use prepared text to pick up changing parameters
    IwGxFontDrawText("This is text printed using IwGxFont's parameter substitution:\nI saw {subject} jump over {object}.\n\nText 1 to toggle the subject ({subject}) and Text 2 to toggle the object ({object}).");

    //Flush and show
    IwGxFlush();
    IwGxSwapBuffers();
}
