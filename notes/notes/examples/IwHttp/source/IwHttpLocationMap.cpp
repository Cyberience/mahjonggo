/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

//-----------------------------------------------------------------------------
// ExampleIwHTTPLocationMap
//-----------------------------------------------------------------------------

/**
 * @page ExampleIwHTTPLocationMap IwHTTP Location Map Example
 *
 * The following example uses S3E's Location API to display the user's current
 * lattitude, longitude and altitude plus the accuracy of the reading,
 * similarly to the s3eLocation example.
 *
 * The application then attemps to connect to Google Maps to download the map
 * image of the user's location as a gif. This is then displayed to the screen
 * using Iw2DDrawImage().
 *
 * The following graphic illustrates the example output:
 *
 * @image html IwHttpLocationMapImage.png
 *
 * @include IwHttpLocationMap.cpp
 */
#include "ExamplesMain.h"
#include "s3e.h"
#include "s3eLocation.h"
#include "IwHTTP.h"
#include "IwImage.h"
#include "Iw2D.h"
#include "IwGxPrint.h"

s3eLocation gPrevLocation;
s3eLocation gLocation;
s3eResult   gError = S3E_RESULT_ERROR;
CIwHTTP*    gHttpDownloader;
char*       gResult = NULL;
uint32      gResultLen = 0;
uint8*      pImage = 0;
char        gURL[512];
int32       g_Width;
int32       g_Height;
CIw2DImage* g_LocationGIF;

enum GifStatus
{
    kNoGIF,
    kDownloading,
    kOK,
    kError
};

GifStatus   status = kNoGIF;

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    Iw2DInit();

    gHttpDownloader = new CIwHTTP;
    g_LocationGIF = NULL;
    gResult = NULL;
    s3eLocationStart();
}

//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    s3eLocationStop();

    delete gHttpDownloader;
    delete g_LocationGIF;
    delete gResult;

    // Terminate
    Iw2DTerminate();
}

static int32 GotData(void*, void*)
{
    // This is the callback indicating that a ReadDataAsync call has
    // completed.  Either we've finished, or a bigger buffer is
    // needed.  If the correct ammount of data was supplied initially,
    // then this will only be called once. However, it may well be
    // called several times when using chunked encoding.

    // Firstly see if there's an error condition.
    if (gHttpDownloader->GetStatus() == S3E_RESULT_ERROR)
    {
        // Something has gone wrong
        status = kError;
    }
    else if (gHttpDownloader->ContentReceived() != gHttpDownloader->ContentLength())
    {
        // We have some data but not all of it. We need more space.
        uint32 oldLen = gResultLen;

        // If iwhttp has a guess how big the next bit of data is (this
        // basically means chunked encoding is being used), allocate
        // that much space. Otherwise guess.
        if (gResultLen < gHttpDownloader->ContentExpected())
            gResultLen = gHttpDownloader->ContentExpected();
        else
            gResultLen += 1024;

        // Allocate some more space and fetch the data.
        gResult = (char*)s3eRealloc(gResult, gResultLen);
        gHttpDownloader->ReadDataAsync(&gResult[oldLen], gResultLen - oldLen, 0, GotData);
    }
    else
    {
        delete g_LocationGIF;
        g_LocationGIF = NULL;

        // Put the results pointer returned from CIwHTTP::Get into a s3eFile then create an CIwImage
        s3eFile* tempfile = s3eFileOpenFromMemory(gResult, gResultLen);
        CIwImage tempimage;
        tempimage.ReadFile(tempfile);

        // Create an Iw2DImage for painting to the screen.
        if (tempimage.GetFormat() != CIwImage::FORMAT_UNDEFINED)
            g_LocationGIF = Iw2DCreateImage(tempimage);

        if (g_LocationGIF)
            status = kOK;
        else
        {
            status = kError;
        }

        s3eFileClose(tempfile);
    }

    return 0;
}

//-----------------------------------------------------------------------------
// Called when the response headers have been received
//-----------------------------------------------------------------------------
static int32 GotHeaders(void*, void*)
{
    if (gHttpDownloader->GetStatus() == S3E_RESULT_ERROR)
    {
        // Something has gone wrong
        status = kError;
    }
    else
    {
        // Depending on how the server is communicating the content
        // length, we may actually know the length of the content, or
        // we may know the length of the first part of it, or we may
        // know nothing. ContentExpected always returns the smallest
        // possible size of the content, so allocate that much space
        // for now if it's non-zero. If it is of zero size, the server
        // has given no indication, so we need to guess. We'll guess at 1k.
        gResultLen = gHttpDownloader->ContentExpected();

        if (!gResultLen)
            gResultLen = 1024;

        delete gResult;
        gResult = (char*)s3eMalloc(gResultLen + 1);
        gResult[gHttpDownloader->ContentExpected()] = 0;
        gHttpDownloader->ReadDataAsync(gResult,
                                    gHttpDownloader->ContentLength(),
                                    0,
                                    GotData,
                                        NULL);
    }
    return 0;
}

bool ExampleUpdate()
{
    // Get the current location
    gError = s3eLocationGet(&gLocation);

    bool downLoad = false;

    if (gError == S3E_RESULT_SUCCESS)
    {
        if (status == kNoGIF)
            downLoad = true;

        double  dLat = gPrevLocation.m_Latitude - gLocation.m_Latitude;
        double  dLng = gPrevLocation.m_Longitude - gLocation.m_Longitude;

        if (dLat < 0)
            dLat = -dLat;

        if (dLng < 0)
            dLng = -dLng;

        if (status == kOK && (dLat > 0.00001 || dLng  > 0.00001))
        {
            s3eFree(gResult);
            delete g_LocationGIF;
            gResult = 0;
            g_LocationGIF = NULL;
            downLoad = true;
            status = kNoGIF;
        }
    }
    else
    {
        gLocation.m_Latitude = 51;
        gLocation.m_Longitude = -0.1;

        double  dLat = gPrevLocation.m_Latitude - gLocation.m_Latitude;
        double  dLng = gPrevLocation.m_Longitude - gLocation.m_Longitude;

        if (dLat < 0)
            dLat = -dLat;

        if (dLng < 0)
            dLng = -dLng;

        if (status == kNoGIF)
            downLoad = true;

        if (status == kOK && (dLat > 0.00001 || dLng  > 0.00001))
        {
            gResult = 0;
            s3eFree(gResult);
            downLoad = true;
            status = kNoGIF;
        }
    }

    if (downLoad)
    {
        s3eDebugTracePrintf("downloading google maps GIF for latitude %f longitude %f\n", (float)gLocation.m_Latitude, (float)gLocation.m_Longitude);


        gPrevLocation = gLocation;

        g_Width = s3eSurfaceGetInt(S3E_SURFACE_WIDTH);
        g_Height = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT)/2;

        //Limit height and width sensibily in case we're on a large screen
        if (g_Width > 512)
            g_Width = 512;

        if (g_Height > 512)
            g_Height = 512;



        snprintf(   gURL,
                    sizeof(gURL),
                    "http://maps.google.com/maps/api/staticmap?center=%f,%f&zoom=14&size=%dx%d&markers=%f,%f&sensor=true",
                    gLocation.m_Latitude,
                    gLocation.m_Longitude,
                    g_Width,
                    g_Height,
                    gLocation.m_Latitude,
                    gLocation.m_Longitude);

        s3eDebugTracePrintf("URL: %s\n", gURL);

        status = kDownloading;
        gHttpDownloader->Get(gURL, GotHeaders, NULL);
    }

    return true;
}

//-----------------------------------------------------------------------------
// The following function displays the current time.
//-----------------------------------------------------------------------------
void ExampleRender()
{
    if (status == kOK)
    {
        // Clear the screen
        Iw2DSurfaceClear(0xffffffff);

        //Calculate the top left of the map image
        CIwFVec2 topLeft = CIwFVec2((int16)(Iw2DGetSurfaceWidth() / 2 - g_LocationGIF->GetWidth() / 2),
            (int16)(Iw2DGetSurfaceHeight() / 2 - g_LocationGIF->GetHeight() / 2));

        Iw2DDrawImage(g_LocationGIF, topLeft, CIwFVec2(g_LocationGIF->GetWidth(), g_LocationGIF->GetHeight()));

        Iw2DFinishDrawing();

        const int   textHeight = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);
        int         y = 10;
        char        buf[256];

        if (gError == S3E_RESULT_ERROR)
        {
            IwGxPrintString(10, y, "`x666666Could not determine location!", 0);
            return;
        }

        snprintf(buf, sizeof(buf), "`x666666Latitude %f", (float)gLocation.m_Latitude);
        IwGxPrintString(10, y, buf, 0);
        y += textHeight;

        snprintf(buf, sizeof(buf), "`x666666Longitude %f", (float)gLocation.m_Longitude);
        IwGxPrintString(10, y, buf, 0);
        y += textHeight;

        snprintf(buf, sizeof(buf), "`x666666Altitude %f", (float)gLocation.m_Altitude);
        IwGxPrintString(10, y, buf, 0);
        y += textHeight;

        snprintf(buf, sizeof(buf), "`x666666H accuracy %f", (float)gLocation.m_HorizontalAccuracy);
        IwGxPrintString(10, y, buf, 0);
        y += textHeight;

        snprintf(buf, sizeof(buf), "`x666666V accuracy %f", (float)gLocation.m_VerticalAccuracy);
        IwGxPrintString(10, y, buf, 0);
        y += textHeight;

    }
    else
    {
        const int   textHeight = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);
        int         y = 50;
        char        buf[256];

        if (gError == S3E_RESULT_ERROR)
        {
            IwGxPrintString(10, y, "`x666666Could not determine location!", 0);
            Iw2DSurfaceShow();
            return;
        }

        snprintf(buf, sizeof(buf), "`x666666Latitude %f", (float)gLocation.m_Latitude);
        IwGxPrintString(10, y, buf, 0);
        y += textHeight;

        snprintf(buf, sizeof(buf), "`x666666Longitude %f", (float)gLocation.m_Longitude);
        IwGxPrintString(10, y, buf, 0);
        y += textHeight;

        snprintf(buf, sizeof(buf), "`x666666Altitude %f", (float)gLocation.m_Altitude);
        IwGxPrintString(10, y, buf, 0);
        y += textHeight;

        snprintf(buf, sizeof(buf), "`x666666H accuracy %f", (float)gLocation.m_HorizontalAccuracy);
        IwGxPrintString(10, y, buf, 0);
        y += textHeight;

        snprintf(buf, sizeof(buf), "`x666666V accuracy %f", (float)gLocation.m_VerticalAccuracy);
        IwGxPrintString(10, y, buf, 0);
        y += textHeight;

        if (status == kDownloading)
            IwGxPrintString(10, y, "`x666666downloading GIF", 0);
        else
        {
            if (status == kError)
            {
                if (gResult)
                {
                    IwGxPrintString(10, y, "`x666666Server returned error message:", 0);
                    IwGxPrintString(10, y+10, gResult, 1);
                }
                else
                    IwGxPrintString(10, y, "`x666666error downloading GIF", 0);
            }
        }
    }

    // Show the surface
    Iw2DSurfaceShow();
}
