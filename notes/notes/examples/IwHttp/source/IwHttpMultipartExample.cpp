/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwHTTPMultipart IwHTTP Multipart Example
 *
 * This is an example of the use of the IwHTTP HTTP component.
 * It retreives a page and then displays it.
 * It uses CIwHTTP::Get() to start the request, then CIwHTTP::ReadDataAsync
 * to actually receive the content.
 *
 * A note about the length of the content:
 *
 * HTTP has several different ways of specifying the content
 * length. It can specify the length with the response headers, it can
 * just close the socket at the end or it can provide the content in
 * several 'chunks', each of which has a content length
 * specified. Unfortunately, giving the application developer the
 * control over memory allocation that's often needed on mobile means
 * that this complexity must be exposed to the client. There are 3
 * related functions that indicate the length of the
 * content. ContentLength() returns the total length of the content if
 * known, or 0 if it isn't known. ContentExpected() returns the length
 * of everything we're certain to be getting, which could be just the
 * chunks that we're expecting, or could be 0 if the server is
 * indicating the length by closing the socket. ContentReceived()
 * indicates the ammount of data so far delivered to the client.
 *
 * The following graphic illustrates the example output:
 *
 * @image html IwHttpExampleImage.png
 *
 * @include IwHttpExample.cpp
 */

//-----------------------------------------------------------------------------

#include "IwHttpExample.h"
#include <string>
#include "s3eMemory.h"
#include "ExamplesMain.h"
#include "IwGx.h"
#include "IwGxPrint.h"
#include "IwHttpExampleUtils.h"

enum HTTPStatus
{
    kNone,
    kUploading,
    kOK,
    kError,
};

#define HTTP_URI "http://sdktest.madewithmarmalade.com/post.php"
#define HTTPS_URI "https://sdktest.madewithmarmalade.com/post.php"

CIwHTTP *theHttpObject;
char* result = NULL, *secureResult = NULL;
uint32 len;
HTTPStatus status = kNone;
bool secureTest = false;

static int32 GotData(void*, void*)
{
    // This is the callback indicating that a ReadDataAsync call has
    // completed.  Either we've finished, or a bigger buffer is
    // needed.  If the correct ammount of data was supplied initially,
    // then this will only be called once. However, it may well be
    // called several times when using chunked encoding.

    // Firstly see if there's an error condition.
    if (theHttpObject->GetStatus() == S3E_RESULT_ERROR)
    {
        // Something has gone wrong
        status = kError;
    }
    else if (theHttpObject->ContentReceived() != theHttpObject->ContentLength())
    {
        // We have some data but not all of it. We need more space.
        uint32 oldLen = len;
        // If iwhttp has a guess how big the next bit of data is (this
        // basically means chunked encoding is being used), allocate
        // that much space. Otherwise guess.
        if (len < theHttpObject->ContentExpected())
            len = theHttpObject->ContentExpected();
        else
            len += 1024;

        // Allocate some more space and fetch the data.
        result = (char*)s3eRealloc(result, len);
        theHttpObject->ReadDataAsync(&result[oldLen], len - oldLen, 0, GotData);
    }
    else
    {
        // We've got all the data. Display it.
        status = kOK;
    }
    return 0;
}

//-----------------------------------------------------------------------------
// Called when the response headers have been received
//-----------------------------------------------------------------------------
static int32 GotHeaders(void*, void*)
{
    if (theHttpObject->GetStatus() == S3E_RESULT_ERROR)
    {
        // Something has gone wrong
        status = kError;
    }
    else
    {
        // Depending on how the server is communicating the content
        // length, we may actually know the length of the content, or
        // we may know the length of the first part of it, or we may
        // know nothing. ContentExpected always returns the smallest
        // possible size of the content, so allocate that much space
        // for now if it's non-zero. If it is of zero size, the server
        // has given no indication, so we need to guess. We'll guess at 1k.
        len = theHttpObject->ContentExpected();
        if (!len)
        {
            len = 1024;
        }

        char **resultBuffer = secureTest ? &secureResult: &result;

        s3eFree(*resultBuffer);
        *resultBuffer = (char*)s3eMalloc(len + 1);
        (*resultBuffer)[len] = 0;
        theHttpObject->ReadDataAsync(*resultBuffer, len, 0, GotData, NULL);
    }

    return 0;
}


//-----------------------------------------------------------------------------
void ExampleInit()
{
    IwGxInit();
}

//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    if (theHttpObject)
        theHttpObject->Cancel();
    delete theHttpObject;
    s3eFree(result);
    s3eFree(secureResult);

    IwGxTerminate();
}

bool ExampleUpdate()
{
    if (status == kNone)
    {
        theHttpObject = new CIwHTTP;

        theHttpObject->SetPostChunkedMode(true);
        theHttpObject->SetFormData("test", "Mary had a little lamb");
        theHttpObject->SetFormDataFile("file", "test.txt", "test.txt", "text/plain");
        if (theHttpObject->Post(HTTP_URI, "", 0, GotHeaders, NULL) == S3E_RESULT_SUCCESS)
            status = kUploading;
    }
    else if (status == kOK && !secureTest)
    {
        secureTest = true;

        char buf[256];
        const char *passString = "tobe:ryloth";
        char *base64;
        base64 = base64_encode(passString, strlen(passString), NULL);
        snprintf(buf, 256, "Basic %s", base64);
        free(base64);

        theHttpObject->SetPostChunkedMode(true);

        theHttpObject->SetRequestHeader("Authorization", buf);
        theHttpObject->SetRequestHeader("Cache-Control", "max-age=0");
        theHttpObject->SetRequestHeader("Accept", "application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,* /*;q=0.5");
        theHttpObject->SetRequestHeader("Accept-Encoding", "gzip,deflate,sdch");
        theHttpObject->SetRequestHeader("Accept-Language", "en-GB");
        theHttpObject->SetRequestHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.3");

        theHttpObject->SetFormData("test", "Mary had a little lamb");
        theHttpObject->SetFormDataFile("file", "test.txt", "test.txt", "text/plain");

        if (theHttpObject->Post(HTTPS_URI, NULL, 0, GotHeaders, NULL) == S3E_RESULT_SUCCESS)
            status = kUploading;
    }

    return true;
}

//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear screen
    IwGxClear( IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F );

    // Render text
    if (result || secureResult)
    {
        char buf[4096];

        if (result)
        {
            snprintf(buf, 4096, "http:// -> %s", result);
            IwGxPrintString(10, 50, buf, 1);
        }

        if (secureResult)
        {
            snprintf(buf, 4096, "https:// -> %s", secureResult);
            IwGxPrintString(10, 80, buf, 1);
        }
    }
    else if (status == kError)
    {
        if (!secureTest)
            IwGxPrintString(10, 50, "Error!", 1);
        else
            IwGxPrintString(10, 80, "Error!", 1);
    }
    else
    {
        if (!secureTest)
            IwGxPrintString(10, 50, "Connecting...", 1);
        else
            IwGxPrintString(10, 80, "Connecting...", 1);
    }

    // Swap buffers
    IwGxFlush();
    IwGxSwapBuffers();
}
