/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwHTTPPost IwHTTP Post Example
 *
 * This is an example of the use of the IwHTTP HTTP component.
 * It sends simple data to a test server.
 * The primary call is CIwHTTP::Post().
 *
 * @include IwHttpPostExample.cpp
 */

//-----------------------------------------------------------------------------

#include "IwHttpExample.h"
#include <string>
#include "s3eMemory.h"
#include "ExamplesMain.h"
#include "IwGx.h"
#include "IwGxPrint.h"
#include "IwHttpExampleUtils.h"

enum HTTPStatus
{
    kNone,
    kUploading,
    kOK,
    kError,
};
enum HTTPTest
{
    kPost,
    kSecure,
};

#define HTTP_URI "http://posttestserver.com/post.php?dir=marmalade+" IW_APP_NAME
#define HTTPS_URI "https://posttestserver.com/post.php?dir=marmalade+" IW_APP_NAME

static const char* post_data = "moo moo moo moo"; // Alternative: "Name=Gareth+Wylie&Age=24&Formula=a+%2B+b+%3D%3D+13%25%21";
static const char* post_format = "application/x-www-form-urlencoded";

CIwHTTP *theHttpObject;
HTTPStatus status = kNone;
HTTPTest testType = kPost;
unsigned int response_code = 0;
unsigned int secure_response_code = 0;

static int32 PostComplete(void* systemData, void* userData)
{
    // This is the callback indicating that a Post call has
    // completed.

    // Firstly see if there's an error condition.
    if (theHttpObject->GetStatus() == S3E_RESULT_ERROR)
    {
        // Something has gone wrong
        status = kError;
    }
    else
    {
        // We've sent all the data.
        status = kOK;
    }

    if (testType == kPost)
        response_code = theHttpObject->GetResponseCode();
    else
        secure_response_code = theHttpObject->GetResponseCode();
    return 0;
}



//-----------------------------------------------------------------------------
void ExampleInit()
{
    IwGxInit();
}

//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    if (theHttpObject)
        theHttpObject->Cancel();
    delete theHttpObject;

    IwGxTerminate();
}

// From this point on, the code is general housekeeping functions
// common to other examples.

static char buffer[1024];

bool ExampleUpdate()
{
    if (status == kNone)
    {
        theHttpObject = new CIwHTTP;
        testType = kPost;
        status = kUploading;
        sprintf(buffer, "Data: %s\r\nSection:%s\r\n", post_data, "http"); // Note: extra Data: and Section: just part of example. Not required as such.
        theHttpObject->SetRequestHeader("Content-Type", post_format);
        if (theHttpObject->Post(HTTP_URI, buffer, strlen(buffer), PostComplete, (void*) "http") == S3E_RESULT_ERROR) // pass "http" just to help callback
        {
            return false;
        }
    }
    else if (status == kOK && testType == kPost)
    {
        testType = kSecure;
        status = kUploading;
        sprintf(buffer, "Data: %s\r\nSection:%s\r\n", post_data, "https");
        theHttpObject->SetRequestHeader("Content-Type", post_format);
        if (theHttpObject->Post(HTTPS_URI, buffer, strlen(buffer), PostComplete, (void*) "https") == S3E_RESULT_ERROR) // pass "https" just to help callback
        {
            return false;
        }
    }
    else if (status == kOK && testType == kSecure)
    {
        // return false; // comment in for auto-exit
    }

    return true;
}

//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear screen
    IwGxClear( IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F );

    // Render text
    char buf[512];

    if (response_code)
    {
        snprintf(buf, 512, "http:// -> %d", response_code);
        IwGxPrintString(10, 40, buf, 1);
    }
    if (secure_response_code)
    {
        snprintf(buf, 512, "https:// -> %d", secure_response_code);
        IwGxPrintString(10, 80, buf, 1);
    }
    if (status == kError)
    {
        switch (testType)
        {
            case kPost:
                IwGxPrintString(10, 40, "Error!", 1);
                break;
            case kSecure:
                IwGxPrintString(10, 80, "Error!", 1);
                break;
            default:
                break;
        }
    }
    switch (testType)
    {
        case kPost:
            if (!response_code)
                IwGxPrintString(10, 40, "Connecting...", 1);
            break;
        case kSecure:
            if (!secure_response_code)
                IwGxPrintString(10, 80, "Connecting...", 1);
            break;
        default:
            break;
    }

    // Swap buffers
    IwGxFlush();
    IwGxSwapBuffers();
}
