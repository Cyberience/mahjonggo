#category: Network
IwHttpExample
=============

The IwHttpExample provides a simple introduction to the IwHttp API.
An HTTP GET request is made to a URL and the response from the
server is then retrieved and displayed on screen.
