/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
#include "EGL/egl.h"
#include "GLES/gl.h"
#include "KD/kd.h"

EGLContext init(EGLDisplay egldpy)
{
    EGLint n, vid;
    EGLConfig configs[1];
    EGLConfig eglconfig;
    EGLSurface surface;
    EGLContext eglctx;
    KDWindow* w;
    EGLNativeWindowType nwt = 0;

    static int attribs[] = {EGL_RED_SIZE, 1, EGL_DEPTH_SIZE, 1, EGL_NONE};

    if (!eglChooseConfig(egldpy, attribs, configs, 1, &n))
        return NULL;

    eglconfig = configs[0];
    eglGetConfigAttrib(egldpy, configs[0], EGL_NATIVE_VISUAL_ID, &vid);

    w = kdCreateWindow(egldpy, eglconfig, NULL);
    kdRealizeWindow(w, &nwt);

    surface = eglCreateWindowSurface(egldpy, eglconfig, nwt, 0);
    eglctx = eglCreateContext(egldpy, eglconfig, NULL, NULL);
    if (!eglMakeCurrent(egldpy, surface, surface, eglctx))
        return NULL;
    return surface;
}

KDint kdMain(KDint argc, const KDchar* const* argv)
{
    EGLDisplay display = eglGetDisplay(NULL);
    EGLSurface surface;
    const static GLfloat v[] = {
        0.25, 0.25, 0.0,
        0.75, 0.25, 0.0,
        0.25, 0.75, 0.0,
        0.75, 0.75, 0.0
    };

    eglInitialize(display, NULL, NULL);
    surface = init(display);

    /* select clearing color    */
    glClearColor(0.0, 0.0, 0.0, 0.0);

    /* initialize viewing values  */
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrthof(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
    glVertexPointer(3, GL_FLOAT, 0, v);
    glEnableClientState(GL_VERTEX_ARRAY);

    while (1)
    {
        /* clear all pixels  */
        glClear(GL_COLOR_BUFFER_BIT);

        /* draw white polygon (rectangle) with corners at
         * (0.25, 0.25, 0.0) and (0.75, 0.75, 0.0)
         */
        glColor4f(1.0, 1.0, 1.0, 1.0);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

        /* don't wait!
         * start processing buffered OpenGL routines
         */
        glFlush();
        eglSwapBuffers(display, surface);
        kdDefaultEvent(kdWaitEvent(1000000));
    }
    return 0;
}
