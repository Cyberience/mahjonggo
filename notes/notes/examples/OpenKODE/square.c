/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
#include "KD/kd.h"
#include "EGL/egl.h"
#include "EGL/eglext.h"

// Colours to use on screen
#define BACKGROUND 0x0      // used as an 8-bit value in kdMemset, to fill 16-bit pixels
#define FOREGROUND 0xffff   // 16-bit colour for square

#define SIZE 5              // width and height of square in pixels
#define STEP 5              // movement step in pixels

// EGL/windowing things
KDWindow*  window;

EGLDisplay egl_display;
EGLConfig  egl_config;
EGLSurface egl_surface;
EGLContext egl_context;

// Pointers to the EGL extension functions
PFNEGLLOCKSURFACEKHRPROC* egl_lock_surface;
PFNEGLUNLOCKSURFACEKHRPROC* egl_unlock_surface;

// Details of the EGL surface
EGLint width;
EGLint height;
EGLint origin;
EGLint pointer;
EGLint pitch;

// The location of the 'ball'
KDint x = 0;
KDint y = 0;

static void makeWindow()
{
    EGLint number_of_configs;
    EGLint attrib_list[] = {EGL_SURFACE_TYPE, EGL_WINDOW_BIT | EGL_LOCK_SURFACE_BIT_KHR,
                            EGL_RENDERABLE_TYPE, EGL_DONT_CARE,
                            EGL_MATCH_FORMAT_KHR, EGL_FORMAT_RGB_565_EXACT_KHR,
                            EGL_NONE};
    EGLNativeWindowType window_type;

    // Get the display and initialise EGL
    egl_display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
    eglInitialize(egl_display, KD_NULL, KD_NULL);

    // Find out the addresses of the lock/unlock functions we will need later
    egl_lock_surface = (PFNEGLLOCKSURFACEKHRPROC*)eglGetProcAddress("eglLockSurfaceKHR");
    egl_unlock_surface = (PFNEGLUNLOCKSURFACEKHRPROC*)eglGetProcAddress("eglUnlockSurfaceKHR");

    eglChooseConfig(egl_display, &attrib_list[0], &egl_config, 1, &number_of_configs);

    // Create a window and an EGL surface for it
    window = kdCreateWindow(egl_display, egl_config, KD_NULL);
    kdRealizeWindow(window, &window_type);
    egl_surface = eglCreateWindowSurface(egl_display, egl_config, window_type, KD_NULL);

    // Find out about that surface
    eglQuerySurface(egl_display, egl_surface, EGL_WIDTH, &width);
    eglQuerySurface(egl_display, egl_surface, EGL_HEIGHT, &height);
    eglQuerySurface(egl_display, egl_surface, EGL_BITMAP_ORIGIN_KHR, &origin);
}

void key(const KDEvent *event)
{
    // Ignore key release events
    if (event->data.input.value.i == 0)
        return;

    // Now see if it's an interesting key
    // and if it is, handle it
    switch (event->data.input.index)
    {
    case KD_INPUT_GAMEKEYSNC_UP:
        if (y)
            y -= STEP;
        break;

    case KD_INPUT_GAMEKEYSNC_DOWN:
        if (y < height - SIZE)
            y += STEP;
        break;

    case KD_INPUT_GAMEKEYSNC_LEFT:
        if (x)
            x -= STEP;
        break;

    case KD_INPUT_GAMEKEYSNC_RIGHT:
        if (x < width - SIZE)
            x += STEP;
        break;

    default:
        break;
    }
}

KDint kdMain(KDint argc, const KDchar* const* argv)
{
    KDint i,j;

    makeWindow();
    x = width / 2;
    y = height / 2;

    // Register a callback to do the key processing
    kdInstallCallback(key, KD_EVENT_INPUT, KD_NULL);

    while (1)
    {
        // Lock the surface
        (*egl_lock_surface)(egl_display, egl_surface, KD_NULL);                     // Lock the surface, to write to it
        eglQuerySurface(egl_display, egl_surface, EGL_BITMAP_POINTER_KHR, &pointer); // Now get the pointer that we can write to
        eglQuerySurface(egl_display, egl_surface, EGL_BITMAP_PITCH_KHR, &pitch);    // And the line pitch

        // Clear the screen
        for (j = 0; j < height; j++)
            kdMemset((void*)(pointer + pitch * j), BACKGROUND, width * sizeof(KDuint16));

        // Then draw a 'ball' on it
        for (i = 0; i < SIZE; i++)
            for (j = 0; j < SIZE; j++)
                *(KDuint16*)(pointer + (y + j) * pitch + (x + i) * sizeof(KDuint16)) = FOREGROUND;

        // Unlock the surface and swap buffers
        (*egl_unlock_surface)(egl_display, egl_surface);
        eglSwapBuffers(egl_display, egl_surface);

        kdDefaultEvent(kdWaitEvent(1 * 1000 * 1000 * 1000));
    }
    return 0;
}
