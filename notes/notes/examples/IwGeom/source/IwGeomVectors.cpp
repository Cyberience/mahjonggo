/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwGeomVectors Vectors Example
 *
 * The following example demonstrates how to use the IwGeom Types
 * functionality. The example renders a rotating triangle and its normal.
 * The colour of the triangle depends on the angle its normal makes
 * with the eye.
 *
 * The main classes used to achieve this are:
 *  - CIwFVec3
 *  - CIwFMat
 *  - CIwColour
 *
 * The main functions used to achieve this are:
 *  - CIwFVec3::Normalise()
 *  - CIwFVec3::GetNormalised()
 *  - CIwFVec3::operator +
 *  - CIwFVec3::operator /    [scale]
 *  - CIwFVec3::operator *    [scale]
 *  - CIwFVec3::operator *(CIwVec3)    [dot product]
 *  - CIwFVec3::operator ^             [cross product]
 *  - CIwColour::Set()
 *  - CIwFMat::SetIdentity()
 *  - CIwFMat::SetAxisAngle()
 *  - CIwFMat::RotateVec()
 *  - IwGxDebugPrimTri()
 *  - IwGxSetViewMatrix()
 *
 * The example creates a triangle structure containing a number
 * of CIwFVec3 objects to hold the three points of the triangle as well
 * as the triangle normal and the centre point. The structure is used
 * to create the triangle and for every render loop the triangle is
 * rotated and has its normal calculated.
 *
 * @note For more information on the various types,
 * see the @ref types "Types" section in the <i>IwGeom API Documentation</i>.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwGeomVectorsImage.png
 *
 * @include IwGeomVectors.cpp
 */

#include "IwGeom.h"

// for primitive rendering
#include "IwGx.h"
#include "IwDebugPrim.h"

struct Triangle
{
    // vertices
    CIwFVec3     m_V0, m_V1, m_V2;

    // normal of the triangle
    CIwFVec3    m_Normal;

    // centre of the triangle
    CIwFVec3     m_Centre;

    CIwColour   m_Colour;
};

struct Line
{
    CIwFVec3     m_V0;
    CIwFVec3     m_V1;
};

CIwFSphere s_Sphere;
Triangle s_Triangle;
Line s_Line;


// the angular speed of the triangle
#define TRIANGLE_ROT    PI*0x40/2048.0f

// calculate and set the normal, centre and colour of the triangle
void UpdateTriangle()
{
    // the normal of the triangle is the normal of the result of the cross
    // product of two of its edges (the order of the cross product determines
    // the orientation of the triangle)
    CIwFVec3 edge1 = s_Triangle.m_V1 - s_Triangle.m_V0;
    CIwFVec3 edge2 = s_Triangle.m_V2 - s_Triangle.m_V0;
    s_Triangle.m_Normal = (edge1 ^ edge2).GetNormalised();

    // the centre of the triangle is the average of the sum of its vertices
    s_Triangle.m_Centre = (s_Triangle.m_V0 + s_Triangle.m_V1 + s_Triangle.m_V2) / 3.0f;

    // viewVector is a normal vector pointing along the positive z axis
    CIwFVec3 viewVector(0, 0, 1.0f);

    // calculate the dot product of the triangle normal and viewVector;
    // the result will be the cosine of the angle between them
    // which can be used to scale the red component of the triangle colour
    float dot = s_Triangle.m_Normal * viewVector;

    // set the triangle colour based on the calculated angle
    s_Triangle.m_Colour.Set(abs((int)(255.0f*dot)), 255, 255);
}

void ExampleInit()
{
    // initialise GX
    IwGxInit();

    // initialise triangle
    s_Triangle.m_V0 = CIwFVec3(0, 0, 0);
    s_Triangle.m_V1 = CIwFVec3(0, -0.3f, 0);
    s_Triangle.m_V2 = CIwFVec3(-0.3f, 0, 0);

    UpdateTriangle();
}

void ExampleShutDown()
{
    // terminate GX
    IwGxTerminate();
}

bool ExampleUpdate()
{
    // (arbitrarily) rotate the triangle vertices:

    // define the rotation axis
    CIwFVec3 rotationAxis(1.0f, -1.0f, 0);

    // ensure the axis is normal
    rotationAxis.Normalise();

    // create a rotation matrix, defined by rotationAxis and TRIANGLE_ROT:
    CIwFMat rot;
    rot.SetAxisAngle(rotationAxis, TRIANGLE_ROT);

    s_Triangle.m_V0 = rot.RotateVec(s_Triangle.m_V0);
    s_Triangle.m_V1 = rot.RotateVec(s_Triangle.m_V1);
    s_Triangle.m_V2 = rot.RotateVec(s_Triangle.m_V2);

    UpdateTriangle();

    return true;
}

void ExampleRender()
{
    // clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // start with no lighting
    IwGxLightingOff();

    IwGxSetFarZNearZ(100.0f, 0.01f);

    // the IwGxDebugPrim* functions are used to render primitives in wireframe
    // (only available in debug builds)

    // when rendering we translate the view so that the primitives are visible:

    CIwFMat view;

    // set to the identity rotation matrix with zero translation component
    view.SetIdentity();

    // set the translation vector of the matrix such that the primitives are visible
    view.t.z = -0.7f;

    // set the view matrix
    IwGxSetViewMatrix(&view);

    // render the triangle
    IwGxDebugPrimTri(s_Triangle.m_V0, s_Triangle.m_V1, s_Triangle.m_V2, s_Triangle.m_Colour);

    // render the triangle normal
    IwGxDebugPrimLine(s_Triangle.m_Centre, s_Triangle.m_Centre + (s_Triangle.m_Normal * 0.1f));

    IwGxFlush();

    // swap buffers
    IwGxSwapBuffers();
}
