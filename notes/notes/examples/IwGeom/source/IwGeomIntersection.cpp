/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwGeomIntersection Intersection Example
 *
 * Illustrates some of the geometry intersection functions by rotating a ray such that it
 * intersects with a sphere and triangle
 *
 * Procedure:
 * - initialise triangle, sphere and ray
 * - rotate the ray direction every frame, checking for intersections with the ray
 *   and the sphere and triangle
 * - set the triangle and sphere colours to red if there's an intersection and black if not
 *
 * Main classes:
 * - CIwFVec3
 * - CIwFMat
 * - CIwFSphere
 *
 * Main functions:
 * - IwIntersectLineTriNorm
 * - IwIntersectLineSphere
 * - CIwFVec3::GetNormalised
 * - CIwFVec3 operators
 * - CIwFMat::SetIdentity
 * - CIwFMat::SetRot*
 * - CIwFMat::RotateVec
 * - IwGxDebugPrimTri
 * - IwGxDebugPrimSphere
 * - IwGxDebugPrimLine
 * - IwGxSetViewMatrix
 */

 #include "IwGeom.h"

// for primitive rendering
#include "IwGx.h"
#include "IwDebugPrim.h"

struct Triangle
{
    // vertices
    CIwFVec3     m_V0, m_V1, m_V2;

    // normal of the triangle
    CIwFVec3    m_Normal;

    // colour of the triangle
    CIwColour   m_Colour;
};

struct Ray
{
    // origin of the ray
    CIwFVec3     m_Origin;

    // direction of the ray
    CIwFVec3     m_Direction;
};

struct ColourSphere
{
    CIwFSphere   m_Sphere;
    CIwColour   m_Colour;
};

// primitives:
ColourSphere    s_CSphere;
Triangle        s_Triangle;
Ray             s_Ray;

// the angular speed of the ray
#define RAY_ROT PI*0x20/2048.0

void ExampleInit()
{
    // initialise GX
    IwGxInit();

    // initialise triangle:
    s_Triangle.m_V0 = CIwFVec3(-0.1f, 0.1f, 0.1f) * 4096;
    s_Triangle.m_V1 = CIwFVec3(-0.1f, -0.1f, 0.1f) * 4096;
    s_Triangle.m_V2 = CIwFVec3(-0.3f, 0.1f, -0.1f) * 4096;

    // set the triangle normal:

    // the normal of the triangle is the normal of the result of the cross
    // product of two of its edges (the order of the cross product determines
    // the orientation of the triangle)
    CIwFVec3 edge1 = s_Triangle.m_V1 - s_Triangle.m_V0;
    CIwFVec3 edge2 = s_Triangle.m_V2 - s_Triangle.m_V0;
    s_Triangle.m_Normal = (edge1 ^ edge2).GetNormalised();

    // initialise ray:
    s_Ray.m_Origin = CIwFVec3::g_Zero;
    s_Ray.m_Direction = CIwFVec3(0, 0, -1) * 4096.0f;

    // initialise sphere:
    s_CSphere.m_Sphere.SetRadius(0.3f*4096);
    s_CSphere.m_Sphere.t = CIwFVec3(0.3f, 0, 0.3f) * 4096;
}

void ExampleShutDown()
{
    // terminate GX
    IwGxTerminate();
}

bool ExampleUpdate()
{
    // create a rotation matrix, defined by a rotation of RAY_ROT about the y axis
    CIwFMat rot;
    rot.SetRotY(RAY_ROT);

    // rotate the ray direction
    s_Ray.m_Direction = rot.RotateVec(s_Ray.m_Direction).GetNormalised();

    // set the colour of the triangle based on whether it intersects the ray
    // (intersected => red, otherwise white):
    float f;
    if (IwIntersectLineTriNorm( s_Ray.m_Origin,
                                s_Ray.m_Direction,
                                s_Triangle.m_V0,
                                s_Triangle.m_V1,
                                s_Triangle.m_V2,
                                s_Triangle.m_Normal,
                                f,
                                false) )
    {
        s_Triangle.m_Colour = IwGxGetColFixed(IW_GX_COLOUR_RED);
    }
    else
    {
        s_Triangle.m_Colour = IwGxGetColFixed(IW_GX_COLOUR_BLACK);
    }

    // set the colour of the sphere based on whether it intersects the ray:
    // (intersected => red, otherwise white):
    if (IwIntersectLineSphere(  s_Ray.m_Origin,
                                s_Ray.m_Direction,
                                s_CSphere.m_Sphere  )   )
    {
        s_CSphere.m_Colour = IwGxGetColFixed(IW_GX_COLOUR_RED);
    }
    else
    {
        s_CSphere.m_Colour = IwGxGetColFixed(IW_GX_COLOUR_BLACK);
    }

    return true;
}

void ExampleRender()
{
    // clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // no lighting
    IwGxLightingOff();

    // the IwGxDebugPrim* functions are used to render primitives in wireframe
    // (only available in debug builds)

    // when rendering we translate and orientate the view such that the primitives are visible:

    CIwFMat view;

    // set the matrix to represent a rotation about the x axis
    view.SetRotX(PI*0x120/2048.0);

    // set the translation vector of the matrix
    view.t.z = -0.7f*4096;
    view.t.y = -0.4f*4096;
    view.t.x = 0.1f*4096;

    // set the view matrix
    IwGxSetViewMatrix(&view);

    // render the triangle
    IwGxDebugPrimTri(s_Triangle.m_V0, s_Triangle.m_V1, s_Triangle.m_V2, s_Triangle.m_Colour);

    // render the sphere
    IwGxDebugPrimSphere(s_CSphere.m_Sphere, s_CSphere.m_Colour);

    // render the ray
    IwGxDebugPrimLine(s_Ray.m_Origin, s_Ray.m_Origin + (s_Ray.m_Direction * 0.4f*4096));

    IwGxFlush();

    // swap buffers
    IwGxSwapBuffers();
}

