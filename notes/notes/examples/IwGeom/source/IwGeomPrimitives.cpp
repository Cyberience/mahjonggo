/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwGeomPrimitives Primitives Example
 *
 * The following example demonstrates how to use the IwGeom bounding
 * box functionality. The example renders a sphere, a triangle and
 * a bounding box which bounds the entire triangle and the centre of
 * the sphere.
 *
 * The main classes used to achieve this are:
 *  - CIwFVec3
 *  - CIwFMat
 *  - CIwFSphere
 *  - CIwFBBox
 *
 * The main functions used to achieve this are:
 *  - CIwFSphere::SetRadius
 *  - CIwFBBox::BoundVec
 *  - CIwFMat::SetIdentity
 *  - IwGxDebugPrimSphere
 *  - IwGxDebugPrimTri
 *  - IwGxDebugPrimBBox
 *  - IwGxSetViewMatrix
 *
 * The example creates a sphere primitive and a triangle primitive,
 * and then bounds these items using the CIwFBBox::BoundVec to set the
 * bounding box values. To display these items the example sets the
 * view matrix and renders the sphere, triangle and bounding box using
 * the IwGxDebugPrimSphere(), IwGxDebugPrimTri() and IwGxDebugPrimBBox() functions.
 *
 * @note For more information on using bounding boxes,
 * see the @ref boundingbox "Bounding Box" section in the <i>IwGeom API Documentation</i>.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwGeomPrimitivesImage.png
 *
 * @include IwGeomPrimitives.cpp
 */

#include "IwGeom.h"

// for primitive rendering
#include "IwGx.h"
#include "IwDebugPrim.h"

struct Triangle
{
    // vertices
    CIwFVec3 m_V0, m_V1, m_V2;
};

// primitives:
// floating point sphere
CIwSphere s_Spherex;
CIwFSphere s_Sphere;

// floating point triangle
Triangle s_Triangle;

// initialise the bounding box with the zero vector
CIwFBBox s_BoundingBox(CIwFVec3::g_Zero, CIwFVec3::g_Zero);

void ExampleInit()
{
    // initialise GX
    IwGxInit();

    // initialise sphere:
    s_Sphere.SetRadius(0.2f);
    s_Sphere.t = CIwFVec3(0.1f * 4096.0f, 0.1f * 4096.0f, 0.3f * 4096.0f);

    // initialise triangle
    s_Triangle.m_V0 = CIwFVec3(0, 0, 0);
    s_Triangle.m_V1 = CIwFVec3(0, -0.3f * 4096.0f, 0);
    s_Triangle.m_V2 = CIwFVec3(-0.3f * 4096.0f, 0, 0);

    // setup bounding box:
    // bound the centre of the sphere
    s_BoundingBox.BoundVec((CIwFVec3*) &s_Sphere.GetTrans());

    // bound the triangle
    s_BoundingBox.BoundVec(&s_Triangle.m_V0);
    s_BoundingBox.BoundVec(&s_Triangle.m_V1);
    s_BoundingBox.BoundVec(&s_Triangle.m_V2);
}

void ExampleShutDown()
{
    // terminate GX
    IwGxTerminate();
}

bool ExampleUpdate()
{
    return true;
}

void ExampleRender()
{
    // clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // start with no lighting
    IwGxLightingOff();

    // the IwGxDebugPrim* functions are used to render primitives in wireframe
    // (only available in debug builds)

    // when rendering we translate the view so that the primitives are visible:

    CIwFMat view;

    // set to the identity rotation matrix with zero translation component
    view.SetIdentity();

    // set the translation vector of the matrix such that the primitives are visible
    view.t.z = -0.7f * 4096.0f;

    // set the view matrix
    IwGxSetViewMatrix(&view);

    // render the sphere
    IwGxDebugPrimSphere(s_Sphere, IwGxGetColFixed(IW_GX_COLOUR_BLUE));

    // render the triangle
    IwGxDebugPrimTri(s_Triangle.m_V0, s_Triangle.m_V1, s_Triangle.m_V2, IwGxGetColFixed(IW_GX_COLOUR_RED));

    // render the bounding box
    IwGxDebugPrimBBox(s_BoundingBox, IwGxGetColFixed(IW_GX_COLOUR_YELLOW));

    IwGxFlush();

    // swap buffers
    IwGxSwapBuffers();
}

