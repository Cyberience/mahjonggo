/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwGeomMatrixRotation Matrix Rotation Example
 *
 * The following example demonstrates how to use the IwGeom Matrix
 * functionality. The example renders a triangle and a bounding box,
 * rotating the triangle.
 *
 * The main classes used to achieve this are:
 *  - CIwFVec3
 *  - CIwFMat
 *  - CIwFBBox
 *
 * The main functions used to achieve this are:
 *  - CIwFBBox::BoundVec()
 *  - CIwFVec3::Normalise()
 *  - CIwFVec3::GetNormalised()
 *  - CIwFVec3::operator - [binary minus]
 *  - CIwFMat::SetIdentity()
 *  - CIwFMat::SetAxisAngle()
 *  - CIwFMat::RotateVec()
 *  - IwGxDebugPrimTri()
 *  - IwGxDebugPrimBBox()
 *  - IwGxSetViewMatrix()
 *
 * The example creates a triangle primitive and a bounding box.
 * The example then sets the view matrix and renders
 * the triangle and its bounding box using the IwGxDebugPrimSphere(),
 * IwGxDebugPrimTri() and IwGxDebugPrimBBox() functions. For each render
 * loop the triangle is rotated by a rotation matrix calculated by CIwMat::RotateVec(),
 * and the bounding box is reconstructed to contain the rotated triangle.
 *
 * @note For more information on using matrices,
 * see the @ref matrix "Matrix" section in the <i>IwGeom API Documentation</i>.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwGeomMatrixRotationImage.png
 *
 * @include IwGeomMatrixRotation.cpp
 */

#include "IwGeom.h"

// for primitive rendering
#include "IwGx.h"
#include "IwGxPrint.h"
#include "IwDebugPrim.h"

struct Triangle
{
    // vertices
    CIwFVec3 m_V0, m_V1, m_V2;
};

// primitives:
// floating point Triangle
Triangle    s_Triangle;

// the angular speed of the triangle
#define TRIANGLE_ROT    PI*0x40/2048.0f

// floating point bounding box
CIwFBBox s_BoundingBox(CIwFVec3::g_Zero, CIwFVec3::g_Zero);


// Bound the vertices of the triangle with the bounding box
void BoundTriangle()
{
    s_BoundingBox = CIwFBBox(CIwFVec3::g_Zero, CIwFVec3::g_Zero);
    s_BoundingBox.BoundVec(&s_Triangle.m_V0);
    s_BoundingBox.BoundVec(&s_Triangle.m_V1);
    s_BoundingBox.BoundVec(&s_Triangle.m_V2);
}

void ExampleInit()
{
    // initialise GX
    IwGxInit();

    // initialise triangle
    s_Triangle.m_V0 = CIwFVec3(0, 0, 0);
    s_Triangle.m_V1 = CIwFVec3(0, -0.3f * 4096.0f, 0);
    s_Triangle.m_V2 = CIwFVec3(-0.3f * 4096.0f, 0, 0);

    // bound the triangle with the bounding box
    BoundTriangle();
}

void ExampleShutDown()
{
    // terminate GX
    IwGxTerminate();
}

bool ExampleUpdate()
{
    // (arbitrarily) rotate the triangle vertices:

    // define the rotation axis
    CIwFVec3 rotationAxis(1, -1, 0);
    // ensure the axis is normal
    rotationAxis.Normalise();

    // create a rotation matrix, defined by rotationAxis and TRIANGLE_ROT:
    CIwFMat rot;
    rot.SetAxisAngle(rotationAxis, TRIANGLE_ROT);

    s_Triangle.m_V0 = rot.RotateVec(s_Triangle.m_V0);
    s_Triangle.m_V1 = rot.RotateVec(s_Triangle.m_V1);
    s_Triangle.m_V2 = rot.RotateVec(s_Triangle.m_V2);

    // bound the triangle with the bounding box
    BoundTriangle();

    return true;
}

void ExampleRender()
{
    // clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // start with no lighting
    IwGxLightingOff();

    // the IwGxDebugPrim* functions are used to render primitives in wireframe
    // (only available in debug builds)

    // when rendering we translate the view so that the primitives are visible:

    CIwFMat view;

    // set to the identity rotation matrix with zero translation component
    view.SetIdentity();

    // set the translation vector of the matrix such that the primitives are visible
    view.t.z = -0.7f * 4096.0f;

    // set the view matrix
    IwGxSetViewMatrix(&view);

    // render the triangle
    IwGxDebugPrimTri(s_Triangle.m_V0, s_Triangle.m_V1, s_Triangle.m_V2, IwGxGetColFixed(IW_GX_COLOUR_RED));

    // render the bounding box
    IwGxDebugPrimBBox(s_BoundingBox, IwGxGetColFixed(IW_GX_COLOUR_YELLOW));

    IwGxFlush();

    // swap buffers
    IwGxSwapBuffers();
}
