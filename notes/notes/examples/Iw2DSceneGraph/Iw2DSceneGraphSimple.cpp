/*
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This document is protected by copyright, and contains information
 * proprietary to Marmalade.
 *
 * This file consists of source code released by Marmalade under
 * the terms of the accompanying End User License Agreement (EULA).
 * Please do not use this program/source code before you have read the
 * EULA and have agreed to be bound by its terms.
 */

/**
 * @page ExampleIw2DSceneGraphSimple Iw2DSceneGraph Simple Example
 *
 * This example demonstrates a simple use of the Iw2DSceneGraph API.
 *
 * We create 4 CSprites - 3 attached to the scene graph root, and one attached
 * to a parent sprite.
 *
 * We also create 4 CLabels - 3 attached to the scene graph root, and one attached
 * to a parent label.
 *
 * If the MKB defines IW_2DSCENEGRAPH_USE_TWEEN, then IwTween is compiled into the
 * example through the Iw2DSceneGraph module, and used to animate one of the sprites.
 *
 * Debug drawing is turned on for all nodes; CSprites have their bounding rectangles
 * drawn in red, and CLabels have theirs drawn in green.
 *
 * The main classes used to achieve this are:
 *  - Iw2DSceneGraphCore::CNode
 *  - Iw2DSceneGraph::CSprite
 *  - Iw2DSceneGraph::CLabel
 *
 * The main functions used to achieve this are:
 *  - Iw2DSceneGraphCore::CNode::AddChild()
 *  - Iw2DSceneGraphCore::CNode::Render()
 *  - Iw2DSceneGraphCore::CNode::Update()
 *
 * @include Iw2DSceneGraphSimple.cpp
 */

#include "Iw2DSceneGraph.h"
#include "IwGx.h"

using namespace Iw2DSceneGraphCore;
using namespace Iw2DSceneGraph;

// Scene root node
CNode* g_SceneRoot = NULL;

// Resources to load
CIw2DImage* g_Image = NULL;
CIw2DFont* g_Font = NULL;

// Imitate 25 fps - we can do this as we know our Update/Render code is taking
// very little time
#define FORCED_TIME_UPDATE 0.04f

//------------------------------------------------------------------------------
void InitScene()
{
    // Create root node
    g_SceneRoot = new CNode();

    // Load resources
    g_Image = Iw2DCreateImage("crate.png");
    g_Font = Iw2DCreateFont("arial14.gxfont");

    // Add sprite nodes
    float iw = (float)g_Image->GetWidth();
    float ih = (float)g_Image->GetHeight();

    // Top-left at (0,0) size (35,35)
    CSprite* pSprite = new CSprite();
    pSprite->SetImage(g_Image);
    pSprite->m_ScaleX = 0.5f, pSprite->m_ScaleY = 0.5f;
    g_SceneRoot->AddChild(pSprite);

    // Top-left at (35,35) size (70,70)
    CSprite* pSprite1 = new CSprite();
    pSprite1->SetImage(g_Image);
    pSprite1->m_X = iw/2, pSprite1->m_Y = ih/2;
    g_SceneRoot->AddChild(pSprite1);

    // Top-left at (140,140) size (210,210)
    CSprite* pSprite2 = new CSprite();
    pSprite2->SetImage(g_Image);
    pSprite2->m_X = iw, pSprite2->m_Y = ih;
    pSprite2->m_ScaleX = 3.0f, pSprite2->m_ScaleY = 3.0f;
    pSprite2->m_DebugDraw = true;
    pSprite1->AddChild(pSprite2);

    // Middle at (0,sh) size (140,140) rotation 22.5
    CSprite* pSprite3 = new CSprite();
    pSprite3->SetImage(g_Image);
    pSprite3->m_X = 0.0f, pSprite3->m_Y = (float)IwGxGetScreenHeight();
    pSprite3->m_AnchorX = 0.5f, pSprite3->m_AnchorY = 0.5f;
    pSprite3->m_ScaleX = 3.0f, pSprite3->m_ScaleY = 3.0f;
    pSprite3->m_Angle = 22.f;
    pSprite3->m_DebugDraw = true;
    g_SceneRoot->AddChild(pSprite3);

    // Add some tweening to the sprites
#ifdef IW_2DSCENEGRAPH_USE_TWEEN
    pSprite3->m_Tweener.Tween(1.0f,
        IwTween::FLOAT, &pSprite3->m_Angle, 180.0f,
        IwTween::MIRROR,
        IwTween::EASING, IwTween::Ease::sineInOut,
        IwTween::END);
#endif

    // Add labels
    // Top-left at (0,0) size (100,100) align (top,left) scale (1,1)
    CLabel* pLabel0 = new CLabel();
    pLabel0->SetFont(g_Font);
    pLabel0->SetText("ALIGN TOP-LEFT");
    pLabel0->m_W = pLabel0->m_H = 100.0f;
    pLabel0->m_DebugDraw = true;
    g_SceneRoot->AddChild(pLabel0);

    // Top-right at (sw,0) size (100,100) align (top,right) scale (1,1)
    CLabel* pLabel1 = new CLabel();
    pLabel1->SetFont(g_Font);
    pLabel1->SetText("ALIGN TOP-RIGHT");
    pLabel1->m_W = pLabel1->m_H = 100.0f;
    pLabel1->m_X = (float)IwGxGetScreenWidth();
    pLabel1->m_AnchorX = 1.0f, pLabel1->m_AnchorY = 0.0f;
    pLabel1->m_AlignHor = IW_2D_FONT_ALIGN_RIGHT;
    pLabel1->m_DebugDraw = true;
    g_SceneRoot->AddChild(pLabel1);

    // Bottom-left at (0,sh) size (100,100) align (bottom,left) scale (1,1)
    CLabel* pLabel2 = new CLabel();
    pLabel2->SetFont(g_Font);
    pLabel2->SetText("ALIGN BOTTOM-LEFT");
    pLabel2->m_W = pLabel2->m_H = 100.0f;
    pLabel2->m_Y = (float)IwGxGetScreenHeight();
    pLabel2->m_AnchorX = 0.0f, pLabel2->m_AnchorY = 1.0f;
    pLabel2->m_AlignVer = IW_2D_FONT_ALIGN_BOTTOM;
    pLabel2->m_DebugDraw = true;
    g_SceneRoot->AddChild(pLabel2);

    // Bottom-right at (sw,sh) size (100,100) align (bottom,right) scale (1,1)
    CLabel* pLabel3 = new CLabel();
    pLabel3->SetFont(g_Font);
    pLabel3->SetText("ALIGN BOTTOM-RIGHT");
    pLabel3->m_W = pLabel3->m_H = 100.0f;
    pLabel3->m_X = (float)IwGxGetScreenWidth(); pLabel3->m_Y = (float)IwGxGetScreenHeight();
    pLabel3->m_AnchorX = 1.0f, pLabel3->m_AnchorY = 1.0f;
    pLabel3->m_AlignHor = IW_2D_FONT_ALIGN_RIGHT, pLabel3->m_AlignVer = IW_2D_FONT_ALIGN_BOTTOM;
    pLabel3->m_DebugDraw = true;
    g_SceneRoot->AddChild(pLabel3);
}
//------------------------------------------------------------------------------
void UpdateScene(float deltaTime)
{
    g_SceneRoot->Update(deltaTime);
}
//------------------------------------------------------------------------------
void RenderScene()
{
    g_SceneRoot->Render();
}
//------------------------------------------------------------------------------
void ShutdownScene()
{
    delete g_SceneRoot;
    delete g_Font;
    delete g_Image;
}
//------------------------------------------------------------------------------
int main()
{
    Iw2DInit();

    InitScene();

    // Loop forever, until the user or the OS performs some action to quit the app
    while (!s3eDeviceCheckQuitRequest())
    {
        UpdateScene(FORCED_TIME_UPDATE);

        Iw2DSurfaceClear(0xff000080);
        RenderScene();
        Iw2DSurfaceShow();

        s3eDeviceYield((int64)(FORCED_TIME_UPDATE * 1000));
    }

    ShutdownScene();

    Iw2DTerminate();
    return 0;
}
