// Source file: C:/dev/sdk/main/sdk_examples/ToolsExamples/transparency/transparencyexample.max
CIwModel
{
	name "plant4"
	CMesh
	{
		name "plant4"
		scale 1.0
		CVerts
		{
			numVerts 4
			v {2449.88037,5.69248,729.06897}
			v {2951.63281,5.69248,1187.97131}
			v {2449.88013,999.50812,729.06903}
			v {2951.63281,999.50812,1187.97144}
		}
		CVertNorms
		{
			numVertNorms 1
			vn {-0.15792,0.98745,0}
		}
		CUVs
		{
			setID 0
			numUVs 4
			uv {0.06111,-0.16451}
			uv {0.93889,-0.16451}
			uv {0.06111,-0.83549}
			uv {0.93889,-0.83549}
		}
		CSurface
		{
			material "plant2"
			CQuads
			{
				numQuads 1
				q {1,0,1,-1,-1} {0,0,0,-1,-1} {2,0,2,-1,-1} {3,0,3,-1,-1}
			}
		}
	}
}
