// Source file: C:/dev/sdk/main/sdk_examples/ToolsExamples/transparency/transparencyexample.max
CIwModel
{
	name "plant3"
	CMesh
	{
		name "plant3"
		scale 1.0
		CVerts
		{
			numVerts 4
			v {-682.45142,1367.16382,25.48681}
			v {137.77275,1367.16382,25.48681}
			v {-682.45142,2401.83838,25.48686}
			v {137.77275,2401.83838,25.48686}
		}
		CVertNorms
		{
			numVertNorms 1
			vn {0,1,0}
		}
		CUVs
		{
			setID 0
			numUVs 4
			uv {0.00050,-0.00050}
			uv {0.99950,-0.00050}
			uv {0.00050,-0.99950}
			uv {0.99950,-0.99950}
		}
		CSurface
		{
			material "plant1"
			CQuads
			{
				numQuads 1
				q {1,0,1,-1,-1} {0,0,0,-1,-1} {2,0,2,-1,-1} {3,0,3,-1,-1}
			}
		}
	}
}
