// Source file: C:/dev/sdk/main/sdk_examples/ToolsExamples/transparency/transparencyexample.max
CIwModel
{
	name "floor1"
	CMesh
	{
		name "floor1"
		scale 1.0
		CVerts
		{
			numVerts 4
			v {-3072.95459,0,6163.73584}
			v {3347.66016,0,6163.73584}
			v {-3072.95459,0,18.22510}
			v {3347.66016,0,18.22510}
		}
		CVertNorms
		{
			numVertNorms 1
			vn {0,1,0}
		}
		CUVs
		{
			setID 0
			numUVs 4
			uv {0.00250,-0.00250}
			uv {4.99750,-0.00250}
			uv {0.00250,-4.99750}
			uv {4.99750,-4.99750}
		}
		CSurface
		{
			material "floor"
			CQuads
			{
				numQuads 1
				q {1,0,1,-1,-1} {0,0,0,-1,-1} {2,0,2,-1,-1} {3,0,3,-1,-1}
			}
		}
	}
}
