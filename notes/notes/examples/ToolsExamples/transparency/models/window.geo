// Source file: C:/dev/sdk/main/sdk_examples/ToolsExamples/transparency/transparencyexample.max
CIwModel
{
	name "window"
	CMesh
	{
		name "window"
		scale 1.0
		CVerts
		{
			numVerts 4
			v {-951.42535,1381.00757,-93.47831}
			v {912.09662,1381.00757,-93.47831}
			v {-951.42535,3222.35986,-93.47833}
			v {912.09662,3222.35986,-93.47833}
		}
		CVertNorms
		{
			numVertNorms 1
			vn {0,1,0}
		}
		CUVs
		{
			setID 0
			numUVs 4
			uv {0.06976,0.03033}
			uv {0.93024,0.03033}
			uv {0.06976,-0.99956}
			uv {0.93024,-0.99956}
		}
		CSurface
		{
			material "windows"
			CQuads
			{
				numQuads 1
				q {1,0,1,-1,-1} {0,0,0,-1,-1} {2,0,2,-1,-1} {3,0,3,-1,-1}
			}
		}
	}
}
