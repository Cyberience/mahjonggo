// Source file: jackInTheBox.max
CIwModel
{
	name "Plane01"
	CMesh
	{
		name "Plane01"
		scale 1.0
		CVerts
		{
			numVerts 4
			v {-177.07368,-198.14716,-0.00001}
			v {215.64757,-198.14716,-0.00001}
			v {-177.07368,170.28204,0.00001}
			v {215.64757,170.28204,0.00001}
		}
		CVertNorms
		{
			numVertNorms 1
			vn {0,1,0}
		}
		CUVs
		{
			setID 0
			numUVs 4
			uv {0,-0.74448}
			uv {0.23681,-0.74448}
			uv {0,-0.99648}
			uv {0.23681,-0.99648}
		}
		CSurface
		{
			material "numbersMat"
			CQuads
			{
				numQuads 1
				q {3,0,3,-1,-1} {1,0,1,-1,-1} {0,0,0,-1,-1} {2,0,2,-1,-1}
			}
		}
	}
}
