/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleDerbhBasic Derbh Basic Example
 *
 * This program illustrates how to use the S3E file system to read a file from
 * a compressed Derbh archive. S3E allows the user to mount their own custom file systems which
 * override many of the S3E file functions. Here this functionality is utilised to access a
 * compressed archive as a logical drive.
 *
 * The important functions required to achieve this are:
 *  - dzArchiveAttach()
 *  - dzArchiveDetach()
 *  - s3eFileOpen()
 *  - s3eFileRead()
 *
 * The Derbh interface (dzio.h) allows an archive
 * to be "attached" from a pathname; this automatically mounts the custom file system.
 *
 * The archive "archive1.dz" contains three files: "TestTexture.bmp", "TextFile.txt" and "testvideo2.3gp".
 *
 * Pressing 0 will attach the archive "archive.dz".
 * Pressing 1 will, having attached the archive, decompress it and read the file "TextFile.txt".
 * The contents of this file are then displayed on the screen.
 *
 * The following graphic illustrates the example output.
 *
 * @image html DerbhBasicImage.png
 *
 * @include DerbhBasic.cpp
 */

#include "derbh.h"
#include "s3e.h"
#include "IwArray.h"
#include "IwString.h"
#include "IwGx.h"
#include "ExamplesMain.h"

//-----------------------------------------------------------------------------

enum states { MOUNTED, READ, RESET, ERROR}; //State enumerations
states g_Status;              //Statemachine for file operation
char g_ErrorString[0x100];    //Contains info about errors for displaying purposes
char* g_ReadBuffer;           //Data which is being read from the file

static void CreateButtonsUI(int w, int h)
{
    // Create the UI
    switch (g_Status)
    {
        case RESET:
            AddButton("Attach archive", 10, 200, w/2, 30, s3eKey0);
            break;
        case MOUNTED:
            AddButton("Read archive", 10, 200, w/2, 30, s3eKey1);
            break;
        default:
            break;
    }
}

void SurfaceChangedCallback()
{
    DeleteButtons();
    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise file status
    g_Status = RESET;
    IwGxRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}

//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // Detach the archive.
    dzArchiveDetach();

    // Free the read buffer.
    delete[] g_ReadBuffer;

    IwGxUnRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
}

//-----------------------------------------------------------------------------
// The following function checks the keyboard state and performs a number of
// operations based on it. The options are:
//   - If s3eKey0 is pressed, the archive "archive1.dz" is attached.
//   - If s3eKey1 is pressed, the attached archive is read using the s3e interface,
//     the contents are then displayed on the screen.
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // Pressing 0 attaches an archive and automatically mounts the Derbh custom file system.
    if ((CheckButton("Attach archive") & S3E_KEY_STATE_PRESSED)
        && (g_Status == RESET))
    {
        // Attach archive.
        int32 result = dzArchiveAttach("archive/archive1.dz");
        if (result == S3E_RESULT_ERROR)
        {
            // An error occurred.
            strcpy(g_ErrorString, "Error attaching archive");
            g_Status = ERROR;
        }
        else
        {
            // The filesystem was MOUNTED with no errors.
            g_Status = MOUNTED;
        }
        DeleteButtons();
        CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
    }

    //Press 1 to read the text file from the compressed archive
    //then display the contents on the screen.
    if ((CheckButton("Read archive") & S3E_KEY_STATE_PRESSED)
        && (g_Status == MOUNTED))
    {
        //All of the s3eFile calls from this point map to the versions
        //in the MOUNTED Derbh file system.

        //Open file for reading.

        s3eFile* handle = s3eFileOpen("TextFile.txt","rb");

        //Get the size of the file in its uncompressed state.

        uint32 filesize = s3eFileGetSize(handle);

        //Allocate a read buffer, the extra space is for a formatting string
        //so that s3eDebugPrint will display the text slightly faded.

        g_ReadBuffer = new char[filesize + 8];

        //Copy the formatting string.

        strcpy(g_ReadBuffer,"`x666666");

        //Initialise a pointer to the buffer after the formatting string.

        char* buff = g_ReadBuffer + 8;

        //Read the file into the buffer.

        int result = s3eFileRead(buff, filesize, 1, handle);

        //Close the file.
        s3eFileClose(handle);

        if (result != 0)
        {
            //No error has occurred
            g_Status = READ;
        }
        else
        {
            //Something went wrong during opening of the file
            //retrieve error for display
            g_Status = ERROR;
            s3eFileGetError();
            strcpy(g_ErrorString, s3eFileGetErrorString());
        }
        DeleteButtons();
        CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
    }

    return true;
}

//-----------------------------------------------------------------------------
// The following function constitutes the UI rendering.
//-----------------------------------------------------------------------------
void ExampleRender()
{
    IwGxFlush();

    //Display permanent explanatory text at the top of the window.
    int32 y = 60;
    IwGxPrintString(10, y, "Access to compressed Derbh archive via S3E interface.", true);
    y += 20;
    IwGxPrintString(10, y, "Illustrates the use of mounting a custom file system.", true);
    y += 20;
    IwGxPrintString(10, y, "Press escape at any time to exit.", true);
    y += 20;

    switch (g_Status)
    {
        case RESET:
            IwGxPrintString(10, y, "`x666666Press 0 or the Attach button to attach archive",true);
            break;

        case MOUNTED:
            IwGxPrintString(10, y, "File system MOUNTED.", true);
            y += 10;
            IwGxPrintString(10, y, "`x666666Press 1 or the Read button to read TextFile.txt from archive and display the contents.", true);
            break;

        case READ:
            IwGxPrintString(10, y, g_ReadBuffer, true);
            break;

        case ERROR:
            IwGxPrintString(10, y, g_ErrorString, true);
            break;
    }

    //Show the surface, then clear it for next time round.
    IwGxSwapBuffers();
}
