# -*- coding: utf-8 -*-
# Deployment settings for blocslot.
# This file is autogenerated by the mkb system and used by the s3e deployment
# tool during the build process.

config = {}
cmdline = ['/Applications/Marmalade.app/Contents/s3e/makefile_builder/mkb.py', '/Users/geoffrey/_dev/mjrpoker/notes/examples/games/blocslot/blocslot.mkb', '--deploy-only', '--buildenv=SCONS', '--x86', '--android-x86']
mkb = '/Users/geoffrey/_dev/mjrpoker/notes/examples/games/blocslot/blocslot.mkb'
mkf = ['/Applications/Marmalade.app/Contents/s3e/s3e-default.mkf', '/Applications/Marmalade.app/Contents/extensions/s3eAmazonAds/s3eAmazonAds.mkf', '/Applications/Marmalade.app/Contents/modules/iwutil/iwutil.mkf', '/Applications/Marmalade.app/Contents/modules/third_party/libjpeg/libjpeg.mkf', '/Applications/Marmalade.app/Contents/modules/third_party/libpng/libpng.mkf', '/Applications/Marmalade.app/Contents/modules/third_party/zlib/zlib.mkf', '/Applications/Marmalade.app/Contents/platform_libs/android/amazon-ads-android-sdk/amazon-ads-android-sdk.mkf', '/Applications/Marmalade.app/Contents/platform_libs/iphone/amazon-ads-ios-sdk/amazon-ads-ios-sdk.mkf', '/Applications/Marmalade.app/Contents/modules/iw2d/iw2d.mkf', '/Applications/Marmalade.app/Contents/modules/legacy/iw2d/iw2d_legacy.mkf', '/Applications/Marmalade.app/Contents/modules/iwgx/iwgx.mkf', '/Applications/Marmalade.app/Contents/modules/legacy/iwgx/iwgx_legacy.mkf', '/Applications/Marmalade.app/Contents/modules/iwgeom/iwgeom.mkf', '/Applications/Marmalade.app/Contents/modules/iwresmanager/iwresmanager.mkf', '/Applications/Marmalade.app/Contents/modules/legacy/iwresmanager/iwresmanager_legacy.mkf', '/Applications/Marmalade.app/Contents/modules/iwgl/iwgl.mkf', '/Applications/Marmalade.app/Contents/modules/iwgxfont/iwgxfont.mkf', '/Applications/Marmalade.app/Contents/modules/legacy/iwgxfont/iwgxfont_legacy.mkf', '/Applications/Marmalade.app/Contents/modules/third_party/tiniconv/tiniconv.mkf', '/Users/geoffrey/_dev/mjrpoker/notes/examples/games/blocslot/blocslot.mkf']

class DeployConfig(object):
    pass

######### ASSET GROUPS #############

assets = {}

assets['Default'] = [
    ('/Users/geoffrey/_dev/mjrpoker/notes/examples/games/blocslot/data', '.', 0),
]

assets['default'] = [
    ('/Users/geoffrey/_dev/mjrpoker/notes/examples/games/blocslot/data/splashscreen.jpg', 'splashscreen.jpg', 0),
    ('/Users/geoffrey/_dev/mjrpoker/notes/examples/games/blocslot/data-ram/data-sw/tiles.group.bin,/Users/geoffrey/_dev/mjrpoker/notes/examples/games/blocslot/data-ram/data-gles1/tiles.group.bin', 'tiles.group.bin', 0),
]

assets['artbuild'] = [
    ('/Users/geoffrey/_dev/mjrpoker/notes/examples/games/blocslot/data', '.', 0),
]

######### DEFAULT CONFIG #############

class DefaultConfig(DeployConfig):
    embed_icf = -1
    name = 'blocslot'
    pub_sign_key = 0
    priv_sign_key = 0
    caption = 'blocslot'
    long_caption = 'blocslot'
    version = [0, 0, 1]
    config = ['/Users/geoffrey/_dev/mjrpoker/notes/examples/games/blocslot/data/app.icf']
    data_dir = '/Users/geoffrey/_dev/mjrpoker/notes/examples/games/blocslot/data'
    mkb_dir = '/Users/geoffrey/_dev/mjrpoker/notes/examples/games/blocslot'
    iphone_link_lib = ['s3eAmazonAds']
    tvos_link_lib = []
    osx_ext_dll = []
    iphone_extra_string = []
    wp81_extra_pri = []
    ws8_ext_capabilities = []
    ws8_ext_native_only_dll = []
    win10_ext_uap_capabilities = []
    tizen_so = []
    tvos_extra_string = []
    tvos_link_libs = []
    win10_ext_native_dll = []
    ws81_ext_native_only_dll = []
    android_external_res = []
    win32_ext_dll = []
    tvos_link_libdirs = []
    wp8_ext_capabilities = []
    ws8_extra_res = []
    iphone_extra_plugins = []
    ws81_ext_managed_dll = []
    iphone_link_libdir = ['/Applications/Marmalade.app/Contents/extensions/s3eAmazonAds/lib/iphone']
    wp81_ext_capabilities = []
    android_extra_application_manifest = ['/Applications/Marmalade.app/Contents/extensions/s3eAmazonAds/source/android/ExtraAppManifests.xml']
    win10_extra_res = []
    ws8_ext_native_dll = []
    iphone_link_libdirs = []
    android_external_assets = []
    blackberry_extra_descriptor = []
    android_ext_target_sdk_version = [17]
    android_extra_manifest = []
    wp81_ext_sdk_ref = []
    tvos_link_libdir = []
    wp81_ext_device_capabilities = []
    android_extra_application_attributes_manifest = []
    icon = '/Users/geoffrey/_dev/mjrpoker/notes/examples/games/blocslot/icons'
    win10_ext_capabilities = []
    linux_ext_lib = []
    android_ext_min_sdk_version = [4]
    wp81_ext_native_only_dll = []
    ws8_ext_managed_dll = []
    tvos_extra_plugins = []
    ws8_ext_sdk_manifest_part = []
    ws8_ext_device_capabilities = []
    ws81_extra_pri = []
    android_external_jars = ['/Applications/Marmalade.app/Contents/platform_libs/android/amazon-ads-android-sdk/third_party/lib/amazon-ads-5.5.149.jar', '/Applications/Marmalade.app/Contents/extensions/s3eAmazonAds/lib/android/s3eAmazonAds.jar']
    win8_winrt_extra_res = []
    wp81_ext_sdk_manifest_part = []
    wp81_extra_res = []
    iphone_external_assets = []
    wp81_ext_managed_dll = []
    tvos_link_opts = []
    win10_ext_managed_dll = []
    iphone_extra_plist = ['/Applications/Marmalade.app/Contents/extensions/s3eAmazonAds/s3eAmazonAds.plist']
    ws81_ext_sdk_manifest_part = []
    ws81_ext_device_capabilities = []
    android_supports_gl_texture = []
    ws8_ext_sdk_ref = []
    tvos_asset_catalog = []
    osx_extra_plist = []
    wp8_ext_native_dll = []
    win10_ext_sdk_manifest_part = []
    tvos_extra_plist = []
    win10_extra_pri = []
    win10_ext_device_capabilities = []
    win8_phone_extra_res = []
    win8_store_extra_res = []
    win32_aux_dll = []
    iphone_link_opts = ['-F/Applications/Marmalade.app/Contents/platform_libs/iphone/amazon-ads-ios-sdk/third_party/Ads -framework AmazonAd', '-weak_framework AdSupport -weak_framework CoreLocation -weak_framework SystemConfiguration', '-weak_framework CoreTelephony -weak_framework MediaPlayer', '-weak_framework EventKit -weak_framework EventKitUI']
    ws81_ext_sdk_ref = []
    wp8_extra_res = []
    ws81_ext_native_dll = []
    ws8_extra_pri = []
    wp8_ext_managed_dll = []
    android_extra_packages = []
    android_so = ['/Applications/Marmalade.app/Contents/extensions/s3eAmazonAds/lib/android/libs3eAmazonAds.so']
    wp8_ext_sdk_ref = []
    osx_extra_res = []
    ws81_extra_res = []
    wp81_ext_native_dll = []
    ws81_ext_capabilities = []
    iphone_link_libs = []
    android_extra_strings = []
    target = {
         'gcc_x86' : {
                   'debug'   : r'/Users/geoffrey/_dev/mjrpoker/notes/examples/games/blocslot/build_blocslot_scons_x86/debug/blocslot.so',
                   'release' : r'/Users/geoffrey/_dev/mjrpoker/notes/examples/games/blocslot/build_blocslot_scons_x86/release/blocslot.so',
                 },
         'gcc_x86_tizen' : {
                   'debug'   : r'/Users/geoffrey/_dev/mjrpoker/notes/examples/games/blocslot/build_blocslot_scons_x86_tizen/debug/blocslot.s86',
                   'release' : r'/Users/geoffrey/_dev/mjrpoker/notes/examples/games/blocslot/build_blocslot_scons_x86_tizen/release/blocslot.s86',
                 },
         'firefoxos' : {
                   'debug'   : r'/Users/geoffrey/_dev/mjrpoker/notes/examples/games/blocslot/build_blocslot_scons_firefoxos/debug/blocslot.js',
                   'release' : r'/Users/geoffrey/_dev/mjrpoker/notes/examples/games/blocslot/build_blocslot_scons_firefoxos/release/blocslot.js',
                 },
         'gcc_arm_android' : {
                   'debug'   : r'/Users/geoffrey/_dev/mjrpoker/notes/examples/games/blocslot/build_blocslot_scons_arm_android/debug/blocslot.so',
                   'release' : r'/Users/geoffrey/_dev/mjrpoker/notes/examples/games/blocslot/build_blocslot_scons_arm_android/release/blocslot.so',
                 },
         'arm_gcc' : {
                   'debug'   : r'/Users/geoffrey/_dev/mjrpoker/notes/examples/games/blocslot/build_blocslot_scons_arm/debug/blocslot.s3e',
                   'release' : r'/Users/geoffrey/_dev/mjrpoker/notes/examples/games/blocslot/build_blocslot_scons_arm/release/blocslot.s3e',
                 },
         'naclx86_64' : {
                   'debug'   : r'/Users/geoffrey/_dev/mjrpoker/notes/examples/games/blocslot/build_blocslot_scons_naclx86_64/debug/blocslot.so.s64',
                   'release' : r'/Users/geoffrey/_dev/mjrpoker/notes/examples/games/blocslot/build_blocslot_scons_naclx86_64/release/blocslot.so.s64',
                 },
         'aarch64_gcc' : {
                   'debug'   : r'/Users/geoffrey/_dev/mjrpoker/notes/examples/games/blocslot/build_blocslot_scons_aarch64/debug/blocslot.s3e',
                   'release' : r'/Users/geoffrey/_dev/mjrpoker/notes/examples/games/blocslot/build_blocslot_scons_aarch64/release/blocslot.s3e',
                 },
         'gcc_x86_android' : {
                   'debug'   : r'/Users/geoffrey/_dev/mjrpoker/notes/examples/games/blocslot/build_blocslot_scons_x86_android/debug/blocslot.so',
                   'release' : r'/Users/geoffrey/_dev/mjrpoker/notes/examples/games/blocslot/build_blocslot_scons_x86_android/release/blocslot.so',
                 },
         'x86' : {
                   'debug'   : r'/Users/geoffrey/_dev/mjrpoker/notes/examples/games/blocslot/build_blocslot_scons_x86/debug/blocslot.so',
                   'release' : r'/Users/geoffrey/_dev/mjrpoker/notes/examples/games/blocslot/build_blocslot_scons_x86/release/blocslot.so',
                 },
        }
    arm_arch = ''
    assets_original = assets
    assets = assets['default']

default = DefaultConfig()

######### Configuration: artbuild

c = DeployConfig()
config['artbuild'] = c
c.os = ['win32']
c.target_folder = 'artbuild'
c.assets = assets['artbuild']
