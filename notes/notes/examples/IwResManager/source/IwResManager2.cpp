/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwResManagerResourceSearch IwResManager Resource Search Example
 *
 * The following example demonstrates how to use the IwResManager
 * search functionality. The example searches through resource groups
 * for resources.
 *
 * The main classes used to achieve this are:
 *  - CIwResManager
 *  - CIwResGroup
 *
 * The main functions used to achieve this are:
 *  - CIwResManager::LoadGroup()
 *  - CIwResManager::GetGroupNamed()
 *  - CIwResManager::GetResNamed()
 *  - CIwResGroup::GetResNamed()
 *
 * The example loads groups: <code>group0</code>, <code>group1</code> and <code>group2</code>.
 * The GetResNamed() function is then used to search for a variety
 * of resources by searching through an individual group or by searching
 * through all the loaded groups. The names of all searched for files
 * and the groups in which they were found are then output to the surface.
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwResManager2Image.png
 *
 * @note For more information on searching groups, see the
 * @ref resourcesearches "Resource Searches" section in
 * the <i>IwResManager API Documentation</i>.
 *
 * @include IwResManager2.cpp
 */

#include "IwResManager.h"
#include "IwTexture.h"

// for screen rendering
#include "IwGx.h"
#include "IwGxPrint.h"

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // initialise GX
    IwGxInit();

    // create the CIwResManager singleton
    IwResManagerInit();

    // load group0; since group0 has group1 as a child, group1 will get loaded automatically
    IwGetResManager()->LoadGroup("group0.group");

    // load group2 (a shared group)
    IwGetResManager()->LoadGroup("group2.group");
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // destroy the CIwResManager singleton
    IwResManagerTerminate();

    // terminate GX
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // no lighting
    IwGxLightingOff();

    char display[1000], buffer[100];
    display[0] = 0;

    // do some searching and display some trivia about assets found in groups group0, group1 and group2

    bool        bResult = false;
    CIwTexture *texture = NULL;

    // get out group0
    CIwResGroup *group = IwGetResManager()->GetGroupNamed("group0");

    // find the CIwTexture resource named "testCard_4bit", searching in group0
    texture = (CIwTexture *)group->GetResNamed("testCard_4bit", IW_GX_RESTYPE_TEXTURE);
    bResult = (texture != NULL);
    if (bResult)
    {
        sprintf(buffer, "%s found in %s\n", texture->DebugGetName(), group->DebugGetName());
        strcat(display, buffer);
    }

    // if you don't know which group to search for a resource, you can use CIwResManager::GetResNamed with the
    // IW_RES_SEARCH_ALL_F flag set - this effectively applies CIwResGroup::GetResNamed to all groups
    // (this function, of course, is less efficient then searching only the group you know the resource is contained in)
    texture = (CIwTexture *)IwGetResManager()->GetResNamed("testCard_8bit", IW_GX_RESTYPE_TEXTURE, IW_RES_SEARCH_ALL_F);
    bResult &= (texture != NULL);
    if (bResult)
    {
        sprintf(buffer, "%s found using global search\n", texture->DebugGetName());
        strcat(display, buffer);
    }

    // the following resource is found by searching in group0 because the search will be extended to group1,
    // seeing as group1 is a child of group0
    texture = (CIwTexture *)group->GetResNamed("testCard_16bit", IW_GX_RESTYPE_TEXTURE);
    bResult &= (texture != NULL);
    if (bResult)
    {
        sprintf(buffer, "%s found searching children of %s\n", texture->DebugGetName(), group->DebugGetName());
        strcat(display, buffer);
    }

    // the following resource is found by searching in group0 because the resource is contained in
    // group2 - a shared group, i.e. ANY resource search which doesn't explicitly ignore shared
    // resources (IW_RES_IGNORE_SHARED_F) will consider this group.
    texture = (CIwTexture *)group->GetResNamed("testTexture", IW_GX_RESTYPE_TEXTURE);
    bResult &= (texture != NULL);
    if (bResult)
    {
        sprintf(buffer, "%s found searching shared groups\n", texture->DebugGetName());
        strcat(display, buffer);
    }

    IwGxPrintString(0, 40, display);

    if (bResult)
    {
        IwGxPrintString(0, 110, "All textures successfully located from their Resource Groups!");
    }

    // Flush and swap
    IwGxFlush();
    IwGxSwapBuffers();
}
