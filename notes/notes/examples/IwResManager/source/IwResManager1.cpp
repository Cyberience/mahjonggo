/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwResManagerGroups IwResManager Groups Example
 *
 * The following example demonstrates the loading and destroying
 * of resource groups (including child groups).
 *
 * The main classes used to achieve this are:
 *  - CIwResManager
 *  - CIwResGroup
 *
 * The main functions used to achieve this are:
 *  - CIwResManager::LoadGroup()
 *  - CIwResManager::GetCurrentGroup()
 *  - CIwResManager::GetGroupNamed()
 *  - CIwResManager::DestroyGroup()
 *  - CIwResGroup::GetOwner()
 *
 * The example instantiates the CIwResManager singleton using
 * IwResManagerInit(). Both group0 and group1 are then loaded using
 * the code: <code>IwGetResManager()->LoadGroup("group0.group")</code>.
 * The group0 group file is illustrated below:
 *
 * @code
 * CIwResGroup
 * {
 *  name    "group0"
 *
 *  # textures
 *  "textures/testCard_4bit.bmp"
 *  "textures/testCard_8bit.bmp"
 *
 *  # child group
 *  "group1.group"
 * }
 * @endcode
 *
 * Group0 has group1 as a child so this is loaded automatically
 * and a pointer to group1 is retrieved using IwGetResManager()->GetGroupNamed("group1").
 *
 * The loaded group names are output to screen and their relationships
 * are output also.
 *
 * On shutdown the IwResManagerTerminate is called which destroys
 * all the loaded groups.
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwResManager1Image.png
 *
 * @note For more information on groups, see the @ref groups "Groups" section
 * in the <i>IwResManager API Documentation</i>.
 *
 * @include IwResManager1.cpp
 */
//-----------------------------------------------------------------------------

#include "IwResManager.h"

// for screen rendering
#include "IwGx.h"
#include "IwGxPrint.h"

// array of CIwResGroup pointers used for groups retrieved from the CIwResManager singleton
CIwResGroup *s_Groups[2];

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise GX
    IwGxInit();

    // create the CIwResManager singleton
    IwResManagerInit();

    // load group0 (a resource group);
    // since group0 has group1 as a child, group1 will get loaded automatically
    IwGetResManager()->LoadGroup("group0.group");

    // groups can be searched for by name
    s_Groups[1] = IwGetResManager()->GetGroupNamed("group1");

    // CIwResManager::GetCurrentGroup gives the most recently loaded group
    // (this can also be manually set via CIwResManager::SetCurrentGroup);
    // the following gives the same as IwGetResManager()->GetGroupNamed("group0")
    s_Groups[0] = IwGetResManager()->GetCurrentGroup();
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // groups can be destroyed by name;
    // group1 is also destroyed, in this case, by this call to DestroyGroup since it is a child of group0
    // (note that groups are destroyed automatically when the CIwResManager singleton is destroyed)
    IwGetResManager()->DestroyGroup("group0");

    // destroy the CIwResManager singleton. Note that this actually destroys all groups, so the above line
    // isn't strictly necessary
    IwResManagerTerminate();

    // terminate GX
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // no lighting
    IwGxLightingOff();

    char display[0x100] = "";
    char buffer[0x80];

    // display some trivia about group0 and group1
    for (int32 i = 0; i < 2; i++)
    {
#ifdef IW_DEBUG
        // Note that CIwManaged::DebugGetName() only returns anything useful in Debug builds
        sprintf(buffer, "Group name '%s' : child of group name '%s'\n",
                s_Groups[i]->DebugGetName(),
                s_Groups[i]->GetOwner() ? s_Groups[i]->GetOwner()->DebugGetName() : "<none>");
#else
        sprintf(buffer, "Group %d hash is 0x%x\n", i, s_Groups[i]->m_Hash);
#endif
        strcat(display, buffer);
    }

    IwGxPrintString(0, 50, "Using Resource Groups");
    IwGxPrintString(0, 80, display);

    // Flush and swap
    IwGxFlush();
    IwGxSwapBuffers();
}
