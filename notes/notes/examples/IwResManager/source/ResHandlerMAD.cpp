/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/*
 * ResHandlerMAD.cpp
 *
 * Resource handler for .mad files
 */
//-----------------------------------------------------------------------------

// project
#include "ResHandlerMAD.h"

// library
#include "s3eFile.h"
#include "CMad.h"

//--------------------------------------------------------------------------------
// CResHandlerMAD
//--------------------------------------------------------------------------------
CResHandlerMAD::CResHandlerMAD() : CIwResHandler("mad", "CMad")
{
    SetName("MAD");
}
//--------------------------------------------------------------------------------
CIwResource *CResHandlerMAD::Build(const CIwStringL& pathname)
{
    // Check that the file exists
    if (!s3eFileCheckExists( pathname.c_str() ) )
        return NULL;

    // parse the .mad file
    IwGetTextParserITX()->ParseFile(pathname.c_str());

    return NULL;
}
//--------------------------------------------------------------------------------
