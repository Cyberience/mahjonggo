/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */


/**
 * @page ExampleIwResManagerResourceHandler IwResManager Resource Handler Example
 *
 * The following example demonstrates how to use the IwResManager
 * resource handler functionality. The example creates a custom file
 * type and uses the resource handler functionality to understand the
 * file.
 *
 * The main classes used to achieve this are:
 *  - CIwResHandler
 *  - CIwResManager
 *  - CIwManaged
 *
 * The main functions used to achieve this are:
 *  - CIwResManager::AddHandler()
 *  - CIwResManager::LoadGroup()
 *  - CIwResHandler::Build()
 *  - CIwManaged::ParseAttribute()
 *  - CIwManaged::ParseClose()
 *
 * The example loads in a group (group5) which references a file
 * of type .mad. The resource manager searches for a handler that understands
 * that file type and this is CResHandlerMAD. The CResHandlerMAD::Build()
 * function parses the .mad file using the ITX text parser (this is
 * a common implementation of resource handlers). The contents of the
 * .mad file are as follows:
 *
 * @code
 * CMad
 * {
 *  name    "haggis"
 *  random  42
 * }
 * @endcode
 *
 * The .mad file contains an object of type CMad, named "haggis",
 * which is parsed and added to the group in context (group5). The
 * CMad object is then retrieved from group5 using a search and output
 * to the screen.
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwResManager5Image.png
 *
 * @note For more information on resource handlers,
 * see the @ref resourcehandlers "Resource Handlers" module overview
 * in the <i>IwResManager API Documentation</i>.
 *
 * @include IwResManager5.cpp
 *
 * <b>The Resource Handler</b>
 *
 * @include ResHandlerMAD.cpp
 *
 * <b>The CMad class</b>
 *
 * @include CMad.h
 */

#include "IwResManager.h"

// resource handler for parsing .mad files
#include "ResHandlerMAD.h"

// for screen rendering
#include "IwGx.h"
#include "IwGxPrint.h"

// definition of CMad
#include "CMad.h"

// define the class factory and name functions for CMad
IW_CLASS_FACTORY(CMad);
IW_MANAGED_IMPLEMENT(CMad);

// pointer to the CMad object in group5
CMad *s_Haggis = NULL;

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise GX
    IwGxInit();

    // add CMad to the class factory
    IW_CLASS_REGISTER(CMad);

    // create the CIwResManager singleton
    IwResManagerInit();

#ifdef IW_BUILD_RESOURCES
    // add a CResHandlerMAD object to the resource manager's list of handlers; this means that when the
    // resource manager encounters a file with extension .mad, it will pass that file onto the CResHandlerMAD object
    // NB. This is not required in a non-resource-building build
    IwGetResManager()->AddHandler(new CResHandlerMAD);
#endif

    // load group5; group5 makes reference to a .mad file (which is dealt with by the CResHandlerMAD object)
    IwGetResManager()->LoadGroup("group5.group");

    // search for the CMad object named "haggis" in the current group (group5); this object will exist due to its
    // declaration in the .mad file (parsed by CResHandlerMAD) referenced in group5
    s_Haggis = (CMad *)IwGetResManager()->GetCurrentGroup()->GetResNamed("haggis", "CMad");
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // destroy the CIwResManager singleton
    IwResManagerTerminate();

    // terminate GX
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // no lighting
    IwGxLightingOff();

    // display contents of the CMad object. Note that DebugGetName() returns an empty string in Release builds.
    char display[0x80];
#ifndef IW_DEBUG
    sprintf(display, "name will be an empty string in release builds");
    IwGxPrintString(0, 80, display);
#endif
    sprintf(display, "Loaded .mad file and found CMad obj:\nname = %s\nrandom = %d", s_Haggis->DebugGetName(), s_Haggis->m_RandomNumber);
    IwGxPrintString(0, 50, "Using Resource Handlers");
    IwGxPrintString(0, 100, display);

    // Flush and swap
    IwGxFlush();
    IwGxSwapBuffers();
}
//-----------------------------------------------------------------------------
