/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwResManagerTemplates IwResManager Templates Example
 *
 * The following example demonstrates how to use the IwResManager
 * Template functionality. The example uses resource templates to condition
 * textures.
 *
 * The main classes used to achieve this are:
 *  - CIwResManager
 *  - CIwResGroup
 *
 * The main functions used to achieve this are:
 *  - CIwResManager::LoadGroup()
 *  - CIwTextParserITX::ParseFile()
 *  - CIwResManager::GetGroupNamed()
 *  - CIwResGroup::GetResNamed()
 *
 * The resource template file <i>templates.itx</i> and
 * resource group <code>group3</code> are loaded. When the template
 * file is parsed using IwGetTextParserITX()->ParseFile("templates.itx"),
 * the template "palette4" is applied to one of the two textures referenced
 * in group3, as detailed in group3.group. The template file is illustrated
 * below:
 *
 * @code
 * CIwResTemplateImage
 * {
 *  name        "palette4"
 *
 *  # convert the image format (for SW rasterisation) to PALETTE4_ABGR_1555
 *  formatSW    "PALETTE4_ABGR_1555"
 *
 *  # convert the image format (for HW rasterisation) to PALETTE4_RGBA_4444
 *  formatHW    "PALETTE4_RGBA_4444"
 * }
 * @endcode
 *
 * The formats of the textures are output to the surface to verify
 * the conditioning that has occurred due to the "palette4" template.
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwResManager3Image.png
 *
 * @include IwResManager3.cpp
 */

//-----------------------------------------------------------------------------

#include "IwResManager.h"
#include "IwTexture.h"

// included so that we can load templates.itx (resource templates)
#include "IwTextParserITX.h"

// for screen rendering
#include "IwGx.h"
#include "IwGxPrint.h"

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // initialise GX
    IwGxInit();

    // create the CIwResManager singleton
    IwResManagerInit();

#ifdef IW_BUILD_RESOURCES
    // load available resource templates by parsing the resource templates file
    IwGetTextParserITX()->ParseFile("templates.itx");
#endif

    // load group3 (a resource group);
    // note: group3 makes mention of a resource template defined in the above-mentioned file -
    // this template will be automatically implemented when the group is loaded (below)
    IwGetResManager()->LoadGroup("group3.group");
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // destroy the CIwResManager singleton
    IwResManagerTerminate();

    // terminate GX
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // no lighting
    IwGxLightingOff();

    char display[1000], buffer[100];
    display[0] = 0;

    // get out group3
    CIwResGroup *group = IwGetResManager()->GetGroupNamed("group3");

    CIwTexture *texture;

    // find the CIwTexture resource named "testCard_8bit";
    // this texture is not loaded within the context of a template, so the format we see will be it's native format
    texture = (CIwTexture *)group->GetResNamed("testCard_8bit", IW_GX_RESTYPE_TEXTURE);
    if (texture)
    {
        sprintf(buffer, "%s has format:\n%s (SW)\n%s (HW)\n\n",
                texture->DebugGetName(),
                CIwImage::g_FormatNames[texture->GetFormatSW()],
                CIwImage::g_FormatNames[texture->GetFormatHW()]);
        strcat(display, buffer);
    }

    // find the CIwTexture resource named "testCard_16bit";
    // this texture is loaded within the context of a template, so the format we see will be the one
    // specified by the template (for SW, PALETTE4_ABGR_1555, and for HW, PALETTE4_RGBA_4444)
    texture = (CIwTexture *)group->GetResNamed("testCard_16bit", IW_GX_RESTYPE_TEXTURE);
    if (texture)
    {
        sprintf(buffer, "%s has format:\n%s (SW)\n%s (HW)\n\n",
                texture->DebugGetName(),
                CIwImage::g_FormatNames[texture->GetFormatSW()],
                CIwImage::g_FormatNames[texture->GetFormatHW()]);
        strcat(display, buffer);
    }

    IwGxPrintString(0,  50, "Using resource templates");
    IwGxPrintString(0,  80, display);

    // Flush and swap
    IwGxFlush();
    IwGxSwapBuffers();
}
//-----------------------------------------------------------------------------
