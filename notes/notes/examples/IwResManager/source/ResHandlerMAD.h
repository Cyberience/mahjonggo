/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
/**
 * ResHandlerMAD.h
 *
 * Resource handler for .mad files
 */
//-----------------------------------------------------------------------------

#ifndef RESHANDLERMAD_H
#define RESHANDLERMAD_H

// library
#include "IwResManager.h"

class CIwResource;

//--------------------------------------------------------------------------------
// CResHandlerMAD
//--------------------------------------------------------------------------------
class CResHandlerMAD : public CIwResHandler
{
public:
    CResHandlerMAD();

    virtual CIwResource*    Build(const CIwStringL& pathname);
};
//--------------------------------------------------------------------------------

#endif
