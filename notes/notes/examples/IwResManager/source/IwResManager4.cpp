/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwResManagerAdvancedResourceSearch IwResManager Advanced Resource Search Example
 *
 * The following example demonstrates how to use the IwResManager
 * search functionality. The example controls the way binaries are
 * serialised out using the OMIT_SHARED_F (and OMIT_CHILDREN_F) CIwResManager
 * flags.
 *
 * The main classes used to achieve this are:
 *  - CIwResManager
 *  - CIwResGroup
 *
 * The main functions used to achieve this are:
 *  - CIwResManager::LoadGroup
 *  - CIwResManager::DestroyGroup
 *  - CIwResManager::GetCurrentGroup
 *  - CIwResGroup::GetResNamed
 *
 * The example loads group2 and group4 without setting OMIT_SHARED_F
 * using <code>IwGetResManager()->m_Flags &;= ~CIwResManager::OMIT_SHARED_F</code>. Loading
 * without OMIT_SHARED_F means that groups subsequently loaded will include
 * resources, even if they are available in a shared group previously
 * loaded.
 *
 * The testTexture (testTexture.bmp resides in group2 as well
 * as in group4) is searched for in group4 with IW_RES_IGNORE_SHARED_F
 * set (i.e., don't consider group2 in our search). In this search
 * CIwResManager searches only in group4 and finds testTexture since
 * it is included due to OMIT_SHARED_F is not set.The example then
 * reloads group4 with OMIT_SHARED_F set using <code>IwGetResManager()->m_Flags
 * |= CIwResManager::OMIT_SHARED_F</code>. Loading with OMIT_SHARED_F
 * means that groups subsequently loaded will not include resources
 * should they be available in a shared group previously loaded.The
 * testTexture is searched for in group4 with IW_RES_IGNORE_SHARED_F
 * set. CIwResManager does NOT find testTexture since testTexture was
 * excluded from group4 due to OMIT_SHARED_F being set.
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwResManager4Image.png
 *
 * @note For more information on searching groups, see the
 * @ref resourcesearches "Resource Searches" section in
 * the <i>IwResManager API Documentation</i>.
 *
 * @include IwResManager4.cpp
 */

#include "IwResManager.h"
#include "IwTexture.h"

// for screen rendering
#include "IwGx.h"
#include "IwGxPrint.h"

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // initialise GX
    IwGxInit();

    // create the CIwResManager singleton
    IwResManagerInit();
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // destroy the CIwResManager singleton
    IwResManagerTerminate();

    // terminate GX
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // no lighting
    IwGxLightingOff();

    char display[1000], buffer[100];
    display[0] = 0;

    // since the example loads group4 with different settings, it will only work if the resources are
    // being rebuilt each time.
    if (IwGetResManager()->GetMode() == CIwResManager::MODE_LOAD )
        strcat(display, "NB. This example will give unexpected output because resource building is not enabled.\n\n");

    CIwTexture *texture;
    CIwResGroup *group;

    // load group2 (shared); group2 contains testTexture.bmp
    IwGetResManager()->LoadGroup("group2.group");

    // unset OMIT_SHARED_F (this is the default behaviour and is explicitly done here for completeness);
    // this means that groups subsequently loaded will include resources, even if they are available in
    // a shared group previously loaded
    IwGetResManager()->m_Flags &= ~CIwResManager::OMIT_SHARED_F;
    sprintf(buffer, "OMIT_SHARED_F unset\n");
    strcat(display, buffer);

    // load group4;
    // group4 contains textTexture.bmp too and since the OMIT_SHARED_F flag is not set, group4 will contain testTexture.bmp
    IwGetResManager()->LoadGroup("group4.group");

    // get group4
    group = IwGetResManager()->GetCurrentGroup();

    // try to find the CIwTexture resource named "testTexture";
    // the resource manager will find testTexture (despite the fact that we are choosing to ignore shared
    // groups in the search) because OMIT_SHARED_F is not set and testTexture resides in group4
    // (note that all searches in this example use IW_RES_PERMIT_NULL_F since we need to accept empty search results)
    texture = (CIwTexture *)group->GetResNamed("testTexture", IW_GX_RESTYPE_TEXTURE, IW_RES_PERMIT_NULL_F | IW_RES_IGNORE_SHARED_F);
    if (texture)
        sprintf(buffer, "%s found\n", texture->DebugGetName());
    else
        sprintf(buffer, "testTexture NOT found\n");

    strcat(display, buffer);

    // now we'll destroy group4 and reload it with OMIT_SHARED_F set

    // destroy group4
    IwGetResManager()->DestroyGroup("group4");

    // set OMIT_SHARED_F; this means that groups subsequently loaded will not include resources should they be
    // available in a shared group previously loaded
    IwGetResManager()->m_Flags |= CIwResManager::OMIT_SHARED_F;
    sprintf(buffer, "OMIT_SHARED_F set\n");
    strcat(display, buffer);

    // load in group4 again
    IwGetResManager()->LoadGroup("group4.group");
    group = IwGetResManager()->GetCurrentGroup();

    // try to find the CIwTexture resource named "testTexture";
    // this time the resource manager does NOT find testTexture since it won't search within the shared
    // group (group2) due to IW_RES_IGNORE_SHARED_F being set, and it won't find testTexture within group4
    // since group4 was loaded with OMIT_SHARED_F set
    texture = (CIwTexture *)group->GetResNamed("testTexture", IW_GX_RESTYPE_TEXTURE, IW_RES_PERMIT_NULL_F | IW_RES_IGNORE_SHARED_F);
    if (texture)
        sprintf(buffer, "%s found\n", texture->DebugGetName());
    else
        sprintf(buffer, "testTexture NOT found\n");

    strcat(display, buffer);

    // destroy groups since they will be reloaded next time ExampleRender() is called
    IwGetResManager()->DestroyGroup("group2");
    IwGetResManager()->DestroyGroup("group4");

    //-----------------------------------------------------------------------------
    // the OMIT_CHILDREN_F flag operates in a similar fashion to the OMIT_SHARED_F flag:
    // when set, a resource is not included in a group if the resource exists in one
    // of the child (c.f. shared) groups; as a result, it is not demonstrated here
    //-----------------------------------------------------------------------------

    IwGxPrintString(0,  50, "Using resource searches");
    IwGxPrintString(0,  80, display);

    // Flush and swap
    IwGxFlush();
    IwGxSwapBuffers();
}
//-----------------------------------------------------------------------------
