/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
/*
 * CMad.h
 *
 * Completely arbitrary class, used to illustrate resource handling as part of ExampleIwResManager5
 */
//-----------------------------------------------------------------------------

#ifndef CMAD_H
#define CMAD_H

#include "IwResource.h"
#include "IwTextParserITX.h"

//-----------------------------------------------------------------------------
// CMad
//-----------------------------------------------------------------------------
class CMad : public CIwResource
{
public:
    IW_MANAGED_DECLARE(CMad);

    // arbitrary member
    int32 m_RandomNumber;

    // override of CIwManaged::Serialise
    void Serialise()
    {
        CIwResource::Serialise();

        // serialise the only member
        IwSerialiseInt32(m_RandomNumber);
    }

    // override of CIwManaged::ParseAttribute
    bool ParseAttribute(CIwTextParserITX* pParser, const char* pAttrName)
    {
        if (!strcmp(pAttrName, "random"))
            // read the value of the next token into m_RandomNumber
            pParser->ReadInt32(&m_RandomNumber);
        else
            return CIwManaged::ParseAttribute(pParser, pAttrName);

        return true;
    }

    // override of CIwManaged::ParseClose
    void ParseClose(CIwTextParserITX* pParser)
    {
        // add to current resource group
        IwGetResManager()->GetCurrentGroup()->AddRes("CMad", this);
    }
};
//-----------------------------------------------------------------------------

#endif
