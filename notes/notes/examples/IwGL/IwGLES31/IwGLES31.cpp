/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwGLES3 IwGL OpenGL ES 3.1 Example
 *
 * This example demonstrates the use of IwGL with an OpenGL ES 3.1 codebase.
 *
 * This example is based on s3eGLES2, which is in turn based on an
 * example from the maemo project and was updated by the guys at webos-internals:
 * http://www.webos-internals.org/wiki/OpenGL_Triangle_of_Doom
 *
 * Shader binaries need to be generated before you can run this example on
 * Windows Phone 8. This example is set up to compile shaders and save them to file
 * (in the data-ram/shader_bin_wp8 directory) when run on Windows desktop.
 *
 *
 * Alternatively you could compile the shader binaries as part of the build process,
 * and call glShaderBinary directly, the s3eGLES3 example takes this approach.
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwGLES31Image.png
 *
 * @include IwGLES31.cpp
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "IwGL.h"

#include "s3e.h"

static int eglInit()
{
    if (!IwGLInit() )
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "eglInit failed");
        return 1;
    }

    return 0;
}

const char* vertexSrc = "#version 300 es\n"
    "in highp vec4 position;"
    "out mediump vec2 texCoord;"
    "void main()"
    "{"
    "    gl_Position = position;"
    "    texCoord = position.xy*0.5f + 0.5f;"
    "}";
const char* fragmentSrc = "#version 310 es\n"
    "uniform sampler2D srcTex;"
    "in mediump vec2 texCoord;"
    "layout (location = 0) out lowp vec4 oColour;"
    "void main (void)"
    "{"
    "    mediump float c = texture(srcTex, texCoord).x;"
    "    oColour = vec4(c, 1.0, 1.0, 1.0);"
    "}";
const char* computeSrc = "#version 310 es\n"
    "uniform float roll;"
    "layout(r32f) uniform writeonly highp image2D destTex;"
    "layout (local_size_x = 16, local_size_y = 16) in;"
    "void main() {"
    "    ivec2 storePos = ivec2(gl_GlobalInvocationID.xy);"
    "    float localCoef = length(vec2(ivec2(gl_LocalInvocationID.xy)-8)/8.0);"
    "    float globalCoef = sin(float(gl_WorkGroupID.x+gl_WorkGroupID.y)*0.1 + roll)*0.5;"
    "    imageStore(destTex, storePos, vec4(1.0-globalCoef*localCoef, 0.0, 0.0, 0.0));"
    "}";

void printShaderInfoLog(GLuint shader)
{
    GLint length;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
    if (length > 1)
    {
        char* buffer = (char*)s3eMalloc( sizeof(char) * length ) ;
        glGetShaderInfoLog(shader, length, NULL, buffer);
        printf("%s", buffer);
        s3eFree( buffer ) ;
        GLint success;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
        if (success != GL_TRUE) {
            exit(1);
        }
    }
}
void printProgramInfoLog(GLuint program)
{
    GLint length;
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
    if (length > 1)
    {
        char* buffer = (char*)s3eMalloc( sizeof(char) * length ) ;
        glGetProgramInfoLog(program, length, NULL, buffer);
        printf("%s", buffer);
        s3eFree( buffer ) ;
        GLint success;
        glGetProgramiv(program, GL_LINK_STATUS, &success);
        if (success != GL_TRUE) {
            exit(1);
        }
    }
}

GLuint createShader(GLenum type, const char* pSource)
{
    GLuint shader = glCreateShader(type);
    glShaderSource(shader, 1, &pSource, NULL);
    glCompileShader(shader);
    printShaderInfoLog(shader);
    return shader;
}

int texLocation;
int rollLocation;
GLuint shaderProgram;
GLuint shaderCProgram;

const float vertexArray[] = {
    0, -1, 0, 1,
    1, 1, 0, 1,
    -1, 1, 0, 1
};

int frame = 0;

void render()
{
    int w = s3eSurfaceGetInt(S3E_SURFACE_WIDTH);
    int h = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT);
    glViewport(0, 0, w, h) ;
    glClearColor(0, 1, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    glUseProgram(shaderCProgram);
    glUniform1f(rollLocation, (float)frame*0.01f);

    glDispatchCompute(512/16, 512/16, 1);

    glUseProgram(shaderProgram);
    glUniform1i(texLocation, 0);

    glVertexAttribPointer(0, 4, GL_FLOAT, false, 0, vertexArray);
    glEnableVertexAttribArray(0);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 3);

    // Call IwGL swap instead of egl directly
    IwGLSwapBuffers();

    frame++;
}

int main()
{
    if (eglInit())
       return 1;
    printf("\n");

    int version = IwGLGetInt(IW_GL_VERSION);
    if (version < 0x310)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "This example requires GLES v3.1");
        return 1;
    }

    printf("Screen BPP: %d\n", s3eSurfaceGetInt(S3E_SURFACE_PIXEL_TYPE) & S3E_SURFACE_PIXEL_SIZE_MASK);
    printf("\n");
    printf( "Vendor     : %s\n", (char*)glGetString(GL_VENDOR));
    printf( "Renderer   : %s\n", (char*)glGetString(GL_RENDERER));
    printf( "Version    : %s\n", (char*)glGetString(GL_VERSION));

    int max = 0;
    glGetIntegerv(GL_NUM_EXTENSIONS, &max);

    for (int i = 0; i<max; i++)
    {
        const GLubyte* str = glGetStringi(GL_EXTENSIONS, i);
        if (str == NULL)
            break;
        printf( "  Extension : %s\n", str);
    }

    shaderProgram = glCreateProgram();
    GLuint vertexShader = createShader(GL_VERTEX_SHADER, vertexSrc);
    GLuint fragmentShader = createShader(GL_FRAGMENT_SHADER, fragmentSrc);

    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);

    glLinkProgram(shaderProgram);
    printProgramInfoLog(shaderProgram);

    glUseProgram(shaderProgram);

    texLocation = glGetUniformLocation(shaderProgram, "srcTex");

    if (texLocation < 0)
    {
        s3eDebugErrorPrintf("Unable to get uniform location");
        glDeleteProgram(shaderProgram);
        return 1;
    }

    GLuint texHandle;
    glGenTextures(1, &texHandle);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texHandle);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    //glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, 512, 512, 0, GL_RED, GL_FLOAT, NULL);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_R32F, 512, 512);

    shaderCProgram = glCreateProgram();
    GLuint computeShader = createShader(GL_COMPUTE_SHADER, computeSrc);

    glAttachShader(shaderCProgram, computeShader);

    glLinkProgram(shaderCProgram);
    printProgramInfoLog(shaderCProgram);

    glUseProgram(shaderCProgram);

    glBindImageTexture(0, texHandle, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R32F);

    rollLocation = glGetUniformLocation(shaderCProgram, "roll");

    if (rollLocation < 0)
    {
        s3eDebugErrorPrintf("Unable to get uniform location");
        glDeleteProgram(shaderCProgram);
        return 1;
    }

    bool quit = false;

    int numFrames = 0;

    while (!quit) {
                s3eKeyboardUpdate();
                s3eDeviceYield(0);
                if (s3eDeviceCheckQuitRequest())
                    quit = 1;
                if (s3eKeyboardGetState(s3eKeyEsc) & S3E_KEY_STATE_PRESSED)
                    quit = 1;
                render();
                numFrames++;
    }
    //Shutdown GL system
    IwGLTerminate();
    return 0;
}
