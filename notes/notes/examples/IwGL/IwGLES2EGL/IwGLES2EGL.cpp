/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwGLES2EGL IwGL OpenGL ES 2.x Example
 *
 * This example demonstrates the use of IwGL with an OpenGL ES 2.x codebase.
 *
 * It also demonstrates a custom setup of EGL before calling IwGL which also
 * needs to be called on resume
 *
 * This example is based on s3eGLES2, which is in turn based on an
 * example from the maemo project and was updated by the guys at webos-internals:
 * http://www.webos-internals.org/wiki/OpenGL_Triangle_of_Doom
 *
 * Shader binaries need to be generated before you can run this example on
 * Windows Phone 8 or Windows Store 8. This example is set up to compile shaders and
 * save them to file:
 *  - in the data-ram/shader_bin_wp8 directory for Windows Phone 8
 *  - in the data-ram/shader_bin_win8 directory for Windows Store 8
 * when run on Windows desktop.
 *
 *
 * Alternatively you could compile the shader binaries as part of the build process,
 * and call glShaderBinary directly, the s3eGLES2 example takes this approach.
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwGLES2Image.png
 *
 * @include IwGLES2EGL.cpp
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "IwGL.h"

#include "s3e.h"

static EGLSurface g_EGLSurface = NULL;
static EGLDisplay g_EGLDisplay = NULL;
static EGLContext g_EGLContext = NULL;

#define MAX_CONFIG 32

static bool eglInitReal()
{
    EGLint major;
    EGLint minor;
    EGLint numFound = 0;
    EGLConfig configList[MAX_CONFIG];

    g_EGLDisplay = eglGetDisplay(EGL_DEFAULT_DISPLAY);
    if (!g_EGLDisplay)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "eglGetDisplay failed");
        return false;
    }

    EGLBoolean res = eglInitialize(g_EGLDisplay, &major, &minor);
    if (!res)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "eglInitialize failed");
        return false;
    }

    eglGetConfigs(g_EGLDisplay, configList, MAX_CONFIG, &numFound);
    if (!numFound)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "eglGetConfigs failed to find any configs");
        return false;
    }

    int requiredRedDepth = 8, requiredDepth = 0;
    bool requiredFound = s3eConfigGetInt("GL", "EGL_RED_SIZE", &requiredRedDepth) == S3E_RESULT_SUCCESS;
    s3eConfigGetInt("GL", "EGL_DEPTH_SIZE", &requiredDepth);

    int config = -1;
    int actualRedDepth = 0;
    int actualDepth = 0;
    printf("found %d configs\n", numFound);
    for (int i = 0; i < numFound; i++)
    {
        EGLint renderable = 0;
        EGLint surfacetype = 0;
        eglGetConfigAttrib(g_EGLDisplay, configList[i], EGL_RENDERABLE_TYPE, &renderable);
        eglGetConfigAttrib(g_EGLDisplay, configList[i], EGL_RED_SIZE, &actualRedDepth);
        eglGetConfigAttrib(g_EGLDisplay, configList[i], EGL_DEPTH_SIZE, &actualDepth);
        eglGetConfigAttrib(g_EGLDisplay, configList[i], EGL_SURFACE_TYPE, &surfacetype);

        if ((!requiredFound || (actualRedDepth == requiredRedDepth)) && (requiredDepth == 0 || (actualDepth == requiredDepth)) &&
            (renderable & EGL_OPENGL_ES2_BIT) && (surfacetype & EGL_WINDOW_BIT))
        {
            config = i;
            break;
        }
    }

    if (config == -1)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "No GLES2 configs reported.  Trying random config");
        config = 0;
    }

    int version = s3eGLGetInt(S3E_GL_VERSION)>>8;
    printf("requesting GL version: %d\n", version);

    int configid;
    eglGetConfigAttrib(g_EGLDisplay, configList[config], EGL_CONFIG_ID, &configid);
    printf("choosing config with EGL ID: %d\n", configid);

    EGLint attribs[] = { EGL_CONTEXT_CLIENT_VERSION, version, EGL_NONE, };
    g_EGLContext = eglCreateContext(g_EGLDisplay, configList[config], NULL, attribs);
    if (!g_EGLContext)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "eglCreateContext failed");
        return false;
    }

    version = s3eGLGetInt(S3E_GL_VERSION)>>8;
    if (version != 2)
    {
        printf("reported GL version: %d", version);
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "This example requires GLES v2.x");
        return false;
    }

    void* nativeWindow = s3eGLGetNativeWindow();
    g_EGLSurface = eglCreateWindowSurface(g_EGLDisplay, configList[config], nativeWindow, NULL);
    eglMakeCurrent(g_EGLDisplay, g_EGLSurface, g_EGLSurface, g_EGLContext);

    return true;
}

int _eglResume(void*, void*)
{
    eglInitReal();

    return 0;
}

static int eglInit()
{
    // set up EGL as we want it before calling IwGL
    if (!eglInitReal())
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "eglInit failed");
        return 1;
    }

    // add resume callback to re-setup IwGL
    if(s3eGLGetInt(S3E_GL_MUST_SUSPEND))
    {
        s3eGLRegister(S3E_GL_RESUME, _eglResume, NULL);
    }

    // IwGL uses the EGL setup we have specified
    if (!IwGLInit() )
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "IwGLInit failed");
        return 1;
    }

    return 0;
}

static void eglTerm()
{
    IwGLTerminate();

    if(s3eGLGetInt(S3E_GL_MUST_SUSPEND))
    {
        s3eGLUnRegister(S3E_GL_RESUME, _eglResume);
    }

    if (g_EGLDisplay)
    {
        eglMakeCurrent(g_EGLDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
        eglDestroySurface(g_EGLDisplay, g_EGLSurface);
        eglDestroyContext(g_EGLDisplay, g_EGLContext);
    }
    eglTerminate(g_EGLDisplay);
    g_EGLDisplay = 0;
}

const char* vertexSrc = "attribute vec4 position; uniform highp mat4 mat; varying mediump vec2 pos; void main() { gl_Position = position * mat; pos = position.xy; }";
const char* fragmentSrc = "varying mediump vec2 pos; uniform mediump float phase; void main() { gl_FragColor = vec4(1, 1, 1, 1) * sin((pos.x * pos.x + pos.y * pos.y) * 40.0 + phase); }";
const char* vertex93Src = "attribute vec4 position; uniform highp mat4 mat; varying mediump vec2 pos; void main() { gl_Position = position * mat; pos = position.xy; }";
const char* fragment93Src = "varying mediump vec2 pos; uniform mediump float phase; void main() { gl_FragColor = vec4(1, 1, 1, 1) * sin((pos.x * pos.x + pos.y * pos.y) * 1.0 + phase); }";

void printShaderInfoLog(GLuint shader)
{
    GLint length;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
    if (length)
    {
        char* buffer = (char*)malloc( sizeof(char) * length ) ;
        glGetShaderInfoLog(shader, length, NULL, buffer);
        printf("%s", buffer);
        free( buffer ) ;
        GLint success;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
        if (success != GL_TRUE) {
            exit(1);
        }
    }
}

GLuint createShader(GLenum type, const char* pSource)
{
    GLuint shader = glCreateShader(type);
    glShaderSource(shader, 1, &pSource, NULL);
    glCompileShader(shader);
    printShaderInfoLog(shader);
    return shader;
}

int phaseLocation ;
int matLocation ;

const float vertexArray[] = {
    0, -1, 0, 1,
    1, 1, 0, 1,
    -1, 1, 0, 1
};

void render()
{
    static float offset = 0;

    // Get dimensions from IwGL
    int w = IwGLGetInt(IW_GL_WIDTH);
    int h = IwGLGetInt(IW_GL_HEIGHT);

    glViewport(0, 0, w, h) ;
    glClearColor(0, 1, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUniform1f(phaseLocation, offset);

    // The OpenGL2 API doesn't not allow IwGL to perform rotation
    // automatically (the pipeline is too flexible) so we must
    // communicate with the shader to rotate.
    float angle = IwGLGetInt(IW_GL_ROTATE) * 3.141f / 2;
    float m[16] = { cosf(angle), sinf(angle), 0, 0,
                -sinf(angle), cosf(angle), 0, 0,
                0, 0, 1.0f, 0,
                0, 0, 0, 1.0f};

    glUniformMatrix4fv(matLocation, 1, GL_FALSE, m);
    glVertexAttribPointer(0, 4, GL_FLOAT, false, 0, vertexArray);
    glEnableVertexAttribArray(0);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 3);

    // Call IwGL swap instead of egl directly
    IwGLSwapBuffers();

    offset = fmodf(offset + 0.2f, 2*3.141f);
}

int main()
{
    if (eglInit())
       return 1;

    GLint depthBits = 0;
    glGetIntegerv(GL_DEPTH_BITS, &depthBits);
    printf("Screen BPP: %d\n", s3eSurfaceGetInt(S3E_SURFACE_PIXEL_TYPE) & S3E_SURFACE_PIXEL_SIZE_MASK);
    printf("Depth bits : %d\n", depthBits);
    printf("\n");
    printf( "Vendor     : %s\n", (char*)glGetString(GL_VENDOR));
    printf( "Renderer   : %s\n", (char*)glGetString(GL_RENDERER));
    printf( "Version    : %s\n", (char*)glGetString(GL_VERSION));
    printf( "Extensions : %s\n", (char*)glGetString(GL_EXTENSIONS));
    printf("\n");


    GLuint shaderProgram = glCreateProgram();
    GLuint vertexShader = 0;
    GLuint fragmentShader = 0;

    if ((s3eDeviceGetInt(S3E_DEVICE_OS) != S3E_OS_ID_WS8 &&
         s3eDeviceGetInt(S3E_DEVICE_OS) != S3E_OS_ID_WS81 &&
         s3eDeviceGetInt(S3E_DEVICE_OS) != S3E_OS_ID_WIN10
        ) ||
        s3eDeviceGetInt(S3E_DEVICE_DX_FEATURE_LEVEL) < 93
       )
    {
        vertexShader = createShader(GL_VERTEX_SHADER, vertexSrc);
        fragmentShader = createShader(GL_FRAGMENT_SHADER, fragmentSrc);

        // It is need to compile additional shaders for Windwos Store 8 by Win32 Marmalade Simulator
        // For more information look at README.IwGLES2.txt.
        if (IwGetCompileShadersPlatformType() == IW_CS_OS_ID_WS8 ||
            IwGetCompileShadersPlatformType() == IW_CS_OS_ID_WS81 ||
            IwGetCompileShadersPlatformType() == IW_CS_OS_ID_WIN10
           )
        {
            s3eRegisterShader(vertex93Src, IW_GL_ST_VERTEX, IW_DX_FL_9_3);
            s3eRegisterShader(fragment93Src, IW_GL_ST_PIXEL, IW_DX_FL_9_3);
        }
    }
    else
    {
        vertexShader = createShader(GL_VERTEX_SHADER, vertex93Src);
        fragmentShader = createShader(GL_FRAGMENT_SHADER, fragment93Src);
    }

    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);

    glLinkProgram(shaderProgram);

    glUseProgram(shaderProgram);

    phaseLocation = glGetUniformLocation(shaderProgram, "phase");
    if (phaseLocation < 0) {
        printf("Unable to get uniform location\n");
        return 1;
    }

    matLocation = glGetUniformLocation(shaderProgram, "mat");
    if (matLocation < 0) {
       printf("Unable to get uniform location\n");
       return 1;
    }

    bool quit = false;

    int numFrames = 0;

    while (!quit) {
                s3eKeyboardUpdate();
                s3eDeviceYield(0);
                if (s3eDeviceCheckQuitRequest())
                    quit = 1;
                if (s3eKeyboardGetState(s3eKeyEsc) & S3E_KEY_STATE_PRESSED)
                    quit = 1;
                render();
                numFrames++;
    }
    //Shutdown GL system
    eglTerm();
    return 0;
}
