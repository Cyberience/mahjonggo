#category: Graphics
IwGLES2
=================

This example demonstrates the use of IwGL with an OpenGL ES 2.x codebase.

Shader binaries need to be generated before you can run this example
on Windows Phone 8. This example is set up to compile shaders and save them
to file (in the data-ram/shader_bin_wp8 directory) when run on Windows desktop.

Alternatively you could compile the shader binaries as part of the build
process, and call glShaderBinary directly, the s3eGLES2 example takes this
approach.

Windows Store 8
===============
Windows Store 8 can not compile shaders in runtime, such ability appear from Windows Store 8.1.
So this example also show how to build precompiled shaders(for different DirectX feature level) by running Win32 Marmalade Simulator.

Why for different DirectX feature level?
Because some shaders can not be compiled on lowest feature level. For example:
derivatives (fwidth, ddy, ddx) in shades can be used only from DX feature level 9.3.
So you may need to use different shaders for different feature levels in runtime for the same place.
What is the feature levels: http://msdn.microsoft.com/en-us/library/windows/desktop/ff476876%28v=vs.85%29.aspx

We have icf parameter:
[GL]
DXFeatureLevel=91

which identify for which feature level you application will be running.
But feature level specified in this parameter should be supported on your device.

This example will use different shader in runtime for Window Store 8 and Window Store 8.1 if you specify:
DXFeatureLevel=91 or DXFeatureLevel=93 and will have different picture.

To compile shaders for Windows Store 8 by Win32 Marmalade Simulator you should set next icf parameters:
[GL]
PreCompileShaderBinariesToolPath="angle_dx11/win8/compiletool.exe"
PreCompileShaderBinariesOutputDir="shader_bin_win8"
PreCompileShaderBinaries=1
PreCompileShaderBinariesPlatform="WIN8"
#You can also specify minimum feature level supported by your application to compile shaders for it:
PreCompileShaderBinariesArgs="fl_9_1"

DX Feature level | DXFeatureLevel value | PreCompileShaderBinariesArgs value
9.1                91                     fl_9_1
9.2                92                     fl_9_2
9.3                93                     fl_9_3
10.0               100                    fl_10_0
10.1               101                    fl_10_1
11.0               110                    fl_11_0
11.1               111                    fl_11_1

To specify additional shaders to compile for different, than minimum, feature level you could use functions:
s3eRegisterShader and IwGetCompileShadersPlatformType
For more information about this functions please look at: modules\iwgl\h\IwGL.h and how it could be used in IwGLES2.cpp
