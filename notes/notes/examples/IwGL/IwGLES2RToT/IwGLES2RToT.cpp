/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwGLES2 IwGL OpenGL ES 2.x Example
 *
 * This example demonstrates the use of IwGL with an OpenGL ES 2.x codebase.
 *
 * This example is based on s3eGLES2, which is in turn based on an
 * example from the maemo project and was updated by the guys at webos-internals:
 * http://www.webos-internals.org/wiki/OpenGL_Triangle_of_Doom
 *
 * Shader binaries need to be generated before you can run this example on
 * Windows Phone 8. This example is set up to compile shaders and save them to file
 * (in the data-ram/shader_bin_wp8 directory) when run on Windows desktop.
 *
 *
 * Alternatively you could compile the shader binaries as part of the build process,
 * and call glShaderBinary directly, the s3eGLES2 example takes this approach.
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwGLES2Image.png
 *
 * @include IwGLES2.cpp
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "IwGL.h"

#include "s3e.h"

static int eglInit()
{
    if (!IwGLInit() )
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "eglInit failed");
        return 1;
    }

    return 0;
}

const char* vertexSrc = "attribute vec4 position; uniform highp mat4 mat; varying mediump vec2 pos; void main() { gl_Position = position * mat; pos = position.xy; }";
const char* fragmentSrc = "varying mediump vec2 pos; uniform mediump float phase; void main() { gl_FragColor = vec4(1, 1, 1, 1) * sin((pos.x * pos.x + pos.y * pos.y) * 40.0 + phase); }";

const char* vertexSrc2 = "attribute vec4 position;"
    "uniform highp mat4 mat;"
    "varying mediump vec2 uv;"
    "void main() {"
    "    gl_Position = position * mat;"
    "    uv.xy = (position.xy + 1.0)/2.0;"
    "}";
const char* fragmentSrc2 = "varying mediump vec2 uv;"
    "uniform sampler2D texture;"
    "void main() {"
    "    gl_FragColor = texture2D(texture, uv);"
    "}";

void printShaderInfoLog(GLuint shader)
{
    GLint length;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
    if (length)
    {
        char* buffer = (char*)malloc( sizeof(char) * length ) ;
        glGetShaderInfoLog(shader, length, NULL, buffer);
        printf("%s", buffer);
        free( buffer ) ;
        GLint success;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
        if (success != GL_TRUE) {
            exit(1);
        }
    }
}

GLuint createShader(GLenum type, const char* pSource)
{
    GLuint shader = glCreateShader(type);
    glShaderSource(shader, 1, &pSource, NULL);
    glCompileShader(shader);
    printShaderInfoLog(shader);
    return shader;
}

int phaseLocation;
int matLocation;
int matLocation2;
int textureLocation;
GLuint shaderProgram;
GLuint shaderProgram2;

const float vertexArray[] = {
    0, -1, 0, 1,
    1, 1, 0, 1,
    -1, 1, 0, 1
};

const float vertexArray2[] = {
    -0.9f, -1.0f, 0, 1,
    0.9f, 1.0f, 0, 1,
    -1.0f, 0.9f, 0, 1,
    -0.9f, -1.0f, 0, 1,
    1.0f, -0.9f, 0, 1,
    0.9f, 1.0f, 0, 1
};

void render()
{
    static float offset = 0;

    glClearColor(0, 1, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUseProgram(shaderProgram);

    glUniform1f(phaseLocation, offset);

    // The OpenGL2 API doesn't not allow IwGL to perform rotation
    // automatically (the pipeline is too flexible) so we must
    // communicate with the shader to rotate.
    float angle = IwGLGetInt(IW_GL_ROTATE) * 3.141f / 2;
    float m[16] = { cosf(angle), sinf(angle), 0, 0,
                -sinf(angle), cosf(angle), 0, 0,
                0, 0, 1.0f, 0,
                0, 0, 0, 1.0f};

    glUniformMatrix4fv(matLocation, 1, GL_FALSE, m);
    glVertexAttribPointer(0, 4, GL_FLOAT, false, 0, vertexArray);
    glEnableVertexAttribArray(0);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 3);

    offset = fmodf(offset + 0.2f, 2*3.141f);
}

void render2()
{
    glClearColor(0, 0, 1, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // The OpenGL2 API doesn't not allow IwGL to perform rotation
    // automatically (the pipeline is too flexible) so we must
    // communicate with the shader to rotate.
    float angle = IwGLGetInt(IW_GL_ROTATE) * 3.141f / 2;
    float m[16] = { cosf(angle), sinf(angle), 0, 0,
                -sinf(angle), cosf(angle), 0, 0,
                0, 0, 1.0f, 0,
                0, 0, 0, 1.0f};

    glUseProgram(shaderProgram2);

    glUniformMatrix4fv(matLocation2, 1, GL_FALSE, m);
    glVertexAttribPointer(0, 4, GL_FLOAT, false, 0, vertexArray2);
    glEnableVertexAttribArray(0);
    glDrawArrays(GL_TRIANGLES, 0, 6);
}

int main()
{
    if (eglInit())
       return 1;

    printf("Screen BPP: %d\n", s3eSurfaceGetInt(S3E_SURFACE_PIXEL_TYPE) & S3E_SURFACE_PIXEL_SIZE_MASK);
    printf("\n");
    printf( "Vendor     : %s\n", (char*)glGetString(GL_VENDOR));
    printf( "Renderer   : %s\n", (char*)glGetString(GL_RENDERER));
    printf( "Version    : %s\n", (char*)glGetString(GL_VERSION));
    printf( "Extensions : %s\n", (char*)glGetString(GL_EXTENSIONS));
    printf("\n");

    shaderProgram = glCreateProgram();
    GLuint vertexShader = createShader(GL_VERTEX_SHADER, vertexSrc);
    GLuint fragmentShader = createShader(GL_FRAGMENT_SHADER, fragmentSrc);

    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);

    glLinkProgram(shaderProgram);

    glUseProgram(shaderProgram);

    phaseLocation = glGetUniformLocation(shaderProgram, "phase");
    if (phaseLocation < 0) {
        printf("Unable to get uniform location\n");
        return 1;
    }

    matLocation = glGetUniformLocation(shaderProgram, "mat");
    if (matLocation < 0) {
       printf("Unable to get uniform location\n");
       return 1;
    }

    shaderProgram2 = glCreateProgram();
    GLuint vertexShader2 = createShader(GL_VERTEX_SHADER, vertexSrc2);
    GLuint fragmentShader2 = createShader(GL_FRAGMENT_SHADER, fragmentSrc2);

    glAttachShader(shaderProgram2, vertexShader2);
    glAttachShader(shaderProgram2, fragmentShader2);

    glLinkProgram(shaderProgram2);

    glUseProgram(shaderProgram2);

    matLocation2 = glGetUniformLocation(shaderProgram2, "mat");
    if (matLocation2 < 0) {
       printf("Unable to get uniform location\n");
       return 1;
    }

    textureLocation = glGetUniformLocation(shaderProgram2, "texture");

    // The framebuffer, which regroups 0, 1, or more textures, and 0 or 1 depth buffer.
    GLuint FramebufferName = 0;
    glGenFramebuffers(1, &FramebufferName);

    // The texture we're going to render to
    GLuint renderedTexture;
    glGenTextures(1, &renderedTexture);

    // "Bind" the newly created texture : all future texture functions will modify this texture
    glBindTexture(GL_TEXTURE_2D, renderedTexture);

    // Give an empty image to OpenGL ( the last "0" )
    // Some platforms need power of 2 textures (iOS?)
    glTexImage2D(GL_TEXTURE_2D, 0,GL_RGB, 128, 128, 0,GL_RGB, GL_UNSIGNED_BYTE, 0);

    // Poor filtering. Needed !
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, renderedTexture, 0);

    glUniform1i(textureLocation, 0);

    bool quit = false;

    int numFrames = 0;

    while (!quit) {
                s3eKeyboardUpdate();
                s3eDeviceYield(0);
                if (s3eDeviceCheckQuitRequest())
                    quit = 1;
                if (s3eKeyboardGetState(s3eKeyEsc) & S3E_KEY_STATE_PRESSED)
                    quit = 1;

                glViewport(0, 0, 128, 128);

                glBindFramebuffer(GL_FRAMEBUFFER, FramebufferName);
                render();

                // Get dimensions from IwGL
                int w = IwGLGetInt(IW_GL_WIDTH);
                int h = IwGLGetInt(IW_GL_HEIGHT);

                glViewport(0, 0, w, h);
                glBindFramebuffer(GL_FRAMEBUFFER, 0);
                render2();

                // Call IwGL swap instead of egl directly
                IwGLSwapBuffers();

                numFrames++;
    }
    //Shutdown GL system
    IwGLTerminate();
    return 0;
}
