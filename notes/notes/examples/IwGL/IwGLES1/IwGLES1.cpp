/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
/**
 * @page ExampleIwGLES1 IwGL OpenGL ES 1.x Example
 *
 * This example demonstrates the use of IwGL with an OpenGL ES 1.x codebase.
 *
 * This example is based on s3eGLES1, which is in turn based on the SDL gltest
 * code and was updated to support GLES by the guys at webos-internals:
 * http://www.webos-internals.org/wiki/GLES_1.1_3D_Cube_Demo
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwGLES1Image.png
 *
 * @include IwGLES1.cpp
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "IwGL.h"

#include "s3e.h"

void RunGLTest(int logo, int logocursor, int slowly, int bpp, float gamma, int noframe, int fsaa, int sync, int accel)
{
    // Get dimensions from IwGL
    int w = IwGLGetInt(IW_GL_WIDTH);
    int h = IwGLGetInt(IW_GL_HEIGHT);
    int done = 0;
    int frames;
    int start_time, this_time;
    GLfloat color[96] = {
        1.0,  1.0,  0.0, 1.0,  // 0
        1.0,  0.0,  0.0,  1.0, // 1
        0.0,  1.0,  0.0, 1.0,  // 3
        0.0,  0.0,  0.0,  1.0, // 2

        0.0,  1.0,  0.0, 1.0,  // 3
        0.0,  1.0,  1.0,  1.0, // 4
        0.0,  0.0,  0.0, 1.0,  // 2
        0.0,  0.0,  1.0, 1.0,  // 7

        1.0,  1.0,  0.0, 1.0,  // 0
        1.0,  1.0,  1.0, 1.0,  // 5
        1.0,  0.0,  0.0, 1.0,  // 1
        1.0,  0.0,  1.0, 1.0,  // 6

        1.0,  1.0,  1.0, 1.0,  // 5
        0.0,  1.0,  1.0, 1.0,  // 4
        1.0,  0.0,  1.0, 1.0,  // 6
        0.0,  0.0,  1.0, 1.0,  // 7

        1.0,  1.0,  1.0, 1.0,  // 5
        1.0,  1.0,  0.0, 1.0,  // 0
        0.0,  1.0,  1.0, 1.0,  // 4
        0.0,  1.0,  0.0, 1.0,  // 3

        1.0,  0.0,  1.0, 1.0,  // 6
        1.0,  0.0,  0.0, 1.0,  // 1
        0.0,  0.0,  1.0, 1.0,  // 7
        0.0,  0.0,  0.0, 1.0,  // 2
    };

    GLfloat cube[72] = {

        0.5,  0.5, -0.5,   // 0
        0.5, -0.5, -0.5,   // 1
        -0.5,  0.5, -0.5,   // 3
        -0.5, -0.5, -0.5,   // 2

        -0.5,  0.5, -0.5,   // 3
        -0.5,  0.5,  0.5,   // 4
        -0.5, -0.5, -0.5,   // 2
        -0.5, -0.5,  0.5,   // 7

        0.5,  0.5, -0.5,   // 0
        0.5,  0.5,  0.5,   // 5
        0.5, -0.5, -0.5,   // 1
        0.5, -0.5,  0.5,   // 6

        0.5,  0.5,  0.5,   // 5
        -0.5,  0.5,  0.5,   // 4
        0.5, -0.5,  0.5,   // 6
        -0.5, -0.5,  0.5,   // 7

        0.5,  0.5,  0.5,   // 5
        0.5,  0.5, -0.5,   // 0
        -0.5,  0.5,  0.5,   // 4
        -0.5,  0.5, -0.5,   // 3

        0.5, -0.5,  0.5,   // 6
        0.5, -0.5, -0.5,   // 1
        -0.5, -0.5,  0.5,   // 7
        -0.5, -0.5, -0.5,   // 2
    };

    printf("Screen BPP  : %d\n", s3eSurfaceGetInt(S3E_SURFACE_PIXEL_TYPE) & S3E_SURFACE_PIXEL_SIZE_MASK);
    printf("Screen Size : %dx%d\n", w, h);
    printf("\n");
    printf( "Vendor     : %s\n", (const char*)glGetString( GL_VENDOR ) );
    printf( "Renderer   : %s\n", (const char*)glGetString( GL_RENDERER ) );
    printf( "Version    : %s\n", (const char*)glGetString( GL_VERSION ) );
    printf( "Extensions : %s\n", (const char*)glGetString( GL_EXTENSIONS ) );
    printf("\n");

    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity( );

    glEnable(GL_DEPTH_TEST);

    glDepthFunc(GL_LESS);

    glShadeModel(GL_SMOOTH);

    /* Loop until done. */
    start_time = (int)s3eTimerGetMs();
    frames = 0;
    while (!done)
    {
        GLenum gl_error;

        //To take advantage of IwGL's automatic screen rotation support, the
        //projection matrix and viewport should be set up every frame.
        int w = IwGLGetInt(IW_GL_WIDTH);
        int h = IwGLGetInt(IW_GL_HEIGHT);
        glViewport( 0, 0, w, h );
        glMatrixMode( GL_PROJECTION );
        glLoadIdentity( );

        glOrthof( -2.0, 2.0, -2.0, 2.0, -20.0, 20.0 );

        /* Do our drawing, too. */
        glClearColor(0.0, 0.0, 0.0, 1.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_COLOR_ARRAY);
        glColorPointer(4, GL_FLOAT, 0, color);
        glVertexPointer(3, GL_FLOAT, 0, cube);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 24);
        glDisableClientState(GL_VERTEX_ARRAY);
        glDisableClientState(GL_COLOR_ARRAY);
        glMatrixMode(GL_MODELVIEW);
        glRotatef(5.0, 1.0, 1.0, 1.0);

        // Call IwGL swap instead of egl directly
        IwGLSwapBuffers();

        /* Check for error conditions. */
        gl_error = glGetError();

        if (gl_error != GL_NO_ERROR)
        {
            fprintf( stderr, "testgl: OpenGL error: %#x\n", gl_error );
        }

        /* Allow the user to see what's happening */
        if (slowly)
        {
            s3eDeviceYield(20);
        }
        else
        {
            s3eDeviceYield(0);
        }

        s3eKeyboardUpdate();

        if (s3eDeviceCheckQuitRequest())
            done = 1;

        if (s3eKeyboardGetState(s3eKeyEsc) & S3E_KEY_STATE_PRESSED)
            done = 1;

        ++frames;

        if (frames > 50)
        {
            /* Print out the frames per second */
            this_time = (int)s3eTimerGetMs();
            if (this_time != start_time)
            {
                printf("%2.2f FPS\n", ((float)frames/(this_time-start_time))*1000.0);
            }
            start_time = this_time;
            frames = 0;
        }
    }

}

static int eglInit()
{
    if (!IwGLInit())
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "eglInit failed");
        return 1;
    }

    return 0;
}

int main(int argc, char* argv[])
{
    int logo = 0;
    int logocursor = 0;
    int bpp = 0;
    int slowly = 1;
    float gamma = 0.0;
    int noframe = 0;
    int fsaa = 0;
    int accel = 0;
    int sync = 0;
    if (eglInit())
        return 1;
    RunGLTest(logo, logocursor, slowly, bpp, gamma, noframe, fsaa, sync, accel);
    //Shutdown GL system
    IwGLTerminate();
    return 0;
}
