/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwGLVirtualRes IwGL Virtual Resolution Example
 *
 * This example demonstrates the use of IwGL's virtual resolution feature.
 *
 * This simple drawing application is hardcoded to hVGA landscape resolution.
 *
 * The ICF file specifies the details of the handling of the virtual
 * resolution:
 * @code
 * [GL]
 * VirtualWidth=480
 * VirtualHeight=320
 * VirtualLetterbox=1
 * @endcode
 *
 * This indicates that IwGL will report a resolution of (480, 320) but stretch
 * the output to fullscreen, preserving the original aspect ratio by
 * letter-boxing.
 *
 * This process will be transparent to the application, with the following
 * exceptions:
 *  - The application should use the IW_GL_WIDTH and IW_GL_HEIGHT properties to
 *    determine screen dimensions.
 *  - The application must use IwGLTransform to convert device coordinates to virtual
 *    coordinates. This is a no-op in the case that the virtual resolution matches the
 *    device resolution.
 *  - The application should reset its projection matrix and viewport every
 *    frame (since IwGL hooks into these functions to update relevant GL state).
 *  - (OpenGL ES 2.x only) The rotation from the IW_GL_ROTATE property should
 *    be applied to any output vertices.
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwGLVirtualResImage.png
 *
 * @include IwGLVirtualRes.cpp
 */

#include "IwGL.h"

#include "s3e.h"


//Vertex streams for points
#define MAX_QUADS   2000
int16 g_Verts[MAX_QUADS*4*2];
uint16 g_Inds[MAX_QUADS*6];
uint32 g_Cols[MAX_QUADS*4];

//Colour for each touch ID
uint32 g_PointColours[S3E_POINTER_TOUCH_MAX] =
{
    0xff0000ff,
    0xff00ff00,
    0xffff0000,
    0xff00ffff,
    0xffff00ff,
    0xffffffff,
    0xff000080,
    0xff008000,
    0xff800000,
    0xff008080,
};

//-----------------------------------------------------------------------------
// Main is the application entry point
//-----------------------------------------------------------------------------
int main()
{
    //Initialise IwGL
    IwGLInit();

    //Initialise index stream
    uint16* inds = g_Inds;
    for (int n = 0; n < MAX_QUADS; n++)
    {
        //Each set of 4 vertices generates 2 triangles...

        //Start from nth Quad..
        uint16 baseInd = n*4;

        //Triangle 1
        *inds++ = baseInd;
        *inds++ = baseInd+1;
        *inds++ = baseInd+2;

        //Triangle 2
        *inds++ = baseInd;
        *inds++ = baseInd+2;
        *inds++ = baseInd+3;
    }

    //"Current" quad
    int outQuad = 0;

    //Set GL streams
    glVertexPointer(2, GL_SHORT, 0, g_Verts);
    glEnableClientState(GL_VERTEX_ARRAY);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, g_Cols);
    glEnableClientState(GL_COLOR_ARRAY);
    glClearColor(0.5, 0.5, 0.5, 1.0);

    //Loop until the OS requests quit
    while (!s3eDeviceCheckQuitRequest())
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Reset projection and viewport. IwGL applications must do
        //this every frame to use virtual resolution.
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrthof(0, 480.0f, 320.0f, 0, -10.0f, 10.0f);
        glViewport(0, 0, 480, 320);

        //Paint quads for any pointer touches
        for (int p = 0; p < S3E_POINTER_TOUCH_MAX; p++)
        {
            if (s3ePointerGetTouchState(p) & S3E_POINTER_STATE_DOWN )
            {
                //Transform from device coordinates to virtual coordinates
                CIwGLPoint point(s3ePointerGetTouchX(p), s3ePointerGetTouchY(p));

                //Move current quad to that position
                int16* quadPoints = g_Verts + 2 * 4 * outQuad;
                *quadPoints++ = point.x - 10;
                *quadPoints++ = point.y - 10;

                *quadPoints++ = point.x + 5;
                *quadPoints++ = point.y - 5;

                *quadPoints++ = point.x + 10;
                *quadPoints++ = point.y + 10;

                *quadPoints++ = point.x - 5;
                *quadPoints++ = point.y + 5;

                //Paint current quad the appropriate colour
                uint32* quadCols = g_Cols + 4 * outQuad;
                uint32 col = g_PointColours[p];
                for (int c = 0; c < 4; c++)
                {
                    *quadCols++ = col;
                }

                //Move on to the next quad (or start moving quads from the beginning)
                outQuad = (outQuad + 1) % MAX_QUADS;
            }
        }

        //Draw all quads
        glDrawElements(GL_TRIANGLES, MAX_QUADS*6, GL_UNSIGNED_SHORT, g_Inds);

        //Swap buffers
        IwGLSwapBuffers();

        //Yield and update - Every application should do this.
        s3eDeviceYield();
        s3ePointerUpdate();
    }

    //Shutdown GL system
    IwGLTerminate();

    return 0;
}
