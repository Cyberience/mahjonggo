/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwGLExt IwGL OpenGL ES Extensions Example
 *
 * This example demonstrates the use of IwGL with the GL_OES_framebuffer_object
 * extension.
 *
 * The example renders an orbiting image into a framebuffer object and then
 * uses that object to texture a rippling plane. If the framebuffer extension is not
 * available, the plane will be red.
 *
 * IwGL exposes extension functions as first class functions for all the extensions
 * listed in IwGLExt.h.
 *
 * The application should use IwGLExtAvailable to test the availability of any extension
 * before calling any functions associated with that extension. Functions for disabled
 * extensions will assert and perform no operation. If they have a return value it will
 * be 0.
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwGLExtImage.png
 *
 * @include IwGLExt.cpp
 */
#include "s3e.h"
#include "IwGL.h"
#include "IwUtil.h"
#include "math.h"

// Grid stream data
#define GRID_DIM 20
float s_Verts[GRID_DIM*GRID_DIM*3];
float s_UVs[GRID_DIM*GRID_DIM*2];
uint32 s_Cols[GRID_DIM*GRID_DIM];
uint16 s_Inds[(GRID_DIM-1)*(GRID_DIM-1)*6];

// CFBO encapsulates an OpenGL ES framebuffer object
class CFBO
{
public:
    CFBO(int width, int height, GLenum format, GLenum type)
    {
        m_Width = width;
        m_Height = height;

        // Generate a texture for the frame buffer
        glGenTextures(1, &m_Tex);
        glBindTexture(GL_TEXTURE_2D, m_Tex);

        // Disable mipmapping
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        if (IwGLExtAvailable(IW_GL_OES_framebuffer_object) )
        {
            // Reserve storage
            glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, type, 0);
            glBindTexture(GL_TEXTURE_2D, 0);

            // Use the framebuffer extension to create a framebuffer object.
            // Note that functions can be called directly.
            glGenFramebuffersOES(1, &m_FBO);

            // Generate a depth buffer as a render buffer
            glGenRenderbuffersOES(1, &m_Depth);
            glBindRenderbufferOES(GL_RENDERBUFFER_OES, m_Depth);
            glRenderbufferStorageOES(GL_RENDERBUFFER_OES, GL_DEPTH_COMPONENT16_OES, width, height);
            glBindRenderbufferOES(GL_RENDERBUFFER_OES, 0);

            // Bind both to the frame buffer
            glBindFramebufferOES(GL_FRAMEBUFFER_OES, m_FBO);
            glFramebufferTexture2DOES(GL_FRAMEBUFFER_OES, GL_COLOR_ATTACHMENT0_OES, GL_TEXTURE_2D, m_Tex, 0);
            glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES, GL_DEPTH_ATTACHMENT_OES, GL_RENDERBUFFER_OES, m_Depth);

            // Check the frame buffer for completeness
            IwAssert(TEST, glCheckFramebufferStatus(GL_FRAMEBUFFER_OES) == GL_FRAMEBUFFER_COMPLETE_OES);
            glBindFramebufferOES(GL_FRAMEBUFFER_OES, 0);
        }
        else
        {
            //upload solid red texture to show error
            uint32 rgba = 0xff0000ff;
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_UNSIGNED_BYTE, &rgba);

        }
    }
    ~CFBO()
    {
        // Clear up GL objects
        glDeleteTextures(1, &m_Tex);

        if (IwGLExtAvailable(IW_GL_OES_framebuffer_object) )
        {
            glDeleteFramebuffersOES(1, &m_FBO);
            glDeleteRenderbuffersOES(1, &m_Depth);
        }
    }
    void Bind()
    {
        if (IwGLExtAvailable(IW_GL_OES_framebuffer_object) )
        {
            glBindFramebuffer(GL_FRAMEBUFFER, m_FBO);

            // Must set viewport to framebuffer dimensions
            glViewport(0, 0, m_Width, m_Height);
        }
    }
    void Release()
    {
        if (IwGLExtAvailable(IW_GL_OES_framebuffer_object) )
            glBindFramebuffer(GL_FRAMEBUFFER, 0);

        glViewport(0, 0, IwGLGetInt(IW_GL_WIDTH), IwGLGetInt(IW_GL_HEIGHT));
    }
    GLuint GetTextureHandle()
    {
        return m_Tex;
    }

private:
    int m_Width;
    int m_Height;
    GLuint m_Tex;
    GLuint m_FBO;
    GLuint m_Depth;
};

// CTexture encapsulates an OpenGL ES texture object
class CTexture
{
public:
    CTexture(const char* file)
    {
        // Load image from file
        CIwImage img;
        img.LoadFromFile(file);

        // Convert to an OpenGL ES native format
        CIwImage nativeImg;
        nativeImg.SetFormat(CIwImage::ABGR_8888);
        img.ConvertToImage(&nativeImg);

        // Generate texture object
        glGenTextures(1, &m_Tex);
        glBindTexture(GL_TEXTURE_2D, m_Tex);

        // Upload
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, img.GetWidth(), img.GetHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, nativeImg.GetTexels());

        // Disable mipmapping
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    }

    ~CTexture()
    {
        glDeleteTextures(1, &m_Tex);
    }

    GLuint GetTextureHandle() { return m_Tex; }
private:
    GLuint m_Tex;
};

void InitGrid()
{
    // Initialise constant data in streams
    int y;
    for (y = 0; y < GRID_DIM; y++)
    {
        for (int x = 0; x < GRID_DIM; x++)
        {
            // Grid (x,z) are constant
            s_Verts[(y*GRID_DIM+x)*3] = x * 1.0f;
            s_Verts[(y*GRID_DIM+x)*3+2] = y * 1.0f;

            // UVs are constant
            s_UVs[(y*GRID_DIM+x)*2] = (float)x/(GRID_DIM-1);
            s_UVs[(y*GRID_DIM+x)*2+1] = (float)y/(GRID_DIM-1);
        }
    }

    // Indices are constant
    uint16* pInd = s_Inds;
    for (y = 0; y < GRID_DIM-1; y++)
    {
        for (int x = 0; x < GRID_DIM-1; x++)
        {
           uint16 ind0 = y*GRID_DIM+x;  // TL index
           uint16 ind1 = ind0+1;        // TR index (TL + 1)
           uint16 ind2 = ind0+GRID_DIM; // BL index (TL + pitch)
           uint16 ind3 = ind2+1;        // BR index (BL + 1)

           // Push two triangles
           *pInd++ = ind0;
           *pInd++ = ind1;
           *pInd++ = ind2;
           *pInd++ = ind1;
           *pInd++ = ind2;
           *pInd++ = ind3;
       }
    }
}

// Render an orbiting Logo
void RenderOrbit(float time)
{
    const float LOGO_SIZE = 0.5f;

    // Clear to white
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    // Set ortho projection
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrthof(-1.0f, 1.0f, -1.0f, 1.0f, -1.0f, 1.0f);

    // Rotate position according to time
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(sinf(time)*(1.0f-LOGO_SIZE), cosf(time)*(1.0f-LOGO_SIZE), 0.0f);

    // Interleaved verts
    float verts[] =
    {
        -LOGO_SIZE, -LOGO_SIZE, 1.0f, 0.0f,
        -LOGO_SIZE, LOGO_SIZE, 1.0f, 1.0f,
        LOGO_SIZE, LOGO_SIZE, 0.0f, 1.0f,
        LOGO_SIZE, -LOGO_SIZE, 0.0f, 0.0f,
    };
    uint16 inds[] = { 0, 1, 2, 0, 2, 3 };

    // Draw logo
    glVertexPointer(2, GL_FLOAT, 4*sizeof(float), verts);
    glTexCoordPointer(2, GL_FLOAT, 4*sizeof(float), verts+2);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, inds);
}

// Set perspective projection
void SetPerspective(float fovy, float aspect, float zNear, float zFar)
{
   float xmin, xmax, ymin, ymax;

   xmax = zNear * tanf(fovy * PI / 360.0f);
   xmin = -xmax;
   ymin = xmin * aspect;
   ymax = xmax * aspect;

   glFrustumf(xmin, xmax, ymin, ymax, zNear, zFar);
}

void RenderGrid(float time, int width, int height)
{
    // Clear to black
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    // Set perspective projection
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    SetPerspective(90.0f, (float)height/width, 0.05f, 1000.0f);
    glRotatef(180.0f, 0.0f, 0.0f, 1.0f);

    // Position grid in the frustum
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // Move in and rotate to face us
    glTranslatef(0.0f, -2.0f, -10.0f);
    glRotatef(-30.0f, 1.0f, 0.0f, 0.0f);

    // Rotate around central vertex
    float* centre = &s_Verts[((GRID_DIM/2)*GRID_DIM+GRID_DIM/2)*3];
    glTranslatef(-centre[0], 0.0f, -centre[2]);

    // Update vertices
    for (int y = 0; y < GRID_DIM; y++)
    {
        for (int x = 0; x < GRID_DIM; x++)
        {
            // Ripple diagonally based on sin wave
            s_Verts[(y*GRID_DIM+x)*3+1] = sinf(x + y + time) * 0.15f;

            // Light contribution (derivative of sinf is cosf)
            uint8 light = (uint8)(cosf(x + y + time) * 0x3f+0xc0);
            s_Cols[(y*GRID_DIM+x)] = 0xff000000 | light | (light << 8) | (light << 16);
        }
    }

    // Set up streams
    glVertexPointer(3, GL_FLOAT, 0, s_Verts);
    glTexCoordPointer(2, GL_FLOAT, 0, s_UVs);
    glEnableClientState(GL_COLOR_ARRAY);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, s_Cols);

    // Render
    glDrawElements(GL_TRIANGLES, (GRID_DIM-1)*(GRID_DIM-1)*6, GL_UNSIGNED_SHORT, s_Inds);

    // Disable colour stream
    glDisableClientState(GL_COLOR_ARRAY);
}

int main(int argc, char* argv[])
{
    // Attempt to start up OpenGL
    if (!IwGLInit())
    {
        IwError(("OpenGL could not be initialised"));
        return 0;
    }

    // Check for framebuffer availability
    if (!IwGLExtAvailable(IW_GL_OES_framebuffer_object))
    {
        IwError(("This example requires GL_OES_framebuffer_object, which "
            "is not present. The example will show red where the framebuffer "
            "object would have been."));
    }

    // Initialise constant data
    InitGrid();
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glEnable(GL_TEXTURE_2D);

    // Create FBO
    CFBO* fbo = new CFBO(512,512,GL_RGB,GL_UNSIGNED_BYTE);

    // Load Image
    CTexture* logo = new CTexture("Image.bmp");

    while (!s3eDeviceCheckQuitRequest())
    {
        // If framebuffers are available, update dynamic texture
        if (IwGLExtAvailable(IW_GL_OES_framebuffer_object) )
        {
            fbo->Bind();
            glBindTexture(GL_TEXTURE_2D, logo->GetTextureHandle());
            int64 timeMS = s3eTimerGetMs();
            RenderOrbit(timeMS / -500.0f);

            fbo->Release();
        }

        // Render the grid using the framebuffer
        glBindTexture(GL_TEXTURE_2D, fbo->GetTextureHandle());
        int64 timeMS = s3eTimerGetMs();
        RenderGrid(timeMS / 200.0f, IwGLGetInt(IW_GL_WIDTH), IwGLGetInt(IW_GL_HEIGHT));

        // Present the screen
        IwGLSwapBuffers();
        s3eDeviceYield();
    }

    delete fbo;
    delete logo;
    IwGLTerminate();
    return 0;
}
