/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwGLES2ProgramBinary IwGL OpenGL ES 2.x Program Binary Example
 *
 * This example demonstrates the use of IwGL with Program Binaries.
 *
 * This example is based on s3eGLES2, which is in turn based on an
 * example from the maemo project and was updated by the guys at webos-internals:
 * http://www.webos-internals.org/wiki/OpenGL_Triangle_of_Doom
 *
 * This version is modified to demonstrate use of program binaries
 * If a device does not support program binaries the example will error
 * and quit.
 *
 * If there is shader compiler support:
 * 1) It loads and compiles the shaders and links them into a program
 * 2) It then saves the program binary out to shader_bin/program.bin
 *
 *
 * On platforms with no shader compilation support it loads the program from
 * shader_bin/program.bin.
 * The program binary format must be compatible with the gl drivers it is
 * loaded on. For example to load program binaries on Windows Phone 8, they
 * must have been created with gles2-dx11 drivers on desktop.
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwGLES2Image.png
 *
 * @include IwGLES2ProgramBinary.cpp
 */

#include <stdlib.h>
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "IwGL.h"

#include "s3e.h"

#define MAX_CONFIG 32

// the work is done by IwGL
static int eglInit()
{
    if (!IwGLInit() )
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "eglInit failed");
        return 1;
    }

    return 0;
}

const char* vertexSrc = "attribute vec4 position; uniform highp mat4 mat; varying mediump vec2 pos; void main() { gl_Position = position * mat; pos = position.xy; }";
const char* fragmentSrc = "varying mediump vec2 pos; uniform mediump float phase; void main() { gl_FragColor = vec4(1, 1, 1, 1) * sin((pos.x * pos.x + pos.y * pos.y) * 40.0 + phase); }";

void printShaderInfoLog(GLuint shader)
{
    GLint length;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
    if (length)
    {
        char* buffer = (char*)malloc( sizeof(char) * length ) ;
        glGetShaderInfoLog(shader, length, NULL, buffer);
        printf("%s", buffer);
        free( buffer ) ;
        GLint success;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
        if (success != GL_TRUE) {
            exit(1);
        }
    }
}

// create shader from source
GLuint createShader(GLenum type, const char* pSource)
{
    GLuint shader = glCreateShader(type);
    glShaderSource(shader, 1, &pSource, NULL);
    glCompileShader(shader);
    printShaderInfoLog(shader);
    return shader;
}

int phaseLocation;
int matLocation;

void compileShaders()
{
    // Check for shader compiler support
    GLboolean shader;
    glGetBooleanv(GL_SHADER_COMPILER, &shader);
    if (!shader)
        return;

    //create the program from shader source
    GLuint shaderProgram = glCreateProgram();
    GLuint vertexShader = createShader(GL_VERTEX_SHADER, vertexSrc);
    GLuint fragmentShader = createShader(GL_FRAGMENT_SHADER, fragmentSrc);

    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);

    glLinkProgram(shaderProgram);

    // you can call glGetProgramBinaryOES directly (using eglGetProcAddress("glGetProgramBinaryOES") is also supported)
    /*PFNGLGETPROGRAMBINARYOESPROC glGetProgramBinaryOES = (PFNGLGETPROGRAMBINARYOESPROC)eglGetProcAddress("glGetProgramBinaryOES");
    if (glGetProgramBinaryOES == NULL) {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "Unable to get glGetProgramBinaryOES");
        return;
    }*/

    // get the program binary length
    int len;
    glGetProgramiv(shaderProgram, GL_PROGRAM_BINARY_LENGTH_OES, &len);

    // fill the first 4 bytes of the buffer with the program binary type and
    // the rest with the program binary
    char* buffer = new char[len + 4];
    glGetProgramBinaryOES(shaderProgram, len, &len, (GLenum*)buffer, buffer + 4);

    // save to file
    FILE* fp = fopen("shader_bin/program.bin", "w");
    if (fp == NULL)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "failed to save file shader_bin/program.bin");
        return;
    }
    fwrite(buffer, 1, len + 4, fp);
    fclose(fp);

    delete[] buffer;

    glDeleteProgram(shaderProgram);
}

int loadShader()
{
    GLuint shaderProgram = glCreateProgram();

    // you can call glProgramBinaryOES directly (using eglGetProcAddress("glProgramBinaryOES") is also supported)
    /*PFNGLPROGRAMBINARYOESPROC glProgramBinaryOES = (PFNGLPROGRAMBINARYOESPROC)eglGetProcAddress("glProgramBinaryOES");
    if (glProgramBinaryOES == NULL) {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "Unable to get glProgramBinaryOES");
        return 1;
    }*/

    // read the file, the first 4 bytes are the program binary type, the rest are the program binary
    FILE* fp = fopen("shader_bin/program.bin", "r");
    if (fp == NULL)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "failed to load file shader_bin/program.bin");
        return 1;
    }

    fseek(fp, 0, SEEK_END);
    int len = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    char* buffer = new char[len+1];
    len = fread(buffer, 1, len, fp);
    buffer[len] = 0;

    fclose(fp);

    //set the program binary
    glProgramBinaryOES(shaderProgram, ((GLenum*)buffer)[0], buffer + 4, len - 4);

    delete[] buffer;

    glUseProgram(shaderProgram);

    //get the uniforms
    phaseLocation = glGetUniformLocation(shaderProgram, "phase");
    if (phaseLocation < 0) {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "Unable to get uniform location (is the program binary the wrong format for this device?)");
        return 1;
    }

    matLocation = glGetUniformLocation(shaderProgram, "mat");
    if (matLocation < 0) {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "Unable to get uniform location (is the program binary the wrong format for this device?)");
        return 1;
    }

    return 0;
}

bool CheckExtension(const char* extension)
{
    const char* list = (const char*)glGetString(GL_EXTENSIONS);
    return strstr(list, extension) != NULL;
}

const float vertexArray[] = {
    0, -1, 0, 1,
    1, 1, 0, 1,
    -1, 1, 0, 1
};

void render()
{
    static float offset = 0;

    // Get dimensions from IwGL
    int w = IwGLGetInt(IW_GL_WIDTH);
    int h = IwGLGetInt(IW_GL_HEIGHT);

    glViewport(0, 0, w, h) ;
    glClearColor(0, 1, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUniform1f(phaseLocation, offset);

    // The OpenGL2 API doesn't not allow IwGL to perform rotation
    // automatically (the pipeline is too flexible) so we must
    // communicate with the shader to rotate.
    float angle = IwGLGetInt(IW_GL_ROTATE) * 3.141f / 2;
    float m[16] = { cosf(angle), sinf(angle), 0, 0,
                -sinf(angle), cosf(angle), 0, 0,
                0, 0, 1.0f, 0,
                0, 0, 0, 1.0f};

    glUniformMatrix4fv(matLocation, 1, GL_FALSE, m);
    glVertexAttribPointer(0, 4, GL_FLOAT, false, 0, vertexArray);
    glEnableVertexAttribArray(0);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 3);

    // Call IwGL swap instead of egl directly
    IwGLSwapBuffers();

    offset = fmodf(offset + 0.2f, 2*3.141f);
}

int main()
{
    if (eglInit())
       return 1;

    printf("Screen BPP: %d\n", s3eSurfaceGetInt(S3E_SURFACE_PIXEL_TYPE) & S3E_SURFACE_PIXEL_SIZE_MASK);
    printf("\n");
    printf( "Vendor     : %s\n", (char*)glGetString(GL_VENDOR));
    printf( "Renderer   : %s\n", (char*)glGetString(GL_RENDERER));
    printf( "Version    : %s\n", (char*)glGetString(GL_VERSION));
    printf( "Extensions : %s\n", (char*)glGetString(GL_EXTENSIONS));
    printf("\n");

    if (!CheckExtension("GL_OES_get_program_binary"))
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "need the extension GL_OES_get_program_binary to work");
        return 1;
    }

    compileShaders();

    if (loadShader())
        return 1;

    bool quit = false;

    int numFrames = 0;

    while (!quit) {
                s3eKeyboardUpdate();
                s3eDeviceYield(0);
                if (s3eDeviceCheckQuitRequest())
                    quit = 1;
                if (s3eKeyboardGetState(s3eKeyEsc) & S3E_KEY_STATE_PRESSED)
                    quit = 1;
                render();
                numFrames++;
    }
    return 0;
}
