/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwGLES3 IwGL OpenGL ES 3.x Example
 *
 * This example demonstrates the use of IwGL with an OpenGL ES 3.x codebase.
 *
 * This example is based on s3eGLES2, which is in turn based on an
 * example from the maemo project and was updated by the guys at webos-internals:
 * http://www.webos-internals.org/wiki/OpenGL_Triangle_of_Doom
 *
 * Shader binaries need to be generated before you can run this example on
 * Windows Phone 8. This example is set up to compile shaders and save them to file
 * (in the data-ram/shader_bin_wp8 directory) when run on Windows desktop.
 *
 *
 * Alternatively you could compile the shader binaries as part of the build process,
 * and call glShaderBinary directly, the s3eGLES3 example takes this approach.
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwGLES3Image.png
 *
 * @include IwGLES3.cpp
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "IwGL.h"

#include "s3e.h"

static int eglInit()
{
    if (!IwGLInit() )
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "eglInit failed");
        return 1;
    }

    return 0;
}

const char* vertexSrc = "#version 300 es\n"
    "in highp vec4 position;"
    "out mediump vec2 pos;"
    "void main()"
    "{"
    "    gl_Position = position;"
    "    pos = position.xy;"
    "}";
const char* fragmentSrc = "#version 300 es\n"
    "in mediump vec2 pos;"
    "layout(std140) uniform ValueBlock"
    "{"
    "    highp float shift;"
    "};"
    "uniform mediump float phase;"
    "layout (location = 0) out lowp vec4 oColour;"
    "void main (void)"
    "{"
    "    lowp float value = sin((pos.x * pos.x + pos.y * pos.y) * 40.0 + phase);"
    "    highp vec4 colour = vec4(shift, shift - 1.0f, shift - 2.0f, 1.0f);"
    "    oColour = colour * value;"
    "}";

void printShaderInfoLog(GLuint shader)
{
    GLint length;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
    if (length > 1)
    {
        char* buffer = (char*)s3eMalloc( sizeof(char) * length ) ;
        glGetShaderInfoLog(shader, length, NULL, buffer);
        printf("%s", buffer);
        s3eFree( buffer ) ;
        GLint success;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
        if (success != GL_TRUE) {
            exit(1);
        }
    }
}

GLuint createShader(GLenum type, const char* pSource)
{
    GLuint shader = glCreateShader(type);
    glShaderSource(shader, 1, &pSource, NULL);
    glCompileShader(shader);
    printShaderInfoLog(shader);
    return shader;
}

int phaseLocation;
GLuint valueBlockIndex = -1;
GLuint valueBlockHandle[] = {0, 0};
int currBlock = 0;
int timer = 40;

const float vertexArray[] = {
    0, -1, 0, 1,
    1, 1, 0, 1,
    -1, 1, 0, 1
};

void render()
{
    static float offset = 0;

    // Get dimensions from IwGL
    int w = IwGLGetInt(IW_GL_WIDTH);
    int h = IwGLGetInt(IW_GL_HEIGHT);

    glViewport(0, 0, w, h) ;
    glClearColor(0, 1, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUniform1f(phaseLocation, offset);
    if (timer <= 0)
    {
        timer = 100;
        currBlock++;
        if (currBlock >= 2)
            currBlock = 0;

        glBindBufferBase(GL_UNIFORM_BUFFER, valueBlockIndex, valueBlockHandle[currBlock]);
    }

    // The OpenGL2 API doesn't not allow IwGL to perform rotation
    // automatically (the pipeline is too flexible) so we must
    // communicate with the shader to rotate.
    //float angle = IwGLGetInt(IW_GL_ROTATE) * 3.141f / 2;
    /*float m[16] = { cosf(angle), sinf(angle), 0, 0,
                -sinf(angle), cosf(angle), 0, 0,
                0, 0, 1.0f, 0,
                0, 0, 0, 1.0f};*/

    glVertexAttribPointer(0, 4, GL_FLOAT, false, 0, vertexArray);
    glEnableVertexAttribArray(0);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 3);

    // Call IwGL swap instead of egl directly
    IwGLSwapBuffers();

    offset = fmodf(offset + 0.2f, 2*3.141f);
    timer--;
}

int main()
{
    if (eglInit())
       return 1;

    printf("Screen BPP: %d\n", s3eSurfaceGetInt(S3E_SURFACE_PIXEL_TYPE) & S3E_SURFACE_PIXEL_SIZE_MASK);
    printf("\n");
    printf( "Vendor     : %s\n", (char*)glGetString(GL_VENDOR));
    printf( "Renderer   : %s\n", (char*)glGetString(GL_RENDERER));
    printf( "Version    : %s [%d]\n", (char*)glGetString(GL_VERSION), IwGLGetInt(IW_GL_VERSION));
    printf( "Extensions : %s\n", (char*)glGetString(GL_EXTENSIONS));
    printf("\n");

    int version = IwGLGetInt(IW_GL_VERSION)>>8;
    if (version != 3)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "This example requires GLES v3.x");
        return 1;
    }

    GLuint shaderProgram = glCreateProgram();
    GLuint vertexShader = createShader(GL_VERTEX_SHADER, vertexSrc);
    GLuint fragmentShader = createShader(GL_FRAGMENT_SHADER, fragmentSrc);

    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);

    glLinkProgram(shaderProgram);

    glUseProgram(shaderProgram);

    phaseLocation = glGetUniformLocation(shaderProgram, "phase");

    if (phaseLocation < 0)
    {
        s3eDebugErrorPrintf("Unable to get uniform location");
        glDeleteProgram(shaderProgram);
        return 1;
    }

    GLint valueBlockSize = 0;
    valueBlockIndex = glGetUniformBlockIndex(shaderProgram, "ValueBlock");
    glGetActiveUniformBlockiv(shaderProgram, valueBlockIndex, GL_UNIFORM_BLOCK_DATA_SIZE, &valueBlockSize);

    GLubyte* valueBlock = (GLubyte*)malloc(valueBlockSize*2);
    memset(valueBlock, 0, valueBlockSize*2);

    const GLchar* names[] = {"shift"};
    GLuint valueBlockIndices[] = {(GLuint)-1};

    glGetUniformIndices(shaderProgram, 1, names, valueBlockIndices);

    if (valueBlockIndices[0] == 0xffffffff)
    {
        s3eDebugErrorPrintf("Unable to get uniform location");
        glDeleteProgram(shaderProgram);
        return 1;
    }

    glGenBuffers(2, valueBlockHandle);

    float* f1 = (float*)valueBlock;
    float* f2 = (float*)(valueBlock + valueBlockSize);

    f1[0] = 1.0f;
    f2[0] = 1.5f;

    glBindBuffer(GL_UNIFORM_BUFFER, valueBlockHandle[0]);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(valueBlockSize), valueBlock, GL_STATIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, valueBlockHandle[1]);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(valueBlockSize), valueBlock + sizeof(valueBlockSize), GL_STATIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

    delete valueBlock;

    glBindBufferBase(GL_UNIFORM_BUFFER, valueBlockIndex, valueBlockHandle[0]);

    bool quit = false;

    int numFrames = 0;

    while (!quit) {
                s3eKeyboardUpdate();
                s3eDeviceYield(0);
                if (s3eDeviceCheckQuitRequest())
                    quit = 1;
                if (s3eKeyboardGetState(s3eKeyEsc) & S3E_KEY_STATE_PRESSED)
                    quit = 1;
                render();
                numFrames++;
    }
    //Shutdown GL system
    IwGLTerminate();
    return 0;
}
