/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

//-----------------------------------------------------------------------------
// ExampleIwUIHelloWorldDynamic
//-----------------------------------------------------------------------------

/**
 * @page ExampleIwUIHelloWorldDynamic IwUI Hello World Dynamic Example
 * The following example demonstrates simple creation and displaying of a dialogue.
 *
 * The main classes used to achieve this are:
 *  - CIwUIView
 *  - CIwUIController
 *  - CIwUIElement
 *  - CIwUIButton
 *  - CIwUILabel
 *  - CIwUIImage
 *  - CIwUIStyleSheetManager
 *
 * The main functions used to achieve this are:
 *  - IwUIInit()
 *  - IwUITerminate();
 *
 * This example demonstrates the instantiation of elements from code.
 *
 * While elements are normally created from data, it can sometimes be useful to create them
 * directly in code. This example matches IwUIHelloWorld, except it manually creates the
 * elements instead of using a .ui file. CreateDialog() does the creation of the elements.
 *
 * Note that few properties apart from style need to be set. This example relies on the
 * IwUI "house style", which is loaded from data, to define most of the element properties.
 *
 * Once the element has been created it is added to the UI view singleton. The UI view
 * contains the root element and is responsible for maintaining the elements.
 *
 * The CIwUIController is a singleton that converts input into events. Events from the system
 * are constantly gathered and are dispatched when CIwUIController::Update is called.
 *
 * For the button to function CIwUIView::Update and CIwUIController::Update must be called.
 *
 * This example registers a single global handler (CHelloWorldHandler) to exit on button presses.
 *
 * Finally CIwUIView::Render is called to render the UI.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwUIHelloWorldDynamicImage.png
 *
 * @include IwUIHelloWorldDynamic.cpp
 *
 */
#include "IwGx.h"
#include "IwUI.h"


//Simple handler to quit application on any button press
class CHelloWorldHandler : public IIwUIEventHandler
{
    virtual bool HandleEvent(CIwEvent* pEvent)
    {
        return false;
    }

    virtual bool FilterEvent(CIwEvent* pEvent)
    {
        if (pEvent->GetID() == IWUI_EVENT_BUTTON )
        {
            static bool disableExit =
                s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WS8 ||
                s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WS81 ||
                s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WIN10;

            if (!disableExit)
            {
                s3eDeviceRequestQuit();
            }

            return true;
        }

        return false;
    }
} g_HelloWorldHandler;


//CreateDialog creates the same controls and layout as the .ui file in the regular HelloWorld application
CIwUIElement* CreateDialog()
{
    //Create a container for the dialog
    CIwUIElement* pContainer = new CIwUIElement;

    //Set the container to resize to fit it's children automatically.
    //The layout of the root view element will cause this to be centred on the screen.
    pContainer->SetSizeToContent(true);

    //Give the container a vertical layout. This will automatically arrange it's children vertically.
    pContainer->SetLayout(new CIwUILayoutVertical);


    //Create a label to say "HELLO WORLD"
    CIwUILabel* pLabel = new CIwUILabel;

    //Give the label the standard label style. This is the name of a property set
    //from the current style. This control will inherit the properties of that set.
    pLabel->SetStyle("<label_large>");

    //Set the caption of the label
    pLabel->SetCaption("HELLO WORLD");

    //Add the label to the container and the container's layout
    pContainer->AddChild(pLabel);
    pContainer->GetLayout()->AddElement(pLabel);


    //Create a button in the standard button style
    CIwUIButton* pButton = new CIwUIButton;
    pButton->SetStyle("<button>");

    //Set the maximum size of the button. This tells the button how large to physically
    //expand to if it has unlimited space.
    pButton->SetSizeMax(CIwVec2(120, 37));

    //Set the caption to be "Goodbye world"
    pButton->SetCaption("Goodbye World");

    //Add the button to the container and the container's layout
    pContainer->AddChild(pButton);
    pContainer->GetLayout()->AddElement(pButton);


    //Create an image
    CIwUIImage* pImage = new CIwUIImage;

    //Set the logo texture to govern its appearance.
    pImage->SetProperty("texture", (CIwTexture*)IwGetResManager()->GetResNamed("Image", "CIwTexture"));

    //Set the image to automatically resize to the size of the material's texture
    pImage->SetSizeToContent(true);

    //Add the image to the container and the container's layout
    pContainer->AddChild(pImage);
    pContainer->GetLayout()->AddElement(pImage);

    return pContainer;
}

void ExampleInit()
{
    //Initialise the IwUI module
    IwUIInit();

    //Instantiate the view and controller singletons.
    //IwUI will not instantiate these itself, since they can be subclassed to add functionality.
    new CIwUIController;
    new CIwUIView;

    //Provide global event handler
    IwGetUIController()->AddEventHandler(&g_HelloWorldHandler);

    //Load the hello world UI
    IwGetResManager()->LoadGroup("IwUIHelloWorld.group");

    //Set the default style sheet
    CIwResource* pResource = IwGetResManager()->GetResNamed("iwui", IW_UI_RESTYPE_STYLESHEET);
    IwGetUIStyleManager()->SetStylesheet(IwSafeCast<CIwUIStylesheet*>(pResource));

    //And instantiate it
    CIwUIElement* pDialog = CreateDialog();
    IwGetUIView()->AddElement(pDialog);
    IwGetUIView()->AddElementToLayout(pDialog);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    delete IwGetUIController();
    delete IwGetUIView();

    //Terminate the IwUI module
    IwUITerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    //Update the controller (this will generate control events etc.)
    IwGetUIController()->Update();

    //Update the view (this will do animations etc.) The SDK's example framework has a fixed
    //framerate of 20fps, so we pass that duration to the update function.
    IwGetUIView()->Update(1000/20);

    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    //Render the UI
    IwGetUIView()->Render();

    //Flush IwGx
    IwGxFlush();
    //Display the rendered frame
    IwGxSwapBuffers();
}
