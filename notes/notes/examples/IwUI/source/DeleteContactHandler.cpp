/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

// Standard includes
#include "IwUIElement.h"
#include "IwUIElementEventHandler.h"
#include "IwUIElementSignal.h"

// Project includes
#include "IwUIContacts.h"

//-----------------------------------------------------------------------------

class CDeleteContactHandler : public CIwUIElementEventHandler
{
public:
    IW_MANAGED_DECLARE(CDeleteContactHandler)
        CDeleteContactHandler() {}

private:
    // CIwUIElementEventHandler virtuals
    virtual void Activate(bool val)
    {
        CIwUIElementEventHandler::Activate(val);

        if (val)
        {
            IW_UI_CREATE_HANDLER_SLOT1(this, CDeleteContactHandler,
                OkDeleteContact, CIwUIElement*);
            IW_UI_CREATE_HANDLER_SLOT1(this, CDeleteContactHandler,
                CancelDeleteContact, CIwUIElement*);
        }
        else
        {
            IW_UI_DESTROY_HANDLER_SLOTS(this);
        }
    }

    virtual void Clone(CIwUIElementEventHandler* pTarget) const
    {
        IW_UI_CLONE_SUPERCLASS(pTarget, CDeleteContactHandler, CIwUIElementEventHandler)
    }

    // Slot functions
    void OkDeleteContact(CIwUIElement*)
    {
        GetApp()->DeleteConfirmed();
        GetApp()->HideDeleteDialog();
    }

    void CancelDeleteContact(CIwUIElement*)
    {
        GetApp()->HideDeleteDialog();
    }
};

IW_MANAGED_IMPLEMENT_FACTORY(CDeleteContactHandler)
