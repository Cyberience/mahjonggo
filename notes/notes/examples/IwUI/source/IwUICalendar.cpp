/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwUICalendar IwUI Calendar Example
 *
 * The following example demonstrates a simple calendar app implemented in IwUI.
 *
 * The main classes used to achieve this are:
 *  - CIwUIView
 *  - CIwUIController
 *  - CIwUIElement
 *  - CIwUIButton
 *  - CIwUILabel
 *
 * @image html IwUICalendarImage.png
 *
 * @include IwUICalendar.cpp
 */

// Library includes
#include "IwGx.h"
#include "IwGxFont.h"
#include "IwUI.h"
#include "IwUIAnimation.h"
#include "IwUIController.h"
#include "IwUIButton.h"
#include "IwUICheckbox.h"
#include "IwUIElement.h"
#include "IwUIEvent.h"
#include "IwUILabel.h"
#include "IwUISlider.h"
#include "IwUIView.h"
#include "IwUIProgressBar.h"
#include "IwUIPropertySet.h"
#include "s3eKeyboard.h"
#include "IwUISoftKeyboard.h"
#include "IwUITextInput.h"
#include "IwEventList.h"
#include "IwUIDayView.h"
#include "IwUIAddView.h"
#include "IwUIMonthView.h"
#include "IwUIListView.h"

extern bool g_IwTextureAssertPowerOf2;
bool g_Quit;

//Active day
time_t g_Day;
//Current day
time_t g_Today;

//Item Template for Events
CIwUIElement* g_ItemTemplate;
//Dynamic Top Element and Layout
CIwUIElement* pTop;
CIwUILayout*  pLayout;
//View mode Tab bar
CIwUITabBar * pTabBar;

//Action for Today Button Click
void ClickToday();
//Get the time of day
time_t GetNow();
//Switch view mode
void ViewMode(int16 mode);

class CController : public CIwUIController
{
public:
    CController()
    {

        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClickButtonToday, CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClickButtonAdd, CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClickButtonEventAdd, CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClickButtonEventCancel, CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClickButtonEventRemove, CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClickButtonEventClick, CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClickButtonDayClick, CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClickButtonPrev, CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClickButtonNext, CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT2(this, "CController", CController, OnRadioView, CIwUIElement* , int16)
    }

    void Init()
    {
        CIwResource* pResource = IwGetResManager()->GetResNamed("iwui", IW_UI_RESTYPE_STYLESHEET);
        m_pStyleIwUI = IwSafeCast<CIwUIStylesheet*>(pResource);
        IwGetUIStyleManager()->SetStylesheet(m_pStyleIwUI);
    }

private:

    CIwUIStylesheet* m_pStyleIwUI;

    void OnRadioView(CIwUIElement*, int16 mode)
    {
        //Switch View
        ViewMode(mode);
    }

    void OnClickButtonToday(CIwUIElement*)
    {
        //Go to today
        ClickToday();
    }

    void OnClickButtonNext(CIwUIElement*)
    {
        //Next date
        if (g_MonthView->m_Element->IsVisible()) g_MonthView->ButtonNext();
        else if (g_DayView->m_Element->IsVisible()) g_DayView->ButtonNext();
    }

    void OnClickButtonPrev(CIwUIElement*)
    {
        //Previous date
        if (g_MonthView->m_Element->IsVisible()) g_MonthView->ButtonPrev();
        else if (g_DayView->m_Element->IsVisible()) g_DayView->ButtonPrev();
    }

    void OnClickButtonEventClick(CIwUIElement* pElement)
    {
        //Find the correct Event for Click
        for (uint32 i=0;i<g_EventList->m_EventList.size();i++)
        {
            if (g_EventList->m_EventList[i]->m_Element == pElement)
            {
                //Edit Event
                g_AddView->EditView(*g_EventList->m_EventList[i], i);
                //Switch to Edit View
                ViewMode(3);

                break;
            }
        }
    }

    void OnClickButtonDayClick(CIwUIElement* pElement)
    {
        //Pass Click to MonthView object
        if (pElement)
            g_MonthView->DayClick(*pElement);
    }

    void OnClickButtonAdd(CIwUIElement*)
    {
        time_t day;
        g_Today = GetNow();

        //Select the appropriate date as default value
        if (g_MonthView->m_Element->IsVisible()) day = g_Day;
        else if (g_DayView->m_Element->IsVisible()) day = g_Day;
        else day = g_Today;

        g_AddView->NewView(day);

        ViewMode(3);
    }

    void OnClickButtonEventAdd(CIwUIElement*)
    {
        //Add the Event to Eventlist
        g_AddView->AddEvent();
        //Reset the Event we are currently editing
        g_AddView->m_EditIndex = -1;

        int16 mode = pTabBar->GetSelected();
        ViewMode(mode);
    }

    void OnClickButtonEventCancel(CIwUIElement*)
    {
        //Reset the Event we are currently editing
        g_AddView->m_EditIndex = -1;

        //Return to original view
        int16 mode = pTabBar->GetSelected();
        ViewMode(mode);
    }

    void OnClickButtonEventRemove(CIwUIElement*)
    {
        //Reset the Event we are currently editing
        g_AddView->EventRemove();

        //Return to original view
        int16 mode = pTabBar->GetSelected();
        ViewMode(mode);
    }
};


#ifdef IW_BUILD_RESOURCES
void _ITXReadCommentLine(CIwTextParser*);
void _ITXReadCommentBlock(CIwTextParser*);

void IgnoreToken(CIwTextParser* pImp)
{
    CIwTextParser::State* pParseState = pImp->_GetState();
    const char* pToken = pParseState->m_Token;

    if (strstr(pToken, "//"))
    {
        _ITXReadCommentLine(pImp);
    }
    else if (strstr(pToken, "/*"))
    {
        _ITXReadCommentBlock(pImp);
    }
}
#endif

void ViewMode(int16 mode)
{
    //Cycle through all views and ensure they are invisible and removed from the layout
    for (int i=pTop->GetNumChildren()-1;i>=0;i--)
    {
        CIwUIElement * child = pTop->GetChild(i);
        if (child->IsWithinLayout()) pLayout->RemoveElement(child);
        child->SetVisible(false);
    }

    if (mode == 0)
    {
        //Switch to the List View
        pLayout->AddElement(g_ListView->m_Element);
        g_ListView->m_Element->SetVisible(true);
        g_ListView->Refresh();
    }
    else if (mode == 1)
    {
        //Switch to the Day View
        pLayout->AddElement(g_DayView->m_Element);
        g_DayView->m_Element->SetVisible(true);
        g_DayView->Refresh();
    }
    else if (mode == 2)
    {
        //Switch to the Month View
        pLayout->AddElement(g_MonthView->m_Element);
        g_MonthView->m_Element->SetVisible(true);
        g_MonthView->Refresh();
    }
    else if (mode == 3)
    {
        //Switch to the Add Event View
        pLayout->AddElement(g_AddView->m_Element);
        g_AddView->m_Element->SetVisible(true);
    }
}

time_t GetNow()
{
    //Get local time
    time_t curtime;
    struct tm *loctime;
    curtime = time (NULL);
    loctime = localtime (&curtime);
    return mktime(loctime);
}

void ClickToday()
{
    //Get todays date and time
    g_Today = GetNow();

    if (g_ListView->m_Element->IsVisible())
    {
        //Focus on the next occuring event
        g_ListView->SetFocus(g_Today);
    }
    if (g_MonthView->m_Element->IsVisible())
    {
        //Switch to today
        g_MonthView->SetDay(g_Today);
        g_MonthView->Refresh();
    }
    if (g_DayView->m_Element->IsVisible())
    {
        //Switch to today
        g_Day = g_Today;
        g_DayView->Refresh();
    }
}

IW_CLASS_FACTORY(CIwEventDate);
IW_CLASS_FACTORY(CIwEventList);

int main()
{
    g_IwTextureAssertPowerOf2 = false;
    g_Quit = false;

    // Initialize IwUI
    IwUIInit();
    IW_CLASS_REGISTER(CIwDateAndTimeItemSource);
    IW_CLASS_REGISTER(CIwEventDate);
    IW_CLASS_REGISTER(CIwEventList);

    // Always display focus so selected data appears
    IwUISetDisplayFocus(true);

#ifdef IW_BUILD_RESOURCES
    // Ignore utf-8 signature
    char signature[0x4] = { 0xef, 0xbb, 0xbf, 0x0 };
    IwGetTextParserITX()->AddTokenFunction(signature, IgnoreToken);
    IwGetTextParserITX()->ParseFile("resource_templates.itx");
#endif

    {
        CIwUIView view;
        CController controller;

        //Load the resource group
        CIwResGroup* pResGroup = IwGetResManager()->LoadGroup("IwUICalendar.group");

        //Initialise the controller
        controller.Init();

        //Load base UI from UI file
        CIwUIElement* pWidgets = CIwUIElement::CreateFromResource("Calendar");
        view.AddElement(pWidgets);
        view.AddElementToLayout(pWidgets);

        //Locate elements
        pTop = (CIwUIElement*)pWidgets->GetChildNamed("Top");
        pLayout = (CIwUILayout*)pTop->GetLayout();
        pTabBar = (CIwUITabBar*)pWidgets->GetChildNamed("View");

        //Load the event item template
        g_ItemTemplate = (CIwUIElement*)IwGetResManager()->GetResNamed("ListItem", "CIwUIElement");

        //Initialise views
        g_ListView = new CIwUIListView();
        g_DayView = new CIwUIDayView();
        g_MonthView = new CIwUIMonthView();
        g_AddView = new CIwUIAddView();

        //Add views as children to the Top UI Element
        pTop->AddChild(g_ListView->m_Element);
        pTop->AddChild(g_DayView->m_Element);
        pTop->AddChild(g_MonthView->m_Element);
        pTop->AddChild(g_AddView->m_Element);

        //Get todays date
        g_Today = GetNow();
        g_Day = g_Today;

        //Read Event Date List by Serialisation
        g_EventList = new CIwEventList();

        if (IwSerialiseExists("dates.bin"))
        {
            IwSerialiseOpen("dates.bin", true);
            g_EventList->Serialise();
            IwSerialiseClose();
        }

        //pEventList->EventList.clear();

        //Create text input singleton
        CIwUITextInput textinput;
        textinput.CreateSoftKeyboard();

        //Select the ListView as the default view
        pTabBar->SetSelected(0);

        //Ensure layout is valid prior to setting focus element
        IwGetUIView()->Layout();

        //Select the next event
        ClickToday();

        int32 updateTime = 0;
        while (!g_Quit)
        {
            int64 start = s3eTimerGetMs();

            IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

            // Update and render UI
            IwGetUIController()->Update();
            view.Update(updateTime);
            view.Render();

            // Display frame
            IwGxFlush();
            IwGxSwapBuffers();

            // Usual device interaction
            s3eDeviceYield();
            s3eKeyboardUpdate();
            s3ePointerUpdate();

            // Attempt frame rate
            int32 frameTime = 1000 / 30; // 30fps
            const int32 tickTime = (int32)(s3eTimerGetMs() - start);

            if (tickTime < frameTime)
            {
                int32 yield = frameTime - tickTime;
                yield = MAX(0, MIN(yield, 1000));
                s3eDeviceYield(yield);
            }

            updateTime = (int32)(s3eTimerGetMs() - start);
            updateTime = MAX(frameTime, MIN(updateTime, 1000));

            // Check for quit request
            g_Quit |= (s3eDeviceCheckQuitRequest() != 0);
        }

        // Save Event Date List by Serialisation
        IwSerialiseOpen("dates.bin", false);
        g_EventList->Serialise();
        IwSerialiseClose();

        //Free the event list
        delete g_EventList;

        //Destroy the resource group
        IwGetResManager()->DestroyGroup(pResGroup);

        //Delete the view classes
        delete g_DayView;
        delete g_AddView;
        delete g_MonthView;
        delete g_ListView;

    }

    IW_CLASS_REMOVE(CIwDateAndTimeItemSource);
    IW_CLASS_REMOVE(CIwEventDate);
    IW_CLASS_REMOVE(CIwEventList);

    IwUITerminate();
    return 0;
}
