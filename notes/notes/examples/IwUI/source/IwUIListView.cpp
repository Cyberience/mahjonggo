/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

#include "IwUIListView.h"

CIwUIListView* g_ListView;

CIwUIListView::CIwUIListView()
{
    //Create UI from resource
    m_Element = CIwUIElement::CreateFromResource("ListView");
}

void CIwUIListView::Refresh()
{
    //Find the scrollable view
    CIwUIScrollableView * pScrollable = (CIwUIScrollableView*)m_Element->GetChildNamed("Scrollable");
    CIwUILayoutVertical * pScrollLayout = (CIwUILayoutVertical*)pScrollable->GetLayout();

    //Remove all existing children
    int32 i;
    for (i=pScrollable->GetNumChildren()-1;i>=0;i--)
    {
        CIwUIElement * child = pScrollable->GetChild(i);
        pScrollable->RemoveChild(child);
        delete child;
    }

    struct tm dayHeader;
    dayHeader.tm_yday = 0;
    dayHeader.tm_year = 0;

    if (!g_EventList->m_EventList.size())
    {
        CIwUILabel * pLabel = new CIwUILabel();
        pLabel->SetStyle("<label_large>");
        pLabel->SetCaption("(no events)");

        //Add to the layout
        pScrollLayout->AddElement(pLabel, 0, IW_UI_ALIGN_LEFT, IW_UI_ALIGN_BOTTOM);
        return;
    }

    for (i=0;i<(int32)g_EventList->m_EventList.size();i++)
    {
        CIwUIElement* pItem = g_ItemTemplate->Clone();

        struct tm theday;
        localtime_r(&g_EventList->m_EventList[i]->m_Time, &theday);

        if (dayHeader.tm_yday != theday.tm_yday || dayHeader.tm_year != theday.tm_year)
        {
            //Create a Header for each day that has Events
            dayHeader = theday;

            CIwUILabel * pLabel = new CIwUILabel();
            pLabel->SetStyle("<label_large>");
            char HeaderText[100];
            strftime (HeaderText, 100, "%A %x", &dayHeader);
            pLabel->SetCaption(HeaderText);

            //Add to the layout
            pScrollLayout->AddElement(pLabel, 0, IW_UI_ALIGN_LEFT, IW_UI_ALIGN_BOTTOM);
        }

        char LabelText[100];
        strftime(LabelText, 100, "%I:%M %p\t", &theday);
        strncat(LabelText, g_EventList->m_EventList[i]->m_EventText, 100);

        //Set name/value
        CIwUIButton * Button = (CIwUIButton*)pItem->GetChildNamed("Button");
        Button->SetCaption(LabelText);

        g_EventList->m_EventList[i]->m_Element = (CIwUIElement*)Button;

        //Generate a unique name for the new item
        static uint32 num = 0;
        char buf[20];
        sprintf(buf, "item_%u", num++);
        pItem->SetName(buf);

        //Add to the layout
        pScrollLayout->AddElement(pItem, IW_UI_ALIGN_LEFT);
    }

}

void CIwUIListView::SetFocus(time_t time)
{
    //Focus on the next occurring event
    for (uint32 i=0;i<g_EventList->m_EventList.size();i++)
    {
        if (!(g_EventList->m_EventList[i]->m_Time < time)) //The event has not yet occurred
        {
            //Set focus on the event element
            IwGetUIView()->RequestFocus(g_EventList->m_EventList[i]->m_Element);

            return;
        }
    }
}
