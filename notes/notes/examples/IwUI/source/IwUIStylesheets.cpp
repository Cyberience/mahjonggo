/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

//-----------------------------------------------------------------------------
// ExampleIwUIStylesheets
//-----------------------------------------------------------------------------

/**
 * @page ExampleIwUIStylesheets IwUI Stylesheets Example
 * The following example demonstrates switching stylesheets on an application.
 *
 * The main classes used to achieve this are:
 *  - CIwUIView
 *  - CIwUIController
 *  - CIwUIElement
 *  - CIwUIButton
 *  - CIwUILabel
 *  - CIwUITabBar
 *  - CIwUIProgressBar
 *  - CIwUISlider
 *  - CIwUIAlertDialog
 *  - CIwUIStyleManager
 *
 * The main functions used to achieve this are:
 *  - IwUIInit()
 *  - IwUITerminate();
 *  - CIwUIStyleManager::SetStylesheet
 *  - CIwUIStyleManager::QueueStylesheet
 *
 * This example demonstrates an application with multiple stylesheets that can be
 * switched during run-time by the user.
 *
 * The stylesheets are defined in itx files that override the default appearance
 * of the application with custom designs.
 *
 * The stylesheets are loaded from the resource manager and can be changed by clicking
 * a button on the tab bar.
 *
 * A confirmation alert dialog is displayed when a user tries to quit the application.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwUIStylesheetsImage.png
 *
 * @include IwUIStyleSheets.cpp
 */

// Library includes
#include "IwGx.h"
#include "IwGxFont.h"
#include "IwUI.h"
#include "IwUIAnimation.h"
#include "IwUIController.h"
#include "IwUIButton.h"
#include "IwUICheckbox.h"
#include "IwUIElement.h"
#include "IwUIEvent.h"
#include "IwUILabel.h"
#include "IwUISlider.h"
#include "IwUIView.h"
#include "IwUIProgressBar.h"
#include "IwUIPropertySet.h"
#include "s3eKeyboard.h"
#include "IwUISoftKeyboard.h"
#include "IwUITextInput.h"

extern bool g_IwTextureAssertPowerOf2;

bool g_Quit;
// Alert dialog
CIwUIElement* g_QuitScreen;

class CController : public CIwUIController
{
public:
    CController()
    {
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClickShowQuitScreen, CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClickHideQuitScreen, CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClickQuit, CIwUIElement*)

        IW_UI_CREATE_VIEW_SLOT2(this, "CController", CController, OnRadioStyle, CIwUIElement* , int16 )
    }

    void Init()
    {
        // Load the stylesheets
        {
            CIwResource* pResource = IwGetResManager()->GetResNamed("iwui", IW_UI_RESTYPE_STYLESHEET);
            m_pStyleIwUI = IwSafeCast<CIwUIStylesheet*>(pResource);
        }
        {
            CIwResource* pResource = IwGetResManager()->GetResNamed("alt", IW_UI_RESTYPE_STYLESHEET);
            m_pStyleAlt = IwSafeCast<CIwUIStylesheet*>(pResource);
        }
        {
            CIwResource* pResource = IwGetResManager()->GetResNamed("green", IW_UI_RESTYPE_STYLESHEET);
            m_pStyleGreen = IwSafeCast<CIwUIStylesheet*>(pResource);
        }
        {
            CIwResource* pResource = IwGetResManager()->GetResNamed("black", IW_UI_RESTYPE_STYLESHEET);
            m_pStyleBlack = IwSafeCast<CIwUIStylesheet*>(pResource);
        }
        IwGetUIStyleManager()->SetStylesheet(m_pStyleAlt);
    }

private:
    CIwUIStylesheet* m_pStyleIwUI;
    CIwUIStylesheet* m_pStyleAlt;
    CIwUIStylesheet* m_pStyleGreen;
    CIwUIStylesheet* m_pStyleBlack;

    void OnClickShowQuitScreen(CIwUIElement*)
    {
        // Display the quit screen
        g_QuitScreen->SetVisible(true);
        IwGetUIView()->SetModal(g_QuitScreen);
    }

    void OnRadioStyle(CIwUIElement*, int16 mode)
    {
        // Change the stylesheet
        if ((0<=mode) && (mode<4))
        {
            CIwUIStylesheet* sheets[] = {m_pStyleIwUI,m_pStyleAlt,m_pStyleGreen,m_pStyleBlack};
            IwGetUIStyleManager()->QueueStylesheet(sheets[mode]);
        }
    }

    void OnClickHideQuitScreen(CIwUIElement*)
    {
        // Hide the quit screen
        g_QuitScreen->SetVisible(false);
        IwGetUIView()->SetModal(NULL);
    }

    void OnClickQuit(CIwUIElement*)
    {
        // Quit the application
        g_Quit = true;
    }
};

int main()
{
    g_IwTextureAssertPowerOf2 = false;
    g_Quit = false;

    // Initialize IwUI
    IwUIInit();

    {
        CIwUIView view;
        CController controller;

        CIwResGroup* pResGroup = IwGetResManager()->LoadGroup("IwUIStylesheets.group");

        controller.Init();

        // Create the main dialog
        CIwUIElement* pWidgets = CIwUIElement::CreateFromResource("Stylesheets");
        view.AddElement(pWidgets);
        view.AddElementToLayout(pWidgets);

        // Create the alert dialog
        CIwUIElement* pQuitScreen = CIwUIElement::CreateFromResource("quit_screen");
        g_QuitScreen = pQuitScreen;
        pQuitScreen->SetVisible(false);
        view.AddElement(pQuitScreen);
        view.AddElementToLayout(pQuitScreen);

        // Ensure layout is valid prior to setting focus element
        view.Layout();

        // Get the progress bar
        CIwUIProgressBar*   pProgress = (CIwUIProgressBar*)pWidgets->GetChildNamed("Progress");

        // Set a value on the slider
        CIwUISlider*        pSlider   = (CIwUISlider*)     pWidgets->GetChildNamed("Slider");
        pSlider->SetValue(15);

        int32 updateTime = 0;
        while (!g_Quit)
        {
            int64 start = s3eTimerGetMs();

            IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

            // Test Progress Update
            {
                static uint32 progress = 0;
                progress = (progress+4)%(1<<12);
                pProgress->SetProgress(progress);
            }

            IwGetUIController()->Update();
            view.Update(updateTime);
            view.Render();

            IwGxFlush();
            IwGxSwapBuffers();
            s3eDeviceYield();
            s3eKeyboardUpdate();
            s3ePointerUpdate();

            // Attempt frame rate
            const int32 frameTime = 1000 / 30; // 30fps
            int32 tickTime = (int32)(s3eTimerGetMs() - start);

            if (tickTime < frameTime)
            {
                int32 yield = frameTime - tickTime;
                yield = MAX(0, MIN(yield, 1000));
                s3eDeviceYield(yield);
            }

            updateTime = (int32)(s3eTimerGetMs() - start);
            updateTime = MAX(frameTime, MIN(updateTime, 1000));

            // Check for quit request
            g_Quit |= (s3eDeviceCheckQuitRequest() != 0);
        }

        IwGetResManager()->DestroyGroup(pResGroup);
    }

    IwUITerminate();
    return 0;
}
