/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

//-----------------------------------------------------------------------------
// ExampleIwUILocalise
//-----------------------------------------------------------------------------

/**
 * @page ExampleIwUILocalise IwUI Localise Example
 * The following example demonstrates localisation of a simple dialogue.
 *
 * The main classes used to achieve this are:
 *  - CIwUIView
 *  - CIwUIController
 *  - CIwUIElement
 *  - CIwUIStyleSheetManager
 *
 * The main functions used to achieve this are:
 *  - IwUIInit()
 *  - IwUISetLocaliseCallback()
 *  - IwUITerminate();
 *
 * This example demonstrates the loading localised strings and the useage of a
 * callback to provide this data to IwUI.
 *
 * @image html IwUILocaliseImage.png
 *
 * @include IwUILocalise.cpp
 */
#include "IwGx.h"
#include "IwUI.h"

// Enumeration of languages we localise for
enum LocaliseLanguages
{
    LOCALISE_ENGLISH,
    LOCALISE_FRENCH,

    NUM_LANGUAGES,

    DEFAULT_LANGUAGE = LOCALISE_ENGLISH
};

// Current language
LocaliseLanguages g_Localise = DEFAULT_LANGUAGE;

// String names of languages
const char* g_LanguageNames[] =
{
    "english",
    "french"
};

// Resource class to contain data for each localised string
#define RESTYPE_LOCALISED_STRING    "CLocalisedString"
class  CLocalisedString : public CIwResource
{
public:
    IW_MANAGED_DECLARE(CLocalisedString);

    CLocalisedString()
    {
    }

    ~CLocalisedString()
    {
    }

    // IwResource virtuals
    virtual bool ParseAttribute(CIwTextParserITX* pParser, const char* pAttrName)
    {
        for (int i=0; i<NUM_LANGUAGES; i++)
        {
            if (!stricmp(g_LanguageNames[i], pAttrName))
            {
                pParser->ReadString(m_Strings[i]);
                return true;
            }
        }

        return CIwResource::ParseAttribute(pParser, pAttrName);
    }

    virtual void ParseClose(CIwTextParserITX* pParser)
    {
        CIwManaged* pParent = pParser->GetObject(-1);
        if (CIwResGroup* pResGroup = dynamic_cast<CIwResGroup*>(pParent))
        {
            // Add ourselves to the current res group.
            pResGroup->AddRes(RESTYPE_LOCALISED_STRING, this);
        }
        else
        {
            CIwResource::ParseClose(pParser);
        }
    }

    virtual void Serialise()
    {
        CIwResource::Serialise();

        for (int i=0; i<NUM_LANGUAGES; i++)
        {
            m_Strings[i].Serialise();
        }
    }

    // Public interface
    const char* GetString(LocaliseLanguages language)
    {
        if ((0 <= language) && (language < NUM_LANGUAGES))
        {
            return m_Strings[language].c_str();
        }

        return NULL;
    }

private:
    // Member data
    CIwStringL m_Strings[NUM_LANGUAGES];
};
IW_MANAGED_IMPLEMENT_FACTORY(CLocalisedString);

// Custom controller containing slot functions to be attached to elements
class CController : public CIwUIController
{
public:
    CController()
    {
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClickQuit,
            CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT2(this, "CController", CController, OnRadioLanguage,
            CIwUIElement* , int16 )
    }

private:
    void OnRadioLanguage(CIwUIElement*, int16 mode)
    {
        if (mode >= 0 && mode < NUM_LANGUAGES)
        {
            g_Localise = (LocaliseLanguages) mode;
        }
    }

    void OnClickQuit(CIwUIElement*)
    {
        static bool disableExit =
            s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WS8 ||
            s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WS81 ||
            s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WIN10;

        if (!disableExit)
        {
            s3eDeviceRequestQuit();
        }
    }
};
//-----------------------------------------------------------------------------
const char* LocaliseCallback(const CIwPropertySet& propertySet)
{
    uint32 hash = 0;
    if (propertySet.GetProperty("localiseCaption", hash, true))
    {
        CLocalisedString* pLocString = (CLocalisedString*)
            IwGetResManager()->GetResHashed(hash,
                RESTYPE_LOCALISED_STRING, IW_RES_SEARCH_ALL_F);

        if (pLocString)
        {
            return pLocString->GetString(g_Localise);
        }
    }
    return NULL;
}
//-----------------------------------------------------------------------------
void ExampleInit()
{
    //Initialise the IwUI module
    IwUIInit();

    IW_CLASS_REGISTER(CLocalisedString);

    //Set localilisation callback
    IwUISetLocaliseCallback(LocaliseCallback);

    //Instantiate the view and controller singletons.
    //IwUI will not instantiate these itself, since they can be subclassed to add functionality.
    new CIwUIView;
    new CController;

    //Load the hello world UI
    IwGetResManager()->LoadGroup("IwUILocalise.group");

    //Set the default style sheet
    CIwResource* pResource = IwGetResManager()->GetResNamed("iwui", IW_UI_RESTYPE_STYLESHEET);
    IwGetUIStyleManager()->SetStylesheet(IwSafeCast<CIwUIStylesheet*>(pResource));

    //Find the dialog template
    CIwUIElement* pDialogTemplate = (CIwUIElement*)IwGetResManager()->GetResNamed("Vertical", "CIwUIElement");

    //And instantiate it
    CIwUIElement* pDialog = pDialogTemplate->Clone();
    IwGetUIView()->AddElement(pDialog);
    IwGetUIView()->AddElementToLayout(pDialog);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    delete IwGetUIController();
    delete IwGetUIView();

    IW_CLASS_REMOVE(CLocalisedString);

    //Terminate the IwUI module
    IwUITerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    static LocaliseLanguages g_OldLocalise = DEFAULT_LANGUAGE;
    if (g_OldLocalise != g_Localise)
    {
        g_OldLocalise = g_Localise;
        IwGetUIView()->Reactivate();
    }

    //Update the controller (this will generate control events etc.)
    IwGetUIController()->Update();

    //Update the view (this will do animations etc.) The SDK's example framework has a fixed
    //framerate of 20fps, so we pass that duration to the update function.
    IwGetUIView()->Update(1000/20);

    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    //Render the UI
    IwGetUIView()->Render();

    //Flush IwGx
    IwGxFlush();
    //Display the rendered frame
    IwGxSwapBuffers();
}
