/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

#include "IwUI.h"
#include "IwUIElement.h"
#include "IwEventList.h"

class CIwUIMonthView
{
public:
    CIwUIMonthView();
    CIwUIElement * m_Element;

    void DayClick(CIwUIElement & Element);
    void SetDay(time_t time);
    void Refresh();
    void ButtonNext();
    void ButtonPrev();
};

extern CIwUIMonthView* g_MonthView;
