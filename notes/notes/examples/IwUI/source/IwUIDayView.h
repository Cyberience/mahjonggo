/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

#include "IwUI.h"
#include "IwUIElement.h"
#include "IwEventList.h"

class CIwUIDayView
{
public:
    CIwUIDayView();

    //The UI element
    CIwUIElement * m_Element;

    void Refresh();
    void ButtonNext();
    void ButtonPrev();
};

extern CIwUIDayView* g_DayView;
