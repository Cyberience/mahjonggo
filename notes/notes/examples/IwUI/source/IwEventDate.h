/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

#include "IwManaged.h"
#include "IwSerialise.h"
#include "IwUIElement.h"
#include <string>
#include "time.h"



class CIwEventDate : public CIwManaged
{
public:
    IW_MANAGED_DECLARE(CIwEventDate);

    //Serialised data
    time_t m_Time;
    char * m_EventText;

    //Discardable data
    CIwUIElement * m_Element;

    CIwEventDate(void);
    ~CIwEventDate(void);
    void Serialise();
};
