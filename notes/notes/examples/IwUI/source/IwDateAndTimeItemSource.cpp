/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

// Standard includes
#include "IwDateAndTimeItemSource.h"

// Library includes
#include "IwUILabel.h"

//-----------------------------------------------------------------------------

IW_MANAGED_IMPLEMENT_FACTORY(CIwDateAndTimeItemSource);

CIwDateAndTimeItemSource::~CIwDateAndTimeItemSource()
{
    for (int i=0; i<(int)m_CachedDate.size(); i++)
    {
        delete m_CachedDate[i];
    }
    for (int j=0; j<(int)m_CachedLabels.size(); j++)
    {
        delete m_CachedLabels[j];
    }
}

time_t CIwDateAndTimeItemSource::GetTimeFromDays(int32 days)
{
    tm timeinfo;
    memset(&timeinfo,0,sizeof(tm));
    timeinfo.tm_year = GetCurrentYear() - 1900;
    timeinfo.tm_mday = days + 1;

    return mktime(&timeinfo);
}

int32 CIwDateAndTimeItemSource::GetDaysFromTime(time_t time)
{
    tm timeinfo;
    memset(&timeinfo,0,sizeof(tm));
    timeinfo.tm_year = GetCurrentYear() - 1900;
    timeinfo.tm_mday = 1;
    time_t startTime = mktime(&timeinfo);

    return (int32)((time - startTime) / (24 * 60 * 60));
}

int32 CIwDateAndTimeItemSource::GetCurrentYear()
{
    time_t curtime = time(NULL);
    tm* loctime = localtime(&curtime);
    return (1900 + loctime->tm_year);
}

int32 CIwDateAndTimeItemSource::GetNumColumns() const
{
    return 3;
}

int32 CIwDateAndTimeItemSource::GetNumRowsForColumn(int32 column) const
{
    const int rowsForColumn[]= { 365*100, 24,60/5 };

    return rowsForColumn[column];
}

CIwUIElement* CIwDateAndTimeItemSource::CreateItem(int32 column, int32 row)
{
    const char* labelStyle = "<label_spinner>";

    if (column == 0)
    {
        CIwUIElement* pDate;
        if (m_CachedDate.size())
        {
            pDate = m_CachedDate.pop_back_get();
        }
        else
        {
            pDate = new CIwUIElement;
            pDate->SetStyle("<date-item>");

            // Naming elements allows layout to find them
            const char* pLabelNames[] = { "day", "date", "month" };
            for (int i=0; i<3; i++)
            {
                CIwUILabel* pLabel = new CIwUILabel;
                pLabel->SetName(pLabelNames[i]);
                pLabel->SetStyle(labelStyle);
                pDate->AddChild(pLabel);
            }
        }

        char buffer[0x100];
        const char* weekdays[] =
        {
            "Mon","Tue","Wed","Thu","Fri","Sat","Sun"
        };
        const char* months[] =
        {
            "Jan","Feb","Mar","Apr","May","Jun",
            "Jul","Aug","Sep","Oct","Nov","Dec"
        };

        tm timeinfo;
        memset(&timeinfo,0,sizeof(tm));
        timeinfo.tm_year = GetCurrentYear() - 1900;
        timeinfo.tm_mday = row+1;
        mktime(&timeinfo);

        sprintf(buffer,"%d",timeinfo.tm_mday);
        const char* pCaptions[3] =
            { weekdays[timeinfo.tm_wday], buffer, months[timeinfo.tm_mon] };

        for (int i=0; i<3; i++)
        {
            IwSafeCast<CIwUILabel*>(pDate->GetChild(i))->SetCaption(pCaptions[i]);
        }

        return pDate;
    }
    else
    {
        CIwUILabel* pLabel;
        if (m_CachedLabels.size())
        {
            pLabel = (CIwUILabel*) m_CachedLabels.pop_back_get();
        }
        else
        {
            pLabel = new CIwUILabel;
        }

        char buffer[0x100];
        if (column == 1)
        {
            sprintf(buffer,"%02d",row);
        }
        else
        {
            int minute = row*5;
            sprintf(buffer,"%02d",minute);
        }

        pLabel->SetStyle(labelStyle);
        pLabel->SetCaption(buffer);

        return pLabel;
    }
}

void CIwDateAndTimeItemSource::ReleaseItem(CIwUIElement* pItem, int32 column, int32 row)
{
    if (column == 0)
    {
        m_CachedDate.push_back(pItem);
    }
    else
    {
        m_CachedLabels.push_back(pItem);
    }
}

int32 CIwDateAndTimeItemSource::GetColumnWidthHint(int32 column) const
{
    if (column == 0)
    {
        return 9;
    }
    else
    {
        return 2;
    }
}

int32 CIwDateAndTimeItemSource::GetRowHeightForColumn(int32 column, int32 columnWidth) const
{
    return 32;
}

void CIwDateAndTimeItemSource::Clone(CIwUIPickerViewItemSource* pTarget) const
{
    IW_UI_CLONE_SUPERCLASS(pTarget, CIwDateAndTimeItemSource, CIwUIPickerViewItemSource);
}
