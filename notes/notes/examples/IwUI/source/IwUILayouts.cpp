/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

//-----------------------------------------------------------------------------
// ExampleIwUILayouts
//-----------------------------------------------------------------------------

/**
 * @page ExampleIwUILayouts IwUI Layouts Example
 * The following example demonstrates the use of custom properties to provide additional
 * functionality. The data demonstrates the use of layouts for screen-size independence.
 *
 * The main classes used to achieve this are:
 *  - CIwManaged
 *  - CIwUIController
 *
 * The main functions used to achieve this are:
 *  - CIwUIController::AddEventHandler
 *
 * This example installs a global event filter (g_LayoutsHandler) to allow CIwUIButtons
 * with a custom 'uilink' property to open other dialogs.
 *
 * Custom properties can be added entirely in data. A property definition is required.
 * This is in IwUILayouts.group:
 * @code
 *  CIwPropertyDefine
 *  {
 *      CIwPropertyString uilink
 *  }
 * @endcode
 *
 * The expmetatemplate file can be modified to expose custom properties through the editor.
 *
 * A global concept of the current screen (g_Screen) is introduced and the handler
 * replaces the current screen with the specified target screen, found using a simple resource
 * query.
 *
 * Various screens are layed out with the UI editor tool, and are navigatible. Any button marked
 * with 'exit' quits the application.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwUILayoutsImage.png
 *
 * @include IwUILayouts.cpp
 */
#include "IwGx.h"
#include "IwUI.h"
#include "IwString.h"

CIwUIElement* g_Screen;
CIwString<16> g_String;


void SetScreen(const char* screen)
{
    //Find the dialog template
    CIwUIElement* pDialogTemplate = (CIwUIElement*)IwGetResManager()->GetResNamed(screen, "CIwUIElement");

    if (!pDialogTemplate)
        return;

    //Remove the old screen
    if (g_Screen)
        IwGetUIView()->DestroyElements();

    //And instantiate it
    g_Screen = pDialogTemplate->Clone();
    IwGetUIView()->AddElement(g_Screen);
    IwGetUIView()->AddElementToLayout(g_Screen);
}

//Simple handler to quit application on any button press
class CLayoutsHandler : public IIwUIEventHandler
{
    virtual bool FilterEvent(CIwEvent* pEvent)
    {
        if (pEvent->GetID() == IWUI_EVENT_BUTTON )
        {
            CIwPropertyString uilink;
            if ((IwSafeCast<CIwUIElement*>(pEvent->GetSender()))->GetProperty("uilink", uilink, true) )
            {
                if (!strcmp(uilink.c_str(), "exit") )
                {
                    static bool disableExit =
                        s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WS8 ||
                        s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WS81 ||
                        s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WIN10;

                    if (!disableExit)
                    {
                        s3eDeviceRequestQuit();
                    }

                    return true;
                }

                g_String = uilink.c_str();

                return true;
            }
        }

        return false;
    }

    virtual bool HandleEvent(CIwEvent* pEvent)
    {
        return false;
    }
} g_LayoutsHandler;


void ExampleInit()
{
    //Initialise the IwUI module
    IwUIInit();

    //Instantiate the view and controller singletons.
    //IwUI will not instantiate these itself, since they can be subclassed to add functionality.
    new CIwUIController;
    new CIwUIView;

    //Provide global event handler
    IwGetUIController()->AddEventHandler(&g_LayoutsHandler);

    //Load the hello world UI
    IwGetResManager()->LoadGroup("IwUILayouts.group");

    //Set the default style sheet
    CIwResource* pResource = IwGetResManager()->GetResNamed("iwui", IW_UI_RESTYPE_STYLESHEET);
    IwGetUIStyleManager()->SetStylesheet(IwSafeCast<CIwUIStylesheet*>(pResource));

    SetScreen("main");
}


//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    delete IwGetUIController();
    delete IwGetUIView();

    //Terminate the IwUI module
    IwUITerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    //Update the UI Screen
    if (g_String.length())
    {
        SetScreen(g_String.c_str());
        g_String = "";
    }

    //Update the controller (this will generate control events etc.)
    IwGetUIController()->Update();

    //Update the view (this will do animations etc.) The SDK's example framework has a fixed
    //framerate of 20fps, so we pass that duration to the update function.
    IwGetUIView()->Update(1000/20);

    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    //Render the UI
    IwGetUIView()->Render();

    //Flush IwGx
    IwGxFlush();
    //Display the rendered frame
    IwGxSwapBuffers();
}
