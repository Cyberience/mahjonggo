/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

//-----------------------------------------------------------------------------
// ExampleIwUITwitterSearch
//-----------------------------------------------------------------------------

/**
 * @page ExampleIwUITwitterSearch IwUI Twitter Search
 *
 * The following example demonstrates social networking interaction using
 * HTTP messages and an IwUI GUI.
 *
 * It fetches Twitter Search API results using the CIwHTTP class and displays
 * them dynamically in a UI.
 *
 * The main classes used to achieve this are:
 * - CIwUIView
 * - CIwUIController
 * - CIwUIElement
 * - CIwUIButton
 * - CIwUILabel
 * - CIwUIImage
 * - CIwHTTP
 *
 * The main functions used to achieve this are:
 * - IwUIInit()
 * - IwUITerminate()
 *
 * @image html IwUITwitterSearchImage.png
 *
 * Twitter Interaction (IwTwitterSearch.cpp):
 * @include IwTwitterSearch.cpp
 *
 * GUI and HTTP (IwTwitterSearchGUI.cpp):
 * @include IwTwitterSearchGUI.cpp
 *
 * Main App Framework (IwUITwitterSearch.cpp):
 * @include IwUITwitterSearch.cpp
 *
 */

// Library includes
#include "IwUI.h"
#include "IwHTTPQueue.h"
#include "IwJPEG.h"
#include "IwTwitterSearch.h"
#include "s3eLocation.h"
#include "Iw2D.h"
#include "IwTwitterSearchGUI.h"
#include "s3eConfig.h"
#include "s3eVideo.h"

// Attempt to lock to 25 frames per second
#define MS_PER_FRAME (1000 / 25)

extern bool g_IwTextureAssertPowerOf2;

//-----------------------------------------------------------------------------
void ShowSplashScreen(const char* file)
{
    IwGxSetColClear(0xff, 0xff, 0xff, 0xff);

    s3eFile* pFile = s3eFileOpen(file, "rb");
    if (!pFile)
    {
        IwAssertMsg(UI, false, ("Failed to open splash screen %s", file));
        return;
    }

    int bufferSize = s3eFileGetSize(pFile);
    char* buffer = new char[bufferSize];

    int bytesRead = s3eFileRead(buffer, 1, bufferSize, pFile);
    s3eFileClose(pFile);

    if (bytesRead != bufferSize)
    {
        return;
    }

    CIwTexture* pTexture = new CIwTexture;
    JPEGTexture(buffer, bufferSize, *pTexture);
    delete[] buffer;

    pTexture->Upload();

    int64 splashStart = s3eTimerGetMs();
    while ((s3eTimerGetMs() - splashStart) < 1000)
    {
        IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

        CIwSVec2 wh(pTexture->GetWidth(), pTexture->GetHeight());
        CIwSVec2 xy((IwGxGetDisplayWidth() - wh.x) / 2, (IwGxGetDisplayHeight() - wh.y) / 2);

        CIwMaterial* pMaterial = IW_GX_ALLOC_MATERIAL();
        pMaterial->SetTexture(pTexture);
        pMaterial->SetColAmbient(0xffffffff);

        IwGxSetMaterial(pMaterial);
        IwGxDrawRectScreenSpace(&xy, &wh);

        IwGxFlush();
        IwGxSwapBuffers();
    }

    delete pTexture;
}

//-----------------------------------------------------------------------------
void ExampleInit()
{
    g_IwTextureAssertPowerOf2 = false;

    // Start tracking location
    if (s3eLocationAvailable())
        s3eLocationStart();

    // Initialize IwUI
    IwUIInit();
    Iw2DInit();

    // Show apps splash screen
    ShowSplashScreen("IwUITwitterSearch/TwitAbout.jpg");

    g_IwTwitterSearchGUI = new CIwTwitterSearchGUI;
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    if (s3eLocationAvailable())
        s3eLocationStop();

    delete g_IwTwitterSearchGUI;

    Iw2DTerminate();

    //Terminate the IwUI module
    IwUITerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    g_IwTwitterSearchGUI->m_SearchReady = true;

    //Update the view (this will do animations etc.) The SDK's example framework has a fixed
    //framerate of 20fps, so we pass that duration to the update function.
    int updateTime = (1000/20);

    g_IwTwitterSearchGUI->Update(updateTime);

    // Update HTTP
    IwGetHTTPQueue()->Update();

    //Update the controller (this will generate control events etc.)
    IwGetUIController()->Update();

    //Update the view (this will do animations etc.)
    IwGetUIView()->Update(updateTime);

    return true;
}

//-----------------------------------------------------------------------------
void ExampleRender()
{
    g_IwTwitterSearchGUI->Draw2D();

    //Render the UI
    IwGetUIView()->Render();

    //Flush IwGx
    IwGxFlush();

    //Display the rendered frame
    IwGxSwapBuffers();
}

//-----------------------------------------------------------------------------
// Main global function
//-----------------------------------------------------------------------------
int main()
{
    // Example main loop
    ExampleInit();

    while (1)
    {
        s3eDeviceYield(0);
        s3eKeyboardUpdate();
        s3ePointerUpdate();

        int64 start = s3eTimerGetMs();

        bool result = ExampleUpdate();
        if  (
            (result == false) ||
            (s3eKeyboardGetState(s3eKeyEsc) & S3E_KEY_STATE_DOWN) ||
            (s3eKeyboardGetState(s3eKeyAbsBSK) & S3E_KEY_STATE_DOWN) ||
            (s3eDeviceCheckQuitRequest())
            )
            break;

        // Clear the screen
        IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);
        ExampleRender();

        // Attempt frame rate
        while ((s3eTimerGetMs() - start) < MS_PER_FRAME)
        {
            int32 yield = (int32) (MS_PER_FRAME - (s3eTimerGetMs() - start));
            if (yield<0)
                break;
            s3eDeviceYield(yield);
        }
    }

    ExampleShutDown();

    return 0;
}
