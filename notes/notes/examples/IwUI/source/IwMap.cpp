/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

#include "IwMap.h"

#include "IwUIImage.h"
#include "IwUIView.h"
#include "s3eLocation.h"

#include <math.h>
#include <algorithm>

#ifdef _MSC_VER
// 'this' : used in base member initializer list
#pragma warning(disable:4355)
#endif

// Defines
#define NUM_TILES_X ((int)(sizeof(m_Tiles)/sizeof(m_Tiles[0])))
#define NUM_TILES_Y ((int)(sizeof(m_Tiles[0])/sizeof(m_Tiles[0][0])))

// Macros
IW_MANAGED_IMPLEMENT_FACTORY(CIwMap);

class CMapProjection
{
public:
    CMapProjection(int zoom)
    {
        const double mapScaling = 16777216. / PI;
        const double zoomScaling = pow(2,(double)zoom);

        m_Radius = mapScaling / zoomScaling;
    }

    int64 longToX(double longitudeDegrees)
    {
        double longitude = degreesToRadians(longitudeDegrees);
        return (int64)(m_Radius * longitude);
    }

    int64 latToY(double latitudeDegrees)
    {
        double latitude = degreesToRadians(latitudeDegrees);
        double sinLatitude = sin(latitude);

        double Y = log((1. + sinLatitude) / (1. - sinLatitude));
        return (int64)((m_Radius * Y) / 2.);
    }

    double xToLong(int64 x)
    {
        double longRadians = (double)x / m_Radius;
        double longDegrees = radiansToDegress(longRadians);

        double rotations = floor((longDegrees + 180.)/360.);
        return longDegrees - (rotations * 360.);
    }

    double yToLat(int64 y)
    {
        double latRadians = (double)PI/2 - 2. * atan(exp((-1. * (double)y) / m_Radius));

        return radiansToDegress(latRadians);
    }

private:
    static double degreesToRadians(double degrees)
    {
        return (degrees * PI) / 180.;
    }

    static double radiansToDegress(double radians)
    {
        return (radians * 180.) / PI;
    }

    double m_Radius;
};

CIwMap::CIwMap() :
    m_Geonames(this),
    m_RequestedGeonames(false),
    m_ClearOnUpdate(false),
    m_Zoom(0),
    m_LoadingTexture(NULL),
    m_Marker(NULL),
    m_StartLatitude(0.),
    m_StartLongitude(0.),
    m_CentralTilePos(CIwVec2::g_Zero)
{
    SetProperty("scrollToFocus", false);

    for (int i=0; i<NUM_TILES_X; ++i)
        for (int j=0; j<NUM_TILES_Y; ++j)
            m_Tiles[i][j] = NULL;
}

void CIwMap::SetZoom(int Zoom)
{
    if (Zoom != m_Zoom)
    {
        // Hide location marker
        m_Marker->SetVisible(false);

        if (m_Zoom == Zoom + 1)
        {
            // Play an zoom in animation
            IwGetUIAnimManager()->StopAnim(this);
            uint32 handle = IwGetUIAnimManager()->PlayAnim("ZoomIn", this, true);

            IwGetUIAnimManager()->SetObserver(handle, this);
        }
        else if (m_Zoom == Zoom - 1)
        {
            // Play zoom out animation
            IwGetUIAnimManager()->StopAnim(this);
            uint32 handle = IwGetUIAnimManager()->PlayAnim("ZoomOut", this, true);

            IwGetUIAnimManager()->SetObserver(handle, this);
        }
        else
        {
            // No animation, just clear all.
            // Next update we'll start downloading tiles
            m_ClearOnUpdate = true;

            // Keep same location centered when zooming
            CenterMap();
        }

        m_Zoom = Zoom;
    }
}

void CIwMap::SetOverlayBox(CIwUIElement* pOverlayBox)
{
    CIwUIElement* pOverlay = GetChildNamed("Overlay");

    m_Geonames.SetOverlays(pOverlay, pOverlayBox);
}

void CIwMap::AddOverlay(CIwVec2 Position, const char* Title, double Latitude, double Longitude,
    const char* Summary, const char* ImageURL)
{
    // TODO Discard overlays which are off-screen
    m_Geonames.AddOverlay(Position, Title, Latitude, Longitude, Summary, ImageURL);
}

bool CIwMap::ClickOverlay(CIwUIElement* pClick)
{
    return m_Geonames.ClickOverlay(pClick);
}

// IwUIElement virtuals
void CIwMap::UpdateElement(int32 deltaMS)
{
    CIwUIScrollableView::UpdateElement(deltaMS);

    const int32 numTilesX = NUM_TILES_X;
    const int32 numTilesY = NUM_TILES_Y;

    // Have we got a queued request to download images again?
    if (m_ClearOnUpdate)
    {
        // Clear overlays
        m_Geonames.DestroyAllOverlays();
        m_RequestedGeonames = false;

        // Clear images
        ClearImages();
        m_ClearOnUpdate = false;
    }

    // Request necessary tiles
    for (int i=0; i<numTilesX; ++i)
    {
        for (int j=0; j<numTilesY; ++j)
        {
            CIwUIImage* pTile = m_Tiles[i][j];

            bool haveRequestedTile = false;
            if (pTile->GetProperty("haveRequestedTile", haveRequestedTile) &&
                !haveRequestedTile)
            {
                RequestTile(i, j);
            }
        }
    }

    // Request overlays
    if (!m_RequestedGeonames && (m_Zoom < 3))
    {
        m_RequestedGeonames = true;

        CIwUIImage* pTile = m_Tiles[numTilesX/2][numTilesY/2];

        double latitude, longitude;
        GetLocationFromTilePosition(pTile->GetPos(), latitude, longitude);

        m_Geonames.RequestOverlays(latitude, longitude, m_Zoom, pTile->GetSize(), pTile);
    }

    // Update current location marker position
    s3eLocation location;
    if (s3eLocationGet(&location) == S3E_RESULT_SUCCESS)
    {
        CIwVec2 markerPos;
        GetElementPositionFromLocation(location.m_Latitude, location.m_Longitude,
            markerPos);

        // The marker CIwUIElement position must be limited to avoid the fixed number calculation overflow
        const int maxDistance = 10000;
        markerPos.x = std::max(std::min(markerPos.x, maxDistance), -maxDistance);
        markerPos.y = std::max(std::min(markerPos.y, maxDistance), -maxDistance);

        m_Marker->SetPos(markerPos);
        m_Marker->SetVisible(true);
    }
    else
    {
        m_Marker->SetVisible(false);
    }
}

void CIwMap::Clone(CIwUIElement* pTarget) const
{
    IW_UI_CLONE_SUPERCLASS(pTarget, CIwMap, CIwUIScrollableView);
}

void CIwMap::Activate(bool val)
{
    CIwUIScrollableView::Activate(val);

    const int32 numTilesX = NUM_TILES_X;
    const int32 numTilesY = NUM_TILES_Y;

    if (val)
    {
        m_LoadingTexture = (CIwTexture*)
            IwGetResManager()->GetResNamed("close_iconx2", "CIwTexture");

        // Create tiles
        for (int i=0; i<numTilesX; ++i)
        {
            for (int j=0; j<numTilesY; ++j)
            {
                CIwUIImage* pTile = new CIwUIImage();

                pTile->SetTexture(m_LoadingTexture);
                pTile->SetProperty("haveRequestedTile", false);

                m_Tiles[i][j] = pTile;
                AddChild(pTile);
            }
        }

        // Store initial location (corresponds to no scrolling)
        s3eLocation location;
        if (s3eLocationGet(&location) == S3E_RESULT_SUCCESS)
        {
            m_StartLatitude = location.m_Latitude;
            m_StartLongitude = location.m_Longitude;
        }
        else
        {
            // The default location
            m_StartLatitude = 51.511791;
            m_StartLongitude = -0.191084;
        }

        m_CentralTilePos = CIwVec2::g_Zero;
        SetScrollPosition(CIwVec2::g_Zero);

        // Initially no zoom
        m_Zoom = 0;

        // Lookup children
        m_Marker = GetChildNamed("Marker");
    }
    else
    {
        // Destroy tiles
        for (int i=0; i<numTilesX; ++i)
        {
            for (int j=0; j<numTilesY; ++j)
            {
                CIwUIImage*& pTile = m_Tiles[i][j];
                ReleaseTile(i, j);
                RemoveChild(pTile);
                delete pTile;
                pTile = NULL;
            }
        }
    }
}

void CIwMap::RequestTile(int i, int j)
{
    // Request new data
    CIwUIImage* pTile = m_Tiles[i][j];
    pTile->SetTexture(m_LoadingTexture);

    const CIwVec2& size = GetSize();

    CIwVec2 tilePos(
        (m_CentralTilePos.x + i - NUM_TILES_X/2) * size.x,
        (m_CentralTilePos.y + j - NUM_TILES_Y/2) * size.y
    );
    CIwVec2 tileSize(GetSize());

    pTile->SetPos(tilePos);
    pTile->SetSize(tileSize);

    double latitude, longitude;
    GetLocationFromTilePosition(tilePos, latitude, longitude);

    m_MapImage.RequestImage(latitude, longitude, m_Zoom, size, pTile);
    pTile->SetProperty("haveRequestedTile", true);
}

void CIwMap::ReleaseTile(int i, int j)
{
    // Release old data
    CIwUIImage* pTile = m_Tiles[i][j];

    CIwTexture* pTexture = NULL;
    if (pTile->GetProperty("texture", pTexture) &&
        pTexture != m_LoadingTexture)
    {
        delete pTexture;
        pTile->FreeTexture();
    }

    CIwSVec2 loadingTexutreSize(
        (int16)m_LoadingTexture->GetWidth(),
        (int16)m_LoadingTexture->GetHeight());

    pTile->SetTexture(m_LoadingTexture);
    pTile->SetProperty("uv1", loadingTexutreSize);
    pTile->SetProperty("haveRequestedTile", false);

    m_MapImage.CancelImage(pTile);
}

void CIwMap::ClearImages()
{
    const int32 numTilesX = NUM_TILES_X;
    const int32 numTilesY = NUM_TILES_Y;

    for (int i=0; i<numTilesX; ++i)
    {
        for (int j=0; j<numTilesY; ++j)
        {
            ReleaseTile(i, j);
        }
    }
}

void CIwMap::ArrangeTiles(const CIwVec2& size)
{
    if ((size.x > 0) && (size.y > 0))
    {
        const int32 numTilesX = NUM_TILES_X;
        const int32 numTilesY = NUM_TILES_Y;

        CIwVec2 tileIndex(m_CentralTilePos);

        CIwVec2 firstTilePos(
            (tileIndex.x - numTilesX / 2) * size.x,
            (tileIndex.y - numTilesY / 2) * size.y);

        CIwVec2 tilePos(firstTilePos);
        for (int i=0; i<numTilesX; ++i)
        {
            tilePos.y = firstTilePos.y;

            for (int j=0; j<numTilesY; ++j)
            {
                CIwUIImage* pTile = m_Tiles[i][j];

                pTile->SetPos(tilePos);
                pTile->SetSize(size);

                tilePos.y += size.y;
            }

            tilePos.x += size.x;
        }
    }
}

void CIwMap::CenterMap()
{
    // Keep same location centered when zooming
    double currentLatitude, currentLongitude;
    GetLocationFromTilePosition(GetScrollPosition(),
        currentLatitude, currentLongitude);
    m_StartLatitude = currentLatitude;
    m_StartLongitude = currentLongitude;
    SetScrollPosition(CIwVec2::g_Zero);
    m_CentralTilePos = CIwVec2::g_Zero;
}

void CIwMap::GetLocationFromTilePosition(const CIwVec2& pos,
    double& latitude, double& longitude) const
{
    CMapProjection mapProjection(m_Zoom);
    latitude = mapProjection.yToLat(mapProjection.latToY(m_StartLatitude) - pos.y);
    longitude = mapProjection.xToLong(mapProjection.longToX(m_StartLongitude) + pos.x);
}

void CIwMap::GetElementPositionFromLocation(double latitude, double longitude,
    CIwVec2& pos) const
{
    CMapProjection mapProjection(m_Zoom);
    pos.x = (int32)(mapProjection.longToX(longitude) -
        mapProjection.longToX(m_StartLongitude));
    pos.y = (int32)(mapProjection.latToY(m_StartLatitude) -
        mapProjection.latToY(latitude));

    const CIwVec2& size = GetSize();
    pos.x += size.x / 2;
    pos.y += size.y / 2;
}

void CIwMap::Arrange(const CIwVec2& pos, const CIwVec2& size)
{
    ArrangeTiles(size);

    CIwUIScrollableView::Arrange(pos, size);
}

void CIwMap::OnSizeChanged()
{
    // Download all images again
    m_ClearOnUpdate = true;
}

// IwUIScrollableView virtuals
CIwUIRect CIwMap::GetContentRect() const
{
    CIwVec2 size = GetSize();

    return CIwUIRect(
        (m_CentralTilePos.x - 1) * size.x,
        (m_CentralTilePos.y - 1) * size.y,
        3 * size.x,
        3 * size.y);
}

void CIwMap::OnScrollPositionChanged()
{
    CIwUIScrollableView::OnScrollPositionChanged();

    const int32 numTilesX = NUM_TILES_X;
    const int32 numTilesY = NUM_TILES_Y;

    CIwVec2 scrollPosition = GetScrollPosition();
    CIwVec2 size = GetSize();

    if ((size.x <= 0) || (size.y <= 0))
    {
        return;
    }

    CIwVec2 newCentralTilePos(m_CentralTilePos);

    // When moving more than half a tile away from current central position,
    // step to new central position.
    for (int c=0; c<2; ++c)
    {
        if ((scrollPosition[c] + (3*size[c]) / 4) < size[c] * (m_CentralTilePos[c]))
        {
            newCentralTilePos[c] = (scrollPosition[c] - size[c] / 4) / size[c];
        }
        else if ((scrollPosition[c] + size[c] / 4) > size[c] * (m_CentralTilePos[c]+1))
        {
            newCentralTilePos[c] = (scrollPosition[c] + size[c] / 4) / size[c];
        }
    }

    // Do we need to rearrange tiles?
    if (m_CentralTilePos != newCentralTilePos)
    {
        CIwVec2 tilePosDelta = newCentralTilePos - m_CentralTilePos;
        m_CentralTilePos = newCentralTilePos;
        m_RequestedGeonames = false;

        CIwUIImage* pOldTiles[numTilesX][numTilesY];

        int i,j;
        for (i=0; i<numTilesX; ++i)
            for (j=0; j<numTilesY; ++j)
                pOldTiles[i][j] = m_Tiles[i][j];

        for (i=0; i<numTilesX; ++i)
        {
            for (j=0; j<numTilesY; ++j)
            {
                int32 x = i + tilePosDelta.x;
                int32 y = j + tilePosDelta.y;

                if ((0 <= x) && (x < numTilesX) &&
                    (0 <= y) && (y < numTilesY))
                {
                    // Reuse old tile
                    m_Tiles[i][j] = pOldTiles[x][y];
                }
                else
                {
                    // Release old tile so we request new
                    // tile next update
                    x %= numTilesX; if(x < 0) x += numTilesX;
                    y %= numTilesY; if(y < 0) y += numTilesY;

                    CIwUIImage* pTile = pOldTiles[x][y];
                    m_Tiles[i][j] = pTile;

                    ReleaseTile(i, j);
                }
            }
        }
    }

    if (IwGetUIView()->InFocusChain(this))
    {
        // Update which geoname button has focus
        m_Geonames.SetFocus(GetScrollPosition(), GetSize());
    }
}

// IwUIAnimationObserver virtualsa
void CIwMap::NotifyProgress(CIwUIAnimator* pAnimator)
{
}

void CIwMap::NotifyStopped(CIwUIAnimator* pAnimator)
{
    // Clear the images once animation has finished
    m_ClearOnUpdate = true;
    CenterMap();
}
