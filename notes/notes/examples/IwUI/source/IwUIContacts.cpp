/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

//-----------------------------------------------------------------------------
// ExampleIwUIContacts
//-----------------------------------------------------------------------------

/**
 * @page ExampleIwUIContacts IwUI Contacts Example
 *
 * @include IwUIContacts.cpp
 */

// Standard includes
#include "IwUIContacts.h"

// Standard includes
#include "IwGx.h"
#include "IwUI.h"
#include "s3eContacts.h"

// Static data
static CIwUIContacts* s_App;

//-----------------------------------------------------------------------------

int main()
{
    CIwUIContacts app;
    app.Run();
    return 0;
}

//-----------------------------------------------------------------------------

CIwUIContacts* GetApp()
{
    IwAssertMsg(UI, s_App, ("Accessing NULL app singleton"));

    return s_App;
}

CIwUIContacts::CIwUIContacts() :
    m_Contacts(NULL),
    m_CreateDialog(NULL),
    m_DeleteDialog(NULL),
    m_EditUID(-1),
    m_DeleteUID(-1),
    m_ItemSource(NULL)
{
    // Set singleton
    IwAssert(UI, !s_App);
    s_App = this;

    // Initialise the IwUI module
    IwUIInit();

    // Register class factories
    IW_CLASS_REGISTER(CContactsViewHandler);
    IW_CLASS_REGISTER(CContactsItemSource);
    IW_CLASS_REGISTER(CCreateContactHandler);
    IW_CLASS_REGISTER(CDeleteContactHandler);

    // Instantiate singletons.
    new CIwUIController;
    new CIwUIView;
    new CIwUITextInput;
}

CIwUIContacts::~CIwUIContacts()
{
    // Destroy singletons.
    delete IwGetUITextInput();
    delete IwGetUIController();
    delete IwGetUIView();

    // Remove class factories
    IW_CLASS_REMOVE(CContactsViewHandler);
    IW_CLASS_REMOVE(CContactsItemSource);
    IW_CLASS_REMOVE(CCreateContactHandler);
    IW_CLASS_REMOVE(CDeleteContactHandler);

    // Terminate the IwUI module
    IwUITerminate();

    // Clear singleton
    IwAssert(UI, s_App == this);
    s_App = NULL;
}

// Public interface
void CIwUIContacts::Run()
{
    Init();

    uint64 lastTime = s3eTimerGetMs();
    while (!s3eDeviceCheckQuitRequest())
    {
        // Calculate an update time step.
        uint64 currentTime = s3eTimerGetMs();
        int32 updateTime = currentTime > lastTime ?
            MIN((int32)(currentTime - lastTime), 1000) : 0;
        lastTime = currentTime;

        // Update & render UI
        Update(updateTime);
        Render();

        // Standard device interaction
        s3eDeviceYield();
        s3eKeyboardUpdate();
        s3ePointerUpdate();
    }
}

void CIwUIContacts::SetActiveItemSource(IContactsItemSource* pItemSource)
{
    IwAssert(UI, !pItemSource || !m_ItemSource);

    m_ItemSource = pItemSource;
}

void CIwUIContacts::UpdateAllContacts()
{
    m_ItemSource->UpdateAllUIDs();
}

void CIwUIContacts::ShowCreateDialog(int32 editUID)
{
    m_EditUID = editUID;

    m_CreateDialog->SetVisible(true);
    IwGetUIView()->SetModal(m_CreateDialog);
}

static void GetField(int32 uid, s3eContactsField field, ContactField& data)
{
    if ((s3eContactsGetNumEntries(uid, field) < 1) ||
        (s3eContactsGetField(uid, field, data, sizeof(data)) != S3E_RESULT_SUCCESS))
    {
        // Set empty string
        data[0] = '\0';
    }
}

static void SetField(int32 uid, s3eContactsField field, const char* data, bool editExisting)
{
    if (!editExisting && !strlen(data))
    {
        // No point creating empty field
        return;
    }
    s3eContactsSetField(uid, field, data);
}

void CIwUIContacts::GetInitialContact(ContactField& firstName,
    ContactField& lastName, ContactField& mobileNumber) const
{
    if (m_EditUID == -1)
    {
        firstName[0] = '\0';
        lastName[0] = '\0';
        mobileNumber[0] = '\0';
    }
    else
    {
        GetField(m_EditUID, S3E_CONTACTS_FIELD_FIRST_NAME, firstName);
        GetField(m_EditUID, S3E_CONTACTS_FIELD_LAST_NAME, lastName);
        GetField(m_EditUID, S3E_CONTACTS_FIELD_MOBILE_PHONE, mobileNumber);
    }
}

void CIwUIContacts::CreateConfirmed(const char* pFirstName,
    const char* pLastName, const char* pMobileNumber)
{
    // Either editing existing, or create new contact
    bool editingExisting = m_EditUID != -1;
    int32 uid = editingExisting ? m_EditUID : s3eContactsCreate();

    if (uid != -1)
    {
        SetField(uid, S3E_CONTACTS_FIELD_FIRST_NAME, pFirstName, editingExisting);
        SetField(uid, S3E_CONTACTS_FIELD_LAST_NAME, pLastName, editingExisting);
        SetField(uid, S3E_CONTACTS_FIELD_MOBILE_PHONE, pMobileNumber, editingExisting);

        // Tell table view to display update or new contact
        if (editingExisting)
        {
            m_ItemSource->UpdatedUID(uid);
        }
        else
        {
            m_ItemSource->CreatedUID(uid);
        }
    }
    else
    {
        IwAssertMsg(UI, false, ("Failed to create contact"));
    }

    m_EditUID = -1;
}

void CIwUIContacts::HideCreateDialog()
{
    IwGetUIView()->SetModal(NULL);
    m_CreateDialog->SetVisible(false);
}

void CIwUIContacts::ShowDeleteDialog(int32 contactIndex)
{
    // Get UID of contact we want to delete
    int32 uid = m_ItemSource->GetUID(contactIndex);
    IwAssertMsg(UI, uid != -1, ("Trying to delete invalid contact index"));

    if (uid != -1)
    {
        // Store UID we're trying to delete
        m_DeleteUID = uid;

        // Show delete contact dialog
        m_DeleteDialog->SetVisible(true);
        IwGetUIView()->SetModal(m_DeleteDialog);
    }
}

void CIwUIContacts::DeleteConfirmed()
{
    IwAssertMsg(UI, m_DeleteUID != -1, ("Never showed delete dialog"));

    s3eResult result = s3eContactsDelete(m_DeleteUID);
    IwAssertMsg(UI, result == S3E_RESULT_SUCCESS, ("Failed to delete contact"));

    // Tell table view to remove contact
    m_ItemSource->DeletedUID(m_DeleteUID);

    m_DeleteUID = -1;
}

void CIwUIContacts::HideDeleteDialog()
{
    IwGetUIView()->SetModal(NULL);
    m_DeleteDialog->SetVisible(false);
}

// Private interface
void CIwUIContacts::Init()
{
    // Load the hello world UI
    IwGetResManager()->LoadGroup("IwUIContacts.group");

    // Set the default style sheet
    IwGetUIStyleManager()->SetStylesheet("iwui");

    // Create soft-keyboard and ensure appears on top.
    IwGetUITextInput()->CreateSoftKeyboard();
    IwGetUIView()->GetChildNamed("soft_keyboard")->SetRenderSlot(INT32_MAX-1);

    // Clone UI elements
    m_Contacts = CIwUIElement::CreateFromResource("Contacts");
    m_CreateDialog = CIwUIElement::CreateFromResource("CreateDialog");
    m_DeleteDialog = CIwUIElement::CreateFromResource("DeleteDialog");

    // Set initial visibility
    m_CreateDialog->SetVisible(false);
    m_DeleteDialog->SetVisible(false);

    // Add into view
    IwGetUIView()->AddElementToLayout(m_Contacts);
    IwGetUIView()->AddElementToLayout(m_CreateDialog);
    IwGetUIView()->AddElementToLayout(m_DeleteDialog);

    // Choose initial focus
    IwGetUIView()->ChooseFocus(m_Contacts);
}

void CIwUIContacts::Update(int32 deltaMS)
{
    // Update the controller (this will generate control events etc.)
    IwGetUIController()->Update();

    // Update the view (this will do animations etc.)
    IwGetUIView()->Update(deltaMS);
}

void CIwUIContacts::Render()
{
    // Clear surface
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Render the UI
    IwGetUIView()->Render();

    // Flush IwGx
    IwGxFlush();

    // Display the rendered frame
    IwGxSwapBuffers();
}
