/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwUILocation IwUI Location Example
 * The following example looks up the users location and fetches a map of the area.
 * The map is then populated by surrounding artifacts.
 *
 * The main classes used to achieve this are:
 *
 *  - CIwUIView
 *  - CIwUIController
 *  - CIwUIElement
 *  - CIwUIButton
 *  - CIwUILabel
 *  - CIwUIImage
 *  - CIwHTTP
 *
 * The main functions used to achieve this are:
 *
 *  - s3eLocationStart()
 *  - s3eLocationGet();
 *
 * This example demonstrates using the s3eLocation api to detect the users location. The example then
 * displays a map of the surrounding location and overlays it with information from wikipedia.
 *
 * All the map tiles, the overlay information and thumbnails are downloaded over http automatically.
 * These http requests are queued up and fetched in series by the HTTPQueue object that calls the
 * appropriate callback when the request completes successfully.
 *
 * The tiles and overlay are stored in a ScrollableView element so that the user can scroll around the
 * map on a touchscreen. Additionally the arrow or numpad keys can be used to scroll while the slider or
 * keys 1 and 3 can be used zoom in and out.
 *
 * @image html IwUILocationImage.png
 *
 * @include IwUILocation.cpp
 */

// Library includes
#include "IwGx.h"
#include "IwUI.h"
#include "IwMap.h"
#include "IwHTTPQueue.h"
#include "s3eOSExec.h"
#include "s3eOSReadString.h"
#include "s3eKeyboard.h"
#include "s3eLocation.h"

extern bool g_IwTextureAssertPowerOf2;

// The Base UI Element of the Application
CIwUIElement* g_Main;
// The quit dialog
CIwUIElement* g_QuitScreen;
// The overlay box
CIwUIElement* g_OverlayBox;
// The scrollable map view
CIwMap* g_Map;
// The zoom slider
CIwUISlider * g_Zoom;

// Controller class to handle button clicks
class CController : public CIwUIController
{
public:
    CController()
    {
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClickQuit, CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClickShowQuitScreen, CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClickHideQuitScreen, CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClickOverlay, CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClickHideOverlay, CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT2(this, "CController", CController, OnZoomChanged, CIwUIElement*, int16)
    }

private:
    enum IwUILocationView
    {
        IW_UI_MAIN,             // The main screen
        IW_UI_QUIT,             // The quit screen
    };

    void SetView(IwUILocationView mode)
    {
        if (mode == IW_UI_MAIN)
        {
            // Hide all other screens
            g_QuitScreen->SetVisible(false);
            g_OverlayBox->SetVisible(false);

            IwGetUIView()->SetModal(NULL);
        }
        else if (mode == IW_UI_QUIT)
        {
            // Display the quit dialog
            g_QuitScreen->SetVisible(true);

            IwGetUIView()->SetModal(g_QuitScreen);

            {
                // Play an anim on this element
                IwGetUIAnimManager()->StopAnim(g_QuitScreen);
                IwGetUIAnimManager()->PlayAnim("popupAnim", g_QuitScreen, true);
            }
        }
    }

    void OnZoomChanged(CIwUIElement*, int16 zoom)
    {
        g_Map->SetZoom(zoom);
    }

    void OnClickShowQuitScreen(CIwUIElement*)
    {
        // Open the quit dialog
        SetView(IW_UI_QUIT);
    }

    void OnClickHideQuitScreen(CIwUIElement*)
    {
        // Hide the quit dialog
        SetView(IW_UI_MAIN);
    }

    void OnClickQuit(CIwUIElement*)
    {
        // Quit the application
        static bool disableExit =
            s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WS8 ||
            s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WS81 ||
            s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WIN10;

        if (!disableExit)
        {
            s3eDeviceRequestQuit();
        }
    }

    void OnClickOverlay(CIwUIElement* pButton)
    {
        // Show the overlay
        if (g_Map->ClickOverlay(pButton))
        {
            // Display the dialog
            g_OverlayBox->SetVisible(true);

            IwGetUIView()->SetModal(g_OverlayBox);

            {
                // Play an anim on this element
                IwGetUIAnimManager()->StopAnim(g_OverlayBox);
                IwGetUIAnimManager()->PlayAnim("popupAnim", g_OverlayBox, true);
            }
        }
    }

    void OnClickHideOverlay(CIwUIElement*)
    {
        // Cancel overlay http request
        IwGetHTTPQueue()->CancelByArgument((void*)g_OverlayBox->GetChildNamed("Panel"));

        CIwUIImage * pImage = (CIwUIImage*)g_OverlayBox->GetChildNamed("Image", true, true);
        if (pImage)
        {
            CIwUIElement * pParent = pImage->GetParent();
            pParent->RemoveChild(pImage);
            delete pImage;
        }

        // Hide the overlay dialog box
        SetView(IW_UI_MAIN);
    }

    virtual bool FilterEvent(CIwEvent* pEvent)
    {
        if (pEvent->GetID() == IWUI_EVENT_KEY)
        {
            CIwUIEventKey* pEventKey = IwSafeCast<CIwUIEventKey*>(pEvent);

            if (!pEventKey->GetPressed())
            {
                // Zoom keys, set slider, which in turn controls map
                switch(pEventKey->GetKey())
                {
                case s3eKey1:
                    g_Zoom->SetValue(g_Zoom->GetValue()-1);
                    return true;

                case s3eKey3:
                    g_Zoom->SetValue(g_Zoom->GetValue()+1);
                    return true;

                default:
                    break;
                }
            }
        }

        // Default handling
        return CIwUIController::FilterEvent(pEvent);
    }
};

int main()
{
    IW_CALLSTACK("main");
    g_IwTextureAssertPowerOf2 = false;
    bool quit = false;

    // Initialize IwUI
    IwUIInit();

    IW_CLASS_REGISTER(CIwMap);

    {
        // Instantiate the view and controller singletons.
        CIwUIView view;
        CController controller;

        // Initialise the http queue
        CIwHTTPQueue * pHTTP = new CIwHTTPQueue();

        // Load the resource group
        CIwResGroup* pResGroup = IwGetResManager()->LoadGroup("IwUILocation.group");

        // Set style sheet
        CIwResource* pResource = pResGroup->GetResNamed("Location-Style", IW_UI_RESTYPE_STYLESHEET);
        IwGetUIStyleManager()->SetStylesheet(IwSafeCast<CIwUIStylesheet*>(pResource));

        // Create the main UI
        g_Main = CIwUIElement::CreateFromResource("Main");
        view.AddElement(g_Main);
        view.AddElementToLayout(g_Main);

        // Create the quit dialog
        g_QuitScreen = CIwUIElement::CreateFromResource("Quit");
        view.AddElement(g_QuitScreen);
        view.AddElementToLayout(g_QuitScreen);

        // Create the overlay dialog
        g_OverlayBox = CIwUIElement::CreateFromResource("OverlayBox");
        view.AddElement(g_OverlayBox);
        view.AddElementToLayout(g_OverlayBox);

        // Resolve contained elements
        g_Map = (CIwMap*)g_Main->GetChildNamed("Map");
        g_Map->SetOverlayBox(g_OverlayBox);
        g_Zoom = (CIwUISlider*)g_Main->GetChildNamed("Zoom");

        // Start tracking location
        s3eLocationStart();

        // Give the quit button focus by default
        CIwUIElement* pFocusElement = g_Main->GetChildNamed("ButtonQuit");
        IwGetUIView()->RequestFocus(pFocusElement);

        int32 updateTime = 0;
        while (!quit)
        {
            int64 start = s3eTimerGetMs();
            IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

            pHTTP->Update();
            IwGetUIController()->Update();

            IwGetUIView()->Update(updateTime);
            IwGetUIView()->Render();

            IwGxFlush();
            IwGxSwapBuffers();

            s3eDeviceYield();
            s3eKeyboardUpdate();
            s3ePointerUpdate();

            // Attempt frame rate
            int32 frameTime = 1000 / 30; // 30fps
            int32 tickTime = (int32)(s3eTimerGetMs() - start);

            if (tickTime < frameTime)
            {
                int32 yield = frameTime - tickTime;
                yield = MAX(0, MIN(yield, 1000));
                s3eDeviceYield(yield);
            }

            updateTime = (int32)(s3eTimerGetMs() - start);
            updateTime = MAX(frameTime, MIN(updateTime, 1000));

            quit |= (s3eDeviceCheckQuitRequest() != 0);
        }

        s3eLocationStop();

        view.DestroyElements();

        delete pHTTP;

        // Destroy the resource group
        IwGetResManager()->DestroyGroup(pResGroup);
    }

    IW_CLASS_REMOVE(CIwMap);

    // Terminate the IwUI module
    IwUITerminate();
    return 0;
}
