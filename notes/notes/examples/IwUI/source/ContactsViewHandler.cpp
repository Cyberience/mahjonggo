/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

// Marmalade includes
#include "IwUIElementEventHandler.h"
#include "IwUITableView.h"

// Project includes
#include "IwUIContacts.h"

//-----------------------------------------------------------------------------

class CContactsViewHandler : public CIwUIElementEventHandler
{
public:
    IW_MANAGED_DECLARE(CContactsViewHandler)
        CContactsViewHandler() : m_TableView(NULL) {}

private:
    // CIwUIElementEventHandler virtuals
    virtual void Activate(bool val)
    {
        CIwUIElementEventHandler::Activate(val);

        if (val)
        {
            IW_UI_CREATE_HANDLER_SLOT1(this, CContactsViewHandler,
                CreateContact, CIwUIElement*);
            IW_UI_CREATE_HANDLER_SLOT1(this, CContactsViewHandler,
                DeleteContact, CIwUIElement*);
            IW_UI_CREATE_HANDLER_SLOT1(this, CContactsViewHandler,
                UpdateContact, CIwUIElement*);
            IW_UI_CREATE_HANDLER_SLOT1(this, CContactsViewHandler,
                UpdateAllContacts, CIwUIElement*);

            GetElement()->LookupChildNamed(m_TableView, "TableView");
        }
        else
        {
            IW_UI_DESTROY_HANDLER_SLOTS(this);

            m_TableView = NULL;
        }
    }

    virtual void Clone(CIwUIElementEventHandler* pTarget) const
    {
        IW_UI_CLONE_SUPERCLASS(pTarget, CContactsViewHandler, CIwUIElementEventHandler)
    }

    // Slot functions
    void CreateContact(CIwUIElement*)
    {
        GetApp()->ShowCreateDialog();
    }

    void DeleteContact(CIwUIElement*)
    {
        if (m_TableView)
        {
            // Have we got a selection?
            int32 selection = m_TableView->GetSelection();
            if (selection != -1)
            {
                GetApp()->ShowDeleteDialog(selection);
            }
        }
    }

    void UpdateContact(CIwUIElement* pButtonElement)
    {
        // Should find UID stored of parent of button (the table view item).
        int32 uid = -1;
        pButtonElement->GetParent()->GetProperty("contactUID", uid);

        GetApp()->ShowCreateDialog(uid);
    }

    void UpdateAllContacts(CIwUIElement*)
    {
        GetApp()->UpdateAllContacts();
    }

    // Member data
    CIwUITableView* m_TableView;
};

IW_MANAGED_IMPLEMENT_FACTORY(CContactsViewHandler)
