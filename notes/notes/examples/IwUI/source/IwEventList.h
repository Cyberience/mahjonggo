/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
#pragma once

#include "IwUI.h"
#include "IwUIElement.h"
#include "IwEventDate.h"
#include "IwArray.h"
#include "IwManaged.h"
#include "IwSerialise.h"

class CIwEventList : public CIwManaged
{
public:
    IW_MANAGED_DECLARE(CIwEventList);

    void AddEvent(const char* title, time_t time);
    void Serialise();

    //Array of Events
    CIwArray<CIwEventDate*> m_EventList;

    CIwEventList(void);
    ~CIwEventList(void);
};


extern CIwUIElement* g_ItemTemplate;
extern CIwEventList * g_EventList;
extern time_t g_Today;
extern time_t g_Day;
