/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

//-----------------------------------------------------------------------------
// ExampleIwUIAnimatior
//-----------------------------------------------------------------------------

/**
 * @page ExampleIwUIAnimation IwUI Animation Example
 * The following example demonstrates playing animations on an element in
 * response to element events.
 *
 * The main classes used to achieve this are:
 *  - CIwUIElementEventHandler
 *  - CIwUIAnimManager
 *
 * The main functions used to achieve this are:
 *  - CIwUIAnimManager::PlayAnim
 *  - CIwUIAnimManager::SetObserver
 *  - CIwUIAnimManager::StopAnim
 *
 * This example defines a custom CIwUIElementEventHandler derived class. This is
 * attached to an element in data. The handler registers a slot which are called
 * by buttons within the element. The slot controls the playing on animations on
 * another element. The animations are defined in data. An animation observer is
 * installed. The obsever waits until an animation finishes before exiting the
 * application.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwUIAnimationImage.png
 *
 * @include IwUIAnimation.cpp
 */
#include "IwGx.h"
#include "IwUI.h"

// Class to catch button events to play an animation.
// Installs an oberserver on the animation to play a second animation when
// the first has finished.
class CButtonEventHandler : public CIwUIElementEventHandler,
    public IIwUIAnimatorObserver
{
public:
    IW_MANAGED_DECLARE(CButtonEventHandler)

protected:
    // IIwUIEventHandler virtuals
    virtual void Activate(bool val)
    {
        CIwUIElementEventHandler::Activate(val);

        if (val)
        {
            IW_UI_CREATE_HANDLER_SLOT1(this, CButtonEventHandler, FadeIn, CIwUIElement*)
        }
    }

    virtual void Clone(CIwUIElementEventHandler* pTarget) const
    {
        IW_UI_CLONE_SUPERCLASS(pTarget, CButtonEventHandler, CIwUIElementEventHandler)
    }

    // IIwUIAnimationObserver
    virtual void NotifyProgress(CIwUIAnimator* pAnimator)
    {
    }

    virtual void NotifyStopped(CIwUIAnimator* pAnimator)
    {
        CIwUIElement* pElement = pAnimator->GetTarget();

        // Fade in animation has finished or been stopped.
        // Play fade out, blending from current state
        CIwUIAnimation* pAnim = GetAnimation(pElement, false);
        IwGetUIAnimManager()->PlayAnim(pAnim, pElement, false, false, 1000);
    }

private:
    void FadeIn(CIwUIElement* pElement)
    {
        CIwUIAnimation* pAnim = GetAnimation(pElement, true);

        IwGetUIAnimManager()->StopAnim(pElement);
        uint32 handle = IwGetUIAnimManager()->PlayAnim(pAnim, pElement);
        IwGetUIAnimManager()->SetObserver(handle, this);
    }

    CIwUIAnimation* GetAnimation(CIwUIElement* pElement, bool inNotOut)
    {
        CIwUIAnimation* pAnimation = NULL;
        pElement->GetProperty(inNotOut ? "animIn" : "animOut", pAnimation);
        return pAnimation;
    }
};
IW_MANAGED_IMPLEMENT_FACTORY(CButtonEventHandler)

//-----------------------------------------------------------------------------
void ExampleInit()
{
    //Initialise the IwUI module
    IwUIInit();
    IW_CLASS_REGISTER(CButtonEventHandler);

    //Instantiate the view and controller singletons.
    //IwUI will not instantiate these itself, since they can be subclassed to add functionality.
    new CIwUIController;
    new CIwUIView;

    //Load the hello world UI
    IwGetResManager()->LoadGroup("IwUIAnimation.group");

    // Create main element
    CIwUIElement* pElement = CIwUIElement::CreateFromResource("Animation");
    IwGetUIView()->AddElementToLayout(pElement);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // Stop all animations
    IwGetUIAnimManager()->StopAllAnims();

    // Destroy the view and controller singletons.
    delete IwGetUIController();
    delete IwGetUIView();

    //Terminate the IwUI module
    IW_CLASS_REMOVE(CButtonEventHandler);
    IwUITerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    //Update the controller (this will generate control events etc.)
    IwGetUIController()->Update();

    //Update the view (this will do animations etc.) The SDK's example framework has a fixed
    //framerate of 20fps, so we pass that duration to the update function.
    IwGetUIView()->Update(1000/20);

    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    //Render the UI
    IwGetUIView()->Render();

    //Flush IwGx
    IwGxFlush();

    //Display the rendered frame
    IwGxSwapBuffers();
}
