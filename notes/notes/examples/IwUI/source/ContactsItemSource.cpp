/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

// Standard includes
#include "IwUILabel.h"
#include "IwUILayoutGrid.h"
#include "IwUITableView.h"
#include "IwUITableViewItemSource.h"
#include "s3eContacts.h"

// Project includes
#include "IwUIContacts.h"

//-----------------------------------------------------------------------------

class CContactsItemSource : public CIwUITableViewItemSource,
    public IContactsItemSource
{
public:
    IW_MANAGED_DECLARE(CContactsItemSource)
    CContactsItemSource() :
        m_Active(false),
        m_ColumnWidth(-1),
        m_RowHeight(-1),
        m_Template(NULL),
        m_TemplateClone(NULL)
    {
    }

    virtual ~CContactsItemSource()
    {
        IwAssert(UI, !m_TemplateClone);
        delete m_Template;
    }

private:
    // IwManaged virtuals
    virtual void ParseCloseChild(CIwTextParserITX* pParser, CIwManaged* pChild)
    {
        m_Template = IwSafeCast<CIwUIElement*>(pChild);
    }

    virtual void Serialise()
    {
        CIwUITableViewItemSource::Serialise();

        IwSerialiseManagedObject(*(CIwManaged**)&m_Template);
    }

    virtual void Resolve()
    {
        CIwUITableViewItemSource::Resolve();

        CIwManaged* pTemplate = m_Template;
        if (pTemplate)
        {
            pTemplate->Resolve();
        }
    }

    // IwUITableViewItemSource virtuals
    virtual bool IsRowAvailable(int32 row) const
    {
        return (0 <= row) && (row < (int32)m_ContactUIDs.size());
    }

    virtual CIwUIElement* CreateItem(int32 row)
    {
        IwAssert(UI, IsRowAvailable(row));

        char buffer[S3E_CONTACTS_STRING_MAX];

        const int32 uid = m_ContactUIDs[row];

        // Store UID with item for future reference
        CIwUIElement* pItem = m_Template->Clone();
        pItem->SetProperty("contactUID", uid);

        // Set labels within template
        const char* pLabelNames[] =
        {
            "FirstValue",
            "LastValue",
            "MobileValue"
        };

        s3eContactsField fields[] =
        {
            S3E_CONTACTS_FIELD_FIRST_NAME,
            S3E_CONTACTS_FIELD_LAST_NAME,
            S3E_CONTACTS_FIELD_MOBILE_PHONE
        };

        for (int16 i=0; i<3; ++i)
        {
            const s3eContactsField field = fields[i];

            CIwUILabel* pLabel;
            pItem->LookupChildNamed(pLabel, pLabelNames[i]);

            if ((s3eContactsGetNumEntries(uid, field) >= 1) &&
                s3eContactsGetField(uid, field, buffer, sizeof(buffer)) == S3E_RESULT_SUCCESS)
            {
                pLabel->SetCaption(buffer);
            }
            else
            {
                pLabel->SetCaption("");
            }
        }

        // Use unique identifier to create unique name
        sprintf(buffer, "Contact: %d", uid);
        pItem->SetName(buffer);

        return pItem;
    }

    virtual void ReleaseItem(CIwUIElement* pItem, int32 row)
    {
        delete pItem;
    }

    virtual int32 GetRowHeight(int32 row, int32 columnWidth) const
    {
        IwAssert(UI, IsRowAvailable(row));

        if (m_ColumnWidth != columnWidth)
        {
            CIwVec2 itemSize =
                m_TemplateClone->Measure(CIwVec2(columnWidth, LAYOUT_INFINITY));
            m_ColumnWidth = columnWidth;
            m_RowHeight = itemSize.y;
        }

        return m_RowHeight;
    }

    virtual void Activate(bool val)
    {
        CIwUITableViewItemSource::Activate(val);

        if (val)
        {
            if (!s3eContactsAvailable())
            {
                IwError(("Contacts are not available"));
            }

            // Clone template item into table (making invisible)
            // so we can determine its size.
            m_TemplateClone = m_Template->Clone();
            m_TemplateClone->SetVisible(false);
            GetTableView()->AddChild(m_TemplateClone);

            // Attach to application
            GetApp()->SetActiveItemSource(this);

            // Cache UIDs
            UpdateAllUIDs();
        }
        else
        {
            GetApp()->SetActiveItemSource(NULL);
            m_ContactUIDs.clear();

            GetTableView()->RemoveChild(m_TemplateClone);
            delete m_TemplateClone;
            m_TemplateClone = NULL;
        }

        m_ColumnWidth = -1;
        m_RowHeight = -1;
        m_Active = val;
    }

    virtual void Clone(CIwUITableViewItemSource* pTarget) const
    {
        IW_UI_CLONE_SUPERCLASS(pTarget, CContactsItemSource, CIwUITableViewItemSource);

        CContactsItemSource* pItemSource = (CContactsItemSource*) pTarget;
        pItemSource->m_Template = m_Template->Clone();
    }

    // ContactsItemSource virtuals
    virtual int32 GetUID(int32 index)
    {
        return (0 <= index) && (index < (int32)m_ContactUIDs.size()) ?
            m_ContactUIDs[index] : -1;
    }

    virtual void CreatedUID(int32 uid)
    {
        IwAssertMsg(UI, !m_ContactUIDs.contains(uid),
            ("Already have contact for that uid"));

        // Add item at end
        int32 index = m_ContactUIDs.size();
        m_ContactUIDs.insert_slow(uid, index);

        if (m_Active)
        {
            // Tell table of new row
            GetTableView()->InsertRow(index);

            // Show new contact
            GetTableView()->SetSelection(index);
            GetTableView()->SetScrollToRow(index);
        }
    }

    virtual void UpdatedUID(int32 uid)
    {
        // Find index for UID
        int32 index = m_ContactUIDs.find(uid);

        if (index != -1)
        {
            if (m_Active)
            {
                // Tell table to update row
                GetTableView()->UpdateRow(index);
            }
        }
    }

    virtual void DeletedUID(int32 uid)
    {
        // Find index for UID
        int32 index = m_ContactUIDs.find(uid);

        if (index != -1)
        {
            m_ContactUIDs.erase(index);

            if (m_Active)
            {
                // Tell table to remove row
                GetTableView()->DeleteRow(index);
            }
        }
    }

    virtual void UpdateAllUIDs()
    {
        m_ContactUIDs.clear();

        // Read all contact UIDs
        s3eContactsUpdate();
        int32 numContacts = s3eContactsGetNumRecords();
        for (int32 i=0; i<numContacts; ++i)
        {
            const int32 uid = s3eContactsGetUID(i);
            IwAssertMsg(UI, uid != -1, ("Invalid UID"));
            if (uid != -1)
            {
                m_ContactUIDs.push_back(uid);
            }
        }

        if (m_Active)
        {
            // Tell table to rebuild all items
            GetTableView()->RecreateItemsFromSource();
        }
    }

    // Member data
    bool                m_Active;
    mutable int32       m_ColumnWidth;
    mutable int32       m_RowHeight;
    CIwUIElement*       m_Template;
    CIwUIElement*       m_TemplateClone;
    CIwArray<int32>     m_ContactUIDs;
};

IW_MANAGED_IMPLEMENT_FACTORY(CContactsItemSource);
