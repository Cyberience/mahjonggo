/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

// Includes
#include "IwTwitterSearchGUI.h"

#include "IwGx.h"
#include "IwGxFont.h"
#include "IwUI.h"
#include "s3eKeyboard.h"
#include "s3eOSExec.h"
#include "s3eOSReadString.h"
#include "IwHTTPQueue.h"
#include "IwModalStack.h"
#include "IwTwitterSearch.h"
#include "IwTwitterSearchGUI.h"
#include "time.h"
#include "s3eLocation.h"
#include "Iw2D.h"

enum
{
    BIRD_LEFT_STATIC,
    BIRD_LEFT_FLY1,
    BIRD_LEFT_FLY2,
    BIRD_RIGHT_STATIC,
    BIRD_RIGHT_FLY1,
    BIRD_RIGHT_FLY2,
    HILL,
    RADIAL_BACKGROUND,
    SUNRAY,
    TREE,
    TREEBUSH,
    TSGUI_MAX_IMAGE,
};

static const int s_TransitionTime = 750;

void CIwTwitterSearchGUI::CreateImages()
{
    static const char* img_names[] =
    {
        "Bird_staticFaceLeft64x64",
        "faceLeftFrame1",
        "faceLeftFrame2",
        "Bird_staticFaceRight64x64",
        "faceRightFrame1",
        "faceRightFrame2",
        "hill",
        "radial_background",
        "sunray",
        "Tree",
        "TreeBush",
    };

    m_Images = new CIw2DImage*[TSGUI_MAX_IMAGE];

    int i;
    for (i=0;i<TSGUI_MAX_IMAGE;++i)
    {
        m_Images[i] = Iw2DCreateImageResource(img_names[i]);
    }
}

void CIwTwitterSearchGUI::DestroyImages()
{
    int i;
    for (i=0;i<TSGUI_MAX_IMAGE;++i)
    {
        delete m_Images[i];
    }
    delete [] m_Images;
}

void CIwTwitterSearchGUI::SetUIImageTexture(CIwUIImage * pUIImage, CIwTexture * ImageTex)
{
    // Add the texture to a group so it can be freed later
    m_ImageTextures->AddRes(IW_GX_RESTYPE_TEXTURE, ImageTex);

    pUIImage->SetProperty("texture", ImageTex);
}

char* CIwTwitterSearchGUI::ThousandsString(int v)
{
    static char buf[128];
    char *c = &buf[sizeof(buf)-1];
    int i = 0;
    *c = '\0';
    do{
        if ((i%3 == 0)&&(i>0)) *--c = ',';
        *--c= '0'+v%10;
        v/=10;
        ++i;
    } while(v>0);
    return c;
}

CIwTwitterSearchGUI* g_IwTwitterSearchGUI= NULL;

void Append(char*& c, const char* m)
{
    while (*m)
    {
        *c++ = *m++;
    }
}

void StripUrls(char* msg, char* url, const char* full_string)
{
    // Replace URLs with a placeholder, while extracting them for potential use in browser

    *msg = 0;
    *url = 0;

    if (!full_string) full_string = "";

    char* c = (char*)full_string;
    char* m = msg;
    char* u = url;
    bool in_url = false;
    while (*c)
    {
        if (!in_url)
        {
            if (strncmp(c,"www",3) == 0)
            {
                c += 2;
                Append(u,"www");
                Append(m,"[link]");
                in_url=true;
            }
            else
                if (strncmp(c,"http://",7)==0)
                {
                    c += 6;
                    Append(u,"http://");
                    Append(m,"[link]");
                    in_url=true;
                }
                else
                {
                    *m++ = *c;
                }
        }
        else
        {
            if (*c == ' '||*c==':'||*c=='\n'||*c=='\t'||*c=='\r')
            {
                in_url = false;
                *m++ = *c;
                Append(u," | ");
            }
            else
            {
                *u++ = *c;
            }
        }
        ++c;
    };
    *m = '\0';
    *u = '\0';
}

void CIwTwitterSearchGUI::SetupTweet(CTweet* tweet)
{
    ClearList();

    if (tweet)
    {
        CIwUIButton* but;
        but = IwSafeCast<CIwUIButton*>(m_TweetBody);
        char msg[512];
        char msgurl[512];
        StripUrls(msg, msgurl, tweet->GetStr(CTweet::TEXT));
        but->SetCaption(msg);

        CIwUILabel* lbl = IwSafeCast<CIwUILabel*>(m_TweetName);
        lbl->SetCaption(tweet->GetStr(CTweet::FROM_USER));

        CIwUIImage* img;
        img = IwSafeCast<CIwUIImage*>(m_TweetImage);
        img->SetProperty("texture", m_DefaultTex);
        const char* url = tweet->GetStr(CTweet::PROFILE_IMAGE_URL);
        int l = strlen(url);
        if (l>3)
        {
            IwGetHTTPQueue()->GetFirst(url, (void*)img, SetImageTexture);
            IwTrace("UI",(url));
        }

        char buf[128];
        bool show_urls=false;
        sprintf(buf,"Tweets/Day: %s\n%s",ThousandsString(g_IwTwitterSearch->GetTweetsPerDay()),show_urls?msgurl:"");
        CIwUILabel* freq = IwSafeCast<CIwUILabel*>(m_TweetFreq);
        freq->SetCaption(buf);

        ShuffleBird();
    }
    else
    {
        CIwUIButton* but;
        but = IwSafeCast<CIwUIButton*>(m_TweetBody);
        but->SetCaption("\n ? ? ? \n");

        CIwUILabel* lbl = IwSafeCast<CIwUILabel*>(m_TweetName);
        lbl->SetCaption("? ? ?");

        CIwUIImage* img;
        img = IwSafeCast<CIwUIImage*>(m_TweetImage);
        img->SetProperty("texture", m_DefaultTex);

        CIwUILabel* freq = IwSafeCast<CIwUILabel*>(m_TweetFreq);
        freq->SetCaption("Tweets/Day: ?");

        ShuffleBirds(0);
    }

    if (g_IwTwitterSearch->GetSearchName())
    {
        CIwUILabel* lbl = IwSafeCast<CIwUILabel*>(m_SearchTerm);
        lbl->SetCaption(g_IwTwitterSearch->GetSearchName());
    }
}

void CIwTwitterSearchGUI::InitDelegates()
{
    IW_UI_CREATE_VIEW_SLOT1(this, "TSGUI", CIwTwitterSearchGUI, OnClickQuit, CIwUIElement*)
    IW_UI_CREATE_VIEW_SLOT1(this, "TSGUI", CIwTwitterSearchGUI, OnClickShowQuitScreen, CIwUIElement*)
    IW_UI_CREATE_VIEW_SLOT1(this, "TSGUI", CIwTwitterSearchGUI, OnClickHideQuitScreen, CIwUIElement*)

    IW_UI_CREATE_VIEW_SLOT1(this, "TSGUI", CIwTwitterSearchGUI, OnClickRefresh, CIwUIElement*)
    IW_UI_CREATE_VIEW_SLOT1(this, "TSGUI", CIwTwitterSearchGUI, OnClickGotoNavigation, CIwUIElement*)
    IW_UI_CREATE_VIEW_SLOT1(this, "TSGUI", CIwTwitterSearchGUI, OnClickNextTweet, CIwUIElement*)
}


void CIwTwitterSearchGUI::OnClickNextTweet(CIwUIElement* Clicked)
{
    // Add a new feed
    g_IwTwitterSearchGUI->SetupTweet(g_IwTwitterSearch->GetTweet());
}

void CIwTwitterSearchGUI::OnClickRefresh(CIwUIElement* Clicked)
{
    // Activate Current Search
    g_IwTwitterSearchGUI->ClickRefresh();
}

// ToDo - Combine new view into one function and read a property
void CIwTwitterSearchGUI::OnClickGotoNavigation(CIwUIElement* Clicked)
{
    g_IwTwitterSearchGUI->SetView(IW_UI_TS_NAVIGATION);
}

void CIwTwitterSearchGUI::OnClickShowQuitScreen(CIwUIElement*)
{
    // Open the quit dialog
    g_IwTwitterSearchGUI->SetView(IW_UI_TS_QUIT);
}

void CIwTwitterSearchGUI::OnClickHideQuitScreen(CIwUIElement*)
{
    // Hide the quit dialog
    g_IwTwitterSearchGUI->m_QuitScreen->SetVisible(false);
    IwModalStackSetModal(NULL);
}

void CIwTwitterSearchGUI::OnClickQuit(CIwUIElement*)
{
    // Quit the application
    static bool disableExit =
        s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WS8 ||
        s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WS81 ||
        s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WIN10;

    if (!disableExit)
    {
        s3eDeviceRequestQuit();
    }
}

class CTextInput : public CIwUITextInput
{
public:
    CTextInput()
    {

    }

protected:
    virtual void SetSoftKeyboardModal(CIwUISoftKeyboard* pSoftkeyboard)
    {
        if (pSoftkeyboard)
        {
            // Prevent the UI from rendering the dialog behind the fullscreen keyboard
            if (!pSoftkeyboard->UsingInlineEditor())
            {
                g_IwTwitterSearchGUI->m_TS->SetVisible(false);
            }
        }
        else
        {
            g_IwTwitterSearchGUI->m_TS->SetVisible(true);
        }

        IwModalStackSetModal(pSoftkeyboard);
    }

    virtual void DoTextEntry(CIwUIElement* pTextField, const char* pOriginalText)
    {
        if (pTextField == g_IwTwitterSearchGUI->m_EditText)
        {
            CIwUITextField* tf = IwSafeCast<CIwUITextField*>(g_IwTwitterSearchGUI->m_EditText);
            tf->SetProperty("textColour",CIwUIColour(IW_GX_COLOUR_BLACK));
        }

        // Clear output text
        CIwUITextInput::DoTextEntry(pTextField,"");
    }

    void FilterEventTextInput(CIwUIEventTextInput* pEvent)
    {
        switch(pEvent->GetAction())
        {
        case CIwUIEventTextInput::eAccepted:
            if (g_IwTwitterSearchGUI->m_SearchReady)
            {
                g_IwTwitterSearchGUI->SearchFor(pEvent->GetText());
                g_IwTwitterSearchGUI->m_SearchReady = false;
            }
            // Deliberately fall through to next case
        case CIwUIEventTextInput::eCancelled:
            {
                // Reset help tip.
                CIwUITextField* tf = IwSafeCast<CIwUITextField*>(g_IwTwitterSearchGUI->m_EditText);
                // textColour
                tf->SetProperty("textColour",CIwUIColour(IW_GX_COLOUR_GREY));
                tf->SetCaption("Type your search here...");
            }
            break;
        default:
            break;
        }
    }

    bool FilterEvent(CIwEvent* pEvent)
    {
        switch (pEvent->GetID())
        {
        case IWUI_EVENT_TEXT_INPUT:
            FilterEventTextInput(IwSafeCast<CIwUIEventTextInput*>(pEvent));
            break;
        }

        // Unclassified.
        return CIwUITextInput::FilterEvent(pEvent);
    }
};

void CIwTwitterSearchGUI::ClearList()
{
    // Destroy all previously uploaded images
    IwGetResManager()->DestroyGroup(m_ImageTextures);
    m_ImageTextures = new CIwResGroup;
    IwGetResManager()->AddGroup(m_ImageTextures);
}

void CIwTwitterSearchGUI::SetView(int mode)
{
    m_Mode = mode;
    if (mode == IW_UI_TS_NAVIGATION)
    {
        // Hide all other screens
        m_QuitScreen->SetVisible(false);
        m_Navigation->SetVisible(true);
        m_TweetScreen->SetVisible(true);
        m_SceneChooser->SetVisible(false);

        IwModalStackSetModal(NULL);
    }
    else if (mode == IW_UI_TS_TWEET)
    {
        // Switch to the feed view
        m_QuitScreen->SetVisible(false);
        m_Navigation->SetVisible(true);
        m_TweetScreen->SetVisible(true);
        m_SceneChooser->SetVisible(false);
    }
    else if (mode == IW_UI_TS_SCENECHOOSER)
    {
        // Switch to the feed view
        m_QuitScreen->SetVisible(false);
        m_Navigation->SetVisible(false);
        m_TweetScreen->SetVisible(false);
        m_SceneChooser->SetVisible(true);
    }
    else if (mode == IW_UI_TS_QUIT)
    {
        // Display the quit dialog
        m_QuitScreen->SetVisible(true);

        IwModalStackSetModal(m_QuitScreen);

        {
            // Play an anim on this element
            IwGetUIAnimManager()->StopAnim(m_QuitScreen);
            IwGetUIAnimManager()->PlayAnim("popupAnim", m_QuitScreen, true);
        }
    }
}

CIwSearchData::CIwSearchData()
{
    AddTestData();
    mData = this;
}
CIwSearchData::~CIwSearchData()
{
    int n = mSearchTerms.size();
    int i;
    for (i=0;i<n;++i)
    {
        delete mSearchTerms[i];
    }
}

const char* CIwSearchData::GetRowTitle(int32 row, int32 column) const
{
    return mSearchTerms[row]->GetStr(CTrend::NAME);
}
CIwUIElement* CIwSearchData::GetRowContent(int32 row,int32 column)
{
    return NULL;
}
int32 CIwSearchData::GetNumColumns() const                  {return 1;}
int32 CIwSearchData::GetNumRows(int32 column) const
{
    return mSearchTerms.size();
}
int32 CIwSearchData::GetColumnWidth(int32 column) const {return 50;}

CTrend* CIwSearchData::GetTrend(int t)
{
    // Clamp query
    if (t>=(int)mSearchTerms.size()) t = mSearchTerms.size()-1;
    if (t<0) t=0;
    return mSearchTerms[t];
}

void CIwSearchData::AddTrend(CTrend* t)
{
    mSearchTerms.append(t);
}

void CIwSearchData::AddTestData()
{
    if (s3eLocationAvailable())
    {
        mSearchTerms.append(new CTrend("Local Search 15km", NULL,   CTrend::TT_GPS));
        mSearchTerms.append(new CTrend("Local Search 50km", NULL,   CTrend::TT_GPS));
    }

    mSearchTerms.append(new CTrend("Marmalade", NULL,   CTrend::TT_STATIC));
    mSearchTerms.append(new CTrend("Software",  NULL,   CTrend::TT_STATIC));
    mSearchTerms.append(new CTrend("Coding",    NULL,   CTrend::TT_STATIC));
}
CIwSearchData* CIwSearchData::mData;

CIwSearchData* CIwSearchData::Get()
{
    return CIwSearchData::mData;
}

class CTrendButton : public CIwUIButton
{
public:
    CTrendButton(CTrend* trend):m_Trend(trend){}
    CTrend* GetTrend(){return m_Trend;}

    bool HandleEvent(CIwEvent* pEvent)
    {
        if (pEvent->GetID() == IWUI_EVENT_BUTTON)
        {
            CIwUIEventButton* pEventButton = IwSafeCast<CIwUIEventButton*>(pEvent);
            if (pEventButton->GetButton() == this)
            {
                g_IwTwitterSearch->Search(m_Trend);
                g_IwTwitterSearchGUI->SetView(IW_UI_TS_TWEET);
                g_IwTwitterSearchGUI->SetupTweet(NULL);
                return true;
            }
        }
        return CIwUIButton::HandleEvent(pEvent);
    }

private:
    CTrend* m_Trend;
};

void CIwTwitterSearchGUI::ClearDynamicTrends()
{
    CIwSearchData* data = CIwSearchData::Get();
    data->ClearDynamic();
}

CIwUILayout* CIwTwitterSearchGUI::PreparePanel(CIwUILayout* pLayout, int mode)
{
    // Add a Panel
    CIwUIElement* panel = new CIwUIElement();
    CIwUILayout* layout = new CIwUILayout();
    layout->SetSizeToSpace(true);
    panel->SetLayout(layout);
    pLayout->AddElement(panel);

    // Add Background Image
    CIwUIImage* img = new CIwUIImage();
    img->SetStyle("<TextPanel>");
    layout->AddElement(img);

    // Add Vertical List
    CIwUIElement* vlist = new CIwUIElement();
    CIwUILayoutVertical* vlayout = new CIwUILayoutVertical();
    vlist->SetLayout(vlayout);
    layout->AddElement(vlist);

    const char* title = NULL;
    if (mode <= CTrend::TT_USER)
        title = "Favourites";
    else if (mode == CTrend::TT_DYNAMIC)
        title = "Popular";

    if (title)
    {
        CIwUILabel* label = new CIwUILabel;
        label->SetStyle("<TitleLabel>");
        label->SetCaption(title);
        vlayout->AddElement(label,0,IW_UI_ALIGN_LEFT,IW_UI_ALIGN_MIDDLE,CIwSVec2(8,0));
    }

    return vlayout;
}

void CIwTwitterSearchGUI::SetSearchData()
{
    //return;
    m_SearchTerms->DestroyChildren();
    CIwUILayout* pLayout = m_SearchTerms->GetLayout();

    int32 button_type = CTrend::TT_STATIC;

    {
        CIwUILayout* vlayout = NULL;

        CIwSearchData* data = CIwSearchData::Get();
        int n = data->GetNumRows(0);
        int i;
        int num_in_panel = 0;
        for (i=0;i<n;++i)
        {
            CTrend* trend = data->GetTrend(i);
            if ((!vlayout) || (button_type != trend->GetType()) )
            {
                button_type = trend->GetType();
                vlayout = PreparePanel(pLayout,button_type);
                num_in_panel = 0;
            }
            else
            {
                if (num_in_panel>0)
                {
                    CIwUIImage* lineimg = new CIwUIImage();
                    lineimg->SetStyle("<ListLine>");
                    vlayout->AddElement(lineimg,IW_UI_ALIGN_CENTRE,IW_UI_ALIGN_MIDDLE,CIwSVec2(8,0));
                }
            }

            ++num_in_panel;
            CIwUIButton* element = new CTrendButton(trend);
            element->SetStyle("<ListButton>");
            const char* txt = data->GetRowTitle(i,0);
            element->SetCaption(txt);
            CIwUILayout* buttonlayout = new CIwUILayout();
            element->SetLayout(buttonlayout);
            CIwUIImage* buttonimg = new CIwUIImage();

            // Choose an icon for the button
            if (button_type<=CTrend::TT_USER)
                buttonimg->SetStyle("<ListButtonStar>");
            else if (button_type<=CTrend::TT_GPS)
                buttonimg->SetStyle("<ListButtonGPS>");
            else
                buttonimg->SetStyle("<ListButtonPopular>");

            buttonlayout->AddElement(buttonimg,IW_UI_ALIGN_RIGHT,IW_UI_ALIGN_MIDDLE,CIwSVec2(8,0));
            vlayout->AddElement(element);
        }
    }
}

void CIwTwitterSearchGUI::BuildSearchList(CIwUIElement* pRoot)
{
    m_SearchTerms = IwSafeCast<CIwUIElement*>(pRoot->GetChildNamed("SearchTermsList"));
    SetSearchData();
}


void CIwTwitterSearchGUI::SearchFor(const char* text)
{
    CTrend* trend = new CTrend(text,NULL,CTrend::TT_USER);
    CIwSearchData::Get()->AddTrend(trend);
    SetSearchData();

    // Initiate Query
    g_IwTwitterSearch->SearchTwitter( trend );

    SetView(IW_UI_TS_TWEET);
    SetupTweet(NULL);
}

void CIwTwitterSearchGUI::ClickRefresh()
{
    if (m_Mode == IW_UI_TS_NAVIGATION)
    {
        g_IwTwitterSearch->FetchTrendingTopics();
    }
    else
    if (m_Mode == IW_UI_TS_TWEET)
    {
        ScareBirds();
        g_IwTwitterSearch->RepeatSearch();
    }
}

void CIwTwitterSearchGUI::Init()
{
    // Instantiate the view and controller singletons.
    new CIwUIView;
    CIwUIView& view = *IwGetUIView();

    m_ViewTransition = 0;
    m_SmoothTransition = 0;
    m_Mode=0;

    new CIwUIController;

    InitDelegates();

    new CIwModalStack;

    // Load the resource group
    CIwResGroup* pResGroup = IwGetResManager()->LoadGroup("IwUITwitterSearch.group");

    CreateImages();

    CIwResource* pResource = pResGroup->GetResNamed("Twitter-Style", IW_UI_RESTYPE_STYLESHEET);
    IwGetUIStyleManager()->SetStylesheet(IwSafeCast<CIwUIStylesheet*>(pResource));

    m_DefaultTex = IwSafeCast<CIwTexture*>(pResGroup->GetResNamed("user_picture_mask", IW_GX_RESTYPE_TEXTURE));

    // Create the main UI
    m_TS = CIwUIElement::CreateFromResource("TwitterSearch");
    view.AddElementToLayout(m_TS);

    // Create the quit dialog
    m_QuitScreen = CIwUIElement::CreateFromResource("Quit");
    view.AddElementToLayout(m_QuitScreen);

    // Find the Screens
    m_Navigation   = m_TS->GetChildNamed("NavigationScreen");
    m_TweetScreen  = m_TS->GetChildNamed("TweetScreen");
    m_SceneChooser = m_TS->GetChildNamed("SceneChooser");
    m_TweetBody    = m_TS->GetChildNamed("TweetBody");
    m_TweetImage   = m_TS->GetChildNamed("TweetImage");
    m_TweetName    = m_TS->GetChildNamed("TweetName");
    m_TweetFreq    = m_TS->GetChildNamed("TweetFreq");
    m_SearchTerm   = m_TS->GetChildNamed("SearchTerm");
    m_EditText     = m_TS->GetChildNamed("TextField");
    m_Background   = m_TS->GetChildNamed("Background");
    m_TopBarBackground = m_TS->GetChildNamed("BackG");

    // Grey out text
    CIwUITextField* tf = IwSafeCast<CIwUITextField*>(m_EditText);
    tf->SetProperty("textColour",CIwUIColour(IW_GX_COLOUR_GREY));

    // Init screen visibility
    SetView(IW_UI_TS_NAVIGATION);

    // Instantiate the Twitter Search class and texture group
    m_ImageTextures = new CIwResGroup;
    g_IwTwitterSearch = new CIwTwitterSearch();

    // Create the store of search data terms
    new CIwSearchData;

    // Initialise the http queue
    CIwHTTPQueue * pHTTP = new CIwHTTPQueue();
    pHTTP->SetMaxResultLen(0x100000>>2); // 0.25Mb File Size Limit

    // Create text input singleton
    new CTextInput;
    IwGetUITextInput()->CreateSoftKeyboard();

    view.Layout();
    BuildSearchList(m_TS);

    m_SearchReady = true; // Throttle repeated searches
    m_NumBirds = 0;
    m_LastActiveBird = 0;

    g_IwTwitterSearch->FetchTrendingTopics();

    m_TreePos = CIwSVec2(0,0);
    m_TreePos = CIwSVec2(100,100);
}
void CIwTwitterSearchGUI::Shutdown()
{
    IwGetResManager()->DestroyGroup(m_ImageTextures);

    delete g_IwTwitterSearch;
    delete CIwSearchData::Get();

    delete IwGetUITextInput();
    delete IwGetUIController();
    delete IwGetUIView();
    delete IwGetModalStack();
    delete IwGetHTTPQueue();

    DestroyImages();
}

static const int32 gSettleTime=60;
static const int32 gStageSize = 16000; // Device independent coords dimension
static const int32 gSettleRange = gStageSize/16;

class CBird
{
public:
    CBird()
    {
        Init();
    }
    CIwVec2 GetPos()
    {
        return (mSettle>0) ? (mSettle*mDest+(gSettleTime-mSettle)*mPos)/gSettleTime : mPos;
    };

public:
    CIwVec2 mPos;
    CIwVec2 mVel;
    CIwVec2 mDest;
    int mSettle; // If positive, settle down by frames
    bool mOnTree;
    int mFlap;

    void FindPointOnTree()
    {
        mDest = CIwTwitterSearchGUI::GetAttachPoint();
        mSettle = 0;
    }

    void Scare()
    {
        mSettle = 0;
        mDest = CIwVec2((rand()%(gStageSize*3))-gStageSize,-gStageSize/2);
        mVel = CIwVec2(0,-(rand()%64));
        mOnTree = false;
    }

    void Init()
    {
        mSettle = 0;
        mPos = CIwVec2(rand()%gStageSize,rand()%gStageSize);
        mDest = (mPos + CIwVec2(rand()%gStageSize,rand()%gStageSize))>>1;
        mVel = CIwVec2(0,rand()%32);
        mOnTree = false;
    }

    void Update()
    {
        if (mOnTree&&(rand()%1000 == 0))
        {
            FindPointOnTree();
        }

        mPos += mVel;
        CIwVec2 ofs = (mDest - mPos);
        if (ofs.x||ofs.y)
        {
            mVel += ofs.GetNormalised()>>8;
        }
        mVel.x = (mVel.x*127)/128;
        mVel.y = (mVel.y*31)/32;

        if (mSettle == 0)
        {
            if (abs(ofs.x) < gSettleRange && abs(ofs.y) < gSettleRange)
            {
                mSettle++;
            }
            mFlap = (mFlap+2)%16;
        }
        else
        {
            mSettle++;
            if (mSettle>gSettleTime)
            {
                mSettle = gSettleTime;
                mPos = mDest;
                mVel = CIwVec2(0,0);
                mFlap = -1;
            }
            else
            {
                mFlap = (mFlap+1)%16;
            }
        }
    }
};
CBird gBirds[MAX_BIRDS];

void CIwTwitterSearchGUI::ScareBirds()
{
    int i;
    for (i=0;i<MAX_BIRDS;++i)
    {
        CBird& b = gBirds[i];
        b.Scare();
    }
}

CBird& CIwTwitterSearchGUI::GetNextBird()
{
    m_LastActiveBird = (m_LastActiveBird+1)%MAX_BIRDS;
    return gBirds[m_LastActiveBird];
}

CIwSVec2 CIwTwitterSearchGUI::GetAttachPoint()
{
    static const CIwSVec2 attach_points[] = {
        CIwSVec2( 7743, 51 ),
        CIwSVec2( 8769, 666 ),
        CIwSVec2( 12820, 307 ),
        CIwSVec2( 10871, 2102 ),
        CIwSVec2( 6820, 1641 ),
        CIwSVec2( 5692, 1948 ),
        CIwSVec2( 4102, 1743 ),
        CIwSVec2( 6974, 3743 ),
        CIwSVec2( 8153, 4358 ),
        CIwSVec2( 10564, 6461 ),
        CIwSVec2( 11435, 4871 ),
        CIwSVec2( 15487, 5076 ),
        CIwSVec2( 13948, 7076 ),
        CIwSVec2( 14307, 9230 ),
        CIwSVec2( 12820, 8102 ),
        CIwSVec2( 10820, 8769 ),
        CIwSVec2( 10205, 8256 ),
        CIwSVec2( 6205, 6769 ),
        CIwSVec2( 5538, 6000 ),
        CIwSVec2( 4102, 6153 ),
        CIwSVec2( 2717, 5641 ),
        CIwSVec2( 1179, 4512 ),
        CIwSVec2( 410, 5487 ),
        CIwSVec2( 0, 7589 ),
        CIwSVec2( 871, 7692 ),
        CIwSVec2( 2000, 8769 ),
        CIwSVec2( 2717, 7487 ),
        CIwSVec2( 4102, 9230 ),
        CIwSVec2( 4615, 9435 ),
        CIwSVec2( 6256, 9128 ),
        CIwSVec2( 9435, 8717 ),
        CIwSVec2( 10256, 8256 ),
        CIwSVec2( 10820, 8769 ),
        CIwSVec2( 12871, 8205 ),
        CIwSVec2( 13435, 9128 ),
        CIwSVec2( 14512, 9230 ),
    };
    static const int num = sizeof(attach_points)/sizeof(CIwSVec2);

    return attach_points[rand()%num];
}

void CIwTwitterSearchGUI::AttractBirds(int count)
{
    int i;
    for (i=0;i<count;++i)
    {
        CBird& b = GetNextBird();

        if (b.mPos.y <0)
        {
            // Out of sight so can do a full restart.
            b.mPos = CIwVec2(rand()%gStageSize,-gStageSize/8-((rand()%512)*gStageSize)/1024);
            b.mVel = CIwVec2((rand()%1024)-(rand()%1024),-rand()%512);
        }
        else
        {
            // In sight so be more gentle
            b.mVel = CIwVec2((rand()%64)-(rand()%64),-(rand()%64));
        }
        //
        b.FindPointOnTree();

        b.mOnTree = true;
    }
}

void CIwTwitterSearchGUI::ShuffleBird()
{
    bool found = false;
    int i;
    for (i=0;i<MAX_BIRDS;++i)
    {
        CBird& b = gBirds[i];
        if (b.mOnTree)
        {
            found = true;
            b.Scare();
            break;
        }
    }

    if (found)
        AttractBirds(1);
}

void CIwTwitterSearchGUI::ShuffleBirds(int count)
{
    m_NumBirds = count;

    if (count > MAX_BIRDS)
        count = MAX_BIRDS;

    ScareBirds();
    AttractBirds(count);
}

void CIwTwitterSearchGUI::Update2D()
{
    int i;

    for (i=0;i<MAX_BIRDS;++i)
    {
        CBird& b = gBirds[i];
        b.Update();
    }
}

void CIwTwitterSearchGUI::Update(int updateTime)
{
    if (m_Mode == IW_UI_TS_NAVIGATION)
    {
        m_ViewTransition -= updateTime;
        if (m_ViewTransition<0)
            m_ViewTransition = 0;
    }
    else
    if (m_Mode == IW_UI_TS_TWEET)
    {
        m_ViewTransition += updateTime;
        if (m_ViewTransition>s_TransitionTime)
            m_ViewTransition = s_TransitionTime;
    }

    m_SmoothTransition = (IW_GEOM_ONE-IW_GEOM_COS((m_ViewTransition*IW_ANGLE_PI)/s_TransitionTime))>>1;
}

void CIwTwitterSearchGUI::Draw2D()
{
    int i;

    m_Background->SetVisible(false);//!m_TweetScreen->IsVisible());

    CIwSVec2 pos  = CIwSVec2(m_TS->GetPosAbsolute());
    CIwSVec2 size = CIwSVec2(m_TS->GetSize());

    int tweet_ofs = ((IW_GEOM_ONE-m_SmoothTransition)*size.x)>>IW_GEOM_POINT;

    CIwUIColour col(255,255,255,((IW_GEOM_ONE-m_SmoothTransition)*255)>>IW_GEOM_POINT);
    if (size.x<size.y)
        col.a=255;
    m_TopBarBackground->SetColour(col);

    // Scroll screens
    m_TweetScreen->SetTransformPos(CIwVec2(tweet_ofs,0));
    m_Navigation->SetTransformPos(CIwVec2((-m_SmoothTransition*size.x)>>IW_GEOM_POINT,0));

    // Hide screens if not visible
    m_Navigation->SetVisible(m_ViewTransition<s_TransitionTime);
    m_TweetScreen->SetVisible(m_ViewTransition>0);

    CIwMat2D mat;
    mat.SetIdentity();
    Iw2DSetTransformMatrix(mat);

    {
        Update2D();

        Iw2DSetColour(0xFCFFFFFF);

        Iw2DSetImageTransform(IW_2D_IMAGE_TRANSFORM_NONE);



        int sky_size = MAX(size.x,size.y);
        Iw2DDrawImage(m_Images[RADIAL_BACKGROUND], CIwSVec2(size.x/2-sky_size/2,size.y/2-sky_size/2),CIwSVec2(sky_size,sky_size));


        int bottom_banner_height = 130;
        size.y -= bottom_banner_height; // Allow space at bottom

        //Iw2DSetImageTransform(IW_2D_IMAGE_TRANSFORM_FLIP_X);
        //Iw2DDrawImage(m_Sky, CIwSVec2(10,90));

        int treewidth = MIN(size.x-8,size.y);
        // Store Tree Location (as a square)
        m_TreeSize = CIwSVec2(treewidth,treewidth);
        // Calculate image sizes
        int bush_offset = m_TreeSize.x/7;
        CIwSVec2 draw_bush_size(m_TreeSize.x+8,((m_TreeSize.x+8)*m_Images[TREEBUSH]->GetHeight())/m_Images[TREEBUSH]->GetWidth());
        CIwSVec2 draw_tree_size(m_TreeSize.x,(m_TreeSize.x*m_Images[TREE]->GetHeight()  )/m_Images[TREE]->GetWidth());
        // Align tree to bottom of screen
        m_TreePos = pos+CIwSVec2(size.x/2-draw_tree_size.x/2,size.y-draw_tree_size.y);
        CIwSVec2 bush_pos = pos+CIwSVec2(size.x/2-draw_bush_size.x/2,-bush_offset+size.y-draw_bush_size.y);

        // Draw Sun
        CIwSVec2 sun_pos = CIwSVec2(size.x/2,size.y);
        int sun_size = 2*((int)(1+MAX(sun_pos.y/.95,sun_pos.x*2)));
        Iw2DDrawImage(m_Images[SUNRAY],
            sun_pos - CIwSVec2(sun_size/2,
            (int)(sun_size*.95)),
            CIwSVec2(sun_size,sun_size));

        mat.SetTrans(CIwSVec2(tweet_ofs,0));
        Iw2DSetTransformMatrix(mat);

        // Draw Hill
        // Hill Midpoint on image 716
        CIwSVec2 hill_size(512,144);
        int hill_height = bottom_banner_height+2;
        int hill_width = (hill_size.x*hill_height)/hill_size.y;
        int hill_offset = size.x/2 - (716*hill_width)/512;

        Iw2DDrawImageRegion(m_Images[HILL],
            CIwSVec2(hill_offset, size.y-1), CIwSVec2(hill_width,hill_height),
            CIwSVec2::g_Zero, hill_size);

        Iw2DDrawImageRegion(m_Images[HILL],
            CIwSVec2(hill_offset+hill_width, size.y-1), CIwSVec2(hill_width,hill_height),
            hill_size, hill_size);

        if (m_TweetScreen->IsVisible())
        {
            // Draw Tree
            Iw2DDrawImage(m_Images[TREEBUSH], bush_pos, draw_bush_size);
            Iw2DDrawImage(m_Images[TREE],     m_TreePos,draw_tree_size);

    #ifdef IW_DEBUG
    //#define TRACE_ATTACH_POINTS
    #ifdef TRACE_ATTACH_POINTS
            // Helper routine - Click to Trace locations of points on tree,
            // useful to create attach point list
            if (S3E_POINTER_STATE_RELEASED == s3ePointerGetState(S3E_POINTER_BUTTON_LEFTMOUSE))
            {
                int x = ((s3ePointerGetX()-m_TreePos.x)*gStageSize)/m_TreeSize.x;
                int y = ((s3ePointerGetY()-m_TreePos.y)*gStageSize)/m_TreeSize.y;
                IwTrace(UI,("CIwSVec2( %d, %d ),",x,y));
            }
    #endif
    #endif

            for (i=0;i<MAX_BIRDS;++i)
            {
                CBird& b = gBirds[i];
                CIwVec2 pos = b.GetPos();

                int img = BIRD_RIGHT_STATIC;

                if (b.mDest.x<b.mPos.x)
                    img = BIRD_LEFT_STATIC;

                int ox = 0;

                if (b.mFlap>=0)
                {
                    if (b.mFlap<8)
                        img += 1;
                    else
                        img += 2;
                }
                else
                {
                    if (i%2)
                        img = BIRD_LEFT_STATIC;
                    else
                        img = BIRD_RIGHT_STATIC;

                    if (img/3)
                        ox = 1;
                    else
                        ox = 6;
                }

                int16 bird_size = (32*m_TreeSize.y)/312;

                Iw2DDrawImage(m_Images[img],
                    CIwSVec2(ox-(bird_size/2)    +m_TreePos.x+(int16)((pos.x*m_TreeSize.y)/gStageSize),
                               -((bird_size*3)/4)+m_TreePos.y+(int16)((pos.y*m_TreeSize.y)/gStageSize)),
                    CIwSVec2(bird_size,bird_size));
            }
        }

        Iw2DFinishDrawing();

        IwGxClear(IW_GX_DEPTH_BUFFER_F);
    }
}
