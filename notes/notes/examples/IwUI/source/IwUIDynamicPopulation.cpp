/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

//-----------------------------------------------------------------------------
// ExampleIwUIDynamicPopulation
//-----------------------------------------------------------------------------

/**
 * @page ExampleIwUIDynamicPopulation IwUI Dynamic Population Example
 * The following example demonstrates using ui files as a template to fill a
 * scrolling view with custom components.
 *
 * The main classes used to achieve this are:
 *  - CIwUIElement
 *  - CIwUILabel
 *
 * The main functions used to achieve this are:
 *  - CIwUIElement::GetChildNamed
 *  - CIwUILabal::SetCaption
 *
 * This example demonstrates using multiple cloned versions of a template element
 * to produce a scrolling list of System properties.
 *
 * Once the element is cloned, specific component elements can be found using
 * GetChildNamed. They can then be cast to the appropriate type for customisation.
 *
 * IwSafeCast will provide an assertion that the target type is the correct one.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwUIDynamicPopulationImage.png
 *
 * @include IwUIDynamicPopulation.cpp
 *
 */
#include "IwGx.h"
#include "IwUI.h"


//Add a string item
void AddStringItem(CIwUIElement* pList, CIwUIElement* pItemTemplate, const char* name, const char* value)
{
    //Instantiate the new item
    CIwUIElement* pItem = pItemTemplate->Clone();

    //Set name/value
    IwSafeCast<CIwUILabel*>(pItem->GetChildNamed("PropName"))->SetCaption(name);
    IwSafeCast<CIwUILabel*>(pItem->GetChildNamed("PropValue"))->SetCaption(value);

    //Generate a unique name for the new item
    char buf[20];
    sprintf(buf, "item_%d", pList->GetNumChildren());
    pItem->SetName(buf);

    //Add to the list and its layout
    pList->GetLayout()->AddElement(pItem);
}

//Add an int item (by converting to a string item call)
void AddIntItem(CIwUIElement* pList, CIwUIElement* pItemTemplate, const char* name, int value)
{
    char strval[20];
    sprintf(strval, "%d", value);
    AddStringItem(pList, pItemTemplate, name, strval);
}


void ExampleInit()
{
    //Initialise the IwUI module
    IwUIInit();

    //Instantiate the view and controller singletons.
    //IwUI will not instantiate these itself, since they can be subclassed to add functionality.
    new CIwUIController;
    new CIwUIView;

    //Load the hello world UI
    IwGetResManager()->LoadGroup("IwUIDynamicPopulation.group");

    //Set the default style sheet
    CIwResource* pResource = IwGetResManager()->GetResNamed("iwui", IW_UI_RESTYPE_STYLESHEET);
    IwGetUIStyleManager()->SetStylesheet(IwSafeCast<CIwUIStylesheet*>(pResource));

    //Find the main dialog template
    CIwUIElement* pDialogTemplate = (CIwUIElement*)IwGetResManager()->GetResNamed("Main", "CIwUIElement");

    //And instantiate it
    CIwUIElement* pDialog = pDialogTemplate->Clone();
    IwGetUIView()->AddElementToLayout(pDialog);

    //Get the list to populate
    CIwUIElement* pList = pDialog->GetChildNamed("PropList");
    //Get the item template
    CIwUIElement* pItemTemplate = (CIwUIElement*)IwGetResManager()->GetResNamed("PropItem", "CIwUIElement");

    //Now populate with various system properties
    AddStringItem(pList, pItemTemplate, "S3E_DEVICE_OS", s3eDeviceGetString(S3E_DEVICE_OS));
    AddStringItem(pList, pItemTemplate, "S3E_DEVICE_OS_VERSION", s3eDeviceGetString(S3E_DEVICE_OS_VERSION));
    AddStringItem(pList, pItemTemplate, "S3E_DEVICE_CLASS", s3eDeviceGetString(S3E_DEVICE_CLASS));
    AddStringItem(pList, pItemTemplate, "S3E_DEVICE_ID", s3eDeviceGetString(S3E_DEVICE_ID));
    AddStringItem(pList, pItemTemplate, "S3E_DEVICE_PHONE_NUMBER", s3eDeviceGetString(S3E_DEVICE_PHONE_NUMBER));
    AddStringItem(pList, pItemTemplate, "S3E_DEVICE_ARCHITECTURE", s3eDeviceGetString(S3E_DEVICE_ARCHITECTURE));
    AddStringItem(pList, pItemTemplate, "S3E_DEVICE_S3E_VERSION", s3eDeviceGetString(S3E_DEVICE_S3E_VERSION));

    AddIntItem(pList, pItemTemplate, "S3E_DEVICE_OS", s3eDeviceGetInt(S3E_DEVICE_OS));
    AddIntItem(pList, pItemTemplate, "S3E_DEVICE_CLASS", s3eDeviceGetInt(S3E_DEVICE_CLASS));
    AddIntItem(pList, pItemTemplate, "S3E_DEVICE_ID", s3eDeviceGetInt(S3E_DEVICE_ID));
    AddIntItem(pList, pItemTemplate, "S3E_DEVICE_LANGUAGE", s3eDeviceGetInt(S3E_DEVICE_LANGUAGE));
    AddIntItem(pList, pItemTemplate, "S3E_DEVICE_TOTAL_RAM", s3eDeviceGetInt(S3E_DEVICE_TOTAL_RAM));
    AddIntItem(pList, pItemTemplate, "S3E_DEVICE_FREE_RAM", s3eDeviceGetInt(S3E_DEVICE_FREE_RAM));
    AddIntItem(pList, pItemTemplate, "S3E_DEVICE_ARCHITECTURE", s3eDeviceGetInt(S3E_DEVICE_ARCHITECTURE));
    AddIntItem(pList, pItemTemplate, "S3E_DEVICE_S3E_VERSION", s3eDeviceGetInt(S3E_DEVICE_S3E_VERSION));
    AddIntItem(pList, pItemTemplate, "S3E_DEVICE_CHIPSET", s3eDeviceGetInt(S3E_DEVICE_CHIPSET));
    AddIntItem(pList, pItemTemplate, "S3E_DEVICE_FPU", s3eDeviceGetInt(S3E_DEVICE_FPU));
    AddIntItem(pList, pItemTemplate, "S3E_DEVICE_UNIQUE_ID", s3eDeviceGetInt(S3E_DEVICE_UNIQUE_ID));
    AddStringItem(pList, pItemTemplate, "S3E_DEVICE_IMSI", s3eDeviceGetString(S3E_DEVICE_IMSI));

    AddIntItem(pList, pItemTemplate, "S3E_SURFACE_WIDTH", s3eSurfaceGetInt(S3E_SURFACE_WIDTH));
    AddIntItem(pList, pItemTemplate, "S3E_SURFACE_HEIGHT", s3eSurfaceGetInt(S3E_SURFACE_HEIGHT));
    AddIntItem(pList, pItemTemplate, "S3E_SURFACE_PITCH", s3eSurfaceGetInt(S3E_SURFACE_PITCH));
    AddIntItem(pList, pItemTemplate, "S3E_SURFACE_PIXEL_TYPE", s3eSurfaceGetInt(S3E_SURFACE_PIXEL_TYPE));
    AddIntItem(pList, pItemTemplate, "S3E_SURFACE_DEVICE_WIDTH", s3eSurfaceGetInt(S3E_SURFACE_DEVICE_WIDTH));
    AddIntItem(pList, pItemTemplate, "S3E_SURFACE_DEVICE_HEIGHT", s3eSurfaceGetInt(S3E_SURFACE_DEVICE_HEIGHT));
    AddIntItem(pList, pItemTemplate, "S3E_SURFACE_DEVICE_PIXEL_TYPE", s3eSurfaceGetInt(S3E_SURFACE_DEVICE_PIXEL_TYPE));
    AddIntItem(pList, pItemTemplate, "S3E_SURFACE_DEVICE_PITCH", s3eSurfaceGetInt(S3E_SURFACE_DEVICE_PITCH));
    AddIntItem(pList, pItemTemplate, "S3E_SURFACE_BLIT_DIRECTION", s3eSurfaceGetInt(S3E_SURFACE_BLIT_DIRECTION));
    AddIntItem(pList, pItemTemplate, "S3E_SURFACE_DISPLAY", s3eSurfaceGetInt(S3E_SURFACE_DISPLAY));
    AddIntItem(pList, pItemTemplate, "S3E_SURFACE_NUM_DISPLAYS", s3eSurfaceGetInt(S3E_SURFACE_NUM_DISPLAYS));
    AddIntItem(pList, pItemTemplate, "S3E_SURFACE_DEVICE_BLIT_DIRECTION", s3eSurfaceGetInt(S3E_SURFACE_DEVICE_BLIT_DIRECTION));
    AddIntItem(pList, pItemTemplate, "S3E_SURFACE_DEVICE_WIDTH_QUANTISED", s3eSurfaceGetInt(S3E_SURFACE_DEVICE_WIDTH_QUANTISED));
    AddIntItem(pList, pItemTemplate, "S3E_SURFACE_DEVICE_HEIGHT_QUANTISED", s3eSurfaceGetInt(S3E_SURFACE_DEVICE_HEIGHT_QUANTISED));

    if (IwGxGetHWType() == IW_GX_HWTYPE_GL1 || IwGxGetHWType() == IW_GX_HWTYPE_GL2 )
    {
       AddStringItem(pList, pItemTemplate, "GL_VENDOR", (char*)glGetString(GL_VENDOR));
       AddStringItem(pList, pItemTemplate, "GL_RENDERER", (char*)glGetString(GL_RENDERER));
       AddStringItem(pList, pItemTemplate, "GL_VERSION", (char*)glGetString(GL_VERSION));
       AddStringItem(pList, pItemTemplate, "GL_EXTENSIONS", (char*)glGetString(GL_EXTENSIONS));
    }
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    delete IwGetUIController();
    delete IwGetUIView();

    //Terminate the IwUI module
    IwUITerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    //Update the controller (this will generate control events etc.)
    IwGetUIController()->Update();

    //Update the view (this will do animations etc.) The SDK's example framework has a fixed
    //framerate of 20fps, so we pass that duration to the update function.
    IwGetUIView()->Update(1000/20);

    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    //Render the UI
    IwGetUIView()->Render();

    //Flush IwGx
    IwGxFlush();
    //Display the rendered frame
    IwGxSwapBuffers();
}
