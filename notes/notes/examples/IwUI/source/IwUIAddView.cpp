/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */


#include "IwUIAddView.h"
#include "IwDateAndTimeItemSource.h"

CIwUIAddView* g_AddView;

CIwUIAddView::CIwUIAddView()
{
    //Create UI from resource
    m_Element = CIwUIElement::CreateFromResource("AddView");
    m_Picker = IwSafeCast<CIwUIPickerView*>(m_Element->GetChildNamed("Picker"));
}

void CIwUIAddView::EventRemove()
{
    //If the edit index is valid, remove the event
    if (g_AddView->m_EditIndex >= 0)
    {
        delete g_EventList->m_EventList[g_AddView->m_EditIndex];
        g_EventList->m_EventList.erase(g_AddView->m_EditIndex);
        g_AddView->m_EditIndex = -1;
    }
}

void CIwUIAddView::SetView(const char *text, time_t time)
{
    //Set the text, date and time on the ui elements
    CIwUITextField* pTextField = (CIwUITextField*)m_Element->GetChildNamed("TextField");
    pTextField->SetCaption(text);

    int32 day = CIwDateAndTimeItemSource::GetDaysFromTime(time);
    tm timeinfo = *localtime(&time);

    m_Picker->SetSelectedRowForColumn(0, day); // Days
    m_Picker->SetSelectedRowForColumn(1, timeinfo.tm_hour); // Hours
    m_Picker->SetSelectedRowForColumn(2, timeinfo.tm_min / 5); // Minutes
}

void CIwUIAddView::EditView(CIwEventDate & Event, int32 index)
{
    //Display the remove button
    CIwUIButton* pRemoveButton = (CIwUIButton*)m_Element->GetChildNamed("RemoveButton");
    pRemoveButton->SetVisible(true);

    //Set the EditIndex to the Event currently being edited
    m_EditIndex = index;

    //Set the ui elements to match the event
    SetView(Event.m_EventText, Event.m_Time);
}

void CIwUIAddView::NewView(time_t time)
{
    //Hide the remove button
    CIwUIButton* pRemoveButton = (CIwUIButton*)m_Element->GetChildNamed("RemoveButton");
    pRemoveButton->SetVisible(false);

    //Reset the EditIndex
    m_EditIndex = -1;

    //Set the ui elements
    SetView("New Event", time);
}

void CIwUIAddView::AddEvent()
{
    //If the EditIndex is valid, remove the original event
    if (m_EditIndex >= 0)
    {
        delete g_EventList->m_EventList[m_EditIndex];
        g_EventList->m_EventList.erase(m_EditIndex);
    }

    CIwUITextField* pTextField = (CIwUITextField*)m_Element->GetChildNamed("TextField");

    int32 day = m_Picker->GetSelectedRowForColumn(0);
    time_t time = CIwDateAndTimeItemSource::GetTimeFromDays(day);

    tm timeinfo = *localtime(&time);
    timeinfo.tm_hour = m_Picker->GetSelectedRowForColumn(1); // Hours
    timeinfo.tm_min = m_Picker->GetSelectedRowForColumn(2) * 5; // Minutes

    //Add the Event to the Eventlist
    g_EventList->AddEvent(pTextField->GetCaption(), mktime(&timeinfo));
}
