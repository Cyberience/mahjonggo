/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

#include "IwEventDate.h"

#include <stdio.h>

IW_MANAGED_IMPLEMENT(CIwEventDate);

CIwEventDate::CIwEventDate(void)
{
    m_Element = 0;
    m_EventText = 0;
}

CIwEventDate::~CIwEventDate(void)
{
    if (m_EventText)
        delete[] m_EventText;
}

void CIwEventDate::Serialise()
{
    if (IwSerialiseIsWriting())
    {
        //Write out the object state
        uint32 time = (uint32)m_Time;
        uint32 size = strlen(m_EventText) + 1;
        IwSerialiseUInt32(time);
        IwSerialiseUInt32(size);
        IwSerialiseChar(*m_EventText, size);
    }
    else
    {
        //Read in the object state
        uint32 time;
        uint32 size;
        IwSerialiseUInt32(time);
        IwSerialiseUInt32(size);
        m_Time = time;
        m_EventText = new char[size];
        IwSerialiseChar(*m_EventText, size);
    }
}
