/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

#include "IwGeonames.h"

#include "IwMap.h"
#include "IwUIButton.h"
#include "IwUIImage.h"
#include "IwUILabel.h"
#include "IwHTTPQueue.h"
#include "tinyxml.h"
#include "IwPropertyString.h"
#include "IwJPEG.h"
#include "IwUILayoutVertical.h"
#include "IwUIView.h"
#include <math.h>

// Defines
#define RADPERDEG   0.017453292519943295769236907684886

CIwGeonames::CIwGeonames(CIwMap* pOwner) :
    m_Owner(pOwner),
    m_Overlay(NULL),
    m_OverlayBox(NULL),
    m_BoxTitle(NULL),
    m_BoxSummary(NULL)
{

}

CIwGeonames::~CIwGeonames()
{
    DestroyAllOverlays();
}

void CIwGeonames::SetOverlays(CIwUIElement* pOverlay, CIwUIElement* pOverlayBox)
{
    m_Overlay = pOverlay;
    m_OverlayBox = pOverlayBox;

    m_OverlayBox->LookupChildNamed(m_BoxTitle, "Title");
    m_OverlayBox->LookupChildNamed(m_BoxSummary, "Summary");
}

void CIwGeonames::DestroyAllOverlays()
{
    // Destroy all the overlay items
    for (uint32 i=0;i<m_Items.size();i++)
    {
        DestroyOverlay(i);
    }

    m_Items.clear();
    m_Textures.Delete();

    // Cancel any previous request
    if (IwGetHTTPQueue())
        IwGetHTTPQueue()->CancelByArgument(this);
}

void CIwGeonames::DestroyOverlay(int index)
{
    // Destroy overlay at given index

    delete [] (char*)m_Items[index].Summary;

    if (m_Items[index].ImageURL)
        delete [] (char*)m_Items[index].ImageURL;

    m_Overlay->RemoveChild(m_Items[index].pButton);

    delete m_Items[index].pButton;
}

bool CIwGeonames::ClickOverlay(CIwUIElement * pClick)
{
    // Loop through the overlay items and find the button that was clicked
    for (uint32 i=0;i<m_Items.size();i++)
    {
        if (pClick == m_Items[i].pButton)
        {
            CIwPropertyString pString;

            m_Items[i].pButton->GetProperty("caption", pString);

            m_BoxTitle->SetCaption(pString.c_str());

            m_BoxSummary->SetCaption(m_Items[i].Summary);

            if (m_Items[i].ImageURL)
            {
                // Fetch the thumbnail image
                IwGetHTTPQueue()->GetFirst(m_Items[i].ImageURL, this, AddThumbnailCallback);
            }

            return true;
        }
    }
    return false;
}

void CIwGeonames::SetFocus(const CIwVec2 & scroll, const CIwVec2 & size)
{
    // Focus on buttons in the centre of the display
    int mindist = size.x  / 3;

    CIwUIButton* pFocus = NULL;

    CIwVec2 offset(scroll.x + size.x / 2, scroll.y + size.y / 2);

    // Loop through the overlay items and find the most central button
    for (uint32 i=0;i<m_Items.size();i++)
    {
        CIwVec2 position = m_Items[i].pButton->GetPos() +
            (m_Items[i].pButton->GetSize() / IW_FIXED(2)) - offset;

        int dist = position.GetLength();

        if (mindist > dist)
        {
            mindist = dist;
            pFocus = m_Items[i].pButton;
        }
    }

    m_FocusButton = pFocus;

    // Focus on the button
    if (pFocus)
    {
        IwGetUIView()->RequestFocus(pFocus);
    }
}

void CIwGeonames::AddOverlay(CIwVec2 Position, const char* Title,
    double Latitude, double Longitude, const char* Summary, const char* ImageURL)
{
    uint32 titleHash = IwHashString(Title);

    // Check for duplicate entries
    for (int i=m_Items.size()-1;i>=0;i--)
    {
        if (m_Items[i].Hash == titleHash)
            return;
    }

    // Add a button to the overlay
    CIwUIButton * pButton = new CIwUIButton;

    pButton->SetStyle("<button>");
    pButton->SetCaption(Title);
    pButton->SetSizeToContent(true);
    pButton->SetSizeMax(CIwVec2(-1,-1));
    pButton->AttachClickSlot("OnClickOverlay");

    m_Overlay->AddChild(pButton);

    pButton->SetPos(Position);

    m_Items.push_back();

    Overlay & addOverlay = m_Items.back();

    char * newSummary = new char [strlen(Summary)+1];
    char * newImageURL = 0;

    if (ImageURL)
    {
        newImageURL = new char[strlen(ImageURL)+1];
        strcpy(newImageURL, ImageURL);
    }

    strcpy(newSummary, Summary);

    addOverlay.pButton = pButton;
    addOverlay.Hash = titleHash;
    addOverlay.Latitude = Latitude;
    addOverlay.Longitude = Longitude;
    addOverlay.ImageURL = newImageURL;
    addOverlay.Summary = newSummary;

}

void CIwGeonames::RequestOverlays(double Latitude, double Longitude, int32 zoom,
    const CIwVec2& size, CIwUIImage * pImage)
{
    // Update current overlay positions
    for (int i=m_Items.size()-1;i>=0;i--)
    {
        CIwVec2 pos;
        m_Owner->GetElementPositionFromLocation(
            m_Items[i].Latitude, m_Items[i].Longitude, pos);

        if (pos.x < 0) // Overlay item is out of range
        {
            DestroyOverlay(i);
            m_Items.erase_fast(i);
        }
        else
        {
            m_Items[i].pButton->SetPos(pos);
        }
    }


    // Calculate grid coordinates
    double addLatitude = -1.5*pow(2,(double)zoom)*0.5*size.y/75000.0;
    double addLongitude = 1.5*pow(2,(double)zoom)*0.5*size.x/(75000.0 * cos(Latitude*RADPERDEG));
    char cURL[512];
    cURL[sizeof(cURL)-1] = '\0';

    snprintf(cURL,
            sizeof(cURL),
            "http://ws.geonames.org/wikipediaBoundingBox?north=%f&south=%f&east=%f&west=%f",
            Latitude - addLatitude,
            Latitude + addLatitude,
            Longitude + addLongitude,
            Longitude - addLongitude);

    IwTrace(UI, ("URL: %s\n", cURL));

    // Cancel any previous request
    IwGetHTTPQueue()->CancelByArgument(this);

    // Request new overlays
    IwGetHTTPQueue()->Get(cURL, this, ParseXMLCallback);
}

void CIwGeonames::ParseXML(const char * Result, uint32 ResultLen)
{
    // Parse the RSS data
    TiXmlDocument doc( "names.xml" );
    doc.Parse( Result, 0, TIXML_ENCODING_UTF8 );

    TiXmlElement * node = doc.RootElement();
    TiXmlNode * entry;

    // Parse Geonames entries into overlay items
    if (node)// != 0 && node->ToElement())
    {
        for (entry = node->FirstChild("entry");
        entry;
        entry = entry->NextSibling("entry") )
        {
            TiXmlNode * value, * summary, * thumburl;

            if (!entry->FirstChild("lat") || !(value = entry->FirstChild("lat")->FirstChild()))
                continue;

            double lat = atof(value->Value());

            if (!entry->FirstChild("lng") || !(value = entry->FirstChild("lng")->FirstChild()))
                continue;

            double lng = atof(value->Value());

            CIwVec2 pos;
            m_Owner->GetElementPositionFromLocation(lat, lng, pos);

            if (!entry->FirstChild("title") || !(value = entry->FirstChild("title")->FirstChild()))
                continue;

            if (!entry->FirstChild("summary") || !(summary = entry->FirstChild("summary")->FirstChild()))
                continue;

            if (!entry->FirstChild("thumbnailImg") || !(thumburl = entry->FirstChild("thumbnailImg")->FirstChild()))
                m_Owner->AddOverlay(pos, value->Value(), lat, lng, summary->Value());
            else
                m_Owner->AddOverlay(pos, value->Value(), lat, lng, summary->Value(), thumburl->Value());
        }
    }
}

void CIwGeonames::ParseXMLCallback(void * Argument, const char* URL,
    const char * Result, int32 ResultLen)
{
    ((CIwGeonames*)Argument)->ParseXML(Result, ResultLen);
}

// This is the callback for overlay thumbnails
void CIwGeonames::AddThumbnail(const char * Result, uint32 ResultLen)
{
    if (!m_OverlayBox->IsVisible())
        return;

    // Load from memory as a JPEG
    CIwTexture * ImageTex = new CIwTexture();
    JPEGTexture(Result, ResultLen, *ImageTex);
    ImageTex->Upload();

    // Add the texture to a group so it can be freed later
    m_Textures.Add(ImageTex);

    CIwUIImage* pImage = new CIwUIImage;
    pImage->SetName("Image");
    pImage->SetProperty("texture", ImageTex);
    pImage->SetSizeToContent(true);

    CIwUILayoutVertical * pLayout = IwSafeCast<CIwUILayoutVertical*>(
        m_OverlayBox->GetChildNamed("Panel")->GetLayout());
    pLayout->AddElement(pImage);
}

void CIwGeonames::AddThumbnailCallback(void * Argument, const char* URL,
    const char * Result, int32 ResultLen)
{
    ((CIwGeonames*)Argument)->AddThumbnail(Result, ResultLen);
}
