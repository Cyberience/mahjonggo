/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

//-----------------------------------------------------------------------------
// ExampleIwUIHelloWorld
//-----------------------------------------------------------------------------

/**
 * @page ExampleIwUIHelloWorld IwUI Hello World Example
 * The following example demonstrates simple load and display of a dialogue.
 *
 * The main classes used to achieve this are:
 *  - CIwUIView
 *  - CIwUIController
 *  - CIwUIElement
 *  - CIwUIStyleSheetManager
 *
 * The main functions used to achieve this are:
 *  - IwUIInit()
 *  - IwUITerminate();
 *
 * This example demonstrates the loading and instantiation of an element defined in data.
 *
 * In IwUI, elements are typically stored as a standard resource in IwResManager. It is
 * good practise to clone these resources rather than using them directly. Elements can also
 * be dynamically built from code. See the IwUIHelloWorldDynamic for an example of this usage.
 *
 * Once the element has been cloned it is added to the UI view singleton. The UI view
 * contains the root element and is responsible for maintaining the elements.
 *
 * The CIwUIController is a singleton that converts input into events. Events from the system
 * are constantly gathered and are dispatched when CIwUIController::Update is called.
 *
 * For the button to function CIwUIView::Update and CIwUIController::Update must be called.
 *
 * This example registers a single global handler (CHelloWorldHandler) to exit on button presses.
 *
 * Finally CIwUIView::Render is called to render the UI.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwUIHelloWorldImage.png
 *
 * @include IwUIHelloWorld.cpp
 *
 */
#include "IwGx.h"
#include "IwUI.h"


//Simple handler to quit application on any button press
class CHelloWorldHandler : public IIwUIEventHandler
{
    virtual bool HandleEvent(CIwEvent* pEvent)
    {
        return false;
    }

    virtual bool FilterEvent(CIwEvent* pEvent)
    {
        if (pEvent->GetID() == IWUI_EVENT_BUTTON )
        {
            static bool disableExit =
                s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WS8 ||
                s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WS81 ||
                s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WIN10;

            if (!disableExit)
            {
                s3eDeviceRequestQuit();
            }

            return true;
        }

        return false;
    }
} g_HelloWorldHandler;


void ExampleInit()
{
    //Initialise the IwUI module
    IwUIInit();

    //Instantiate the view and controller singletons.
    //IwUI will not instantiate these itself, since they can be subclassed to add functionality.
    new CIwUIController;
    new CIwUIView;

    //Provide global event handler
    IwGetUIController()->AddEventHandler(&g_HelloWorldHandler);

    //Load the hello world UI
    IwGetResManager()->LoadGroup("IwUIHelloWorld.group");

    //Set the default style sheet
    CIwResource* pResource = IwGetResManager()->GetResNamed("iwui", IW_UI_RESTYPE_STYLESHEET);
    IwGetUIStyleManager()->SetStylesheet(IwSafeCast<CIwUIStylesheet*>(pResource));

    //Find the dialog template
    CIwUIElement* pDialogTemplate = (CIwUIElement*)IwGetResManager()->GetResNamed("Vertical", "CIwUIElement");

    //And instantiate it
    CIwUIElement* pDialog = pDialogTemplate->Clone();
    IwGetUIView()->AddElement(pDialog);
    IwGetUIView()->AddElementToLayout(pDialog);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    delete IwGetUIController();
    delete IwGetUIView();

    //Terminate the IwUI module
    IwUITerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    //Update the controller (this will generate control events etc.)
    IwGetUIController()->Update();

    //Update the view (this will do animations etc.) The SDK's example framework has a fixed
    //framerate of 20fps, so we pass that duration to the update function.
    IwGetUIView()->Update(1000/20);

    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    //Render the UI
    IwGetUIView()->Render();

    //Flush IwGx
    IwGxFlush();
    //Display the rendered frame
    IwGxSwapBuffers();
}
