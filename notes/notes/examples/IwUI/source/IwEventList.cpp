/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

#include "IwEventList.h"

IW_MANAGED_IMPLEMENT(CIwEventList);

CIwEventList * g_EventList;

CIwEventList::CIwEventList(void)
{

}

CIwEventList::~CIwEventList(void)
{
    for (uint32 i=0;i<m_EventList.size();i++)
        delete m_EventList[i];

    m_EventList.clear();
}

void CIwEventList::AddEvent(const char* title, time_t time)
{
    CIwEventDate * newDate = new CIwEventDate();

    newDate->m_EventText = new char[strlen(title)+1];
    strcpy(newDate->m_EventText, title);
    newDate->m_Time = time;

    //Find correct date position by walking backwards through the list
    int i=m_EventList.size()-1;
    while (i >= 0)
    {
        if (time < m_EventList[i]->m_Time)
            i--;
        else
            break;
    }

    //Insert the event while maintaining the correct order
    m_EventList.insert_slow(newDate, i+1);
}

void CIwEventList::Serialise()
{
    if (IwSerialiseIsWriting())
    {
        //Writing EventList
        uint32 Events = m_EventList.size();

        IwSerialiseUInt32(Events);

        for (uint16 i=0;i<Events;i++)
        {
            m_EventList[i]->Serialise();
        }
    }
    else
    {
        //Reading EventList
        uint32 Events, i;

        for (i=0;i<m_EventList.size();i++)
            delete m_EventList[i];

        m_EventList.clear();

        IwSerialiseUInt32(Events);

        //Read in Events one by one
        for (i=0;i<Events;i++)
        {
            CIwEventDate * newDate = new CIwEventDate();
            newDate->Serialise();

            m_EventList.push_back(newDate);
        }
    }
}
