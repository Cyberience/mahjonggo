/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

// Includes
#include "IwTwitterSearch.h"
#include "IwUIAnimManager.h"
#include <string>
#include "s3eMemory.h"
#include <stdio.h>
#include "JSON_parser.h"
#include <assert.h>
#include "IwUIImage.h"
#include "IwJPEG.h"
#include "math.h"

#include "s3eLocation.h"
#include "IwTwitterSearchGUI.h"
#include "IwHTTPQueue.h"

// Globals
CIwTwitterSearch * g_IwTwitterSearch = NULL;

static char* g_SearchTermStr = NULL;
static int g_TweetsPerDay = 0;

static char* URLEncode(const char* text);

const char* gBaseAPIstr = "http://search.twitter.com/search.json?lang=en&rpp=30&q=";

// Logic for parsing a twitter feed...
enum TWIT_PARSE_STATE
{
    TPS_SEARCH_FOR_RESULTS,
    TPS_IN_RESULTS_ARRAY,
    TPS_READING_RESULT,
} gTwitParseState;

static int s_Level = 0;

//static const char* s_pIndention = "  ";

static int s_IsKey = 0;

static CTweet* s_Tweet = NULL;
static CTrend* s_Trend = NULL;
static int s_ReadValue; // Field to read to from the next parsed value


static const char* s_TweetStringKeys[] =
{
    "profile_image_url",
    "created_at",
    "from_user",
    "text",
};

static const char* s_TrendStringKeys[] =
{
    "name",
    "url",
    "query",
};

//Hold the UI state
int LoadSearch = -1;
int ViewSearch = -1;

bool g_Quit;

//-----------------------------------------------------------------------------

static int ProcessValue(void* ctx, int type, const JSON_value* value);
static int ProcessTrendValue(void* ctx, int type, const JSON_value* value);
char* MakeQueryFromName(const char* query);

//-----------------------------------------------------------------------------

char* CIwTwitterSearch::GetSearchName()
{
    return g_SearchTermStr;
}

int CIwTwitterSearch::GetTweetsPerDay()
{
    return g_TweetsPerDay;
}

void SetImageTexture(void * Argument, const char* URL, const char * Result, int32 ResultLen)
{
    if (!Result || ResultLen <= 0)
    {
        return;
    }

    CIwTexture * ImageTex = new CIwTexture();

    if (IsJPEG(Result,ResultLen))
    {
        // Load from memory as a JPEG
        JPEGTexture(Result, ResultLen, *ImageTex);
    }
    else
    {
        CIwImage tempimage;

        s3eFile* tempfile = s3eFileOpenFromMemory((void*)Result, ResultLen);
        tempimage.ReadFile(tempfile);
        s3eFileClose(tempfile);

        ImageTex->CopyFromImage(&tempimage);
    }

    // Check for valid texture before upload (and limit size)
    if (ImageTex->GetFormat() != CIwImage::FORMAT_UNDEFINED &&
        ImageTex->GetWidth() > 0 && ImageTex->GetWidth() <= 512 &&
        ImageTex->GetHeight() > 0 && ImageTex->GetHeight() <= 512)
    {
        ImageTex->Upload();
        g_IwTwitterSearchGUI->SetUIImageTexture((CIwUIImage*)Argument,ImageTex);
    }
    else
    {
        IwAssertMsg(UI, false, ("Failed to load texture"));
        delete ImageTex;
    }
}

void UpdateActiveTweet()
{
    g_IwTwitterSearchGUI->SetupTweet(g_IwTwitterSearch->GetTweet());
}

//-----------------------------------------------------------------------------

CTweet::CTweet()
{
    for (int i=0;i<NUM_TWEET_STRINGS;++i)
    {
        m_TweetStrings[i] = NULL;
    }
}

void SanitiseText(char* in_c)
{
    char* c = in_c;
    while (*c)
    {
        if (*c == '\r') *c = ' ';
        if (*c == '\n') *c = ' ';
        if (*c == '\t') *c = ' ';
        if (*c == ' ') *c = ' ';
        ++c;
    }

    // Replace symbol names with symbols
    // As output is always shorter than input this is safe to do in place.

    char* out_c = in_c;
    c = in_c;

    while (*c)
    {
        char replace_char = 0;
        int  skip_chars   = 0;
        if (*c == '&')
        {
            if      (strncasecmp(c,"&quot;",6) == 0){ replace_char = '"'; skip_chars = 6; }
            else if (strncasecmp(c,"&amp;", 5) == 0){ replace_char = '&'; skip_chars = 5; }
            else if (strncasecmp(c,"&lt;",  4) == 0){ replace_char = '<'; skip_chars = 4; }
            else if (strncasecmp(c,"&gt;",  4) == 0){ replace_char = '>'; skip_chars = 4; }
        }
        if (replace_char)
        {
            *out_c++ = replace_char;
            c += skip_chars;
        }
        else
        {
            *out_c++ = *c++;
        }
    }
    *out_c = '\0';
}

void CTweet::SetStr(int ts, const char* str)
{
    IwAssert("UI",ts>=0&&ts<NUM_TWEET_STRINGS);

    if (m_TweetStrings[ts])
    {
        delete [] m_TweetStrings[ts];
        m_TweetStrings[ts] = NULL;
    }

    if (str)
    {
        m_TweetStrings[ts] = new char[strlen(str)+1];
        strcpy(m_TweetStrings[ts], str);

        if (ts == TEXT)
        {
            SanitiseText(m_TweetStrings[ts]);
        }
    }
}

CTweet::~CTweet()
{
    for (int i=0;i<NUM_TWEET_STRINGS;++i)
    {
        delete [] m_TweetStrings[i];
    }
}

void CTrend::Init()
{
    for (int i=0;i<NUM_TREND_STRINGS;++i)
    {
        m_TrendStrings[i] = NULL;
    }

    mType = TT_STATIC;
}

CTrend::CTrend()
{
    Init();
}

CTrend::CTrend(const char* name, const char* query, E_TWEET_TYPE type)
{
    Init();
    SetStr(NAME, name);

    mType = type;

    if (!query)
    {
        char* autoquery = MakeQueryFromName(name);
        SetStr(QUERY,autoquery);
        delete [] autoquery;
    }
    else
    {
        SetStr(QUERY,query);
    }
}

char* ConvertSearch2API(const char* str)
{
    // Build an API query from original html path
    const char* stripc = "http://search.twitter.com/search?q=";
    if (strncmp(stripc,str,strlen(stripc)) == 0)
    {
        char* newstr = (char*)&str[strlen(stripc)];
        int baselen = strlen(gBaseAPIstr);
        int textlen = strlen(newstr);
        char* fullurl = new char[baselen + textlen + 1];
        strcpy(fullurl, gBaseAPIstr);
        strcpy(&fullurl[baselen], newstr);
        return fullurl;
    }

    // Or just copy the incoming string
    char* cp = new char[strlen(str) + 1];
    strcpy(cp, str);
    return cp;
}

void CTrend::SetStr(E_TREND_STRING ts, const char* str)
{
    IwAssert("UI",ts>=0&&ts<NUM_TREND_STRINGS);

    if (m_TrendStrings[ts])
    {
        delete [] m_TrendStrings[ts];
        m_TrendStrings[ts] = NULL;
    }

    if (str)
    {
        if (ts == QUERY)
        {
            m_TrendStrings[ts] = ConvertSearch2API(str);
        }
        else
        if (ts == QUERY_NOPATH)
        {
            m_TrendStrings[QUERY] = MakeQueryFromName(str);
        }
        else
        {
            m_TrendStrings[ts] = new char[strlen(str)+1];
            strcpy(m_TrendStrings[ts], str);
        }
    }
}

CTrend::~CTrend()
{
    for (int i=0;i<NUM_TREND_STRINGS;++i)
    {
        delete [] m_TrendStrings[i];
    }
}

int MonthFromStr(char* str)
{
    static const char* months[] =
    {
        "Jan","Feb","Mar","Apr","May","Jun",
        "Jul","Aug","Sep","Oct","Nov","Dec"
    };
    for (int i=0;i<12;++i)
    {
        if (strcasecmp(str,months[i]) == 0)
            return i;
    }
    return 0;
}

void CIwTwitterSearch::HTTPHandler(void* argument, const char* url,
    const char* result, int32 resultLen)
{
    if (argument && result && resultLen > 0)
    {
        ((CIwTwitterSearch*)argument)->_HandleResult(result);
    }
}

void CIwTwitterSearch::_HandleResult(const char *data)
{
    _ParseJSON(data);

    time_t lowest_t=(time_t)-1;
    time_t highest_t=0;

    // Compute Meta Result - ie: Tweet Frequency
    for (uint32 i=0;i<array_Tweets.size();i++)
    {
        IwTrace("UI",(array_Tweets[i]->GetStr(CTweet::CREATED_AT)));

        int day,month,year,hour,minute,second;
        int offset_sign,offset_hour,offset_minute;

        char month_str[4];
        month_str[3] = '\0';

        char sign;

        // Example Date String
        // Wed, 18 Nov 2009 21:08:09 +0000

        sscanf(array_Tweets[i]->GetStr(CTweet::CREATED_AT),
            "%*3c, %2d %3c %4d %2d:%2d:%2d %c%2d%2d",
            &day,month_str,&year,&hour,&minute,&second,
            &sign,&offset_hour,&offset_minute);

        month = MonthFromStr(month_str);
        offset_sign = (sign == '+')?1:-1;

        tm timeinfo;
        memset(&timeinfo,0,sizeof(tm));
        timeinfo.tm_year = year-1900;
        timeinfo.tm_mday = day;
        timeinfo.tm_mon  = month;
        timeinfo.tm_hour = hour   - offset_sign*offset_hour;
        timeinfo.tm_min  = minute - offset_sign*offset_minute;
        timeinfo.tm_sec  = second;
        time_t t = mktime(&timeinfo);

        if (t<lowest_t)
            lowest_t = t;
        if (t>highest_t)
            highest_t = t;
    }

    int num_birds = 0;

    if (array_Tweets.size()>0)
    {
        time_t elapsed = (highest_t-lowest_t);
        if (elapsed == 0)
            elapsed = 1;
        g_TweetsPerDay = (60*60*24*array_Tweets.size())/elapsed;

        double d = log10((double)g_TweetsPerDay);
        num_birds = (int)((d/6)*MAX_BIRDS);
        if (num_birds<1)
            num_birds = 1;
    }
    g_IwTwitterSearchGUI->ShuffleBirds(num_birds);

    UpdateActiveTweet();
}

void GenericParseJSON(const char * str, JSON_parser_callback callback);

void ParseTweets(const char * str)
{
    if (s_Tweet)
    {
        delete s_Tweet;
        s_Tweet = NULL;
    }

    GenericParseJSON(str,&ProcessValue);

    if (s_Tweet)
    {
        delete s_Tweet;
        s_Tweet = NULL;
    }
}
void ParseTrends(const char * str)
{
    if (s_Trend)
    {
        delete s_Trend;
        s_Trend = NULL;
    }

    g_IwTwitterSearchGUI->ClearDynamicTrends();

    GenericParseJSON(str,&ProcessTrendValue);

    g_IwTwitterSearchGUI->SetSearchData();

    if (s_Trend)
    {
        delete s_Trend;
        s_Trend = NULL;
    }
}

void CIwSearchData::ClearDynamic()
{
    // Extract TT_DYNAMIC trends

    CIwArray<CTrend*> old = mSearchTerms;
    mSearchTerms.clear();

    int n = old.size();
    int i;
    for (i=0;i<n;++i)
    {
        if (old[i]->GetType() == CTrend::TT_DYNAMIC)
        {
            delete old[i];
        }
        else
        {
            mSearchTerms.append(old[i]);
        }
    }
}

void CIwTwitterSearch::_ParseJSON(const char * str)
{
    // This code is a preprocess to detect the type of data
    // before running the full parser
    const char* trendkey = "\"trends\":";
    const char* resulkey = "\"results\":";
    char* res = strstr(str,resulkey);
    char* tre = strstr(str,trendkey);
    if (tre&&(!res||tre<res))
        ParseTrends(str);
    else
        ParseTweets(str);
}

void GenericParseJSON(const char * str, JSON_parser_callback callback)
{
    // Init State
    gTwitParseState = TPS_SEARCH_FOR_RESULTS;
    s_Level = 0;
    s_IsKey = 0;

    s_ReadValue = CTweet::KEY_UNKNOWN;

    // Parse JSON

    int count = 0;

    JSON_config config;

    struct JSON_parser_struct* jc = NULL;

    init_JSON_config(&config);

    config.depth                  = 20;
    config.callback               = callback;
    config.allow_comments         = 1;
    config.handle_floats_manually = 0;

    /* Important! Set locale before parser is created.*/

    jc = new_JSON_parser(&config);

    char* c = (char*)str;
    while (*c) {
        int next_char = *c;
        if (next_char <= 0) {
            break;
        }
        if (!JSON_parser_char(jc, next_char)) {
            delete_JSON_parser(jc);
            fprintf(stderr, "JSON_parser_char: syntax error, byte %d\n", count);
            return;
        }
        ++c;
    }
    if (!JSON_parser_done(jc)) {
        delete_JSON_parser(jc);
        fprintf(stderr, "JSON_parser_end: syntax error\n");
        return;
    }



    free(jc);
}

CIwTwitterSearch::CIwTwitterSearch()
{
    m_Progress = IW_FIXED(0);
    current_Tweet = 0;

    m_LastURL[0] = '\0';
    m_LastName[0] = '\0';
}

void CIwTwitterSearch::Reset()
{
    // Cancel HTTP requests and free the feed vector
    IwGetHTTPQueue()->CancelByArgument(this);

    current_Tweet = 0;
    g_TweetsPerDay = 0;
    for (uint32 i=0;i<array_Tweets.size();i++)
    {
        delete array_Tweets[i];
    }
    array_Tweets.clear();
}

void CIwTwitterSearch::AddTweet(CTweet* tweet)
{
    array_Tweets.push_back(tweet);
}

CIwTwitterSearch::~CIwTwitterSearch(void)
{
    Reset();
    delete[] g_SearchTermStr;
    g_SearchTermStr = NULL;
}

/*
static void print_indention()
{
    for (int i = 0; i < s_Level; ++i) {
        printf(s_pIndention);
    }
}
*/

static int ProcessValue(void* ctx, int type, const JSON_value* value)
{
    static int read_level = 0;
    switch(type) {
    case JSON_T_ARRAY_BEGIN:
        //if (!s_IsKey) print_indention();
        s_IsKey = 0;
        //printf("[\n");
        ++s_Level;
        break;
    case JSON_T_ARRAY_END:
        //assert(!s_IsKey);
        if (s_Level > 0) --s_Level;
        //print_indention();
        //printf("]\n");
        break;
   case JSON_T_OBJECT_BEGIN:
       //if (!s_IsKey) print_indention();
       s_IsKey = 0;
       //printf("{\n");
        ++s_Level;
        if (gTwitParseState == TPS_IN_RESULTS_ARRAY)
        {
            gTwitParseState = TPS_READING_RESULT;
            IwAssert("UI",!s_Tweet);
            s_Tweet = new CTweet;
            read_level = s_Level;
        }
        break;
    case JSON_T_OBJECT_END:
        //assert(!s_IsKey);
        if (s_Level > 0) --s_Level;
        //print_indention();
        //printf("}\n");
        if (gTwitParseState == TPS_IN_RESULTS_ARRAY)
        {
            gTwitParseState = TPS_SEARCH_FOR_RESULTS;
        }
        else
        if (s_Level == read_level-1)
        if (gTwitParseState == TPS_READING_RESULT)
        {
            g_IwTwitterSearch->AddTweet(s_Tweet);
            s_Tweet = NULL;
            gTwitParseState = TPS_IN_RESULTS_ARRAY;
        }
        break;
    case JSON_T_INTEGER:
        //if (!s_IsKey) print_indention();
        s_IsKey = 0;
        //printf("integer: "JSON_PARSER_INTEGER_SPRINTF_TOKEN"\n", value->vu.integer_value);
        break;
    case JSON_T_FLOAT:
        //if (!s_IsKey) print_indention();
        s_IsKey = 0;
        //printf("float: %f\n", value->vu.float_value); /* We wanted stringified floats */
        break;
    case JSON_T_NULL:
        //if (!s_IsKey) print_indention();
        s_IsKey = 0;
        //printf("null\n");
        break;
    case JSON_T_TRUE:
        //if (!s_IsKey) print_indention();
        s_IsKey = 0;
        //printf("true\n");
        break;
    case JSON_T_FALSE:
        //if (!s_IsKey) print_indention();
        s_IsKey = 0;
        //printf("false\n");
        break;
    case JSON_T_KEY:
        s_IsKey = 1;
        //print_indention();
        //printf("key = '%s', value = ", value->vu.str.value);
        if (gTwitParseState == TPS_SEARCH_FOR_RESULTS)
        {
            if (strcmp("results",value->vu.str.value) == 0)
            {
                gTwitParseState = TPS_IN_RESULTS_ARRAY;
            }
        }
        else
        if (gTwitParseState == TPS_READING_RESULT)
        {
            s_ReadValue = CTweet::KEY_UNKNOWN;
            for (int i=0;i<CTweet::NUM_TWEET_STRINGS;++i)
            {
                if (strcmp(s_TweetStringKeys[i],value->vu.str.value) == 0)
                {
                    s_ReadValue = (CTweet::E_TWEET_STRING)i;
                    break;
                }
            }
        }

        break;
    case JSON_T_STRING:
        //if (!s_IsKey) print_indention();
        s_IsKey = 0;
        //printf("string: '%s'\n", value->vu.str.value);
        if (gTwitParseState == TPS_READING_RESULT)
        {
            if (s_ReadValue != CTweet::KEY_UNKNOWN)
            {
                if (s_Tweet)
                {
                    s_Tweet->SetStr(s_ReadValue,value->vu.str.value);
                }
            }
        }
        break;
    default:
        break;
    }

    return 1;
}

static int ProcessTrendValue(void* ctx, int type, const JSON_value* value)
{
    // Simpler Parser to extract trend data
    switch(type)
    {
    case JSON_T_OBJECT_BEGIN:
        if (gTwitParseState == TPS_IN_RESULTS_ARRAY)
        {
            gTwitParseState = TPS_READING_RESULT;
            IwAssert("UI",!s_Trend);
            s_Trend = new CTrend;
        }
        break;
    case JSON_T_OBJECT_END:
        if (gTwitParseState == TPS_IN_RESULTS_ARRAY)
        {
            gTwitParseState = TPS_SEARCH_FOR_RESULTS;
        }
        else
        if (gTwitParseState == TPS_READING_RESULT)
        {
            s_Trend->SetType(CTrend::TT_DYNAMIC);
            CIwSearchData::Get()->AddTrend(s_Trend);
            s_Trend = NULL;
            gTwitParseState = TPS_IN_RESULTS_ARRAY;
        }
        break;
    case JSON_T_KEY:
        if (gTwitParseState == TPS_SEARCH_FOR_RESULTS)
        {
            if (strcmp("trends",value->vu.str.value) == 0)
            {
                gTwitParseState = TPS_IN_RESULTS_ARRAY;
            }
        }
        else
        if (gTwitParseState == TPS_READING_RESULT)
        {
            s_ReadValue = CTrend::KEY_UNKNOWN;
            for (int i=0;i<CTrend::NUM_TREND_STRINGS;++i)
            {
                if (strcmp(s_TrendStringKeys[i],value->vu.str.value) == 0)
                {
                    s_ReadValue = (CTrend::E_TREND_STRING)i;
                    break;
                }
            }
        }

        break;
    case JSON_T_STRING:
        if (gTwitParseState == TPS_READING_RESULT)
        {
            if (s_ReadValue != CTrend::KEY_UNKNOWN)
            {
                if (s_Trend)
                {
                    s_Trend->SetStr((CTrend::E_TREND_STRING)s_ReadValue, value->vu.str.value);
                }
            }
        }
        break;
    default:
        break;
    }

    return 1;
}

CTweet* CIwTwitterSearch::GetTweet()
{
    CTweet* tweet = NULL;
    int num_tweets = array_Tweets.size();
    if (num_tweets>0)
    {
        current_Tweet = current_Tweet%num_tweets;
        tweet = array_Tweets[current_Tweet];
        current_Tweet = (current_Tweet+1)%num_tweets;
    }
    return tweet;
}

inline bool URLSafeChar(char d)
{
    // Check to see if character can pass unchanged in URL
    return (
        (d>='a'&&d<='z')||
        (d>='A'&&d<='Z')||
        (d>='0'&&d<='9')
        );
}

static char* URLEncode(const char* text)
{
    // Turn unsafe characters into URL Encoded characters
    static const char hexdigit[] = "0123456789ABCDEF";

    // String might get 3* longer in worse case
    char* enc = new char[strlen(text)*3+1];

    const char* c = text;
    char* o = enc;
    while (*c)
    {
        if (*c == ' ')
        {
            *o++ = '+';
        }
        else
        if (!URLSafeChar(*c))
        {
            // Convert to Hex
            int v = (unsigned char)*c;
            *o++ = '%';
            *o++ = hexdigit[(v/16)%16];
            *o++ = hexdigit[ v    %16];
        }
        else
        {
            *o++ = *c;
        }

        ++c;
    }
    *o = '\0';

    return enc;
}

void CIwTwitterSearch::_RunHttpAPI(const char* url, const char* title)
{
    if (m_LastURL != url)
        strcpy(m_LastURL,url);
    if (m_LastName != title)
        strcpy(m_LastName,title);

    if (g_SearchTermStr) delete [] g_SearchTermStr;
    g_SearchTermStr = new char[strlen(title) + 1];
    strcpy(g_SearchTermStr, title);

    Reset();

    CIwURI URIAddress = CIwURI(url);

    if (URIAddress.GetProtocol() == CIwURI::HTTP)
    {
        // Fetch remote file
        IwGetHTTPQueue()->Get(URIAddress.GetAll(), this, HTTPHandler);
    }
}

void CIwTwitterSearch::RepeatSearch()
{
    _RunHttpAPI(m_LastURL,m_LastName);
}

char* MakeQueryFromName(const char* query)
{
    char* url_encoded_search = URLEncode(query);
    int baselen = strlen(gBaseAPIstr);
    int textlen = strlen(url_encoded_search);
    char* fullurl = new char[baselen + textlen + 1];
    strcpy(fullurl, gBaseAPIstr);
    strcpy(&fullurl[baselen], url_encoded_search);
    delete [] url_encoded_search;
    return fullurl;
}

void CIwTwitterSearch::Search(CTrend* trend)
{
    if (trend->GetType() == CTrend::TT_GPS)
    {
        int distance;
        if (sscanf(trend->GetStr(CTrend::NAME), "Local Search %dkm", &distance) == 1)
        {
            SearchLocalGPS(distance);
        }
        else
        {
            IwAssertMsg(UI, false, ("Failed to parse distance for local search"));
        }
    }
    else
    {
        SearchTwitter(trend);
    }
}

void CIwTwitterSearch::SearchTwitter(CTrend* trend)
{
    _RunHttpAPI(trend->GetStr(CTrend::QUERY), trend->GetStr(CTrend::NAME));
}

void CIwTwitterSearch::SearchLocalGPS(int range)
{
    s3eLocation location;
    if (s3eLocationGet(&location) != S3E_RESULT_SUCCESS)
    {
        //The default location
        location.m_Latitude = 51.511791;        location.m_Longitude = -0.191084;
    }
    char url[256];
    char name[256];

    sprintf(url,"http://search.twitter.com/search.json?lang=en&rpp=30&geocode=%f%%2C%f%%2C%dkm",
        location.m_Latitude, location.m_Longitude, range);
    sprintf(name,"Local Search %dkm", range);
    _RunHttpAPI(url,name);
}

void CIwTwitterSearch::FetchTrendingTopics()
{
    Reset();

    const char* trend_api[] = {
        "http://search.twitter.com/trends.json",
        "http://search.twitter.com/trends/current.json",
        "http://search.twitter.com/trends/daily.json",
        "http://search.twitter.com/trends/weekly.json",
    };

    CIwURI URIAddress = CIwURI(trend_api[0]);

    if (URIAddress.GetProtocol() == CIwURI::HTTP)
    {
        // Fetch remote file
        IwGetHTTPQueue()->Get(URIAddress.GetAll(), this, HTTPHandler);
    }
}
