/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

#include "IwUIMonthView.h"

CIwUIMonthView* g_MonthView;

CIwUIMonthView::CIwUIMonthView()
{
    //Create UI from resource
    m_Element = CIwUIElement::CreateFromResource("MonthView");
}

void CIwUIMonthView::SetDay(time_t time)
{
    g_Day = time;

    //Find CIwUIElement corresponding to the given day
    struct tm* loctime = localtime(&g_Day);
    loctime->tm_mday = 1;
    time_t newtime = mktime(loctime);

    int offset = (int)((g_Day - newtime) / 86400) + loctime->tm_wday;

    char buf[10];
    sprintf(buf, "Day%d%d", offset / 7, offset % 7);
    CIwUIElement* pDay = (CIwUIElement*)m_Element->GetChildNamed(buf, true, true);

    //Set focus on the day element
    if (pDay)
        IwGetUIView()->RequestFocus(pDay);
}

void CIwUIMonthView::ButtonPrev()
{
    struct tm* loctime = localtime(&g_Day);
    mktime(loctime);

    //Go back one month
    loctime->tm_mday = 1;
    loctime->tm_mon--;
    if (loctime->tm_mon < 0) {
        loctime->tm_year--;
        loctime->tm_mon = 11;
    }

    g_Day = mktime(loctime);

    Refresh();
}

void CIwUIMonthView::ButtonNext()
{
    struct tm* loctime = localtime(&g_Day);
    mktime(loctime);

    //Go forward one month
    loctime->tm_mday = 1;
    loctime->tm_mon++;
    if (loctime->tm_mon > 11) {
        loctime->tm_year++;
        loctime->tm_mon = 0;
    }

    g_Day = mktime(loctime);

    Refresh();
}

void CIwUIMonthView::DayClick(CIwUIElement & Element)
{
    int32 day = 0;

    if (Element.GetProperty("day", day, true))
    {
        g_Day = day;
    }

    Refresh();
}

void CIwUIMonthView::Refresh()
{
    //Get the Month header text
    char title[64];
    struct tm* loctime = localtime(&g_Day);
    strftime (title, 64, "%B %Y", loctime);

    //Find the UI elements
    CIwUILabel* pMonth = (CIwUILabel*)m_Element->GetChildNamed("Month");

    //Set the header text
    pMonth->SetCaption(title);

    int theMonth = loctime->tm_mon;

    loctime->tm_mday = 1;
    time_t newtime = mktime(loctime);

    //Start on a Monday
    time_t start = newtime - loctime->tm_wday * 86400;

    bool bFinished = false;

    for (int16 r=0;r<6;r++)
    for (int16 c=0;c<7;c++)
    {
        //Get the Button for the row and column
        char buf[10];
        sprintf(buf, "Day%d%d", r, c);
        CIwUIButton* pDay = (CIwUIButton*)m_Element->GetChildNamed(buf);

        loctime = localtime(&start);

        //End on the last Sunday
        if (!bFinished)
            bFinished = (loctime->tm_mon != theMonth) && g_Day < start && loctime->tm_wday == 0;

        if (bFinished)
        {
            //Hide the remaining days
            pDay->SetVisible(false);
            pDay->SetSizeMin(CIwVec2(24, 0));
            pDay->SetSizeHint(CIwVec2(500, 0));
            continue;
        }

        //Display the day
        pDay->SetSizeMin(CIwVec2(24, 24));
        pDay->SetSizeHint(CIwVec2(500, 24));
        pDay->SetVisible(true);

        //Set the day number
        sprintf(buf, "%d", loctime->tm_mday);
        pDay->SetCaption(buf);

        //Set the day property
        pDay->SetProperty("day", (int32)start);

        CIwColour tmpColour;
        if (loctime->tm_mon != theMonth)
        {
            //Grey out days from other months
            tmpColour.Set(230, 230, 230);
            pDay->SetColour(tmpColour);
        }
        else if ((start / 86400) == (g_Today / 86400))
        {
            //Colour today green
            tmpColour.Set(0, 255, 0);
            pDay->SetColour(tmpColour);
        }
        else
        {
            //Default colour
            tmpColour.Set(255, 255, 255);
            pDay->SetColour(tmpColour);
        }

        //Go forward one day
        start += 86400;
    }

    //Display todays events
    CIwUIScrollableView * pScrollable = (CIwUIScrollableView*)m_Element->GetChildNamed("Scrollable");
    CIwUILayoutVertical * pScrollLayout = (CIwUILayoutVertical*)pScrollable->GetLayout();

    //Remove any existing items
    int32 i;
    for (i=pScrollable->GetNumChildren()-1;i>=0;i--)
    {
        CIwUIElement * child = pScrollable->GetChild(i);
        pScrollable->RemoveChild(child);
        delete child;
    }

    struct tm dayHeader = { 0 };
    localtime_r(&g_Day, &dayHeader);

    //Loop through the list of events
    for (i=0;i<(int32)g_EventList->m_EventList.size();i++)
    {
        struct tm theday = { 0 };
        localtime_r(&g_EventList->m_EventList[i]->m_Time, &theday);

        //Only display todays events
        if (dayHeader.tm_yday != theday.tm_yday || dayHeader.tm_year != theday.tm_year)
            continue;

        CIwUIElement* pItem = g_ItemTemplate->Clone();

        char LabelText[100];
        strftime(LabelText, 100, "%I:%M %p\t", &theday);
        strncat(LabelText, g_EventList->m_EventList[i]->m_EventText, 100);

        //Set name/value
        CIwUIButton * Button = (CIwUIButton*)pItem->GetChildNamed("Button");
        Button->SetCaption(LabelText);

        g_EventList->m_EventList[i]->m_Element = (CIwUIElement*)Button;

        //Generate a unique name for the new item
        static uint32 num = 0;
        char buf[20];
        sprintf(buf, "item_%u", num++);
        pItem->SetName(buf);

        //Add the Event to the layout
        pScrollLayout->AddElement(pItem, IW_UI_ALIGN_LEFT);
    }

    //Set the focus on the day
    SetDay(g_Day);
}
