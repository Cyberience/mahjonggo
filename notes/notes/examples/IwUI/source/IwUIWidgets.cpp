/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

//-----------------------------------------------------------------------------
// ExampleIwUIWidgets
//-----------------------------------------------------------------------------

/**
 * @page ExampleIwUIWidgets IwUI Widgets Example
 * The following example demonstrates some of the basic widgets on a dialog.
 *
 * The main classes used to achieve this are:
 *  - CIwUIView
 *  - CIwUIController
 *  - CIwUIElement
 *  - CIwUIButton
 *  - CIwUILabel
 *  - CIwUITabBar
 *  - CIwUITextField
 *  - CIwUIProgressBar
 *  - CIwUISlider
 *  - CIwUICheckBox
 *  - CIwUIAlertDialog
 *  - CIwUIAnimation
 *
 * The main functions used to achieve this are:
 *  - IwUIInit()
 *  - IwUITerminate();
 *  - IW_UI_CREATE_VIEW_SLOT1
 *
 * This example demonstrates the loading and instantiation of some basic widgets.
 *
 * The widgets are layed out in the .ui file, instantiated and added to CIwUIView.
 * Button clicks are handled by subclassing the CIwUIController and adding view
 * slots.
 *
 * The CIwUITextInput is used to handle text input to the text field on the dialog
 * and can be subclassed to provide functionality.
 *
 * A confirmation alert dialog is displayed when a user tries to quit the application.
 * This displays a basic animation on the buttons that is created by the animation
 * manager.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwUIWidgetsImage.png
 *
 * @include IwUIWidgets.cpp
 */

// Library includes
#include "IwGx.h"
#include "IwGxFont.h"
#include "IwUI.h"

extern bool g_IwTextureAssertPowerOf2;

bool g_Quit;
CIwUIElement* g_QuitScreen;
CIwUIElement* g_AnimQuitButton;

class CController : public CIwUIController
{
public:
    CController()
    {
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClickLaunchKeyboard, CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClickLaunchNumpad, CIwUIElement*)

        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClickShowQuitScreen, CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClickHideQuitScreen, CIwUIElement*)
        IW_UI_CREATE_VIEW_SLOT1(this, "CController", CController, OnClickQuit, CIwUIElement*)

        IW_UI_CREATE_VIEW_SLOT2(this, "CController", CController, OnRadioTextEntryMode, CIwUIElement* , int16 )
    }

private:
    void OnClickLaunchKeyboard(CIwUIElement*)
    {
        IwGetUITextInput()->SetEditorMode(CIwUITextInput::eSoftKeyboard);
        IwGetUITextInput()->DoTextEntry(NULL,"");
    }

    void OnClickLaunchNumpad(CIwUIElement*)
    {
        IwGetUITextInput()->SetEditorMode(CIwUITextInput::eSoftNumpad);
        IwGetUITextInput()->DoTextEntry(NULL,"");
    }

    void OnClickShowQuitScreen(CIwUIElement*)
    {
        g_QuitScreen->SetVisible(true);
        IwGetUIView()->SetModal(g_QuitScreen);

        IwGetUIAnimManager()->PlayAnim("quitAnim", g_AnimQuitButton, true);
    }

    void OnRadioTextEntryMode(CIwUIElement*, int16 mode)
    {
        IwGetUITextInput()->SetEditorMode((CIwUITextInput::EditorMode)mode);
    }

    void OnClickHideQuitScreen(CIwUIElement*)
    {
        g_QuitScreen->SetVisible(false);
        IwGetUIView()->SetModal(NULL);

        IwGetUIAnimManager()->StopAnim(g_AnimQuitButton, true);
    }

    void OnClickQuit(CIwUIElement*)
    {
        g_Quit = true;
    }
};

int main()
{
    g_IwTextureAssertPowerOf2 = false;
    g_Quit = false;

    // Initialize IwUI
    IwUIInit();

    {
        CIwUIView view;
        CController controller;

        CIwResGroup* pResGroup = IwGetResManager()->LoadGroup("IwUIWidgets.group");

        {
            CIwResource* pResource = IwGetResManager()->GetResNamed("iwui", IW_UI_RESTYPE_STYLESHEET);
            IwGetUIStyleManager()->SetStylesheet(IwSafeCast<CIwUIStylesheet*>(pResource));
        }

        CIwUIElement* pWidgets = CIwUIElement::CreateFromResource("Widgets");
        view.AddElement(pWidgets);
        view.AddElementToLayout(pWidgets);

        CIwUIElement* pQuitScreen = CIwUIElement::CreateFromResource("quit_screen");
        CIwUIElement* pAnimElement = pQuitScreen->GetChildNamed("alert_dialog");

        g_QuitScreen = pQuitScreen;
        g_AnimQuitButton = pAnimElement;

        pQuitScreen->SetVisible(false);
        view.AddElement(pQuitScreen);
        view.AddElementToLayout(pQuitScreen);

        CIwUITextInput textinput;
        textinput.CreateSoftKeyboard();

        // Ensure layout is valid prior to setting focus element
        view.Layout();

        // Pick an element to have focus by default
        CIwUIElement* pFocusElement = pWidgets->GetChildNamed("Button-Plain");
        IwGetUIView()->RequestFocus(pFocusElement);

        // Get the progress bar
        CIwUIProgressBar* pProgress = (CIwUIProgressBar*)pWidgets->GetChildNamed("Progress");

        uint64 lastTime = s3eTimerGetMs();
        while (!g_Quit)
        {
            IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

            // Test Progress Update
            {
                static int32 progress = 0;
                progress = (progress+4)%(1<<12);
                pProgress->SetProgress(progress);
            }

            IwGetUIController()->Update();

            // Calculate an update time step.
            uint64 currentTime = s3eTimerGetMs();
            int32 updateTime = currentTime > lastTime ?
                MIN((int32)(currentTime - lastTime), 1000) : 0;
            lastTime = currentTime;

            view.Update(updateTime);
            view.Render();

            IwGxFlush();
            IwGxSwapBuffers();
            s3eDeviceYield();
            s3eKeyboardUpdate();
            s3ePointerUpdate();

            g_Quit |= (s3eDeviceCheckQuitRequest() != 0);
        }

        // Ensure quit button animation is stopped
        IwGetUIAnimManager()->StopAllAnims();

        // Destroy ui resources
        IwGetResManager()->DestroyGroup(pResGroup);
    }

    // Terminate IwUI
    IwUITerminate();
    return 0;
}
