/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

#include "IwUIDayView.h"

CIwUIDayView* g_DayView;

CIwUIDayView::CIwUIDayView()
{
    //Create UI from resource
    m_Element = CIwUIElement::CreateFromResource("DayView");
}

void CIwUIDayView::ButtonPrev()
{
    //Decrease the current day by one
    g_Day -= 86400;

    Refresh();
}

void CIwUIDayView::ButtonNext()
{
    //Increase the current day by one
    g_Day += 86400;

    Refresh();
}

void CIwUIDayView::Refresh()
{
    //Set the header text using the current day
    char title[64];
    strftime (title, 64, "%A %d %b %Y", localtime(&g_Day));
    CIwUILabel* pTitle = (CIwUILabel*)m_Element->GetChildNamed("Title");
    pTitle->SetCaption(title);

    //Select the scrollable view
    CIwUIScrollableView * pScrollable = (CIwUIScrollableView*)m_Element->GetChildNamed("Scrollable");
    CIwUILayoutVertical * pScrollLayout = (CIwUILayoutVertical*)pScrollable->GetLayout();

    //Remove any existing items
    int32 i;
    for (i=pScrollable->GetNumChildren()-1;i>=0;i--)
    {
        CIwUIElement * child = pScrollable->GetChild(i);
        pScrollable->RemoveChild(child);
        delete child;
    }

    struct tm dayHeader = { 0 };
    localtime_r(&g_Day, &dayHeader);

    //Loop through the list of events
    for (i=0;i<(int32)g_EventList->m_EventList.size();i++)
    {
        struct tm theday = { 0 };
        localtime_r(&g_EventList->m_EventList[i]->m_Time, &theday);

        //Only display todays events
        if (dayHeader.tm_yday != theday.tm_yday || dayHeader.tm_year != theday.tm_year)
            continue;

        CIwUIElement* pItem = g_ItemTemplate->Clone();

        char LabelText[100];
        strftime(LabelText, 100, "%I:%M %p\t", &theday);
        strncat(LabelText, g_EventList->m_EventList[i]->m_EventText, 100);

        //Set name/value
        CIwUIButton * Button = (CIwUIButton*)pItem->GetChildNamed("Button");
        Button->SetCaption(LabelText);

        g_EventList->m_EventList[i]->m_Element = (CIwUIElement*)Button;

        //Generate a unique name for the new item
        static uint32 num = 0;
        char buf[20];
        sprintf(buf, "item_%u", num++);
        pItem->SetName(buf);

        //Add the Event to the layout
        pScrollLayout->AddElement(pItem, IW_UI_ALIGN_LEFT);
    }
}
