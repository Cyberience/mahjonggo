/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

//-----------------------------------------------------------------------------
// ExampleIwUISignals
//-----------------------------------------------------------------------------

/**
 * @page ExampleIwUISignalsandSlots IwUI Signals and Slots Example
 * The following example demonstrates use of signals and slots to data drive functionality.
 *
 * The main classes used to achieve this are:
 *  - CIwUIElement
 *
 * The main functions used to achieve this are:
 *  - IW_UI_CREATE_HANDLER_SLOT1
 *
 * This example demonstrates the connection of signals to slots. A slot is a single action
 * that an element knows how to perform. A signal is a means of attaching an event
 * (normally one originating below it in the UI hierarchy) to a slot. Signals and slots
 * can be preferable to normal event handling because they are more explicit.
 *
 * In this case a custom handler is created (CWanderingElement) that knows how to move itself
 * in 2 axes. It 'advertises' these movements as slots.
 *
 * In data, button presses can be attached to slots of elements. For example:
 * @code
 *  CIwUIButton
 *  {
 *      OnButtonClick { OnClickLeft }
 *  }
 * @endcode
 *
 * The example instantiates a
 * control panel element that contains 4 buttons which are configured to send signals to the
 * 4 slots in the wandering handler.
 *
 * The advantages of slots and signals can be seen by contrasting this approach with a more
 * conventional event handing approach. The CWanderingHandler could override the event handling
 * method and invoke an action based on a custom element property. This would require rather more code.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwUISignalsImage.png
 *
 * @include IwUISignals.cpp
 */

// Includes
#include "IwGx.h"
#include "IwUI.h"

//The Wandering Handler class provides slots (signal handlers) to move the element
//it is attached to around.
class CWanderingHandler : public CIwUIElementEventHandler
{
public:
    IW_MANAGED_DECLARE(CWanderingHandler)

private:
    // CIwUIElementEventHandler virtuals
    virtual void Activate(bool val)
    {
        CIwUIElementEventHandler::Activate(val);

        if (val)
        {
            //Add slots. Certain elements generate
            //signals which can be attached to these slots in data.
            IW_UI_CREATE_HANDLER_SLOT1(this, CWanderingHandler, OnClickUp, CIwUIElement*);
            IW_UI_CREATE_HANDLER_SLOT1(this, CWanderingHandler, OnClickDown, CIwUIElement*);
            IW_UI_CREATE_HANDLER_SLOT1(this, CWanderingHandler, OnClickLeft, CIwUIElement*);
            IW_UI_CREATE_HANDLER_SLOT1(this, CWanderingHandler, OnClickRight, CIwUIElement*);
        }
    }

    virtual void Clone(CIwUIElementEventHandler* pTarget) const
    {
        IW_UI_CLONE_SUPERCLASS(pTarget, CWanderingHandler, CIwUIElementEventHandler)
    }

    //Move the control up.
    void OnClickUp(CIwUIElement*)
    {
        CIwUIElement* pElement = GetElement();
        pElement->SetPos(pElement->GetPos() + CIwVec2(0, -10));
    }
    //Move the control down.
    void OnClickDown(CIwUIElement*)
    {
        CIwUIElement* pElement = GetElement();
        pElement->SetPos(pElement->GetPos() + CIwVec2(0, 10));
    }
    //Move the control left.
    void OnClickLeft(CIwUIElement*)
    {
        CIwUIElement* pElement = GetElement();
        pElement->SetPos(pElement->GetPos() + CIwVec2(-10, 0));
    }
    //Move the control right.
    void OnClickRight(CIwUIElement*)
    {
        CIwUIElement* pElement = GetElement();
        pElement->SetPos(pElement->GetPos() + CIwVec2(10, 0));
    }

};
IW_MANAGED_IMPLEMENT_FACTORY(CWanderingHandler)

void ExampleInit()
{
    //Initialise the IwUI module
    IwUIInit();

    //Class factory only really required if we attached the handler in data
    IW_CLASS_REGISTER(CWanderingHandler);

    //Instantiate the view and controller singletons.
    //IwUI will not instantiate these itself, since they can be subclassed to add functionality.
    new CIwUIController;
    new CIwUIView;

    //Load the hello world UI
    IwGetResManager()->LoadGroup("IwUISignals.group");

    //Set the default style sheet
    CIwResource* pResource = IwGetResManager()->GetResNamed("iwui", IW_UI_RESTYPE_STYLESHEET);
    IwGetUIStyleManager()->SetStylesheet(IwSafeCast<CIwUIStylesheet*>(pResource));

    //Find the movement controls template
    CIwUIElement* pMovementTemplate = (CIwUIElement*)
        IwGetResManager()->GetResNamed("Vertical", "CIwUIElement");

    //And instantiate it
    for (uint16 i = 0; i < 2; i++)
    {
        // Clone the movement buttons
        CIwUIElement* pControl = pMovementTemplate->Clone();

        //Generate a unique name for each control
        char buf[20];
        sprintf(buf, "Element%d", i);
        pControl->SetName(buf);

        //Set the initial position
        pControl->SetPos(CIwVec2(i*100, 70 + i * 100));

        //Attach movement handler
        //This could alternatively be done in data.
        pControl->AddEventHandler(new CWanderingHandler);

        //Add the control to the root element. NB. It is not added to the layout
        //because it wants to dictate its own position.
        IwGetUIView()->AddElement(pControl);
    }
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    delete IwGetUIController();
    delete IwGetUIView();

    //Terminate the IwUI module
    IW_CLASS_REMOVE(CWanderingHandler);
    IwUITerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    //Update the controller (this will generate control events etc.)
    IwGetUIController()->Update();

    //Update the view (this will do animations etc.) The SDK's example framework has a fixed
    //framerate of 20fps, so we pass that duration to the update function.
    IwGetUIView()->Update(1000/20);

    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    //Render the UI
    IwGetUIView()->Render();

    //Flush IwGx
    IwGxFlush();
    //Display the rendered frame
    IwGxSwapBuffers();
}
