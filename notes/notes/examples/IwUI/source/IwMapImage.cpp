/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

#include "IwMapImage.h"

#include "IwUIImage.h"
#include "IwImage.h"
#include "IwHTTPQueue.h"

void SetImageTexture(void * Argument, const char* URL,
    const char * Result, int32 ResultLen)
{
    // Update the tile texture using the received gif data
    s3eFile* tempfile = s3eFileOpenFromMemory((void*)Result, ResultLen);

    CIwImage tempimage;
    tempimage.ReadFile(tempfile);

    s3eFileClose(tempfile);

    if (tempimage.GetFormat() == CIwImage::FORMAT_UNDEFINED)
    {
        IwAssertMsg(UI, 0, ("Failed to load texture"));
        return;
    }

    // Create new texture with image
    CIwTexture * pTexture = new CIwTexture();

    pTexture->CopyFromImage(&tempimage);
    pTexture->Upload();

    // Some fudging to pretend original image was a power-of-2
    // to avoid inaccuracy in clipping
    CIwImage& texImage = pTexture->_GetImage();
    CIwFVec2& texUVScale = pTexture->m_UVScale;

    CIwSVec2 originalImageSize((int16)texImage.GetWidth(), (int16)texImage.GetHeight());
    CIwSVec2 newImageSize(
        (int16) IW_FIXED_DIV(originalImageSize.x, static_cast<int16>(texUVScale.x)),
        (int16) IW_FIXED_DIV(originalImageSize.y, static_cast<int16>(texUVScale.y)));

    texImage.SetPitch(texImage.GetByteDepth() * newImageSize.x);
    texImage.SetWidth(newImageSize.x);
    texImage.SetHeight(newImageSize.y);
    texUVScale.x = IW_GEOM_ONE;
    texUVScale.y = IW_GEOM_ONE;

    // Set image to use new texture
    CIwUIImage* pImage = (CIwUIImage*)Argument;

    pImage->SetTexture(pTexture);
    pImage->SetProperty("uv1", originalImageSize);
}

void CIwMapImage::CancelImage(CIwUIImage* pImage)
{
    IwGetHTTPQueue()->CancelByArgument(pImage);
}

void CIwMapImage::RequestImage(double latitude, double longitude, int32 zoom,
    const CIwVec2& size, CIwUIImage *pImage)
{
    // Request a google map tile given the coordinates and zoom
    char cURL[512];
    cURL[sizeof(cURL)-1] = '\0';

    snprintf(cURL, sizeof(cURL),
        "http://maps.google.com/maps/api/staticmap?center=%f,%f&zoom=%d&size=%dx%d&sensor=false",
        latitude, longitude, 17-zoom, size.x, size.y);

    IwTrace(UI, ("URL: %s\n", cURL));

    IwGetHTTPQueue()->Get(cURL, (void*)pImage, SetImageTexture);
}
