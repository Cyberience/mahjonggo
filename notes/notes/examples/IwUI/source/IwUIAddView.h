/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */


#include "IwUI.h"
#include "IwEventList.h"

class CIwUIAddView
{
public:
    CIwUIAddView();
    CIwUIElement* m_Element;
    CIwUIPickerView* m_Picker;

    void AddEvent();

    void SetView(const char * text, time_t time);
    void EditView(CIwEventDate & Event, int32 index);
    void NewView(time_t time);
    void EventRemove();

    int32 m_EditIndex;
};

extern CIwUIAddView* g_AddView;
