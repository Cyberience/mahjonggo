/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

//-----------------------------------------------------------------------------
// ExampleIwUIDataBinding
//-----------------------------------------------------------------------------

/**
 * @page ExampleIwUIDataBinding IwUI Data Binding Example
 * The following example demonstrates data binding to a database.
 *
 * CDatabaseHandler holds a connection to a database. It provides functionality
 * to query the database. It also tell observers when changes to the database
 * are made.
 *
 * CDataBinding is derived from CIwPropertySetBinding and is used to translate
 * requests to get or set a property into database queries. It is also
 * responsible for telling the element it provides a property for when it has
 * changed due to the database being updated.
 *
 * @image html IwUIDataBindingImage.png
 *
 * @include IwUIDataBinding.cpp
 *
 */

// Library Includes
#include "IwUI.h"

// Example Includes
#include "DatabaseHandler.h"

// Externs
extern bool g_IwTextureAssertPowerOf2;

// Globals
bool g_Quit = false;
CIwArray<class CDataBinding*> g_NotifyBindings;

class CCommandHandler
{
public:
    CCommandHandler()
    {
        IW_UI_CREATE_VIEW_SLOT1(this, "CCommandHandler", CCommandHandler,
            OnClickQuit, CIwUIElement*)
    }

    ~CCommandHandler()
    {
        IW_UI_DESTROY_VIEW_SLOTS(this)
    }

private:
    void OnClickQuit(CIwUIElement*)
    {
        g_Quit = true;
    }
};

int main()
{
    g_IwTextureAssertPowerOf2 = false;
    g_Quit = false;

    // Initialize IwUI
    IwUIInit();
    IwGxSetColClear(0xf0, 0xf0, 0xf0, 0xff);

    IW_CLASS_REGISTER(CDataBinding);

    // Create singletons
    CDatabaseHandler* pDatabaseHandler = new CDatabaseHandler;
    new CIwUIView;
    new CIwUIController;
    CCommandHandler* pCommandHandler = new CCommandHandler;

    // Load database
    bool ret = pDatabaseHandler->OpenDatabaseInMemory();
    IwAssertMsg(UI, ret, ("Failed to open databse"));
    (void) ret;

    // Ensure we have table
    if (!pDatabaseHandler->TableExists("tbl"))
    {
        const char* pSQL = "create table tbl(id integer primary key, val int);";
        pDatabaseHandler->Exec(pSQL);

        // Put some data in the table
        pSQL = "insert into tbl values (1, 25);";
        pDatabaseHandler->Exec(pSQL);
    }

    // Load resources
    CIwResGroup* pResGroup = IwGetResManager()->LoadGroup("IwUIDataBinding.group");

    // Create main page
    CIwUIElement* pTable = CIwUIElement::CreateFromResource("Table");
    IwGetUIView()->AddElementToLayout(pTable);

    // Pick an element to have focus by default
    CIwUIElement* pFocusElement = pTable->GetChildNamed("Slider");
    IwGetUIView()->RequestFocus(pFocusElement);

    uint64 lastTime = s3eTimerGetMs();
    while (!g_Quit)
    {
        IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

        IwGetUIController()->Update();

        // Calculate an update time step.
        uint64 currentTime = s3eTimerGetMs();
        int32 updateTime = currentTime > lastTime ?
            MIN((int32)(currentTime - lastTime), 1000) : 0;
        lastTime = currentTime;

        // This isn't great but must be a asynchronous
        while (g_NotifyBindings.size())
        {
            CDataBinding* pBinding = g_NotifyBindings.pop_back_get();
            pBinding->NotifyElementPropertyChanged();
        }

        // Update & render
        IwGetUIView()->Update(updateTime);
        IwGetUIView()->Render();

        // Flush render queue
        IwGxFlush();
        IwGxSwapBuffers();

        // Standard device interaction
        s3eDeviceYield();
        s3eKeyboardUpdate();
        s3ePointerUpdate();

        // Check for quit request
        g_Quit |= (s3eDeviceCheckQuitRequest() != 0);
    }

    // Destroy resources
    IwGetResManager()->DestroyGroup(pResGroup);

    // Close database
    pDatabaseHandler->CloseDatabase();

    // Destroy singletons
    delete pCommandHandler;
    delete IwGetUIController();
    delete IwGetUIView();
    delete pDatabaseHandler;

    IW_CLASS_REMOVE(CDataBinding);

    // Free array
    g_NotifyBindings.clear_optimised();

    // Terminate IwUI
    IwUITerminate();
    return 0;
}
