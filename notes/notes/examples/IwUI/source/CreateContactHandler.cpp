/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

// Marmalade includes
#include "IwUIElementEventHandler.h"
#include "IwUIEvent.h"
#include "IwUITextField.h"

// Project includes
#include "IwUIContacts.h"

//-----------------------------------------------------------------------------

class CCreateContactHandler : public CIwUIElementEventHandler
{
public:
    IW_MANAGED_DECLARE(CCreateContactHandler)
    CCreateContactHandler() {}

private:
    // CIwUIElementEventHandler virtuals
    virtual void Activate(bool val)
    {
        CIwUIElementEventHandler::Activate(val);

        if (val)
        {
            IW_UI_CREATE_HANDLER_SLOT1(this, CCreateContactHandler,
                OkCreateContact, CIwUIElement*);
            IW_UI_CREATE_HANDLER_SLOT1(this, CCreateContactHandler,
                CancelCreateContact, CIwUIElement*);

            CIwUIElement* pElement = GetElement();
            pElement->LookupChildNamed(m_FirstName, "FirstValue");
            pElement->LookupChildNamed(m_LastName, "LastValue");
            pElement->LookupChildNamed(m_MobileNumber, "MobileValue");
        }
        else
        {
            IW_UI_DESTROY_HANDLER_SLOTS(this);
        }
    }

    virtual void Clone(CIwUIElementEventHandler* pTarget) const
    {
        IW_UI_CLONE_SUPERCLASS(pTarget, CCreateContactHandler, CIwUIElementEventHandler)
    }

    virtual bool HandleEvent(CIwEvent* pEvent)
    {
        if (pEvent->GetID() == IWUI_EVENT_VISIBILITY)
        {
            CIwUIEventVisibility* pEventVisibility =
                IwSafeCast<CIwUIEventVisibility*>(pEvent);

            // Is our element becoming visible?
            if ((pEventVisibility->GetElement() == GetElement()) &&
                GetElement()->IsVisible())
            {
                ContactField firstName;
                ContactField lastName;
                ContactField mobileNumber;

                GetApp()->GetInitialContact(firstName, lastName, mobileNumber);

                m_FirstName->SetCaption(firstName);
                m_LastName->SetCaption(lastName);
                m_MobileNumber->SetCaption(mobileNumber);
            }
        }

        return CIwUIElementEventHandler::HandleEvent(pEvent);
    }

    // Slot functions
    void OkCreateContact(CIwUIElement*)
    {
        const char* pFirstName = m_FirstName->GetCaption();
        const char* pLastName = m_LastName->GetCaption();
        const char* pMobileNumber = m_MobileNumber->GetCaption();

        GetApp()->CreateConfirmed(pFirstName, pLastName, pMobileNumber);
        GetApp()->HideCreateDialog();
    }

    void CancelCreateContact(CIwUIElement*)
    {
        GetApp()->HideCreateDialog();
    }

    // Member data
    CIwUITextField* m_FirstName;
    CIwUITextField* m_LastName;
    CIwUITextField* m_MobileNumber;
};

IW_MANAGED_IMPLEMENT_FACTORY(CCreateContactHandler)
