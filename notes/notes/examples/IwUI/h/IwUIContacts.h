/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

#ifndef IWUI_CONTACTS_H
#define IWUI_CONTACTS_H

// Standard includes
#include "s3eTypes.h"

// Forward declarations
class CIwUIElement;

// Typedefs
typedef char ContactField[0x100];

//-----------------------------------------------------------------------------

class IContactsItemSource
{
public:
    // Public interface
    virtual int32 GetUID(int32 index) = 0;
    virtual void CreatedUID(int32 uid) = 0;
    virtual void UpdatedUID(int32 uid) = 0;
    virtual void DeletedUID(int32 uid) = 0;
    virtual void UpdateAllUIDs() = 0;
};

//-----------------------------------------------------------------------------

class CIwUIContacts
{
public:
    CIwUIContacts();
    ~CIwUIContacts();

    // Public interface
    void Run();

    // Allow app to reference table item source
    void SetActiveItemSource(IContactsItemSource* pItemSource);

    // Reload all contacts
    void UpdateAllContacts();

    // Create / edit contact dialog
    void ShowCreateDialog(int32 editUID = -1);
    void GetInitialContact(ContactField& firstName, ContactField& lastName,
        ContactField& mobileNumber) const;
    void CreateConfirmed(const char* pFirstName, const char* pLastName,
        const char* pMobileNumber);
    void HideCreateDialog();

    // Delete contact dialog
    void ShowDeleteDialog(int32 contactIndex);
    void DeleteConfirmed();
    void HideDeleteDialog();

private:
    // Private interface
    void Init();
    void Update(int32 deltaMS);
    void Render();

    // Member data
    CIwUIElement*           m_Contacts;
    CIwUIElement*           m_CreateDialog;
    CIwUIElement*           m_DeleteDialog;

    int32                   m_DeleteUID;
    int32                   m_EditUID;
    IContactsItemSource*    m_ItemSource;
};

// Singleton accessor
CIwUIContacts* GetApp();

#endif
