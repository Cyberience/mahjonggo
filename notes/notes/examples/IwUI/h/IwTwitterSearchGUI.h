/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
#ifndef IW_TWITTER_SEARCH_GUI_H
#define IW_TWITTER_SEARCH_GUI_H

//-----------------------------------------------------------------------------
enum IwUITSView
{
    IW_UI_TS_NAVIGATION,    // The main screen
    IW_UI_TS_TWEET,         // Screen to view search results
    IW_UI_TS_SCENECHOOSER,  // Add/Edit screen
    IW_UI_TS_QUIT,          // The quit screen
};
//-----------------------------------------------------------------------------

struct CTweet;
class CIwUISpinnerGroup;
class CIwUIElement;
class CIwUILayout;
class CIwUITextField;
class CIwResGroup;
class CIwTexture;
class CIw2DImage;
class CIwUIImage;
class CBird;

#include "IwGeom.h"

class CIwTwitterSearchGUI
{
public:
    CIwTwitterSearchGUI(){Init();}
    ~CIwTwitterSearchGUI(){Shutdown();}

    void Init();
    void Shutdown();

    void Update(int updateTime);

    void ClearList();
    void SetView(int mode);
    void SetSearchData();
    void ClearDynamicTrends();

    void BuildSearchList(CIwUIElement* pRoot);
    void SearchFor(const char* text);
    void SetupTweet(CTweet* tweet);
    void ClickRefresh();

    CIwUILayout* PreparePanel(CIwUILayout* pLayout, int mode);

    void SetUIImageTexture(CIwUIImage * pUIImage, CIwTexture * ImageTex);
    void CreateImages();
    void DestroyImages();
    char* ThousandsString(int v);

    void ShuffleBirds(int count);
    void ShuffleBird();
    void ScareBirds();
    CBird& GetNextBird();
    void AttractBirds(int count);
    static CIwSVec2 GetAttachPoint();
    void Update2D();
    void Draw2D();

    void InitDelegates();

    void OnClickNextTweet(CIwUIElement* Clicked);
    void OnClickRefresh(CIwUIElement* Clicked);
    void OnClickGotoNavigation(CIwUIElement* Clicked);
    void OnClickShowQuitScreen(CIwUIElement*);
    void OnClickHideQuitScreen(CIwUIElement*);
    void OnClickQuit(CIwUIElement*);

    bool m_SearchReady; // Throttle repeated searches
    int m_NumBirds;

    int m_LastActiveBird;
    int m_Mode;

    CIwSVec2 m_TreePos;
    CIwSVec2 m_TreeSize;

    int m_ViewTransition;
    int m_SmoothTransition;

    // The Base UI Element of the Application
    CIwUIElement* m_TS;
    // Pointers to the Edit boxes
    CIwUITextField* m_TextSearchTerm;
    // The item templates

    // UI elements
    CIwUIElement *m_Navigation, *m_TweetScreen, *m_SceneChooser;
    // The quit dialog
    CIwUIElement* m_QuitScreen;

    CIwUIElement *m_TweetBody,*m_TweetImage,*m_TweetName;
    CIwUIElement *m_SearchTerm,*m_TweetFreq;
    CIwUIElement *m_SearchTerms;
    CIwUIElement *m_EditText;
    CIwUISpinnerGroup* m_SpinnerGroup;
    CIwUIElement* m_Background;
    CIwUIElement* m_TopBarBackground;

    // Resource group to hold the image textures
    CIwResGroup* m_ImageTextures;

    CIwTexture* m_DefaultTex;

    CIw2DImage** m_Images; // Collection of Images for Iw2D rendering
};

extern CIwTwitterSearchGUI* g_IwTwitterSearchGUI;

#endif
