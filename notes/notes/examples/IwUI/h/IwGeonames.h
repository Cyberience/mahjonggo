/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
#ifndef IW_GEONAMES_H
#define IW_GEONAMES_H

#include "IwManagedList.h"

// Forward declarations
class CIwUIElement;
class CIwUIButton;
class CIwUIImage;
class CIwUILabel;
class CIwMap;
class CIwVec2;

class CIwGeonames
{
public:
    CIwGeonames(CIwMap* pOwner);
    ~CIwGeonames();

    void SetOverlays(CIwUIElement* pOverlay, CIwUIElement* pOverlayBox);

    void DestroyAllOverlays();
    void RequestOverlays(double Latitude, double Longitude, int32 zoom,
        const CIwVec2& size, CIwUIImage * pImage);
    void AddOverlay(CIwVec2 Position, const char* Title, double Latitude, double Longitude,
        const char* Summary, const char* ImageURL);
    bool ClickOverlay(CIwUIElement * pClick);
    void SetFocus(const CIwVec2 & scroll, const CIwVec2 & size);

private:

    struct Overlay
    {
        CIwUIButton* pButton;
        uint32 Hash;
        double Latitude;
        double Longitude;
        const char* Summary;
        const char* ImageURL;
    };
    CIwArray<Overlay> m_Items;

    void AddThumbnail(const char * Result, uint32 ResultLen);
    static void AddThumbnailCallback(void * Argument, const char * URL,
        const char * Result, int32 ResultLen);

    void ParseXML(const char * Result, uint32 ResultLen);
    static void ParseXMLCallback(void * Argument, const char* URL,
        const char * Result, int32 ResultLen);

    CIwMap*         m_Owner;
    CIwManagedList  m_Textures;
    CIwUIElement*   m_Overlay;
    CIwUIElement*   m_OverlayBox;
    CIwUILabel*     m_BoxTitle;
    CIwUILabel*     m_BoxSummary;
    CIwUIButton*    m_FocusButton;

    void DestroyOverlay(int index);
    bool CheckTitleHash(uint32 hash);
};

#endif
