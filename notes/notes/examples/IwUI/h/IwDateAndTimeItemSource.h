/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

#ifndef IW_DATE_AND_TIME_ITEM_SOURCE_H
#define IW_DATE_AND_TIME_ITEM_SOURCE_H

// Library includes
#include "IwUIPickerViewItemSource.h"
#include "time.h"

//-----------------------------------------------------------------------------

class CIwDateAndTimeItemSource : public CIwUIPickerViewItemSource
{
public:
    IW_MANAGED_DECLARE(CIwDateAndTimeItemSource);
    virtual ~CIwDateAndTimeItemSource();

    // Public interface
    static time_t GetTimeFromDays(int32 days);
    static int32 GetDaysFromTime(time_t time);
    static int32 GetCurrentYear();

    // IwUIPickerViewItemSource virtuals
    virtual int32 GetNumColumns() const;
    virtual int32 GetNumRowsForColumn(int32 column) const;
    virtual CIwUIElement* CreateItem(int32 column, int32 row);
    virtual void ReleaseItem(CIwUIElement* pItem, int32 column, int32 row);
    virtual int32 GetColumnWidthHint(int32 column) const;
    virtual int32 GetRowHeightForColumn(int32 column, int32 columnWidth) const;
    virtual void Clone(CIwUIPickerViewItemSource* pTarget) const;

private:
    // Avoid always recreating items by caching elements
    CIwArray<CIwUIElement*> m_CachedDate;
    CIwArray<CIwUIElement*> m_CachedLabels;
};

#endif
