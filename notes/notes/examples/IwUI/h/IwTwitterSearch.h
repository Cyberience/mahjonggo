/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
#ifndef IW_TWITTER_SEARCH_H
#define IW_TWITTER_SEARCH_H

#include "s3eDebug.h"
#include "s3eDevice.h"
#include "s3eFile.h"
#include "s3eKeyboard.h"
#include "s3eTimer.h"
#include "s3eSurface.h"
#include "IwHTTP.h"
#include "IwUILabel.h"
#include "IwArray.h"
#include <string>
#include <vector>

enum{
    MAX_BIRDS = 32,
};

struct CTweet
{
public:
    CTweet();
    ~CTweet();

    enum E_TWEET_STRING{
        KEY_UNKNOWN = -1,
        PROFILE_IMAGE_URL,
        CREATED_AT,
        FROM_USER,
        TEXT,
        NUM_TWEET_STRINGS
    };

    const char* GetStr(int ts)  const { return m_TweetStrings[ts]; }
    void        SetStr(int ts, const char* str);

private:
    char* m_TweetStrings[NUM_TWEET_STRINGS];

};

struct CTrend
{
public:
    enum E_TWEET_TYPE
    {
        TT_STATIC,  // Preloaded
        TT_USER,    // Typed in
        TT_GPS,     // GPS
        TT_DYNAMIC, // Added from Trends
    };

    enum E_TREND_STRING
    {
        KEY_UNKNOWN = -1,
        NAME,
        QUERY,
        QUERY_NOPATH,
        NUM_TREND_STRINGS
    };

    CTrend(const char* name, const char* query = NULL, E_TWEET_TYPE type = TT_STATIC);
    CTrend();
    ~CTrend();

    const char* GetStr(E_TREND_STRING ts) const { return m_TrendStrings[ts]; }
    void SetStr(E_TREND_STRING ts, const char* str);

    E_TWEET_TYPE GetType() const { return mType; }
    void SetType(E_TWEET_TYPE type) { mType = type; }

private:
    char* m_TrendStrings[NUM_TREND_STRINGS];
    E_TWEET_TYPE mType;

    void Init();
};

class CIwTwitterSearch
{
public:
    CIwTwitterSearch();
    ~CIwTwitterSearch(void);

    // Clear all pending feeds
    void Reset();

    // Add a feed
    void AddTweet( CTweet* tweet );

    // Display
    CTweet* GetTweet();

    int NumTweets(){return array_Tweets.size();}

    void Search(CTrend* trend);

    void SearchTwitter(CTrend* trend);
    void SearchLocalGPS(int range);
    void FetchTrendingTopics();
    void RepeatSearch();

    char* GetSearchName();
    int   GetTweetsPerDay();

    iwfixed m_Progress;
private:
    static void HTTPHandler(void* argument, const char* url,
        const char* result, int32 resultLen);

    void _ParseJSON(const char* data);
    void _AddImage(const char* url, const char* title);
    void _HandleResult(const char* data);
    void _RunHttpAPI(const char* url, const char* title);

    // Vector holding the search terms that need to be read
    uint16 current_Tweet;
    CIwArray<CTweet*> array_Tweets;

    char m_LastURL[1024];
    char m_LastName[1024];
};

extern CIwTwitterSearch * g_IwTwitterSearch;

void AddJSONEntry(const char* title, const char* description);
void AddJSONImage(const char* title, const char* buf, int len = 0);

// Image Setup callback to handle HTTP image download
void SetImageTexture(void * Argument, const char* URL, const char * Result, int32 ResultLen);

class CIwSearchData
{
public:
    CIwSearchData();
    ~CIwSearchData();

    const char* GetRowTitle(int32 row, int32 column) const;
    CIwUIElement* GetRowContent(int32 row,int32 column);
    int32 GetNumColumns() const;
    int32 GetNumRows(int32 column) const;
    int32 GetColumnWidth(int32 column) const;
    CTrend* GetTrend(int t);
    void AddTrend(CTrend* t);

    void ClearDynamic();

    static CIwSearchData* Get();

private:
    CIwArray<CTrend*> mSearchTerms;
    void AddTestData();
    static CIwSearchData* mData;
};

#endif
