/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
/**
 * @page       page_examples   IwUI - Examples
 *
 * - @subpage  ExampleIwUIDynamicPopulation    "IwUI Dynamic Population Example"
 * - @subpage  ExampleIwUIHelloWorld           "IwUI Hello World Example"
 * - @subpage  ExampleIwUIHelloWorldDynamic    "IwUI Hello World Dynamic Example"
 * - @subpage  ExampleIwUILayouts              "IwUI Layout Example"
 * - @subpage  ExampleIwUILocalise             "IwUI Localise Example"
 * - @subpage  ExampleIwUIRSS                  "IwUI RSS Example"
 * - @subpage  ExampleIwUISignals              "IwUI Signals and Slots example"
 */
