/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
#ifndef IW_MAP_H
#define IW_MAP_H

#include "IwUIAnimManager.h"
#include "IwUIScrollableView.h"
#include "IwGeonames.h"
#include "IwMapImage.h"

class CIwMap : public CIwUIScrollableView, private IIwUIAnimatorObserver
{
public:
    IW_MANAGED_DECLARE(CIwMap);
    CIwMap();

    // Set map zoom level
    void SetZoom(int Zoom);

    // Coordinate converstions
    void GetElementPositionFromLocation(double latitude, double longitude,
        CIwVec2& pos) const;
    void GetLocationFromTilePosition(const CIwVec2& pos,
        double& latitude, double& longitude) const;

    // Overlays
    void SetOverlayBox(CIwUIElement* pOverlayBox);
    void AddOverlay(CIwVec2 Position, const char* Title,
        double Latitude, double Longitude,
        const char* Summary, const char* ImageURL = NULL);
    bool ClickOverlay(CIwUIElement * pClick);

private:
    // IwUIElement virtuals
    virtual void UpdateElement(int32 deltaMS);
    virtual void Clone(CIwUIElement* pTarget) const;
    virtual void Activate(bool val);
    virtual void Arrange(const CIwVec2& pos, const CIwVec2& size);
    virtual void OnSizeChanged();

    // IwUIScrollableView virtuals
    virtual CIwUIRect GetContentRect() const;
    virtual void OnScrollPositionChanged();

    // IwUIAnimationObserver virtuals
    virtual void NotifyProgress(CIwUIAnimator* pAnimator);
    virtual void NotifyStopped(CIwUIAnimator* pAnimator);

    // Private utils
    void RequestTile(int i, int j);
    void ReleaseTile(int i, int j);
    void ClearImages();
    void ArrangeTiles(const CIwVec2& size);
    void CenterMap();

    // Member data
    CIwGeonames     m_Geonames;
    CIwMapImage     m_MapImage;

    bool            m_RequestedGeonames;
    bool            m_ClearOnUpdate;
    int             m_Zoom;
    CIwTexture*     m_LoadingTexture;
    CIwUIElement*   m_Marker;
    CIwUIImage*     m_Tiles[3][3];
    CIwVec2         m_CentralTilePos;

    double          m_StartLatitude;
    double          m_StartLongitude;
};

#endif /*! IW_MAP_H */
