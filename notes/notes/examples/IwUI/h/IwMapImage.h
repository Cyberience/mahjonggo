/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

#ifndef IW_MAPIMAGE_H
#define IW_MAPIMAGE_H

#include "s3eTypes.h"

// Forward declarations
class CIwUIImage;
class CIwVec2;

class CIwMapImage
{
public:
    void RequestImage(double Latitude, double Longitude, int32 zoom,
        const CIwVec2& size, CIwUIImage * pImage);
    void CancelImage(CIwUIImage * pImage);
};

#endif
