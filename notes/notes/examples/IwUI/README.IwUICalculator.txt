#category: User Interface
IwUICalculator
==============

IwUICalculator makes use of the IwUI API which is used to implement skinned
user interfaces.  The example implements a simple calculator and demonstrates
how a user interface can be constructed using a UI file and how to respond to
button click events.

