/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleAndroidJNILVL Android JNI License Verification Library Example
 * The following example uses JNI to accesss the Android License Verification
 * Library.
 *
 * The following functions are used to achieve this:
 * - s3eAndroidJNIGetVM()
 * - s3eDebugPrintf()
 *
 * This example uses the s3eAndroidJNIGetVM() API to retrieve a handle to the
 * Java VM. The Android License Verification library can then be invoked to
 * determine whether the application has been purchased on the market and has
 * permission to run.
 *
 * If you wish to make it harder for pirates to remove the copy protection from
 * your application then you are free to modify the verification library. The
 * source for the library is available in the directory named "java" alongside
 * the mkb. You can build the java source into a jar file by double clicking
 * the MakeLVLibrary.mkb file.
 *
 * You should replace the BASE64_PUBLIC_KEY inside s3eAndroidLVL.cpp with the
 * public key paired to your developer account. See the overview in the S3E API
 * Documentation for more information.
 *
 * @include s3eAndroidLVL.cpp
 */

#include "ExamplesMain.h"
#include "s3eAndroidJNI.h"
#define HAVE_INTTYPES_H
#include <jni.h>
#include <stdlib.h>

const char * g_Status = "Waiting...";
const char * BASE64_PUBLIC_KEY = "ENTER YOUR PUBLIC KEY HERE";

void licenseResult(JNIEnv* env, jobject obj, int result)
{
    if (!result) // The application should be allowed to run
        g_Status = "Success";

    // Something went wrong - the application should exit
    else if (result == 1)
        g_Status = "Blocked";
    else if (result == 2)
        g_Status = "Invalid package name";
    else if (result == 3)
        g_Status = "Requested for a package that is not the current app";
    else if (result == 4)
        g_Status = "Market does not know about the package";
    else if (result == 5)
        g_Status = "Check in progress";
    else if (result == 6)
        g_Status = "Invalid public key";
    else if (result == 7)
        g_Status = "Missing manifest permission";
    else if (result == 8)
        g_Status = "Unknown error";
}

void ExampleInit()
{
    if (!s3eAndroidJNIAvailable())
    {
        g_Status = "JNI Extension is not available";
        return;
    }

    //Get a pointer to the JVM using the JNI extension
    JavaVM* jvm = (JavaVM*)s3eAndroidJNIGetVM();

    if (!jvm)
    {
        g_Status = "Unable to get JVM";
        return;
    }


    //Get the environment from the pointer
    JNIEnv* env = NULL;
    jvm->GetEnv((void**)&env, JNI_VERSION_1_6);

    //Find the MainActivity class using the environment
    jclass MainActivity = env->FindClass("com/example/android/market/licensing/MainActivity");

    if (!MainActivity)
    {
        g_Status = "Unable to find the MainActivity class";
        return;
    }

    jfieldID fid = env->GetStaticFieldID(MainActivity, "m_Activity", "Lcom/example/android/market/licensing/MainActivity;");
    if (!fid)
    {
        g_Status = "Unable to get activity field m_Activity";
        return;
    }

    jobject m_Activity = env->GetStaticObjectField(MainActivity, fid);
    if (!m_Activity)
    {
        g_Status = "Unable to get activity object m_Activity";
        return;
    }

    //Find the checkLicense method
    jmethodID checkLicense = env->GetMethodID(MainActivity, "checkLicense", "(Ljava/lang/String;)V");

    //Register the HelloNative method on the object
    static const JNINativeMethod jnm = { "licenseResult", "(I)V", (void*)&licenseResult };

    if (env->RegisterNatives(MainActivity, &jnm, 1))
    {
        g_Status = "Could not register native methods";
        return;
    }

    jstring jkey = env->NewStringUTF(BASE64_PUBLIC_KEY);
    //Call the checkLicense method on the object
    env->CallVoidMethod(m_Activity, checkLicense, jkey);

    jthrowable exc = env->ExceptionOccurred();
    if (exc)
    {
        env->ExceptionClear();
        g_Status = "Exception. Have you added your public key to s3eAndroidLVL.cpp?";
    }
    //Clean up
    env->DeleteLocalRef(jkey);

}

void ExampleTerm()
{
}

bool ExampleUpdate()
{
    return true;
}

/*
 * The following function outputs the status to screen. It uses
 * The s3eDebugPrint() function to print the string.
 */
void ExampleRender()
{
    // Print the status
    s3eDebugPrintf(50, 100, 1, "`x666666 %s", g_Status);
}
