/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleArmASM Example of using inline ARM assembler
 *
 * The following example implement the integer sum in normal C code and
 * also in ARM NEON assembler.
 *
 * @include arm_asm.cpp
 */
#include <s3e.h>
#include <stdio.h>
#include <stdlib.h>

#define ARRAY_SIZE 512

// __ARM_NEON__ will be defined by the arm compiler as long as the relevant
// mkb are set.
// For gcc it is enought to specify: arm-cpu=7 and arm-fpu=neon

S3E_BEGIN_C_DECL

#if defined I3D_ARCH_ARM && !defined __ARM_NEON__
#error "Neon not available.  Check compiler settings."
#endif

#if defined __ARM_NEON__
// Declare as noinline otherwise the function get inlined and we end up
// with duplicate symbol for loop1.
int computeSumNeon(const int a[]) __attribute__ ((noinline));

#if defined __GNUC__
int computeSumNeon(const int a[])
{
    // Computes the sum of all elements in the input array
    int res = 0;
    asm("vmov.i32 q8, #0          \n\t"  //clear our accumulator register
        "mov r3, #512             \n\t"  //Loop condition n = ARRAY_SIZE
        ".loop1:                  \n\t"
        "vld1.32 {d0, d1, d2, d3}, [%[input]]! \n\t" //load 8 elements into d0, d1, d2, d3 = q0, q1
        "pld [%[input]]           \n\t" // preload next set of elements
        "vadd.i32 q8, q0, q8      \n\t" // q8 += q0
        "vadd.i32 q8, q1, q8      \n\t" // q8 += q1
        "subs r3, r3, #8          \n\t" // n -= 8
        "bne .loop1               \n\t" // n == 0?
        "vpadd.i32 d0, d16, d17   \n\t" // d0[0] = d16[0] + d16[1], d0[1] = d17[0] + d17[1]
        "vpaddl.u32 d0, d0        \n\t" // d0[0] = d0[0] + d0[1]
        "vmov.32 %[result], d0[0] \n\t"
        : [result] "=r" (res) , [input] "+r" (a)
        :
        : "q0", "q1", "q8", "r3");
    return res;
}
#endif
#endif

//[] Allows better compiler reordering of loads over *
static int computeSum(const int inp[])
{
    int res = 0;
    for (int i = 0; i < ARRAY_SIZE; i++)
    {
        res += inp[i];
    }
    return res;
}

S3E_END_C_DECL

#define ITERATIONS 1000000

int main()
{
    s3eSurfaceClear(0, 0, 0);
    s3eDebugPrintf(20, 100, 1, "Computing results");
    s3eSurfaceShow();
    s3eDeviceYield(0);

    int* inp;
    posix_memalign((void**)&inp, 64, ARRAY_SIZE*sizeof(int)); // Align to cache line size (64bytes on a cortex A8)

    // Initialise the array with consecutive integers.
    int i;
    for (i = 0; i < ARRAY_SIZE; i++)
    {
        inp[i] = i;
    }

    int res = 0;

    // benchmark is 1 million invocation of computeSum
    int bench = 0;
    uint64 start = s3eTimerGetUST();
    for (i = 0; i < ITERATIONS; i++)
        res = computeSum(inp);
    bench = (s3eTimerGetUST() - start);

#if defined __ARM_NEON__
    // perform the same benchmark with the NEON version of the code
    int res2 = 0;
    int bench2 = 0;
    start = s3eTimerGetUST();
    for (i = 0; i < ITERATIONS; i++)
        res2 = computeSumNeon(inp);
    bench2 = (s3eTimerGetUST() - start);
#endif

    while (!s3eDeviceCheckQuitRequest())
    {
        s3eSurfaceClear(0, 0, 0);

        //Compile time detection of NEON architecture
        s3eDebugPrintf(20, 100, 1, "Non-NEON results:");
        s3eDebugPrintf(40, 120, 1, "Result: %d", res);
        s3eDebugPrintf(40, 140, 1, "Benchmark: %d", bench);

#ifdef __ARM_NEON__
        s3eDebugPrintf(20, 180, 1, "NEON Results:");
        s3eDebugPrintf(40, 200, 1, "Result: %d", res2);
        s3eDebugPrintf(40, 220, 1, "Benchmark: %d", bench2);
#else
        s3eDebugPrintf(20, 180, 1, "Example built without NEON support.");
#endif

        //TODO - make another with run time detection of NEON architecture
        //Use links on the internet

        s3eSurfaceShow();
        s3eDeviceYieldUntilEvent();
    }

    free(inp);
    return 0;
}
