/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwAudioCapture IwAudioCapture Example
 *
 * This is an example of the use of the IwAudioCapture component.
 * It opens an interface for the user to record audio.
 *
 * @include IwAudioCapture.cpp
 */

//-----------------------------------------------------------------------------

#include <string>
#include "s3eMemory.h"
#include "ExamplesMain.h"
#include "IwGx.h"
#include "IwGxPrint.h"
#include "s3eAudio.h"

#include "IwAudioCapture.h"

static const char* g_szAssets[] =
    {
     "images/img_main.png",
     "images/img_green_top_bar.png",
     "images/butt_rec_on.png",
     "images/butt_rec_off.png",
     "images/butt_stop_on.png",
     "images/butt_stop_off.png",
     "images/butt_close.png"
    };

static IwAudioCapture* g_Capture = NULL;
static bool g_RecordingActive = false;

const char* s_AudioFiles[2] = {
    "record0.wav",
    "record1.wav",
};
static int s_AudioFileN = -1;
static const char* getFileToPlay()
{
    if (s_AudioFileN < 0)
        return "";
    return s_AudioFiles[s_AudioFileN];
}
static const char* getFileToRecord()
{
    const int nextFile = (s_AudioFileN + 1) % 2;
    return s_AudioFiles[nextFile];
}
static void setNextFileToPlay()
{
    s_AudioFileN++;
    s_AudioFileN = s_AudioFileN % 2;
}


//-----------------------------------------------------------------------------
void ExampleInit()
{
    AddButton("Start Recording", 10, 10, 100, 30, s3eKey0);
    AddButton("Stop Recording", 10, 50, 100, 30, s3eKey1);
    AddButton("Start Dialog", 10, 90, 100, 30, s3eKey2);
    AddButton("Play Recording", 10, 130, 100, 30, s3eKey3);
    IwAudioCaptureInitAssets(g_szAssets);
}

//-----------------------------------------------------------------------------
void ExampleShutDown()
{
}

// From this point on, the code is general housekeeping functions
// common to other examples.

bool ExampleUpdate()
{
    if (CheckButton("Start Dialog"))
    {
        if (g_RecordingActive)
            return true;


        if (IwAudioCaptureWithDialog(getFileToRecord()) == IW_AUDIO_CAPTURE_RESULT_SUCCESS)
        {
            setNextFileToPlay();
        }
    }
    else if (CheckButton("Start Recording"))
    {
        if (g_RecordingActive)
            return true;

        g_RecordingActive = true;

        g_Capture = IwAudioCaptureToFile(getFileToRecord());
        if (g_Capture == NULL)
        {
            g_RecordingActive = false;
        }
    }
    else if (CheckButton("Stop Recording"))
    {
        if (g_Capture == NULL)
            return true;

        if (IwAudioCaptureStop(g_Capture) == S3E_RESULT_SUCCESS)
        {
            setNextFileToPlay();
        }
        g_Capture = NULL;
        g_RecordingActive = false;
    }
    else if (CheckButton("Play Recording"))
    {
        s3eAudioPlay(getFileToPlay());
    }
    return true;
}

//-----------------------------------------------------------------------------
void ExampleRender()
{
    IwGxFlush();

    char buffer[256];

    sprintf(buffer, "Audio File: %s", getFileToPlay());
    IwGxPrintString(10, 180, buffer, true);

    if (g_Capture == NULL)
    {
        IwGxPrintString(10, 200, "Record not in progress", true);
    }
    else
    {
        switch (IwAudioCaptureStatus(g_Capture))
        {
            case IW_AUDIO_CAPTURE_RESULT_RUNNING:
                IwGxPrintString(10, 200, "Recording...", true);
                break;
            case IW_AUDIO_CAPTURE_RESULT_SUCCESS:
                IwGxPrintString(10, 200, "Recording done", true);
                break;
            case IW_AUDIO_CAPTURE_RESULT_CANCELLED:
                IwGxPrintString(10, 200, "Recording Cancelled", true);
                break;
            case IW_AUDIO_CAPTURE_RESULT_FINISHED:
                IwGxPrintString(10, 200, "Recording Finished", true);
                break;
            default:
                IwGxPrintString(10, 200, "Recording Error", true);
                break;
        }
    }

    //Show the surface, then clear it for next time round.
    IwGxSwapBuffers();
}
