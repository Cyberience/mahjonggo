These are examples of mdev package redirection
----------------------------------------------

Example client tries to pull in a subproject "pkg1". pkg1 is implemented as part of packages/pkg2, but it is necessary to build and install both pkg1 and pkg2 to get this to work, since there needs to be a pkg1 which effectively points to pkg2.

The following commands in packages:

cd pkg2
mdev create
mdev install pkg2_0.0.1-1.mdev
cd ../pkg1
mdev create
mdev install pkg1_0.0.2-1.mdev

(it may be necessary to use "%S3E_DIR%\bin\mdev" on PC instead of plain "mdev")

The opening the client will pull in these packages via mdev.


Package badredirect is an example of how not to do it - it redirects to itself.
