This is an example of how to subproject with a mixed-case name
--------------------------------------------------------------

Example client tries to pull in subproject Mixed_Case_Package which is to be implemented by an mdev package. However, it is not possible to have a package called that name, so the usual rule that the package has the same name as the project does not work. Instead we have to name the package following some simple rules: all characters are converted to lower-case and any underscore is converted to a hyphen. mkb itself will do the required translation automatically so the package will be found without further intervention.

Run the following commands in directory packages:

cd mixed-case-package
mdev create
mdev install mixed-case-package_0.0.1-1.mdev

(it may be necessary to use "%S3E_DIR%\bin\mdev" on PC instead of plain "mdev")

Then look in directory client. Open s3eMdevMixedCase.mkb either from the Hub or on the command line. This will pull the package in automatically allowing the client to be built and run on the simulator or whatever.

