/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

#include "Category.h"

#include "UnitConverter.h"

using namespace IwNUI;

//-----------------------------------------------------------------------------

Category::Category()
{
    Init();
}

Category::Category(UnitConverter* d, const char* caption)
{
    // Initializing Category
    Init();
    m_Caption = caption;
    m_Delegate = d;
}

void Category::Init()
{
    m_SourceIndex = 0;
    m_ResultIndex = 0;
}

Unit Category::GetByIndex(size_t index) const
{
    return m_Units.at(index);
}

void Category::SetDefaulUnit(const char *source, const char *result)
{
    for (size_t i = 0; i < m_Units.size(); ++i)
    {
        if (source == m_Units[i].m_Caption)
        {
            m_SourceIndex = i;
        }
        if (result == m_Units[i].m_Caption)
        {
            m_ResultIndex = i;
        }
    }
}

void Category::SelectUnit(IwNUI::CListBoxPtr unitsListBox, const Unit &unit)
{
    size_t index = 0;
    for (size_t i = 0; i < m_Units.size(); ++i)
    {
        if (unit.m_Caption.c_str() == m_Units[i].m_Caption)
        {
            index = i;
        }
    }
    unitsListBox->SetAttribute("selected", (int)index);
}

void Category::FillListBox(IwNUI::CListBoxPtr unitsListBox)
{
    CStringArray strArray;
    for (size_t i = 0; i < m_Units.size(); ++i)
    {
        strArray.AddString(CString(m_Units[i].m_Caption.c_str()));
    }

    unitsListBox->SetAttribute("listBoxItems", strArray);
}

Category& Category::AddUnit(const char* caption, float ratio)
{
    // Creating a unit and adding it to our internal vector
    m_Units.push_back(Unit(caption, ratio, 0));

    return *this;
}
