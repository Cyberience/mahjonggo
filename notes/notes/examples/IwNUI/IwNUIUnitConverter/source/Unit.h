/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

#ifndef UNIT_H
#define UNIT_H

#include <string>

//-----------------------------------------------------------------------------

class Unit
{
public:
    Unit() : m_Caption(""), m_Ratio(1.0f), m_Value(0.0f) {};
    Unit(const char* c, float r, float v) : m_Caption(c), m_Ratio(r), m_Value(v) {};
    Unit(Unit* u) : m_Caption(u->m_Caption), m_Ratio(u->m_Ratio), m_Value(0) {};

    void set(const char* caption, float ratio, float value = 0);

public:
    std::string m_Caption;
    float m_Ratio;
    float m_Value;
};

#endif // !UNIT_H
