/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
#ifndef FLOAT_LEXICAL_CAST_H
#define FLOAT_LEXICAL_CAST_H

#include <sstream>

//-----------------------------------------------------------------------------

template <typename Target>
struct stream_char { typedef char type; };

template <>
struct stream_char<std::wstring> {typedef wchar_t type; };

template <typename Target, typename Source>
Target LexicalCast(const Source& source)
{
    typedef typename stream_char<Target>::type stream_char_type;
    typedef typename std::basic_stringstream<stream_char_type> StreamType;

    StreamType buff;
    Target result = Target();
    buff << source;

    if (EOF != buff.peek())
    {
        buff >> result;
        if ((std::ios::failbit & buff.rdstate()) != 0)
        {
            throw std::runtime_error("Convertation failed.");
        }
        if (!buff.eof())
        {
            throw std::runtime_error("Unexpected characters.");
        }
    }

    return result;
}

template <typename Target, typename Source>
Target LexicalCastNoThrow(const Source& source, const Target &defaultValue)
{
    try
    {
        return LexicalCast<Target, Source>(source);
    }
    catch (const std::exception &)
    {
    }
    return defaultValue;
}

#endif // !FLOAT_LEXICAL_CAST_H
