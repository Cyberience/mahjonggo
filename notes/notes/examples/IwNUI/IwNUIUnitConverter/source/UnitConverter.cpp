/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

#include "UnitConverter.h"

#include "Category.h"
#include "FloatLexicalCast.h"
#include "s3eSurface.h"

using namespace IwNUI;

//-----------------------------------------------------------------------------

UnitConverter::UnitConverter(CAppPtr app) :
    m_Source("", 1.0f, 0),
    m_Result("", 1.0f, 0),
    m_ChangeUnitForSourceButton(true)
{
    m_App = app;
}

bool UnitConverter::OnSourceEdited(CTextField* textField, const char* text)
{
    // Doing a lexical cast to get float from text
    m_Source.m_Value = LexicalCastNoThrow<float>(text, 0);
    UpdateResult();
    return true;
}

bool UnitConverter::OnResultEdited(CTextField* textField, const char* text)
{
    // Doing a lexical cast to get float from text
    m_Result.m_Value = LexicalCastNoThrow<float>(text, 0);
    UpdateSource();
    return true;
}

bool UnitConverter::OnSelectTab(CTabBar* bar, int tab)
{
    // Once user clicks on the bottom tab bar we need to change category to a new one
    if (tab < (int)m_Categories.size() &&
        m_ActiveCategory != m_Categories.at(tab))
    {
        // In this case we get the default units and set them to Source and Result variables
        SetCategory(m_Categories[tab]);
        // Updating navigation bar to show what is being converted at the moment
        m_CategoryLabel->SetAttribute("title", m_ActiveCategory->GetCaption().c_str());
    }
    return true;
}

bool UnitConverter::OnMainViewClick(IwNUI::CView* view)
{
    return m_App->DeactivateInput();
}

void UnitConverter::UpdateValues(const Unit &source, Unit &result, IwNUI::CTextFieldPtr textField, IwNUI::CButtonPtr unitButton)
{
    result.m_Value = source.m_Value * result.m_Ratio/source.m_Ratio;
    // Updating "text" attribute on a resulting textfield with the lexical cast from float to std::string
    textField->SetAttribute("text", LexicalCastNoThrow<std::string>(result.m_Value, "").c_str());
    unitButton->SetAttribute("caption", result.m_Caption.c_str());
}

void UnitConverter::UpdateResult()
{
    UpdateValues(m_Source, m_Result, m_ResultText, m_ResultButton);
}

void UnitConverter::UpdateSource()
{
    UpdateValues(m_Result, m_Source, m_SourceText, m_SourceButton);
}

bool UnitConverter::OnUnitSelectedSource(IwNUI::CButton *button)
{
    m_ChangeUnitForSourceButton = (button == m_SourceButton.get());
    m_ActiveCategory->SelectUnit(m_UnitsListBox, m_ChangeUnitForSourceButton ? m_Source : m_Result);
    m_App->ShowWindow(m_SelectUnitWindow);
    return true;
}

bool UnitConverter::OnCancelButton(IwNUI::CButton *button)
{
    m_App->ShowWindow(m_MainWindow);
    return true;
}

bool UnitConverter::OnApplyButton(IwNUI::CButton *button)
{
    int index = 0;
    m_UnitsListBox->GetAttribute("selected", index);
    if (m_ChangeUnitForSourceButton)
    {
        m_Source = m_ActiveCategory->GetByIndex(index);
        UpdateSource();
    }
    else
    {
        m_Result = m_ActiveCategory->GetByIndex(index);
        UpdateResult();
    }

    m_App->ShowWindow(m_MainWindow);
    return true;
}

void UnitConverter::InitScreen()
{
    // Creating window for storing the main view with all its children
    m_MainWindow = CreateWindow();

    m_SelectUnitWindow = CreateWindow();

    // Adding the window to the app
    m_App->AddWindow(m_MainWindow);
    m_App->AddWindow(m_SelectUnitWindow);


    m_SelectUnitView = CreateView();
    m_SelectUnitWindow->SetChild(m_SelectUnitView);

    m_UnitsListBox = CreateListBox(CAttributes()
        .Set("width", "100%")
        .Set("height", "90%")
        );
    m_SelectUnitView->AddChild(m_UnitsListBox);

    CButtonPtr applyButton = CreateButton(CAttributes()
        .Set("caption", "Apply")
        .Set("alignH", "bottom")
        .Set("width", "25%")
        .Set("x1", "50%"));
    applyButton->SetEventHandler("click", this, &UnitConverter::OnApplyButton);
    m_SelectUnitView->AddChild(applyButton);

    CButtonPtr cancelButton = CreateButton(CAttributes()
        .Set("caption", "Cancel")
        .Set("alignH",  "bottom")
        .Set("width", "25%")
        .Set("x1", "25%"));
    cancelButton->SetEventHandler("click", this, &UnitConverter::OnCancelButton);
    m_SelectUnitView->AddChild(cancelButton);

    // Creating the view for storing all the visual components and adding it to the main window
    CViewPtr view = CreateView();
    view->SetEventHandler("clickview", this, &UnitConverter::OnMainViewClick);
    m_MainWindow->SetChild(view);

    // Creating Category tab bar with all the tab items
    CreateMainWindowTabBar(view);

    // Creating all the components for the main application screen
    CreateMainWindow(view);

    // Showing Main Window
    m_App->ShowWindow(m_MainWindow);
}

void UnitConverter::CreateMainWindowTabBar(CViewPtr& view)
{
    // Creating tabbar at the bottom of the screen
    // Selected tab is tab number 0
    CTabBarPtr categoryBar = CreateTabBar(CAttributes()
        .Set("name",                "CategoryTabBar")
        .Set("x1",                  "50%")
        .Set("alignH",              "bottom")
        .Set("width",               "100%")
        .Set("selected",            0)
        .Set("backgroundColour",    CColour(0x000000))
        .Set("alignW",              "centre"));

    // Setting event handler for handling tab selection
    categoryBar->SetEventHandler("selecttab", this, &UnitConverter::OnSelectTab);

    // Adding 5 tab bar items to the tab
    // on tab bar item for each category
    // They have only name and caption
    // After creation we add them as a children of the categoryBar
    CTabBarItemPtr item = CreateTabBarItem(CAttributes()
        .Set("name",        "LengthTabItem")
        .Set("caption",     "Length"));

    categoryBar->AddChild(item);

    item = CreateTabBarItem(CAttributes()
        .Set("name",        "SpeedTabBarItem")
        .Set("caption",     "Speed"));

    categoryBar->AddChild(item);

    item = CreateTabBarItem(CAttributes()
        .Set("name",        "AreaTabBarItem")
        .Set("caption",     "Area"));

    categoryBar->AddChild(item);

    item = CreateTabBarItem(CAttributes()
        .Set("name",        "MassTabBarItem")
        .Set("caption",     "Mass"));

    categoryBar->AddChild(item);

    item = CreateTabBarItem(CAttributes()
        .Set("name",        "VolumeTabBarItem")
        .Set("caption",     "Volume"));

    categoryBar->AddChild(item);

    // Adding categoryBar to main view
    view->AddChild(categoryBar);
}

void UnitConverter::CreateMainWindow(CViewPtr& view)
{
    // Creating navigation bar that show current category
    m_CategoryLabel = CreateNavigationBar(CAttributes()
        .Set("name",        "categoryLabel")
        .Set("title",       "Convert Length")
        .Set("y1",          "0")
        .Set("width",       "100%")
        .Set("font",        "font/lsans.ttf,font/Servetica Medium Public.ttf,serif")
        .Set("fontSize",    12)
        .Set("fontColour",  "Red")
        .Set("fontAlignV",  "bottom")
        .Set("fontAlignH",  "centre")
        );
    view->AddChild(m_CategoryLabel);

    // Creating temporary instance of CAttributes that stores common attributes
    // for our components. This one is for Text Fields
    CAttributes textFieldAttrs = CAttributes()
        .Set("name",        "sourceTextField")
        .Set("x1",          "4%")
        .Set("width",       "44%")
        .Set("height",      "10%")
        .Set("font",        "serif")
        .Set("fontSize",    8)
        .Set("fontColour",  CColour(0xFF000000))
        .Set("backgroundColour", CColour(0xFFFFFFFF))
        .Set("fontAlignH",  "right");

    // This one is for button
    CAttributes listButtonAttrs = CAttributes()
        .Set("name",        "sourceButton")
        .Set("caption",     "")
        .Set("x1",          "52%")
        .Set("width",       "44%")
        .Set("height",      "10%")
        .Set("font",        "font/lsans.ttf,font/Servetica Medium Public.ttf,serif")
        .Set("fontSize",    8)
        .Set("fontColour",  "Black")
        .Set("fontAlignV",  "middle")
        .Set("fontAlignH",  "centre");

    // Creating TextField with the existing attributes variable and updating y position
    m_SourceText = CreateTextField(
        textFieldAttrs
        .Set("y1",          "35%")
        );
    // Adding textfield to the view and setting event handler
    view->AddChild(m_SourceText);
    m_SourceText->SetEventHandler("textchanged", this, &UnitConverter::OnSourceEdited);

    // Creating button with the existing attributes variable and updating y position
    m_SourceButton = CreateButton(
        listButtonAttrs
        .Set("y1", "35%")
        );
    view->AddChild(m_SourceButton);
    m_SourceButton->SetEventHandler("click", this, &UnitConverter::OnUnitSelectedSource);

    // Creating button with the existing attributes variable and updating y position
    m_ResultButton = CreateButton(
        listButtonAttrs
        .Set("y1", "60%")
        );
    view->AddChild(m_ResultButton);
    m_ResultButton->SetEventHandler("click", this, &UnitConverter::OnUnitSelectedSource);

    // Creating TextField with the existing attributes variable and updating y position
    m_ResultText = CreateTextField(
        textFieldAttrs
        .Set("y1",          "60%")
        );
    // Adding textfield to the view and setting event handler
    view->AddChild(m_ResultText);
    m_ResultText->SetEventHandler("textchanged", this, &UnitConverter::OnResultEdited);
}

void UnitConverter::InitCategories()
{
    // Creating categories and adding units to them

    CategoryPtr lengthCategory(new Category(this, "Convert Length"));
    (*lengthCategory)
        .AddUnit("Centimeters",     100.0f)
        .AddUnit("Meters",          1.0f)
        .AddUnit("Kilometers",      0.001f)
        .AddUnit("Inches",          39.370079f)
        .AddUnit("Feet",            3.28084f)
        .AddUnit("Yards",           1.093613f)
        .AddUnit("Miles",           0.000621f)
        .AddUnit("Ells",            1.142857f)
        .AddUnit("Cables",          0.005468f)
        .AddUnit("Furlongs",        0.004971f)
        .AddUnit("Hands",           9.84252f)
        .AddUnit("Thou",            39370.07874f);
    lengthCategory->SetDefaulUnit("Miles", "Kilometers");
    m_Categories.push_back(lengthCategory);

    CategoryPtr speedCategory(new Category(this, "Convert Speed"));
    (*speedCategory)
        .AddUnit("km/h",            1.0f)
        .AddUnit("m/sec",           0.277778f)
        .AddUnit("cm/sec",          27.7778f)
        .AddUnit("feet/h",          3280.833333f)
        .AddUnit("feet/sec",        0.911344f)
        .AddUnit("knot",            0.539957f)
        .AddUnit("mph",             0.621371f)
        .AddUnit("yards/min",       18.226852f);
    speedCategory->SetDefaulUnit("mph", "km/h");
    m_Categories.push_back(speedCategory);

    CategoryPtr areaCategory(new Category(this, "Convert Area"));
    (*areaCategory)
        .AddUnit("Square meters",   1.0f)
        .AddUnit("Acres",           0.000247f)
        .AddUnit("Ares",            0.01f)
        .AddUnit("Hides",           0.000002f)
        .AddUnit("Roods",           0.000959f)
        .AddUnit("Circular inches", 1914.781289f)
        .AddUnit("Hectares",        0.0001f)
        .AddUnit("Square feet",     10.76391f)
        .AddUnit("Square inches",   1550.0031f)
        .AddUnit("Square yards",    1.19599f);
    areaCategory->SetDefaulUnit("Square meters", "Acres");
    m_Categories.push_back(areaCategory);

    CategoryPtr massCategory(new Category(this, "Convert Mass"));
    (*massCategory)
        .AddUnit("Kilograms",       1.0f)
        .AddUnit("Carats",          5000.0f)
        .AddUnit("Cental",          0.022046f)
        .AddUnit("Grains",          15432.358353f)
        .AddUnit("Grams",           1000.0f)
        .AddUnit("Ounces",          35.273962f)
        .AddUnit("Pounds",          2.204623f)
        .AddUnit("Stones",          0.157473f)
        .AddUnit("Pounds",          2.204623f)
        .AddUnit("Tons UK",         0.000984f)
        .AddUnit("Tonnes",          0.001f);
    massCategory->SetDefaulUnit("Kilograms", "Pounds");
    m_Categories.push_back(massCategory);

    CategoryPtr volumeCategory(new Category(this, "Convert Volume"));
    (*volumeCategory)
        .AddUnit("Liters",          1.0f)
        .AddUnit("Barrels(oil)",    0.00629f)
        .AddUnit("Bushels",         0.027496f)
        .AddUnit("Centiliters",     100.0f)
        .AddUnit("Cubic feet",      0.035315f)
        .AddUnit("Cups",            4.226753f)
        .AddUnit("Dram",            281.560638f)
        .AddUnit("Fluid ounces",    35.19508f)
        .AddUnit("Gallons",         0.219969f)
        .AddUnit("Mililiters",      1000.0f)
        .AddUnit("Pints",           1.759754f)
        .AddUnit("Quarts",          0.879877f)
        .AddUnit("Tea spoons",      202.884136f);
    volumeCategory->SetDefaulUnit("Liters", "Gallons");
    m_Categories.push_back(volumeCategory);

    SetCategory(lengthCategory);
}

void UnitConverter::SetCategory(CategoryPtr category)
{
    // Getting default units and updating buttons
    m_ActiveCategory = category;
    m_Source = m_ActiveCategory->GetDefaultUnitSource();
    m_Result = m_ActiveCategory->GetDefaultUnitResult();

    m_ActiveCategory->FillListBox(m_UnitsListBox);

    m_Source.m_Value = 1;

    UpdateResult();
    UpdateSource();
}
