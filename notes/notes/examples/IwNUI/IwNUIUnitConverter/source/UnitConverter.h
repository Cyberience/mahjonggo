/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

#ifndef UNIT_CONVERTER_H
#define UNIT_CONVERTER_H

#include "Unit.h"

#include "IwNUI.h"

// Forward declarations
class Category;

//-----------------------------------------------------------------------------

class UnitConverter
{
public:
    typedef IwNUI::CSharedPtr<Category> CategoryPtr;
    typedef std::vector<CategoryPtr> tCategories;

    UnitConverter(IwNUI::CAppPtr app);

    void InitScreen();
    void InitCategories();

private:
    void CreateMainWindowTabBar(IwNUI::CViewPtr& view);
    void CreateMainWindow(IwNUI::CViewPtr& view);

    bool OnSelectTab(IwNUI::CTabBar* bar, int tab);
    bool OnMainViewClick(IwNUI::CView* view);
    bool OnSourceEdited(IwNUI::CTextField* textField, const char* text);
    bool OnResultEdited(IwNUI::CTextField* textField, const char* text);
    bool OnUnitSelectedSource(IwNUI::CButton *button);
    bool OnApplyButton(IwNUI::CButton *button);
    bool OnCancelButton(IwNUI::CButton *button);

    void SetCategory(CategoryPtr category);
    void UpdateValues(const Unit &source, Unit &result, IwNUI::CTextFieldPtr textField, IwNUI::CButtonPtr unitButton);
    void UpdateResult();
    void UpdateSource();

private:
    bool                    m_StoRdir;          // boolean variable representing Source to Result direction
    Unit                    m_Source;           // Source unit of convertion
    Unit                    m_Result;           // Resulting unit of convertion

    IwNUI::CAppPtr          m_App;              // Pointer to our app. Is used for adding windows and switching them
    IwNUI::CWindowPtr       m_MainWindow;       // Instance of main UnitConverter window

    IwNUI::CWindowPtr       m_SelectUnitWindow; // Instance of window for selecting convertion unit
    IwNUI::CViewPtr         m_SelectUnitView;

    IwNUI::CListBoxPtr      m_UnitsListBox;    // ListBox to select source unit for conversion

    IwNUI::CButtonPtr       m_SourceButton;    // ListBox to select source unit for conversion
    IwNUI::CButtonPtr       m_ResultButton;    // ListBox to select destination unit for convertion
    bool                    m_ChangeUnitForSourceButton;

    IwNUI::CTextFieldPtr    m_SourceText;
    IwNUI::CTextFieldPtr    m_ResultText;

    IwNUI::CNavigationBarPtr    m_CategoryLabel;

    CategoryPtr             m_ActiveCategory;
    tCategories             m_Categories;
};

#endif // !UNIT_CONVERTER_H
