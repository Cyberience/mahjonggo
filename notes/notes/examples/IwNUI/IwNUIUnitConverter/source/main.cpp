/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwNUIUnitConverter NUI Unit Converter Test Application
 *
 * The test application demonstrates how to include and use NUI Extension
 * for creation Unit Converter application. It allows to select a unit for
 * conversion, choose destination and displays the result when the users presses
 * button.
 *
 * It uses the following components:
 * - Button
 * - TabBar
 * - Label
 * - TextField
 * - ScrollView
 * - CheckBox
 *
 * MKB:
 * @include IwNUI/IwNUIUnitConverter/IwNUIUnitConverter.mkb
 *
 * Source:
 * @include IwNUI/IwNUIUnitConverter/source/main.cpp
 * @include IwNUI/IwNUIUnitConverter/source/UnitConverter.h
 * @include IwNUI/IwNUIUnitConverter/source/UnitConverter.cpp
 * @include IwNUI/IwNUIUnitConverter/source/Category.h
 * @include IwNUI/IwNUIUnitConverter/source/Category.cpp
 * @include IwNUI/IwNUIUnitConverter/source/Unit.h
 * @include IwNUI/IwNUIUnitConverter/source/Unit.cpp
 *
 */

#include "IwNUI.h"

#include "Unit.h"
#include "UnitConverter.h"
#include "Category.h"

using namespace IwNUI;

//-----------------------------------------------------------------------------

int main()
{
    // Creating an instance of the application
    CAppPtr myApp = CreateApp();

    // Creating an instance of the main class
    UnitConverter converter(myApp);

    // Initing the main screen with buttons, textfields and tabbar
    converter.InitScreen();
    // After all we add categories for convertion
    converter.InitCategories();

    // At last we run our application
    myApp->Run();

    return 0;
}
