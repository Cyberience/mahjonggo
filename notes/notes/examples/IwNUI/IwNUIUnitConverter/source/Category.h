/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

#ifndef CATEGORY_H
#define CATEGORY_H

#include "Unit.h"

#include "IwNUI.h"

class UnitConverter;

//-----------------------------------------------------------------------------

class Category
{
public:
    Category();
    Category(UnitConverter* d, const char* caption);
    Category& AddUnit(const char* caption, float ratio);

    std::string         GetCaption()        const { return m_Caption; }
    Unit                GetDefaultUnitSource()    const { return m_Units.at(m_SourceIndex); }
    Unit                GetDefaultUnitResult()    const { return m_Units.at(m_ResultIndex); }

    void SelectUnit(IwNUI::CListBoxPtr unitsListBox, const Unit &unit);
    void SetDefaulUnit(const char *source, const char *result);
    void FillListBox(IwNUI::CListBoxPtr unitsListBox);
    Unit GetByIndex(size_t index) const;

private:
    void Init();

public:
    IwNUI::CViewPtr m_View;

private:
    uint32 m_ActiveUnitID;
    std::vector<Unit> m_Units;
    std::string m_Caption;
    size_t m_SourceIndex;
    size_t m_ResultIndex;

    UnitConverter* m_Delegate;
};

#endif // !CATEGORY_H
