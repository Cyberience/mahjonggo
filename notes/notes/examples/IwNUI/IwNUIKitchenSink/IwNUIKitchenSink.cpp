/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

//-----------------------------------------------------------------------------
// Example IwNUIKitchenSink
//-----------------------------------------------------------------------------

/**
 * @page ExampleIwNUIKitchenSink IwNUI Kitchen Sink Example
 * The following example demonstrates the all the IwNUI controls.
 *
 * <TABLE border="0">
 *   <TR>
 *     <TD padding="5px">
 * @image html IwNUIKitchenSinkImageIPhone.png "iOS"
 *     </TD>
 *     <TD padding="5px">
 * @image html IwNUIKitchenSinkImageAndroid.png "Android"
 *     </TD>
 *     <TD padding="5px">
 * @image html IwNUIKitchenSinkImage.png "IwUI"
 *     </TD>
 *   </TR>
 * </TABLE>
 *
 * @include IwNUIKitchenSink.cpp
 *
 */

#include "IwNUI.h"

#include "s3e.h"
#include <vector>

using namespace IwNUI;

const int sliderMaxinumValue = 50;
const CColour COLOUR_BLACK      (0xff000000);
const CColour COLOUR_WHITE      (0xffFFFFFF);
const CColour COLOUR_PAGE       (0xffECECEC);

const CColour COLOUR_LABEL      (0xff656565);


const CColour COLOUR_DARK_GREY  (0xff666666);
const CColour COLOUR_GREY       (0xffC9C0BC);
const CColour COLOUR_LIGHTGREY  (0xffF0F0F0);
const CColour COLOUR_ALPHA      (0xa0656565);
const CColour COLOUR_BLUE       (0xffFF0000);
const CColour COLOUR_RED        (0xff0000FF);
const CColour COLOUR_GREEN      (0xff0CD00C);

class Handler
{
private:
    std::vector<CTableItemPtr> m_TableRows;
    Handler(const Handler&);
    Handler& operator=(const Handler&);

public:
    Handler()
    {
        IwAssert(NUI, !s_Handler);
        s_Handler = this;
    }

    ~Handler()
    {
        s_Handler = NULL;
    }

    static Handler* s_Handler;

    CViewPtr* views;

    CLabelPtr buttonsStatusLabel;

    CLabelPtr toolbarStatusLabel;

    CLabelPtr checkBoxStatusLabel;

    CLabelPtr sliderBarStatusLabel;

    CTablePtr table;

    CStringArray tableItems;

    CLabelPtr tableStatusLabel;

    CStringArray listBoxItems;

    CListBoxPtr listBox;

    CLabelPtr labelPicker;

    CAlertDialogPtr alertDialog;

    CProgressBarPtr progressBar;

    CActivityIndicatorPtr indicator;

    CToolBarButtonPtr indicatorButton;

    CAppPtr   app;

    bool onToolbarClick(CToolBar* toolbar, int button)
    {
        char buffer[0x100];
        sprintf(buffer, "Toolbar Button %d", button+1);

        toolbarStatusLabel->SetAttribute("caption", buffer);
        return true;
    }

    bool onSelectTab(CTabBar* bar, int tab)
    {
        for (int i = 0; i < 5; i++)
        {
            views[i]->SetAttribute("visible", i == tab);
        }

        return true;
    }

    bool onButton1Click(CButton* button)
    {
        buttonsStatusLabel->SetAttribute("caption", "Clicked: Button 1");
        return true;
    }

    bool onButton2Click(CButton* button)
    {
        buttonsStatusLabel->SetAttribute("caption", "Clicked: Button 2");
        return true;
    }

    bool onButton3Click(CButton* button)
    {
        buttonsStatusLabel->SetAttribute("caption", "Clicked: Button 3");
        return true;
    }

    bool onButton4Click(CButton* button)
    {
        buttonsStatusLabel->SetAttribute("caption", "Clicked: Button 4");
        return true;
    }

    bool onButton5Click(CButton* button)
    {
        buttonsStatusLabel->SetAttribute("caption", "Clicked: Button 5");
        return true;
    }

    bool onButton6Click(CButton* button)
    {
        buttonsStatusLabel->SetAttribute("caption", "Clicked: Button 6");
        return true;
    }

    bool onButton7Click(CButton* button)
    {
        buttonsStatusLabel->SetAttribute("caption", "Clicked: Button 7");
        return true;
    }

    bool onCheckBoxChange(CCheckBox* pCheck, bool checked)
    {
        if (checked)
        {
            checkBoxStatusLabel->SetAttribute("caption", "Checkbox Checked");
        }
        else
        {
            checkBoxStatusLabel->SetAttribute("caption", "Checkbox Unchecked");
        }

        return true;
    }

    bool onSlide(CSliderBar* pSliderBar, int sliderValue)
    {
        char str[256];
        sprintf(str, "SliderBar - %d/50", sliderValue);
        sliderBarStatusLabel->SetAttribute("caption", str);
        progressBar->SetAttribute("progress", (sliderValue * 1.0f) / (sliderMaxinumValue));
        return true;
    }

    bool OnMisc1ViewClick(CView* view)
    {
        return app->DeactivateInput();
    }

    bool onSelectTable(CTable* table, int tab)
    {
        char tmp[0xff];
        sprintf(tmp, "Selected: %d", tab);

        tableStatusLabel->SetAttribute("caption", tmp);
        return true;
    }

    bool onAddRowClick(CButton* button)
    {
        AddTableRow();
        return true;
    }

    bool onRemoveRowClick(CButton* button)
    {
        RemoveTableRow();
        return true;
    }

    bool onIndicatorToolbarClick(CToolBar* toolbar, int)
    {
        static bool active = true;
        active = !active;
        indicator->SetAttribute("animated", active);
        indicatorButton->SetAttribute("caption", active ? "de-activate" : "activate");
        return true;
    }

    bool onListBoxItemSelected(CListBox* button, int item)
    {
        char str[256];
        snprintf(str, 256, "ListBox item selected = %s", listBoxItems.GetString(item).Get());
        labelPicker->SetAttribute("caption" , str);

        return true;
    }

    bool onNavBarClick(CNavigationBar* navbar, int button)
    {
        char str[256];
        static int curPage = 3;

        switch (button)
        {
            case S3E_NUI_NAVIGATION_BAR_BUTTON_LEFT:
                --curPage;
                break;
            case S3E_NUI_NAVIGATION_BAR_BUTTON_RIGHT:
                ++curPage;
                break;
            default:
                break;
        }

        sprintf(str, "%d of 5", curPage);
        navbar->SetAttribute("title" , str);

        if (curPage > 1)
        {
            sprintf(str, "Prev (%d)", (curPage - 1));
        }
        else
        {
            str[0] = 0;
        }
        navbar->SetAttribute("leftButtonCaption" , str);

        if (curPage < 5)
        {
            sprintf(str, "Next (%d)", (curPage + 1));
        }
        else
        {
            str[0] = 0;
        }
        navbar->SetAttribute("rightButtonCaption" , str);

        return true;
    }

    void AddTableRow()
    {
        int rows = m_TableRows.size();
        if (rows >= tableItems.GetSize())
            return;

        CTableItemPtr ti = CreateTableItem(CAttributes()
            .Set("width",   "100%")
            .Set("height",  "50"));

        char tmp[0xff];
        snprintf(tmp, 0xff, "LabelTable%d", rows);

        CLabelPtr label = CreateLabel(CAttributes()
            .Set("name",    CString(tmp))
            .Set("caption", tableItems.GetString(rows))
            .Set("width", "100%")
            .Set("height", "100%")
            );

        ti->AddChild(label);

        table->AddChild(ti);
        m_TableRows.push_back(ti);
    }

    void RemoveTableRow()
    {
        uint32 rows = m_TableRows.size();
        if (rows == 0)
            return;

        table->RemoveChild(m_TableRows[rows-1]);
        m_TableRows.pop_back();
    }

    bool onDialogAlertButtonClick(CButton* button)
    {
        alertDialog->SetAttribute("message", "Would you like to close this dialogue?");
        alertDialog->SetAttribute("positiveButtonCaption",  "Yes");
        alertDialog->SetAttribute("negativeButtonCaption",  "No");
        alertDialog->SetAttribute("neutralButtonCaption",   "Maybe");

        alertDialog->Show();
        return true;
    }

    bool onAlertButtonClick(CAlertDialog* dialog, EAlertDialogButton button)
    {
        switch(button)
        {
            case IWNUI_ALERT_DIALOG_POSITIVE:
                // Let alert dialog close after positive button press
                break;
            case IWNUI_ALERT_DIALOG_NEGATIVE:
                alertDialog->SetAttribute("message",
                    "Are you sure you don't want to close this dialogue?\n"
                    "Did you actually mean to click yes?");
                alertDialog->Show();
                break;
            case IWNUI_ALERT_DIALOG_NEUTRAL:
                alertDialog->SetAttribute("message",
                    "Make a decision...\n"
                    "Would you like to close this dialogue?");
                alertDialog->SetAttribute("neutralButtonCaption", "");

                alertDialog->Show();
                break;
        }
        return true;
    }
};


Handler* Handler::s_Handler = NULL;

CViewPtr CreateTabPageView()
{
    return CreateView(CAttributes()
        .Set("x1",      0)
        .Set("y1",      0)
        .Set("width",   "100%")
        .Set("height",  "90%")
        .Set("backgroundColour", COLOUR_PAGE)
        );
}


CAttributes CreateLabelsAttrHelper(const char* x1, const char* y1,
    const char* width, const char* height,
    const char* name, const char* caption,
    const char* alignH, const char* alignV)
{
    return CAttributes()
        .Set("name",       name)
        .Set("x1",         x1)
        .Set("y1",         y1)
        .Set("width",      width)
        .Set("height",     height)
        .Set("caption",    caption)
        .Set("fontSize",   "medium")
        .Set("fontAlignH", alignH)
        .Set("fontAlignV", alignV);
}

CAttributes CreateLabelsAttrHelper(const char* y1,
    const char* name, const char* caption,
    const char* alignH = "left")
{
    return CreateLabelsAttrHelper("4%", y1, "92%", "7%", name, caption, alignH, "middle");
}

CLabelPtr CreateLabelsViewLabel(const char* y1,
    const char* name, const char* caption,
    const char* alignH)
{
    return CreateLabel(
        CreateLabelsAttrHelper(y1, name, caption, alignH)
        .Set("fontColour", COLOUR_LABEL)
        );
}

CViewPtr CreateLabelsView()
{
    CViewPtr result = CreateTabPageView();

    result->AddChild(CreateLabelsViewLabel("7%",  "Label1", "Label1 (default)",         "left"));

    result->AddChild(CreateLabelsViewLabel("16%", "Label2", "Label2 (align - center)",  "centre"));

    result->AddChild(CreateLabelsViewLabel("25%", "Label3", "Label 3 (align - right)",  "right"));

    CViewPtr grayView = CreateView(CAttributes()
        .Set("name",        "grayView")
        .Set("x1",          0)
        .Set("y1",          "34%")
        .Set("width",       "100%")
        .Set("height",      "18%")
        .Set("backgroundColour", COLOUR_GREY));

    result->AddChild(grayView);

    CLabelPtr label4 = CreateLabel(
        CreateLabelsAttrHelper("4%", "0", "92%", "50%",
            "Label4", "Label4 (align top)", "right", "top")
        .Set("fontColour",       COLOUR_LABEL)
        .Set("backgroundColour", COLOUR_GREY)
        .Set("shadow",           true)
        .Set("shadowOffset",     CVector2(0, 2))
        .Set("shadowColour",     COLOUR_WHITE)
        );

    grayView->AddChild(label4);

    CLabelPtr label5 = CreateLabel(
        CreateLabelsAttrHelper("4%", "50%", "92%", "50%",
            "Label5", "Label5 (align bottom)", "left", "bottom")
        .Set("fontColour",       COLOUR_LABEL)
        .Set("backgroundColour", COLOUR_GREY)
        .Set("shadow",           true)
        .Set("shadowOffset",     CVector2(0, 2))
        .Set("shadowColour",     COLOUR_WHITE)
        );

    grayView->AddChild(label5);


    CLabelPtr label6 = CreateLabel(
        CreateLabelsAttrHelper("52%", "Label6", "Label 6 (custom colour)")
        .Set("fontColour", COLOUR_RED)
        );
    result->AddChild(label6);

    CLabelPtr label7 = CreateLabel(
        CreateLabelsAttrHelper("61%", "Label7", "Label 7 (custom font)")
        .Set("fontColour",  COLOUR_LABEL)
        .Set("font",        "font/Strato-unlinked.ttf,Strato,serif")
        );
    result->AddChild(label7);

    CLabelPtr label8 = CreateLabel(
        CreateLabelsAttrHelper("70%", "Label8", "Label 8 (bold)")
        .Set("fontColour",  COLOUR_LABEL)
        .Set("fontStyle",   "bold")
        );
    result->AddChild(label8);

    CLabelPtr label9 = CreateLabel(
        CreateLabelsAttrHelper("79%", "Label9", "Label 9 (Italic)")
        .Set("fontColour",  COLOUR_LABEL)
        .Set("fontStyle",   "italic")
        );
    result->AddChild(label9);

    CLabelPtr label10 = CreateLabel(
        CreateLabelsAttrHelper("88%", "Label10", "Label 10 (alpha)")
        .Set("fontColour",  COLOUR_ALPHA)
        .Set("fontStyle",   "italic")
        .Set("alpha",       0.5f)
        );
    result->AddChild(label10);

    return result;
}

CViewPtr CreateButtonsView()
{
    Handler& handler = *Handler::s_Handler;

    CViewPtr result = CreateTabPageView();

    CButtonPtr button1 = CreateButton(CAttributes()
        .Set("name",    "Button1")
        .Set("caption", "Button1 (default)")
        .Set("x1",      "10%")
        .Set("y1",      "2%")
        .Set("width",   "80%")
        .Set("height",  "10%")
        .Set("fontSize", "medium"));

    button1->SetEventHandler("click", &handler, &Handler::onButton1Click);
    result->AddChild(button1);

    CButtonPtr button2 = CreateButton(CAttributes()
        .Set("name",    "Button2")
        .Set("caption", "Button2 (textcolour)")
        .Set("x1",      "10%")
        .Set("y1",      "13%")
        .Set("width",   "80%")
        .Set("height",  "10%")
        .Set("fontColour", COLOUR_GREEN)
        .Set("fontSize", "medium")
        );

    button2->SetEventHandler("click", &handler, &Handler::onButton2Click);
    result->AddChild(button2);

    CButtonPtr button3 = CreateButton(CAttributes()
        .Set("name",    "Button3")
        .Set("caption", "Button3 (alignment)")
        .Set("x1",      "10%")
        .Set("y1",      "24%")
        .Set("width",   "80%")
        .Set("height",  "10%")
        .Set("fontAlignH", "right")
        .Set("fontAlignV", "bottom")
        .Set("fontSize", "medium"));

    button3->SetEventHandler("click", &handler, &Handler::onButton3Click);
    result->AddChild(button3);

    CButtonPtr button4 = CreateButton(CAttributes()
        .Set("name",    "Button4")
        .Set("caption", "Button4 (custom font)")
        .Set("x1",      "10%")
        .Set("y1",      "36%")
        .Set("width",   "80%")
        .Set("height",  "10%")
        .Set("font",    "serif")
        .Set("fontSize", "medium"));

    button4->SetEventHandler("click", &handler, &Handler::onButton4Click);
    result->AddChild(button4);

    CButtonPtr button5 = CreateButton(CAttributes()
        .Set("name",    "Button5")
        .Set("caption", "Button5 (font size)")
        .Set("x1",      "2%")
        .Set("y1",      "47%")
        .Set("width",   "96%")
        .Set("height",  "22%")
        .Set("fontColour", COLOUR_LABEL)
        .Set("fontSize", "xx-large"));

    button5->SetEventHandler("click", &handler, &Handler::onButton5Click);
    result->AddChild(button5);

    CButtonPtr button6 = CreateButton(CAttributes()
        .Set("name",    "Button6")
        .Set("caption", "Button6 (alpha)")
        .Set("x1",      "10%")
        .Set("y1",      "70%")
        .Set("width",   "80%")
        .Set("height",  "10%")
        .Set("alpha", 0.5f)
        .Set("fontSize", "medium"));

    button6->SetEventHandler("click", &handler, &Handler::onButton6Click);
    result->AddChild(button6);

    CButtonPtr button7 = CreateButton(CAttributes()
        .Set("name",    "Button7")
        .Set("caption", "Custom Button")
        .Set("x1",      "10%")
        .Set("y1",      "81%")
        .Set("width",   "80%")
        .Set("height",  "10%")
        .Set("fontColour",  COLOUR_LABEL)
        .Set("font",        "font/Strato-unlinked.ttf,Strato,serif")
        .Set("normal.image","custom_button.png")
        .Set("pressed",     CAttributes()
            .Set("image",   "custom_button_off.png"))
        .Set("disabled",    CAttributes()
        .   Set("image",    "custom_button_off.png"))
        .Set("fontSize",    "large"));

    button7->SetEventHandler("click", &handler, &Handler::onButton7Click);
    result->AddChild(button7);

    CLabelPtr statusLabel = CreateLabel(CAttributes()
        .Set("name",    "StatusLabel")
        .Set("x1",      "18%")
        .Set("y1",      "92%")
        .Set("width",   "68%")
        .Set("height",  "6%")
        .Set("caption", "")
        .Set("fontSize",    "x-large")
        .Set("fontColour", COLOUR_BLACK)
        .Set("fontAlignH", "centre")
        .Set("fontAlignV", "middle"));

    result->AddChild(statusLabel);

    handler.buttonsStatusLabel = statusLabel;

    return result;
};

CViewPtr CreateMisc1View()
{
    Handler& handler = *Handler::s_Handler;

    int32 screenWidth = s3eSurfaceGetInt(S3E_SURFACE_WIDTH);
    CViewPtr result = CreateTabPageView();

    CScrollViewPtr scrollView = CreateScrollView(CAttributes()
        .Set("name",    "ScrollView")
        .Set("x1",      0)
        .Set("y1",      0)
        .Set("width",   "100%")
        .Set("height",  "100%")
        .Set("scrollX", 0)
        .Set("scrollY", 0)
        .Set("direction","vertical")
        .Set("backgroundColour", COLOUR_PAGE));

    result->AddChild(scrollView);

    CToolBarPtr toolbar = CreateToolBar(CAttributes()
        .Set("name",    "ToolBar")
        .Set("x1",      "0%")
        .Set("y1",      "0%")
        .Set("width",   "100%")
        .Set("height",  "10%")
        );

    CToolBarButtonPtr item = CreateToolBarButton(CAttributes()
        .Set("name",    "ToolBarItem1")
        .Set("caption", "Button1"));

    toolbar->AddChild(item);

    item = CreateToolBarButton(CAttributes()
        .Set("name",    "ToolBarItem2")
        .Set("caption", "Button2"));

    toolbar->AddChild(item);

    toolbar->SetEventHandler("clicktoolbar", &handler, &Handler::onToolbarClick);

    scrollView->AddChild(toolbar);

    CLabelPtr toolbarStatusLabel = CreateLabel(CAttributes()
        .Set("name",    "ToolbarStatusLabel")
        .Set("width",   "98%")
        .Set("height",  "10%")
        .Set("caption", "Toolbar")
        .Set("fontSize",   "medium")
        .Set("fontStyle",  "bold")
        .Set("fontColour", COLOUR_WHITE)
        .Set("fontAlignH", "right")
        .Set("fontAlignV", "middle"));

    scrollView->AddChild(toolbarStatusLabel);
    handler.toolbarStatusLabel = toolbarStatusLabel;


    CCheckBoxPtr checkBox = CreateCheckBox(CAttributes()
        .Set("name",    "CheckBox")
        .Set("x1",      "52%")
        .Set("y1",      "13%")
        .Set("check",   true));

    checkBox->SetEventHandler("checked", &handler, &Handler::onCheckBoxChange);
    scrollView->AddChild(checkBox);

    CLabelPtr checkBoxStatusLabel = CreateLabel(CAttributes()
        .Set("name",    "CheckBoxStatusLabel")
        .Set("x1",      "3%")
        .Set("y1",      "14%")
        .Set("width",   "50%")
        .Set("caption", "Checkbox Checked")
        .Set("fontSize",   "small")
        .Set("fontStyle",  "bold")
        .Set("fontColour", COLOUR_DARK_GREY)
        .Set("fontAlignH", "left")
        .Set("fontAlignV", "top"));

    scrollView->AddChild(checkBoxStatusLabel);
    handler.checkBoxStatusLabel = checkBoxStatusLabel;

    CSliderBarPtr sliderBar = CreateSliderBar(CAttributes()
        .Set("name",    "SliderBar")
        .Set("x1",      "42%")
        .Set("y1",      "22%")
        .Set("width",   "50%")
        .Set("minimumValue",  0)
        .Set("maximumValue",  sliderMaxinumValue)
        .Set("value", 25));

    sliderBar->SetEventHandler("slide", &handler, &Handler::onSlide);
    scrollView->AddChild(sliderBar);

    CLabelPtr sliderBarStatusLabel = CreateLabel(CAttributes()
        .Set("name",    "SliderBarStatusLabel")
        .Set("x1",      "3%")
        .Set("y1",      "30%")
        .Set("width",   "50%")
        .Set("caption", "SliderBar - 25/50")
        .Set("fontSize",   "small")
        .Set("fontStyle",  "bold")
        .Set("fontColour", COLOUR_DARK_GREY)
        .Set("fontAlignH", "left")
        .Set("fontAlignV", "top"));

    scrollView->AddChild(sliderBarStatusLabel);
    handler.sliderBarStatusLabel = sliderBarStatusLabel;

    CProgressBarPtr progressBar = CreateProgressBar(CAttributes()
        .Set("name",    "ProgressBar")
        .Set("x1",      "42%")
        .Set("y1",      "30%")
        .Set("width",   "50%")
        .Set("progress", 0.5f));

    handler.progressBar = progressBar;
    scrollView->AddChild(progressBar);

    CToolBarPtr indicatorToolbar = CreateToolBar(CAttributes()
        .Set("name",    "indicatorToolbar")
        .Set("x1",      "0%")
        .Set("y1",      "41%")
        .Set("width",   "100%")
        .Set("height",  "10%"));

    item = CreateToolBarButton(CAttributes()
        .Set("name",    "ToolBarItem2")
        .Set("caption", "de-activate"));

    handler.indicatorButton = item;

    indicatorToolbar->SetEventHandler("clicktoolbar", &handler, &Handler::onIndicatorToolbarClick);
    indicatorToolbar->AddChild(item);

    scrollView->AddChild(indicatorToolbar);

    CActivityIndicatorPtr indicator = CreateActivityIndicator(CAttributes()
                                        .Set("name",    "buttonIndicatorSwitch")
                                        .Set("animated", true)
                                        .Set("x1",      "35%")
                                        .Set("y1",      "42%")
                                        .Set("height",  (int)(screenWidth * 0.08))
                                        .Set("width",   (int)(screenWidth * 0.08)));
    handler.indicator = indicator;
    scrollView->AddChild(indicator);

    CTextFieldPtr textfield1 = CreateTextField(CAttributes()
        .Set("name",    "TextField1")
        .Set("x1",      "5%")
        .Set("y1",      "60%")
        .Set("width",   "85%")
        .Set("height",  "15%")
        .Set("text", "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda.")
        .Set("fontSize",    "medium")
        .Set("fontColour",  COLOUR_BLACK)
        .Set("backgroundColour", COLOUR_WHITE)
        .Set("fontAlignH",  "left"));

    scrollView->AddChild(textfield1);

    CTextFieldPtr textfield2 = CreateTextField(CAttributes()
        .Set("name",    "TextField2")
        .Set("x1",      "5%")
        .Set("y1",      "80%")
        .Set("width",   "85%")
        .Set("height",  "15%")
        .Set("text", "Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda.")
        .Set("fontSize",    "medium")
        .Set("fontColour",  COLOUR_BLUE)
        .Set("backgroundColour", COLOUR_WHITE)
        .Set("fontStyle",   "bold")
        .Set("fontAlignH",  "middle"));

    scrollView->AddChild(textfield2);

    CImagePtr image = CreateImage(CAttributes()
        .Set("name",    "Image")
        .Set("x1",      "15%")
        .Set("y1",      "105%")
        .Set("width",   (int)(0.7f * screenWidth))
        .Set("height",  (int)(0.7f * screenWidth))
        .Set("image",   "test1.png")
        .Set("backgroundColour", COLOUR_PAGE)
        .Set("alpha",   0.75f));

    scrollView->AddChild(image);

    return result;
}

CViewPtr CreateTablesView()
{
    CViewPtr result = CreateTabPageView();

    Handler& handler = *Handler::s_Handler;

    Handler::s_Handler->tableItems.AddString(CString("January"));
    Handler::s_Handler->tableItems.AddString(CString("February"));
    Handler::s_Handler->tableItems.AddString(CString("March"));
    Handler::s_Handler->tableItems.AddString(CString("April"));
    Handler::s_Handler->tableItems.AddString(CString("May"));
    Handler::s_Handler->tableItems.AddString(CString("June"));
    Handler::s_Handler->tableItems.AddString(CString("July"));
    Handler::s_Handler->tableItems.AddString(CString("August"));
    Handler::s_Handler->tableItems.AddString(CString("September"));
    Handler::s_Handler->tableItems.AddString(CString("October"));
    Handler::s_Handler->tableItems.AddString(CString("November"));
    Handler::s_Handler->tableItems.AddString(CString("December"));

    CCheckBoxPtr checkBox = CreateCheckBox(CAttributes()
        .Set("name",    "CheckBoxTable1")
        .Set("x1",      "30%")
        .Set("y1",      20)
        .Set("check",   false)
        );
    result->AddChild(checkBox);

    CButtonPtr button = CreateButton(CAttributes()
        .Set("name",    "ButtonTable1")
        .Set("x1",      0)
        .Set("y1",      20)
        .Set("width",   "30%")
        .Set("caption", "Button 1")
        );
    result->AddChild(button);

    result->AddChild(CreateLabelsViewLabel("22%", "TableLabel", "This Year", "left"));

    handler.table = CreateTable(CAttributes()
        .Set("name",    "Table1")
        .Set("x1",      0)
        .Set("y1",      "27%")
        .Set("width",   "100%")
        .Set("height",  "45%")
        );

    handler.table->SetEventHandler("selectrow", &handler, &Handler::onSelectTable);

    for (int i = 0 ; i < 5; i++)
        handler.AddTableRow();

    result->AddChild(handler.table);

    CButtonPtr buttonAdd = CreateButton(CAttributes()
        .Set("name",    "addRow")
        .Set("caption", "add row")
        .Set("x1",      "1%")
        .Set("y1",      "82%")
        .Set("width",   "25%")
        .Set("height",  "14%")
        .Set("fontSize", "medium"));
    buttonAdd->SetEventHandler("click", &handler, &Handler::onAddRowClick);

    result->AddChild(buttonAdd);

    handler.tableStatusLabel = CreateLabel(CAttributes()
        .Set("name",    "TableStatusLabel")
        .Set("x1",      "28%")
        .Set("y1",      "82%")
        .Set("width",   "48%")
        .Set("height",  "6%")
        .Set("caption", "")
        .Set("fontSize",    "x-large")
        .Set("fontColour", COLOUR_BLACK)
        .Set("fontAlignH", "centre")
        .Set("fontAlignV", "middle"));
    result->AddChild(handler.tableStatusLabel);

    CButtonPtr buttonRemove = CreateButton(CAttributes()
        .Set("name",    "delRow")
        .Set("caption", "del row")
        .Set("x1",      "74%")
        .Set("y1",      "82%")
        .Set("width",   "25%")
        .Set("height",  "14%")
        .Set("fontSize", "medium"));
    buttonRemove->SetEventHandler("click", &handler, &Handler::onRemoveRowClick);

    result->AddChild(buttonRemove);

    return result;
}

CViewPtr CreateMisc2View()
{
    CViewPtr result = CreateTabPageView();

    Handler& handler = *Handler::s_Handler;

    CNavigationBarPtr navBar = CreateNavigationBar(CAttributes()
        .Set("name",    "navBar")
        .Set("title",   "3 of 5")
        .Set("leftButtonCaption",  "Prev")
        .Set("rightButtonCaption", "Next")
        .Set("x1",      0)
        .Set("y1",      0)
        .Set("width",   "100%")
        .Set("height",  "10%"));

    navBar->SetEventHandler("clicknavigationbar", &handler, &Handler::onNavBarClick);

    result->AddChild(navBar);

    Handler::s_Handler->listBoxItems.AddString(CString("Monday"));
    Handler::s_Handler->listBoxItems.AddString(CString("Tuesday"));
    Handler::s_Handler->listBoxItems.AddString(CString("Wednesday"));
    Handler::s_Handler->listBoxItems.AddString(CString("Thursday"));
    Handler::s_Handler->listBoxItems.AddString(CString("Friday"));
    Handler::s_Handler->listBoxItems.AddString(CString("Saturday"));
    Handler::s_Handler->listBoxItems.AddString(CString("Sunday"));

    CListBoxPtr listBox = CreateListBox(CAttributes()
        .Set("name",    "Listbox sample")
        .Set("x1",      0)
        .Set("y1",      "10%")
        .Set("width",   "100%")
        .Set("height",  "50%")
        .Set("listBoxItems", Handler::s_Handler->listBoxItems));

    handler.listBox = listBox;

    listBox->SetEventHandler("selectitem", &handler, &Handler::onListBoxItemSelected);

    result->AddChild(listBox);

    handler.labelPicker = CreateLabel(CAttributes()
        .Set("name",    "LabelPicker")
        .Set("x1",      "5%")
        .Set("y1",      "64%")
        .Set("width",   "90%")
        .Set("height",  "9%")
        .Set("caption", "ListBox item selected = ?")
        .Set("fontColour", COLOUR_BLACK)
        .Set("fontAlignH", "left")
        .Set("fontAlignV", "middle"));

    result->AddChild(handler.labelPicker);

    CButtonPtr dialogButton = CreateButton(CAttributes()
        .Set("name",    "AlertDialogButton")
        .Set("caption", "Fire Alert Dialog")
        .Set("x1",      "14%")
        .Set("y1",      "78%")
        .Set("width",   "75%")
        .Set("height",  "10%")
        .Set("fontSize", "medium"));

    dialogButton->SetEventHandler("click", &handler, &Handler::onDialogAlertButtonClick);
    result->AddChild(dialogButton);

    CAlertDialogPtr dialog = CreateAlertDialog(CAttributes()
        .Set("name",                    "AlertDialog")
        .Set("title",                   "s3eNUIAlertDialog")
        .Set("message",                 "Would you like to close this dialogue?")
        .Set("positiveButtonCaption",   "Yes")
        .Set("negativeButtonCaption",   "No")
        .Set("neutralButtonCaption",    "Maybe"));

    handler.alertDialog = dialog;
    handler.alertDialog->SetEventHandler("dismissalertdialog", &handler, &Handler::onAlertButtonClick);

    return result;
}

CViewPtr CreateTabBarView()
{
    Handler& handler = *Handler::s_Handler;

    CViewPtr result = CreateView(CAttributes()
        .Set("backgroundColour", COLOUR_LIGHTGREY));

    CTabBarPtr tabBar = CreateTabBar(CAttributes()
        .Set("name",    "MyTabBar")
        .Set("x1",      "0%")
        .Set("width",   "100%")
        .Set("alignH",  "bottom")
        .Set("height",  "10%")
        .Set("selected", 0)
        );

    tabBar->SetEventHandler("selecttab", &handler, &Handler::onSelectTab);

    CTabBarItemPtr item = CreateTabBarItem(CAttributes()
        .Set("name",    "TabBarItem1")
        .Set("caption", "Labels")
        .Set("normal.image", "ico_labels_off.png")
        .Set("pressed.image", "ico_labels_on.png"));

    tabBar->AddChild(item);

    item = CreateTabBarItem(CAttributes()
        .Set("name",    "TabBarItem2")
        .Set("caption", "Buttons")
        .Set("normal.image", "ico_buttons_off.png")
        .Set("pressed.image", "ico_buttons_on.png"));

    tabBar->AddChild(item);

    item = CreateTabBarItem(CAttributes()
        .Set("name",    "TabBarItem3")
        .Set("caption", "Table")
        .Set("normal.image", "ico_table_off.png")
        .Set("pressed.image", "ico_table_on.png"));

    tabBar->AddChild(item);

    item = CreateTabBarItem(CAttributes()
        .Set("name",    "TabBarItem4")
        .Set("caption", "Misc 1")
        .Set("normal.image", "ico_misc_off.png")
        .Set("pressed.image", "ico_misc_on.png"));

    tabBar->AddChild(item);

    item = CreateTabBarItem(CAttributes()
        .Set("name",    "TabBarItem5")
        .Set("caption", "Misc 2")
        .Set("normal.image", "ico_misc_off.png")
        .Set("pressed.image", "ico_misc_on.png"));

    tabBar->AddChild(item);

    result->AddChild(tabBar);

    return result;
}

int main()
{
    CAppPtr app = CreateApp();
    CWindowPtr window = CreateWindow();

    Handler handler;

    handler.app = app;

    app->AddWindow(window);

    CViewPtr containerView = CreateView();

    CViewPtr tabBarView = CreateTabBarView();

    CViewPtr labelsView = CreateLabelsView();

    CViewPtr buttonsView = CreateButtonsView();

    CViewPtr tablesView = CreateTablesView();

    CViewPtr misc1View = CreateMisc1View();

    CViewPtr misc2View = CreateMisc2View();

    buttonsView->SetAttribute("visible", false);
    tablesView->SetAttribute("visible", false);
    misc1View->SetAttribute("visible", false);
    misc2View->SetAttribute("visible", false);

    misc1View->SetEventHandler("clickview", &handler, &Handler::OnMisc1ViewClick);

    containerView->AddChild(tabBarView);
    containerView->AddChild(labelsView);
    containerView->AddChild(buttonsView);
    containerView->AddChild(tablesView);
    containerView->AddChild(misc1View);
    containerView->AddChild(misc2View);
    window->SetChild(containerView);

    CViewPtr views[] = {labelsView, buttonsView, tablesView, misc1View, misc2View};
    handler.views = views;

    app->ShowWindow(window);

    app->Run();

    return 0;
}
