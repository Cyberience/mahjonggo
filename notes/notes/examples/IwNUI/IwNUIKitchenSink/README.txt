#category: User Interface
IwNUIKitchenSink
================

IwNUIKitchenSink makes use of the IwNUI API which is used to implement native
user interfaces.  The example implements several of the UI components in IwNUI.
