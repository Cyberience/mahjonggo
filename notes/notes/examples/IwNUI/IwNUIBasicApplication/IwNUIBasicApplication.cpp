/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

//-----------------------------------------------------------------------------
// ExampleIwNUIBasicApplication
//-----------------------------------------------------------------------------

/**
 * @page ExampleIwNUIBasicApplication IwNUI Basic Application Example
 * The following example demonstrates a basic IwNUI application.
 *
 * @image html IwNUIBasicApplicationImage.png
 *
 * @include IwNUIBasicApplication.cpp
 *
 */

#include "IwNUI.h"

using namespace IwNUI;

bool OnButton1Click(void* data, CButton* button)
{
    button->SetAttribute("caption", "Clicked!");
    return true;
}

int main()
{
    CAppPtr app = CreateApp();
    CWindowPtr window = CreateWindow();

    app->AddWindow(window);

    CViewPtr view = CreateView("canvas");

    CButtonPtr button1 = CreateButton(CAttributes()
        .Set("name",    "Button1")
        .Set("caption", "Hello World!")
        .Set("x1",      "10")
        .Set("y1",      "10"));

    button1->SetEventHandler("click", (void*)NULL, &OnButton1Click);

    view->AddChild(button1);

    window->SetChild(view);

    app->ShowWindow(window);

    app->Run();

    return 0;
}
