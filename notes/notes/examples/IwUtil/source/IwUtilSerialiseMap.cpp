/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
// ExampleIwUtilSerialiseMap
//-----------------------------------------------------------------------------

/**
 * Illustrates the IwSerialise API for serialising "mapped" data.
 *
 * The important constructs in this example are:
 * IwSerialiseMappedData()
 */

// for screen rendering
#include "IwGx.h"
#include "IwGxPrint.h"
#include "ExamplesMain.h"

// structure containing various types
//-----------------------------------------------------------------------------
struct CBundle
{
    int16   m_Int16;
    uint32  m_UInt32;
    float   m_Float;
    char    m_Chars[8];

    // setup the structure
    void Init(uint8 number)
    {
        m_UInt32 = m_Int16 = number;
        m_Float = number;
        memset(m_Chars, 48 + number, 7);
        m_Chars[7] = 0;
    }
};
//-----------------------------------------------------------------------------

// an array of CBundle's that will be serialised
#define NUM_BUNDLES 3
CBundle *s_Bundles = NULL;

//-----------------------------------------------------------------------------
void CreateBundles()
{
    if (s_Bundles)
        delete[] s_Bundles;

    s_Bundles = new CBundle[NUM_BUNDLES];
    memset(s_Bundles, 0, sizeof(CBundle)*NUM_BUNDLES);
}
//-----------------------------------------------------------------------------
void ExampleInit()
{
    // initialise GX
    IwGxInit();

    // no lighting
    IwGxLightingOff();

    // create and fill the bundles with arb data:
    CreateBundles();

    for (int i = 0; i < NUM_BUNDLES; i++)
        s_Bundles[i].Init(i);

    // serialise bundles out to file:

    IwSerialiseOpen("bundles.file", false);

    // describe the structure of CBundle
    uint16 map[] =
    {
        IW_TYPE_UINT16,
        IW_TYPE_PAD(IW_TYPE_UINT16),
        IW_TYPE_UINT32,
        IW_TYPE_FLOAT,
        IW_TYPE_ARRAY(IW_TYPE_CHAR, 8),
        IW_TYPE_NONE,
    };

    // serialise out using the above-defined structure
    IwSerialiseMappedData(map, s_Bundles, NUM_BUNDLES, sizeof(CBundle));

    IwSerialiseClose();

    // create and don't init bundles
    CreateBundles();

    // serialise bundles in from file:

    IwSerialiseOpen("bundles.file", true);

    // serialise in using the above-defined structure
    IwSerialiseMappedData(map, s_Bundles, NUM_BUNDLES, sizeof(CBundle));

    IwSerialiseClose();

    EnableScaling(true);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    delete[] s_Bundles;
    s_Bundles = NULL;

    // terminate GX
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    int fontScale = IwGxPrintGetScale();

    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // display the bundles:

    char display[1000], output[200];
    display[0] = 0;

    strcat(display, "Bundles after map serialising\n");

    for (int i = 0; i < NUM_BUNDLES; i++)
    {
        sprintf(output, "m_Int16: %i\nm_UInt32: %u\nm_Float: %0.2f\nm_Chars: %s\n\n",
            s_Bundles[i].m_Int16, s_Bundles[i].m_UInt32, s_Bundles[i].m_Float, s_Bundles[i].m_Chars);
        strcat(display, output);
    }

    // display
    IwGxPrintString(0, 30 * fontScale, "Serialising Mapped Data");
    IwGxPrintString(0, 50 * fontScale, display);

    // Flush and swap
    IwGxFlush();
    IwGxSwapBuffers();
}
//-----------------------------------------------------------------------------
