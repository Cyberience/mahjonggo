/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwUtilClassFactory Class Factory Example
 *
 * The following example creates classes from the class factory.
 *
 * The main functions used to achieve this are:
 *
 *  - <code>IW_CLASS_FACTORY() </code>
 *  - <code>IW_CLASS_REGISTER()</code>
 *  - <code>IwClassFactoryCreate()</code>
 *
 * The example defines two classes <code>MyClassOne</code> and <code>MyClassTwo</code> using
 * the  <code>IW_CLASS_FACTORY()</code> function and then registers
 * them with the class factory using the <code>IW_CLASS_REGISTER()</code> function.
 * The classes are then instantiated using the <code>IwClassFactoryCreate()</code> function.
 * Member functions from both instances are then called displaying
 * the class name and the hash string equivalent.
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwUtilCFactoryImage.png
 *
 * @note For more information on class factories,
 * see the @ref classfactory "Class Factory" section in the <i>IwUtil API Documention</i>.
 *
 * @include IwUtilCFactory.cpp
 */

//-----------------------------------------------------------------------------

// for screen rendering
#include "IwGx.h"
#include "IwGxPrint.h"
#include "ExamplesMain.h"

// dummy classes to be put in the class factory

//-----------------------------------------------------------------------------
class MyClassOne
{
private:
    CIwStringS m_Name;  // arbitrary member

public:
    MyClassOne()
        : m_Name("MyClassOne") {}

    // display state
    void Render(int32 x0, int32 y0)
    {
        int fontScale = IwGxPrintGetScale();
        char output[100];
        sprintf(output, "%s = %u", m_Name.c_str(), IwHashString(m_Name));
        IwGxPrintString(x0 * fontScale, y0 * fontScale, output);
    }
};
//-----------------------------------------------------------------------------

// define the factory function for MyClassOne
IW_CLASS_FACTORY(MyClassOne);

//-----------------------------------------------------------------------------
class MyClassTwo
{
private:
    CIwStringS m_Name;  // arbitrary member

public:
    MyClassTwo()
        : m_Name("MyClassTwo") {}

    // display state
    void Render(int32 x0, int32 y0)
    {
        int fontScale = IwGxPrintGetScale();
        char output[100];
        sprintf(output, "%s = %u", m_Name.c_str(), IwHashString(m_Name));
        IwGxPrintString(x0 * fontScale, y0 * fontScale, output);
    }
};
//-----------------------------------------------------------------------------

// define the factory function for MyClassTwo
IW_CLASS_FACTORY(MyClassTwo);

MyClassOne* myclassone;
MyClassTwo* myclasstwo;

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // initialise GX
    IwGxInit();

    // add classes to the class factory
    IW_CLASS_REGISTER(MyClassOne);
    IW_CLASS_REGISTER(MyClassTwo);

    // create instances of the classes from the class factory
    myclassone = (MyClassOne*)IwClassFactoryCreate("MyClassOne");
    myclasstwo = (MyClassTwo*)IwClassFactoryCreate("MyClassTwo");

    EnableScaling(true);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // terminate GX
    IwGxTerminate();

    // clean up
    delete myclassone;
    delete myclasstwo;
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    int fontScale = IwGxPrintGetScale();

    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Start with no lighting
    IwGxLightingOff();

    IwGxPrintString(0, 50 * fontScale, "Creating instance through class factories");

    // display object states
    myclassone->Render(0, 80);
    myclasstwo->Render(0, 90);

    // Flush and swap
    IwGxFlush();
    IwGxSwapBuffers();
}
