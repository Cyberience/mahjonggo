/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwUtilRandomNumber Random Number Example
 *
 * The following example generates random numbers. The important
 * functions in this example are:
 *  - IwRandSeed()
 *  - s3eTimerGetMs()
 *  - IwRandMinMax()
 *
 * @note For more information on generating random numbers, see
 * the @ref random "Random" section of the <i>IwUtil API Documentation</i>.
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwUtilRandomImage.png
 *
 * @include IwUtilRandom.cpp
 */

//-----------------------------------------------------------------------------

// for screen rendering
#include "IwGx.h"
#include "IwGxPrint.h"
#include "ExamplesMain.h"

#include "IwRandom.h"

// used for random seeding
#include "s3eTimer.h"

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // initialise GX
    IwGxInit();

    EnableScaling(true);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // terminate GX
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Start with no lighting
    IwGxLightingOff();

    IwGxFlush();

    char display[1000], output[100];
    display[0] = 0;

    // set the seed to a constant - i.e. same sequence every time
    IwRandSeed(-1);

    // print out a random number in 0..0x7fff
    sprintf(output, "Constant seed gives: \n%d (in [0,32767])\n\n", IwRand());
    strcat(display, output);

    // set the seed to time elapsed - i.e. different sequence every time
    IwRandSeed((int32)s3eTimerGetMs());

    // print out a random number in 10..20
    sprintf(output, "Time-based seed gives: \n%d (in [10,20])", IwRandMinMax(10, 20));
    strcat(display, output);

    IwGxPrintString(10, 56, display);

    // Swap buffers
    IwGxSwapBuffers();
}
