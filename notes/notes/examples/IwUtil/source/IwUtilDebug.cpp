/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwUtilDebug Debug Example
 *
 * The following example creates trace and assert messages.
 * The important functions in this example are:
 *
 *  - IwAssertMsg()
 *  - IwTrace()
 *
 * On startup this example will trace the message "MYAPP: val is set to 0".
 * Trace output will appear in the dev studio's program output window.
 * All of the Studio modules will trace information in their own channels.
 * You can disable channels using the app.icf file -
 * try changing the line "MYAPP=1" in the [Trace] section to "MYAPP=0" and notice
 * that the MYAPP message no longer appears.
 * Trace channels default to on.
 *
 * The application will then present a menu which can be used to trigger errors
 * and assertions.  The assertion will assert that val == 0 and val == 2 in
 * turn. Since val has been set to 1 in the ExampleUpdate function, the
 * asserts will fail and pop up warning dialogues in turn:
 *
 * Select 'abort' to stop at the line where the assert failed.
 * Select 'retry' to continue. The other assert will immediately fire again as the program loops.
 * Select 'ignore' to continue, ignoring further failings of the assert. Now only the other assert
 * will fire. If you ignore the other one as well, then neither assert will fire and the program
 * will spin silently.
 *
 * Asserts are used to catch unexpected runtime behaviour. They are not compiled into release builds.
 * Studio modules will also assert when unexpected runtime behaviour occurs.
 * Asserts can be disabled like tracing - using an [Assert] icf section.
 *
 * @note For more information on using the IwUtil functionality
 * to create trace and assert messages, see the @ref debug "Debug" section of
 * the <i>IwUtil API Documentation</i>.
 *
 * @include IwUtilDebug.cpp
 */

#include "IwDebug.h"
#include "IwGx.h"
#include "ExamplesMain.h"

// arbitrary int
static int val = 0;

void ExampleInit()
{
    // trace to the MYAPP trace channel
    IwTrace(MYAPP, ("val is set to %i", val));
    int w = IwGxGetScreenWidth();
    AddButton("1: Trigger IwError", 5, 100, (w / 2) - 10, 20, s3eKey1);
    AddButton("2: Trigger IwAssert", 5, 150, (w / 2) - 10, 20, s3eKey2);

    EnableScaling(true);
}

void ExampleShutDown()
{
}

bool ExampleUpdate()
{
    val = 1;
    if (CheckButton("2: Trigger IwAssert") & S3E_KEY_STATE_PRESSED)
    {
        // cause an assertion on the MYAPP assert channel
        IwAssertMsg(MYAPP, val == 0, ("Please don't set val to %i - it's best set to 0", val));
        IwAssertMsg(MYAPP, val == 2, ("Please don't set val to %i - it's best set to 2", val));
    }
    else if (CheckButton("1: Trigger IwError") & S3E_KEY_STATE_PRESSED)
    {
        IwError(("IwError message will show up in both debug and release builds"));
    }
    return true;
}

void ExampleRender()
{

    // we need to flush/swap here, to drain buffers filled by the example framework.
    IwGxFlush();
    IwGxSwapBuffers();
}
