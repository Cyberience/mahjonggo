/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwUtilSerialising Serialising Example
 *
 * The following example demonstrates the IwUtil serialisation
 * functionality. The example serialises a single managed object as
 * well as a list of managed objects.
 *
 * The important functions in this example are:
 *
 *  - CIwManaged::IwSerialiseOpen()
 *  - CIwManaged::IwSerialiseClose()
 *  - CIwManaged::Serialise()
 *
 * The managed objects are declared, and a file is open to serialise
 * out to using the IwSerialiseOpen() function. The declared serialisation
 * function is called to serialise back. The <code>IwSerialiseClose()</code>function
 * is then called. The same actions are then carried out on a managed
 * list.
 *
 * @note For more information on Serialisation,
 * see the @ref serialisation "Serialisation"
 * section in the <i>IwUtil API Documention</i>.
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwUtilSerialiseImage.png
 *
 * @include IwUtilSerialise.cpp
 */

// for screen rendering
#include "IwGx.h"
#include "IwGxPrint.h"
#include "ExamplesMain.h"

// simple managed class that contains an int32
//-----------------------------------------------------------------------------
class CHappy : public CIwManaged
{
public:
    IW_MANAGED_DECLARE(CHappy);

    CHappy()
        : m_Age(0) {}

    // simple serialisation - just an int32 member
    void Serialise()
    {
        IwSerialiseInt32(m_Age);
    }

    int32       m_Age;
};
//-----------------------------------------------------------------------------

// define class name function
IW_MANAGED_IMPLEMENT(CHappy);

// managed class that contains another managed class
//-----------------------------------------------------------------------------
class CBored : public CIwManaged
{
public:
    IW_MANAGED_DECLARE(CBored);

    CBored()
    {
        m_Happy = new CHappy;
    }

    ~CBored()
    {
        if (m_Happy)
            delete m_Happy;
    }

    // more complicated serialisation - need to serialise a member object
    void Serialise()
    {
        uint8 isObject;

        if (IwSerialiseIsWriting())
        // writing
        {
            // record whether m_Happy exists
            isObject = m_Happy ? 1 : 0;
            IwSerialiseUInt8(isObject);

            // serialise out m_Happy if it exists
            if (m_Happy)
                m_Happy->Serialise();
        }
        else
        // reading
        {
            IwSerialiseUInt8(isObject);

            // if an object was written out then read it in
            if (isObject)
            {
                // if m_Happy already exists then delete it
                if (m_Happy)
                    delete m_Happy;

                m_Happy = new CHappy;

                // serialise in m_Happy
                m_Happy->Serialise();
            }
        }
    }

    CHappy  *m_Happy;
};
//-----------------------------------------------------------------------------

// define class name function
IW_MANAGED_IMPLEMENT(CBored);

// define class factory functions
IW_CLASS_FACTORY(CHappy);
IW_CLASS_FACTORY(CBored);

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // initialise GX
    IwGxInit();

    // register the classes with the class factory - this is required for serialisation of managed lists
    IW_CLASS_REGISTER(CHappy);
    IW_CLASS_REGISTER(CBored);

    EnableScaling(true);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // terminate GX
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    int fontScale = IwGxPrintGetScale();

    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Start with no lighting
    IwGxLightingOff();

    char display[1000], output[100];
    display[0] = 0;

    // Serialising (out and then in) of a single managed object

    // create object
    CBored *obj0 = new CBored;
    obj0->m_Happy->m_Age = 99;

    strcat(display, "Before serialising in:\n");

    // display object state
    sprintf(output, "obj0->m_Happy->m_Age = %d\n", obj0->m_Happy->m_Age);
    strcat(display, output);

    // open file for serialising out
    IwSerialiseOpen("objects.bin", false);

    // serialise the object out
    obj0->Serialise();

    // close the file
    IwSerialiseClose();

    // open file for serialising in
    IwSerialiseOpen("objects.bin", true);

    // serialise the object in
    obj0->Serialise();

    // close the file
    IwSerialiseClose();

    strcat(display, "\nAfter serialising in:\n");

    // display object state
    sprintf(output, "obj0->m_Happy->m_Age = %d\n", obj0->m_Happy->m_Age);
    strcat(display, output);

    // Serialising (out and then in) of a list of managed objects
    CIwManagedList objects;

    // create stand alone CHappy object
    CHappy *obj1 = new CHappy;
    obj1->m_Age = -14;

    // add both objects to the list
    objects.Add(obj0);
    objects.Add(obj1);

    strcat(display, "\nBefore serialising in:\n");

    // display objects' states

    sprintf(output, "obj0->m_Happy->m_Age = %d\n", ((CBored *)objects[0])->m_Happy->m_Age);
    strcat(display, output);

    sprintf(output, "obj1->m_Happy->m_Age = %d\n", ((CHappy *)objects[1])->m_Age);
    strcat(display, output);

    // open file for serialising out
    IwSerialiseOpen("objects.bin", false);

    // when serialising out a managed list, hashes of the objects' class names are written
    objects.Serialise();

    // close the file
    IwSerialiseClose();

    // open file for serialising in
    IwSerialiseOpen("objects.bin", true);

    // when serialising in a managed list, the above-mentioned hashes are used to create objects and serialise them in
    objects.Serialise();

    // close the file
    IwSerialiseClose();

    strcat(display, "\nAfter serialising in:\n");

    // display objects' states

    sprintf(output, "obj0->m_Happy->m_Age = %d\n", ((CBored *)objects[0])->m_Happy->m_Age);
    strcat(display, output);

    sprintf(output, "obj1->m_Happy->m_Age = %d\n", ((CHappy *)objects[1])->m_Age);
    strcat(display, output);

    // display
    IwGxPrintString(0, 30 * fontScale, "Using the Serialising API");
    IwGxPrintString(0, 50 * fontScale, display);

    //clear serialised instances
    objects.Delete();

    //clear original instances
    delete obj0;
    delete obj1;

    // Flush and swap
    IwGxFlush();
    IwGxSwapBuffers();
}
