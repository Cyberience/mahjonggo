/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */


/**
 * @page ExampleIwUtilTextParsing Text Parsing Example
 *
 * The following example demonstrates the IwUtil text parsing
 * functionality. The example parses the file ExamplesIwUtil_parser.itx,
 * creating a singleton object that contains another object. The important
 * functions in this example are:
 *
 *  - <code>IW_MANAGED_DECLARE()</code>
 *  - <code>IW_MANAGED_IMPLEMENT()</code>
 *  - CIwManaged::ParseAttribute()
 *  - CIwManaged::ParseClose()
 *  - <code>IW_CLASS_REGISTER()</code>
 *  - <code>IwGetTextParserITX()</code>
 *  - CIwTextParserITX::ParseFile()
 *
 * The file that is parsed "IwUtil_parser.itx" is made
 * up of the following code:
 *
 * @code
 * CSingleParent
 *  * {
 *  *   name    lonely
 *  *   CChild
 *  *   {
 *  *       age         72
 *  *       firstname   picklesworth
 *  *   }
 *  * }
 * @endcode
 *
 * Both example classes are registered with the class factory
 * so they are recognised when parsing. This is done using the <code>IW_CLASS_REGISTER()</code> function.
 * The text parser is then instantiated, using and passed the ITX file
 * for parsing.
 *
 * The parser will first encounters a class name which is checked
 * by the class factory to see if it is a registered class, the class
 * will be instantiated. The following tokens are passed to the derived
 * ParseAttribute() function, setting each value as it is encountered.The
 * CChild class has two values that are populated by the ITX file m_FirstName
 * and m_Age.
 *
 * @note For more information on Text Parsing,
 * see the @ref textparsing "Text Parsing"
 * section in the <i>IwUtil API Documention</i>.
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwUtilParserImage.png
 *
 * @include IwUtilParser.cpp
 */

#include "IwTextParserITX.h"

// for screen rendering
#include "IwGx.h"
#include "IwGxPrint.h"
#include "ExamplesMain.h"

// simple managed class - for this example, objects of this type
// can only exist within an object of type CSingleParent (see below)
//-----------------------------------------------------------------------------
class CChild : public CIwManaged
{
public:
    IW_MANAGED_DECLARE(CChild);

    CChild()
        : m_Age(0) {}

    // function invoked by the text parser when parsing attributes for objects of this type
    virtual bool ParseAttribute(CIwTextParserITX *pParser, const char *pAttrName);

    // function invoked by the text parser when the object definition end is encountered
    virtual void ParseClose(CIwTextParserITX *pParser);

    CIwStringS  m_FirstName;
    int32       m_Age;
};
//-----------------------------------------------------------------------------

// define class name function
IW_MANAGED_IMPLEMENT(CChild);

// managed class (singleton) that contains another managed class (CChild)
//-----------------------------------------------------------------------------
class CSingleParent : public CIwManaged
{
private:
    // singleton pointer
    static CSingleParent    *s_Singleton;

public:
    IW_MANAGED_DECLARE(CSingleParent);

    CSingleParent()
        : m_Child(NULL)
    {
        s_Singleton = this;
    }

    ~CSingleParent()
    {
        if (m_Child)
            delete m_Child;

        s_Singleton = NULL;
    }

    // public access to the singleton object
    static CSingleParent *Get()
    {
        return s_Singleton;
    }

    // CIwManaged::ParseAttribute is not overridden here - i.e. the only permitted attribute is 'name'

    // function invoked by the text parser when the object definition end is encountered
    virtual void ParseClose(CIwTextParserITX *pParser);

    CChild  *m_Child;

};
//-----------------------------------------------------------------------------

// define class name function
IW_MANAGED_IMPLEMENT(CSingleParent);

// CChild implementation

// function invoked by the text parser when parsing attributes for objects of this type
//-----------------------------------------------------------------------------
bool CChild::ParseAttribute(CIwTextParserITX* pParser, const char* pAttrName)
{
    char line[0x100];
    if (!strcmp(pAttrName, "firstname"))
    {
        // read firstname
        pParser->ReadString(line, 0x100);
        m_FirstName = line;
    }
    else if (!strcmp(pAttrName, "age"))
    {
        // read age
        int32 age;
        pParser->ReadInt32(&age);
        m_Age = age;
    }
    else
    // call base function
        return CIwManaged::ParseAttribute(pParser, pAttrName);

    return true;
}
// function invoked by the text parser when the object definition end is encountered
//-----------------------------------------------------------------------------
void CChild::ParseClose(CIwTextParserITX* pParser)
{
    // we know that this object has been parsed in the context of a CSingleParent object
    CSingleParent *parent = (CSingleParent *)IwGetTextParserITX()->GetObject(-1);

    // set the child of parent to this
    parent->m_Child = this;
}

// CSingleParent implementation

// function invoked by the text parser when the object definition end is encountered
//-----------------------------------------------------------------------------
void CSingleParent::ParseClose(CIwTextParserITX* pParser)
{
    // if a CSingleParent block is encountered in the ITX file then set the singleton

    if (s_Singleton && (s_Singleton != this) )
        delete s_Singleton;

    s_Singleton = this;
}
//-----------------------------------------------------------------------------

// define class factory functions
IW_CLASS_FACTORY(CChild);
IW_CLASS_FACTORY(CSingleParent);

// CSingleParent singleton
CSingleParent *CSingleParent::s_Singleton = NULL;

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // initialise GX
    IwGxInit();

    // register the classes with the class factory - this is required for parsing
    IW_CLASS_REGISTER(CChild);
    IW_CLASS_REGISTER(CSingleParent);

    // create the text parser
    new CIwTextParserITX;

    // TIM TEST
    IwGetTextParserITX()->SetAssertOnUnrecognisedTokens(true);

    // parse the file
    IwGetTextParserITX()->ParseFile("IwUtilParser.itx");

    EnableScaling(true);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // clear up data
    delete CSingleParent::Get();

    // destroy the text parser
    delete IwGetTextParserITX();

    // terminate GX
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    int fontScale = IwGxPrintGetScale();

    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Start with no lighting
    IwGxLightingOff();

    char display[1000], output[100];
    display[0] = 0;

    // the singleton exists at this point since it has been parsed
    CSingleParent *parent = CSingleParent::Get();

    // display the state of the singleton

#ifndef IW_DEBUG
    sprintf(display, "CSingleParent object name will be an empty string in release builds\n");
#endif

    sprintf(output, "CSingleParent object\nnamed %s:\n", parent->DebugGetName());
    strcat(display, output);

    sprintf(output, "m_Child->m_Age = %d,\nm_Child->m_FirstName = %s\n", parent->m_Child->m_Age, parent->m_Child->m_FirstName.c_str());
    strcat(display, output);

    // display
    IwGxPrintString(0, 50 * fontScale, "Using the Text Parser");
    IwGxPrintString(0, 80 * fontScale, display);

    // Flush and swap
    IwGxFlush();
    IwGxSwapBuffers();
}
