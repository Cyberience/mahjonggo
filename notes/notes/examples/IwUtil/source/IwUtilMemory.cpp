/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */


/**
 * @page ExampleIwUtilMemoryUtility Memory Utility Example
 *
 * The following example demonstrates how to use the IwUtil memory
 * bucket system. The example allows you to create up to four memory
 * buckets, allocate memory from them and then delete those memory
 * buckets.
 *
 * The main functions used to achieve this are:
 *  - <code>IwUtilInit()</code>
 *  - <code>IwMemBucketCreate()</code>
 *  - <code>IwMemBucketSet()</code>
 *  - <code>IwMemBucketFree()</code>
 *
 * The example initialises the memory bucket functionality, as
 * well as all other IwUtil functionality, using the IwUtilInit() function.
 * Memory buckets are created using the the IwMemBucketCreate() function.
 * It is possible to swap between buckets once they are created using
 * the IwMemBucketSet() function. Finally, you can delete the buckets
 * you have created using the IwMemBucketFree() function.
 *
 * You can optimise the use of memory buckets using the following
 * functions:
 *  - <code>IwMemBucketGetUsed()</code>
 *  - <code>IwMemBucketGetFree()</code>
 *
 * The following controls are provided for the example:
 *  - Press left/right to cycle through the available buckets
 *    and the main heap.
 *  - Press Enter to create a new bucket from the heap memory.
 *  - Press up/down to call new/delete for the selected bucket/heap.
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwUtilMemoryImage.png
 *
 * @note For more information on Memory Utility,
 * see the @ref memorymanagement "Memory Management" section
 * in the <i>IwUtil API Documention</i>.
 *
 * @include IwUtilMemory.cpp
 */

#include "IwUtil.h"
#include "IwMemory.h"
#include "IwGx.h"
#include "IwGxPrint.h"
#include "ExamplesMain.h"

// Enumerate buckets
enum
{
    FIXED = IW_MEM_BUCKET_ID_USER,
    GUI,
    LEVEL,
    SOUND,
};


static const uint32 MEM_SIZE_32     = 32;       // 32
static const uint32 MEM_SIZE_128    = 128;      // 128
static const uint32 MEM_SIZE_512    = 512;      // 512
static const uint32 MEM_SIZE_1024   = 1024;     // 1024
static const uint32 MEM_SIZE_32K    = MEM_SIZE_32   * MEM_SIZE_1024; // 32k
static const uint32 MEM_SIZE_128K   = MEM_SIZE_128  * MEM_SIZE_1024; // 128k
static const uint32 MEM_SIZE_1024K  = MEM_SIZE_1024 * MEM_SIZE_1024; // 1024k
static const uint32 MEM_SIZE_512K   = MEM_SIZE_512  * MEM_SIZE_1024; // 512k
static const uint32 NUM_BUCKETS     = 4;
static const uint32 MAX_ALLOCATIONS = 10;

// Which is the active bucket in use
static uint32 s_ActiveBucket        = 0;

struct BucketInfo
{
    uint32      UniqueID;
    uint32      Capacity;
    const char* Name;
};

// Information about the bucket in use
static const BucketInfo s_BUCKET_INFO[NUM_BUCKETS] =
{
    { FIXED,    MEM_SIZE_128K,      "Fixed"},
    { GUI,      MEM_SIZE_1024 * 3,  "Gui" },
    { LEVEL,    MEM_SIZE_128K,      "Level" },
    { SOUND,    MEM_SIZE_32K,       "Sound" }
};

// Arrays containing the allocations made by each bucket.
static CIwArray<char*> s_Allocations[NUM_BUCKETS];

// Buffer containing a description of the last action taken by the user.
static char s_LastAction[256] = "\0";

static void CreateButtonsUI(int w, int h)
{
    // Create the UI layout
    AddButton("1: Allocate 32 bytes", 5, 100, (w / 2) - 10 , 20, s3eKey1);
    AddButton("2: Allocate 128 bytes", 5, 125, (w / 2) - 10, 20, s3eKey2);
    AddButton("3: Allocate 512 bytes", 5, 150, (w / 2) - 10, 20, s3eKey3);
    AddButton("4: Allocate 1024 bytes", 5, 175, (w / 2) - 10, 20, s3eKey4);
    AddButton("5: Free last allocation", 5, 200, (w / 2) - 10, 20, s3eKey5);
}

static void SurfaceChangedCallback()
{
    DeleteButtons();
    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise (indirectly) bucket system
    IwGxRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
    IwUtilInit();
    IwGxInit();

    // Create buckets
    for (uint32 currentBucket = 0; currentBucket < NUM_BUCKETS; ++currentBucket)
    {
        IwMemBucketCreate(s_BUCKET_INFO[currentBucket].UniqueID,
                          s_BUCKET_INFO[currentBucket].Name,
                          s_BUCKET_INFO[currentBucket].Capacity);

        // Reserve space for allocations in the array now whilst the system memory bucket is active.
        s_Allocations[currentBucket].reserve( MAX_ALLOCATIONS );
    }

    IwGxSetColClear( 0x00, 0x00, 0x00, 0xff );

    // Turn all lighting off
    IwGxLightingOff();

    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());

    EnableScaling(true);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // Free all user created memory buckets.
    for (uint32 currentBucket = 0; currentBucket < NUM_BUCKETS; ++currentBucket)
    {
        for (uint32 currentAlloc = 0; currentAlloc < s_Allocations[ currentBucket ].size(); ++currentAlloc)
        {
            // delete that last entry in the allocation array
            delete[]  s_Allocations[ currentBucket ][ currentAlloc ];
        }

        // Free the bucket now no allocations remain
        IwMemBucketFree(s_BUCKET_INFO[currentBucket].UniqueID);

        // Release storage for array
        s_Allocations[currentBucket].clear_optimised();
    }

    // Terminate (indirectly) bucket system
    IwGxTerminate();
    IwUtilTerminate();
    IwGxUnRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // Check if the user has selected a new memory bucket.
    if (CheckCursorState() == EXCURSOR_LEFT )
    {
        s_ActiveBucket = (s_ActiveBucket == 0) ? ( NUM_BUCKETS - 1 ) : ( s_ActiveBucket - 1 );
    }
    else if (CheckCursorState() == EXCURSOR_RIGHT )
    {
        s_ActiveBucket = ((s_ActiveBucket + 1) == NUM_BUCKETS) ? ( 0 ) : ( s_ActiveBucket + 1 );
    }


    // Set the active bucket before making any allocations.
    IwMemBucketPush( s_BUCKET_INFO[s_ActiveBucket].UniqueID );


    // Allow extra allocation only if we have not exceeded the maximum allowed for this bucket.
    if (MAX_ALLOCATIONS > s_Allocations[ s_ActiveBucket ].size() )
    {

        if (CheckButton("1: Allocate 32 bytes") & S3E_KEY_STATE_PRESSED)
        {
            s_Allocations[s_ActiveBucket].push_back(new char[MEM_SIZE_32]);

            sprintf(s_LastAction, "%u bytes allocated from %s", MEM_SIZE_32, s_BUCKET_INFO[s_ActiveBucket].Name);
        }
        else if (CheckButton("2: Allocate 128 bytes") & S3E_KEY_STATE_PRESSED )
        {
            s_Allocations[ s_ActiveBucket ].push_back( new char[ MEM_SIZE_128 ] );

            sprintf(s_LastAction, "%u bytes allocated from %s", MEM_SIZE_128, s_BUCKET_INFO[s_ActiveBucket].Name);
        }
        else if (CheckButton("3: Allocate 512 bytes") & S3E_KEY_STATE_PRESSED )
        {
            s_Allocations[ s_ActiveBucket ].push_back( new char[ MEM_SIZE_512 ] );

            sprintf(s_LastAction, "%u bytes allocated from %s", MEM_SIZE_512, s_BUCKET_INFO[s_ActiveBucket].Name);
        }
        else if (CheckButton("4: Allocate 1024 bytes") & S3E_KEY_STATE_PRESSED )
        {
            s_Allocations[ s_ActiveBucket ].push_back( new char[ MEM_SIZE_1024 ] );

            sprintf(s_LastAction, "%u bytes allocated from %s", MEM_SIZE_1024, s_BUCKET_INFO[s_ActiveBucket].Name);
        }

    }
    else
    {
        sprintf(s_LastAction, "Allocation limit reached");
    }


    if (CheckButton("5: Free last allocation") & S3E_KEY_STATE_PRESSED )
    {

        // Only free an allocation if any allocations remain.
        if (!s_Allocations[ s_ActiveBucket ].empty() )
        {
            // delete that last entry in the allocation array
            delete[]  ( s_Allocations[ s_ActiveBucket ].back() );

            // and remove the pointer from the back of the array.
            s_Allocations[ s_ActiveBucket ].pop_back();

            // Display the last action taken
            sprintf(s_LastAction, "Allocated free from %s", s_BUCKET_INFO[s_ActiveBucket].Name);
        }

    }

    // Note that we must pop any reference to these buckets from the ID stack first
    IwMemBucketPop();


    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    int fontScale = IwGxPrintGetScale();

    IwGxClear( IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F );

    char textBuffer[256];

    // Even before any allocation occur from a memory bucket there will be a fixed overhead
    // associated with the initialisation of a memory bucket, the size of which will depend
    // upon the type of memory bucket used.  This effect of this is that new memory bucket
    // will not have 100% of its capacity available for use by a user.
    sprintf(textBuffer, "Active Bucket: %s", s_BUCKET_INFO[s_ActiveBucket].Name);
    IwGxPrintString(0, 40 * fontScale, textBuffer);
    sprintf(textBuffer, "Size: %u", s_BUCKET_INFO[s_ActiveBucket].Capacity);
    IwGxPrintString(0, 48 * fontScale, textBuffer);
    sprintf(textBuffer, "Free Memory: %u", IwMemBucketGetFree(s_BUCKET_INFO[s_ActiveBucket].UniqueID));
    IwGxPrintString(0, 56 * fontScale, textBuffer);
    sprintf(textBuffer, "Used Memory: %u", IwMemBucketGetUsed(s_BUCKET_INFO[s_ActiveBucket].UniqueID));
    IwGxPrintString(0, 64 * fontScale, textBuffer);

    // Display the last action taken by the user.
    IwGxPrintString(0, 90 * fontScale, s_LastAction );

    // Control instruction
    IwGxPrintString(0, IwGxGetScreenHeight() - 70 * fontScale, "Left & Right: Select a bucket" );
    RenderCursorskeys();

    // Swap buffers
    IwGxFlush();
    IwGxSwapBuffers();
}
