/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwUtilHash Hash Example
 *
 * The following example demonstrates how to use the IwUtil hash
 * functionality. The example outputs two strings and their hash values.
 *
 * The functions used to achieve this are:
 *
 *  - <code>IwHashString()</code>
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwUtilHashImage.png
 *
 * @note For more information on hashing,
 * see the @ref stringhashing "String Hashing"
 * section in the <i>IwUtil API Documention</i>.
 *
 * @include IwUtilHash.cpp
 */

// for screen rendering
#include "IwGx.h"
#include "IwGxPrint.h"
#include "ExamplesMain.h"

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // initialise GX
    IwGxInit();

    EnableScaling(true);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // terminate GX
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    int fontScale = IwGxPrintGetScale();

    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Start with no lighting
    IwGxLightingOff();

    IwGxPrintString(0, 50 * fontScale, "String to Hash Representation");

    // display hashes of (similar) strings
    const char *string1 = "a string";
    const char *string2 = "another string";
    char output[100];
    sprintf(output, "%s - %u\n%s - %u", string1, IwHashString(string1), string2, IwHashString(string2));
    IwGxPrintString(0, 100 * fontScale, output);

    // Flush and swap
    IwGxFlush();
    IwGxSwapBuffers();
}
