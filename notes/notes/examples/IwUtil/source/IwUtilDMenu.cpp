/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

//-----------------------------------------------------------------------------
// ExampleIwUtilDMenu
//-----------------------------------------------------------------------------

/**
 * @page ExampleIwUtilDebugMenu Debug Menu Example
 *
 * The following example demonstrates how to use the IwMenu API to display
 * a menu system which can be used to view and alter custom parameters.
 * This example is designed to work only in debug builds.
 *
 * The important constructs in this example are:
 *   - CIwMenu
 *   - CIwMenuItem
 *   - CIwMenuManager
 *   - CIwManaged
 *
 * This example delares a simple custom class, CHansol, and a matching menu
 * class, CIwMenuItemHansol, which derive from CIwManaged and CIwMenuItem
 * respectively. This allows the menu item object which is created to be
 * part of the debug menu and to have access to the member variables of
 * a CHansol object.
 *
 * An object, s_Hansol, is instanciated and its members given some starting
 * values. By pressing F6, the debug menu can be displayed. It will show the
 * s_Hansol object. Press the right direction key to expand the object and
 * display its members; left direction key collapses a menu item. Use the
 * +/- keys to alter values which are writable.
 *
 * The example also gets the member values directly and prints them to screen to
 * demonstrate the effect changing values on the menu has on the actual object.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwUtilDMenuImage.png
 *
 * @note For more information on using the IwUtil functionality to create
 * debug menus, see the @ref debugmenus "Debug Menus" section of
 * the <i>IwUtil API Documentation</i>.
 *
 * @include IwUtilDMenu.cpp
 */

#include "IwMenu.h"

// For screen rendering
#include "IwGx.h"
#include "IwGxPrint.h"

// A simple class that we will inspect with the debug menu
//-----------------------------------------------------------------------------
class CHansol : public CIwManaged
{
public:
    IW_MANAGED_DECLARE(CHansol);

    CHansol() : m_Flags(0), m_Size(14), m_Character('b')
    {}

    enum
    {
        ON      = (1 << 0),
        BRIGHT  = (1 << 1)
    };

    // some arbitrary members
    uint32  m_Flags;
    int32   m_Size;
    char    m_Character;

};

//-----------------------------------------------------------------------------

IW_MANAGED_IMPLEMENT(CHansol);

// The CHansol object
CHansol *s_Hansol = NULL;

//-----------------------------------------------------------------------------

// A custom menu item used to display / edit s_Hansol's members
class CIwMenuItemHansol : public CIwMenuItem
{
public:
    CIwMenuItemHansol()
        : CIwMenuItem("", "s_Hansol", 0, CIwMenuItem::MENU) {};

    // Override the select function
    void Select()
    {
        CIwMenu *menu = new CIwMenu;

        // title string
        menu->AddItem(new CIwMenuItemTitle("s_Hansol"));

        // display read-only
        menu->AddItem(new CIwMenuItemDisplayValue("m_Character", s_Hansol->m_Character, IW_TYPE_STRING));

        // display writable
        menu->AddItem(new CIwMenuItemEditInt32("m_Size", &s_Hansol->m_Size));

        // display writable
        menu->AddItem(new CIwMenuItemEditFlags("ON", &s_Hansol->m_Flags, CHansol::ON));

        // display writable
        menu->AddItem(new CIwMenuItemEditFlags("BRIGHT", &s_Hansol->m_Flags, CHansol::BRIGHT));

        // set selection position
        menu->SetItemID(1);

        // add the new menu
        IwGetMenuManager()->AddMenu(menu);
    }
};

//-----------------------------------------------------------------------------

// Callback used to create the main menu
CIwMenu* DebugCreateMainMenu()
{
    CIwMenu* menu = new CIwMenu;

    // Create a menu item that leads to the CHansol object
    menu->AddItem(new CIwMenuItemHansol);

    // Set the start position to render the menu
    menu->SetXY(0, 100);

    return menu;
}

//-----------------------------------------------------------------------------

void ExampleInit()
{
    // Initialise GX
    IwGxInit();

    // Create the CHansol object
    s_Hansol = new CHansol;

    // Initialise menu manager
    new CIwMenuManager;

    // Set the debug menu text rendering callback
    IwGetMenuManager()->SetTextCallback(IwGxPrintMenuCallback);

    // Set the callback that creates the main menu
    IwGetMenuManager()->SetMainMenuFn(DebugCreateMainMenu);
}

//-----------------------------------------------------------------------------

void ExampleShutDown()
{
    // Destroy menu manager
    delete IwGetMenuManager();

    // Destroy the CHansol object
    delete s_Hansol;

    // Terminate GX
    IwGxTerminate();
}

//-----------------------------------------------------------------------------

bool ExampleUpdate()
{
    // Update menu
    IwGetMenuManager()->Update();

    return true;
}

//-----------------------------------------------------------------------------

void ExampleRender()
{
    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Start with no lighting
    IwGxLightingOff();

    IwGxPrintString(0, 50, "Demonstrating the IwMenu API");

    // Display state of the CHansol object
    char display[1000];
    sprintf(display,    "s_Hansol has\nm_Character = %c,\nm_Size = %d,\nON = %s,\nBRIGHT = %s",
                        s_Hansol->m_Character,
                        s_Hansol->m_Size,
                        (s_Hansol->m_Flags & CHansol::ON) ? "on" : "off",
                        (s_Hansol->m_Flags & CHansol::BRIGHT) ? "on" : "off");

    IwGxPrintString(0, IwGxGetDeviceHeight() - 120, display);


    IwGxPrintString(0, IwGxGetDeviceHeight() - 60, "F6: Bring up the Debug Menu");
    IwGxPrintString(0, IwGxGetDeviceHeight() - 50, "Arrow Keys: Navigate Setting");
    IwGxPrintString(0, IwGxGetDeviceHeight() - 40, "Num Pad +: Increment Setting");
    IwGxPrintString(0, IwGxGetDeviceHeight() - 30, "Num Pad -: Decrement Setting");

    // Display the menu
    IwGetMenuManager()->Render();

    // Flush and swap
    IwGxFlush();
    IwGxSwapBuffers();
}
