/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwUtilImageFormat Image Format Example
 *
 * The following example demonstrates how to use the IwUtil image
 * functionality. The example loads an image from file and displays
 * it using a number of different formats.
 *
 * The important functions in this example are:
 *
 *  - CIwImage::SetFormat()
 *  - CIwImage::ConvertToImage()
 *
 * You can cycle through the available formats by pressing any
 * button. When any key is pressed the example creates a new Image,
 * Texture and Material and attaches the image in the new format using
 * the<code> SetFormat()</code> function. The <code>ConvertToImage()</code> function
 * is used to set the display image to be the newly created one.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwUtilImageImage.png
 *
 * @note For more information on Image Formatting,
 * see the @ref imagemanipulation "Image Manipulation" section in the <i>IwUtil API Documention</i>.
 *
 * @include IwUtilImage.cpp
 */

//-----------------------------------------------------------------------------

// for screen rendering
#include "IwGx.h"
#include "IwGxPrint.h"
#include "ExamplesMain.h"

#include "IwImage.h"
#include "IwMaterial.h"
#include "IwTexture.h"
#include "s3eKeyboard.h"

//-----------------------------------------------------------------------------
#define NUM_FORMATS 10

// simple format + description pair
struct FormatDesc
{
    CIwImage::Format format;
    const char *desc;
};

#define FORMATDESC(x)   { CIwImage::x, #x }

// available formats
FormatDesc s_Formats[NUM_FORMATS] =
{
    FORMATDESC(RGB_332),    //!< Unpalettised 8-bit.
    FORMATDESC(BGR_332),    //!< Unpalettised 8-bit.

    FORMATDESC(RGB_565),    //!< Unpalettised 16-bit, no alpha.
    FORMATDESC(BGR_565),    //!< Unpalettised 16-bit, no alpha.

    FORMATDESC(RGB_888),    //!< Unpalettised 24-bit, no alpha.
    FORMATDESC(BGR_888),    //!< Unpalettised 24-bit, no alpha.

    FORMATDESC(PALETTE8_RGB_888),   //!< 256-colour palettised.
    FORMATDESC(PALETTE8_RGB_565),   //!< 256-colour palettised.

    FORMATDESC(PALETTE4_RGB_888),   //!< 16-colour palettised.
    FORMATDESC(PALETTE4_RGB_565)    //!< 16-colour palettised.
};

// index into available formats
int         s_FormatIdx = 0;

CIwImage    *s_Image = NULL;
CIwMaterial *s_Material = NULL;
CIwTexture  *s_Texture = NULL;

//-----------------------------------------------------------------------------
void LoadImage()
{
    // clean up
    if (s_Material)
        delete s_Material;

    if (s_Texture)
        delete s_Texture;

    if (s_Image)
        delete s_Image;

    s_Image = new CIwImage;
    s_Texture = new CIwTexture;
    s_Material = new CIwMaterial;

    // load image from file
    s3eFile* pFile = s3eFileOpen("textures/testTexture.bmp", "rb");
    s_Image->ReadFile(pFile);
    s3eFileClose(pFile);

    // to change the format, width, height or pitch of an image
    // call ConvertToImage on the source image;
    // the argument image must have the desired parameter values

    CIwImage* newImage = new CIwImage;

    // set the format of the new image
    newImage->SetFormat(s_Formats[s_FormatIdx].format);

    // NB: the result of the conversion lies in newImage, i.e., s_Image is unaltered.
    s_Image->ConvertToImage(newImage);

    // assign the new image to the texture and upload
    s_Texture->SetImage(newImage);
    s_Texture->Upload();

    // assign the texture to the material
    s_Material->SetColAmbient(IwGxGetColFixed(IW_GX_COLOUR_WHITE));
    s_Material->SetTexture(s_Texture);

    delete newImage;
}
//-----------------------------------------------------------------------------
void ExampleInit()
{
    // initialise GX
    IwGxInit();

    // no lighting
    IwGxLightingOff();

    LoadImage();

    EnableScaling(true);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    delete s_Image;
    delete s_Texture;
    delete s_Material;

    // terminate GX
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // convert between image formats when a key is pressed
    if (s3eKeyboardAnyKey())
    {
        s_FormatIdx = (s_FormatIdx + 1) % NUM_FORMATS;
        LoadImage();
    }

    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // display the image

    // set the material
    IwGxSetMaterial(s_Material);

    // draw a screen space rectangle (using the material)
    CIwSVec2 XY = CIwSVec2(0, 0);
    CIwSVec2 WH = CIwSVec2((int16)IwGxGetDeviceWidth(), (int16)IwGxGetDeviceHeight());
    IwGxDrawRectScreenSpace(&XY, &WH);

    // display the format description
    IwGxPrintString(0, 0, s_Formats[s_FormatIdx].desc);

    // Flush and swap
    IwGxFlush();
    IwGxSwapBuffers();
}
