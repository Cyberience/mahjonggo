/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwUtilArray Array Example
 *
 * The following example demonstrates how to use the IwUtil array
 * functionality. The example creates an array, populates it and then
 * performs a number of operations on it.
 *
 * The main functions used to achieve this are:
 *
 *  - CIwArray::push_back()
 *  - CIwArray::front()
 *  - CIwArray::back()
 *  - CIwArray::find()
 *  - CIwArray::erase()
 *  - CIwArray::size()
 *
 * The array's contents are added to the output string, then the first element
 * is found using numbers.front() and then decremented. The last element
 * is found using numbers.back() and then incremented. A search is
 * performed using numbers.find(2) and then that element is replaced.
 * The new contents of the array are then displayed.
 *
 * An array iterator is used to cycle through the array to display each element's
 * value. The IW_ARRAY_ITERATE macro is used, and this is equivalent
 * to, in this case:
 *
 * @code
 * for (CIwArray<int>::iterator it = numbers.begin();
 * it != numbers.end(); it++)
 * @endcode
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwUtilArrayImage.png
 *
 * @note For more information on array manipulation functionality,
 * see the @ref array "Array" section of the <i>IwUtil API Documentation</i>.
 *
 * @include IwUtilArray.cpp
 */

//-----------------------------------------------------------------------------

// for screen rendering
#include "IwGx.h"
#include "IwGxPrint.h"
#include "ExamplesMain.h"

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // initialise GX
    IwGxInit();

    EnableScaling(true);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // terminate GX
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Start with no lighting
    IwGxLightingOff();

    // create an empty array
    CIwArray<int> numbers;

    // populate the array
    numbers.push_back(1);
    numbers.push_back(2);
    numbers.push_back(3);
    numbers.push_back(4);

    char display[1000], output[100];
    display[0] = 0;

    sprintf(output, "Contents (size = %u):\n", numbers.size());
    strcat(display, output);

    /*
     * navigate the array, displaying contents
     * the IW_ARRAY_ITERATE macro is equivalent to, in this case,
     * for (CIwArray<int>::iterator it = numbers.begin(); it != numbers.end(); it++)
     */

    uint32 count = 0;
    IW_ARRAY_ITERATE(int, it, numbers)
    {
        sprintf(output, "Element %u = %d\n", count, *it);
        strcat(display, output);
        count++;
    }

    // decrement the first element
    numbers.front()--;

    // increment the last element
    numbers.back()++;

    // find the element equal to 2
    int searchIdx = numbers.find(2);

    // use operator[] to index into the array, and set this element to -1
    if (searchIdx != -1)
        numbers[searchIdx] = -1;

    // erase the second last element
    numbers.erase(numbers.end() - 2);

    sprintf(output, "Contents (size = %u):\n", numbers.size());
    strcat(display, output);

    IW_ARRAY_ITERATE_INT(i, numbers)
    {
        sprintf(output, "Element %u = %i\n", i, numbers[i]);
        strcat(display, output);
    }

    IwGxPrintString(10, 56, display);

    // Flush and swap
    IwGxFlush();
    IwGxSwapBuffers();
}
