/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwUtilManagedObject Managed Object Example
 *
 * The following example demonstrates how to use IwUtil managed
 * objects. This example creates instances of managed objects and outputs
 * their names using a managed list. The important functions in this
 * example are:
 *
 *  - <code>IW_MANAGED_DECLARE()</code>
 *  - <code>IW_MANAGED_IMPLEMENT()</code>
 *  - <code>IW_MANAGED_LIST_ITERATE()</code>
 *
 * The example derives two user classes from the CIwManaged class.
 * Both derived classes use the <code>IW_MANAGED_DECLARE</code>(<i><classname></i>)
 * macro to declare the essential methods and data. The <code>IW_MANAGED_IMPLEMENT</code>(<i><classname></i>)
 * macro is then used to implement the declared functionality.
 *
 * A list of managed objects is created using <code> IwManagedList
 * objects</code> and objects are added using <code>objects.Add(new
 * CHappy)</code>.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwUtilManagedImage.png
 *
 * @note For more information on managed objects,
 * see the @ref managedobjects "Managed Objects" section in the <i>IwUtil API Documention</i>.
 *
 * @include IwUtilManaged.cpp
 */

//-----------------------------------------------------------------------------

// for screen rendering
#include "s3e.h"
#include "IwGx.h"
#include "IwGxPrint.h"
#include "ExamplesMain.h"

// simple pointless class
//-----------------------------------------------------------------------------
class CHappy : public CIwManaged
{
public:
    IW_MANAGED_DECLARE(CHappy);
};
//-----------------------------------------------------------------------------

// define the class name function
IW_MANAGED_IMPLEMENT(CHappy);

// simple pointless class
//-----------------------------------------------------------------------------
class CSad : public CIwManaged
{
public:
    IW_MANAGED_DECLARE(CSad);
};
//-----------------------------------------------------------------------------

// define the class name function
IW_MANAGED_IMPLEMENT(CSad);

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // initialise GX
    IwGxInit();

    EnableScaling(true);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // terminate GX
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    if (s3eKeyboardGetState(s3eKeyOk) & S3E_KEY_STATE_PRESSED )
    {
        return false;
    }

    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    int fontScale = IwGxPrintGetScale();

    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Start with no lighting
    IwGxLightingOff();

    char display[1000], output[100];
    display[0] = 0;

    // create a list of objects
    CIwManagedList objects;

    // add to the list
    objects.Add(new CHappy);
    objects.Add(new CSad);

    // display the object names in the list
    IW_MANAGED_LIST_ITERATE(objects, it)
    {
        sprintf(output, "Found object named %s\n", (*it)->GetClassName());
        strcat(display, output);
    }

    //clear up objects
    objects.Delete();

    IwGxPrintString(0, 50 * fontScale, "MANAGED OBJECT EXAMPLE");
    IwGxPrintString(0, 80 * fontScale, display);

    // Flush and swap
    IwGxFlush();
    IwGxSwapBuffers();
}
