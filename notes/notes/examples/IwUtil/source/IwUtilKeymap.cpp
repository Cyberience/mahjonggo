/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwUtilKeyMapping Key Mapping Example
 *
 * The following example demonstrates how to use the IwKeymap
 * API. The example allows you to swap between a number of key mappings;
 * illustrating how you can create functionality that can easily adapt
 * to device type.
 *
 * The important function in this example is:
 *
 *  - <code>IwKeymapMapKey()</code>
 *
 * The example declares an enumeration of four keys: Up, Down,
 * Left, Right. By pressing "M" you can move through the key mappings
 * set for each key using the IwKeymapMapKey() function. This mapping
 * allows you to call the IwKeymapGetState(LEFT) function to determine
 * whether that key (depending on the selected mapping) has been pressed.
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwUtilKeyMapImage.png
 *
 * @note For more information on Key Mapping,
 * see the @ref keymapping "Key Mapping"
 * section in the <i>IwUtil API Documention</i>.
 *
 * @include IwUtilKeymap.cpp
 */

#include "IwGx.h"
#include "IwGxPrint.h"
#include "IwKeymap.h"
#include "ExamplesMain.h"

int32 s_Mapping;

// App keys
enum
{
    LEFT,
    RIGHT,
    UP,
    DOWN,
};

static const char* s_MappingNames[] =
{
    "ARROW KEYS",
    "NUMPAD ARROW KEYS",
    "A, D, W, S",
    "1, 2, 3, 4",
};

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    IwGxInit();

    // Set initial mapping
    s_Mapping = -1;

    EnableScaling(true);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // Terminate
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // Press 'M' to move to the next mapping
    if  (
        (s3eKeyboardGetState(s3eKeyM) & S3E_KEY_STATE_PRESSED) ||
        (s_Mapping < 0)
        )
    {
        s_Mapping = (s_Mapping + 1) & 0x3;
        switch(s_Mapping)
        {
        case 0:
            IwKeymapMapKey(LEFT,    s3eKeyLeft);
            IwKeymapMapKey(RIGHT,   s3eKeyRight);
            IwKeymapMapKey(UP,      s3eKeyUp);
            IwKeymapMapKey(DOWN,    s3eKeyDown);
            break;

        case 1:
            IwKeymapMapKey(LEFT,    s3eKeyNumPad4);
            IwKeymapMapKey(RIGHT,   s3eKeyNumPad6);
            IwKeymapMapKey(UP,      s3eKeyNumPad8);
            IwKeymapMapKey(DOWN,    s3eKeyNumPad2);
            break;
        case 2:
            IwKeymapMapKey(LEFT,    s3eKeyA);
            IwKeymapMapKey(RIGHT,   s3eKeyD);
            IwKeymapMapKey(UP,      s3eKeyW);
            IwKeymapMapKey(DOWN,    s3eKeyS);
            break;

        case 3:
            IwKeymapMapKey(LEFT,    s3eKey1);
            IwKeymapMapKey(RIGHT,   s3eKey2);
            IwKeymapMapKey(UP,      s3eKey3);
            IwKeymapMapKey(DOWN,    s3eKey4);
            break;
        }
    }
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    int fontScale = IwGxPrintGetScale();

    IwGxClear(IW_GX_COLOUR_BUFFER_F);

    IwGxPrintString(0, 50 * fontScale, "Key Mapping Example");
    IwGxPrintString(0, 80 * fontScale, "'m' key: Change key mapping" );

    // Display the mapping we are currently using
    int16 x = 2;
    int16 y = 110;
    IwGxPrintString(x * fontScale, y * fontScale, s_MappingNames[s_Mapping]);

    x += 0x10;
    y += 0x10;
    if (IwKeymapGetState(LEFT) & S3E_KEY_STATE_DOWN)
        IwGxPrintString(x * fontScale, y * fontScale, "LEFT");

    y += 8;
    if (IwKeymapGetState(RIGHT) & S3E_KEY_STATE_DOWN)
        IwGxPrintString(x * fontScale, y * fontScale, "RIGHT");

    y += 8;
    if (IwKeymapGetState(UP) & S3E_KEY_STATE_DOWN)
        IwGxPrintString(x * fontScale, y * fontScale, "UP");

    y += 8;
    if (IwKeymapGetState(DOWN) & S3E_KEY_STATE_DOWN)
        IwGxPrintString(x * fontScale, y * fontScale, "DOWN");

    // Flush and swap
    IwGxFlush();
    IwGxSwapBuffers();
}
