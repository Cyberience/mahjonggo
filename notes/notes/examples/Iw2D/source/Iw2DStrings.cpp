/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIw2DStrings Iw2D Strings Example
 *
 * The following example demonstrates how to draw text using Iw2D
 *
 * The main functions used to achieve this are:
 *  - Iw2DSetColour()
 *  - Iw2DCreateFont()
 *  - Iw2DDrawString()
 *
 * Iw2D supports simple rendering of fonts.
 * Fonts are complex resources that can only be created through the IwResManager
 * resource management system
 *
 * The following graphics illustrates the example output.
 *
 * @image html Iw2DStringsImage.png
 *
 * @include Iw2DStrings.cpp
 */

#include "Iw2D.h"
#include "IwResManager.h"

CIw2DFont* g_Font;

//-----------------------------------------------------------------------------
// The following function initialises the modules, loads the resources and
// prepares them for Iw2D
//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    Iw2DInit();

    // Initialise IwResManager
    IwResManagerInit();

    // Load the group containing the "arial14" font
    IwGetResManager()->LoadGroup("Iw2DStrings.group");

    // Prepare the iwgxfont resource for rendering using Iw2D
    g_Font = Iw2DCreateFontResource("arial14");
}
//-----------------------------------------------------------------------------
// The following function uninitialises the modules and clears up the resources
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    //Clear up the Iw2DFont
    delete g_Font;

    //IwResManagerTerminate will automatically clear up any loaded groups,
    //so there is no need to explicitly destroy the group.
    //However, for demonstration purposes we do so now
    IwGetResManager()->DestroyGroup("Iw2DStrings");

    // Terminate all modules initiaised
    IwResManagerTerminate();
    Iw2DTerminate();
}
//-----------------------------------------------------------------------------
// The following function does nothing in this example
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    return true;
}
//-----------------------------------------------------------------------------
// The following function renders a string in the centre of the screen
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen (in this example the screen is cleared by the example
    // framework, so there's no need to call it here)
    // Iw2DSurfaceClear(0xffffffff);

    // Set the current font
    Iw2DSetFont(g_Font);

    // Draw in black
    Iw2DSetColour(0xff000000);

    // Render the text into a 100x100 region
    CIwFVec2 region(100, 100);

    // Centred on the centre of the surface
    CIwFVec2 topLeft(Iw2DGetSurfaceWidth()/2.0f - region.x/2.0f,
        Iw2DGetSurfaceHeight()/2.0f - region.y/2.0f);

    // Draw the string into the region
    Iw2DDrawString("Hello world from Iw2D", topLeft, region, IW_2D_FONT_ALIGN_CENTRE, IW_2D_FONT_ALIGN_CENTRE);

    // Show the surface
    Iw2DSurfaceShow();
}
