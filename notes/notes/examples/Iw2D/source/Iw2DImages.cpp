/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
//-----------------------------------------------------------------------------
// ExampleIw2DImages
//-----------------------------------------------------------------------------

/**
 * @page ExampleIw2DImages Iw2D Images Example
 *
 * The following example demonstrates how to draw images using Iw2D
 *
 * The main functions used to achieve this are:
 *  - Iw2DSetColour()
 *  - Iw2DCreateImage()
 *  - Iw2DDrawImageRegion()
 *
 * Iw2D provides a variety of simple image drawing.
 * This example demonstrates drawing using an image.
 *
 * The following graphics illustrates the example output.
 *
 * @image html Iw2DImagesImage.png
 *
 * @include Iw2DImages.cpp
 */

#include "Iw2D.h"

int32 g_Frame = 0;

CIwColour g_Colours[] =
{
    { 0x00, 0x00, 0xff, 0xff },
    { 0x00, 0x20, 0xff, 0xff },
    { 0x00, 0x40, 0xff, 0xff },
    { 0x00, 0x60, 0xff, 0xff },
    { 0x00, 0x80, 0xff, 0xff },
    { 0x40, 0xa0, 0xff, 0xff },
    { 0x80, 0xc0, 0xff, 0xff },
    { 0xc0, 0xe0, 0xff, 0xff },
    { 0xff, 0xff, 0xff, 0xff },
    { 0xcf, 0xef, 0xff, 0xff },
    { 0x80, 0xc0, 0xff, 0xff },
    { 0x40, 0xa0, 0xff, 0xff },
    { 0x00, 0x80, 0xff, 0xff },
    { 0x00, 0x60, 0xff, 0xff },
    { 0x00, 0x40, 0xff, 0xff },
    { 0x00, 0x20, 0xff, 0xff },
};

CIw2DImage* g_Image;

//-----------------------------------------------------------------------------
// The following function initialises the 2D module
//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    Iw2DInit();

    // Create an iw2d compatible image
    g_Image = Iw2DCreateImage("Image.bmp");
}
//-----------------------------------------------------------------------------
// The following function uninitialises the 2D module
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    delete g_Image;

    // Terminate
    Iw2DTerminate();
}
//-----------------------------------------------------------------------------
// The following function increments the frame counter
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    g_Frame++;

    return true;
}
//-----------------------------------------------------------------------------
// The following function renders the image cut up into tiles with a colour
// cycle
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen (in this example the screen is cleared by the example
    // framework, so there's no need to call it here)
    // Iw2DSurfaceClear(0xffffffff);

    const uint32 c_NumberCols = sizeof(g_Colours)/sizeof(g_Colours[0]);
    const float c_RegionSize = 32;

    //Calculate the top left of the logo
    //Offset by half a rectangle width so the rectangles are centred on the calculate points
    CIwFVec2 topLeft = CIwFVec2(Iw2DGetSurfaceWidth() / 2.0f - g_Image->GetWidth() / 2.0f,
        Iw2DGetSurfaceHeight() / 2.0f - g_Image->GetHeight() / 2.0f);

    for (uint32 y = 0; y < g_Image->GetHeight() / c_RegionSize; y++)
    {
        for (uint32 x = 0; x < g_Image->GetWidth() / c_RegionSize; x++)
        {
            //Set the colour for each rectangle
            uint32 col = (x + y + g_Frame) % c_NumberCols;
            Iw2DSetColour(g_Colours[col]);

            //Calculate the top left of the region in the image
            CIwFVec2 pos = CIwFVec2(x * c_RegionSize, y * c_RegionSize);

            //Draw a sub region of the image at the position (offset by the topLeft)
            Iw2DDrawImageRegion(g_Image, topLeft + pos, pos, CIwFVec2(c_RegionSize, c_RegionSize));
        }
    }

    // Show the surface
    Iw2DSurfaceShow();
}
