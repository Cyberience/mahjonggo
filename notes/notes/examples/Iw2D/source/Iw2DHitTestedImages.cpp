/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
// ExampleIw2DHitTestedImages
//-----------------------------------------------------------------------------

/**
 * @page ExampleIw2DHitTestedImages Iw2D Images Example
 *
 * The following example demonstrates how to store an alpha channel and
 * use it to hit test images drawn using Iw2D.
 *
 * The main functions used to achieve this are:
 * - CIwImage::LoadFromFile
 * - CIwImage::ConvertToImage
 * - Iw2DCreateImage
 *
 * A frequently asked question is how to detect mouse clicks on an image respecting the
 * image's alpha channel.
 *
 * This example demonstrates the recommended method across all Marmalade modules using Iw2D.
 *
 * The application takes a copy of the image's alpha channel before handing it to Iw2D and
 * uses that to do its tests.
 *
 * In general, bitmap data passed to Studio rendering modules should be considered
 * not to be accessible. The main reasons for this are:
 *
 * - The image may get converted the image to another format (e.g. for SW rendering)
 * - The image may not be stored in cpu-accessible memory (e.g. uploaded to VRAM)
 *
 * Some advantages of storing just the alpha channel are:
 *
 * - Uses less data than keeping a copy of the whole image.
 * - Because the image has a known format, generic pixel access is not required.
 * - The alpha map could be altered independently of the image, for example to make
 *   collision more forgiving.
 *
 * @include Iw2DHitTestedImages.cpp
 */

#include "Iw2D.h"
#include "s3e.h"

int32 g_Frame = 0;

CIwColour g_Colours[] =
{
    { 0x00, 0x00, 0xff, 0xff },
    { 0x00, 0x20, 0xff, 0xff },
    { 0x00, 0x40, 0xff, 0xff },
    { 0x00, 0x60, 0xff, 0xff },
    { 0x00, 0x80, 0xff, 0xff },
    { 0x40, 0xa0, 0xff, 0xff },
    { 0x80, 0xc0, 0xff, 0xff },
    { 0xc0, 0xe0, 0xff, 0xff },
    { 0xff, 0xff, 0xff, 0xff },
    { 0xcf, 0xef, 0xff, 0xff },
    { 0x80, 0xc0, 0xff, 0xff },
    { 0x40, 0xa0, 0xff, 0xff },
    { 0x00, 0x80, 0xff, 0xff },
    { 0x00, 0x60, 0xff, 0xff },
    { 0x00, 0x40, 0xff, 0xff },
    { 0x00, 0x20, 0xff, 0xff },
};

//-----------------------------------------------------------------------------
// The following class encapsulates an Iw2D image with an alpha hitmap
//-----------------------------------------------------------------------------
class CImageWithHitMap
{
public:
    CImageWithHitMap(const char* filename)
    {
        //Get the image into memory
        CIwImage img;
        img.LoadFromFile(filename);

        //build hitmap - we store an 8bit per pixel alpha map
        m_Hitmap.SetFormat(CIwImage::A_8);
        m_Hitmap.SetWidth(img.GetWidth());
        m_Hitmap.SetHeight(img.GetHeight());

        //Convert the loaded image into an A_8 format image
        img.ConvertToImage(&m_Hitmap);

        //m_Hitmap now contains an 8-bit version of the alpha channel

        //Hand over to Iw2D
        m_Image = Iw2DCreateImage(filename);
    }

    bool TestHit(const CIwFVec2& imgPos, const CIwFVec2& testPos)
    {
        //Move the test position into "local" coordinate space
        CIwFVec2 localPos = testPos - imgPos;

        //Test for location outside the image rectangle
        if (localPos.x < 0
            || localPos.y < 0
            || localPos.x > (float)m_Hitmap.GetWidth()
            || localPos.y > (float)m_Hitmap.GetHeight() )
            return false;

        //Return a hit if the specified local alpha value is greater than half
        return m_Hitmap.GetTexels()[(int)localPos.y * m_Hitmap.GetWidth() + (int)localPos.x] > 0x80;
    }

    //Accessors
    float GetWidth() { return m_Image->GetWidth(); }
    float GetHeight() { return m_Image->GetHeight(); }
    CIw2DImage* GetImage() { return m_Image; }

    ~CImageWithHitMap()
    {
        delete m_Image;
    }

private:
    CIw2DImage* m_Image; //The Iw2DImage associated with this image
    CIwImage m_Hitmap; //8-bit version of the image's alpha channel
};

CImageWithHitMap* g_Image;


//-----------------------------------------------------------------------------
// The following function initialises the 2D module
//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    Iw2DInit();

    // Create an iw2d compatible image
    g_Image = new CImageWithHitMap("AlphaImage.png");
}
//-----------------------------------------------------------------------------
// The following function uninitialises the 2D module
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    delete g_Image;

    // Terminate
    Iw2DTerminate();
}
//-----------------------------------------------------------------------------
// The following function increments the frame counter
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    g_Frame++;

    return true;
}
//-----------------------------------------------------------------------------
// The following function renders the image cut up into tiles with a colour
// cycle
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen (in this example the screen is cleared by the example
    // framework, so there's no need to call it here)
    // Iw2DSurfaceClear(0xffffffff);

    const uint32 c_NumberCols = sizeof(g_Colours)/sizeof(g_Colours[0]);
    const float c_RegionSize = 32;

    //Calculate the top left of the logo
    //Offset by half a rectangle width so the rectangles are centred on the calculate points
    CIwFVec2 topLeft = CIwFVec2(Iw2DGetSurfaceWidth() / 2.0f - g_Image->GetWidth() / 2.0f,
        Iw2DGetSurfaceHeight() / 2.0f - g_Image->GetHeight() / 2.0f);

    //Make the logo pink if the cursor is over a solid part of the image
    bool altColours = g_Image->TestHit(topLeft, CIwFVec2((float)s3ePointerGetX(), (float)s3ePointerGetY()));

    for (uint32 y = 0; y < g_Image->GetHeight() / c_RegionSize; y++)
    {
        for (uint32 x = 0; x < g_Image->GetWidth() / c_RegionSize; x++)
        {
            //Set the colour for each rectangle
            uint32 col = (x + y + g_Frame) % c_NumberCols;
            if (altColours)
            {
                CIwColour c;
                c = g_Colours[col];
                c.r = 0xff;
                Iw2DSetColour(c);
            }
            else
            {
                Iw2DSetColour(g_Colours[col]);
            }

            //Calculate the top left of the region in the image
            CIwFVec2 pos = CIwFVec2((float)x * c_RegionSize, (float)y * c_RegionSize);

            //Draw a sub region of the image at the position (offset by the topLeft)
            Iw2DDrawImageRegion(g_Image->GetImage(), topLeft + pos, pos, CIwFVec2(c_RegionSize, c_RegionSize));
        }
    }

    // Show the surface
    Iw2DSurfaceShow();
}
