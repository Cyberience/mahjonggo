/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
//-----------------------------------------------------------------------------
// ExampleIw2DSurfaces
//-----------------------------------------------------------------------------

/**
 * @page ExampleIw2DSurfaces Iw2D Surfaces Example
 *
 * The following example demonstrates how to use surfaces using Iw2D
 *
 * The main functions used to achieve this are:
 *  - Iw2DSetSurface()
 *
 * Iw2D supports the use of offscreen surfaces for special effects.
 *
 * This example draws the logo into a surface and adds a red rectangle to it.
 *
 * It then draws using the surface in the same way as the Iw2DImages example.
 *
 * The following graphics illustrates the example output.
 *
 * @image html Iw2DSurfacesImage.png
 *
 * @include Iw2DImages.cpp
 */

#include "Iw2D.h"

int32 g_Frame = 0;

CIwColour g_Colours[] =
{
    { 0x00, 0x00, 0xff, 0xff },
    { 0x00, 0x20, 0xff, 0xff },
    { 0x00, 0x40, 0xff, 0xff },
    { 0x00, 0x60, 0xff, 0xff },
    { 0x00, 0x80, 0xff, 0xff },
    { 0x40, 0xa0, 0xff, 0xff },
    { 0x80, 0xc0, 0xff, 0xff },
    { 0xc0, 0xe0, 0xff, 0xff },
    { 0xff, 0xff, 0xff, 0xff },
    { 0xcf, 0xef, 0xff, 0xff },
    { 0x80, 0xc0, 0xff, 0xff },
    { 0x40, 0xa0, 0xff, 0xff },
    { 0x00, 0x80, 0xff, 0xff },
    { 0x00, 0x60, 0xff, 0xff },
    { 0x00, 0x40, 0xff, 0xff },
    { 0x00, 0x20, 0xff, 0xff },
};

CIw2DSurface* g_Surface;
CIw2DImage* g_SurfaceImage;

//-----------------------------------------------------------------------------
// The following function initialises the 2D module
//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    Iw2DInit();

    // Create an iw2d compatible image
    CIw2DImage* image = Iw2DCreateImage("Image.bmp");

    // Create a surface
    g_Surface = Iw2DCreateSurface((int32)image->GetWidth(), (int32)image->GetHeight());

    // Create an image containing the surface
    g_SurfaceImage = Iw2DCreateImage(g_Surface);

    // Set Iw2D to draw to the surface
    if (Iw2DSetSurface(g_Surface) )
    {
        // Clear the surface
        Iw2DSurfaceClear(0);

        // Draw the image into the surface
        Iw2DDrawImage(image, CIwFVec2(0, 0));

        // Add a red rectangle at the top of the logo
        Iw2DSetColour(0xff0000ff);
        Iw2DFillRect(CIwFVec2(15, 15), CIwFVec2(image->GetWidth()-30, 30));

        // Set Iw2D to draw to the screen again
        Iw2DSetSurface(NULL);

        // Free the logo image since it's no longer needed
        delete image;
    }
    else
    {
        // Surfaces are not available on this device. Use the logo image directly instead.
        delete g_SurfaceImage;

        g_SurfaceImage = image;
    }
}
//-----------------------------------------------------------------------------
// The following function uninitialises the 2D module
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // Delete Iw2d resources
    delete g_Surface;
    delete g_SurfaceImage;

    // Terminate
    Iw2DTerminate();
}
//-----------------------------------------------------------------------------
// The following function increments the frame counter
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    g_Frame++;

    return true;
}
//-----------------------------------------------------------------------------
// The following function renders the surface cut up into tiles with a
// colours cycle
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen (in this example the screen is cleared by the example
    // framework, so there's no need to call it here)
    // Iw2DSurfaceClear(0xffffffff);

    const uint32 c_NumberCols = sizeof(g_Colours)/sizeof(g_Colours[0]);
    const float c_RegionSize = 32;

    // Calculate the top left of the logo
    // Offset by half a rectangle width so the rectangles are centred on the calculate points
    CIwFVec2 topLeft = CIwFVec2(Iw2DGetSurfaceWidth() / 2.0f - g_SurfaceImage->GetWidth() / 2.0f,
        Iw2DGetSurfaceHeight() / 2.0f - g_SurfaceImage->GetHeight() / 2.0f);

    for (uint32 y = 0; y < g_SurfaceImage->GetHeight() / c_RegionSize; y++)
    {
        for (uint32 x = 0; x < g_SurfaceImage->GetWidth() / c_RegionSize; x++)
        {
            // Set the colour for each rectangle
            uint32 col = (x + y + g_Frame) % c_NumberCols;
            Iw2DSetColour(g_Colours[col]);

            // Calculate the top left of the region in the image
            CIwFVec2 pos = CIwFVec2(x * c_RegionSize, y * c_RegionSize);

            // Draw a sub region of the image at the position (offset by the topLeft)
            Iw2DDrawImageRegion(g_SurfaceImage, topLeft + pos, pos, CIwFVec2(c_RegionSize, c_RegionSize));
        }
    }

    // Show the surface
    Iw2DSurfaceShow();
}
