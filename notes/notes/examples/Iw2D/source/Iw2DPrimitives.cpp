/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
//-----------------------------------------------------------------------------
// ExampleIw2DPrimitives
//-----------------------------------------------------------------------------

/**
 * @page ExampleIw2DPrimitives Iw2D Primitives Example
 *
 * The following example demonstrates how to draw simple primitives using Iw2D
 *
 * The main functions used to achieve this are:
 *  - Iw2DSetColour()
 *  - Iw2DDrawArc()
 *  - Iw2DDrawLine()
 *  - Iw2DDrawPolygon()
 *  - Iw2DDrawRect()
 *  - Iw2DFillArc()
 *  - Iw2DFillPolygon()
 *  - Iw2DFillRect()
 *
 * Iw2D supports a number of primitives that can be rendered without any assets.
 * This example demonstrates drawing using these primitives.
 *
 * The following graphics illustrates the example output.
 *
 * @image html Iw2DPrimitivesImage.png
 *
 * @include Iw2DPrimitives.cpp
 */

#include "Iw2D.h"

int32 g_Frame = 0;

CIwColour g_Colours[] =
{
    { 0x00, 0x20, 0xff, 0xff },
    { 0x00, 0x40, 0xff, 0xff },
    { 0x00, 0x60, 0xff, 0xff },
    { 0x00, 0x80, 0xff, 0xff },
    { 0x00, 0xa0, 0xff, 0xff },
    { 0x00, 0xc0, 0xff, 0xff },
    { 0x00, 0xe0, 0xff, 0xff },
    { 0x00, 0xff, 0xff, 0xff },
};

//-----------------------------------------------------------------------------
// The following function initialises the 2D module
//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    Iw2DInit();
}
//-----------------------------------------------------------------------------
// The following function uninitialises the 2D module
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // Terminate
    Iw2DTerminate();
}
//-----------------------------------------------------------------------------
// The following function increments the frame counter
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    g_Frame++;

    return true;
}
//-----------------------------------------------------------------------------
// The following function renders a circle of rectangles, using the frame
// number to cycle the colours
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen (in this example the screen is cleared by the example
    // framework, so there's no need to call it here)
    // Iw2DSurfaceClear(0xffffffff);

    const uint32 c_NumberCols = sizeof(g_Colours)/sizeof(g_Colours[0]);
    const uint32 c_NumberRects = 30;
    const float c_RectSize = 40.0f;

    //Calculate the centre of the circle to be the centre of the surface
    //Offset by half a rectangle width so the rectangles are centred on the calculate points
    CIwFVec2 centre = CIwFVec2(Iw2DGetSurfaceWidth() / 2.0f - c_RectSize / 2.0f,
        Iw2DGetSurfaceHeight() / 2.0f - c_RectSize / 2.0f);

    for (uint32 i = 0; i < c_NumberRects; i++)
    {
        //Set the colour for each rectangle
        uint32 col = (i + g_Frame) % c_NumberCols;
        Iw2DSetColour(g_Colours[col]);

        //Calculate a rotated radius
        CIwFVec2 pos = CIwFVec2((float)sin(2* PI * i / c_NumberRects), (float)cos(2 * PI * i / c_NumberRects));

        //Scale the radius to quarter surface width
        pos *= Iw2DGetSurfaceWidth() / 4.0f;

        //Offset to centre
        pos += centre;

        //Draw a solid rectangle at that point
        Iw2DFillRect(pos, CIwFVec2(c_RectSize, c_RectSize));
    }

    // Show the surface
    Iw2DSurfaceShow();
}
