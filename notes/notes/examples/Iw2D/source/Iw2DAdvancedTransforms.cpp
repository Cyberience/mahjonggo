/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
//-----------------------------------------------------------------------------
// ExampleIw2DAdvancedAdvancedTransforms
//-----------------------------------------------------------------------------

/**
 * @page ExampleIw2DAdvancedTransforms Iw2D Advanced Transforms Example
 *
 * The following example demonstrates how to apply arbitrary post-transformations to Iw2D drawing
 *
 * The main functions used to achieve this are:
 *  - Iw2DSetColour()
 *  - Iw2DSetPostTransformFn()
 *  - Iw2DCreateImage()
 *  - Iw2DDrawImageRegion()
 *
 * Iw2D can use a callback function to post-transform positions and colours prior to rendering.
 * This allows non-linear transformation and lighting effects. This example adds lighting
 * and a non-linear transformation to the Iw2DImage example.
 *
 * The following graphics illustrates the example output.
 *
 * @image html Iw2DAdvancedTransformsImage.png
 *
 * @include Iw2DAdvancedTransforms.cpp
 */

#include "Iw2D.h"

int32 g_Frame = 0;

// The centre of the effect
CIwFVec2 g_EffectCentre;

CIw2DImage* g_Image;

//-----------------------------------------------------------------------------
// The following function initialises the 2D module
//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    Iw2DInit();

    g_Image = Iw2DCreateImage("Image.bmp");
}
//-----------------------------------------------------------------------------
// The following function uninitialises the 2D module
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    delete g_Image;

    // Terminate
    Iw2DTerminate();
}
//-----------------------------------------------------------------------------
// The following function increments the frame counter
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    g_Frame++;

    return true;
}


//-----------------------------------------------------------------------------
// The following function transforms vertices and colours such that ripples
// appear spreading out from the centre
//-----------------------------------------------------------------------------
void TransformAndLight(CIwFVec2* v, CIwColour* c, int32 numberPoints)
{
    const int32 c_RippleFrequency = 16;
    const float c_RippleSpeed = 250;
    const float c_RippleStrength = 0.025f;

    //For each point/colour passed in
    while (numberPoints--)
    {
        CIwFVec2 toCentre = *v - g_EffectCentre;

        float dist = toCentre.GetLength();

        //Get the direction to the centre of the effect
        if (dist)
            toCentre.Normalise();

        //Calculate the ripple strength
        float amount = PI * (dist*c_RippleFrequency - g_Frame*c_RippleSpeed) / 2048.0f;
        float ofs = (float)cos(amount) * c_RippleStrength * 512.0f;

        //Offset this point according to the strength
        *v += toCentre * ofs;

        //Light based on the offset
        uint8 brightness = (uint8)MIN(255, (ofs*8 + 0xa0));

        v++;

        //Set the light
        c->SetGrey(brightness);

        //Knock out red channel below the centre
        c->r -= (uint8)MAX(0, (toCentre.y * 0x20));
        c++;

    }
}

//-----------------------------------------------------------------------------
// The following function renders the image cut up into tiles with a colour
// cycle
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen (in this example the screen is cleared by the example
    // framework, so there's no need to call it here)
    // Iw2DSurfaceClear(0xffffffff);

    const float c_RegionSize = 32;

    //Calculate the top left of the logo
    //Offset by half a rectangle width so the rectangles are centred on the calculate points
    CIwFVec2 centre = CIwFVec2(Iw2DGetSurfaceWidth() / 2.0f, Iw2DGetSurfaceHeight() / 2.0f);
    CIwFVec2 topLeft = centre - CIwFVec2(g_Image->GetWidth() / 2, g_Image->GetHeight() / 2);

    //Calculate the centre of the effect (it rotates around the edge of the screen as time passes)
    float angle = 2 * PI * g_Frame * 0.01f;
    CIwFVec2 vec((float)sin(angle), (float)cos(angle));
    g_EffectCentre = vec * (Iw2DGetSurfaceWidth()/2.0f) + centre;

    //Set the post transform function
    Iw2DSetPostTransformFn(TransformAndLight);

    //The final images will still be modulated on the same 'order' as their untransformed colour,
    //So to see the blue in the final image we must set a colour that would require full RGB modulation
    //(i.e. The RGB must not be equal)
    Iw2DSetColour(0x80ff8080);


    for (uint32 y = 0; y < g_Image->GetHeight() / c_RegionSize; y++)
    {
        for (uint32 x = 0; x < g_Image->GetWidth() / c_RegionSize; x++)
        {
            //Calculate the top left of the region in the image
            CIwFVec2 pos = CIwFVec2(x * c_RegionSize, y * c_RegionSize);

            //Draw a sub region of the image at the position (offset by the topLeft)
            Iw2DDrawImageRegion(g_Image, topLeft + pos, pos, CIwFVec2(c_RegionSize, c_RegionSize));
        }
    }

    //Clear the post-transformation
    Iw2DSetPostTransformFn(NULL);


    // Show the surface
    Iw2DSurfaceShow();
}
