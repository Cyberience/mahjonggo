#section: Examples - Quick
#category: Tutorial
#tool: Quick
My first Marmalade Quick App

The objectives of this tutorial series include:
    Build a Match-3 game (similar to games such as Bejeweled and Candy Crush) from scratch using Marmalade Quick in stages
    Introduce key concepts of Marmalade Quick as we progress from stage to stage
    Cover more advanced topics such as supporting in-app purchase, ads and social integration

Stage 8: Audio and Particles

The aims of this tutorial stage include:
    Add audio hooks to play background music sound effects
    Pre-load resources
    Create and use particle systems to add spot effects
