--[[
User interface
--]]

module(..., package.seeall)

-- Globals
pauseScene = nil

-- UI
local continueGameButton
local newGameButton

-- End new game button animation (starts new game)
function newGame(event)
	-- Tween button back to normal
	tween:to(newGameButton, { alpha=1, time=0.2 } )
	-- Switch to game scene
	switchToScene("game")
	-- Start new game
	game:newGame()
end

-- End continue game button animation (resumes game)
function continueGame(event)
	-- Tween button back to normal
	tween:to(continueGameButton, { alpha=1, time=0.2 } )
	-- Switch to game scene
	switchToScene("game")
	-- Resume game
	game:resumeGame()
end

-- New game button touched event handler
function newGameButtonTouched(event)
	if (event.phase == "ended") then
		-- Animate the play button
        pauseScene.isTouchable = false
		tween:to(newGameButton, { alpha=0.3, time=0.2, onComplete=newGame } )
	end
end

-- Continue game button touched event handler
function continueGameButtonTouched(event)
	if (event.phase == "ended") then
		-- Animate the play button
        pauseScene.isTouchable = false
		tween:to(continueGameButton, { alpha=0.3, time=0.2, onComplete=continueGame } )
	end
end

-- Scene will now handle touch
function setUpHandler()
    pauseScene.isTouchable = true
end

-- Create and initialise the main menu
function init()
	-- Create a scene to contain the main menu
	pauseScene = director:createScene()
    pauseScene:addEventListener("setUp", setUpHandler)
	-- Create menu background
	local background = director:createSprite(director.displayCenterX, director.displayCenterY, "textures/menu_bkg.jpg")
	background.xAnchor = 0.5
	background.yAnchor = 0.5
	-- Fit background to screen size
	local bg_width, bg_height = background:getAtlas():getTextureSize()
	background.xScale = director.displayWidth / bg_width
	background.yScale = director.displayHeight / bg_height
  
	-- Create Continue Game button
	continueGameButton = director:createSprite(director.displayCenterX, 0, "textures/button_bg.png")
	local atlas = continueGameButton:getAtlas()
	local atlas_w, atlas_h = atlas:getTextureSize()
	local button_height = atlas_h * game.graphicsScale
	local y_pos = button_height * 2
 	continueGameButton.y = y_pos
	continueGameButton.xAnchor = 0.5
	continueGameButton.yAnchor = 0
	continueGameButton.xScale = game.graphicsScale
	continueGameButton.yScale = game.graphicsScale
	continueGameButton:addEventListener("touch", continueGameButtonTouched)
	-- Create Continue Game button text
	local label = director:createLabel( {
		x = 0, y = 0, 
		w = atlas_w, h = atlas_h, 
		hAlignment="centre", vAlignment="middle", 
		font=game.defaultFont, 
		text="Continue", 
		textXScale = game.fontScale / game.graphicsScale,	-- Compound scaling factors as label ie child of button
		textYScale = game.fontScale / game.graphicsScale, 
		})
	continueGameButton:addChild(label)
	y_pos = y_pos - button_height

	-- Create New Game button
	newGameButton = director:createSprite(director.displayCenterX, y_pos, "textures/button_bg.png")
	newGameButton.xAnchor = 0.5
	newGameButton.yAnchor = 0
	newGameButton.xScale = game.graphicsScale
	newGameButton.yScale = game.graphicsScale
	newGameButton:addEventListener("touch", newGameButtonTouched)
	-- Create Continue Game button text
	local label = director:createLabel( {
		x = 0, y = 0, 
		w = atlas_w, h = atlas_h, 
		hAlignment="centre", vAlignment="middle", 
		font=game.defaultFont, 
		text="New Game", 
		textXScale = game.fontScale / game.graphicsScale, -- Compound scaling factors as label is child of button
		textYScale = game.fontScale / game.graphicsScale, 
		})
	newGameButton:addChild(label)


end

