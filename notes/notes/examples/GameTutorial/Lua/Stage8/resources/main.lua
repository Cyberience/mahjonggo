--[[
Match 3 game
--]]

-- Enable ZeroBraneStudio debugging
--require("mobdebug").start()

-- Set up 'exit' event listener to allow Ctrl-R functionality
function exit(event)
	dbg.print("unrequiring")
	unrequire("class")
	unrequire("game")
	unrequire("grid")
	unrequire("gem")
	unrequire("explosion")
	unrequire("selector")
	unrequire("mainMenu")
	unrequire("pauseMenu")
end
system:addEventListener("exit", exit)

-- Limit frame rate to 30 fps
system:setFrameRateLimit(30)

-- Create and initialise the game
require("game")
game.init()

-- Switch to specific scene
function switchToScene(scene_name)
	if (scene_name == "game") then
		director:moveToScene(game.gameScene, {transitionType="slideInL", transitionTime=0.5})
	elseif (scene_name == "main") then
		director:moveToScene(mainMenu.menuScene, {transitionType="slideInL", transitionTime=0.5})
	elseif (scene_name == "pause") then
		director:moveToScene(pauseMenu.pauseScene, {transitionType="slideInL", transitionTime=0.5})
	end
end

