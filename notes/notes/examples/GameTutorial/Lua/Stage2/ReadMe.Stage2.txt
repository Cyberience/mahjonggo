#section: Examples - Quick
#category: Tutorial
#tool: Quick
My first Marmalade Quick App

The objectives of this tutorial series include:
    Build a Match-3 game (similar to games such as Bejeweled and Candy Crush) from scratch using Marmalade Quick in stages
    Introduce key concepts of Marmalade Quick as we progress from stage to stage
    Cover more advanced topics such as supporting in-app purchase, ads and social integration

Stage 2: Interactivity, Tweening, Labels and Debugging

The aims of this tutorial stage include:
    Subscribe to the touch event of a sprite to respond to touches
    Print debug information and use Traceview
    Introduce you to animation using basic tweening
    Use ZeroBrane Studio to debug an application
