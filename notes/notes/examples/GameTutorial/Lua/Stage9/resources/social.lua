--[[

Facebook posting

--]]

module(..., package.seeall)

-- OO functions
local class = require("class")

-- Create the social class
social = inheritsFrom(baseClass)

-- Creates an instance of the social class
function new()
	local o = social:create()
	social.init(o)
	return o
end

function social:postUpdate()
  local message = "I just scored " .. self.currentScore .. " on level " .. self.currentLevel .. " playing Match-3"
  facebook:postUpdate(message)
end

function social:facebook(event)
	if event.type == "login" then
		if (event.result) then
      self.loggedIn = true
      self:postUpdate()
		end
	end
end

-- Initialise the social class
function social.init(self)
  self.available = false
  self.loggedIn = false
  self.currentScore = 0
  self.currentLevel = 0
  if (facebook:isAvailable()) then
    self.available = true
    facebook:register{appID="add_your_own_facebook_app_id"}
    system:addEventListener("facebook", self)
  end
end

-- Post score and level 
function social:postScoreUpdate(level, score)
  self.currentScore = score
  self.currentLevel = level
  if (self.loggedIn == false) then
    facebook:login()
  else
    self:postUpdate()
  end
end


