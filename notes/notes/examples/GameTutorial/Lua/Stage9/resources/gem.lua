--[[
A gem game obejct
--]]

module(..., package.seeall)

-- OO functions
require("class")
require("explosion")
require("game")

-- Gem image animations
local gemAtlases = {}
table.insert(gemAtlases, director:createAtlas( { width=85, height=85, numFrames=15, textureName="textures/sparkle_aubergine.png" }))
table.insert(gemAtlases, director:createAtlas( { width=85, height=85, numFrames=15, textureName="textures/sparkle_carrots.png" }))
table.insert(gemAtlases, director:createAtlas( { width=85, height=85, numFrames=15, textureName="textures/sparkle_cauliflower.png" }))
table.insert(gemAtlases, director:createAtlas( { width=85, height=85, numFrames=15, textureName="textures/sparkle_pepper.png" }))
table.insert(gemAtlases, director:createAtlas( { width=85, height=85, numFrames=15, textureName="textures/sparkle_tomato.png" }))

-- Globals
gemBitmapWidth, gemBitmapHeight = gemAtlases[1]:getTextureSize()
gemBitmapWidth = gemBitmapWidth / 5
gemBitmapHeight = gemBitmapHeight / 3
gemActualWidth = (director.displayWidth * gemBitmapWidth) / game.graphicDesignWidth -- The actual device coordinate width of the gem
gemActualHeight = gemActualWidth						-- The actual device coordinate height of the gem
gemFallSpeed = 500										-- Speed at which gems fall into place (pixels per second)
gemDestroyedScore = 10									-- number of points to award the player for destroying a gem

-- Create the gem class
gem = inheritsFrom(baseClass)

-- Creates an instance of a new gem
function new(type, x, y)
	local o = gem:create()
	gem:init(o, type, x, y)
	return o
end

-- Initialise the gem
function gem:init(o, type, x, y)
	-- Create a sprite
	o.sprite = director:createSprite(x, y)
	o.sprite.xScale = gemActualWidth / gemBitmapWidth
	o.sprite.yScale = gemActualHeight / gemBitmapHeight
	o.type = type
end

-- Change the gem type
function gem:setType(type)
	if (self.type ~= type) then
		self.type = type
		if (type > 0) then
			-- Set new animation
			self.sprite:setAnimation(director:createAnimation( { start=1, count=15, atlas=gemAtlases[type], delay=1/8} ))
			self.sprite:play({ startFrame=0, loopCount=1 })
		end
	end
end

-- Explode gem
function gem:explode()
	-- Fade the gem out
	tween:to(self.sprite, { alpha=0, time=0.2, easing=ease.sineIn } )
	-- Remove the gem from play
	game.removeGem(self)
	-- Update round score
	game.addToRoundScore(gemDestroyedScore, self.type)
	-- Create an explosion where this gem used to be
	explosion.createExplosion(self.sprite.x + gemActualWidth / 2, self.sprite.y + gemActualHeight / 2, self.type)
end

function getGemAnimation(type)
	return director:createAnimation( { start=1, count=15, atlas=gemAtlases[type], delay=1/8} )
end

