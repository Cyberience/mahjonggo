--[[

Flurry

--]]

module(..., package.seeall)

-- OO functions
local class = require("class")

-- Create the stats class
stats = inheritsFrom(baseClass)

-- Creates an instance of the stats class
function new()
	local o = stats:create()
	stats:init(o)
	return o
end

-- Initialise the stats class
function stats:init(o)
  self.available = false
  if (flurry:isAvailable()) then
    self.available = true
    -- Start session
    flurry:startSession("CFFQXVS29JRY2MDX8GP4") -- change this to the app ID provided to you by Flurry
  end
end

-- Log an event
function stats:logEvent(message)
  if (self.available) then
   	flurry:logEvent(message)
  end
end

-- End session
function stats:endSession()
  if (self.available) then
    analytics:endSession()
  end
end
