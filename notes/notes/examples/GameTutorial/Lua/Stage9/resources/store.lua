--[[

Facebook posting

--]]

module(..., package.seeall)

-- OO functions
local class = require("class")

-- No ads product id's (Add your own product ID's to test)
local productIds = {
  ["IPHONE"] = "com.companyname.appname.noads",
  ["ANDROID"] = "android.test.purchased"
  }

-- Create the store class
store = inheritsFrom(baseClass)

-- Creates an instance of the store class
function new()
	local o = store:create()
	store:init(o)
	return o
end

-- Called in response to a billing event
function store:billing(event)
	if (event.type == "receiptAvailable") then
    if (billing:finishTransaction(event.finaliseData)) then
      game.disableAds()
    end
	end
end

-- Initialise the store class
function store:init(o)
  self.available = false
  if (billing:isAvailable()) then
    if (billing:init()) then
      self.available = true
      system:addEventListener("billing", self)
    end
  end
end

-- Purchase no-ads
function store:purchaseNoAds()
  if (self.available) then
    local platform = device:getInfo("platform")
    billing:purchaseProduct(productIds[platform])
  end
end



