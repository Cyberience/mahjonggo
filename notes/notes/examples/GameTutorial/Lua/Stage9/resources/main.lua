--[[
Match 3 game
--]]

-- Enable ZeroBraneStudio debugging
--require("mobdebug").start()

-- Limit frame rate to 30 fps
system:setFrameRateLimit(30)

-- Create and initialise the game
local game = require("game")
game.init()

-- Set up 'exit' event listener to allow Ctrl-R functionality
function exit(event)
  game.stats:endSession()
	dbg.print("unrequiring")
	unrequire("class")
	unrequire("game")
	unrequire("grid")
	unrequire("gem")
	unrequire("explosion")
	unrequire("selector")
	unrequire("mainMenu")
	unrequire("pauseMenu")
	unrequire("adverts")
 	unrequire("social")
 	unrequire("store")
 	unrequire("stats")
end
system:addEventListener("exit", exit)

-- Switch to specific scene
function switchToScene(scene_name)
	if (scene_name == "game") then
		director:moveToScene(game.gameScene, {transitionType="slideInL", transitionTime=0.5})
	elseif (scene_name == "main") then
		director:moveToScene(mainMenu.menuScene, {transitionType="slideInL", transitionTime=0.5})
	elseif (scene_name == "pause") then
		director:moveToScene(pauseMenu.pauseScene, {transitionType="slideInL", transitionTime=0.5})
	end
end

