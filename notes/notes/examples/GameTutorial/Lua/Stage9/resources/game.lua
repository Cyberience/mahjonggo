--[[
Main game logic
--]]

module(..., package.seeall)

-- Global design time constants
gemCountX = 8										    -- Total columns in gem grid
gemCountY = 8										    -- Total rows in gem grid
fontHeight = 15										  -- Pixel height of font
fontDesignWidth = 320								-- Font was designed to be displayed on a 320 wide screen
graphicDesignWidth = 768						-- Graphics were designed to be displayed on a 768 wide screen

-- OO functions
local class = require("class")
require("grid")
require("selector")
require("mainMenu")
require("pauseMenu")
require("explosion")
require("adverts")
require("social")
require("store")
require("stats")

-- Local constants
local maxGemTypes = 5				  -- Maximum different types of gems
local startRoundTime = 110		-- Players time limit for first round
local roundTimeStep = 10			-- Amount to increment time limit each round
local startTargetScore = 600	-- Players target score for first round
local targetScoreStep = 100		-- Amount to increment target score each round

-- More constants
fontScale = director.displayWidth / fontDesignWidth	-- Font is correct size on 320 wide screen so we scale to match native screen size
actualFontHeight = fontHeight * fontScale			      -- The actual pixel height of the font
graphicsScale = director.displayWidth / graphicDesignWidth	-- Graphics are designed for 768 wide screen so we scale to native screen size
gridActualHeight = gem.gemActualHeight * gemCountY			    -- The actual device coordinate height of the gems grid
gridOffsetX = 41 * graphicsScale                    -- Grid offset screen position on x-axis
gridOffsetY = 37 * graphicsScale                    -- Grid offset screen position on y-axis
defaultFont = director:createFont("fonts/ComicSans24.fnt")	-- The default font
adverts = adverts.new()       -- Create adverts class
social = social.new()         -- create social class
store = store.new()           -- Create store class
stats = stats.new()           -- Create stats class

-- List of game states
gameStates = {
	paused				    = 0,	-- The game is paused
	waitingFirstGem		= 1,	-- The game is waiting for first gem selection
	waitingSecondGem	= 2,	-- The game is waiting for second gem selection
	thinking			    = 3,	-- The game is analayzing the move and removing blocks / adding new blocks
	gemsFalling			  = 4,	-- The game is waiting for gems to stop falling
	roundOver			    = 5,	-- The game round is over
}

-- The game scene
gameScene = nil

-- Game state
local currentRound = 1				  -- Current tound that is in play
local currentRoundScore = 0			-- Current round score
local currentRoundTime = 0			-- Current round time (this counts downwards)
local targetRoundScore = 0			-- Target round score (player must beat this to win round)
local lastRound = 0            -- Remember previous "current round" at the end of the round
local lastRoundScore = 0       -- Remember previous "current round score" at the end of the round
local targetGem = 1					    -- Current target gem to destroy to gain bonus points
local gemsGrid						      -- The grid of gems
local gridTouchesX = { -1, -1 }		-- First and second grid touch pos on x-axis
local gridTouchesY = { -1, -1 }		-- First and second grid touch pos on y-axis
local gameState = gameStates.paused	-- Current game state
local oldGameState					      -- Previous game state

-- UI components
local leftPlacard
local rightPlacard
local scoreLabelText
local scoreLabel
local targetScoreLabelText
local targetScoreLabel
local targetGemSprite
local roundLabelText
local roundLabel
local timerLabelText
local timerLabel
local pauseSprite
local infoPanel
local infoPanelTitleLabel
local infoPanelMessageLabel
local uiYPosition = director.displayHeight

-- Event handler that is called when a gem swap back animation finishes
function gemSwapBackFinished(event)
	-- Switch back to waiting for first gem selection game state
	if (gameState ~= gameStates.roundOver) then
		changeGameState(gameStates.waitingFirstGem)
	end
end

-- Event handler that is called to start the gems falling process (called after 3 or more gems are matched)
function startGemsFalling(event)
	-- Change game state to move gems down and new gems in
	if (gameState ~= gameStates.roundOver) then
		changeGameState(gameStates.gemsFalling)
	end
end

-- Called back when a gem swap animation finishes
function gemSwapFinished(event)
	if (gemsGrid:checkMatches() == false) then
		-- No match found so swap gems back
		gemsGrid:swapGems(gridTouchesX, gridTouchesY)
		-- Start a timer that expires when gems have been swapped back
		local timer = gameScene:addTimer(gemSwapBackFinished, 0.6, 1, 0)
	else
		-- Match found so start a timer to switch to gem falling state (this gives a small delay to allow destroyed gems to clear)
		gameScene:addTimer(startGemsFalling, 0.6, 1, 0)
	end
end

-- Remove a gem from the game
function removeGem(gem)
	gemsGrid:removeGem(gem)
end

-- Changes the game state
function changeGameState(game_state)
	gameState = game_state
	if (gameState == gameStates.waitingFirstGem) then
		-- Hide selector
		selector:hide()
	elseif (gameState == gameStates.waitingSecondGem) then
		-- Show selector
		selector:show((gridTouchesX[1] - 1) * gem.gemActualWidth + (gem.gemActualWidth / 2) + gridOffsetX, ((gridTouchesY[1] - 1) * gem.gemActualHeight + (gem.gemActualHeight / 2) + gridOffsetY))
	elseif (gameState == gameStates.thinking) then
		-- Hide selector
		selector:hide()
		-- Swap the gems
		gemsGrid:swapGems(gridTouchesX, gridTouchesY)
		-- Start a timer that expires afer the gems have been swapped
		gameScene:addTimer(gemSwapFinished, 0.6, 1, 0)
	end
end

-- Add to round score
function addToRoundScore(score, gem_type)
	-- Double score if matched gem matches target gem
	if (gem_type == targetGem) then
		score = score * 2
	end
	currentRoundScore = currentRoundScore + score
	if (currentRoundScore > targetRoundScore) then
		-- Play sound effect to let player know they reached target score
	end
	scoreLabel.text = currentRoundScore
end

-- Choose a random target gem
function chooseTargetGem(event)
	if (gameState ~= gameStates.roundOver) then
		-- Select a random target gem
		targetGem = math.random(maxGemTypes)
		-- Change target gem sprite to show the new target gem
		targetGemSprite:setAnimation(gem.getGemAnimation(targetGem))
	end
end

-- Shows the info panel
function showInfoPanel(title, info)
	infoPanelVisible = true
	infoPanelTitleLabel.text = title
	infoPanelMessageLabel.text = info
	local panel_atlas = infoPanel:getAtlas()
	local panel_w, panel_h = panel_atlas:getTextureSize()
	tween:to(infoPanel, { x=director.displayCenterX - (panel_w / 2) * graphicsScale, time=1, easing=ease.sineIn } )
end

-- infopanel hidden animation ended (starts next round)
function hideInfoPanelHideEnd(event)
  initRound()
end

-- Hides the info panel
function hideInfoPanel()
	if (infoPanelVisible == true) then
		infoPanelVisible = false
		tween:to(infoPanel, { x=director.displayWidth, time=1, easing=ease.sineOut, onComplete=hideInfoPanelHideEnd } )
	end
end

-- Info panel touch event handler
function infoPanelTouched(event)
	if (gameState == gameStates.roundOver and event.phase == "ended") then
		-- Hide info panel if its visible
		hideInfoPanel()
	end
end

-- Pause game handler that is called when the user taps the pause sprite
function pauseGame(event)
	if (event.phase == "ended") then
		oldGameState = gameState
		gameState = gameStates.paused
		explosion.stopExplosions()
		gameScene:pauseTimers()
		gameScene:pauseTweens()
		switchToScene("pause")
	end
end

-- Resumes the game from a previously paused state
function resumeGame(event)
    gameState = oldGameState
    gameScene:resumeTree()
end

-- Initialise a round
function initRound()
	-- We resume timers and tweens because we may have entered here from new game (pause menu) which 
	-- pauses all timers and tweens in the game scene
	gameScene:resumeTimers()
	gameScene:resumeTweens()
	-- Cancel all game scene timers
	for i,v in ipairs(gameScene.timers) do
		v:cancel()
		gameScene.timers[i] = nil
	end
	-- Cancel all game scene tweens
	for i,v in ipairs(gameScene.tweens) do
		v:cancel()
		gameScene.tweens[i] = nil
	end
	-- Regenrate grid
	gemsGrid:regenerate()
	-- Reset round score
	currentRoundScore = 0
	-- Calculate target round score
	targetRoundScore = math.floor(startTargetScore + currentRound * targetScoreStep)
	-- Calculate round time
	currentRoundTime = math.floor(startRoundTime + currentRound * roundTimeStep)
	-- Set up labels
	scoreLabel.text = currentRoundScore
	targetScoreLabel.text = targetRoundScore
	roundLabel.text = currentRound
	timerLabel.text = currentRoundTime
	-- Create target gem timer (changes the target gem every 10 seconds)
	gameScene:addTimer(chooseTargetGem, 10, 0, 0)
	-- Set the game going
	changeGameState(gameStates.waitingFirstGem)
end

-- End of round - Called when the round is over (when game timer runs out)
function endOfRound()
  lastRound = currentRound
  lastRoundScore = currentRoundScore
  -- Show end of round info
  if (lastRoundScore >= targetRoundScore) then
    showInfoPanel("Round " .. lastRound .. " Complete", "Score: " .. lastRoundScore)
	-- Move to next round number
	currentRound = lastRound + 1
    audio:playSound("audio/round_won.raw")
  else
    showInfoPanel("Round " .. lastRound .. " Failed", "Score: " .. lastRoundScore)
    audio:playSound("audio/round_lost.raw")
  end
  -- Change to round over game state
  changeGameState(gameStates.roundOver)
end

-- Starts a new game - Called from the main and pause menus
function newGame()
	currentRound = 1
	initRound()
	-- Log new game event
	stats:logEvent("New Game")
end

-- Touch event handler (called when the user touches the screen)
local touch = function(event)
	if (director:getCurrentScene() == gameScene and event.phase == "ended") then
		if (gameState == gameStates.roundOver) then
		elseif (event.y < gridActualHeight) then
			-- Get touched grid coordinates
			local grid_x, grid_y = gemsGrid:screenToGrid(event.x, event.y)
			if grid_x >= 1 and grid_x <= gemCountX and
			   grid_y >= 1 and grid_y <= gemCountY then
				if (gameState == gameStates.waitingFirstGem) then
					-- In waiting for first gem state we place a selector over the first selected gem
					gridTouchesX[1] = grid_x
					gridTouchesY[1] = grid_y
					changeGameState(gameStates.waitingSecondGem)
				elseif (gameState == gameStates.waitingSecondGem) then
					gridTouchesX[2] = grid_x
					gridTouchesY[2] = grid_y
					local dx = gridTouchesX[2] - gridTouchesX[1]
					local dy = gridTouchesY[2] - gridTouchesY[1]
					if ((dx == 0 and (dy == -1 or dy == 1)) or (dy == 0 and (dx == -1 or dx == 1))) then
						-- Second gem is swappable so switch to thinking state
						changeGameState(gameStates.thinking)
						audio:playSound("audio/gem_swap.raw")
					else
						-- Second gem is too far or the same gem, so reset
						changeGameState(gameStates.waitingFirstGem)
						audio:playSound("audio/gem_wrong.raw")
					end
				end
            end
		end
	end
end

-- Game update loop
local update = function(event)
	-- Update falling gems in grid
	if (gameState == gameStates.gemsFalling) then
		gemsGrid:gemsFalling()
	end
	-- Update round timer
	if (gameState ~= gameStates.roundOver and gameState ~= gameStates.paused) then
		timerLabel.text = math.floor(currentRoundTime)
		currentRoundTime = currentRoundTime - system.deltaTime
		if (currentRoundTime <= 0) then
			currentRoundTime = 0
			-- Allow last played move to finish
			if (gameState == gameStates.waitingFirstGem or gameState == gameStates.waitingSecondGem) then
				endOfRound()
			end
		end
	end
	-- Update explosions
	explosion.updateExplosions()
end

-- Initialise audio
function initAudio()
	-- Preload sound effects
	audio:loadSound("audio/gem_land.raw")
	audio:loadSound("audio/gem_destroyed.raw")
	audio:loadSound("audio/gem_swap.raw")
	audio:loadSound("audio/gem_wrong.raw")
	audio:loadSound("audio/round_won.raw")
	audio:loadSound("audio/round_lost.raw")
end

-- Initialise the games user interface
function initUI()
	-- Create grid background
	local background = director:createSprite(director.displayCenterX, director.displayCenterY, "textures/bkg.jpg")
	background.xAnchor = 0.5
	background.yAnchor = 0.5
	-- Fit background to screen size
	local bg_width, bg_height = background:getAtlas():getTextureSize()
	background.xScale = director.displayWidth / bg_width
	background.yScale = director.displayHeight / bg_height

	-- Create grid background sprite
	gridSprite = director:createSprite( {
		x = director.displayCenterX, y = 0, 
		xAnchor = 0.5, 
		xScale = graphicsScale, 
		yScale = graphicsScale, 
		source = "textures/gamearea.png"
		} )
 	local sprite_w, sprite_h
	sprite_w, sprite_h = gridSprite:getAtlas():getTextureSize()
	gridSprite.y = 0
  
	-- Create left placard
	leftPlacard = director:createSprite(0, 0, "textures/info_panel.png")
	local plac_atlas = leftPlacard:getAtlas()
	local plac_w, plac_h = plac_atlas:getTextureSize()
	leftPlacard.x = 0
	leftPlacard.y = uiYPosition - plac_h * graphicsScale
	leftPlacard.xScale = graphicsScale
	leftPlacard.yScale = graphicsScale
	
	-- Create right placard
	rightPlacard = director:createSprite(0, 0, "textures/info_panel.png")
	rightPlacard.x = director.displayWidth - plac_w * graphicsScale
	rightPlacard.y = uiYPosition - plac_h * graphicsScale
	rightPlacard.xScale = graphicsScale
	rightPlacard.yScale = graphicsScale
  
	-- Create score label text
	scoreLabelText = director:createLabel( {
		x = 20 * fontScale, y = uiYPosition - 20 * fontScale, 
		w = director.displayWidth, h = actualFontHeight, 
		text="Score:",
		hAlignment="left", vAlignment="top", 
		font=defaultFont, 
		textXScale = fontScale, 
		textYScale = fontScale, 
		color = color.yellow
		})

	-- Create score label (displays actual score)
	scoreLabel = director:createLabel( {
		x = 80 * fontScale, y = uiYPosition - 20 * fontScale, 
		w = director.displayWidth, h = actualFontHeight, 
		hAlignment="left", vAlignment="top", 
		text="", 
		font=defaultFont, 
		textXScale = fontScale, 
		textYScale = fontScale, 
		})

	-- Create target score label text
	targetScoreLabelText = director:createLabel( {
		x = 20 * fontScale, y = uiYPosition - 40 * fontScale, 
		w = director.displayWidth / fontScale, h = actualFontHeight, 
		text="Target:",
		hAlignment="left", vAlignment="top", 
		font=defaultFont, 
		textXScale = fontScale, 
		textYScale = fontScale, 
		color = color.yellow
		})

	-- Create target score label (displays target score)
	targetScoreLabel = director:createLabel( {
		x = 80 * fontScale, y = uiYPosition - 40 * fontScale, 
		w = director.displayWidth, h = actualFontHeight, 
		hAlignment="left", vAlignment="top", 
		text="", 
		font=defaultFont, 
		textXScale = fontScale, 
		textYScale = fontScale, 
		})

	-- Create round label text
	roundLabelText = director:createLabel( {
		x = director.displayWidth - 100 * fontScale, y = uiYPosition - 20 * fontScale, 
		w = director.displayWidth, h = actualFontHeight, 
		text="Round:",
		hAlignment="left", vAlignment="top", 
		font=defaultFont, 
		textXScale = fontScale, 
		textYScale = fontScale, 
		color = color.yellow
		})

	-- Create round label
	roundLabel = director:createLabel( {
		x = -30 * fontScale, y = uiYPosition - 20 * fontScale, 
		w = director.displayWidth, h = actualFontHeight, 
		hAlignment="right", vAlignment="top", 
		text="", 
		font=defaultFont, 
		textXScale = fontScale, 
		textYScale = fontScale, 
		})

	-- Create timer label text
	timerLabelText = director:createLabel( {
		x = director.displayWidth - 100 * fontScale, y = uiYPosition - 40 * fontScale, 
		w = director.displayWidth, h = actualFontHeight, 
		text="Time:",
		hAlignment="left", vAlignment="top", 
		font=defaultFont, 
		textXScale = fontScale, 
		textYScale = fontScale, 
		color = color.yellow
		})

	-- Create timer label
	timerLabel = director:createLabel( {
		x = -30 * fontScale, y = uiYPosition - 40 * fontScale, 
		w = director.displayWidth, h = actualFontHeight, 
		hAlignment="right", vAlignment="top", 
		text="", 
		font=defaultFont, 
		textXScale = fontScale, 
		textYScale = fontScale, 
		})

	-- Create pause menu sprite (docked to top of screen)
	pauseSprite = director:createSprite( {
		x = director.displayCenterX, y = 0, 
		xAnchor = 0.5, 
		xScale = graphicsScale, 
		yScale = graphicsScale, 
		source = "textures/pause_icon.png"
		} )
	sprite_w, sprite_h = pauseSprite:getAtlas():getTextureSize()
	pauseSprite.y = uiYPosition - sprite_h * pauseSprite.yScale
	pauseSprite:addEventListener("touch", pauseGame)

	-- Create target gem sprite
	targetGemSprite = director:createSprite( {
		x = director.displayCenterX, y = gridActualHeight - gem.gemActualHeight / gem.gemBitmapHeight + gridOffsetY, 
		xAnchor = 0.5, 
		xScale = gem.gemActualWidth / gem.gemBitmapWidth, 
		yScale = gem.gemActualHeight / gem.gemBitmapHeight
		} )

end

function facebookButtonTouched(event)
	if (event.phase == "ended") then
		social:postScoreUpdate(lastRound, lastRoundScore)
	end
end

-- Initialise the Info Panel UI (Info panel shows information such as end of round score and round number)
function initInfoPanelUI()
	-- Create info panel
	infoPanel = director:createSprite(0, 0, "textures/popup.png")
	local panel_atlas = infoPanel:getAtlas()
	local panel_w, panel_h = panel_atlas:getTextureSize()
	infoPanel.x = director.displayWidth
	infoPanel.y = director.displayCenterY - (panel_h / 2) * graphicsScale
	infoPanel.xScale = graphicsScale
	infoPanel.yScale = graphicsScale
	-- Create info panel header
	infoPanelTitleLabel = director:createLabel( {
		x = panel_w / 2, y = panel_h / 2 - 100, 
		w = panel_w, h = panel_h, 
		text="Round Complete",
		hAlignment="center", vAlignment="middle", 
		font=defaultFont, 
		xAnchor = 0.5, 
		yAnchor = 0.5, 
		textXScale = fontScale / graphicsScale, -- Compound scaling factors as label is child of button
		textYScale = fontScale / graphicsScale, 
		})
	infoPanel:addChild(infoPanelTitleLabel)
	infoPanel:addEventListener("touch", infoPanelTouched)
	-- Create info panel message
	infoPanelMessageLabel = director:createLabel( {
		x = panel_w / 2, y = panel_h / 2 + 100, 
		w = panel_w, h = panel_h, 
		text="Score: 1000",
		hAlignment="center", vAlignment="middle", 
		font=defaultFont, 
		xAnchor = 0.5, 
		yAnchor = 0.5, 
		textXScale = fontScale / graphicsScale, -- Compound scaling factors as label is child of button
		textYScale = fontScale / graphicsScale, 
		})
	infoPanel:addChild(infoPanelMessageLabel)
  
	if (social.available) then
		-- Create Facebook share button
		local facebookButton = director:createSprite(0, 0, "textures/facebook_button.png")
		local fb_atlas = facebookButton:getAtlas()
		local fb_w, fb_h = fb_atlas:getTextureSize()
		facebookButton.x = panel_w / 2
		facebookButton.y = 0
		facebookButton.xAnchor = 0.5
		facebookButton.yAnchor = 1.0
		facebookButton.xScale = 2
		facebookButton.yScale = 2
		facebookButton:addEventListener("touch", facebookButtonTouched)
		infoPanel:addChild(facebookButton)
	end
end

function disableAds()
	-- Disable ads
	adverts:disable()
	-- Remove pause menu Ad button
	pauseMenu.removeNoAdsButton()
	-- Move UI to top of screen
	uiYPosition = director.displayHeight
	local plac_atlas = leftPlacard:getAtlas()
	local plac_w, plac_h = plac_atlas:getTextureSize()
	leftPlacard.y = uiYPosition - plac_h * graphicsScale
	rightPlacard.y = uiYPosition - plac_h * graphicsScale
	scoreLabelText.y = uiYPosition - 20 * fontScale
	scoreLabel.y = uiYPosition - 20 * fontScale
	targetScoreLabelText.y = uiYPosition - 40 * fontScale
	targetScoreLabel.y = uiYPosition - 40 * fontScale
	roundLabelText.y = uiYPosition - 20 * fontScale
	roundLabel.y = uiYPosition - 20 * fontScale
	timerLabelText.y = uiYPosition - 40 * fontScale
	timerLabel.y = uiYPosition - 40 * fontScale
	sprite_w, sprite_h = pauseSprite:getAtlas():getTextureSize()
	pauseSprite.y = uiYPosition - sprite_h * pauseSprite.yScale
  -- Log ads removed event
	stats:logEvent("Ads Removed")
end

function purchaseNoAds()
	store:purchaseNoAds()
end

-- Initialise the game
function init()
	-- Create a scene via the director to contain the main game view
	gameScene = director:createScene()

    function gameScene:suspend(event)
		system:pauseTimers()
		self:pauseTree()
		audio:pauseStream()
	end
	
	function gameScene:resume(event)
		audio:resumeStream()
		system:resumeTimers()
		if gameState ~= gameStates.paused then
		    self:resumeTree()
		end
	end
	
	system:addEventListener({"suspend", "resume"}, gameScene)

	-- Initialise audio
	initAudio()

	-- Show adverts
	if (adverts.enabled == true) then
	    adverts:show()
	    uiYPosition = director.displayHeight - 70
	end

	-- Initialise UI
	initUI()

	-- Create gems grid
	gemsGrid = grid.new(gemCountX, gemCountY, gridOffsetX, gridOffsetY, maxGemTypes)

	-- initialise info panel UI
	initInfoPanelUI()
  
	-- Create selector
	selector = selector.new()

	-- Create Pause Menu
	pauseMenu.init()

	-- Create Main Menu
	mainMenu.init()

	-- Add event touch and update handlers
	system:addEventListener("touch", touch)
	system:addEventListener("update", update)
end
