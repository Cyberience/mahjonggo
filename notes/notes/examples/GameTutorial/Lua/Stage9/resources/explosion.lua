--[[
A gem game obejct
--]]

module(..., package.seeall)

-- OO functions
require("class")

-- Create the explosion class
explosion = inheritsFrom(baseClass)

-- Creates an instance of a new explosion
function new(x, y)
	local o = explosion:create()
	explosion:init(o, x, y)
	return o
end

-- Initialise the explosion
function explosion:init(o, x, y)
	-- Create a particle emitter
	o.used = true
	-- Create particle system from plist
	o.particles = director:createParticles("textures/explosion.plist")
	o.particles.sourcePos.x = x / game.graphicsScale
	o.particles.sourcePos.y = y / game.graphicsScale
	o.particles.xScale = game.graphicsScale
	o.particles.yScale = game.graphicsScale
end

-- Start explosion
function explosion:restart()
	self.particles:reset()
	self.used = true
end

-- Update the explosion
function explosion:update()
	if (self.used == true and self.particles:isActive() == false) then
		self.used = false
	end
end


-- Explosions table
local explosions = {}

-- Create a new explosion
function createExplosion(x, y, type)
	-- Search for an unused particle emitter
	for i,exp in ipairs(explosions) do
		if (exp.used == false) then
			exp.particles.sourcePos.x = x / game.graphicsScale
			exp.particles.sourcePos.y = y / game.graphicsScale
			exp:restart()
			return exp
		end
	end

	-- No free particle emitter so create a new one
	local exp = new(x, y)
	table.insert(explosions, exp)
	return exp
end

-- Update all active explosions
function updateExplosions()
	for i,exp in ipairs(explosions) do
		if (exp.used) then
			exp:update()
		end
	end
end

-- Stop all explosions
function stopExplosions()
	for i,exp in ipairs(explosions) do
		-- Remove from scene
		exp.particles:removeFromParent()
		explosions[i] = nil
	end
end