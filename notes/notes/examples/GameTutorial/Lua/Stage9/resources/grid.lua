--[[
A grid game object
--]]

module(..., package.seeall)

-- OO functions
require("class")
require("gem")

-- Create the grid class
grid = inheritsFrom(baseClass)

-- Creates an instance of a new grid
function new(gems_wide, gems_high, gems_offset_x, gems_offset_y, max_gem_types)
	local o = grid:create()
	grid:init(o, gems_wide, gems_high, gems_offset_x, gems_offset_y, max_gem_types)
	return o
end

-- Convert screen x, y to grid pos
function grid:screenToGrid(x, y)
	local grid_x = math.floor((x - self.offsetX) / gem.gemActualWidth + 0.001)
	local grid_y = math.floor((y - self.offsetY) / gem.gemActualHeight + 0.001)
	return grid_x + 1, grid_y + 1
end

-- Get gem at screen x, y
function grid:getGem(x, y)
	local grid_x, grid_y = self:screenToGrid(x, y)
	return self.gemsGrid[grid_x][grid_y]
end

-- Add gem to matches table
function grid:addGemToMatches(gem)
	-- Check to make sure that the gem is not already present
	for i,g in ipairs(self.matchedGems) do
		if (g == gem) then
			return
		end
	end

	-- Add to matched gems table
	table.insert(self.matchedGems, gem)
end

-- Check for any 3+ matches and builds a table of gems that need to be removed
function grid:checkMatches()
	local match = false
	-- Clear ,atched gem list
	for i in pairs(self.matchedGems) do
		matchedGems[i] = nil
	end
	-- Check columns for a match
	for x = 1, self.width do
		local count = 0
		local focus
		local ox, oy
		for y = 1, self.height do
			local gem = self.gemsGrid[x][y]
			local current = gem.type
			if (count == 0) then
				focus = current
				count = 1
				ox = x
				oy = y
			elseif (current == focus) then
				count = count + 1
			end
			-- We reached the end of a sequence or the end of the column so check for min matches
			if (current ~= focus or y == self.height) then
				-- Remove matches in column
				if (count >= 3) then
					match = true
					for i = 1, count do
						self:addGemToMatches(self.gemsGrid[ox][oy])
						oy = oy + 1
					end
				end
				focus = current
				ox = x
				oy = y
				count = 1
			end
		end
	end	
	-- Check rows for a match
	for y = 1, self.height do
		local count = 0
		local focus
		local ox, oy
		for x = 1, self.width do
			local gem = self.gemsGrid[x][y]
			local current = gem.type
			if (count == 0) then
				focus = current
				count = 1
				ox = x
				oy = y
			elseif (current == focus) then
				count = count + 1
			end
			-- We reached the end of a sequence or the end of the row so check for min matches
			if (current ~= focus or x == self.width) then
				-- Remove matches in row
				if (count >= 3) then
					match = true
					for i = 1, count do
						self:addGemToMatches(self.gemsGrid[ox][oy])
						ox = ox + 1
					end
				end
				focus = current
				ox = x
				oy = y
				count = 1
			end
		end
	end

	-- Destroy any matched gems
	if (#self.matchedGems > 0) then
		audio:playSound("audio/gem_destroyed.raw")
	end
	for i,g in ipairs(self.matchedGems) do
		g:explode()
	end

	return match
end

-- Fix grid - removes initial 3+ matches
function grid:fix()
	-- We check the grids columns and rows for any 3 matches if found then we change one or more gems to the next colour gem 
	-- to remove the match. We keep re-checking the grid until no matches are found
	local redo = true
	while (redo) do
		redo = false
		-- Check columns for a match
		for x = 1, self.width do
			local count = 0
			local focus
			local ox, oy
			for y = 1, self.height do
				local current = self.gemsGrid[x][y].type
				if (count == 0) then
					focus = current
					count = 1
					ox = x
					oy = y
				elseif (current == focus) then
					count = count + 1
				end
				-- We reached the end of a sequence or the end of the column so check for min matches
				if (current ~= focus or y == self.height) then
					-- Remove matches in column
					if (count >= 3) then
						oy = oy + 1	-- Skip to next gem (prevents removing 2 gems in a 4 match)
						for i = 1, math.floor((count - 1) / 2) do
							local type = self.gemsGrid[ox][oy].type
							self.gemsGrid[ox][oy]:setType(((type + 1) % self.maxGemTypes) + 1)	-- Change to next gem in sequence
							oy = oy + 2
						end
						redo = true
					end
					focus = current
					ox = x
					oy = y
					count = 1
				end
			end
		end	
		-- Check rows for a match
		for y = 1, self.height do
			local count = 0
			local focus
			local ox, oy
			for x = 1, self.width do
				local current = self.gemsGrid[x][y].type
				if (count == 0) then
					focus = current
					count = 1
					ox = x
					oy = y
				elseif (current == focus) then
					count = count + 1
				end
				-- We reached the end of a sequence or the end of the row so check for min matches
				if (current ~= focus or x == self.width) then
					-- Remove matches in row
					if (count >= 3) then
						ox = ox + 1	-- Skip to next gem (prevents removing 2 gems in a 4 match)
						for i = 1, math.floor((count - 1) / 2) do
							local type = self.gemsGrid[ox][oy].type
							self.gemsGrid[ox][oy]:setType(((type + 1) % self.maxGemTypes) + 1)	-- Change to next gem in sequence
							ox = ox + 2
						end
						redo = true
					end
					focus = current
					ox = x
					oy = y
					count = 1
				end
			end
		end
	end
end

-- Reenerate a random grid of gems
function grid:regenerate()
	local gem_index = 1
	-- Generate a random grid
	for x = 1, self.width do
		for y = 1, self.height do
			-- if an empty grid cell is found then put the gem back into the cell from the gem matches list
			if (self.gemsGrid[x][y] == nil) then
				self.gemsGrid[x][y] = self.matchedGems[gem_index]
				self.matchedGems[gem_index] = nil
				gem_index = gem_index + 1
			end
			-- Reset the gems attributes
			self.gemsGrid[x][y]:setType(math.random(self.maxGemTypes))
			self.gemsGrid[x][y].sprite.x = (x - 1) * gem.gemActualWidth + self.offsetX
			self.gemsGrid[x][y].sprite.y = (y - 1) * gem.gemActualHeight + self.offsetY
			self.gemsGrid[x][y].sprite.alpha = 1
		end
	end	
	-- Fix any matches
	self:fix()
end

-- Initialise the grid
function grid:init(o, gems_wide, gems_high, gems_offset_x, gems_offset_y, max_gem_types)
	o.width = gems_wide
	o.height = gems_high
	o.offsetX = gems_offset_x
	o.offsetY = gems_offset_y
	o.maxGemTypes = max_gem_types
	o.matchedGems = {}

	-- Create grid of gem objects
	o.gemsGrid = {}
	for x = 1, gems_wide do
		o.gemsGrid[x] = {}
		for y = 0, gems_high do
			if (y == 0) then
				o.gemsGrid[x][y] = 1	-- We fill extra row with 1 to give lowest line of blocks something solid to rest on
			else
				o.gemsGrid[x][y] = gem.new(0, (x - 1) * gem.gemActualWidth + o.offsetX, (y - 1) * gem.gemActualHeight + o.offsetY)
			end
		end
	end	
end

-- Swap gems
function grid:swapGems(grid_x, grid_y)
	local gem1 = self.gemsGrid[grid_x[1]][grid_y[1]]
	local gem2 = self.gemsGrid[grid_x[2]][grid_y[2]]

	-- Start gem swap animation
	if (grid_x[1] == grid_x[2]) then
		tween:to(gem1.sprite, { y=gem2.sprite.y, time=0.5, easing=ease.sineIn } )
		tween:to(gem2.sprite, { y=gem1.sprite.y, time=0.5, easing=ease.sineIn } )
	elseif (grid_y[1] == grid_y[2]) then
		tween:to(gem1.sprite, { x=gem2.sprite.x, time=0.5, easing=ease.sineIn } )
		tween:to(gem2.sprite, { x=gem1.sprite.x, time=0.5, easing=ease.sineIn } )
	end

	-- Swap gems in the grid
	self.gemsGrid[grid_x[1]][grid_y[1]] = gem2
	self.gemsGrid[grid_x[2]][grid_y[2]] = gem1
end

-- Remove gem from grid
function grid:removeGem(gem)
	local grid_x, grid_y = self:screenToGrid(gem.sprite.x, gem.sprite.y)
	self.gemsGrid[grid_x][grid_y] = nil
end

-- Handle falling gems
function grid:gemsFalling()
	local falling = false
	local dt = system.deltaTime
	for x = 1, self.width do
		for y = 1, self.height do
			local g = self.gemsGrid[x][y]
			local gem_below = self.gemsGrid[x][y - 1]
			if (g ~= nil) then
				local old_x, old_y = self:screenToGrid(g.sprite.x, g.sprite.y)
				local dy = gem.gemFallSpeed * g.sprite.yScale * dt -- Scale fall speed to match different height displays
				if (dy > gem.gemActualHeight - 1) then
					dy = gem.gemActualHeight - 1
				end
				g.sprite.y = g.sprite.y - dy
				local new_x, new_y = self:screenToGrid(g.sprite.x, g.sprite.y)
				if (new_y == y) then
					-- Still in same block
					falling = true
				else
					-- Moved into new block below
					if (self.gemsGrid[new_x][new_y] == nil) then
						-- Remove from previous grid cell and insert into new empty cell below
						self.gemsGrid[old_x][old_y] = nil
						self.gemsGrid[new_x][new_y] = g
						falling = true
					else
						-- Position sprite because it may have gone over boundary
						g.sprite.y = (old_y - 1) * gem.gemActualHeight + self.offsetY
					end
				end
			end
		end
	end

	-- Traverse removed gems and drop them back into the grid
	local new_gem = false
	if (falling == false) then
		if (#self.matchedGems > 0) then
			falling = true
			for i = #self.matchedGems, 1, -1 do
				local g = self.matchedGems[i]
				local x, y = self:screenToGrid(g.sprite.x, g.sprite.y)
				if (self.gemsGrid[x][self.height] == nil) then
					-- Add new gem
					self.gemsGrid[x][self.height] = g
					g:setType(math.random(self.maxGemTypes))
					g.sprite.y = self.height * gem.gemActualHeight + self.offsetY
					tween:to(g.sprite, { alpha=1, time=0.5, easing=ease.sineOut } )
					table.remove(self.matchedGems, i)
					new_gem = true
				end
			end
		end
	end
	if (new_gem == true) then
		audio:playSound("audio/gem_land.raw")
	end

	if (falling == false) then
		if (self:checkMatches() == false) then
			-- Switch back to waiting for gem selection state
			game.changeGameState(game.gameStates.waitingFirstGem)
		end
	end

end
