#section: Examples - Quick
#category: Tutorial
#tool: Quick
My first Marmalade Quick App

The objectives of this tutorial series include:
    Build a Match-3 game (similar to games such as Bejeweled and Candy Crush) from scratch using Marmalade Quick in stages
    Introduce key concepts of Marmalade Quick as we progress from stage to stage
    Cover more advanced topics such as supporting in-app purchase, ads and social integration

Stage 9: Network Services

The aims of this tutorial stage include:
    Create a Facebook app and post a status update to the users Facebook
    Setup a Flurry account and track user interactions within the app using Analytics
    Show Ads within the app to generate revenue
    Set up ads using Inner-active
    Use billing to allow users to remove ads
    Testing in-app purchases on iOS and Android
