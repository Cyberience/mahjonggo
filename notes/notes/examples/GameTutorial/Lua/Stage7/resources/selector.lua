--[[
A selector game obejct
--]]

module(..., package.seeall)

-- OO functions
require("class")
require("gem")

-- Constants

-- Create the selector class
selector = inheritsFrom(baseClass)

-- Creates an instance of a new selector
function new()
	local o = selector:create()
	selector:init(o)
	return o
end

-- Initialise the selector
function selector:init(o)
	o.sprite = director:createSprite(0, 0, "textures/selector.png")
	o.sprite.isVisible = true
	o.sprite.xAnchor = 0.5
	o.sprite.yAnchor = 0.5
	o.sprite.xScale = gem.gemActualWidth / gem.gemBitmapWidth
	o.sprite.yScale = gem.gemActualHeight / gem.gemBitmapHeight
	o.type = type
	tween:to(o.sprite, { alpha=0.3, time=1, mode="mirror", easing=ease.sineInOut } )
end

-- Show the selector
function selector:show(x, y)
	self.sprite.x = x
	self.sprite.y = y
	self.sprite.isVisible = true
end

-- Hide the selector
function selector:hide()
	self.sprite.isVisible = false
end

