#section: Examples - Quick
#category: Tutorial
#tool: Quick
My first Marmalade Quick App

The objectives of this tutorial series include:
    Build a Match-3 game (similar to games such as Bejeweled and Candy Crush) from scratch using Marmalade Quick in stages
    Introduce key concepts of Marmalade Quick as we progress from stage to stage
    Cover more advanced topics such as supporting in-app purchase, ads and social integration

Stage 7: Game Play

The aims of this tutorial stage include:
    Introduce key game play concepts
    Walk through important sections of the the game code
