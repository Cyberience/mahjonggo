--[[
Stage3 - Scenes and multiple files
--]]

--require("mobdebug").start()

-- Set up 'exit' event listener to allow Ctrl-R functionality
function exit(event)
	dbg.print("unrequiring")
	unrequire("class")
	unrequire("game")
	unrequire("grid")
	unrequire("gem")
	unrequire("mainMenu")
	unrequire("pauseMenu")
end
system:addEventListener("exit", exit)

require("mainMenu")
require("pauseMenu")
require("game")

-- Switch to specific scene
function switchToScene(scene_name)
	if (scene_name == "game") then
		director:moveToScene(gameScene, {transitionType="slideInL", transitionTime=0.5})
	elseif (scene_name == "main") then
		director:moveToScene(menuScene, {transitionType="slideInL", transitionTime=0.5})
	elseif (scene_name == "pause") then
		director:moveToScene(pauseScene, {transitionType="slideInL", transitionTime=0.5})
	end
end

