--[[
A gem game object
--]]

module(..., package.seeall)

-- OO functions
require("class")

-- Create the gem class
gem = inheritsFrom(baseClass)

-- Constants
width = 40
height = 40

-- Creates an instance of a new gem
function new(type, x, y)
	local o = gem:create()
	gem:init(o, type, x, y)
	return o
end

-- Initialise the gem
function gem:init(o, type, x, y)
  -- Create a sprite
	o.sprite = director:createSprite(x, y, "textures/gem" .. type .. ".png")
  
  -- Calculate scale based on sprites texture size
  o.sprite.xScale = width / o.sprite.w
  o.sprite.yScale = height / o.sprite.h
	o.type = type
end


