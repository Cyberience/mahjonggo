--[[
Main game
--]]

require("grid")


local currentScore = 0
local maxGemTypes = 5				-- Maximum different types of gems
local gemCountX = 8					-- Total columns in gem grid
local gemCountY = 8					-- Total rows in gem grid

-- Creaté the game scene
gameScene = director:createScene()

-- Create game background
local background = director:createSprite(director.displayCenterX, director.displayCenterY, "textures/bkg.jpg")
background.xAnchor = 0.5
background.yAnchor = 0.5
-- Fit background to screen size
background.xScale = director.displayWidth / 768
background.yScale = director.displayHeight / 1136

-- Create gems grid
gemsGrid = grid.new(gemCountX, gemCountY, maxGemTypes)

-- Touch event handler (called when the user touches the screen)
local touch = function(event)
	if (director:getCurrentScene() == gameScene and event.phase == "ended") then
		if (event.y < gemCountY * gem.height) then
			-- Get tapped grid gem
			local tappedGem = gemsGrid:getGem(event.x, event.y)
			-- Create a tween that scales the gem up and down
			tween:to(tappedGem.sprite, {
				y=tappedGem.sprite.y + 10, 
				time=0.25, 
				mode="mirror"
				} )
		end
	end
end
-- Add system event touch handler
system:addEventListener("touch", touch)

-- Create a label to display the score
local scoreLabel = director:createLabel( {
	x=0, y=director.displayHeight - 30, 
	w=director.displayWidth, h=30, 
	hAlignment="left", vAlignment="top", 
	text="0", 
	color=color.yellow
	})

-- Pause game event handler (called by pauselabel)
function pauseGame(event)
	if (event.phase == "ended") then
    -- Switch to the pause scene
    switchToScene("pause")
  end
end

-- Create pause menu sprite (docked to top of screen)
local pause_sprite = director:createSprite( {
	x = director.displayCenterX, y = 0, 
	xAnchor = 0.5, 
	source = "textures/pause_icon.png"
  })
sprite_w, sprite_h = pause_sprite:getAtlas():getTextureSize()
pause_sprite.y = director.displayHeight - sprite_h
pause_sprite:addEventListener("touch", pauseGame)



