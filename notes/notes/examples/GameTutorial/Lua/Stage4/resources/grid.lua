--[[
A grid game object
--]]

module(..., package.seeall)

require("class")
require("gem")

-- Create the grid class
grid = inheritsFrom(baseClass)

-- Creates an instance of a new grid
function new(gems_wide, gems_high, max_gem_types)
	local o = grid:create()
	grid:init(o, gems_wide, gems_high, max_gem_types)
	return o
end

-- Initialise the grid
function grid:init(o, gems_wide, gems_high, max_gem_types)
	-- Create grid of gem objects
	o.gemsGrid = {}
	for x = 1, gems_wide do
		o.gemsGrid[x] = {}
		for y = 1, gems_high do
			o.gemsGrid[x][y] = gem.new(math.random(max_gem_types), (x - 1) * gem.width, (y - 1) * gem.height)
		end
	end	
end

-- Convert screen x, y to grid pos
function grid:screenToGrid(x, y)
	local grid_x = math.floor(x / gem.width)
	local grid_y = math.floor(y / gem.height)
	return grid_x + 1, grid_y + 1
end

-- Get gem at screen x, y
function grid:getGem(x, y)
	local grid_x, grid_y = self:screenToGrid(x, y)
	return self.gemsGrid[grid_x][grid_y]
end



