#section: Examples - Quick
#category: Tutorial
#tool: Quick
My first Marmalade Quick App

The objectives of this tutorial series include:
    Build a Match-3 game (similar to games such as Bejeweled and Candy Crush) from scratch using Marmalade Quick in stages
    Introduce key concepts of Marmalade Quick as we progress from stage to stage
    Cover more advanced topics such as supporting in-app purchase, ads and social integration

Stage 1: Setting up a Project and Displaying a gem

The aims of this tutorial stage include:
    Show you how to create a project using the Marmalade Hub and display a single gem on screen
    Using the simulator and basic deployment to device
    Using ZeroBrane Studio to edit code and launch the simulator
