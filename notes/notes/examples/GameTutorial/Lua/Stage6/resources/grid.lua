--[[
A grid game object
--]]

module(..., package.seeall)

-- OO functions
require("class")
require("gem")

-- Create the grid class
grid = inheritsFrom(baseClass)

-- Creates an instance of a new grid
function new(gems_wide, gems_high, gems_offset_x, gems_offset_y, max_gem_types)
	local o = grid:create()
	grid:init(o, gems_wide, gems_high, gems_offset_x, gems_offset_y, max_gem_types)
	return o
end

-- Initialise the grid
function grid:init(o, gems_wide, gems_high, gems_offset_x, gems_offset_y, max_gem_types)
	o.width = gems_wide
	o.height = gems_high
	o.offsetX = gems_offset_x
	o.offsetY = gems_offset_y
	-- Create grid of gem objects
	o.gemsGrid = {}
	for x = 1, gems_wide do
		o.gemsGrid[x] = {}
		for y = 0, gems_high do
			if (y == 0) then
				o.gemsGrid[x][y] = 1	-- We fill extra row with 1 to give lowest line of blocks something solid to rest on
			else
				o.gemsGrid[x][y] = gem.new(math.random(max_gem_types), (x - 1) * gem.gemActualWidth + o.offsetX, (y - 1) * gem.gemActualHeight + o.offsetY)
			end
		end
	end	
end

-- Convert screen x, y to grid pos
function grid:screenToGrid(x, y)
	local grid_x = math.floor((x - self.offsetX) / gem.gemActualWidth + 0.001)
	local grid_y = math.floor((y - self.offsetY) / gem.gemActualHeight + 0.001)
	return grid_x + 1, grid_y + 1

end

-- Get gem at screen x, y
function grid:getGem(x, y)
	local grid_x, grid_y = self:screenToGrid(x, y)
	return self.gemsGrid[grid_x][grid_y]
end

-- Removes a gem from the grid
function grid:removeGem(x, y)
	local grid_x, grid_y = self:screenToGrid(x, y)
  if (self.gemsGrid[grid_x][grid_y] ~= nil) then
    self.gemsGrid[grid_x][grid_y].sprite:removeFromParent()
    self.gemsGrid[grid_x][grid_y].sprite = nil
    self.gemsGrid[grid_x][grid_y] = nil
  end
end

-- Handle falling gems
function grid:gemsFalling()
	local falling = false
  local dt = system.deltaTime
	for x = 1, self.width do
		for y = 1, self.height do
			local g = self.gemsGrid[x][y]
			local gem_below = self.gemsGrid[x][y - 1]
			if (g ~= nil) then
				local old_x, old_y = self:screenToGrid(g.sprite.x, g.sprite.y)
        local dy = gem.gemFallSpeed * g.sprite.yScale * dt -- Scale fall speed to match different height displays
        if (dy > gem.gemActualHeight - 1) then
          dy = gem.gemActualHeight - 1
        end
				g.sprite.y = g.sprite.y - dy
				local new_x, new_y = self:screenToGrid(g.sprite.x, g.sprite.y)
				if (new_y == y) then
					-- Still in same block
					falling = true
				else
					-- Moved into new block below
					if (self.gemsGrid[new_x][new_y] == nil) then
						-- Remove from previous grid cell and insert into new empty cell below
						self.gemsGrid[old_x][old_y] = nil
						self.gemsGrid[new_x][new_y] = g
						falling = true
					else
						-- Position sprite because it may have gone over boundary
						g.sprite.y = (old_y - 1) * gem.gemActualHeight + self.offsetY
					end
				end
			end
		end
	end
end


