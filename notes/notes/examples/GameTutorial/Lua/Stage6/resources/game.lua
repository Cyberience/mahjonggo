--[[
Main game
--]]

module(..., package.seeall)

-- Global constants
maxGemTypes = 5				-- Maximum different types of gems
gemCountX = 8					-- Total columns in gem grid
gemCountY = 8					-- Total rows in gem grid
fontHeight = 15										-- Pixel height of font
fontDesignWidth = 320							-- Font was designed to be displayed on a 320 wide screen
graphicDesignWidth = 768					-- Graphics were designed to be displayed on a 768 wide screen
graphicsScale = director.displayWidth / graphicDesignWidth	-- Graphics are designed for 768 wide screen so we scale to native screen size
gridOffsetX = 41 * graphicsScale                    -- Grid offset screen position on x-axis
gridOffsetY = 37 * graphicsScale                    -- Grid offset screen position on y-axis

require("grid")

-- Font is correct size on 320 wide screen so we scale to match native screen size
fontScale = director.displayWidth / fontDesignWidth
-- Graphics are designed for 768 wide screen so we scale to native screen size
graphicsScale = director.displayWidth / graphicDesignWidth	
-- The actual pixel height of the font
actualFontHeight = fontHeight * fontScale

local currentScore = 0


-- Creaté the game scene
gameScene = director:createScene()

-- Create game background
local background = director:createSprite(director.displayCenterX, director.displayCenterY, "textures/bkg.jpg")
background.xAnchor = 0.5
background.yAnchor = 0.5
-- Fit background to screen size
local bg_width, bg_height = background:getAtlas():getTextureSize()
background.xScale = director.displayWidth / bg_width
background.yScale = director.displayHeight / bg_height
-- Create grid background sprite
gridSprite = director:createSprite( {
	x = director.displayCenterX, y = 0, 
	xAnchor = 0.5, 
	xScale = graphicsScale, 
	yScale = graphicsScale, 
	source = "textures/gamearea.png"
	} )
local sprite_w, sprite_h
sprite_w, sprite_h = gridSprite:getAtlas():getTextureSize()

-- Create gems grid
gemsGrid = grid.new(gemCountX, gemCountY, gridOffsetX, gridOffsetY, maxGemTypes)

-- Touch event handler (called when the user touches the screen)
local touch = function(event)
	if (director:getCurrentScene() == gameScene and event.phase == "ended") then
		if (event.y < gemCountY * gem.gemActualHeight) then
			-- Remove tapped gem
			gemsGrid:removeGem(event.x, event.y)
		end
	end
end
-- Add system event touch handler
system:addEventListener("touch", touch)

-- Game update loop
local update = function(event)
	-- Update falling gems in grid
	gemsGrid:gemsFalling()
end
-- Add system event update handler
system:addEventListener("update", update)

-- Create a label to display the score
local scoreLabel = director:createLabel( {
	x=0, y=director.displayHeight - actualFontHeight, 
  w=director.displayWidth, h=actualFontHeight, 
  textXScale = fontScale, 
  textYScale = fontScale, 
	hAlignment="left", vAlignment="top", 
	text="0", 
	color=color.yellow
	})

-- Pause game event handler (called by pauselabel)
function pauseGame(event)
	if (event.phase == "ended") then
    -- Switch to the pause scene
    switchToScene("pause")
  end
end

-- Create pause menu sprite (docked to top of screen)
local pause_sprite = director:createSprite( {
  x = director.displayCenterX, y = 0, 
  xAnchor = 0.5, 
  xScale = graphicsScale, 
  yScale = graphicsScale, 
  source = "textures/pause_icon.png"
  } )
sprite_w, sprite_h = pause_sprite:getAtlas():getTextureSize()
pause_sprite.y = director.displayHeight - sprite_h * pause_sprite.yScale
pause_sprite:addEventListener("touch", pauseGame)


