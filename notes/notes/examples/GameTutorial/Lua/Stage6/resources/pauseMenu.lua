--[[
Pause Menu
--]]

-- Scene will now handle touch
function setUpHandler()
    pauseScene.isTouchable = true
end

-- Create a scene to contain the main menu
pauseScene = director:createScene()
pauseScene:addEventListener("setUp", setUpHandler)
-- UI
local continueGameButton
local exitGameButton


-- Continue game event handler, called when the user taps the Continue Game button
function continueGame(event)
	-- Switch to game scene
    pauseScene.isTouchable = false
	switchToScene("game")
end

-- Exit game event handler, called when the user taps the Exit Game button
function exitGame(event)
	-- Switch to main menu scene
    pauseScene.isTouchable = false
	switchToScene("main")
end


-- Create menu background
local background = director:createSprite(director.displayCenterX, director.displayCenterY, "textures/menu_bkg.jpg")
background.xAnchor = 0.5
background.yAnchor = 0.5
-- Fit background to screen size
local bg_width, bg_height = background:getAtlas():getTextureSize()
background.xScale = director.displayWidth / bg_width
background.yScale = director.displayHeight / bg_height

-- Create Continue Game button
continueGameButton = director:createSprite(director.displayCenterX, 0, "textures/button_bg.png")
local atlas = continueGameButton:getAtlas()
local atlas_w, atlas_h = atlas:getTextureSize()
local button_height = atlas_h * game.graphicsScale
local y_pos = button_height * 2
continueGameButton.y = y_pos
continueGameButton.xAnchor = 0.5
continueGameButton.yAnchor = 0
continueGameButton.xScale = game.graphicsScale
continueGameButton.yScale = game.graphicsScale
continueGameButton:addEventListener("touch", continueGame)
-- Create Continue Game button text
local label = director:createLabel( {
	x = 0, y = 0, 
	w = atlas_w, h = atlas_h, 
	hAlignment="centre", vAlignment="middle", 
	font=game.defaultFont, 
	text="Continue", 
	textXScale = game.fontScale / game.graphicsScale,	-- Compound scaling factors as label ie child of button
	textYScale = game.fontScale / game.graphicsScale, 
})
continueGameButton:addChild(label)
y_pos = y_pos - button_height

-- Create Exit Game button
exitGameButton = director:createSprite(director.displayCenterX, y_pos, "textures/button_bg.png")
exitGameButton.xAnchor = 0.5
exitGameButton.yAnchor = 0
exitGameButton.xScale = game.graphicsScale
exitGameButton.yScale = game.graphicsScale
exitGameButton:addEventListener("touch", exitGame)
-- Create Exit Game button text
local label = director:createLabel( {
	x = 0, y = 0, 
	w = atlas_w, h = atlas_h, 
	hAlignment="centre", vAlignment="middle", 
	font=game.defaultFont, 
	text="Exit Game", 
	textXScale = game.fontScale / game.graphicsScale, -- Compound scaling factors as label ie child of button
	textYScale = game.fontScale / game.graphicsScale, 
})
exitGameButton:addChild(label)




