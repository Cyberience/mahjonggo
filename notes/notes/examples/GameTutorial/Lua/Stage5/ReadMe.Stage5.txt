#section: Examples - Quick
#category: Tutorial
#tool: Quick
My first Marmalade Quick App

The objectives of this tutorial series include:
    Build a Match-3 game (similar to games such as Bejeweled and Candy Crush) from scratch using Marmalade Quick in stages
    Introduce key concepts of Marmalade Quick as we progress from stage to stage
    Cover more advanced topics such as supporting in-app purchase, ads and social integration

Stage 5: Supporting Multiple Screen Resolutions

The aims of this tutorial stage include:
    Introduce methods for supporting multiple resolutions
    Show how to use the Virtual Resolution system
    Show how to use resolution independent design
