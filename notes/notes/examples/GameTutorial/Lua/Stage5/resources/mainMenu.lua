--[[
Main Menu
--]]

-- Scene will now handle touch
function setUpHandler()
    menuScene.isTouchable = true
end

-- Create a scene to contain the main menu
menuScene = director:createScene()
menuScene:addEventListener("setUp", setUpHandler)
-- UI
local playButton
local playText

-- New game event handler, called when the user taps the New Game button
function newGame(event)
    menuScene.isTouchable = false
	-- Switch to game scene
	switchToScene("game")
end

-- Create menu background
local background = director:createSprite(director.displayCenterX, director.displayCenterY, "textures/menu_bkg.jpg")
background.xAnchor = 0.5
background.yAnchor = 0.5
-- Fit background to screen size
local bg_width, bg_height = background:getAtlas():getTextureSize()
background.xScale = director.displayWidth / bg_width
background.yScale = director.displayHeight / bg_height

-- Create Start Game button
local y_pos = director.displayHeight / 3
playButton = director:createSprite(director.displayCenterX, y_pos, "textures/info_panel.png")
playButton.xAnchor = 0.5
playButton.yAnchor = 0.5
playButton.xScale = game.graphicsScale * 1.5
playButton.yScale = game.graphicsScale * 1.5
playButton:addEventListener("touch", newGame)
-- Create Start Game button text
playText = director:createSprite(director.displayCenterX, y_pos, "textures/play.png")
playText.xAnchor = 0.5
playText.yAnchor = 0.5
playText.xScale = game.graphicsScale
playText.yScale = game.graphicsScale
 
