--[[
A gem game object
--]]

module(..., package.seeall)

-- OO functions
require("class")
require("game")

-- Create the gem class
gem = inheritsFrom(baseClass)

-- Constants
gemActualWidth = director.displayWidth / game.gemCountX	-- The actual device coordinate width of the gem
gemActualHeight = gemActualWidth						            -- The actual device coordinate height of the gem

-- Creates an instance of a new gem
function new(type, x, y)
	local o = gem:create()
	gem:init(o, type, x, y)
	return o
end

-- Initialise the gem
function gem:init(o, type, x, y)
  -- Create a sprite
	o.sprite = director:createSprite(x, y, "textures/gem" .. type .. ".png")
  
  -- Calculate scale based on sprites texture size
  o.sprite.xScale = gemActualWidth / o.sprite.w
  o.sprite.yScale = gemActualHeight / o.sprite.h
	o.type = type
end


