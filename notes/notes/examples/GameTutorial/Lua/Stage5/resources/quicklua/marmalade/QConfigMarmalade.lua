--[[/*
 * (C) 2012-2013 Marmalade.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */--]]

--[[
/**
We reuse the config table from a project's config.lua and add ICF functions to
it. config.lua always loads before QXXX.lua files.
*/
--]]

--[[
    /**
    Get a runtime configuration setting from the ICF file as a string.
    @return A triplet of: success (bool), value (string), error (string)
    An existing string can be passed as the third parameter in which
    case, *if the setting is found*, that string will be updated with the
    value and the returned string will be a reference to the same string
    object. If the setting is not found in this situation, the string
    passed in will not be updated. If no string was passed in originally,
    no string is returned (success, value = config:getString() would result
    in value being nil)
    Error is a string describing the error, or nil if no error occurred.
    */
--]]
function config:getIcfString(group, name)
	return quick.QConfig:getIcfString(group, name)
end

--[[
    /**
    Get a runtime configuration setting from the ICF file as an integer.
    @return A triplet of: success (bool), value (number), error (string)
    An existing number reference can be passed as the third parameter in which
    case, *if the setting is found*, that reference will be updated with the
    value. If the setting is not found in this situation, the string
    passed in will not be updated. If no string was passed in originally,
    no string is returned (success, value = config:getString() would result
    in value being nil)
    Error is a string describing the error, or nil if no error occurred.
    */
--]]
function config:getIcfInt(group, name)
	return quick.QConfig:getIcfInt(group, name)
end
