/*
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This document is protected by copyright, and contains information
 * proprietary to Marmalade.
 *
 * This file consists of source code released by Marmalade under
 * the terms of the accompanying End User License Agreement (EULA).
 * Please do not use this program/source code before you have read the
 * EULA and have agreed to be bound by its terms.
 */

#ifndef __ADS_H
#define __ADS_H

#include <string>
#include "s3eWebView.h"

/**
 * @class Ads
 *
 * @brief Unified ads
 *
 * Displays html based ads and enables interaction with them via a web view
 *
 * Example usage:
 *
 *    // Create ads view
 *    g_pAds = new Ads();
 *    if (g_pAds->Init())
 *        g_pAds->NewAd(y, height, Ads::admoda, Ads::banner, "your_ad_provider_id");
 *
 * Replace your_ad_provider_id with the ID that was provided by the ad provider
 *
 * The ads system supports a number of ad providers (see eAdProvider for list) and supports banner / app wall ad types.
 *
 */
class Ads
{
public:

    /**
     * @enum  eAdProvider
     *
     * @brief Currently supported ad providers.
     */
    enum eAdProvider
    {
        inneractive,
        leadbolt,
        admoda
    };
    /**
     * @enum  eAdType
     *
     * @brief Currently supported ad types.
     */
    enum eAdType
    {
        banner,
        wall
    };
protected:
    std::string m_Platform;     // The device platform
    std::string m_Url;          // The url
    s3eWebView* m_View;         // The Marmalade web view object
    eAdProvider m_Provider;     // Ad provider
    eAdType     m_Type;         // Ad type
    std::string m_Id;           // Ad provider identifier
    int         m_Y;            // Y-axis position of ad
    int         m_H;            // Height of the ad

    void        NavigateAd(const std::string& html);
    bool        NewInneractiveAd();
    bool        NewLeadboltAd();
    bool        NewAdmodaAd();
public:
    eAdProvider GetProvider() const { return m_Provider; }
    eAdType     GetType() const     { return m_Type; }
    bool        IsEnabled();

public:
    Ads() : m_View(0) {}
    ~Ads();

    /**
     * @fn    bool Ads::Init();
     *
     * @brief Initialises the ads system.
     *
     * @return    true if it succeeds, false if it fails.
     */
    bool    Init();

    /**
     * @fn    void Ads::Disable();
     *
     * @brief Disables the ads system
     *
     * Permanently disables the ads system and removes the ads views
     */
    void    Disable();

    /**
     * @fn    bool Ads::NewAd(int y, int height, eAdProvider provider, eAdType type, const char *id);
     *
     * @brief Creates a new ad.
     *
     * @param y           The ads screen y coordinate.
     * @param height      The ads height.
     * @param provider    The ad provider.
     * @param type        The ads type.
     * @param id          The ad provider identifier.
     *
     * @return    true if it succeeds, false if it fails.
     */
    bool    NewAd(int y, int height, eAdProvider provider, eAdType type, const char *id);

    /**
     * @fn    bool Ads::NewAd();
     *
     * @brief Creates a new ad of previously specified type
     *
     * @return    true if it succeeds, false if it fails.
     */
    bool    NewAd();

    // Internal
    static int32    LoadedCallback(s3eWebView *instance, void *systemData, void *userData);
    static int32    ErrorCallback(s3eWebView *instance, void *systemData, void *userData);
    static int32    StartedLoadingCallback(s3eWebView *instance, void *systemData, void *userData);
};

extern Ads* g_pAds;

#endif // __ADS_H
