/*
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This document is protected by copyright, and contains information
 * proprietary to Marmalade.
 *
 * This file consists of source code released by Marmalade under
 * the terms of the accompanying End User License Agreement (EULA).
 * Please do not use this program/source code before you have read the
 * EULA and have agreed to be bound by its terms.
 */

#ifndef __STORE_H
#define __STORE_H

#include "IwBilling.h"

/**
 * @class Store
 *
 * @brief Store class enables in-app purchasing of the no ads item.
 *
 * Example usage:
 *
 *    g_pStore = new Store();
 *    if (g_pStore->Init())
 *        g_pStore->PurchaseNoAds();
 */
class Store
{
protected:

public:
    Store() {}
    ~Store();

    /**
     * @fn    bool Store::Init();
     *
     * @brief Initialises the store.
     *
     * @return    true if it succeeds, false if it fails.
     */
    bool    Init();

    /**
     * @fn    bool Store::PurchaseNoAds();
     *
     * @brief Purchases the noads in-app item.
     *
     * @return    true if it succeeds, false if it fails.
     */
    bool    PurchaseNoAds();

    // Internal
    bool    FinishTransaction(void *transaction_data);
    static void ErrorCallback(void* caller, IwBilling::CIwBillingErrorData* data);
    static void ProductInfoAvailableCallback(void* caller, IwBilling::CIwBillingInfoAvailableData* data);
    static void ReceiptAvailableCallback(void* caller, IwBilling::CIwBillingReceiptAvailableData* data);
    static void RefundCallback(void* caller, IwBilling::CIwBillingRefundData* data);

};

extern Store* g_pStore;

#endif // __STORE_H
