/*
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This document is protected by copyright, and contains information
 * proprietary to Marmalade.
 *
 * This file consists of source code released by Marmalade under
 * the terms of the accompanying End User License Agreement (EULA).
 * Please do not use this program/source code before you have read the
 * EULA and have agreed to be bound by its terms.
 */

#include "ads.h"
#include "s3eFile.h"
#include "s3eOSExec.h"
#include "IwGx.h"

Ads* g_pAds = 0;

Ads::~Ads()
{
    // Destroy the web view
    if (m_View)
    {
        s3eWebViewUnRegister(S3E_WEBVIEW_STARTED_LOADING, Ads::StartedLoadingCallback, m_View);
        s3eWebViewUnRegister(S3E_WEBVIEW_FINISHED_LOADING, Ads::LoadedCallback, m_View);
        s3eWebViewUnRegister(S3E_WEBVIEW_FAILED_LOADING, Ads::ErrorCallback, m_View);
        s3eWebViewDestroy(m_View);
        m_View = NULL;
    }
}

bool Ads::Init()
{
    // Check ads are enabled
    if (!IsEnabled())
        return false;

    // Check that the web view extension is available
    if (s3eWebViewAvailable() == S3E_FALSE)
        return false;

    // Create the web view
    m_View = s3eWebViewCreate(true);
    if (!m_View)
        return false;

    // Check that the platform is supported
    m_Platform = s3eDeviceGetString(S3E_DEVICE_OS);
    if (m_Platform != "ANDROID" && m_Platform != "IPHONE" && m_Platform != "WINDOWS" & m_Platform != "OSX")
        return false;

    // Register callbacks
    s3eWebViewRegister(S3E_WEBVIEW_STARTED_LOADING, Ads::StartedLoadingCallback, this, m_View);
    s3eWebViewRegister(S3E_WEBVIEW_FINISHED_LOADING, Ads::LoadedCallback, this, m_View);
    s3eWebViewRegister(S3E_WEBVIEW_FAILED_LOADING, Ads::ErrorCallback, this, m_View);

    return true;
}

bool Ads::IsEnabled()
{
    // Check storage to see if ads are enabled
    bool enabled = true;
    s3eFile* fh = s3eFileOpen("ads.bin", "rb");
    if (fh != NULL)
    {
        s3eFileRead(&enabled, sizeof(enabled), 1, fh);
        s3eFileClose(fh);
    }

    return enabled;
}

void Ads::Disable()
{
    // Save ads disabled state to storage
    bool enabled = false;
    s3eFile* fh = s3eFileOpen("ads.bin", "wb");
    if (fh != NULL)
    {
        s3eFileWrite(&enabled, sizeof(enabled), 1, fh);
        s3eFileClose(fh);
    }

    // Destroy the web view
    if (m_View)
    {
        s3eWebViewUnRegister(S3E_WEBVIEW_STARTED_LOADING, Ads::StartedLoadingCallback, m_View);
        s3eWebViewUnRegister(S3E_WEBVIEW_FINISHED_LOADING, Ads::LoadedCallback, m_View);
        s3eWebViewUnRegister(S3E_WEBVIEW_FAILED_LOADING, Ads::ErrorCallback, m_View);
        s3eWebViewDestroy(m_View);
        m_View = NULL;
    }
}

void Ads::NavigateAd(const std::string& html)
{
    // Save the html that will display the ad
    s3eFile* fh = s3eFileOpen("ad_html.html", "wb");
    s3eFileWrite(html.c_str(), html.length(), 1, fh);
    s3eFileClose(fh);

    // Show the web view
    s3eWebViewShow(m_View, 0, m_Y, IwGxGetScreenWidth(), m_H);

    // Navigate to the saved ad html file
    s3eWebViewNavigate(m_View, "ram://ad_html.html");
}

bool Ads::NewInneractiveAd()
{
    std::string portal_id = "642";
    if (m_Platform == "ANDROID")
        portal_id = "559";
    else
    if (m_Platform != "IPHONE")
        return false;

    // Generate ad html
    std::string html;
    html.reserve(2000);
    html.append("<html><head>");
            html.append("<meta name='viewport' content='width=device-width, initial-scale=1.0, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no' />");
            html.append("<style> #iaAdContainerDiv { text-align: center; } </style>");
        html.append("</head><body>");
            html.append("<script>");
                html.append("var APP_ID = '");
                html.append(m_Id);
                html.append("'; ");
                html.append("var PORTAL = ");
                html.append(portal_id);
                html.append("; ");
                html.append("var CATEGORY = '';");
                html.append("var KEYWORDS = ''; ");
                html.append("var GPS_COORDINATES = ''; ");
                html.append("var LOCATION = ''; ");
                html.append("var IMPRESSION_PIXEL = ''; ");
                html.append("var CLICK_PIXEL = ''; ");
                html.append("var FAILOVER = 'No ad'; ");
                html.append("var IS_MOBILE_WEB = false; ");
                html.append("var IS_ORMMA_SUPPORT = false; ");
                html.append("var IS_MRAID_SUPPORT = false; ");
                if (m_Type == wall)
                    html.append("var IS_INTERSTITIAL_AD = 'true'; ");
                else
                    html.append("var IS_INTERSTITIAL_AD = 'false'; ");
                html.append("document.write(\"<script language='javascript' src='http://ad-tag.inner-active.mobi/simpleM2M/RequestTagAd?v=\" + ((IS_ORMMA_SUPPORT) ? ((IS_MRAID_SUPPORT) ? \"Stag-2.1.0&f=116\" : \"Stag-2.1.0&f=52\") : ((IS_MRAID_SUPPORT) ? \"Stag-2.1.0&f=84\" : \"Stag-2.0.1&f=20\")) + ((IS_INTERSTITIAL_AD) ? \"&fs=true\" : \"&fs=false\") + \"&aid=\" + encodeURIComponent(APP_ID) + \"&po=\" + PORTAL + \"&c=\" + encodeURIComponent(CATEGORY) + \"&k=\" + encodeURIComponent(KEYWORDS) + ((FAILOVER) ? \"&noadstring=\" + encodeURIComponent(FAILOVER) : \"&test=true\") + \"&lg=\" + encodeURIComponent(GPS_COORDINATES) + \"&l=\" + encodeURIComponent(LOCATION) + \"&mw=\" + ((IS_MOBILE_WEB) ? \"true\" : \"false\") + \"'></script>\"); ");
            html.append("</script></body>");
    html.append("</html>");

    // Show ad
    NavigateAd(html);

    return true;
}

bool Ads::NewLeadboltAd()
{
    if (m_Type == wall)
    {
        // SHow the web view
        s3eWebViewShow(m_View, 0, m_Y, IwGxGetScreenWidth(), m_H);

        // Navigate to the saved ad html file
        std::string url = "http://ad.leadboltads.net/show_app_wall?section_id=" + m_Id;
        s3eWebViewNavigate(m_View, url.c_str());

        return true;
    }

    // Create html to display ad
    std::string html;
    html.reserve(500);
    html.append("<html><head>");
            html.append("<meta name='viewport' content='width=device-width, initial-scale=1.0, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no' />");
            html.append("<style> #iaAdContainerDiv { text-align: center; } </style>");
        html.append("</head><body style='margin:0;padding:0;text-align: center;'>");
            html.append("<script src='http://ad.leadboltads.net/show_app_ad.js?section_id=");
            html.append(m_Id);
            html.append("'></script>");
        html.append("</body></html>");

    // Show ad
    NavigateAd(html);

    return true;
}

bool Ads::NewAdmodaAd()
{
    if (m_Type != banner)
        return false;

    // Create html to display ad
    std::string html;
    html.reserve(500);
    html.append("<html><head>");
            html.append("<meta name='viewport' content='width=device-width, initial-scale=1.0, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no' />");
            html.append("<style> #iaAdContainerDiv { text-align: center; } </style>");
        html.append("</head><body style='margin:0;padding:0;text-align: center;'>");
            html.append("<script src='http://www.admoda.com/ads/jsbannertext.php?v=4&l=javascript&z=");
            html.append(m_Id);
            html.append("'></script>");
        html.append("</body></html>");

    // Show ad
    NavigateAd(html);

    return true;
}

bool Ads::NewAd(int y, int height, eAdProvider provider, eAdType type, const char *id)
{
    if (m_View == 0)
        return false;

    m_Y = y;
    m_H = height;
    m_Provider = provider;
    m_Type = type;
    m_Id = id;

    switch (provider)
    {
    case inneractive:
        return NewInneractiveAd();
        break;
    case leadbolt:
        return NewLeadboltAd();
        break;
    case admoda:
        return NewAdmodaAd();
        break;
    }

    return false;
}


bool Ads::NewAd()
{
    if (m_View == 0)
        return false;

    switch (m_Provider)
    {
    case inneractive:
        return NewInneractiveAd();
        break;
    case leadbolt:
        return NewLeadboltAd();
        break;
    case admoda:
        return NewAdmodaAd();
        break;
    }

    return false;
}

int32 Ads::LoadedCallback(s3eWebView *instance, void *systemData, void *userData)
{
    printf("Ad page loaded - %s\n", (char*)systemData);
    return 1;
}

int32 Ads::ErrorCallback(s3eWebView *instance, void *systemData, void *userData)
{
    printf("Errror loaded ad - %s\n", (char*)systemData);
    return 1;
}

int32 Ads::StartedLoadingCallback(s3eWebView *instance, void *systemData, void *userData)
{
    Ads* ads = (Ads*)userData;
    std::string url = (char*)systemData;

    if (ads->GetProvider() == leadbolt && url.find("show_app_ad?") != std::string::npos)
    {
        // Special case for leadbolt that prevents redirects being recognised as an ad click
    }
    else
    if (ads->GetType() == wall)
    {
        // Special case for leadbolt app wall ads, these do not open in a new browser window
    }
    else
    {
        if (url.find("http:") != std::string::npos)
        {
            // launch URL
            printf("Launching ad with url - %s\n", (char*)systemData);
            s3eOSExecExecute(url.c_str(), S3E_FALSE);

            // Get a new ad
            ads->NewAd();
        }
    }

    return 1;
}

