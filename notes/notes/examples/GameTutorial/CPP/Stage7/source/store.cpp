/*
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This document is protected by copyright, and contains information
 * proprietary to Marmalade.
 *
 * This file consists of source code released by Marmalade under
 * the terms of the accompanying End User License Agreement (EULA).
 * Please do not use this program/source code before you have read the
 * EULA and have agreed to be bound by its terms.
 */

#include "store.h"
#include "game.h"

Store* g_pStore = 0;

// In-app item ID's, add your own to test
#define IOS_NOADS_ID        "add_your_own_ios_app_id"
#define ANDROID_NOADS_ID    "android.test.purchased"

Store::~Store()
{
    // Terminate the billing system
    if (IwBilling::isAvailable())
        IwBilling::Terminate();
}

bool Store::Init()
{
    // Check that billing is available
    if (!IwBilling::isAvailable())
        return false;

    // Initialise billing
    if (!IwBilling::Init())
        return false;

    // Set up billing callbacks
    IwBilling::setErrorCallback(ErrorCallback);
    IwBilling::setReceiptAvailableCallback(ReceiptAvailableCallback);

    return true;
}

bool Store::PurchaseNoAds()
{
    // Ensure that billing is available
    if (!IwBilling::isAvailable())
        return false;

    // Make purchase call
    std::string platform = s3eDeviceGetString(S3E_DEVICE_OS);
    if (platform == "ANDROID")
        return IwBilling::PurchaseProduct(ANDROID_NOADS_ID);
    else
    if (platform == "IPHONE")
        return IwBilling::PurchaseProduct(IOS_NOADS_ID);

    return false;
}

bool Store::FinishTransaction(void *transaction_data)
{
    // Finalise transaction
    if (!IwBilling::FinishTransaction(transaction_data))
        return false;

    // Disable ads
    ((Game*)g_pSceneManager->Find("game"))->disableAds();

    return true;
}

void Store::ErrorCallback(void* caller, IwBilling::CIwBillingErrorData* data)
{
    printf("Error attempting to purchase %s (%d)\n", data->ProductID, data->Error);
}

void Store::ReceiptAvailableCallback(void* caller, IwBilling::CIwBillingReceiptAvailableData* data)
{
    g_pStore->FinishTransaction(data->FinaliseData);
}

