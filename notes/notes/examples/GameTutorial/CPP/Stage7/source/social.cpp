/*
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This document is protected by copyright, and contains information
 * proprietary to Marmalade.
 *
 * This file consists of source code released by Marmalade under
 * the terms of the accompanying End User License Agreement (EULA).
 * Please do not use this program/source code before you have read the
 * EULA and have agreed to be bound by its terms.
 */

#include "social.h"
#include "s3e.h"

#define FACEBOOK_APP_ID     "add_your_own_facebook_app_id"

Social* g_pSocial = 0;

const char* Social::s_PublishPermission = "publish_actions";

Social::~Social()
{
    if (CurrentSession != NULL)
    {
        s3eFBTerminate(CurrentSession);
    }
}

bool Social::Init()
{
    // Check that Facebook support is available
    if (s3eFacebookAvailable() != S3E_TRUE)
        return false;

    return true;
}

bool Social::LogIn()
{
    // Check that Facebook support is available
    if (s3eFacebookAvailable() != S3E_TRUE)
        return false;

    // If no session then create one
    if (CurrentSession == NULL)
        CurrentSession = s3eFBInit(FACEBOOK_APP_ID);

    if (CurrentSession)
    {
        // Log in to Facebook using the session
        if (s3eFBSession_Login(CurrentSession, LoginCallback, this, NULL, 0, s3eFBSessionBehaviourWithFallbackToWebView) != S3E_TRUE)
            return false;
    }

    return true;
}

void Social::LoginCallback(s3eFBSession *session, s3eResult *result, void *data)
{
    // Recover the context
    Social *context = (Social *)data;

    if (*result == S3E_RESULT_SUCCESS)
    {
        context->LoggedIn = true;

        context->OnOkLogin();
    }
    else
    {
        context->LoggedIn = false;
        context->Waiting = false;
    }
}

void Social::OnOkLogin()
{
    // we need to find if we have publish_action permission first
    bool publish_actions = false;

    if (HasPublishPermissions(CurrentSession))
    {
        PostUpdate();
    }
    else
    {
        // need to reauthorize - looks a bit like logging in again
        const char* permissions[] = { Social::s_PublishPermission };
        s3eFBSession_ReauthorizeWithPublishPermissions(CurrentSession,
               permissions, sizeof(permissions)/sizeof(permissions[0]), s3eFBSessionDefaultAudienceEveryone, ReauthorizeCallback, (void*) this);
    }
}

void Social::ReauthorizeCallback(s3eFBSession *session, s3eResult *result, void *data)
{
    // Recover the context
    Social *context = (Social *)data;

    if (*result == S3E_RESULT_SUCCESS)
    {
        if (!context->HasPublishPermissions(session))
        {
            // TODO: if the user has not granted publish permission, handle error here.
            printf("Error: publish_actions permission not granted.\n");
            context->Waiting = false;
        }
        else
        {
            // Post update to users wall
            context->PostUpdate();
        }
    }
    else
    {
        context->Waiting = false;
    }
}

bool Social::PostUpdate(const char* update)
{
    Waiting = true;
    StatusUpdate = update;
    if (!PostUpdate())
        return false;
    WaitUntilCompleted();

    return true;
}

bool Social::PostUpdate()
{
    if (LoggedIn)
    {
        // Build a request that is used to post update to users wall
        Request = s3eFBRequest_WithGraphPath(CurrentSession, "me/feed", "POST");
        s3eFBRequest_AddParamString(Request, "message", StatusUpdate.c_str());
        s3eResult result = s3eFBRequest_Send(Request, RequestCallback, this);
        if (result != S3E_RESULT_SUCCESS)
        {
            s3eFBRequest_Delete(Request);
            Request = NULL;
            return false;
        }
    }
    else
        return LogIn();

    return true;
}

void Social::RequestCallback(s3eFBRequest *request, s3eResult *result, void *data)
{
    // Recover the context
    Social *context = (Social *)data;

    // Delete the request
    s3eFBRequest_Delete(context->Request);
    context->Request = NULL;
    context->Waiting = false;
}

void Social::WaitUntilCompleted()
{
    // Wait for request to complete
    while (Waiting && !s3eDeviceCheckQuitRequest())
    {
        s3eDeviceYield(100);
    }
}

bool Social::HasPublishPermissions(s3eFBSession *session)
{
    if (session)
    {
        const char*const *const permissions = s3eFBSession_GetPermissions(session);

        if (permissions)
        {
            for (const char*const*ptr = permissions; *ptr !=  NULL; ptr++)
            {
                if (strcmp(*ptr, Social::s_PublishPermission) == 0)
                    return true;
            }
        }
    }
    return false;
}
