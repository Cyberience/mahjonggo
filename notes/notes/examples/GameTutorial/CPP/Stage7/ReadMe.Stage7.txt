#category: Tutorial
My first Marmalade C++ App

Stage 7: Network Services

The aims of this tutorial stage include:
    * Create a Facebook app and post a status update
      to the users Facebook wall
    * Setup a Flurry account and track user interactions
      within the app using Analytics
    * Show Ads within the app to generate revenue
    * Set up ads using Inner-active
    * Use billing to allow users to remove ads
    * Testing in-app purchases on iOS and Android
