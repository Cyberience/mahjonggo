/*
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This document is protected by copyright, and contains information
 * proprietary to Marmalade.
 *
 * This file consists of source code released by Marmalade under
 * the terms of the accompanying End User License Agreement (EULA).
 * Please do not use this program/source code before you have read the
 * EULA and have agreed to be bound by its terms.
 */

#if !defined(__GRID_H__)
#define __GRID_H__

#include "gem.h"
#include "Iw2DSceneGraph.h"
#include <list>

#define MAX_GEM_TYPES   5

using namespace Iw2DSceneGraphCore;
using namespace Iw2DSceneGraph;

class Grid
{
protected:
    Gem**               Gems;               // Grid cells
    int                 Width, Height;      // Grid width and height
    int                 GemSize;            // Actual isply size of gem
    int                 GridOriginX;        // Grids y-axis origin
    int                 GridOriginY;        // Grids y-axis origin
public:
    int         getWidth() const        { return Width; }
    int         getHeight() const       { return Height; }
    int         getGridOriginX() const  { return GridOriginX; }
    int         getGridOriginY() const  { return GridOriginY; }
    int         getGemSize() const      { return GemSize; }
public:
    Grid(CNode* scene, int num_columns, int num_rows, int offset_x, int offset_y, int grid_width);
    ~Grid();

};



#endif  // __GRID_H__


