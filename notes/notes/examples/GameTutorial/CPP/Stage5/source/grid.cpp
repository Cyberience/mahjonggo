/*
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This document is protected by copyright, and contains information
 * proprietary to Marmalade.
 *
 * This file consists of source code released by Marmalade under
 * the terms of the accompanying End User License Agreement (EULA).
 * Please do not use this program/source code before you have read the
 * EULA and have agreed to be bound by its terms.
 */

#include "IwGx.h"
#include "IwTween.h"
#include "audio.h"
#include "scene.h"
#include "grid.h"
#include "game.h"
#include "resources.h"
#include "main.h"

using namespace IwTween;

Grid::Grid(CNode* scene, int num_columns, int num_rows, int offset_x, int offset_y, int grid_width)
{
    Width = num_columns;
    Height = num_rows;
    Gems = new Gem* [num_columns * num_rows];

    // Calculate gem display size
    int bm_width = (int)g_pResources->getGemAtlas(0)->GetFrameWidth();
    GemSize = (IwGxGetScreenWidth() * bm_width) / GRAPHIC_DESIGN_WIDTH;

    // Calculate gem scale
    float gem_scale = (float)GemSize / bm_width;

    // Create gems
    int index = 0;
    GridOriginX = offset_x;
    GridOriginY = IwGxGetScreenHeight() - (num_rows * GemSize) - offset_y;
    for (int y = 0; y < num_rows; y++)
    {
        for (int x = 0; x < num_columns; x++)
        {
            // Init gem
            int type = rand() % MAX_GEM_TYPES;
            Gems[index] = new Gem();
            Gems[index]->init((float)x * GemSize + GridOriginX, GridOriginY + (float)y * GemSize, g_pResources->getGemAtlas(type));
            Gems[index]->m_ScaleX = gem_scale;
            Gems[index]->m_ScaleY = gem_scale;
            // Add to scene
            scene->AddChild(Gems[index]);
            index++;
        }
    }
}

Grid::~Grid()
{
    if (Gems != 0)
        delete [] Gems;
}


