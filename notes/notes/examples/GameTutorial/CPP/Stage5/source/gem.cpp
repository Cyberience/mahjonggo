/*
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This document is protected by copyright, and contains information
 * proprietary to Marmalade.
 *
 * This file consists of source code released by Marmalade under
 * the terms of the accompanying End User License Agreement (EULA).
 * Please do not use this program/source code before you have read the
 * EULA and have agreed to be bound by its terms.
 */

#include "gem.h"
#include "game.h"
#include "resources.h"
#include "main.h"

Gem::~Gem()
{
}

void Gem::setType(int type)
{
    Type = type;
    SetAtlas(g_pResources->getGemAtlas(type));
    SetAnimRepeat(1);
}

void Gem::init(float x, float y, CAtlas* atlas)
{
    m_X = x;
    m_Y = y;
    SetAtlas(atlas);
    SetAnimDuration(2);
    SetAnimRepeat(1);
}

