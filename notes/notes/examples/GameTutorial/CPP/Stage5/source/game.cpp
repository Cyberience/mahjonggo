/*
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This document is protected by copyright, and contains information
 * proprietary to Marmalade.
 *
 * This file consists of source code released by Marmalade under
 * the terms of the accompanying End User License Agreement (EULA).
 * Please do not use this program/source code before you have read the
 * EULA and have agreed to be bound by its terms.
 */

#include "IwGx.h"
#include "IwHashString.h"

#include "game.h"
#include "input.h"
#include "audio.h"
#include "pauseMenu.h"
#include "resources.h"

// Grid background offset
#define GRID_OFFSET_X           41
#define GRID_OFFSET_Y           37

Game::~Game()
{
    // Cleanup game grid
    if (gemsGrid != 0)
        delete gemsGrid;
}

void Game::addToRoundScore(int score)
{
    // Add to score
    currentRoundScore += score;

    // Update score label text to show new score
    char str[16];
    snprintf(str, 16, "%d", currentRoundScore);
    scoreLabel->m_Text = str;
}

void Game::pauseGame()
{
    // Switch to pause menu scene
    g_pSceneManager->SwitchTo(g_pSceneManager->Find("pausemenu"));
}

void Game::resumeGame()
{
}

void Game::newGame()
{
    // Reset score
    currentRoundScore = 0;
}

void Game::Update(float deltaTime, float alphaMul)
{
    if (!m_IsActive)
        return;

    // Update base scene
    Scene::Update(deltaTime, alphaMul);

    // Detect screen tap
    if (m_IsInputActive && m_Manager->GetCurrent() == this && !g_pInput->m_Touched && g_pInput->m_PrevTouched)
    {
        g_pInput->Reset();
        if (pauseSprite->HitTest(g_pInput->m_X, g_pInput->m_Y))
        {
            // Enter pause menu
            pauseGame();
        }
    }
}

void Game::Render()
{
    Scene::Render();
}

// Initialise the games user interface
void Game::initUI()
{
    // Create background
    CSprite* background = new CSprite();
    background->m_X = (float)IwGxGetScreenWidth() / 2;
    background->m_Y = (float)IwGxGetScreenHeight() / 2;
    background->SetImage(g_pResources->getGameBG());
    background->m_W = background->GetImage()->GetWidth();
    background->m_H = background->GetImage()->GetHeight();
    background->m_AnchorX = 0.5;
    background->m_AnchorY = 0.5;
    // Fit background to screen size
    background->m_ScaleX = (float)IwGxGetScreenWidth() / background->GetImage()->GetWidth();
    background->m_ScaleY = (float)IwGxGetScreenHeight() / background->GetImage()->GetHeight();
    AddChild(background);

    // Create grid background sprite
    gridSprite = new CSprite();
    gridSprite->m_X = 0;
    gridSprite->m_Y = IwGxGetScreenHeight() - g_pResources->getGrid()->GetHeight() * graphicsScale;
    gridSprite->m_ScaleX = graphicsScale;
    gridSprite->m_ScaleY = graphicsScale;
    gridSprite->SetImage(g_pResources->getGrid());
    AddChild(gridSprite);

    // Create score label text
    scoreLabelText = new CLabel();
    scoreLabelText->m_X = 30 * fontScale;
    scoreLabelText->m_Y = 10 * fontScale;
    scoreLabelText->m_W = FONT_DESIGN_WIDTH;
    scoreLabelText->m_H = actualFontHeight;
    scoreLabelText->m_Text = "Score:";
    scoreLabelText->m_AlignHor = IW_2D_FONT_ALIGN_LEFT;
    scoreLabelText->m_AlignVer = IW_2D_FONT_ALIGN_TOP;
    scoreLabelText->m_Font = g_pResources->getFont();
    scoreLabelText->m_ScaleX = fontScale;
    scoreLabelText->m_ScaleY = fontScale;
    scoreLabelText->m_Color = CColor(0xff, 0xff, 0x30, 0xff);
    AddChild(scoreLabelText);

    // Create score label (displays actual score)
    scoreLabel = new CLabel();
    scoreLabel->m_X = 80 * fontScale;
    scoreLabel->m_Y = 10 * fontScale;
    scoreLabel->m_W = FONT_DESIGN_WIDTH;
    scoreLabel->m_H = actualFontHeight;
    scoreLabel->m_Text = "0";
    scoreLabel->m_AlignHor = IW_2D_FONT_ALIGN_LEFT;
    scoreLabel->m_AlignVer = IW_2D_FONT_ALIGN_TOP;
    scoreLabel->m_Font = g_pResources->getFont();
    scoreLabelText->m_Color = CColor(0xff, 0xff, 0xff, 0xff);
    scoreLabel->m_ScaleX = fontScale;
    scoreLabel->m_ScaleY = fontScale;
    AddChild(scoreLabel);

    // Create pause menu sprite (docked to top of screen)
    pauseSprite = new CSprite();
    pauseSprite->SetImage(g_pResources->getPauseIcon());
    pauseSprite->m_X = (float)IwGxGetScreenWidth() / 2;
    pauseSprite->m_Y = 0;
    pauseSprite->m_W = pauseSprite->GetImage()->GetWidth();
    pauseSprite->m_H = pauseSprite->GetImage()->GetHeight();
    pauseSprite->m_AnchorX = 0.5;
    pauseSprite->m_ScaleX = graphicsScale;
    pauseSprite->m_ScaleY = graphicsScale;
    AddChild(pauseSprite);

}

void Game::Init(int grid_width, int grid_height)
{
    Scene::Init();

    fontScale = (float)IwGxGetScreenWidth() / FONT_DESIGN_WIDTH;
    actualFontHeight = FONT_HEIGHT * fontScale;
    graphicsScale = (float)IwGxGetScreenWidth() / GRAPHIC_DESIGN_WIDTH;
    currentRoundScore = 0;

    // Initialise UI
    initUI();

    // Create grid
    gemsGrid = new Grid(this, grid_width, grid_height, (int)(GRID_OFFSET_X * graphicsScale), (int)(GRID_OFFSET_Y * graphicsScale), IwGxGetScreenWidth());
}

