#category: Tutorial
My first Marmalade C++ App

Stage 5: Supporting Multiple Screen Resolutions

The aims of this tutorial stage include:
   * Introduce methods for supporting multiple resolutions
   * Show how to lock display orientation
   * Show how to use the Virtual Resolution system
   * Show how to use resolution independent design
