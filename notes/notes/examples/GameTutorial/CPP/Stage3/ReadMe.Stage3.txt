#category: Tutorial
My first Marmalade C++ App

Stage 3: Scenes, Sprites and Labels

The aims of this tutorial stage include:

  *  Show how to use Iw2DSceneGraph to
     build and manage visual components
  *  Show how to create and use sprites
