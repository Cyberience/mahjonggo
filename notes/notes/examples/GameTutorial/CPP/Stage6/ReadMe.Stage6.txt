#category: Tutorial
My first Marmalade C++ App

Stage 6: Game Play

The aims of this tutorial stage include:
    * Introduce key game play concepts
    * Walk through important sections of the game code
    * Introduce timers
