#category: Tutorial
My first Marmalade C++ App

Stage 2: Input and Audio

The aims of this tutorial stage include:
  *  Show how to monitor touch input and
     respond to screen touches
  *  Show how to play background music
  *  Show how to load and play sound effects
