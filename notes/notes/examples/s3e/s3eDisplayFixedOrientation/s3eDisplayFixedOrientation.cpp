/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EDisplayFixedOrientation S3E Display Fixed Orientation Example
 *
 * Sample that demonstrates different device orientation locks
 *
 * @include s3eDisplayFixedOrientation.cpp
 */
#include "ExamplesMain.h"

static Button* g_FreeButton           = NULL;
static Button* g_LandscapeButton      = NULL;
static Button* g_FixedLandscapeButton = NULL;
static Button* g_PortraitButton       = NULL;
static Button* g_FixedPortraitButton  = NULL;

const char* OrientationLockToString(int32 lock)
{
    switch(lock)
    {
        case S3E_SURFACE_ORIENTATION_FREE:
        {
            return "Free orientation";
        }
        case S3E_SURFACE_PORTRAIT:
        {
            return "Portrait";
        }
        case S3E_SURFACE_LANDSCAPE:
        {
            return "Landscape";
        }
        case S3E_SURFACE_PORTRAIT_FIXED:
        {
            return "Fixed Portrait";
        }
        case S3E_SURFACE_LANDSCAPE_FIXED:
        {
            return "Fixed Landscape";
        }
        default:
        {
            return "Unknown orientation lock";
        }
    }
}

void ExampleInit()
{
    g_FreeButton           = NewButton("Free");
    g_LandscapeButton      = NewButton("Landscape");
    g_FixedLandscapeButton = NewButton("Fixed Landscape");
    g_PortraitButton       = NewButton("Portrait");
    g_FixedPortraitButton  = NewButton("Fixed Portrait");

    InitMessages(30, 128);

    int32 lock = s3eSurfaceGetInt(S3E_SURFACE_DEVICE_ORIENTATION_LOCK);
    AppendMessageColour(BLACK, "Initial lock = %s", OrientationLockToString(lock));
}

void ExampleTerm()
{
}

bool ExampleUpdate()
{
    const Button* const pressed = GetSelectedButton();

    s3eSurfaceOrientationLock lock = S3E_SURFACE_ORIENTATION_FREE;
    if (pressed == g_FreeButton)
    {
        lock = S3E_SURFACE_ORIENTATION_FREE;
    }
    else if (pressed == g_LandscapeButton)
    {
        lock = S3E_SURFACE_LANDSCAPE;
    }
    else if (pressed == g_FixedLandscapeButton)
    {
        lock = S3E_SURFACE_LANDSCAPE_FIXED;
    }
    else if (pressed == g_PortraitButton)
    {
        lock = S3E_SURFACE_PORTRAIT;
    }
    else if (pressed == g_FixedPortraitButton)
    {
        lock = S3E_SURFACE_PORTRAIT_FIXED;
    }
    else
    {
        return true;
    }

    s3eSurfaceSetInt(S3E_SURFACE_DEVICE_ORIENTATION_LOCK, lock);
    AppendMessageColour(BLACK, "New lock = %s", OrientationLockToString(lock));

    return true;
}

void ExampleRender()
{
    int height = s3eDebugGetInt(S3E_DEBUG_FONT_HEIGHT);

    int y = GetYBelowButtons();
    y += 2*height;
    PrintMessages(10, y);
}
