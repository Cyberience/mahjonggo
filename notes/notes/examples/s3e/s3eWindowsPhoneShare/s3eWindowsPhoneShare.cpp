/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EWindowsPhoneShare S3E Windows Phone Share Example
 * The following example demonstrates the Windows Phone Share extension.
 *
 * The following function is used to achieve this:
 * - s3eWindowsPhoneShareLink()
 * - s3eWindowsPhoneSmsCompose()
 * - s3eWindowsPhoneEmailCompose()
 *
 * @include s3eWindowsPhoneShare.cpp
 */

#include "ExamplesMain.h"
#include "s3eWindowsPhoneShare.h"

static Button* g_ButtonShare = NULL;
static Button* g_ButtonSmsCompose = NULL;
static Button* g_ButtonEmailCompose = NULL;
int result = -1;

void ExampleInit()
{
    g_ButtonShare = NewButton("Share URL");
    g_ButtonSmsCompose = NewButton("Compose SMS message");
    g_ButtonEmailCompose = NewButton("Compose Email message");
    if (!s3eWindowsPhoneShareAvailable())
    {
        g_ButtonShare->m_Enabled = false;
        g_ButtonSmsCompose->m_Enabled = false;
        g_ButtonEmailCompose->m_Enabled = false;
    }
}

void ExampleTerm()
{
}

bool ExampleUpdate()
{
    if (!s3eWindowsPhoneShareAvailable())
        return true;

    Button* pressed = GetSelectedButton();
    if (pressed == g_ButtonShare)
    {
        result = s3eWindowsPhoneShareLink("Marmalade", "http://www.madewithmarmalade.com", "Visit Marmalade Web Site");
    }
    else if (pressed == g_ButtonSmsCompose)
    {
        result = s3eWindowsPhoneSmsCompose("11111111", "Visit Marmalade Web Site http://www.madewithmarmalade.com");
    }
    else if (pressed == g_ButtonEmailCompose)
    {
        result = s3eWindowsPhoneEmailCompose("Marmalade Web Site", "Visit Marmalade Web Site http://www.madewithmarmalade.com", "some@email.com");
    }

    return true;
}

void ExampleRender()
{
    if (!s3eWindowsPhoneShareAvailable())
    {
        s3eDebugPrint(50, 100, "`xff0000WP8 Share extension not found.", 1);
        return;
    }
    else
    {
        switch (result) {
            case S3E_RESULT_ERROR:
                s3eDebugPrint(50, 100, "`xff0000Failed to share", 1);
                break;
            case S3E_RESULT_SUCCESS:
                s3eDebugPrint(50, 100, "`x00ff00Shared successfully", 1);
                break;
            default:
                break;
        };
    }
}
