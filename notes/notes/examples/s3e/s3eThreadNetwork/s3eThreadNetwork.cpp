/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EThreadNetwork S3E Thread Network Example
 * The following example demonstrates S3E's threading functionality.
 *
 * The main thread renders the status screen. Another thread is
 * created to wait for incoming network connections on port 8080.
 *
 * When an incoming connection is received, a new thread is created.
 * It then acts as a simple HTTP server - as soon as it sees
 * 'GET' in the incoming data, a block of text is returned with
 * a simple HTTP header.
 *
 * To test this example, run in on a host machine and
 * view @verbatim http://<ip address of host device>:8080 @endverbatim
 * in a web browser on another machine on the same network, or telnet to port
 * 8080 of the host machine and type 'GET' (this way you can see that multiple
 * simultaneous connections are maintained)
 *
 * @include s3eThreadNetwork.cpp
 */

#include "s3e.h"
#include "s3eThread.h"
#include "ExamplesMain.h"

#include <string.h>
#include <unistd.h>

#define MAX_CONNECTIONS 32
#define PORT 8080

//The size of the response we send back
#define RESPONSE_SIZE 1024

static s3eThread* g_ListenThread;
static s3eThread* g_DataThreads[MAX_CONNECTIONS];
static s3eThreadLock* g_ThreadQueueLock;
static int g_NumConnections = 0;
static int g_TotalConnections = 0;
static int g_FailedConnections=  0;
static const char* g_NetworkError = NULL;

void ExampleRender()
{
    int height = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT);

    //Clear screen
    const int textHeight = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);

    //We'll animate a string of dots whilst waiting for connections
    const int dots_time = 5000;
    const int num_dots = 10;

    int y = height/3;

    if (g_NetworkError)
    {
        s3eDebugPrintf(20, y, 1, "`x000000Network error: %s - unable to accept incoming connections", g_NetworkError);

    }
    else
    {
        if (!g_NumConnections)
        {
            //Display a message indicating we're waiting for connections

            uint64 timeMilli = s3eTimerGetUTC() % dots_time;
            int dots = ((int)timeMilli * num_dots) / dots_time;

            //Set dotsstring to a string of 'dots' number of dots
            char dotsstring[num_dots+1];
            int  i;
            for (i = 0; i < dots; i++)
                dotsstring[i] = '.';
            dotsstring[i] = '\0';


            s3eDebugPrintf(20, y, 1, "`x000000Waiting for connections on port %d%s", PORT, dotsstring);
        }
        else
        {
            s3eDebugPrintf(20, y, 1, "`x000000%d active simultaneous connections", g_NumConnections);
        }
    }

    y += textHeight*2;
    s3eDebugPrintf(20, y, 1, "`x000000%d total connections", g_TotalConnections);

    y += textHeight;
    s3eDebugPrintf(20, y, 1, "`x000000%d failed connections", g_FailedConnections);
}

//Read incoming socket data, and return false(success) if we
//see 'GET' in the data, otherwise return true. In essence,
//the return value indicates errors.
bool ReadIncomingData(s3eSocket* socket)
{
    char buf[4096] = {0};
    char* readptr = buf;

    while (true)
    {
        //read data into 'buf' byte by byte
        int res = s3eSocketRecv(socket, readptr, 1, 0);

        if (res == -1)
        {
            if (s3eSocketGetError() != S3E_SOCKET_ERR_WOULDBLOCK)
            {
                //connection failed
                return true;
            }
            else
            {
                //No data - wait until an event (which will include all socket events)
                s3eDeviceYieldUntilEvent();
                continue;
            }
        }

        if (strstr(buf, "GET"))
        {
            //if we see "GET", return success (false)
            return false;
        }

        readptr += res;
        if (res == 0 || readptr > buf + sizeof(buf))
        {
            //We never saw 'GET' before our buffer filled up or
            //the connection ended
            return true;
        }
    }
}

//Send the NULL-terminated string 'content' on 'socket'
//return true on failure.
bool SendString(s3eSocket* socket, const char* content)
{
    const char* send_ptr = content;
    int len_remaining = strlen(content);

    while (len_remaining)
    {
        //try to send as many bytes as possible
        int res = s3eSocketSend(socket, send_ptr, len_remaining, 0);

        if (res == -1)
        {
            if (s3eSocketGetError() != S3E_SOCKET_ERR_WOULDBLOCK)
            {
                //connection failed
                return true;
            }
            else
            {
                //Socket would block - wait until an event (which will include all socket events)
                //and try again after that
                s3eDeviceYieldUntilEvent();
                continue;
            }
        }

        len_remaining -= res;
        send_ptr += res;
    }

    //All data sent OK
    return false;
}

//Send a large HTTP response. Hopefully this will take some time
//so you can see multiple connections being serve simultaneously
bool SendResponse(s3eSocket* socket)
{
    char header[512];

    //create buffer for response
    char* content = (char*)s3eMalloc(RESPONSE_SIZE);
    memset(content, 0, RESPONSE_SIZE);

    //We'll fill the buffer with repetitions of this line
    char response_line[] = "This is the response!\r\n";

    //fill the buffer
    int size_remaining = RESPONSE_SIZE;
    char* content_ptr = content;
    while (size_remaining > (int)sizeof(response_line))
    {
        memcpy(content_ptr, response_line, sizeof(response_line));
        content_ptr += strlen(response_line);
        size_remaining -= sizeof(response_line);
    }

    //compose a minimal header for HTTP
    sprintf(header,
        "HTTP/1.0 200 OK\r\n"
        "Content-Type: text/plain\r\n"
        "Content-Length: %u\r\n\r\n", (int)strlen(content));

    //try sending both
    if (SendString(socket, header) ||
        SendString(socket, content))
    {
        s3eFree(content);
        return true;
    }

    s3eFree(content);
    return false;
}

//This is the entry point for a connection thread. The listener
//thread passes the socket handle via s3eThreadCreate.
void* ConnectionMain(void* _socket)
{
    s3eSocket* socket = (s3eSocket*)_socket;

    //Scan for 'GET' in incoming data
    if (ReadIncomingData(socket))
    {
        //an error happened, drop this connection
        s3eSocketClose(socket);
        g_FailedConnections++;
        return NULL;
    }

    //Send out a basic HTTP response
    if (SendResponse(socket))
    {
        s3eSocketClose(socket);
        g_FailedConnections++;
        return NULL;
    }

    s3eSocketClose(socket);
    return NULL;
}


//This callback is called whenever one of the connection threads finishes
//Let's remove this thread from the list of active connection threads
void ConnectionCleanup()
{
    //We need a lock around the list access as it's potentially not
    //thread-safe
    s3eThreadLockAcquire(g_ThreadQueueLock);

    g_NumConnections--;
    //Look for ourselves in the thread list
    for (int i = 0; i < MAX_CONNECTIONS; i++)
    {
        if (g_DataThreads[i] == s3eThreadGetCurrent())
        {
            g_DataThreads[i] = NULL;
        }
    }

    s3eThreadLockRelease(g_ThreadQueueLock);
}


//Handle a new connection: Create a thread to process
//incoming and outgoing data.
void NewConnection(s3eSocket* sock)
{
    g_TotalConnections++;

    //We use a lock to access a list of active threads. If two threads are
    //both reading and writing to data, a lock is needed
    s3eThreadLockAcquire(g_ThreadQueueLock);

    //Create a thread to process the incoming connection, and add it to
    //the list of active threads

    //We may have run out of threads, so we need an indicator for this
    bool allocated_thread = false;

    for (int i = 0; i < MAX_CONNECTIONS; i++)
    {
        if (!g_DataThreads[i])
        {
            //Found a suitable place for the new thread

            g_NumConnections++;

            //create processing thread
            g_DataThreads[i] = s3eThreadCreate(ConnectionMain, sock, ConnectionCleanup);

            //detach it so that the thread handle is cleared up when the thread exits,
            //as opposed to when we call join
            s3eThreadDetach(g_DataThreads[i]);

            //indicate success
            allocated_thread = true;
            break;
        }
    }

    if (!allocated_thread)
    {
        //No thread available to process the connection. Just close the socket
        s3eSocketClose(sock);
    }

    //Release the lock
    s3eThreadLockRelease(g_ThreadQueueLock);
}

void* ListenMain(void* arg)
{
    //This is the entry point for the listener thread. This thread
    //listens for incoming connections and spawns a new thread for
    //each connection.

    while (!s3eDeviceCheckQuitRequest())
    {
        // Put a short delay in to prevent us running round the loop too fast
        // We restart this loop on any error to try again.
        s3eDeviceYield(100);

        if (!s3eSocketGetInt(S3E_SOCKET_NETWORK_AVAILABLE))
        {
            g_NetworkError = "Network not available";
            continue;
        }

        // Create a socket and set it to listen mode
        s3eSocket* listen = s3eSocketCreate(S3E_SOCKET_TCP);
        if (!listen)
        {
            g_NetworkError = "Failed to create socket";
            continue;
        }

        s3eInetAddress addr;
        memset(&addr,0,sizeof(s3eInetAddress)); //zero out the structure

        addr.m_IPAddress = 0; //allow connections from any IP address
        //The port needs to be stored in 'network byte order', s3eInetHtons does the conversion
        addr.m_Port = s3eInetHtons(PORT);
        if (s3eSocketBind(listen, &addr, 1) != S3E_RESULT_SUCCESS)
        {
            s3eSocketClose(listen);
            g_NetworkError = "s3eSocketBind failed, couldn't bind to port";
            continue;
        }

        if (s3eSocketListen(listen, 1) != S3E_RESULT_SUCCESS)
        {
            s3eSocketClose(listen);
            g_NetworkError = "s3eSocketListen failed";
            continue;
        }

        //Socket is listening, wait for incoming connections
        g_NetworkError = NULL;

        do
        {
            //Get any incoming connection
            s3eSocket*  incoming_conn = s3eSocketAccept(listen, NULL, NULL, NULL);

            if (!incoming_conn)
            {
                //If there's no incoming connection, we sleep here. When a connection comes,
                //s3eDeviceYieldUntilEvent will return. We then go round the loop again to
                //call accept and get the new socket.
                s3eDeviceYieldUntilEvent();
            }
            else
            {
                //Fire off a new thread to handle the connection
                NewConnection(incoming_conn);
            }
        }
        while (!s3eDeviceCheckQuitRequest());

    }

    return NULL;
}

bool ExampleUpdate()
{
    return true;
}

void ExampleInit()
{
    //Create the lock used to access the list of active threads
    g_ThreadQueueLock = s3eThreadLockCreate();

    //Create the thread that'll listen for incoming connections
    g_ListenThread = s3eThreadCreate(ListenMain, NULL, NULL);
}

void ExampleTerm()
{
    //Signal the connection thread to quit
    s3eThreadJoin(g_ListenThread, NULL);

    //Use a polling mechanism to wait for all
    //processing threads to exit
    bool threads_still_active;
    do
    {
        threads_still_active = false;
        for (int i = 0; i < MAX_CONNECTIONS; i++)
        {
            if (g_DataThreads[i])
                threads_still_active = true;
        }
        s3eDeviceYield(1);
    } while (threads_still_active);

    s3eThreadLockDestroy(g_ThreadQueueLock);
    g_ThreadQueueLock = NULL;
}
