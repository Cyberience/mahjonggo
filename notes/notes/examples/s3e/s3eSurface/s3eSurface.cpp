/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3ESurface S3E Surface Example
 *
 * The following example uses the S3E surface functionality to manipulate
 * screen orientation.
 *
 * The functions used to achieve this are:
 * - s3eSurfaceGetInt()
 * - s3eSurfaceSetup()
 *
 * Based on user input the screen orientation is turned clockwise or anit-
 * clockwise.
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eSurfaceImage.png
 *
 * @include s3eSurface.cpp
 */

#include "ExamplesMain.h"

Button* g_ButtonRotateCW = 0;
Button* g_ButtonRotateCCW = 0;
Button* g_ButtonNextDispFixRot = 0;

void ExampleInit()
{
    g_ButtonRotateCW        = NewButton("Rotate cw");
    g_ButtonRotateCCW       = NewButton("Rotate ccw");
    g_ButtonNextDispFixRot  = NewButton("Next DispFixRot");
}

void ExampleTerm()
{
}

/*
 * The following function checks for a button to be pressed and based on this
 * manipulates the orientation of what is displayed on screen. The function
 * uses the s3eSurfaceGetInt() function to find out the surface blit direction,
 * which is identified by S3E_SURFACE_BLIT_DIRECTION. When the user presses
 * s3eKey1 the s3eSurfaceBlitDirection is incremented, and when the user
 * presses s3eKey2 the s3eSurfaceBlitDirection is decremented.
 */
bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    if (pressed == g_ButtonRotateCW)
    {
        // Get actual screen orientation
        int32 surfaceRot = s3eSurfaceGetInt(S3E_SURFACE_BLIT_DIRECTION);

        // Change to next clockwise orientation
        surfaceRot++;

        // Make sure we stay in range
        if (surfaceRot > S3E_SURFACE_BLIT_DIR_ROT270)
            surfaceRot = S3E_SURFACE_BLIT_DIR_NORMAL;

        // Set new orientation
        s3eSurfaceSetup(S3E_SURFACE_PIXEL_TYPE_RGB565, 0, NULL, (s3eSurfaceBlitDirection) surfaceRot);
    }

    if (pressed == g_ButtonRotateCCW)
    {
        // Get actual screen orientation
        int32 surfaceRot = s3eSurfaceGetInt(S3E_SURFACE_BLIT_DIRECTION);

        // Change to next counter clockwise orientation
        surfaceRot--;

        // Make sure we stay in range
        if (surfaceRot < S3E_SURFACE_BLIT_DIR_NORMAL)
            surfaceRot = S3E_SURFACE_BLIT_DIR_ROT270;

        // Set new orientation
        s3eSurfaceSetup(S3E_SURFACE_PIXEL_TYPE_RGB565, 0, NULL, (s3eSurfaceBlitDirection) surfaceRot);
    }

    if (pressed == g_ButtonNextDispFixRot)
    {
        // Get actual orientation lock
        int32 deviceLock = s3eSurfaceGetInt(S3E_SURFACE_DEVICE_ORIENTATION_LOCK);

        // Change to next orientation lock
        deviceLock++;

        // Make sure we stay in range
        if (deviceLock > S3E_SURFACE_ORIENTATION_LOCK_MAX)
            deviceLock = S3E_SURFACE_ORIENTATION_LOCK_MIN;

        // Set new orientation lock
        s3eSurfaceSetInt(S3E_SURFACE_DEVICE_ORIENTATION_LOCK, deviceLock);
    }

    return true;
}

/*
 *  Render the current state to the display.
 */
void ExampleRender()
{
    const int textHeight = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);

    const char* orientationStr[] = { "Normal", "90 Degree rotated", "180 Degree rotated", "270 Degree rotated", "Native" };
    const char* lockStr[] =        { "Free", "Portrait", "Landscape", "FixedPortrait", "FixedLandscape" };
    const char* pSurfaceDir = "Unknown";
    const char* pDeviceDir = "Unknown";
    const char* pScreenLock = "Unknown";

    // get current device state
    int32 surfaceDir = s3eSurfaceGetInt(S3E_SURFACE_BLIT_DIRECTION);
    int32 deviceDir = s3eSurfaceGetInt(S3E_SURFACE_DEVICE_BLIT_DIRECTION);
    int32 screenLock = s3eSurfaceGetInt(S3E_SURFACE_DEVICE_ORIENTATION_LOCK);
    int32 surfaceWidth = s3eSurfaceGetInt(S3E_SURFACE_WIDTH);
    int32 surfaceHeight = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT);

    // Surface orientation
    switch (surfaceDir)
    {
        case S3E_SURFACE_BLIT_DIR_NORMAL:
        case S3E_SURFACE_BLIT_DIR_ROT90:
        case S3E_SURFACE_BLIT_DIR_ROT180:
        case S3E_SURFACE_BLIT_DIR_ROT270:
        case S3E_SURFACE_BLIT_DIR_NATIVE:
            pSurfaceDir = orientationStr[surfaceDir];
        default:
            break;
    }
    // Screen orientation
    switch (deviceDir)
    {
        case S3E_SURFACE_BLIT_DIR_NORMAL:
        case S3E_SURFACE_BLIT_DIR_ROT90:
        case S3E_SURFACE_BLIT_DIR_ROT180:
        case S3E_SURFACE_BLIT_DIR_ROT270:
            pDeviceDir = orientationStr[deviceDir];
        default:
            break;
    }
    // Device orientation lock
    switch (screenLock)
    {
        case S3E_SURFACE_ORIENTATION_FREE:
        case S3E_SURFACE_PORTRAIT:
        case S3E_SURFACE_LANDSCAPE:
        case S3E_SURFACE_PORTRAIT_FIXED:
        case S3E_SURFACE_LANDSCAPE_FIXED:
            pScreenLock = lockStr[screenLock];
        default:
            break;
    }

    int32 y = GetYBelowButtons();

    // Render device state to surface
    s3eDebugPrintf(10, y, 1, "`x666666Screenwidth\t: %d\nScreenHeight\t: %d", surfaceWidth, surfaceHeight);
    s3eDebugPrintf(10, y + 3 * textHeight, 1, "`x666666Current surface orientation:\n\t%s", pSurfaceDir);
    s3eDebugPrintf(10, y + 6 * textHeight, 1, "`x666666Current screen orientation:\n\t%s", pDeviceDir);
    s3eDebugPrintf(10, y + 9 * textHeight, 1, "`x666666Current device orientation lock:\n\t%s", pScreenLock);
}
