/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3ETimerBasic S3E Timer Basic Example
 *
 * The following example uses S3E's Time functionality to get the current time
 * in milliseconds since midnight January 1st 1970 and convert it into the
 * current time in hours, minutes and seconds.
 *
 * The main function used to achieve this is:
 *  - s3eTimerGetUTC()
 *
 * The ExampleUpdate function uses a simple calculation to find the amount of
 * milliseconds that have happened today and to split them up into relevant
 * hours minutes and seconds.
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eTimerBasicImage.png
 *
 * @include s3eTimerBasic.cpp
 */
#include "ExamplesMain.h"
#include <time.h>

/*
 * Time Constants
 */
#define MS_PER_DAY      86400000
#define MS_PER_HOUR     3600000
#define MS_PER_MINUTE   60000
#define MS_PER_SECOND   1000

void ExampleInit()
{
}

void ExampleTerm()
{
}

bool ExampleUpdate()
{
    return true;
}

/*
 * The following function displays the current time using various
 * different methods.
 */
void ExampleRender()
{
    const int textHeight = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);
    const int height = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT);

    // Returns the time since the Epoch (00:00:00 UTC, January 1, 1970),
    // measured in milliseconds.
    uint64 timeMilli = s3eTimerGetUTC() % MS_PER_DAY;

    // Assemble string for displaying of time.
    uint8 hr =  (uint8)  (timeMilli / MS_PER_HOUR);
    uint8 min = (uint8) ((timeMilli / MS_PER_MINUTE) % 60);
    uint8 sec = (uint8) ((timeMilli / MS_PER_SECOND) % 60);
    uint  ms = (uint)(timeMilli % 1000);

    int y = height/3;
    // Displaying clock centered vertically and horizontally.
    s3eDebugPrintf(20, y, 1, "`x666666%02d:%02d:%02d:%03u", hr, min, sec, ms);

    time_t now = time(NULL);
    int64 offset = s3eTimerGetLocaltimeOffset()/1000;

    y += 2 * textHeight;
    s3eDebugPrintf(20, y, 1, "`x666666time      : %lu", now);
    y += 2 * textHeight;
    s3eDebugPrintf(20, y, 1, "`x666666GMT offset: %llds (%lld hours)", offset, offset/60/60);
    y += 2 * textHeight;
    s3eDebugPrintf(20, y, 1, "`x666666gmtime    : %s", asctime(gmtime(&now)));
    y += 2 * textHeight;
    s3eDebugPrintf(20, y, 1, "`x666666ctime     : %s", ctime(&now));
    y += 2 * textHeight;
    struct tm* local_tm = localtime(&now);
    s3eDebugPrintf(20, y, 1, "`x666666localtime : %s", asctime(local_tm));
}
