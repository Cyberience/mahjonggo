/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EGoogleAdMob S3E Google AdMob Example
 *
 * The following demonstrates use of s3eGoogleAdMob API to display ads provided
 * by Google AdMob API.   The example implements a simple state machine
 * for the requesting, loading, showing and destroying of ads:
 *
 * state      | API usage
 *            | s3eGoogleAdMobRegister
 * AD_UNSET   | s3eGoogleAdMobPrepareAd / s3eGoogleAdMobPrepareLayout
 * AD_INIT    | s3eGoogleAdMobLoadAd / s3eGoogleAdMobLoadInterstitialAd
 * AD_LOADING | (see callback usage below)
 * AD_LOADED  | s3eGoogleAdMobShowAd
 * AD_SHOWING | s3eGoogleAdMobDestroyAd
 *            | s3eGoogleAdMobTerminate
 *
 * The state transitions are shown for three types of ads:
 *   1. a small top banner ad that is displayed immedaitely after load.
 *   2. a auto-sized bottom banner ad that stretched the width of the display.
 *   3. a full screen interstitial ad. Example targeting code is shows commented out.
 *
 * This example also demonstrates how to register for callback events:
 *    S3E_GOOGLEADMOB_CALLBACK_AD_LOADED - Used to change state from to LOADED.
 *    S3E_GOOGLEADMOB_CALLBACK_AD_ACTION - Used when dismissing interstitial ads
 *                                      to change state back to INIT.
 *    S3E_GOOGLEADMOB_CALLBACK_AD_ERROR  - Used to change state back to INIT.
 *
 * @remark g_adUnitId contains the standard "test" value. If you register with
 * Google you will get your own AdUnitId and can use that instead.
 *
 * @include s3eGoogleAdMob.cpp
 * @incldue s3eGoogleAdMobUtil.h
 */
#include "s3eGoogleAdMobUtil.h"

#include "ExamplesMain.h"
#include "IwDebug.h"

#include "s3eGoogleAdMob.h"

#include <string>


//
// If you have your own AdUnitId, you can replace the following.
//
static const char* g_adUnitId = "ca-app-pub-3940256099942544/6300978111";

static CEventLog   s_Log;      // helper class for logging

class CAdManager
{
    class CAdState
    {
        friend CAdManager;

        enum AdStateEnum { AD_UNSET, AD_INIT, AD_LOADING, AD_LOADED, AD_SHOWING };
        static std::string m_Labels[5];

        CAdState() {}
    public:
        static const int MaxStates = 5;

        CAdState(std::string name)
        {
            m_Id = 0;
            m_Name = name;
            m_State = AD_UNSET;
            std::string label = m_Labels[AD_UNSET] + m_Name;
            m_Button = NewButton(label.c_str());
        }

        ~CAdState()
        {
            delete m_Button;
        }

        virtual bool do_prepare()
        {
            s_Log.Log(std::string("Preparing request for ") + m_Name);

            s3eResult res = s3eGoogleAdMobSetAdUnitId(g_adUnitId, S3E_TRUE);
            if (res == S3E_RESULT_SUCCESS)
                res = s3eGoogleAdMobPrepareAd(&m_Id);
            return (res == S3E_RESULT_SUCCESS);
        }

        virtual bool do_load(bool show)
        {
            s_Log.Log(std::string("Loading ") + m_Name);

            s3eBool toshow = show ? S3E_TRUE : S3E_FALSE;
            s3eResult res = s3eGoogleAdMobLoadAd(m_Id, toshow);
            return (res == S3E_RESULT_SUCCESS);
        }

        virtual bool do_show()
        {
            s_Log.Log(std::string("Showing ") + m_Name);

            s3eResult res = s3eGoogleAdMobShowAd(m_Id);
            return (res == S3E_RESULT_SUCCESS);
        }

        virtual bool do_destroy()
        {
            s_Log.Log(std::string("Destroying ") + m_Name);

            s3eResult res = s3eGoogleAdMobDestroyAd(m_Id);
            m_Id = -1;
            return (res == S3E_RESULT_SUCCESS);
        }

        virtual void changeState(AdStateEnum state)
        {
            m_State = state;
            std::string str;
            str = m_Labels[state] + m_Name;
            SetButtonName(m_Button, str.c_str());
        }

        virtual bool onClick(Button* button)
        {
            if (m_Button == button)
            {
                switch (m_State)
                {
                case AD_UNSET:      // "prepare request"
                    if (do_prepare())
                        changeState(AD_INIT);
                    break;
                case AD_INIT:       // "load ad"
                    if (do_load(true))
                        changeState(AD_LOADING);
                    break;
                case AD_LOADING:    // "loading..." (disabled)
                    // state change from callback   --> AD_LOADED or AD_SHOWING
                    return false;
                case AD_LOADED:     // "show ad"
                    if (do_show())
                        changeState(AD_SHOWING);
                    break;
                case AD_SHOWING:    // "destroy ad"
                    if (do_destroy())
                        changeState(AD_UNSET);
                    break;
                default:
                    return false;
                }
            }
            return true;
        }

        virtual void onRefresh()
        {
        }

        virtual void onLoad() = 0;

        virtual void onAction(s3eGoogleAdMobActionType action)
        {
            const char *text = NULL;
            switch (action)
            {
            case S3E_GOOGLEADMOB_ACTION_DISMISSED: text = "dismissed"; break;
            case S3E_GOOGLEADMOB_ACTION_EXPANDED: text = "expanded"; break;
            case S3E_GOOGLEADMOB_ACTION_LEFTAP: text = "left ap"; break;
            default:
                text = "unknown";
            }
            s_Log.Log(std::string("onAction for ") + m_Name + " (" + text + ")");
        }

        virtual void onError(int error)
        {
            char temp[64];
            sprintf(temp, "%d", error);
            s_Log.Log(std::string("onError for ") + m_Name + "=" + temp);
            changeState(AD_INIT);
        }

    protected:
        s3eGoogleAdMobId m_Id;
        std::string m_Name;
        AdStateEnum m_State;
        Button* m_Button;
    };

    class CTopBannerAd : public CAdState
    {
    public:
        CTopBannerAd(std::string name) : CAdState(name) {}

        bool do_prepare()
        {
            if (CAdState::do_prepare())
            {
                s3eGoogleAdMobPrepareAdLayout(m_Id,  S3E_GOOGLEADMOB_POSITION_TOP,  S3E_GOOGLEADMOB_SIZE_BANNER);
                return true;
            }
            return false;
        }

        void onRefresh()
        {
            if (m_State == AD_SHOWING)
            {
                s_Log.Log(std::string("orientation change: ") + m_Name);
                if (do_load(true))
                    changeState(AD_LOADING);
            }
        }

        void onLoad()
        {
            s_Log.Log(std::string("onLoad for ") + m_Name);
            changeState(AD_SHOWING);
        }
    };

    class CBottomBannerAd : public CAdState
    {
        bool m_skip_show_button;
    public:
        CBottomBannerAd(std::string name) :
            CAdState(name),
            m_skip_show_button(false)
        {
        }

        void onRefresh()
        {
            if (m_State == AD_SHOWING)
            {
                s_Log.Log(std::string("orientation change: ") + m_Name);
                m_skip_show_button = true;
                if (do_load_show())
                    changeState(AD_LOADING);
            }
        }

        bool do_destroy()
        {
            m_skip_show_button = false;
            return CAdState::do_destroy();
        }

        void onLoad()
        {
            s_Log.Log(std::string("onLoad for ") + m_Name);
            if (!m_skip_show_button)
                changeState(AD_LOADED);
            else
                changeState(AD_SHOWING);
        }

        bool do_load_show()
        {
            return CAdState::do_load(true);
        }

        bool do_load(bool show)
        {
            IwTrace(GOOGLEADMOB_EXAMPLE, ("do_load override"));
            return CAdState::do_load(false);
        }

        bool do_prepare()
        {
            if (CAdState::do_prepare())
            {
                s3eGoogleAdMobPrepareAdLayout(m_Id,  S3E_GOOGLEADMOB_POSITION_BOTTOM,  S3E_GOOGLEADMOB_SIZE_AUTO);
                return true;
            }
            return false;
        }
    };

    class CInterstitialAd : public CAdState
    {
        bool m_skip_show_button;
    public:
        CInterstitialAd(std::string name) :
            CAdState(name),
            m_skip_show_button(false)
        {
        }

        bool do_destroy()
        {
            m_skip_show_button = false;
            return CAdState::do_destroy();
        }

        bool do_prepare()
        {
            bool res = CAdState::do_prepare();

            if (!res)
                return false;

            // example of setting targeting options
            s3eResult s3eRes = s3eGoogleAdMobSetChildDirection(S3E_GOOGLEADMOB_CHLDDIR_FALSE);
            if (s3eRes != S3E_RESULT_SUCCESS)
            {
                char temp[64];
                sprintf(temp, "%d", s3eGoogleAdMobError());
                s_Log.Log(std::string("ERROR: s3eGoogleAdMobSetChildDirection FAILED(") + temp + ")");
                return false;
            }

            s3eRes = s3eGoogleAdMobSetGender(S3E_GOOGLEADMOB_GENDER_FEMALE);
            if (s3eRes != S3E_RESULT_SUCCESS)
            {
                char temp[64];
                sprintf(temp, "%d", s3eGoogleAdMobError());
                s_Log.Log(std::string("ERROR: s3eGoogleAdMobSetGender FAILED(") + temp + ")");
                return false;
            }

            return res;
        }

        bool do_load(bool show)
        {
            // example of s3eGoogleAdMobInspectAd:
            s3eGoogleAdMobAdInfo info;
            s3eGoogleAdMobInspectAd(m_Id, &info);
            if (info.m_IsLoading)
                s_Log.Log(std::string("Warning s3eGoogleAdMobInspectAd::isLoading is true."));
            s_Log.Log(std::string("Loading (Interstitial) ") + m_Name);
            s3eResult res = s3eGoogleAdMobLoadInterstitialAd(m_Id);
            return res == S3E_RESULT_SUCCESS;
        }

        virtual void onLoad()
        {
            s_Log.Log(std::string("onLoad for ") + m_Name);
            if (!m_skip_show_button)
                changeState(AD_LOADED);
            else
                changeState(AD_SHOWING);
        }

        bool do_load_show()
        {
            return do_load(true);
        }

        void onRefresh()
        {
            // onRefresh happens when orientation changes. IF this happens we want to
            // refetch any ad we've fetched to give AdMob chance to get one with the
            // new orientation. On at least some platforms, we won't get this if we
            // are showing because the marmalade app will be suspended, but code it anyway.
            if (m_State == AD_SHOWING || m_State == AD_LOADED || m_State == AD_LOADING)
            {
                s_Log.Log(std::string("interstitial orientation change: ") + m_Name);
                if (m_State == AD_SHOWING)
                    m_skip_show_button = true;
                if (do_load_show())
                    changeState(AD_LOADING);
            }
        }

        void onAction(s3eGoogleAdMobActionType action)
        {
            CAdState::onAction(action);
            if (action == S3E_GOOGLEADMOB_ACTION_DISMISSED)
                changeState(AD_INIT);
        }
    };

public:
    CAdManager()
    {
        // Create three ad state objects to handle button / ad state
        m_Ad[0] = new CTopBannerAd("Top Small Banner Ad");
        m_Ad[1] = new CBottomBannerAd("Bottom Auto Banner Ad");
        m_Ad[2] = new CInterstitialAd("Interstitial Ad");

        // REGISTER for all s3eGoogleAdMob callbacks:
        s3eGoogleAdMobRegister( S3E_GOOGLEADMOB_CALLBACK_AD_LOADED, onAdLoad,  this);
        s3eGoogleAdMobRegister( S3E_GOOGLEADMOB_CALLBACK_AD_ACTION, onAdAction, this);
        s3eGoogleAdMobRegister( S3E_GOOGLEADMOB_CALLBACK_AD_ERROR,  onAdError,  this);

        s3eSurfaceRegister(S3E_SURFACE_SCREENSIZE, ScreenSizeChangeCallback, this);
    }

    void Update()
    {
        Button* pressed = GetSelectedButton();
        for (int i=0; i<numAds; i++)
            m_Ad[i]->onClick(pressed);
    }

private:
    static const int numAds = 3;
    CAdState* m_Ad[numAds];

public:
    //
    // Callbacks
    //
    static int32 onAdLoad(void* systemData, void* userData)
    {
        s3eGoogleAdMobCallbackLoadedData* data = static_cast<s3eGoogleAdMobCallbackLoadedData*>(systemData);
        CAdManager* mgr = static_cast<CAdManager*>(userData);

        if (data == NULL || mgr == NULL)
        {
            s_Log.Log(std::string("onAdLoad error: callback data is NULL."));
            return 1;
        }

        for (int i=0; i<numAds; i++)
            if (mgr->m_Ad[i]->m_Id == data->m_Id)
                mgr->m_Ad[i]->onLoad();

        return 0;
    }

    static int32 onAdAction(void* systemData, void* userData)
    {
        s3eGoogleAdMobCallbackActionData* data = static_cast<s3eGoogleAdMobCallbackActionData*>(systemData);
        CAdManager* mgr = static_cast<CAdManager*>(userData);

        if (data == NULL || mgr == NULL)
        {
            s_Log.Log(std::string("onAdAction error: callback data is NULL."));
            return 1;
        }
        s3eGoogleAdMobActionType action = data->m_Type;

        for (int i=0; i<numAds; i++)
            if (mgr->m_Ad[i]->m_Id == data->m_Id)
                mgr->m_Ad[i]->onAction(action);

        return 0;
    }

    static int32 onAdError(void* systemData, void* userData)
    {
        s3eGoogleAdMobCallbackErrorData* data = static_cast<s3eGoogleAdMobCallbackErrorData*>(systemData);
        CAdManager* mgr = static_cast<CAdManager*>(userData);

        if (data == NULL || mgr == NULL)
        {
            s_Log.Log(std::string("onAdError error: callback data is NULL."));
            return 1;
        }

        for (int i=0; i<numAds; i++)
            if (mgr->m_Ad[i]->m_Id == data->m_Id)
                mgr->m_Ad[i]->onError(data->m_Error);

        return 0;
    }

    static int32 ScreenSizeChangeCallback(void* systemData, void* userData)
    {
        s_Log.Log(std::string("screensize"));

        CAdManager* mgr = static_cast<CAdManager*>(userData);

        if (mgr == NULL)
        {
            s_Log.Log(std::string("callback data is NULL."));
            return 1;
        }

        for (int i=0; i<numAds; i++)
        {
            mgr->m_Ad[i]->onRefresh();
        }
        return 0;
    }

};

std::string CAdManager::CAdState::m_Labels[5] = { "Prepare ", "Load ", "Loading ", "Show ", "Destroy " };
    // note these labels are the strings to show on the button during the associated AdStateEnum,
    // but are not the state names themselves.

static CAdManager* s_Manager;  // helper class for managing button / ad state

void ExampleInit()
{
    // Check if the the s3eGoogleAdMob extension is available
    if (s3eGoogleAdMobAvailable())
    {
        // If android log says "Use AdRequest.Builder.addTestDevice("123456781212") to get test ads on this device."
        // comment out the following line (changing to use the actual literal string in the log). Can be given
        // mulitple times if you have more than one test device.
        // Similarly on iOS if you get "To get test ads on this device, call: request.testDevices = @[ @"b9ff71dd54071b363c3c424bb82ff8ff" ];"
        // Also for iOS ensure you disable Settings->Privacy->Advertising->"Limit Ad Tracking"
        // or you won't actually see the message above.

        // s3eGoogleAdMobTestAd("123456781212");
        s_Manager = new CAdManager();
    }
    else
        s_Log.Log("s3eGoogleAdMob is not available.");
}

void ExampleTerm()
{
    if (s_Manager != NULL)
    {
        delete s_Manager;
        s_Manager = NULL;
    }
}

bool ExampleUpdate()
{
    if (s_Manager != NULL)
        s_Manager->Update();

    return true;
}

void ExampleRender()
{
    // Display event log
    int y = GetYBelowButtons();
    s_Log.Display(y);
}
