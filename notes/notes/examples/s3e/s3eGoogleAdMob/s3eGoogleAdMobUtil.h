/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
#include "IwDebug.h"
#include <deque>
#include <string>

#define MAX_LOG 10

void dump(int &y, const char* tmp)
{
    static char buffer[1024];
    sprintf(buffer, "`xff0000%s", tmp);
    y += 20;
    s3eDebugPrint(50, y, buffer, 1);
}

class CEventLog
{
public:
    CEventLog()
    {
        m_Log.push_back("--- Event Log ---");
    }

    void Log(std::string eventstr)
    {
        IwTrace(GOOGLEADMOB_EXAMPLE, ("%s", eventstr.c_str()));

        m_Log.push_back(eventstr);
        if (m_Log.size() > MAX_LOG)
            m_Log.pop_front();
    }

    void Display(int y)
    {
        std::deque<std::string>::iterator it = m_Log.begin();
        int i = 0;
        while (it != m_Log.end() && i < MAX_LOG)
        {
            dump(y, it->c_str());
            i++;
            it++;
        }
    }
private:
    std::deque<std::string> m_Log;
};
