/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3ESMS S3E SMS Example
 * The following example demonstrates S3E's SMS functionality.
 *
 * The main functions used to achieve this are:
 * - s3eSMSSendMessage()
 * - s3eSMSReceiveStart()
 * - s3eSMSReceiveStop()
 * - s3eSMSRegister()
 * - s3eSMSUnRegister()
 *
 * The example allows the user to send and receive SMS.
 * The user can send an SMS by passing the phone number
 * and message body to s3eSMSSendMessage.
 *
 * To receive SMS the user should register a callback
 * (using s3eSMSRegister) that will be called for every
 * SMS received.
 *
 * @include s3eSMS.cpp
 */

#include "ExamplesMain.h"
#include "s3eSMS.h"
#include "s3eOSReadString.h" // For text entry through native input

const int NUM_MESSAGES = 5;
const int MESSAGE_LEN = 160;
const int PHONENUM_LEN = 20;

static Button* s_ButtonNumber = NULL;
static Button* s_ButtonText = NULL;
static Button* s_ButtonSend = NULL;
static Button* s_ButtonStart = NULL;
static Button* s_ButtonStop = NULL;

static char s_strMessage[MESSAGE_LEN] = "Default text";
static char s_strNumber[PHONENUM_LEN] = "+1234567890";

//This callback will be called for every new SMS once registered.
int32 receivecallback(void* sys, void* user)
{
    s3eSMSMessage* pSMS = (s3eSMSMessage*)sys;

    char messageTxt[MESSAGE_LEN];

    //Display the message body
    strcpy(messageTxt, "`x666666Body: ");
    strncat(messageTxt, pSMS->m_Body, MESSAGE_LEN);
    AppendMessage(messageTxt);

    //Display the message sender
    strcpy(messageTxt, "`x666666Message received from: ");
    strncat(messageTxt, pSMS->m_Sender, MESSAGE_LEN);
    AppendMessage(messageTxt);

    return 0;
}

void ExampleInit()
{
    s_ButtonNumber = NewButton("Edit Phone Number");
    s_ButtonText = NewButton("Edit SMS Text");
    s_ButtonSend = NewButton("Send SMS");
    s_ButtonStart = NewButton("Start Listening");
    s_ButtonStop = NewButton("Stop Listening");

    InitMessages(NUM_MESSAGES, MESSAGE_LEN);

    s_ButtonStop->m_Enabled = false;

    //Check whether the SMS extension is available
    if (!s3eSMSAvailable())
    {
        s_ButtonSend->m_Enabled = false;
        s_ButtonStart->m_Enabled = false;
        AppendMessage("`x666666SMS extension not available");
    }

    if (!s3eOSReadStringAvailable())
    {
        s_ButtonNumber->m_Enabled = false;
        s_ButtonText->m_Enabled = false;
        AppendMessage("`x666666OSReadString extension is not available");
    }
}

void ExampleTerm()
{
    TerminateMessages();
}

bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    if (pressed == s_ButtonText)
    {
        //Use OSReadString to get a message from the user
        const char* pBody = s3eOSReadStringUTF8("Enter message body:");
        if (pBody && strlen(pBody) > 0)
            strlcpy(s_strMessage, pBody, sizeof s_strMessage);
    }

    if (pressed == s_ButtonNumber)
    {
        //Use OSReadString to get a phone number from the user
        const char* pNumber = s3eOSReadStringUTF8("Enter phone number:", S3E_OSREADSTRING_FLAG_NUMBER);

        if (pNumber && strlen(pNumber) >= 3) // valid numbers are at least 3 digits long
            strlcpy(s_strNumber, pNumber, sizeof s_strNumber);
        else
            AppendMessage("`x666666Incorrect phone number");
    }

    if (pressed == s_ButtonSend)
    {
        //Send an SMS Message
        if (s3eSMSSendMessage(s_strMessage, s_strNumber) == S3E_RESULT_SUCCESS)
            AppendMessage("`x666666Message sent");
        else
            AppendMessage("`x666666Message sending failed");
    }

    if (pressed == s_ButtonStart)
    {
        //Start receiving SMS
        if (s3eSMSReceiveStart() == S3E_RESULT_SUCCESS)
        {
            s_ButtonStart->m_Enabled = false;
            s_ButtonStop->m_Enabled = true;

            //Register the receive SMS callback
            if (s3eSMSRegister(S3E_SMS_RECEIVE_EVENT, receivecallback, 0) != S3E_RESULT_SUCCESS)
            {
                AppendMessage("`x666666Error registering callback");
            }
        }
        else
            AppendMessage("`x666666Receiving SMSs is unavailable");
    }

    if (pressed == s_ButtonStop)
    {
        //Stop receiving SMS and remove the callback
        s3eSMSReceiveStop();
        s3eSMSUnRegister(S3E_SMS_RECEIVE_EVENT, receivecallback);

        s_ButtonStart->m_Enabled = true;
        s_ButtonStop->m_Enabled = false;
    }

    return true;
}

void ExampleRender()
{
    int x = 20;
    int y = GetYBelowButtons();
    int lineGap = s3eDebugGetInt(S3E_DEBUG_FONT_HEIGHT) + 2;

    s3eDebugPrintf(x, y, 1, "`x666666Phone Number: %s", s_strNumber);
    y += lineGap;
    s3eDebugPrintf(x, y, 1, "`x666666SMS Text: %s", s_strMessage);
    y += lineGap;

    PrintMessages(x, y);
}
