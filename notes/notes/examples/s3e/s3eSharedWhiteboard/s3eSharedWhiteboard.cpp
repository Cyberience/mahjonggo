/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3ESharedWhiteboard S3E Shared Whiteboard Example
 *
 * The following example demonstrate the use of s3eZeroConf for
 * s3e applications to exchange data between each other.
 *
 * ZeroConf is an s3e extension and is currently only supported on iPhone.
 *
 * The main functions used in this example are:
 * - s3eSocketCreate()
 * - s3eSocketBind()
 * - s3eInetLookup()
 * - s3eSocketSendTo()
 * - s3eSocketRecvFrom()
 * - s3eZeroConfStartSearch()
 * - s3eZeroConfStopSearch()
 * - s3eZeroConfPublish()
 * - s3eZeroConfUnpublish()
 *
 * The application makes a connection to another whiteboard application running
 * on the same network. The 2 applications then share their pointer's data
 * so that they can display both sets on the screen.
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eSharedWhiteboardImage.png
 *
 * @include s3eSharedWhiteboard.cpp
 */


#include "ExamplesMain.h"
#include "s3e.h"
#include "s3eZeroConf.h"
#include "s3eSocket.h"
#include "s3ePointer.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

struct DrawLine
{
    uint8  m_r;
    uint8  m_g;
    uint8  m_b;
    uint16 m_x0;
    uint16 m_y0;
    uint16 m_x1;
    uint16 m_y1;
};

/*
 * Used to record infomation about the services we've discovered
 */
struct Record
{
    void*           m_Ref;
    s3eInetAddress  m_Addr;
    Record*         m_Next;
    Record*         m_Prev;
};

Record*                 g_ResultList = 0;
s3eZeroConfPublished*   g_Service = 0;
s3eZeroConfSearch*      g_Search = 0;
s3eSocket*              g_Sock;
s3eInetAddress          g_MyAddr;
uint32                  g_Counter = 0;
uint32                  g_Timer = 75;
uint16                  g_Port = 0;
uint16                  g_PrevX = 0;
uint16                  g_PrevY = 0;
int32                   g_RGBIndex = 0;
int32                   g_PrevPointerState = 0;

#define BUCKET_SIZE 256

DrawLine                g_Bucket[BUCKET_SIZE];
uint32                  g_DrawCount = 0;

uint8   g_r[] = {255,   0, 0,   255, 0,   255, 255, 127, 127, 255, 127, 255};
uint8   g_g[] = {0,   255, 0,   255, 255, 0,   127, 255, 127, 255, 255, 127};
uint8   g_b[] = {0,     0, 255, 0,   255, 255, 127, 127, 255, 127, 255, 255};

#define _SWAP(a,b) {int32 tmp = a; a = b; b = tmp;}

static int32 PixelSize()
{
    int pxlType = s3eSurfaceGetInt(S3E_SURFACE_PIXEL_TYPE);
    switch(pxlType & S3E_SURFACE_PIXEL_SIZE_MASK)
    {
        case S3E_SURFACE_PIXEL_SIZE_8:
            return 1;
        case S3E_SURFACE_PIXEL_SIZE_16:
            return 2;
        case S3E_SURFACE_PIXEL_SIZE_24:
            return 3;
        case S3E_SURFACE_PIXEL_SIZE_32:
            return 4;
    }
    return 0;
}

// draw a line in uniform colour using Bressenham's algorithm
static void DrawLineRGB(int32 x0, int32 y0, int32 x1, int32 y1, uint8 r, uint8 g, uint8 b)
{
    int32       pitch   = s3eSurfaceGetInt(S3E_SURFACE_PITCH);
    int         pxlSize = PixelSize();
    uint32      col = s3eSurfaceConvertRGB(r,g,b);
    bool        steep = false;

    if (abs(y1 - y0) > abs(x1 - x0))
    {
        _SWAP(x0, y0)
        _SWAP(x1, y1)
        steep = true;
    }

    if (x0 > x1)
    {
        _SWAP(x0, x1)
        _SWAP(y0, y1)
    }

    int32 deltax = x1 - x0;
    int32 deltay = abs(y1 - y0);
    int32 error = deltax / 2;

    int32 _mx = steep ? pitch : pxlSize;
    int32 _my = steep ? pxlSize : pitch;
    int32 ystep = (y0 < y1) ? _my : -_my;
    uint8 *dest = ((uint8 *) s3eSurfacePtr()) + (x0 * _mx) + (y0 * _my);

    int32 x;
    switch (pxlSize)
    {
        case 1:
            for (x = x0; x <= x1; x++, dest += _mx)
            {
                *(uint8*)dest = (uint8)col;

                error -= deltay;

                if (error < 0)
                {
                    dest += ystep;
                    error += deltax;
                }
            }
            break;

        case 2:
            for (x = x0; x <= x1; x++, dest += _mx)
            {
                //don't make assumptions about alignment of dest
                *(uint8*)dest =     (uint8)col;
                *(uint8*)(dest+1) = (uint8)(col>>8);

                error -= deltay;

                if (error < 0)
                {
                    dest += ystep;
                    error += deltax;
                }
            }
            break;

        case 3:
            for (x = x0; x <= x1; x++, dest += _mx)
            {
                //don't make assumptions about alignment of dest
                *(uint8*)dest =     (uint8)col;
                *(uint8*)(dest+1) = (uint8)(col>>8);
                *(uint8*)(dest+2) = (uint8)(col>>16);

                error -= deltay;

                if (error < 0)
                {
                    dest += ystep;
                    error += deltax;
                }
            }
            break;

        case 4:
            for (x = x0; x <= x1; x++, dest += _mx)
            {
                //don't make assumptions about alignment of dest
                *(uint8*)dest =     (uint8)col;
                *(uint8*)(dest+1) = (uint8)(col>>8);
                *(uint8*)(dest+2) = (uint8)(col>>16);
                *(uint8*)(dest+3) = (uint8)(col>>24);

                error -= deltay;

                if (error < 0)
                {
                    dest += ystep;
                    error += deltax;
                }
            }
            break;

        default:
            break;
    }
}


/*
 * this call back is issued when a new service (matching the query) is discovered
 */
static int32 SearchAddCallBack(s3eZeroConfSearch* search, s3eZeroConfAddRecord* rec, void* userData)
{
    s3eDebugTracePrintf("SearchAddCallBack: %s %s %d", rec->m_Name, rec->m_Hostname, rec->m_Port);

    Record* result = (Record*)s3eMalloc(sizeof(Record));
    result->m_Addr.m_Port = rec->m_Port;
    result->m_Addr.m_IPAddress = rec->m_HostIP;

    // make sure this is not our own server (waste of bandwith to send packets to ourself)
    if ((rec->m_Port == g_Port) && g_MyAddr.m_IPAddress == result->m_Addr.m_IPAddress)
    {
        s3eDebugTracePrintf("Was self, ignoring");
        s3eFree(result);
        return 0;
    }

    // push a new record onto the list
    s3eDebugTracePrintf("Adding peer: %s", s3eInetToString(&result->m_Addr));

    result->m_Prev = 0;
    result->m_Ref = rec->m_RecordRef;
    result->m_Next = g_ResultList;

    if (g_ResultList)
        g_ResultList->m_Prev = result;

    g_ResultList = result;

    return 0;
}

/*
 * this call back is issued when a previously discovered service updates it's txtRecord
 */
static int32 SearchUpdateCallBack(s3eZeroConfSearch* search, s3eZeroConfUpdateRecord* rec, void* userData)
{
    return 0;
}

/*
 * this call back is issued when a previously discovered service is unregisted
 */
static int32 SearchRemoveCallBack(s3eZeroConfSearch* search, void* userRef, void* userData)
{
    Record* pIter;

    for (pIter = g_ResultList; pIter; pIter = pIter->m_Next)
    {
        if (pIter->m_Ref == userRef)
            break;
    }

    if (!pIter)
        return 0;

    s3eDebugTracePrintf("SearchRemoveCallBack: %p", pIter);

    // remove pIter from the linked list
    if (pIter->m_Next)
        pIter->m_Next->m_Prev = pIter->m_Prev;

    if (pIter->m_Prev)
        pIter->m_Prev->m_Next = pIter->m_Next;
    else
        g_ResultList = pIter->m_Next;

    // free Iter
    s3eFree(pIter);
    return 0;
}

void ExampleInit()
{
    int64 utcTime = s3eTimerGetUTC();

    // randomise the port to allow multiple instances to run on one host machine for debugging
    g_Port = 8000 + (uint16)((utcTime >> 3) & 127);
    g_RGBIndex = ((int32)(utcTime >> 2) % sizeof(g_r));

    g_Sock = s3eSocketCreate(S3E_SOCKET_UDP, 0);

    memset(&g_MyAddr, 0, sizeof(g_MyAddr));

    g_MyAddr.m_Port = g_Port;

    s3eSocketBind(g_Sock, &g_MyAddr, 1);

    // look up our IP address
    s3eInetLookup(s3eSocketGetString(S3E_SOCKET_HOSTNAME), &g_MyAddr, 0, 0);

    // create a dummy service
    g_Service = s3eZeroConfPublish(g_Port, "s3eDummyService", "_s3ewhiteboard._udp", 0, 0, 0);

    // search for "_http._tcp" services in domain "local"
    g_Search = s3eZeroConfStartSearch("_s3ewhiteboard._udp",
                                        "local",
                                        SearchAddCallBack,
                                        SearchUpdateCallBack,
                                        SearchRemoveCallBack,
                                        0);

    s3eSurfaceClear(255, 255, 255);
    DontDrawCursor();
    DontClearScreen();
    s3eDebugTracePrintf("Waiting for peers: self=%s", s3eInetToString(&g_MyAddr));
}

void ExampleTerm()
{
    s3eDebugTracePrintf("Stopping search: %p", g_Search);
    s3eZeroConfStopSearch(g_Search);
    s3eZeroConfUnpublish(g_Service);

    Record *next;

    // free the results
    for (Record* record = g_ResultList; record; record = next)
    {
        next = record->m_Next;

        // free record
        s3eFree(record);
    }

    g_ResultList = 0;
}

bool ExampleUpdate()
{
    // get the current pointer position and selection state
    uint16 pointerX = (uint16)s3ePointerGetX();
    uint16 pointerY = (uint16)s3ePointerGetY();
    s3ePointerState pointerState = s3ePointerGetState(S3E_POINTER_BUTTON_SELECT);

    if (g_PrevPointerState && pointerState && g_DrawCount < BUCKET_SIZE)
    {
        g_Bucket[g_DrawCount].m_r = g_r[g_RGBIndex];
        g_Bucket[g_DrawCount].m_g = g_g[g_RGBIndex];
        g_Bucket[g_DrawCount].m_b = g_b[g_RGBIndex];
        g_Bucket[g_DrawCount].m_x0 = g_PrevX;
        g_Bucket[g_DrawCount].m_y0 = g_PrevY;
        g_Bucket[g_DrawCount].m_x1 = pointerX;
        g_Bucket[g_DrawCount].m_y1 = pointerY;

        // broadcast this
        for (Record *pIter = g_ResultList; pIter; pIter = pIter->m_Next)
            s3eSocketSendTo(g_Sock, (char*)&g_Bucket[g_DrawCount], sizeof(DrawLine), 0, &pIter->m_Addr);

        g_DrawCount++;
    }

    g_PrevPointerState = pointerState;

    // recieve any broadcasts
    while (g_DrawCount < BUCKET_SIZE)
    {
        s3eInetAddress sender;
        int dataSize = s3eSocketRecvFrom(g_Sock, (char*)&g_Bucket[g_DrawCount], sizeof(DrawLine), 0, &sender);

        if (dataSize == sizeof(DrawLine))
            g_DrawCount++;
        else
            break;
    }

    g_PrevX = pointerX;
    g_PrevY = pointerY;

    return true;
}

void ExampleRender()
{
    for (uint32 i = 0; i < g_DrawCount; i++)
    {
        DrawLineRGB(g_Bucket[i].m_x0, g_Bucket[i].m_y0,
                    g_Bucket[i].m_x1, g_Bucket[i].m_y1,
                    g_Bucket[i].m_r, g_Bucket[i].m_g, g_Bucket[i].m_b);
    }

    g_DrawCount = 0;
}
