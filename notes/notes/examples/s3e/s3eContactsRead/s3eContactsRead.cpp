/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EContactsRead S3E Contacts Read Example
 *
 * This example demonstrates the basic functionality of s3eContacts.
 * It reads from the device's contacts database and displays an overview of
 * a record. The current record displayed can be changed by selecting
 * appropriate on-screen buttons (using the Button functionality from
 * ExamplesMain).
 *
 * This example does not allow the user to alter the contacts in any way or
 * view more than the first entry of a record field. ExampleS3EContactsWrite
 * provides a more advanced example that implements these features.
 *
 * If there are currently no contacts on the device (or simulated device when
 * running/debugging on desktop), the example can add 3 test contacts in order
 * to temporarily populate the database. These will be removed again on
 * application termination.
 *
 * WARNING: This example will access the devices live contacts database. If
 * temporary contacts are added, it will alter the database and may not
 * remove these contacts if it fails to terminate correctly.
 *
 * @image html s3eContactsReadImage.png
 *
 * @include s3eContactsRead.cpp
 */

#include "s3e.h"
#include "s3eContacts.h"
#include "ExamplesMain.h"

#define TEST_CONTACTS_COUNT 3

static Button* g_ButtonNextRecord = 0;
static Button* g_ButtonPrevRecord = 0;
static Button* g_ButtonRefresh = 0;
static Button* g_ButtonAddTest = 0;
static int32 g_CurrentRecordIdx = 0;
static int32 g_NumRecords = 0;

// UIDs for temporary test contacts
static int32 testContact[TEST_CONTACTS_COUNT] = {-1,-1,-1};
// Storage array for field strings
static char strContact[S3E_CONTACTS_FIELD_COUNT][S3E_CONTACTS_STRING_MAX];


void RefreshContacts()
{
    g_CurrentRecordIdx = 0;

    // Update internal contacts UID mapping
    if (s3eContactsUpdate() == S3E_RESULT_ERROR)
        s3eDebugTracePrintf("s3eContactsUpdate failed!");

    g_NumRecords = s3eContactsGetNumRecords();
}

void RefreshButtons()
{
    DeleteButtons();
    g_ButtonAddTest = 0;
    g_ButtonNextRecord = 0;
    g_ButtonPrevRecord = 0;

    g_ButtonRefresh = NewButton("Refresh Contacts");

    if (g_NumRecords > 1)
    {
        g_ButtonNextRecord = NewButton("Next Contact");
        g_ButtonPrevRecord = NewButton("Previous Contact");
    }
}

void ExampleInit()
{
    for (int i=0;i<S3E_CONTACTS_FIELD_COUNT;i++) strContact[i][0] = 0;

    if (!s3eContactsAvailable())
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "s3eContacts extension not found");
        s3eDeviceExit(1);
    }

    RefreshContacts();

    // If no contacts exist, add some test ones
    // These will be deleted on termination of the app

    if (g_NumRecords == 0 && s3eContactsGetInt(S3E_CONTACTS_WRITABLE))
        g_ButtonAddTest = NewButton("Add Test Contacts");
    else
        RefreshButtons();
}

void AddTestContacts()
{
    char contactName[TEST_CONTACTS_COUNT][5] = { "John", "Dave", "Jane" };
    char contactNumber[TEST_CONTACTS_COUNT][8] = { "0123456", "0123457", "0123458" };

    for (int i = 0; i < TEST_CONTACTS_COUNT; i++)
    {
        testContact[i] = s3eContactsCreate();
        // Note that the UIDs returned here are now valid for Get/Set(), but
        // s3eContactsUpdate() must be called before s3eContactsGetUID() and
        // s3eContactsGetNumRecords() would take these new records into account

        if (testContact[i] != -1)
        {
            s3eContactsSetField(testContact[i], S3E_CONTACTS_FIELD_FIRST_NAME, contactName[i]);
            s3eContactsSetField(testContact[i], S3E_CONTACTS_FIELD_LAST_NAME, "Testcontact");
            s3eContactsSetField(testContact[i], S3E_CONTACTS_FIELD_HOME_PHONE, contactNumber[i]);
        }
    }
}

void DeleteTestContacts()
{
    for (int i = 0; i < TEST_CONTACTS_COUNT; i++)
        if (testContact[i] != -1)
            if (s3eContactsDelete(testContact[i]) == S3E_RESULT_ERROR)
                s3eDebugTracePrintf("Failed to delete test contact with uid (%d)", i);
}

bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    if (pressed)
    {
        // Delete the stored values
        for (int i=0;i<S3E_CONTACTS_FIELD_COUNT;i++) strContact[i][0] = 0;

        if (pressed == g_ButtonAddTest)
        {
            AddTestContacts();
            RefreshContacts();
        }

        if (pressed == g_ButtonRefresh)
            RefreshContacts();

        if (pressed == g_ButtonNextRecord)
            g_CurrentRecordIdx = (g_CurrentRecordIdx + 1) % g_NumRecords;

        if (pressed == g_ButtonPrevRecord)
            g_CurrentRecordIdx = g_CurrentRecordIdx == 0 ? g_NumRecords - 1 : g_CurrentRecordIdx - 1;

        RefreshButtons();
    }

    return true;
}

// Print a field of the current record to the screen
void PrintField(int y, const char* name, s3eContactsField field)
{
    // The strContact buffer is cleared whenever a new record is selected (by
    // pressing refresh/next/previous). This function reads from the database
    // on the first call after that, and then uses the cached value on
    // subsequent calls. Calling GetField() on every update would probably
    // make the application run to slowly to be unusable.

    if (!strContact[field][0])
    {
        // No value stored yet, so get it from the contacts database...

        // Get actual UID of the contact
        int32 uid = s3eContactsGetUID(g_CurrentRecordIdx);

        // Check field is supported on this device
        if (!s3eContactsFieldSupported(field))
        {
            snprintf(strContact[field], S3E_CONTACTS_STRING_MAX-1, "<not supported>");
            return;
        }

        // Get number of values currently set for this field
        int32 entriesInField = s3eContactsGetNumEntries(uid, field);

        if (entriesInField == -1)
        {
            snprintf(strContact[field], S3E_CONTACTS_STRING_MAX-1, "<ERROR GETTING NUM ENTRIES>");
        }
        else if (entriesInField == 0)
        {
            snprintf(strContact[field], S3E_CONTACTS_STRING_MAX-1, "<not set>");
        }
        else
        {
            // Get value
            char buffer[S3E_CONTACTS_STRING_MAX];

            if (s3eContactsGetField(uid, field, buffer, sizeof(buffer)) == S3E_RESULT_SUCCESS)
            {
                if (entriesInField > 1) // Just save the first value if more than one exists
                    snprintf(strContact[field], S3E_CONTACTS_STRING_MAX-1, "%s (+%d more)", buffer, (int)entriesInField);
                else
                    snprintf(strContact[field], S3E_CONTACTS_STRING_MAX-1, "%s", buffer);
            }
            else
                snprintf(strContact[field], S3E_CONTACTS_STRING_MAX-1, "ERROR RETRIEVING VALUE");
        }
    }

    // Print value to screen
    s3eDebugPrintf(10, y, 0, "`x666666%15s: %s", name, strContact[field]);
}

void ExampleRender()
{
    int y = GetYBelowButtons();

    if (g_NumRecords)
    {
        s3eDebugPrintf(10, y, 0, "`x666666Record %d of %d:", (int)g_CurrentRecordIdx+1, (int)g_NumRecords);
        y += 16;

#define PRINT_FIELD(_x) PrintField(y, #_x, S3E_CONTACTS_FIELD_##_x); y += 12
        PRINT_FIELD(DISPLAY_NAME);
        PRINT_FIELD(FIRST_NAME);
        PRINT_FIELD(LAST_NAME);
        PRINT_FIELD(MIDDLE_NAME);
        PRINT_FIELD(NICKNAME);
        PRINT_FIELD(HOME_PHONE);
        PRINT_FIELD(MOBILE_PHONE);
        PRINT_FIELD(WORK_PHONE);
        PRINT_FIELD(EMAIL_ADDR);
        PRINT_FIELD(URL);
        PRINT_FIELD(ADDRESS);
        PRINT_FIELD(CITY);
        PRINT_FIELD(POSTAL_CODE);
        PRINT_FIELD(COUNTRY);
        PRINT_FIELD(PHOTO_URL);
    }
    else
    {
        s3eDebugPrint(10, y, "`x666666Contacts database is empty", true);
    }
}


void ExampleTerm()
{
    DeleteTestContacts();
}
