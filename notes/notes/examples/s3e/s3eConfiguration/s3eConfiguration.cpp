/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EConfiguration S3E Configuration Example
 *
 * This example reads the application .icf file and prints out the desired
 * value to screen.
 *
 * The main functions used to achieve this are:
 *  - s3eConfigGetString()
 *
 * The ExampleInit() function uses the s3eConfigGetString() function to query
 * the application icf file for the specified setting, which in the case of our
 * example is the memory size of the application heap.
 *
 * The ExampleRender() function outputs the value that is gained from the
 * s3eConfigGetString() to screen.
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eConfigurationImage.png
 *
 * @include s3eConfiguration.cpp
 */

#include "ExamplesMain.h"

static char g_DisplayString[S3E_CONFIG_STRING_MAX] = {'\0'};

/*
 * The following function gets the required configuration variable using the
 * s3eConfigGetString() function.
 */
void ExampleInit()
{
    char TempString[S3E_CONFIG_STRING_MAX+8] = {'\0'};

    if (s3eConfigGetString("FooSection", "BarSetting", TempString) == S3E_RESULT_ERROR)
    {
        //An error occurred during reading of the configuration file
        strcpy(g_DisplayString, "`x666666");                    //Make the string grey
        strcat(g_DisplayString, s3eConfigGetErrorString());
    }
    else
    {
        //Reading of configuration successful
        strcpy(g_DisplayString, "`x666666");        //Make the string grey
        strcat(g_DisplayString, TempString);        //Add the result of s3eConfigGetString
    }
}

void ExampleTerm()
{
}

bool ExampleUpdate()
{
    return true;
}

/*
 * The following function outputs the value of the queried configuration
 * variable using the s3eDebugPrint() function.
 */
void ExampleRender()
{
    const int textWidth = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_WIDTH);
    const int textHeight = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);

    //Displaying info
    int32 width = s3eSurfaceGetInt(S3E_SURFACE_WIDTH);
    s3eDebugPrint((width-strlen("The current FooSetting is:")*textWidth)/2,
                    5 * textHeight, "`x666666The current FooSetting is:", 0);
    s3eDebugPrint((width-(strlen(g_DisplayString)-8)*textWidth)/2,
                    7 * textHeight, g_DisplayString, 0);
}
