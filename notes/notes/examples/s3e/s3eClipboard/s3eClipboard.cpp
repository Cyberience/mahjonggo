/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
#include "ExamplesMain.h"
#include "s3eClipboard.h"
#include "s3eOSReadString.h" // For text entry through native input

/**
 * @page ExampleS3EClipboard S3E Clipboard Example
 * The following example demonstrates S3E's Clipboard functionality.
 *
 * The main functions used to achieve this are:
 * - s3eClipboardGetText()
 * - s3eClipboardSetText()
 *
 * The application can get, delete and put text data on the clipboard where
 * it can be accessed elsewhere in the operating system. The example can put text
 * data onto the clipboard which can be pasted elsewhere in the OS. The example can
 * also get the text that is currently on the clipboard and display it.
 *
 * @include s3eClipboard.cpp
 */

#define CLIPBOARD_MAX 50

enum ClipStatus
{
    kNone,
    kGet,
    kGetEmpty,
    kGetFailed,
    kSet,
    kSetFailed,
    kUnavailable
};

static char g_Message[CLIPBOARD_MAX+8] = "`x666666";
static ClipStatus g_Status = kNone;
static uint64 g_DisplayTime = 0;
static Button* g_ButtonGet = 0;
static Button* g_ButtonSet = 0;

void ExampleInit()
{
    g_ButtonGet = NewButton("Get Clipboard Text");
    g_ButtonSet = NewButton("Set Clipboard Text");

    if (s3eClipboardAvailable())
    {
        g_ButtonGet->m_Enabled = true;
        g_ButtonSet->m_Enabled = true;
    }
    else
    {
        g_ButtonGet->m_Enabled = false;
        g_ButtonSet->m_Enabled = false;
        g_Status = kUnavailable;
    }
}

void ExampleTerm()
{

}


bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    if (pressed == g_ButtonGet)
    {
        //Get the text from the clipboard
        int strLength = s3eClipboardGetText(g_Message+8, CLIPBOARD_MAX);
        if (strLength > 0)
            g_Status = kGet;
        else if (!strLength)
            g_Status = kGetEmpty;
        else
            g_Status = kGetFailed;

        g_DisplayTime = s3eTimerGetMs();
    }

    if (pressed == g_ButtonSet)
    {
        const char* readText = 0;

        //Use OSReadString to get text from the user
        if (s3eOSReadStringAvailable())
            readText = s3eOSReadStringUTF8("Set Clipboard text:");
        else
            readText = "Sample text";

        //Set the clipboard text
        if (s3eClipboardSetText(readText) == S3E_RESULT_SUCCESS)
            g_Status = kSet;
        else
            g_Status = kSetFailed;

        g_DisplayTime = s3eTimerGetMs();
    }

    // Make sure the message lasts only for a set amount of time
    if (g_DisplayTime && s3eTimerGetMs() - g_DisplayTime > 2000)
    {
        g_Status = kNone;
        g_DisplayTime = 0;
    }
    return true;
}

void ExampleRender()
{
    int y = GetYBelowButtons();
    if (g_Status == kGet)
    {
        s3eDebugPrint(10, y, "`x666666The clipboard contained:", 0);
        y += s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);
        s3eDebugPrint(10, y, g_Message, S3E_TRUE);
    }
    else if (g_Status == kGetEmpty)
    {
        s3eDebugPrint(10, y, "`x666666Clipboard is empty", 0);
    }
    else if (g_Status == kGetFailed)
    {
        s3eDebugPrint(10, y, "`x666666Get clipboard text failed", 0);
    }
    else if (g_Status == kSet)
    {
        s3eDebugPrint(10, y, "`x666666Clipboard text has been set", 0);
    }
    else if (g_Status == kSetFailed)
    {
        s3eDebugPrint(10, y, "`x666666Set clipboard text failed", 0);
    }
    else if (g_Status == kUnavailable)
    {
        s3eDebugPrint(10, y, "`x666666Clipboard extension is not available", 0);
    }
}
