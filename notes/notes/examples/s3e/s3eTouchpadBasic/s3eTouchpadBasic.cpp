/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3ETouchpadBasic S3E Touchpad Basic Multi Touch Example
 *
 * The following example uses the S3E touchpad functionality to display
 * multiple touches to the screen if supported by the device. Currently this
 * is only the Sony Ericsson Experia Play.
 *
 * The functions used to achieve this are:
 * - s3eTouchpadRegister()
 * - s3eTouchpadUnRegister()
 *
 * A device with touchpad support is required to take full advanctage of
 * this example. The app draws a coloured circle under each contact point.
 * If the device supports only one touch point at a time, then just one circle
 * will be drawn. Note that in the s3e examples, regular example UI elements
 * such as the exit button can only be pressed with the first touch on a multi-
 * touch supporting device.
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eTouchpadBasic.png
 *
 * @include s3eTouchpadBasic.cpp
 */

#include "algorithm"
#include "ExamplesMain.h"
#include "s3eTouchpad.h"

#define MAX_TOUCHES 10

//Simple structure to track touches
struct CTouch
{
    int32 x; // position x
    int32 y; // position y
    uint64 last_active; // is touch active (currently down)
    int32 id; // touch's unique identifier
};

char g_TouchEventMsg[128];
bool g_TouchpadAvailable;
CTouch g_Touches[MAX_TOUCHES];

//Colour to render touch in
uint8 g_TouchColours[3*MAX_TOUCHES] =
{
    0xff, 0x00, 0x00,
    0x00, 0xff, 0x00,
    0x00, 0x00, 0xff,
    0xff, 0x00, 0xff,
    0x00, 0xff, 0xff,
    0xff, 0xff, 0x00,
    0x80, 0x00, 0x00,
    0x00, 0x80, 0x00,
    0x00, 0x00, 0x80,
    0x80, 0x00, 0x80,
};

// Test whether a touch index changed in the last second
bool IsTouchActive(int index)
{
    return s3eTimerGetMs() - g_Touches[index].last_active < 1000;
}

//Find an active touch with the specified id, or allocate a free one from the list
CTouch* GetTouch(int32 id)
{
    CTouch* inActive = NULL;

    for (uint32 i = 0; i < MAX_TOUCHES; i++)
    {
        if (id == g_Touches[i].id)
            return &g_Touches[i];
        if (! IsTouchActive(i))
            inActive = &g_Touches[i];
    }

    //Return first inactive touch
    if (inActive)
    {
        inActive->id = id;
        return inActive;
    }

    //No more touches, give up.
    return NULL;
}

void TouchpadMotionCB(s3eTouchpadMotionEvent* event)
{
    CTouch* touch = GetTouch(event->m_TouchID);
    if (touch)
    {
        touch->x = event->m_X;
        touch->y = event->m_Y;
        touch->last_active = s3eTimerGetMs();
    }
}

void ExampleInit()
{
    InitMessages(3,100);
    if (!s3eTouchpadAvailable())
    {
        AppendMessage("s3eTouchpad Extension not available");
        return;
    }
    else
    {
        AppendMessage("s3eTouchpad Extension intialized");
    }

    for (uint32 i = 0; i < MAX_TOUCHES; i++)
    {
        g_Touches[i].last_active = 0;
    }

    //Register for multi touch events on platforms where the functionality is available.
    s3eTouchpadRegister(S3E_TOUCHPAD_MOTION_EVENT, (s3eCallback)TouchpadMotionCB, NULL);
}

void ExampleTerm()
{
    //Clear up
    if (s3eTouchpadAvailable())
        s3eTouchpadUnRegister(S3E_TOUCHPAD_MOTION_EVENT, (s3eCallback)TouchpadMotionCB);
}

bool ExampleUpdate()
{
    if (!s3eTouchpadAvailable())
        return true;

    bool avail = s3eTouchpadGetInt(S3E_TOUCHPAD_AVAILABLE) ? true : false;
    if (avail != g_TouchpadAvailable)
    {
        if (avail)
            AppendMessage("Touchpad available changed: true");
        else
            AppendMessage("Touchpad available changed: false");
        g_TouchpadAvailable = avail;
    }
    return true;
}

/*
 * Render a coloured circle at the specified coordinates
 */
void RenderTouch(int32 x, int32 y, uint16 col)
{
    const int32 radius = 20;
    uint16* surfaceData = (uint16*)s3eSurfacePtr();
    int32 width = s3eSurfaceGetInt(S3E_SURFACE_WIDTH);
    int32 height = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT);
    int32 pitch = s3eSurfaceGetInt(S3E_SURFACE_PITCH)/2;

    // clip circular region
    uint32 startx = std::max(x - radius, 0);
    uint32 endx = std::min(width, x + radius);
    uint32 starty = std::max(0, y - radius);
    uint32 endy = std::min(height, y + radius);

    const int32 radiusSq = radius * radius;

    for (uint32 iy = starty; iy < endy; iy++)
    {
        uint16* out = surfaceData + iy * pitch + startx;
        for (uint32 ix = startx; ix < endx; ix++)
        {
            int32 distx = x - ix;
            int32 disty = y - iy;
            // if length of this line is under the radius paint the pixel
            if (distx * distx + disty * disty < radiusSq)
                *out = col;

            out++;
        }
    }
}

void ExampleRender()
{
    uint16* surfaceData = (uint16*)s3eSurfacePtr();
    int32 width = s3eSurfaceGetInt(S3E_SURFACE_WIDTH);
    int32 height = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT);
    int32 pitch = s3eSurfaceGetInt(S3E_SURFACE_PITCH)/2;

    int32 touchpad_width = std::min(s3eTouchpadGetInt(S3E_TOUCHPAD_WIDTH), width);
    int32 touchpad_height = std::min(s3eTouchpadGetInt(S3E_TOUCHPAD_HEIGHT), width);

    int offset_x = (width - touchpad_width) / 2;
    int offset_y = (height - touchpad_height) / 2;

    // render touchpad area
    int i;
    for (i = 0; i < touchpad_width; i++)
    {
        for (int j=0; j < touchpad_height; j++)
        {
            uint16 col = (uint16)s3eSurfaceConvertRGB(0xee, 0xe9, 0xe9);
            uint16* out = surfaceData + (j + offset_y) * pitch + i + offset_x;

            *out = col;
        }
    }

    for (i = 0; i < MAX_TOUCHES; i++)
    {
        // For each active touch
        if (IsTouchActive(i))
        {
            // Render a circle
            uint16 col = (uint16)s3eSurfaceConvertRGB(g_TouchColours[i*3], g_TouchColours[i*3+1], g_TouchColours[i*3+2]);
            RenderTouch(g_Touches[i].x + offset_x, g_Touches[i].y + offset_y, col);
        }
    }

    s3eDebugPrint(20, GetYBelowButtons(), g_TouchEventMsg, 1);
    PrintMessages(10,30);
}
