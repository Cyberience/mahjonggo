/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EIOSGameCenter S3E Game Center Example
 *
 * The following example demonstrates the iPhone Game Centre extension by
 * implementing a simple "game" where players can send messages to eachother.
 * Note that this example is similar in structure to the s3eIOSGameKit
 * example, but with matches replacing sessions and the session/match creation
 * handled automatically using Game Center's matchmaking facilities.
 *
 * The example also demonstrates Voice Chat, reporting achievements and sending
 * scores to the server to accumulate on leaderboards.
 *
 * This example only runs on iOS devices that support Apple Game Center.
 *
 * In order for the example to work with the Game Center servers you will need
 * to:
 * - Create an app on iTunes Connect
 * - Give the app a unique, full App/Bundle ID (eg com.mycompany.myapp)
 * - Set the example's iphone-appid mkb setting to match the ID you chose
 * - Register a Game Center user account (using the device's Game Center app)
 *
 * To test scores and leaderboards, you will also need to create one or more
 * leaderboards for the app through iTunes Connect. Sending a score with an
 * invalid leaderboard category will likely return
 * S3E_IOSGAMECENTER_ERR_COMMUNICATIONS_FAILURE.
 *
 * The example demonstrates a large amount of Game Center functionality,
 * including the following...
 *
 * Local player authentication:
 * - s3eIOSGameCenterAuthenticate()
 *
 * Handling players:
 * - s3eIOSGameCenterLoadFriends()
 * - s3eIOSGameCenterGetPlayers()
 * - s3eIOSGameCenterPlayersGetString()
 *
 * Externally hosted matches:
 * - s3eIOSGameCenterMatchmakerFindPlayersForHostedRequest()
 *
 * Programatic matchmaking:
 * - s3eIOSGameCenterMatchmakerCreateMatch()
 *
 * GUI Matchmaking:
 * - s3eIOSGameCenterMatchmakerGUI()
 *
 * Invites:
 * - s3eIOSGameCenterSetInviteHandler()
 * - s3eIOSGameCenterMatchmakerAddPlayersToMatch()
 * - s3eIOSGameCenterInviteAcceptGUI()
 *
 * Sending data:
 * - s3eIOSGameCenterSendDataToAllPlayers()
 *
 * Scores and leaderboards:
 * - s3eIOSGameCenterLeaderboardLoadCategories()
 * - s3eIOSGameCenterCreateLeaderboard()
 * - s3eIOSGameCenterLeaderboardLoadScores()
 *
 * Achievements:
 * - s3eIOSGameCenterReportAchievement()
 *
 * Voice Chat:
 * - s3eIOSGameCenterVoiceChatOpenChannel()
 *
 * @include s3eIOSGameCenter.cpp
 */

#include "ExamplesMain.h"
#include "s3eOSReadString.h"
#include "s3eIOSGameCenter.h"
#include <set>

// Fixed maximums for messages and amount of players supported.
#define NUM_MESSAGES 30
#define MESSAGE_LEN 80
#define MAX_OTHER_PLAYERS 3
#define MIN_OTHER_PLAYERS 1

// Buttons
static Button*  g_ButtonAuthenticate = 0;
static Button*  g_ButtonLeaderboard = 0;
static Button*  g_ButtonLeaderboardGUI = 0;
static Button*  g_ButtonAchievements = 0;
static Button*  g_ButtonAchievementInfo = 0;
static Button*  g_ButtonAchievementsGUI = 0;
static Button*  g_ButtonReportAchievement = 0;
static Button*  g_ButtonGetScores = 0;
static Button*  g_ButtonReportScore = 0;
static Button*  g_ButtonFindFriends = 0;
static Button*  g_ButtonFindPlayers = 0;
static Button*  g_ButtonStartJoinMatch = 0;
static Button*  g_ButtonGUIMatch = 0;
static Button*  g_ButtonGUIHosted = 0;
static Button*  g_ButtonStartWithPlayers = 0;
static Button*  g_ButtonAddPlayers = 0;
static Button*  g_ButtonVoiceChat = 0;
static Button*  g_ButtonActiveMicVoiceChat = 0;
static Button*  g_ButtonCancelMatchmaking = 0;
static Button*  g_ButtonSendData = 0;
static Button*  g_ButtonStop = 0;

static bool g_Authenticated = false;
static bool g_MatchmakingInProgress = false;
static bool g_MatchCreated = false;
static char g_UserName[S3E_IOSGAMECENTER_STRING_MAX] = "Unknown User";
static int g_NumFriendsFound = 0;
static char** g_FriendIDs = NULL;
static char** g_PlayerIDsInMatch = NULL;
static s3eIOSGameCenterVoiceChat* g_VoiceChat = NULL;
static void* g_InviteID = NULL;
static struct s3eIOSGameCenterAchievementList* g_Achievements = NULL;
static bool g_AchievementsLoaded = false;

// Display helper functions
static void printPlayerInfo(const char* alias, const char* playerID, bool showUnderage=false, int32 underage=0, bool isFriend=false)
{
    if (showUnderage)
    {
        if (underage != -1)
            AppendMessage("Underage: %s", underage ? "yes" : "no");
        else
            AppendMessageColour(RED, "Error querying underage");
    }

    AppendMessage("Player: %s %s (friend=%d)", playerID, alias, isFriend);
}

const char* ErrorAsString(s3eIOSGameCenterError error)
{
    switch(error)
    {
        case S3E_IOSGAMECENTER_ERR_NONE:
            return "NONE";
        case S3E_IOSGAMECENTER_ERR_PARAM:
            return "PARAM";
        case S3E_IOSGAMECENTER_ERR_UNAUTHENTICATED:
            return "UNAUTHENTICATED";
        case S3E_IOSGAMECENTER_ERR_FRIENDS_NOT_LOADED:
            return "FRIENDS_NOT_LOADED";
        case S3E_IOSGAMECENTER_ERR_ALREADY_REG:
            return "ALREADY_REG";
        case S3E_IOSGAMECENTER_ERR_GAME_UNRECOGNISED:
            return "GAME_UNRECOGNISED";
        case S3E_IOSGAMECENTER_ERR_AUTHENTICATION_IN_PROGRESS:
            return "AUTHENTICATION_IN_PROGRESS";
        case S3E_IOSGAMECENTER_ERR_INVALID_CREDENTIALS:
            return "INVALID_CREDENTIALS";
        case S3E_IOSGAMECENTER_ERR_UNDERAGE:
            return "UNDERAGE";
        case S3E_IOSGAMECENTER_ERR_COMMUNICATIONS_FAILURE:
            return "COMMUNICATIONS_FAILURE";
        case S3E_IOSGAMECENTER_ERR_CANCELLED:
            return "CANCELLED";
        case S3E_IOSGAMECENTER_ERR_USER_DENIED:
            return "DENIED";
        case S3E_IOSGAMECENTER_ERR_INVALID_PLAYER:
            return "INVALID_PLAYER";
        case S3E_IOSGAMECENTER_ERR_SCORE_NOT_SET:
            return "SCORE_NOT_SET";
        case S3E_IOSGAMECENTER_ERR_PARENTAL_CONTROLS_BLOCKED:
            return "PARENTAL_CONTROLS_BLOCKED";
        case S3E_IOSGAMECENTER_ERR_INVALID_MATCH_REQUEST:
            return "INVALID_MATCH_REQUEST";
        case S3E_IOSGAMECENTER_ERR_DEVICE:
        default:
            return "DEVICE/UNKNOWN";
    }
}


// Callback functions to report authentication, matchmaking, data events, etc...

static void GetPlayersCallback(s3eIOSGameCenterPlayerInfo* playersInfo)
{
    if (playersInfo->m_Error != S3E_IOSGAMECENTER_ERR_NONE)
    {
        AppendMessageColour(RED, "Get players info error: %s", ErrorAsString(playersInfo->m_Error));
        return;
    }

    if (!playersInfo->m_PlayerCount)
    {
        AppendMessage("`x996666Error? ReceivedPlayers callback indicates no players!");
        return;
    }

    for (int i = 0; i < playersInfo->m_PlayerCount; i++)
    {
        s3eIOSGameCenterPlayer* player = playersInfo->m_Players[i];

        // NB: GetString only guarantees the string will stay valid until the next call to GetString
        // so we have to copy it if calling more than once and expecting it to persist
        char alias[S3E_IOSGAMECENTER_STRING_MAX];
        strcpy(alias, s3eIOSGameCenterPlayerGetString(player, S3E_IOSGAMECENTER_PLAYER_ALIAS));

        printPlayerInfo(alias,
                        s3eIOSGameCenterPlayerGetString(player, S3E_IOSGAMECENTER_PLAYER_ID),
                        false, 0,
                        s3eIOSGameCenterPlayerGetInt(player, S3E_IOSGAMECENTER_PLAYER_IS_FRIEND) == 1);
    }

    AppendMessageColour(GREEN, "Players detailed info returned:");

    AppendMessageColour(BLUE, "---------------------------------------------------");

    s3eIOSGameCenterReleasePlayers(playersInfo->m_Players, playersInfo->m_PlayerCount);
}

static void LoadFriendsCallback(s3eIOSGameCenterError* error, void* userData)
{
    g_NumFriendsFound = 0;
    if (*error != S3E_IOSGAMECENTER_ERR_NONE)
    {
        AppendMessageColour(RED, "Load Friends Error: %s", ErrorAsString(*error));
    }
    else
    {
        AppendMessageColour(GREEN, "Load Friends request completed...");

        if (!g_FriendIDs)
            return;

        for (int i = 0; i < MAX_OTHER_PLAYERS; i++)
            g_FriendIDs[i][0] = 0;

        int32 friendsFound = s3eIOSGameCenterGetFriendIDs(g_FriendIDs, MAX_OTHER_PLAYERS);

        if (friendsFound == -1)
        {
            AppendMessageColour(RED, "Error requesting friend IDs");
        }
        else if (friendsFound == 0)
        {
            AppendMessage("`xeeee66No friends found");
        }
        else
        {
            g_NumFriendsFound = friendsFound > MAX_OTHER_PLAYERS ? MAX_OTHER_PLAYERS : friendsFound;
            AppendMessage("%d friends found. Saving first %d.", friendsFound, g_NumFriendsFound);
            AppendMessageColour(BLUE, "---------------------------------------------------");

            for (int i = 0; i < g_NumFriendsFound; i++)
                AppendMessage("(%d) %s", i, g_FriendIDs[i]);

            AppendMessageColour(GREEN, "Friend IDs stored:");
            AppendMessageColour(BLUE, "---------------------------------------------------");
            if (s3eIOSGameCenterGetPlayers((const char**)g_FriendIDs, g_NumFriendsFound, GetPlayersCallback) == S3E_RESULT_ERROR)
                AppendMessageColour(RED, "Error requesting friends from IDs: %s", ErrorAsString(s3eIOSGameCenterGetError()));
            else
                AppendMessageColour(BLUE, "Requesting friends info from IDs...");
        }
    }
}

static void LoadCategoriesCallback(s3eIOSGameCenterLoadCategoriesResult* result)
{
    for (int i = 0; i < result->m_CategoriesCount; i++)
    {
        AppendMessageColour(GREEN, "Catrgory: %s Title: %s", result->m_Categories[i], result->m_Titles[i]);
    }
    AppendMessageColour(GREEN, "Found %d leaderboards", result->m_CategoriesCount);
    AppendMessageColour(BLUE, "---------------------------------------------------");
}

static void ReportScoreCallback(s3eIOSGameCenterError* error)
{
    if (*error != S3E_IOSGAMECENTER_ERR_NONE)
    {
        AppendMessageColour(RED, "Score upload failed with error: %d", *error);

        if (*error == S3E_IOSGAMECENTER_ERR_COMMUNICATIONS_FAILURE)
            AppendMessageColour(RED, "Check score category is definied on iTunes Connect!");
    }
    else
        AppendMessageColour(GREEN, "Score upload succeeded");
    AppendMessageColour(BLUE, "---------------------------------------------------");
}

static void LoadScoresCallback(s3eIOSGameCenterLoadScoresResult* result)
{
    if (result->m_Error != S3E_IOSGAMECENTER_ERR_NONE)
    {
        AppendMessageColour(RED, "Error getting scores: %s", ErrorAsString(result->m_Error));
        return;
    }

    const char** pids = (const char**)malloc(sizeof(char*)*result->m_ScoreCount);
    for (int i = 0; i < result->m_ScoreCount; i++)
    {
        s3eIOSGameCenterScore& score = result->m_Scores[i];
        pids[i] = score.m_PlayerID;
        AppendMessageColour(GREEN, "rank: %d value: %d id: %s cat: %s", score.m_Rank, (int)score.m_Value, score.m_PlayerID, score.m_Category ? score.m_Category : "<none>");
    }
    AppendMessageColour(GREEN, "Found %d scores", result->m_ScoreCount);
    AppendMessageColour(BLUE, "---------------------------------------------------");
    s3eIOSGameCenterGetPlayers(pids, result->m_ScoreCount, GetPlayersCallback);
    free(pids);
}

static void FindPlayersCallback(s3eIOSGameCenterPlayerIDsInfo* playerIDsInfo, void* userData)
{
    g_MatchmakingInProgress = false;

    if (playerIDsInfo->m_Error != S3E_IOSGAMECENTER_ERR_NONE)
    {
        AppendMessageColour(RED, "Find players completed with error: %s", ErrorAsString(playerIDsInfo->m_Error));
        return;
    }

    if (!playerIDsInfo->m_PlayerCount)
    {
        AppendMessageColour(BLUE, "Error: No other players found for match!");
        return;
    }

    AppendMessageColour(BLUE, "---------------------------------------------------");

    for (int i = 0; i < playerIDsInfo->m_PlayerCount; i++)
    {
        AppendMessage("(%d) %s", i, playerIDsInfo->m_PlayerIDs[i]);
    }
    AppendMessageColour(GREEN, "Player IDs found for potential matches:");
    AppendMessageColour(BLUE, "---------------------------------------------------");

    if (s3eIOSGameCenterGetPlayers((const char**)playerIDsInfo->m_PlayerIDs, playerIDsInfo->m_PlayerCount, GetPlayersCallback) == S3E_RESULT_ERROR)
        AppendMessageColour(RED, "Error requesting friends from IDs: %s", ErrorAsString(s3eIOSGameCenterGetError()));
    else
        AppendMessageColour(BLUE, "Requesting friends info from IDs...");
}

static void UpdatePlayersInMatch()
{
    int32 actualPlayers = s3eIOSGameCenterGetPlayerIDsInMatch(g_PlayerIDsInMatch, MAX_OTHER_PLAYERS+1);
    int32 playersCount = 0;

    if (actualPlayers == -1)
    {
        AppendMessageColour(RED, "Error requesting player ID in match: %s", ErrorAsString(s3eIOSGameCenterGetError()));
    }
    else if (!actualPlayers)
    {
        AppendMessage("No players currently in match...");
    }
    else
    {
        playersCount = actualPlayers < MAX_OTHER_PLAYERS+1 ? actualPlayers : MAX_OTHER_PLAYERS+1;

        for (int i = 0; i < playersCount; i++)
        {
            AppendMessage("(%d) %s", i, g_PlayerIDsInMatch[i]);
        }
        AppendMessage("Players found: %d...", actualPlayers);
    }

    if (playersCount)
    {
        if (s3eIOSGameCenterGetPlayers((const char**)g_PlayerIDsInMatch, playersCount, GetPlayersCallback) == S3E_RESULT_ERROR)
            AppendMessageColour(RED, "Error requesting info for players in match: %s", ErrorAsString(s3eIOSGameCenterGetError()));
        else
            AppendMessageColour(BLUE, "Requesting player info from IDs...");
    }
}

static void CreateMatchCallback(s3eIOSGameCenterError* createMatchResult, void* userData)
{
    g_MatchmakingInProgress = false;

    if (*createMatchResult != S3E_IOSGAMECENTER_ERR_NONE)
    {
        AppendMessageColour(RED, "Create match completed with error: %s. Not creating match", ErrorAsString(*createMatchResult));
        return;
    }

    g_MatchCreated = true;

    AppendMessageColour(BLUE, "---------------------------------------------------");

    AppendMessageColour(GREEN, "! Match successfully created !");

    AppendMessage("Expected players: %d", s3eIOSGameCenterMatchGetInt(S3E_IOSGAMECENTER_MATCH_EXPECTED_PLAYERS));

    AppendMessageColour(BLUE, "App will periodically check for players joining...");

    AppendMessageColour(BLUE, "---------------------------------------------------");
}

static void AddPlayersToMatchCallback(s3eIOSGameCenterError* error, void* userData)
{
    AppendMessage("AddPlayersToMatchCallback: %s", ErrorAsString(*error));
    g_MatchmakingInProgress = false;
}


static void ReceiveDataCallback(s3eIOSGameCenterReceivedData* data)
{
    AppendMessage("Receive From: %s", data->m_PlayerID);
    AppendMessage("Receive Data: %d %s", data->m_DataSize, data->m_Data);
}

static void PlayerStateChangeCallback(s3eIOSGameCenterPlayerStateChangeInfo* stateChange)
{
    AppendMessage("PlayerStateChangeCallback");

    AppendMessage("PlayerID: %s", stateChange->m_PlayerID);

    const char* state = NULL;
    switch (stateChange->m_State)
    {
        default:
        case S3E_IOSGAMECENTER_PLAYER_STATE_UNKNOWN:
            state = "Unknown";
            break;
        case S3E_IOSGAMECENTER_PLAYER_STATE_CONNECTED:
            state = "Connected";
            break;
        case S3E_IOSGAMECENTER_PLAYER_STATE_DISCONNECTED:
            state = "Disconnected";
            break;
    }

    AppendMessage("Player state: %s", state);

    UpdatePlayersInMatch();
}

static void ConnectionFailureCallback(s3eIOSGameCenterError* error)
{
    AppendMessage("ConnectionFailureCallback: %s", ErrorAsString(*error));
}

static void ConnectToPlayerFailureCallback(s3eIOSGameCenterConnectWithPlayerResult* result)
{
    AppendMessage("ConnectToPlayerFailureCallback");
}

static void LoadAchievementsCallback(s3eIOSGameCenterAchievementList* list)
{
    // free g_Achievements structure (and contents) if allocated already
    if (g_Achievements)
    {
        s3eFree(g_Achievements->m_Achievements);
        s3eFree(g_Achievements);
    }

    // allocate memory for  g_Achievements structure
    g_Achievements = (s3eIOSGameCenterAchievementList*) s3eMalloc(sizeof(struct s3eIOSGameCenterAchievementList));
    memset(g_Achievements, 0, sizeof(struct s3eIOSGameCenterAchievementList));
    g_Achievements->m_Achievements = (s3eIOSGameCenterAchievement*) s3eMalloc(sizeof(struct s3eIOSGameCenterAchievement)*list->m_AchievementCount);
    g_Achievements->m_AchievementCount = list->m_AchievementCount;

    // populate g_Achievements->m_Achievements entries
    for (int i = 0 ; i < list->m_AchievementCount; i++)
    {
        s3eIOSGameCenterAchievement* info = &g_Achievements->m_Achievements[i];
        memcpy(info, &list->m_Achievements[i], sizeof(struct s3eIOSGameCenterAchievement));
        AppendMessage(" %s : %d", info->m_Identifier, info->m_PercentComplete);
    }
    AppendMessage("Got %d achievements", g_Achievements->m_AchievementCount);
    AppendMessageColour(BLUE, "---------------------------------------------------");

    // enable achievement submissions
    g_AchievementsLoaded = g_Achievements->m_AchievementCount ? true : false;
}

static void ReportAchievementCallback(s3eIOSGameCenterError* error)
{
    if (*error != S3E_IOSGAMECENTER_ERR_NONE)
        AppendMessageColour(RED, "Achievement Report Failed: %s", ErrorAsString(*error));
    else
        AppendMessageColour(GREEN, "Achievement Reported Successfully");
}

static void LoadAchievementInfoCallback(s3eIOSGameCenterAchievementInfoList* list)
{
    for (int i = 0; i < list->m_AchievementCount; i++)
    {
        s3eIOSGameCenterAchievementInfo* info = &list->m_Achievements[i];
        AppendMessage("  desc2: %s", info->m_UnachievedDescription);
        AppendMessage("   desc: %s", info->m_AchievedDescription);
        AppendMessage(" %s : %d : %s", info->m_Identifier, info->m_MaxPoints,info->m_Title);
    }
    AppendMessage("Got %d possible achievements", list->m_AchievementCount);
    AppendMessageColour(BLUE, "---------------------------------------------------");
}

static void InviteCallback(s3eIOSGameCenterInvite* invite)
{
    AppendMessageColour(BLUE, "---------------------------------------------------");
    AppendMessageColour(GREEN, "Invitation recieved from: id=%s hosted=%d", invite->m_InviterID, invite->m_Hosted);
    AppendMessageColour(BLUE, "---------------------------------------------------");
    s3eIOSGameCenterMatchCallbacks callbacks = {
        ConnectionFailureCallback,
        ConnectToPlayerFailureCallback,
        PlayerStateChangeCallback,
        ReceiveDataCallback };

    if (s3eIOSGameCenterInviteAcceptGUI(invite->m_InviteID, &callbacks) == S3E_TRUE)
    {
        g_MatchCreated = true;
        AppendMessageColour(GREEN, "Invite Accepted");
    }
    else
    {
        s3eIOSGameCenterError err = s3eIOSGameCenterGetError();
        if (err == S3E_IOSGAMECENTER_ERR_MATCHMAKING_IN_PROGRESS)
        {
            invite->m_RetainInviteID = true;
            g_InviteID = invite->m_InviteID;
            AppendMessage("Invite recieved while already matchmaking - retaining for later...");
        }
        else
            AppendMessageColour(RED, "Invite Declined/Cancelled - %s", ErrorAsString(s3eIOSGameCenterGetError()));
    }
}

static void AuthenticationCallback(s3eIOSGameCenterError* error, void* userData)
{
    if (*error != S3E_IOSGAMECENTER_ERR_NONE)
    {
        AppendMessageColour(RED, "Authentication Error: %s", ErrorAsString(*error));
        g_Authenticated = false;
        return;
    }

    g_Authenticated = true;
    const char* alias = s3eIOSGameCenterGetString(S3E_IOSGAMECENTER_LOCAL_PLAYER_ALIAS);

    if (strlen(alias) > 0)
        strcpy(g_UserName, alias);
    else
        strcpy(g_UserName, "!user alias error!");

    AppendMessageColour(BLUE, "---------------------------------------------------");
    printPlayerInfo(g_UserName,
                    s3eIOSGameCenterGetString(S3E_IOSGAMECENTER_LOCAL_PLAYER_ID),
                    true, s3eIOSGameCenterGetInt(S3E_IOSGAMECENTER_LOCAL_PLAYER_IS_UNDERAGE));
    AppendMessageColour(GREEN, "Local player authenticated:");
    AppendMessageColour(BLUE, "---------------------------------------------------");
    s3eIOSGameCenterSetInviteHandler(InviteCallback);
}

static void GetMyAchievementsHandler(Button* button)
{
    AppendMessageColour(BLUE, "Loading my achievements...");
    s3eIOSGameCenterLoadAchievements(LoadAchievementsCallback);
}

static void GetAchievementsHandler(Button* button)
{
    AppendMessageColour(BLUE, "Loading achievements info...");
    s3eIOSGameCenterLoadAchievementInfo(LoadAchievementInfoCallback);
}

static void LaunchGUIHandler(Button* button)
{
    s3eIOSGameCenterMatchRequest req;
    memset(&req, 0, sizeof(req)); // Group, attributes, players etc all set to 0
    req.m_MaxPlayers = MAX_OTHER_PLAYERS+1;
    req.m_MinPlayers = MIN_OTHER_PLAYERS+1;

    s3eIOSGameCenterMatchCallbacks callbacks = {
        ConnectionFailureCallback,
        ConnectToPlayerFailureCallback,
        PlayerStateChangeCallback,
        ReceiveDataCallback };
    if (s3eIOSGameCenterMatchmakerGUI(&req, &callbacks) == S3E_RESULT_SUCCESS)
    {
        AppendMessageColour(GREEN, "MatchMaker GUI success!");
        g_MatchCreated = true;
    }
    else
    {
        AppendMessageColour(RED, "MatchMaker GUI failed!");
    }
    AppendMessageColour(BLUE, "---------------------------------------------------");
}

static void LaunchGUIHostedHandler(Button* button)
{
    s3eIOSGameCenterMatchRequest req;
    memset(&req, 0, sizeof(req)); // Group, attributes, players etc all set to 0
    req.m_MaxPlayers = MAX_OTHER_PLAYERS+1;
    req.m_MinPlayers = MIN_OTHER_PLAYERS+1;

    if (s3eIOSGameCenterMatchmakerHostedGUI(&req, FindPlayersCallback) == S3E_RESULT_SUCCESS)
    {
        AppendMessageColour(GREEN, "MatchMaker Hosted GUI success!");
        AppendMessageColour(BLUE, "Returning list of players for externally hosted match...");
    }
    else
    {
        AppendMessageColour(RED, "MatchMaker Hosted GUI failed!");
    }
    AppendMessageColour(BLUE, "---------------------------------------------------");
}

static void ReportAchievementHandler(Button* button)
{
    // Ask user for completion data and report them

    for (int i = 0 ; i < g_Achievements->m_AchievementCount ; i++)
    {
        s3eIOSGameCenterAchievement* achievement = &g_Achievements->m_Achievements[i];
        achievement->m_DisplayNotificationBanner = S3E_TRUE;
        char message[512];
        sprintf(message, "Enter completion (0-100) for achievement \'%s\'", achievement->m_Identifier);
        const char* valueStr = s3eOSReadStringUTF8(message, S3E_OSREADSTRING_FLAG_NUMBER);
        if (valueStr)
        {
            int complete = atoi(valueStr);
            if(complete >= 0 && complete <= 100)
            {
                achievement->m_PercentComplete = complete;
            }
        }
    }

    s3eIOSGameCenterReportAchievements(g_Achievements, ReportAchievementCallback);
}

// Authenticate local player - prerequisite for other funcs to be used
static void AuthenticateLocalHandler(Button* button)
{
    // Start new attempt
    AppendMessageColour(BLUE, "%suthenticating local player...", g_Authenticated ? "Re-a" : "A");
    if (!g_Authenticated)
        g_Authenticated = s3eIOSGameCenterGetInt(S3E_IOSGAMECENTER_LOCAL_PLAYER_IS_AUTHENTICATED) ? true : false;

    if (g_Authenticated)
    {
        AppendMessageColour(GREEN, "Authenticated! (player already logged-in)");
        return;
    }

    s3eIOSGameCenterAuthenticate(AuthenticationCallback, NULL);
}

static void StartStopVoiceChat(Button* button)
{
    if (!g_VoiceChat)
    {
        g_VoiceChat = s3eIOSGameCenterVoiceChatOpenChannel("chatter");
        if (g_VoiceChat)
        {
            s3eIOSGameCenterVoiceChatSetInt(g_VoiceChat, S3E_IOSGAMECENTER_VOICE_CHAT_START, 1);
            SetButtonName(button, "Stop voice chat");
        }
        else
        {
            AppendMessageColour(RED, "Error starting voice chat: %s", ErrorAsString(s3eIOSGameCenterGetError()));
        }
    }
    else
    {
        s3eIOSGameCenterVoiceChatSetInt(g_VoiceChat, S3E_IOSGAMECENTER_VOICE_CHAT_START, 0);
        s3eIOSGameCenterVoiceChatCloseChannel(g_VoiceChat);
        SetButtonName(button, "Start voice chat");
        g_VoiceChat = NULL;
    }
}

static void VoiceChatPlayerStateHandler(s3eIOSGameCenterVoiceChatPlayerState* state)
{
    const char* stateStr = NULL;
    switch (state->m_State)
    {
        case S3E_IOSGAMECENTER_VOICE_CHAT_CONNECTED:
            stateStr = "Connected";
            break;
        case S3E_IOSGAMECENTER_VOICE_CHAT_DISCONNECTED:
            stateStr = "Disconnected";
            break;
        case S3E_IOSGAMECENTER_VOICE_CHAT_SPEAKING:
            stateStr = "Speaking";
            break;
        case S3E_IOSGAMECENTER_VOICE_CHAT_SILENT:
            stateStr = "Silent";
            break;
    }

    if (stateStr)
        AppendMessageColour(GREEN, "Chat: Player: %s is %s", state->m_PlayerID, stateStr);
}


void ExampleInit()
{
    if (!s3eIOSGameCenterAvailable())
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "GameCenter extension not found");
        s3eDeviceExit(1);
    }

    // don't show disabled buttons, to save screen realestate
    g_HideDisabledButtons = true;

    // Force small buttons to get everything on screen
    SetButtonScale(GetButtonScale()-1);

    g_ButtonAuthenticate       = NewButton("Authenticate local player", AuthenticateLocalHandler);
    g_ButtonAchievementInfo    = NewButton("Get achievement info", GetAchievementsHandler);
    g_ButtonAchievements       = NewButton("Get my achievements", GetMyAchievementsHandler);
    g_ButtonReportAchievement  = NewButton("Report achievement", ReportAchievementHandler);
    g_ButtonAchievementsGUI    = NewButton("Display achievements");
    g_ButtonLeaderboard        = NewButton("Get leaderboards");
    g_ButtonLeaderboardGUI     = NewButton("Display a leaderboard");
    g_ButtonGetScores          = NewButton("Get scores");
    g_ButtonReportScore        = NewButton("Report score");
    g_ButtonFindFriends        = NewButton("Find friends");
    g_ButtonFindPlayers        = NewButton("Find players for hosted match");
    g_ButtonStartJoinMatch     = NewButton("Start/join match");
    g_ButtonGUIMatch           = NewButton("Start match with GUI", LaunchGUIHandler);
    g_ButtonGUIHosted          = NewButton("Find players for hosted match with GUI", LaunchGUIHostedHandler);
    g_ButtonStartWithPlayers   = NewButton("Start/Join match with friends");
    g_ButtonAddPlayers         = NewButton("Invite friends to match");
    g_ButtonVoiceChat          = NewButton("Start voice chat", StartStopVoiceChat);
    g_ButtonActiveMicVoiceChat = NewButton("Active microphone");
    g_ButtonCancelMatchmaking  = NewButton("Cancel matchmaking");
    g_ButtonSendData           = NewButton("Send data to all players");
    g_ButtonStop               = NewButton("Disconnect from match");

    strcpy(g_UserName, "<not yet authenticated>");

    InitMessages(NUM_MESSAGES, MESSAGE_LEN);
    AppendMessageColour(BLUE, "    for externally hosted games");
    AppendMessageColour(BLUE, "    players using the app - these can then be used");
    AppendMessageColour(BLUE, " * 'Find any players' searches for any gamecenter");
    AppendMessageColour(BLUE, "    specify same category)");
    AppendMessageColour(BLUE, "    category string for the match (players must ");
    AppendMessageColour(BLUE, " * 'Start/Join category match' will prompt for a");
    AppendMessageColour(BLUE, "    like 'Invite friends...'");
    AppendMessageColour(BLUE, "    friends then store them for other functions");
    AppendMessageColour(BLUE, " * 'Find friends' will find and display a list of");
    AppendMessage("");
    AppendMessageColour(GREEN, "       Welcome to the s3e game center example! ");
    AppendMessageColour(BLUE, "---------------------------------------------------");
    AppendMessage("");

    g_FriendIDs = (char**)malloc(sizeof(char*) * MAX_OTHER_PLAYERS);
    for (int i = 0; i < MAX_OTHER_PLAYERS; i++)
    {
        g_FriendIDs[i] = (char*)malloc(sizeof(char) * S3E_IOSGAMECENTER_STRING_MAX);
        g_FriendIDs[i][0] = 0;
    }

    g_PlayerIDsInMatch = (char**)malloc(sizeof(char*) * MAX_OTHER_PLAYERS+1);
    for (int j = 0; j < MAX_OTHER_PLAYERS+1; j++)
    {
        g_PlayerIDsInMatch[j] = (char*)malloc(sizeof(char) * S3E_IOSGAMECENTER_STRING_MAX);
        g_PlayerIDsInMatch[j][0] = 0;
    }

    s3eIOSGameCenterSetVoiceChatUpdateHandler(VoiceChatPlayerStateHandler);
}

void ExampleTerm()
{
    if (g_MatchmakingInProgress)
        s3eIOSGameCenterCancelMatchmaking();

    if (g_MatchCreated)
        s3eIOSGameCenterMatchDisconnect();

    if (g_FriendIDs)
    {
        for (int i = 0; i < MAX_OTHER_PLAYERS; i++)
        {
            if (g_FriendIDs[i])
                free(g_FriendIDs[i]);
        }
        free(g_FriendIDs);
    }
}

bool ExampleUpdate()
{
    // Are buttons available
    g_ButtonAuthenticate->m_Enabled        = !g_Authenticated;
    g_ButtonLeaderboard->m_Enabled         = g_Authenticated;
    g_ButtonLeaderboardGUI->m_Enabled      = g_Authenticated;
    g_ButtonAchievements->m_Enabled        = g_Authenticated;
    g_ButtonReportAchievement->m_Enabled   = g_Authenticated && g_AchievementsLoaded;
    g_ButtonAchievementsGUI->m_Enabled     = g_Authenticated;
    g_ButtonAchievementInfo->m_Enabled     = g_Authenticated;
    g_ButtonGetScores->m_Enabled           = g_Authenticated;
    g_ButtonReportScore->m_Enabled         = g_Authenticated;
    g_ButtonStartJoinMatch->m_Enabled      = g_Authenticated && !g_MatchCreated && !g_MatchmakingInProgress;
    g_ButtonGUIMatch->m_Enabled            = g_Authenticated && !g_MatchCreated && !g_MatchmakingInProgress;
    g_ButtonGUIHosted->m_Enabled           = g_Authenticated && !g_MatchCreated && !g_MatchmakingInProgress;
    g_ButtonStartWithPlayers->m_Enabled    = g_Authenticated && !g_MatchCreated && !g_MatchmakingInProgress && g_NumFriendsFound;
    g_ButtonFindFriends->m_Enabled         = g_Authenticated;
    g_ButtonFindPlayers->m_Enabled         = g_Authenticated && !g_MatchmakingInProgress;
    g_ButtonAddPlayers->m_Enabled          = g_MatchCreated && g_NumFriendsFound;
    g_ButtonVoiceChat->m_Enabled           = g_MatchCreated;
    g_ButtonActiveMicVoiceChat->m_Enabled  = g_VoiceChat ? true : false;
    g_ButtonCancelMatchmaking->m_Enabled   = g_MatchmakingInProgress;
    g_ButtonSendData->m_Enabled            = g_MatchCreated;
    g_ButtonStop->m_Enabled                = g_MatchCreated;

    // Process invites recieved while busy
    if (g_InviteID)
    {
        if (g_MatchmakingInProgress || g_MatchCreated)
        {
            AppendMessage("Ignoring invite as local player started/is starting match...");
            s3eIOSGameCenterReleaseInvite(g_InviteID);
        }
        else
        {
            AppendMessage("Processing invite...");
            s3eIOSGameCenterMatchCallbacks callbacks = {
                ConnectionFailureCallback,
                ConnectToPlayerFailureCallback,
                PlayerStateChangeCallback,
                ReceiveDataCallback };
            if (s3eIOSGameCenterInviteAcceptGUI(g_InviteID, &callbacks) == S3E_TRUE)
            {
                g_MatchCreated = true;
                AppendMessageColour(GREEN, "Invite Accepted");
            }
            else
            {
                AppendMessageColour(RED, "Invite Declined/Cancelled - %s", ErrorAsString(s3eIOSGameCenterGetError()));
            }
        }
        g_InviteID = NULL;
    }

    Button* pressed = GetSelectedButton();

    if (pressed)
    {
        // Find list of friends (they dont have to be connected - just queries game center servers)
        if (pressed == g_ButtonFindFriends)
        {
            // Start new attempt
            AppendMessageColour(BLUE, "%loading friends...", g_NumFriendsFound ? "Re-l" : "L");
            g_NumFriendsFound = 0;

            if (s3eIOSGameCenterLoadFriends(LoadFriendsCallback, NULL) == S3E_RESULT_ERROR)
            {
                AppendMessageColour(RED, "Error starting to load friends: %s", ErrorAsString(s3eIOSGameCenterGetError()));
            }
        }

        if (pressed == g_ButtonGetScores)
        {
            s3eIOSGameCenterLeaderboard* board = s3eIOSGameCenterCreateLeaderboard(NULL, 0);
            const char* category = s3eOSReadStringUTF8("Limit by category (optional):");
            s3eIOSGameCenterLeaderboardSetString(board, S3E_IOSGAMECENTER_LEADERBOARD_CATEGORY, category);
            s3eIOSGameCenterLeaderboardLoadScores(board, LoadScoresCallback);
        }

        if (pressed == g_ButtonReportScore)
        {
            int score = 0;
            const char* scoreStr = s3eOSReadStringUTF8("Enter your score:", S3E_OSREADSTRING_FLAG_NUMBER);
            if(scoreStr != NULL)
            {
                score = atoi(scoreStr);
            }

            const char* category = s3eOSReadStringUTF8("Enter your category:");

            if(category != NULL)
            {
                s3eIOSGameCenterReportScore(score, category, ReportScoreCallback);
                AppendMessageColour(BLUE, "Uploading score %d under '%s'", score, category);
            }
            else
                AppendMessageColour(RED, "No Information Provided");
        }

        if (pressed == g_ButtonLeaderboard)
        {
            AppendMessageColour(BLUE, "Getting leaderboard categories...");
            s3eIOSGameCenterLeaderboardLoadCategories(LoadCategoriesCallback);
        }

        if (pressed == g_ButtonLeaderboardGUI)
        {
            const char* category = s3eOSReadStringUTF8("Enter category for leaderboard:");

            if(category != NULL)
            {
                AppendMessageColour(BLUE, "Display leaderboard for all scores ever for '%s' category", category);

                if (s3eIOSGameCenterLeaderboardShowGUI(category, S3E_IOSGAMECENTER_PLAYER_SCOPE_ALL_TIME) == S3E_RESULT_ERROR)
                    AppendMessageColour(RED, "Failed to get leaderboard");
            }
            else
                AppendMessageColour(RED, "No Information Provided");
        }

        if (pressed == g_ButtonAchievementsGUI)
        {
            if (s3eIOSGameCenterAchievementsShowGUI() == S3E_RESULT_ERROR)
                AppendMessageColour(RED, "Failed to get achievments");
        }

        // Find players for hosted match (searches for online users matching a request)
        if (pressed == g_ButtonFindPlayers)
        {
            AppendMessageColour(BLUE, "Searching for any players connected for this app...");
            g_MatchmakingInProgress = true;

            // Just request to find 3 random users
            s3eIOSGameCenterMatchRequest requestAllPlayers;
            memset(&requestAllPlayers, 0, sizeof(requestAllPlayers)); // Group, attributes, players etc all set to 0
            requestAllPlayers.m_MaxPlayers = MAX_OTHER_PLAYERS+1;
            requestAllPlayers.m_MinPlayers = MIN_OTHER_PLAYERS+1;

            if (s3eIOSGameCenterMatchmakerFindPlayersForHostedRequest(&requestAllPlayers, FindPlayersCallback, NULL) == S3E_RESULT_ERROR)
            {
                AppendMessageColour(RED, "Error starting to find players: %s", ErrorAsString(s3eIOSGameCenterGetError()));
                g_MatchmakingInProgress = false;
            }
        }

        // Start/join match with any players
        if (pressed == g_ButtonStartJoinMatch)
        {
            AppendMessageColour(BLUE, "Starting match with any players online...");
            g_MatchmakingInProgress = true;

            s3eIOSGameCenterMatchRequest request;
            memset(&request, 0, sizeof(request)); // Group, attributes, players etc all set to 0
            request.m_MaxPlayers = MAX_OTHER_PLAYERS+1;
            request.m_MinPlayers = MIN_OTHER_PLAYERS+1;

            s3eIOSGameCenterMatchCallbacks callbacks = {
            ConnectionFailureCallback,
            ConnectToPlayerFailureCallback,
            PlayerStateChangeCallback,
            ReceiveDataCallback };
            if (s3eIOSGameCenterMatchmakerCreateMatch(&request, CreateMatchCallback, &callbacks) == S3E_RESULT_ERROR)
            {
                AppendMessageColour(RED, "Error starting match: %s", ErrorAsString(s3eIOSGameCenterGetError()));
                g_MatchmakingInProgress = false;
            }
        }

        if (pressed == g_ButtonStartWithPlayers)
        {
            AppendMessageColour(BLUE, "Starting match with friends...");
            g_MatchmakingInProgress = true;

            s3eIOSGameCenterMatchRequest requestFriends;
            requestFriends.m_MaxPlayers = MAX_OTHER_PLAYERS+1;
            requestFriends.m_MinPlayers = MIN_OTHER_PLAYERS+1;
            requestFriends.m_PlayerGroup = 0;
            requestFriends.m_UsePlayerAttributes = S3E_FALSE;
            requestFriends.m_PlayersToInvite = g_FriendIDs;
            requestFriends.m_NumPlayersToInvite = g_NumFriendsFound;
            s3eIOSGameCenterMatchCallbacks callbacks = {
                ConnectionFailureCallback,
                ConnectToPlayerFailureCallback,
                PlayerStateChangeCallback,
                ReceiveDataCallback };

            if (s3eIOSGameCenterMatchmakerCreateMatch(&requestFriends, CreateMatchCallback, &callbacks) == S3E_RESULT_ERROR)
            {
                AppendMessageColour(RED, "Error starting match: %s", ErrorAsString(s3eIOSGameCenterGetError()));
                g_MatchmakingInProgress = false;
            }
        }

        if (pressed == g_ButtonAddPlayers)
        {
            AppendMessageColour(BLUE, "Adding friends to existing match...");
            g_MatchmakingInProgress = true;

            s3eIOSGameCenterMatchRequest requestFriends;
            requestFriends.m_MaxPlayers = MAX_OTHER_PLAYERS+1;
            requestFriends.m_MinPlayers = MIN_OTHER_PLAYERS+1;
            requestFriends.m_PlayerGroup = 0;
            requestFriends.m_UsePlayerAttributes = S3E_FALSE;
            requestFriends.m_PlayersToInvite = g_FriendIDs;
            requestFriends.m_NumPlayersToInvite = g_NumFriendsFound;

            if (s3eIOSGameCenterMatchmakerAddPlayersToMatch(&requestFriends, AddPlayersToMatchCallback, NULL) == S3E_RESULT_ERROR)
            {
                AppendMessageColour(RED, "Error adding players to match: %s", ErrorAsString(s3eIOSGameCenterGetError()));
                g_MatchmakingInProgress = false;
            }
        }

        if (pressed == g_ButtonActiveMicVoiceChat)
        {
            bool isActive = s3eIOSGameCenterVoiceChatGetInt(g_VoiceChat, S3E_IOSGAMECENTER_VOICE_CHAT_ACTIVE) ? true : false;

            if (s3eIOSGameCenterVoiceChatSetInt(g_VoiceChat, S3E_IOSGAMECENTER_VOICE_CHAT_ACTIVE, isActive ? 0 : 1)
                == S3E_RESULT_SUCCESS)
            {
                SetButtonName(g_ButtonActiveMicVoiceChat, isActive ? "Active microphone" : "Disable microphone");
            }
        }

        if (pressed == g_ButtonCancelMatchmaking)
        {
            AppendMessageColour(BLUE, "Cancelling all ongoing matchmaking requests...");
            g_MatchmakingInProgress = false;

            s3eIOSGameCenterCancelMatchmaking();
        }

        // Open text entry pop-up to send message
        if (pressed == g_ButtonSendData)
        {
            const char* usrMsg = s3eOSReadStringUTF8("Enter message to send:");
            AppendMessage("`x3333eeSend Message: `x666666%s", usrMsg);
            s3eIOSGameCenterSendDataToAllPlayers(usrMsg, strlen(usrMsg) + 1, S3E_IOSGAMECENTER_MATCH_SEND_DATA_RELIABLE);
        }

        // Stop current session
        if (pressed == g_ButtonStop)
        {
            AppendMessageColour(BLUE, "Disconnecting from match...");
            if (s3eIOSGameCenterMatchDisconnect() == S3E_RESULT_ERROR)
            {
                AppendMessageColour(RED, "Error disconnecting from match: %s", ErrorAsString(s3eIOSGameCenterGetError()));
            }
            else
            {
                AppendMessageColour(GREEN, "Disconnected!");
                g_MatchCreated = false;
            }
        }
    }

    return true;
}

void ExampleRender()
{
    int y = GetYBelowButtons();
    int x = 10*GetButtonScale();

    s3eDebugPrintf(x, y, 1, "`xee3333Local user: '%s'", g_UserName);
    y += 40;

    // Print messages from all peers, newest first
    PrintMessages(x, y);
}
