/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EIOSAudioRoute S3E Audio Route Example
 *
 * The following example plays a music file using S3E's audio functionality.
 * It also allows changing of audio output (route) via the s3eIOSAudioRoute
 * extension
 *
 * The main functions used to achieve this are:
 * - s3eDebugPrint()
 * - s3eKeyboardGetState()
 * - s3eAudioPlay()
 * - s3eAudioPause()
 * - s3eAudioResume()
 * - s3eAudioStop()
 * - s3eIOSAudioRouteSetCategory()
 * - s3eIOSAudioRouteSetDeviceOutput()
 *
 * The application waits for buttons to be pressed before using any of the
 * Audio functionality. Detection of button presses are handled using the
 * generic code in ExamplesMain.cpp
 *
 * The state of playback is output to screen using the s3eDebugPrint()
 * function.
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eIOSAudioRoute.png
 *
 * MKB File:
 * @include s3eIOSAudioRoute.mkb
 *
 * ICF File:
 * @include data/app.icf
 *
 * Source:
 * @include s3eIOSAudioRoute.cpp
 */

#include "ExamplesMain.h"
#include <s3eIOSBackgroundAudio.h>
#include "s3eOSReadString.h"
#include "s3eIOSAudioRoute.h"

#include <stdlib.h> // for atoi(...)

enum PlayerState
{
    STATE_PLAYING,
    STATE_PAUSED,
    STATE_STOPPED
};

static PlayerState g_Status = STATE_STOPPED; //player status information
static Button* g_ButtonPlay = 0;
static Button* g_ButtonPause = 0;
static Button* g_ButtonResume = 0;
static Button* g_ButtonStop = 0;
static Button* g_ButtonSetCategory = 0;
static Button* g_ButtonRouteAudioToReceiver = 0;
static Button* g_ButtonRouteAudioToSpeaker = 0;
static s3eBool g_MP3Supported = S3E_FALSE;

static int32 ResumeCall(void* systemData, void* userData)
{
    // App Resume Callback
    // Reset audio route catergory here. Application will reset
    // audio catergory to Ambient by default on resume

    // s3eIOSAudioRouteSetCategory(_)

    return 1;
}

int32 AudioStopCallback(void* systemData, void* userData)
{
    g_Status = STATE_STOPPED;
    return 0;
}

void ExampleInit()
{
    InitMessages(10,60);

    g_ButtonPlay                    = NewButton("Play");
    g_ButtonPause                    = NewButton("Pause");
    g_ButtonResume                    = NewButton("Resume");
    g_ButtonStop                    = NewButton("Stop");
    g_ButtonSetCategory                = NewButton("Set Category");
    g_ButtonRouteAudioToReceiver    = NewButton("Route Audio To Earpiece");
    g_ButtonRouteAudioToSpeaker        = NewButton("Route Audio To Speaker");

    g_ButtonPlay->m_Enabled                    = false;
    g_ButtonPause->m_Enabled                = false;
    g_ButtonResume->m_Enabled                = false;
    g_ButtonStop->m_Enabled                    = false;
    g_ButtonSetCategory->m_Enabled            = false;
    g_ButtonRouteAudioToReceiver->m_Enabled    = false;
    g_ButtonRouteAudioToSpeaker->m_Enabled    = false;

    g_MP3Supported = s3eAudioIsCodecSupported(S3E_AUDIO_CODEC_MP3);
    s3eAudioRegister(S3E_AUDIO_STOP, AudioStopCallback, NULL);

    if (!s3eIOSAudioRouteAvailable())
    {
        AppendMessageColour(RED,"Extension Not Available");
        return;
    }

    g_ButtonPlay->m_Enabled                    = true;
    g_ButtonPause->m_Enabled                = true;
    g_ButtonResume->m_Enabled                = true;
    g_ButtonStop->m_Enabled                    = true;
    g_ButtonSetCategory->m_Enabled            = true;
    g_ButtonRouteAudioToReceiver->m_Enabled    = true;
    g_ButtonRouteAudioToSpeaker->m_Enabled    = true;

    s3eDeviceRegister(S3E_DEVICE_UNPAUSE, ResumeCall, 0);
}

void ExampleTerm()
{
    s3eDebugTracePrintf("Audio playback finished");
    s3eAudioStop();
}

/*
 * If a button has been pressed then start/pause/resume/stop playback.
 */
bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    if (!g_MP3Supported)
        return true;

    if (pressed == g_ButtonPlay && (g_Status == STATE_STOPPED))
    {
        if (s3eAudioPlay("test.mp3", 0) == S3E_RESULT_SUCCESS)
            g_Status = STATE_PLAYING;
    }

    if (pressed == g_ButtonPause && (g_Status == STATE_PLAYING))
    {
        if (s3eAudioPause() == S3E_RESULT_SUCCESS)
            g_Status = STATE_PAUSED;
    }

    if (pressed == g_ButtonResume && (g_Status == STATE_PAUSED))
    {
        if (s3eAudioResume() == S3E_RESULT_SUCCESS)
            g_Status = STATE_PLAYING;
    }

    if (pressed == g_ButtonStop)
    {
        s3eAudioStop();
        g_Status = STATE_STOPPED;
    }

    if (pressed == g_ButtonSetCategory)
    {
        const char* chEventItem = NULL;
        int8 uiEventNo = 0;

        //Badge No:    // Badge number to be displayed. If 0 the badge is reset
        chEventItem = s3eOSReadStringUTF8("Enter a Category Number", S3E_OSREADSTRING_FLAG_NUMBER);
        uiEventNo = atoi(chEventItem);

        s3eResult result;
        s3eIOSAudioRouteCategory category;
        const char* msg;
        bool try_to_set_category = true;

        switch(uiEventNo)
        {
            case 1:
                result = s3eIOSAudioRouteSetCategory(S3E_IOSAUDIOROUTE_CATEGORY_AMBIENT);
                if (result == S3E_RESULT_SUCCESS)
                {
                    // We must explicitly enable audio mixing.
                    // Mixing is only available with audio session ambient.
                    bool bresult = s3eIOSBackgroundAudioSetMix(true);
                    if (bresult)
                        AppendMessageColour(GREEN, "Audio category set: Ambient");
                    else
                        AppendMessageColour(RED, "s3eIOSBackgroundAudioSetMix: failed");
                }
                else
                    AppendMessageColour(RED, "Audio category set: failed");
                try_to_set_category = false;
                break;

            case 2:
                category = S3E_IOSAUDIOROUTE_CATEGORY_SOLOAMBIENT;
                msg = "Audio category set: Solo Ambient";
                break;

            case 3:
                category = S3E_IOSAUDIOROUTE_CATEGORY_MEDIAPLAYBACK;
                msg = "Audio category set: Media Playback";
                break;

            case 4:
                category = S3E_IOSAUDIOROUTE_CATEGORY_RECORD;
                msg = "Audio category set: Record";
                break;

            case 5:
                category = S3E_IOSAUDIOROUTE_CATEGORY_RECORDANDPLAY;
                msg = "Audio category set: Record & Play";
                break;

            case 6:
                category = S3E_IOSAUDIOROUTE_CATEGORY_AUDIOPROCESSING;
                msg = "Audio category set: Audio Processing";
                break;

            default:
                AppendMessageColour(RED, "Audio category not available");
                try_to_set_category = false;
                break;
        }

        if (try_to_set_category)
        {
            result = s3eIOSAudioRouteSetCategory(category);
            if (result == S3E_RESULT_SUCCESS)
                AppendMessageColour(GREEN, msg);
            else
                AppendMessageColour(RED, "Audio category set: failed");
        }
    }

    if (pressed == g_ButtonRouteAudioToReceiver)
    {
        // This will default to the device headphones if they are plugged in. If not it will use
        // the device earpiece (*iPhone ONLY*)(iPod/iPad this will reset to the device speaker)
        s3eIOSAudioRouteSetDeviceOutput(S3E_IOSAUDIOROUTE_OUTPUT_RECEIVER);
        s3eDebugTracePrintf("Audio output set: Device Reciever");
    }

    if (pressed == g_ButtonRouteAudioToSpeaker)
    {
        s3eIOSAudioRouteSetDeviceOutput(S3E_IOSAUDIOROUTE_OUTPUT_SPEAKER);
        s3eDebugTracePrintf("Audio output set: Device Speaker");
    }


    return true;
}

/*
 * The following function outputs a set of strings. A switch is used to
 * output a string indicating the status. Each string is output using the
 * s3eDebugPrint() function.
 */
void ExampleRender()
{
    const int textHeight = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);

    g_ButtonPlay->m_Enabled = (g_Status == STATE_STOPPED);
    g_ButtonPause->m_Enabled = (g_Status == STATE_PLAYING);
    g_ButtonResume->m_Enabled = (g_Status == STATE_PAUSED);
    g_ButtonStop->m_Enabled = (g_Status == STATE_PLAYING);

    int ypos = GetYBelowButtons() + textHeight * 2;

    if (!g_MP3Supported)
    {
        g_ButtonPlay->m_Enabled = false;
        s3eDebugPrint(10, ypos, "`x666666Device does not support mp3 playback", 0);
        return;
    }

    // Display status
    switch (g_Status)
    {
        case STATE_PLAYING:
            s3eDebugPrint(10, ypos, "`x666666Music Playing", 0);
            break;
        case STATE_PAUSED:
            s3eDebugPrint(10, ypos, "`x666666Music Paused", 0);
            break;
        case STATE_STOPPED:
            s3eDebugPrint(10, ypos, "`x666666Music Stopped", 0);
            break;
    }

    // List of possible audio categories:
    int categoryPos[2] = {160,50};
        s3eDebugPrint(categoryPos[0] -5, categoryPos[1], "`x666666 Audio Category List:", 0);
        s3eDebugPrint(categoryPos[0],     categoryPos[1] += 16, "`x666666 1) Ambient", 0);
        s3eDebugPrint(categoryPos[0],     categoryPos[1] += 15, "`x666666 2) Solo Ambient", 0);
        s3eDebugPrint(categoryPos[0],     categoryPos[1] += 15, "`x666666 3) Media Playback", 0);
        s3eDebugPrint(categoryPos[0],     categoryPos[1] += 15, "`x666666 4) Record", 0);
        s3eDebugPrint(categoryPos[0],     categoryPos[1] += 15, "`x666666 5) Record & Play", 0);
        s3eDebugPrint(categoryPos[0],     categoryPos[1] += 15, "`x666666 6) Audio Processing", 0);


    ypos += 2*textHeight;
    int32 pos = s3eAudioGetInt(S3E_AUDIO_POSITION);
    s3eDebugPrintf(10, ypos, 0, "`x666666Position: %d.%d", pos / 1000, pos % 1000);

    int xpos = 15;
        ypos += 15;

    PrintMessages(xpos, ypos);
}
