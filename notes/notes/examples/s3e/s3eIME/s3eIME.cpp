/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EIME S3E IME Example
 *
 * The following example uses S3E's IME API to perform text input and render
 * to the screen. It uses the IwGxFont TTF font rendering API to render to the
 * screen (in order to support special characters). Input from s3eIME can be
 * on a per character basis, or on a per word basis, and the extension can
 * modify the text in the buffer at any stage.
 *
 * The following example allows the user to input text, and manipulate the
 * cursor position and selection range. The operating system can deliver text
 * from the user input via any method it wishes (hard keyboard, soft keyboard,
 * voice, etc.)
 *
 * The main functions and structures used to achieve this are:
 * - s3eIMERegister()
 * - s3eIMESetBuffer()
 * - s3eIMEGetBuffer()
 * - s3eIMEStartSession()
 * - s3eIMEEndSession()
 *
 * For more information about using IwGx or IwGxFont please see the relevant
 * documentation.
 *
 * The following graphic illustrates the example output:
 *
 * @image html s3eIME.png
 *
 * @include s3eIME.cpp
 */

#include "ExamplesMain.h"
#include "s3eIME.h"
#include <string.h>
#include "IwGx.h"
#include "IwResManager.h"

#include "IwGxFont.h"

#define BUTTON_ENABLE "enable ime input"
#define BUTTON_DISABLE "disable ime input"
#define BUTTON_BACK "Back"
#define BUTTON_FORWARD "Forward"
#define BUTTON_SELECT "Extend selection"

#define CURSOR_SPEED 200


// Display last 5 characters generated
#define NUM_CHARS 5
//static s3eWChar g_Chars[NUM_CHARS];
//static int g_NumChars = 0;

// Last 5 keys pressed down
static s3eKey g_Keys[NUM_CHARS];
static int g_NumKeys = 0;

// Last 5 keys released
static s3eKey g_KeysReleased[NUM_CHARS];
static int g_NumKeysReleased = 0;

static bool g_s3eKey = false;

static char g_Buffer[1024];
static CIwGxFont* s_Font8;
static bool g_BufferChanged;


// Callback event gets called whenever a key state changes
static int32 BufferChanged(void*, void*)
{
    IwTrace(DEBUG, ("BufferChanged!"));
    g_BufferChanged = true;
    return 0;
}

// Callback event gets called whenever a key state changes
static int32 handler(void* sys, void*)
{
    IwTrace(DEBUG, ("handler:%p", sys));

    s3eKeyboardEvent* event = (s3eKeyboardEvent*)sys;

    if (event->m_Pressed) // a key state changed from up to down
    {
        if (g_NumKeys < NUM_CHARS)
            g_NumKeys++;

        // Move previous entries down through the array and add new one at end
        memmove(g_Keys+1, g_Keys, (NUM_CHARS - 1) * sizeof(s3eKey));
        g_Keys[0] = event->m_Key;
    }
    else // state changed from down to up
    {
        if (g_NumKeysReleased < NUM_CHARS)
            g_NumKeysReleased++;

        memmove(g_KeysReleased+1, g_KeysReleased, (NUM_CHARS - 1) * sizeof(s3eKey));
        g_KeysReleased[0] = event->m_Key;
    }

    return 0;
}
static int32 handler2(void* sys, void*)
{
    // DUMMY EVENT, to sure that s3eKeyboard events are not working in s3eIME
    IwTrace(DEBUG, ("s3eKeyboard : handler2:%p", sys));
    g_s3eKey = true;
    return 0;
}

void ExampleInit()
{
    IwResManagerInit();
    IwGxFontInit();

    //Load the group containing the example font
    IwGetResManager()->LoadGroup("IwGxFontTTF.group");

    s_Font8 = (CIwGxFont*)IwGetResManager()->GetResNamed("Serif_8", "CIwGxFont");
    AddButton(BUTTON_ENABLE, 10, 10, 120, 20, s3eKeyMenu, NULL);

    AddButton(BUTTON_BACK, 10, 100, 120, 20, s3eKeyFirst, NULL);
    AddButton(BUTTON_FORWARD, 10, 125, 120, 20, s3eKeyFirst, NULL);
    AddButton(BUTTON_SELECT, 10,150,120,20, s3eKeyFirst, NULL);

    s3eKeyboardRegister(S3E_KEYBOARD_KEY_EVENT, handler2, NULL);

    if (s3eIMEAvailable())
    {
        // Register for callbacks when the buffer changes (not necessarily every character)
        s3eIMERegister(S3E_IME_BUFFER_CHANGED, BufferChanged, NULL);
        s3eIMESetBuffer("Initial Text");

        s3eIMERegister(S3E_IME_KEY_EVENT, handler, NULL);
    }
    g_BufferChanged = true;
}

void ExampleShutDown()
{
    // Clean up
    s3eIMEUnRegister(S3E_IME_BUFFER_CHANGED, BufferChanged);
    IwGxFontTerminate();
    IwResManagerTerminate();
}

bool ExampleUpdate()
{
    if (g_BufferChanged)
    {
        s3eDebugTraceLine("Got Buffer Changed callback, getting buffer...");
        s3eIMEGetBuffer(g_Buffer, sizeof(g_Buffer));
    }

    // damp response of show/hide
    static uint64 last_change = 0;
    if (s3eTimerGetMs() - last_change < CURSOR_SPEED)
    {
        return true;
    }

    const int pos = s3eIMEGetInt(S3E_IME_CURSOR_POS);
    const int len = s3eIMEGetInt(S3E_IME_BUFFER_LEN);
    const int sel = s3eIMEGetInt(S3E_IME_SELECTION_END);

    if (CheckButton(BUTTON_BACK))
    {
        if (pos > 0)
            s3eIMESetInt(S3E_IME_CURSOR_POS, pos - 1);
        last_change = s3eTimerGetMs();
        return true;
    }
    else if (CheckButton(BUTTON_FORWARD))
    {
        if (pos < len)
            s3eIMESetInt(S3E_IME_CURSOR_POS, pos + 1);

        last_change = s3eTimerGetMs();
        return true;
    }
    else if (CheckButton(BUTTON_SELECT))
    {
        if (sel < len)
        {
            s3eDebugTracePrintf("Selection end: %d", sel + 1);
            s3eIMESetInt(S3E_IME_SELECTION_END, sel + 1);
        }
        last_change = s3eTimerGetMs();
        return true;
    }

    if (CheckButton(BUTTON_ENABLE))
    {
        RemoveButton(BUTTON_ENABLE);

        s3eIMEStartSession();
        AddButton(BUTTON_DISABLE, 10, 10, 120, 20, s3eKeyMenu, NULL);
        last_change = s3eTimerGetMs();
    }
    else if (CheckButton(BUTTON_DISABLE))
    {
        RemoveButton(BUTTON_DISABLE);

        s3eIMEEndSession();
        AddButton(BUTTON_ENABLE, 10, 10, 120, 20, s3eKeyMenu, NULL);
        last_change = s3eTimerGetMs();
    }

    return true;
}

void ExampleRender()
{
    IwGxLightingOn();

    //Set font colour to black
    IwGxFontSetCol(0xffa0a0a0);

    //Set the formatting rect - this controls where the text appears and what it is formatted against
    IwGxFontSetRect(CIwRect(10,40,(int16)IwGxGetScreenWidth()-20,40));

    //Set the alignment within the formatting rect
    IwGxFontSetAlignmentVer(IW_GX_FONT_ALIGN_BOTTOM);

    IwGxFontSetFont(s_Font8);
    if (!s3eIMEAvailable())
    {
        IwGxFontDrawText("Could not load IME extension");
    }
    else
    {
        if (g_BufferChanged)
            s3eDebugTracePrintf("Rendering buffer: %s", g_Buffer);
        // Draw title text
        IwGxFontDrawText(g_Buffer);
        if (g_BufferChanged)
            s3eDebugTracePrintf("Rendered buffer");
        g_BufferChanged = false;
    }

    const int pos = s3eIMEGetInt(S3E_IME_CURSOR_POS);
    const int len = s3eIMEGetInt(S3E_IME_BUFFER_LEN);
    const int sel = s3eIMEGetInt(S3E_IME_SELECTION_END);

    char msg[128];
    sprintf(msg, "Length: %d Cursor Pos: %d Selection: %d", len, pos, sel);
    IwGxFontSetRect(CIwRect(10,170,(int16)IwGxGetScreenWidth()-20,40));
    IwGxFontDrawText(msg);

    ////////////////////////////////////////////////////////////
    int lineHeight = 14;
    int y = 20;
    int x = 140;
    IwGxFontSetRect(CIwRect(x,y,(int16)IwGxGetScreenWidth(),10));
    //IwGxFontSetAlignmentVer(IW_GX_FONT_ALIGN_BOTTOM);
    IwGxFontDrawText("IME Keys pressed:");

    char name[128];

    // Display last few keys that were pressed down
    y += lineHeight;

    for (int j = g_NumKeys-1; j >= 0; j--)
    {
        s3eKey key = g_Keys[j];
        s3eKeyboardGetDisplayName(name, key);

        if (key >= s3eKeyAbsGameA && key <= s3eKeyAbsBSK)
            sprintf(msg, "Key: %s (%d - abstract %d)", name, key, key - s3eKeyAbsGameA);
        else
            sprintf(msg, "Key: %s (%d)", name, key);

        IwGxFontSetRect(CIwRect(x,y,(int16)IwGxGetScreenWidth()-20,10));
        IwGxFontDrawText(msg);
        y += lineHeight;
    }

    y += lineHeight;

    // Display last few keys that were released
    IwGxFontSetRect(CIwRect(x,y,(int16)IwGxGetScreenWidth()-20,10));
    IwGxFontDrawText("IME Keys released:");
    y += lineHeight;
    for (int k = g_NumKeysReleased-1; k >= 0; k--)
    {
        s3eKey key = g_KeysReleased[k];
        s3eKeyboardGetDisplayName(name, key);

        if (key >= s3eKeyAbsGameA && key <= s3eKeyAbsBSK)
            sprintf(msg, "Key: %s (%d - abstract %d)", name, key, key - s3eKeyAbsGameA);
        else
            sprintf(msg, "Key: %s (%d)", name, key);

        IwGxFontSetRect(CIwRect(x,y,(int16)IwGxGetScreenWidth()-20,10));
        IwGxFontDrawText(msg);
        y += lineHeight;
    }

    // show is s3eKeyboeard callback called
    IwGxFontSetRect(CIwRect(x,220,(int16)IwGxGetScreenWidth()-20,10));
    IwGxFontDrawText(g_s3eKey ? "s3eKey: YES" :  "s3eKey: NO");


    //Flush and show
    IwGxFlush();
    IwGxSwapBuffers();
}
