/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

#include "ExamplesMain.h"
#include "s3eWebView.h"
#include "s3e.h"
#include "IwGx.h"


/**
 *
 * @page ExampleS3EWebViewAdvanced S3E Web View Advanced Example
 * This example uses the asynchronous methods of s3eWebView to create two
 * seperate web views. Each view can receive focus as the user interacts
 * with them. The native web view is rendered on top of an IwGX surface.
 *
 * The functions used to achieve this are:
 * - s3eWebViewAvailable()
 * - s3eWebViewCreate()
 * - s3eWebViewShow()
 * - s3eWebViewNavigate()
 * - s3eWebViewHide()
 * - s3eWebViewDestroy()
 * - s3eWebViewClearCache()
 *
 * Also see s3eWebViewBasic for an example of creating a full screen modal web view.
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eWebViewAdvancedImage.png
 *
 * @include s3eWebViewAdvanced.cpp
 */

static int g_ButtonHide;
static int g_ButtonShow;
static int g_ButtonModal;
static int g_ButtonCache;

static s3eWebView* g_LeftPanel;
static s3eWebView* g_RightPanel;

#define BUTTON_SHOW "Show Web Views"
#define BUTTON_HIDE "Hide Web Views"
#define BUTTON_MODAL "Modal Web View"
#define BUTTON_CACHE "Clear Cache"

#define MESSAGE_SIZE 512
static char g_Message[MESSAGE_SIZE];

// Callback method, called when a new page is about to be loaded
static int loadingCallback(s3eWebView* instance, void* sysData, void* userData)
{
    const char* url = (const char*)sysData;
    snprintf(g_Message, MESSAGE_SIZE, "Loading: %s", url);
    IwTrace(APP, ("loadingCallback: %s", url));
    return 1;
}

// Callback method, called when a page is finished loading
static int loadedCallback(s3eWebView* instance, void* sysData, void* userData)
{
    const char* url = (const char*) sysData;
    snprintf(g_Message, MESSAGE_SIZE, "Loaded: %s", url);
    IwTrace(APP, ("loadedCallback: %s", url));
    return 1;
}

// Rotate the layout on rotation
static void rotationCallback()
{
    // Draw two panels, side by side
    const int width  = IwGxGetScreenWidth();
    const int height = IwGxGetScreenHeight();
    const int y = 60;

    s3eWebViewHide(g_LeftPanel);
    s3eWebViewHide(g_RightPanel);
    s3eWebViewShow(g_LeftPanel, 0, y, (width/2), height - (y*2));
    s3eWebViewShow(g_RightPanel, (width/2) + 10 , y, (width/2) - 10, height - (y*2));
}

void ExampleInit()
{
    IwGxInit();

    if (!s3eWebViewAvailable())
        return;

    // Create buttons for UI
    g_ButtonShow = AddButton(BUTTON_SHOW, 10, 10, 90, 40, s3eKey1);
    g_ButtonHide = AddButton(BUTTON_HIDE, 110, 10, 90, 40, s3eKey2);
    g_ButtonModal = AddButton(BUTTON_MODAL, 210, 10, 90, 40, s3eKey3);
    g_ButtonCache = AddButton(BUTTON_CACHE, 310, 10, 90, 40, s3eKey4);

    snprintf(g_Message, MESSAGE_SIZE, "s3eWebViewAdvanced");

    // Create two parallel web views
    g_LeftPanel  = s3eWebViewCreate();
    g_RightPanel = s3eWebViewCreate();

    // Register for page loading events
    s3eWebViewRegister(S3E_WEBVIEW_STARTED_LOADING, loadingCallback, NULL, g_LeftPanel);
    s3eWebViewRegister(S3E_WEBVIEW_FINISHED_LOADING, loadedCallback, NULL, g_LeftPanel);
    s3eWebViewRegister(S3E_WEBVIEW_STARTED_LOADING, loadingCallback, NULL, g_RightPanel);
    s3eWebViewRegister(S3E_WEBVIEW_FINISHED_LOADING, loadedCallback, NULL, g_RightPanel);

    s3eWebViewNavigate(g_LeftPanel,  "http://www.google.com");
    s3eWebViewNavigate(g_RightPanel, "http://www.facebook.com");
}
void ExampleShutDown()
{
    // Clean up views
    s3eWebViewDestroy(g_LeftPanel);
    s3eWebViewDestroy(g_RightPanel);

    // Unregister callbacks
    s3eWebViewUnRegister(S3E_WEBVIEW_STARTED_LOADING, loadingCallback, g_LeftPanel);
    s3eWebViewUnRegister(S3E_WEBVIEW_FINISHED_LOADING, loadedCallback, g_LeftPanel);
    s3eWebViewUnRegister(S3E_WEBVIEW_STARTED_LOADING, loadingCallback, g_RightPanel);
    s3eWebViewUnRegister(S3E_WEBVIEW_FINISHED_LOADING, loadedCallback, g_RightPanel);

    IwGxUnRegister(IW_GX_SCREENSIZE, rotationCallback);
    IwGxTerminate();
}

bool ExampleUpdate()
{
    if (CheckButton(BUTTON_SHOW) & S3E_KEY_STATE_PRESSED)
    {
         // Draw two panels, side by side
        const int width  = IwGxGetScreenWidth();
        const int height = IwGxGetScreenHeight();
        const int y = 80;

        s3eWebViewShow(g_LeftPanel, 0, y, width/2, height - (y*2));
        s3eWebViewShow(g_RightPanel, (width/2) + 10 , y, (width/2) - 10, height - (y*2));
        IwGxRegister(IW_GX_SCREENSIZE, rotationCallback);

        s3eDebugTraceLine("Created web views");
    }
    else if (CheckButton(BUTTON_HIDE) & S3E_KEY_STATE_PRESSED)
    {
        s3eWebViewHide(g_LeftPanel);
        s3eWebViewHide(g_RightPanel);
        IwGxUnRegister(IW_GX_SCREENSIZE, rotationCallback);
    }
    else if (CheckButton(BUTTON_MODAL) & S3E_KEY_STATE_PRESSED)
    {
        s3eWebViewCreateModal("http://www.bbc.co.uk");
    }
    else if (CheckButton(BUTTON_CACHE) & S3E_KEY_STATE_PRESSED)
    {
        if (g_LeftPanel)
            s3eWebViewClearCache(g_LeftPanel);
        if (g_RightPanel)
            s3eWebViewClearCache(g_RightPanel);
    }
    return true;
}

/*
 * The following function display the user's prompt and any error messages.
 * The s3eDebugPrint() function to print the strings.
 */
void ExampleRender()
{
    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Start with no lighting
    IwGxLightingOff();

    // Print results
    if (!s3eWebViewAvailable())
    {
        IwGxPrintString(10, 60, "`xff0000Web view isn't available on this device.", 1);
    }
    else
    {
        IwGxPrintString(10, 60, g_Message);
    }

    // End drawing
    IwGxFlush();

    // Swap buffers
    IwGxSwapBuffers();
}
