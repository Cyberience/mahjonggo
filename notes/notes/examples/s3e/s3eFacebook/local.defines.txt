ENABLE_OLD_BUTTONS    If defined show some buttons for no longer supported commands
DONT_FORCE_LOGIN      Disable switching between forced and non-forced web-view login. Forced means will always request new login.
USE_IOS_SYSTEM_LOGIN  Use facebook system login instead of logging in via Facebook app
