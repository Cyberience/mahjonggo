/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EFacebook S3E Facebook Example
 *
 * The following example demonstrates the Facebook extension.
 * It interacts with the Facebook website through Facebook Connect.
 *
 * This example runs on Android, iOS, Windows Phone and Windows Store platforms.
 *
 * The main functions used to achieve this are:
 * - s3eFBInit()
 * - s3eFBSession_Login()
 * - s3eFBSession_ReauthorizeWithPublishPermissions()
 * - s3eFBSession_LoggedIn()
 * - s3eFBSession_AccessToken()
 * - s3eFBTerminate()
 * - s3eFBDialog_WithAction()
 * - s3eFBDialog_AddParamString()
 * - s3eFBDialog_Show()
 * - s3eFBRequest_WithGraphPath()
 * - s3eFBRequest_WithMethodName()
 * - s3eFBRequest_AddParamString()
 * - s3eFBRequest_Send()
 * - s3eFBRequest_ResponseDictionaryItemAsString()
 * - s3eFBSLComposeViewController_Create()
 * - s3eFBSLComposeViewController_Show()
 *
 * You need to register and create a Facebook application at:
 * www.facebook.com/developers/ The example interacts with Facebook
 * by posting updates and retrieving information via this on-line application.
 *
 * Simply clicking Create New Application on Facebook and giving it a name will
 * be enough to create a basic application that this example can interact with.
 *
 * The app will have "App ID" and "App secret" strings which identify it need to
 * hard-coded as FACEBOOK_APP_ID and FACEBOOK_APP_SECRET below.
 *
 * For iOS, you must also update the iphone-bundle-url-schemes deployment option
 * in s3eFacebook.mkb to "fbFACEBOOK_APP_IDs3efacebookexample" (You can use anything
 * for suffix, but it should match the rules described at
 * https://developers.facebook.com/docs/ios/troubleshooting#sharedappid (lower case
 * letters and numbers only) and should match the suffix passed to s3eFBInit).
 * However, if you are launching this application from the Marmalade Hub, you
 * should not modify the mkb directly. Instead, you should enter
 * fbFACEBOOK_APP_IDs3eFacebookExample in the project's iOS
 * configuration page > Advanced tab > Bundle URL Name field.  This URL scheme
 * is required by Facebook to communicate with your App.
 *
 * Similarly, for Android, you must set android-extra-strings="(app_id,FACEBOOK_APP_ID)"
 * in s3eFacebook.mkb, or if using the Hub, in the Extra Strings section in
 * the Advanced section of the project's Android configuration page.
 *
 * The example provides buttons to first log in (and create a session) and then
 * to post messages to the logged-in user's updates stream, using both the
 * request system and the built-in stream-preview GUI.
 *
 * Reauthorization is now required by Facebook to obtain publish permissions.
 *
 * s3eFBSLComposeViewController is supported only on iOS 6+.
 *
 * @include s3eFacebook.cpp
 */
#include "ExamplesMain.h"
#include "s3eOSReadString.h"
#include "s3eFacebook.h"

#include "IwDebug.h"
#include <string>
#include <vector>
#include <algorithm>

// Replace these with your own strings from your Facebook application
// These hex numbers are displayed on the application's main page
// Also update "iphone-bundle-url-schemes" in the deployment section of s3eFacebook.mkb
const char* FACEBOOK_APP_ID = NULL;
const char* FACEBOOK_URL_SUFFIX = "s3efacebookexample";

static Button*  g_ButtonLogin = 0;
static Button*  g_ButtonLogin2 = 0;
static Button*  g_ButtonClose = 0;
static Button*  g_ButtonClearAndClose = 0;
static Button*  g_ButtonAuthorized = 0;
static Button*  g_ButtonReauthorize = 0;
static Button*  g_ButtonStreamDialog = 0;
static Button*  g_ButtonSendRequest = 0;
static Button*  g_ButtonSendRequest2 = 0;
static Button*  g_ButtonGetUID = 0;
static Button*  g_ButtonGetName = 0;
static Button*  g_ButtonGetName2 = 0;
static Button*  g_ButtonGetAccessToken = 0;
static Button*  g_ButtonSLComposeViewControllerURL = 0;
static Button*  g_ButtonSLComposeViewControllerImage = 0;

static s3eFBSession* g_CurrentSession = 0;
static s3eFBRequest* g_Request = 0;
static s3eFBRequest* g_GetUID = 0;
static s3eFBRequest* g_GetName = 0;

static s3eFBSLComposeViewController* g_SLComposeViewController = 0;

std::vector<const char*> g_publish_permissions;
std::vector<const char*> g_read_permissions;
static char g_UID[128] = "";

#if S3EFACEBOOK_API_VERSION >= 2
static s3eBool g_ForceWebNextTime = S3E_FALSE;
#else
static s3eBool g_ForceWebNextTime = S3E_TRUE;
#endif


enum s3eFBStatus
{
    FB_UNINITIALISED,
    FB_LOGGED_OUT,
    FB_LOGGING_IN,
    FB_REQUESTING_PUBLISH_PERMISSIONS,
    FB_LOGGED_IN
};

static s3eFBStatus g_SessionStatus = FB_UNINITIALISED;

#define MESSAGE_LEN 1024
char g_StatusMessage[MESSAGE_LEN+1] = {0};

static void EnableButton(Button* button, bool enabled)
{
    if (button)
        button->m_Enabled = enabled;
}

static void UpdateState()
{
    if (g_SessionStatus == FB_LOGGED_IN)
    {
        // Check for failure causing session to become invalid
        if (!s3eFBSession_LoggedIn(g_CurrentSession))
        {
            g_SessionStatus = FB_LOGGED_OUT;
        }
    }

    SetButtonName(g_ButtonLogin2, g_ForceWebNextTime ? "Log-in (using web, force login)" : "Log-in (using web)");

    if (g_SessionStatus == FB_UNINITIALISED ||
        g_SessionStatus == FB_LOGGED_OUT)
    {
        EnableButton(g_ButtonLogin, true);
        EnableButton(g_ButtonLogin2, true);
        EnableButton(g_ButtonClose, false);
        EnableButton(g_ButtonClearAndClose, false);
        EnableButton(g_ButtonAuthorized, false);
        EnableButton(g_ButtonReauthorize, false);
        EnableButton(g_ButtonStreamDialog, false);
        EnableButton(g_ButtonSendRequest, false);
        EnableButton(g_ButtonSendRequest2, false);
        EnableButton(g_ButtonGetUID, false);
        EnableButton(g_ButtonGetName, false);
        EnableButton(g_ButtonGetName2, false);
        EnableButton(g_ButtonGetAccessToken, false);
        EnableButton(g_ButtonSLComposeViewControllerURL, false);
        EnableButton(g_ButtonSLComposeViewControllerImage, false);
    }
    else if (g_SessionStatus == FB_LOGGING_IN ||
             g_SessionStatus == FB_REQUESTING_PUBLISH_PERMISSIONS)
    {
        EnableButton(g_ButtonLogin, false);
        EnableButton(g_ButtonLogin2, false);
        EnableButton(g_ButtonClose, false);
        EnableButton(g_ButtonClearAndClose, false);
        EnableButton(g_ButtonAuthorized, false);
        EnableButton(g_ButtonReauthorize, false);
        EnableButton(g_ButtonStreamDialog, false);
        EnableButton(g_ButtonSendRequest, false);
        EnableButton(g_ButtonSendRequest2, false);
        EnableButton(g_ButtonGetUID, false);
        EnableButton(g_ButtonGetName, false);
        EnableButton(g_ButtonGetName2, false);
        EnableButton(g_ButtonGetAccessToken, false);
        EnableButton(g_ButtonSLComposeViewControllerURL, false);
        EnableButton(g_ButtonSLComposeViewControllerImage, false);
    }
    else if (g_SessionStatus == FB_LOGGED_IN)
    {
        EnableButton(g_ButtonLogin, false);
        EnableButton(g_ButtonLogin2, false);
        EnableButton(g_ButtonClose, true);
        EnableButton(g_ButtonClearAndClose, true);
        EnableButton(g_ButtonAuthorized, true);
        EnableButton(g_ButtonReauthorize, true);
        EnableButton(g_ButtonStreamDialog, true);
        EnableButton(g_ButtonSendRequest, g_Request == NULL);
        EnableButton(g_ButtonSendRequest2, true); // TODO grey out if we're busy
        EnableButton(g_ButtonGetUID, g_GetUID  == NULL);
        EnableButton(g_ButtonGetName, g_GetName == NULL);
        EnableButton(g_ButtonGetName2, true); // TODO grey out if we're busy
        EnableButton(g_ButtonGetAccessToken, true);
        EnableButton(g_ButtonSLComposeViewControllerURL, s3eFBSLComposeViewController_IsAvailable() ? true : false);
        EnableButton(g_ButtonSLComposeViewControllerImage, s3eFBSLComposeViewController_IsAvailable() ? true : false);
    }
}

// Callback functions to report request responses...
static bool not_equals(const char* a, const char* b)
{
    return std::string(a).compare(std::string(b)) != 0;
}
static void FBReauthorizeWithPublishPermissionsCallback(struct s3eFBSession* session, s3eResult* reauthorizeResult, void* userData)
{
    IwTrace(FACEBOOK, ("FBReauthorizeWithPublishPermissionsCallback %p %p %p", session, reauthorizeResult, userData));

    const char* const * tmp = s3eFBSession_GetPermissions(session);
    std::vector<const char*> all_permissions;
    for (int j = 0; tmp[j] != NULL; j++)
    {
        IwTrace(FACEBOOK, ("permission granted: %s", tmp[j]));
        all_permissions.push_back(tmp[j]);
    }

    // verify intersection of permissions
    std::vector<const char*>::iterator it;
    std::vector<const char*> *intersection = new std::vector<const char*>(std::min(g_publish_permissions.size(), all_permissions.size()));
    it = std::set_intersection(
        all_permissions.begin(), all_permissions.end(),
        g_publish_permissions.begin(), g_publish_permissions.end(),
        intersection->begin(), not_equals);
    intersection->resize(it - intersection->begin());

    for (unsigned int j = 0; j < intersection->size(); j++)
    {
        IwTrace(FACEBOOK, ("required publish permission granted: %s", (*intersection)[j]));
    }

    bool hasRequiredPermissions = intersection->size() == g_publish_permissions.size();

    g_SessionStatus = FB_LOGGED_IN;

    if (*reauthorizeResult == S3E_RESULT_SUCCESS && hasRequiredPermissions)
    {
        sprintf(g_StatusMessage, "`x66ee66Facebook Reathorisation with Publish Permissions Successful");
    }
    else if (!hasRequiredPermissions)
    {
        sprintf(g_StatusMessage, "`xee6666Facebook Reathorisation with Publish Permissions Failed (permission count: %d).", (int)intersection->size());
    }
    else
    {
        sprintf(g_StatusMessage, "`xee6666Facebook Reathorisation with Publish Permissions Failed");
    }

    delete intersection;

    UpdateState();
}

static void FBLoginCallback(struct s3eFBSession* session, s3eResult* loginResult, void* userData)
{
    IwTrace(FACEBOOK, ("FBLoginCallback %p %p %p", session, loginResult, userData));

    if (*loginResult == S3E_RESULT_SUCCESS)
    {
        g_SessionStatus = FB_LOGGED_IN;
        sprintf(g_StatusMessage, "`x66ee66Facebook Log-In succeeded");
    }
    else
    {
        g_SessionStatus = FB_LOGGED_OUT;
        sprintf(g_StatusMessage, "`xee6666Facebook Log-In failed");
    }

    UpdateState();
}

static bool StringIsNotEmpty(const char* str)
{
    return (str && strlen(str));
}

static void FBStreamDialogCallback(struct s3eFBDialog* dialog, s3eResult* streamResult, void* userData)
{
    IwTrace(FACEBOOK, ("FBStreamDialogCallback %p %p %p", dialog, streamResult, userData));

    if (*streamResult == S3E_RESULT_SUCCESS)
    {
        const char* overall_url = s3eFBDialog_DidCompleteWithUrl(dialog, NULL);
        if (StringIsNotEmpty(overall_url))
        {
            // there is a payload - on an error we'd get error_code and error_msg
            // error_msg will have already been shown to the user.
            const char* error_code = s3eFBDialog_DidCompleteWithUrl(dialog, "error_code");
            const char* error_msg = s3eFBDialog_DidCompleteWithUrl(dialog, "error_msg");
            if (StringIsNotEmpty(error_code) || StringIsNotEmpty(error_msg))
            {
                // NOTE: checking for both error_code and _msg is prob overkill
                if (StringIsNotEmpty(error_code))
                {
                snprintf(g_StatusMessage, MESSAGE_LEN,
                         "`xee6666Stream dialog error_code=%s", error_code);
                }
                else
                {
                snprintf(g_StatusMessage, MESSAGE_LEN,
                         "`xee6666Stream dialog error_msg=%s", error_msg);
                }
            }
            else
            {
                // on success we get post_id, check for that
                const char* post_id = s3eFBDialog_DidCompleteWithUrl(dialog, "post_id");
                if (StringIsNotEmpty(post_id))
                {
                    snprintf(g_StatusMessage, MESSAGE_LEN,
                             "`x66ee66Stream dialog succeeded post_id=%s", post_id);
                }
                else
                {
                    // unexpected
                    snprintf(g_StatusMessage, MESSAGE_LEN,
                             "`xee6666Stream dialog unexpected response url=%s", overall_url ? overall_url : "");
                }
            }
        }
        else
        {
            sprintf(g_StatusMessage, "`xee6666Stream dialog cancelled by user");
        }
    }
    else if (s3eFBDialog_Error(dialog))
    {
        snprintf(g_StatusMessage, MESSAGE_LEN,
            "`xee6666Stream dialog error %s", s3eFBDialog_ErrorString(dialog));
    }
    else
    {
        const char* url = s3eFBDialog_DidNotCompleteWithUrl(dialog);

        if (url)
        {
            snprintf(g_StatusMessage, MESSAGE_LEN,
                "`xee6666Stream dialog did not complete with url %s", url);
        }
        else
        {
            sprintf(g_StatusMessage, "`xee6666Stream dialog did not complete");
        }
    }

    s3eFBDialog_Delete(dialog);
}

static void FBRequestCallback(struct s3eFBRequest* request, s3eResult* requestResult, void* userData)
{
    IwTrace(FACEBOOK, ("FBRequestCallback %p %p %p", request, requestResult, userData));

    if (*requestResult == S3E_RESULT_SUCCESS)
    {
        if (s3eFBRequest_ResponseType(request) == STRING_TYPE)
        {
            snprintf(g_StatusMessage, MESSAGE_LEN,
                "`x66ee66Status update request succeeded - returned: %s",
                s3eFBRequest_ResponseAsString(request));
        }
        else
        {
            // If this returned data then we could process it
            snprintf(g_StatusMessage, MESSAGE_LEN,
                "`x66ee66Status update request succeeded - return data: %s",
                s3eFBRequest_ResponseRaw(request));
        }
    }
    else
    {
        snprintf(g_StatusMessage, MESSAGE_LEN, "`xee6666Request failed: (%d) %s",
            (int)s3eFBRequest_ErrorCode(request), s3eFBRequest_ErrorString(request));
    }

    // Clear our single global request and allow reposting
    s3eFBRequest_Delete(g_Request);
    g_Request = NULL;

    UpdateState();
}

static void FBRequestCallback2(struct s3eFBRequest* request, s3eResult* requestResult, void* userData)
{
    // callback from graph-based POST request
    IwTrace(FACEBOOK, ("FBRequestCallback2 %p %p %p", request, requestResult, userData));

    if (*requestResult == S3E_RESULT_SUCCESS)
    {
        if (s3eFBRequest_ResponseType(request) == DICTIONARY_TYPE)
        {
            const char* returned_id = s3eFBRequest_ResponseDictionaryItemAsString(request, "id");

            if (returned_id)
            {
                snprintf(g_StatusMessage, MESSAGE_LEN,
                    "`x66ee66Status update request succeeded - returned: id: %s", returned_id);
            }
            else
            {
                snprintf(g_StatusMessage, MESSAGE_LEN,
                    "`xee6666Status update request seemingly failed - return data: %s",
                    s3eFBRequest_ResponseRaw(request));
             }
        }
        else
        {
            // If this returned data then we could process it
            snprintf(g_StatusMessage, MESSAGE_LEN,
                "`xee6666Status update request probably failed (expected id) - return data: %s",
                s3eFBRequest_ResponseRaw(request));
        }
    }
    else
    {
        snprintf(g_StatusMessage, MESSAGE_LEN, "`xee6666Request failed: (%d) %s",
            (int)s3eFBRequest_ErrorCode(request), s3eFBRequest_ErrorString(request));
    }

    // Clear our single global request and allow reposting
    s3eFBRequest_Delete(request);

    UpdateState();
}

static void FBGetUIDCallback(struct s3eFBRequest* request, s3eResult* requestResult, void* userData)
{
    IwTrace(FACEBOOK, ("FBGetUIDCallback %p %p %p", request, requestResult, userData));

    if (*requestResult == S3E_RESULT_SUCCESS)
    {
        strcpy(g_UID, s3eFBRequest_ResponseDictionaryItemAsString(request, "id"));

        sprintf(g_StatusMessage, "`x66ee66Got user Id: %s", g_UID);
    }
    else
    {
        sprintf(g_StatusMessage, "`xee6666Failed to get user Id");
    }

    s3eFBRequest_Delete(g_GetUID);
    g_GetUID = NULL;

    UpdateState();
}

#if 0
// code not currently used - FBReportFieldCallback is more generic
static void FBGetNameCallback2(struct s3eFBRequest* request, s3eResult* requestResult, void* userData)
    // hard-wired callback reporting value from me?field=id,name query
{
    IwTrace(FACEBOOK, ("FBGetUIDCallback2 %p %p %p", request, requestResult, userData));

    if (*requestResult == S3E_RESULT_SUCCESS)
    {
        const char* name = s3eFBRequest_ResponseDictionaryItemAsString(request, "name");

        sprintf(g_StatusMessage, "`x66ee66Got user name: %s", name);
    }
    else
    {
        sprintf(g_StatusMessage, "`xee6666Failed to get user name");
    }

    s3eFBRequest_Delete(request);

    UpdateState();
}
#endif

static void FBReportFieldCallback(struct s3eFBRequest* request, s3eResult* requestResult, void* userData)
    // treat userData as char* of a fieldname to report
{
    IwTrace(FACEBOOK, ("FBReportFieldCallback %p %p %p", request, requestResult, userData));

    char* field = (char*) userData;

    if (*requestResult == S3E_RESULT_SUCCESS)
    {
        const char* value = s3eFBRequest_ResponseDictionaryItemAsString(request, field);

        sprintf(g_StatusMessage, "`x66ee66Got field: %s: %s", field, value);
    }
    else
    {
        sprintf(g_StatusMessage, "`xee6666Can't find field: %s", field);
    }

    s3eFBRequest_Delete(request);

    UpdateState();
}

static void FBGetNameCallback(struct s3eFBRequest* request, s3eResult* requestResult, void* userData)
{
    IwTrace(FACEBOOK, ("FBGetNameCallback %p %p", request, requestResult));

    if (*requestResult == S3E_RESULT_SUCCESS)
    {
        const char* data = s3eFBRequest_ResponseDictionaryItemAsString(request, "data");
        char buffer[200];
        sprintf(buffer, "`xee6666%s", data);
        size_t length = strlen(buffer);
        for (size_t i=0; i<length; i++)
        {
            if (buffer[i] == '\n' || buffer[i] == '\t')
                buffer[i] = ' ';
        }

        sprintf(g_StatusMessage, "%s", buffer);
    }
    else
    {
        sprintf(g_StatusMessage, "`xee6666Failed to get name using FQL");
    }

    s3eFBRequest_Delete(g_GetName);
    g_GetName = NULL;
    UpdateState();
}

static void FBSLComposeViewControllerCallback(struct s3eFBSLComposeViewController* cvc, s3eResult*, void*)
{
    s3eFBSLComposeViewController_Delete(g_SLComposeViewController);
    g_SLComposeViewController = NULL;
}

void ExampleInit()
{
    if (!s3eFacebookAvailable())
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "Facebook extension not found");
        s3eDeviceExit(1);
    }

    if (!FACEBOOK_APP_ID)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "Please supply a valid Facebook "
            "App ID for FACEBOOK_APP_ID "
            "in s3eFacebook.cpp!");
        s3eDeviceExit(1);
    }

    g_read_permissions.push_back("read_stream");
    g_read_permissions.push_back("user_about_me");

    // set up publish permissions
    const char* permissions[] = { "publish_actions" };
    int num_permissions = sizeof(permissions) / sizeof(permissions[0]);
    for (int i=0; i < num_permissions; i++)
    {
        g_publish_permissions.push_back(permissions[i]);
    }

#ifdef USE_IOS_SYSTEM_LOGIN
    g_ButtonLogin                   = NewButton("Log-in (via system)");
#else
    g_ButtonLogin                   = NewButton("Log-in");
#endif
    g_ButtonLogin2                  = NewButton("Log-in (using web)");
#if defined(S3EFACEBOOK_API_VERSION) && S3EFACEBOOK_API_VERSION >= 2
    g_ButtonClearAndClose           = NewButton("Close (and clear)");
    g_ButtonClose                   = NewButton("Close");
#else
    g_ButtonClose                   = NewButton("Log-out");
#endif
    g_ButtonAuthorized              = NewButton("Show permissions");
    g_ButtonReauthorize             = NewButton("Reauthorize with publish permissions");
    g_ButtonStreamDialog            = NewButton("Show stream preview");
#ifdef ENABLE_OLD_BUTTONS
    g_ButtonSendRequest             = NewButton("Send stream request");
#endif
    g_ButtonSendRequest2            = NewButton("Send stream request (using graph)");
    g_ButtonGetUID                  = NewButton("Get user ID");
#ifdef ENABLE_OLD_BUTTONS
    g_ButtonGetName                 = NewButton("Get user name using FQL");
#endif
    g_ButtonGetName2                = NewButton("Get user name (using graph)");
    g_ButtonGetAccessToken          = NewButton("Get Session Access Token");
    g_ButtonSLComposeViewControllerImage = NewButton("Share Image With Native UI (iOS 6+)");
    g_ButtonSLComposeViewControllerURL = NewButton("Share URL With Native UI (iOS 6+)");

    g_SessionStatus = FB_UNINITIALISED;

    UpdateState();
}

void ExampleTerm()
{
    if (g_Request)
    {
        s3eFBRequest_Delete(g_Request);
        g_Request = NULL;
    }

    if (g_GetUID)
    {
        s3eFBRequest_Delete(g_GetUID);
        g_GetUID = NULL;
    }

    if (g_CurrentSession)
    {
        s3eFBTerminate(g_CurrentSession);
        g_CurrentSession = NULL;
    }
}

bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    if (pressed)
    {
        // Note we don't have to check that the buttons have been created (non-NULL) as we don't
        // enter this bit of code if pressed is NULL, and so won't get such false matches

        // Initialise and log-in
        if (pressed == g_ButtonLogin || pressed == g_ButtonLogin2)
        {
            // Initialise a new session with the API key and secret
            if (!g_CurrentSession)
                g_CurrentSession = s3eFBInit(FACEBOOK_APP_ID, FACEBOOK_URL_SUFFIX);

            if (g_CurrentSession)
            {
                sprintf(g_StatusMessage, "`x66ee66Session initialised, logging-in...");

                std::vector<const char*>& permissions = g_read_permissions;

                s3eFBSessionLoginBehaviour loginBehaviour;

                if (pressed == g_ButtonLogin)
                {
#ifdef USE_IOS_SYSTEM_LOGIN
                    loginBehaviour = s3eFBSessionBehaviourUseSystemAccountIfPresent; // will be equiv to s3eFBSessionBehaviourWithFallbackToWebView on android
#else
                    loginBehaviour = s3eFBSessionBehaviourWithFallbackToWebView;
#endif
                }
                else
                {
                    // g_ButtonLogin2
#if defined(S3EFACEBOOK_API_VERSION) && S3EFACEBOOK_API_VERSION >= 2
                    loginBehaviour = g_ForceWebNextTime ? s3eFBSessionBehaviourForcingWebView : s3eFBSessionBehaviourUseWebView;
#ifndef DONT_FORCE_LOGIN
                    g_ForceWebNextTime = ! g_ForceWebNextTime; // toggle flag - just really for testing
#endif
#else
                    loginBehaviour = s3eFBSessionBehaviourForcingWebView;
#endif
                    (void) g_ForceWebNextTime; // pseudo "use" to keep compiler from giving warning
                }

                // User should login with read permissions only
                // Publish permissions should be requested separately
                // with s3eFBSession_ReauthorizeWithPublishPermissions.
                // Log in to Facebook using the session.
                s3eFBSession_Login(g_CurrentSession, FBLoginCallback, NULL,
                    &permissions[0], (int) permissions.size(),
                    loginBehaviour);

                g_SessionStatus = FB_LOGGING_IN;

                UpdateState();
            }
            else
            {
                sprintf(g_StatusMessage, "`xee6666Failed to initialise session");
                g_SessionStatus = FB_UNINITIALISED;
            }
        }

        else if (pressed == g_ButtonClose || pressed == g_ButtonClearAndClose)
        {
#if defined(S3EFACEBOOK_API_VERSION) && S3EFACEBOOK_API_VERSION >= 2
            // Close session
            s3eFBSessionCloseBehaviour closeBehaviour =
                pressed == g_ButtonClearAndClose ? s3eFBSessionCloseBehaviourClearToken
                                                 : s3eFBSessionCloseBehaviourSimple;
            s3eFBSession_Close(g_CurrentSession, closeBehaviour);
#else
            s3eFBSession_Logout(g_CurrentSession);
#endif
            g_SessionStatus = FB_LOGGED_OUT;
            sprintf(g_StatusMessage, "`x6666eeSession terminated");

            UpdateState();
        }

        else if (pressed == g_ButtonStreamDialog)
        {
            // Construct dialog to allow user to post to their wall
            s3eFBDialog* pDialog = s3eFBDialog_WithAction(g_CurrentSession, "stream.publish");

            // Show the dialog
            s3eFBDialog_Show(pDialog, FBStreamDialogCallback);
        }

        else if (pressed == g_ButtonSendRequest)
        {
            // Construct simple request request and send it to Facebook.
            // We are using the method to publish data to a users stream.

            // If a short message is passed to "stream.publish" with no user
            // uid specified, it will appear as the current (logged-in) user's
            // Facebook status. This will fail if permission hasn't been
            // granted yet.
            g_Request = s3eFBRequest_WithMethodName(g_CurrentSession, "stream.publish", "POST");

            if (!g_Request)
            {
                sprintf(g_StatusMessage, "`xee6666Failed to create request");
            }
            else
            {
                // Construct a message which always changes
                int64 timeUTC = s3eTimerGetUTC() / 1000;
                char message[0x100];
                sprintf(message, "has the time since the epoch as: %lld", timeUTC);

                // Add a value to the request
                s3eFBRequest_AddParamString(g_Request, "message", message);

                // Send the request
                s3eResult result = s3eFBRequest_Send(g_Request, FBRequestCallback);

                // Handle result
                if (result == S3E_RESULT_SUCCESS)
                {
                    UpdateState();
                }
                else
                {
                    sprintf(g_StatusMessage, "`xee6666Failed to send request");
                    s3eFBRequest_Delete(g_Request);
                    g_Request = NULL;
                }
            }
        }

        else if (pressed == g_ButtonSendRequest2)
        {
            // Construct simple request request and send it to Facebook.
            // Write to current user, using looked up value rather than "me" to show wider example

            if (strlen(g_UID) == 0)
            {
                sprintf(g_StatusMessage, "`xee6666First get user id");
            }
            else
            {
                char temp[128];
                sprintf(temp, "%s/feed", g_UID); // could use "me/feed" for just this usecase
                s3eFBRequest* request = s3eFBRequest_WithGraphPath(g_CurrentSession, temp, "POST");

                if (!request)
                {
                    sprintf(g_StatusMessage, "`xee6666Failed to create request");
                }
                else
                {
                    // Construct a message which always changes
                    int64 timeUTC = s3eTimerGetUTC() / 1000;
                    char message[0x100];
                    sprintf(message, "has the time since the epoch as: %lld", timeUTC);

                    // Add a value to the request
                    s3eFBRequest_AddParamString(request, "message", message);

                    // Send the request
                    s3eResult result = s3eFBRequest_Send(request, FBRequestCallback2);

                    // Handle result
                    if (result == S3E_RESULT_SUCCESS)
                    {
                        UpdateState();
                    }
                    else
                    {
                        sprintf(g_StatusMessage, "`xee6666Failed to send request");
                        s3eFBRequest_Delete(request);
                    }
                }
            }
        }

        else if (pressed == g_ButtonGetUID)
        {
            // Construct graph request to retrieve users ID.
            g_GetUID = s3eFBRequest_WithGraphPath(g_CurrentSession, "me", "GET");

            // Send the request
            s3eResult result = s3eFBRequest_Send(g_GetUID, FBGetUIDCallback);

            // Handle result
            if (result == S3E_RESULT_SUCCESS)
            {
                UpdateState();
            }
            else
            {
                sprintf(g_StatusMessage, "`xee6666Failed to send request");
                s3eFBRequest_Delete(g_GetUID);
                g_GetUID = NULL;
            }
        }
        else if (pressed == g_ButtonGetName)
        {
            // Set up FQL using graph api
            s3eFBRequest* fqlRequest = s3eFBRequest_WithGraphPath(g_CurrentSession, "/fql", "GET");

            // Set up FQL query as key/value parameter
            s3eFBRequest_AddParamString(fqlRequest, "q", "SELECT name FROM user WHERE uid = me()");

            // Send the request
            s3eResult result = s3eFBRequest_Send(fqlRequest, FBGetNameCallback);

            // Handle result
            if (result == S3E_RESULT_SUCCESS)
            {
                UpdateState();
            }
            else
            {
                sprintf(g_StatusMessage, "`xee6666FQL request failed");
            }
        }

        else if (pressed == g_ButtonGetName2)
        {
            // Set up graph api
            s3eFBRequest* request = s3eFBRequest_WithGraphPath(g_CurrentSession, "me", "GET");

            // Add the fields to report - we only actually need "name" but this shows how to request several
            s3eFBRequest_AddParamString(request, "fields", "id,name");

            // Send the request
            s3eResult result = s3eFBRequest_Send(request, FBReportFieldCallback, (void*)"name");

            // Handle result
            if (result == S3E_RESULT_SUCCESS)
            {
                UpdateState();
            }
            else
            {
                sprintf(g_StatusMessage, "`xee6666FQL request failed");
                s3eFBRequest_Delete(request);
            }
        }

        else if (pressed == g_ButtonGetAccessToken)
        {
            const char* token = s3eFBSession_AccessToken(g_CurrentSession);
            if (token)
                snprintf(g_StatusMessage, MESSAGE_LEN, "`x66ee66Access Token: %s", token);
            else
                sprintf(g_StatusMessage, "`xee6666Could not retrieve Access Token");
        }

        else if (pressed == g_ButtonAuthorized)
        {
            const char*const *const permissions = s3eFBSession_GetPermissions(g_CurrentSession);
            char temp[256];
            strcpy(temp, "");
            (void)permissions;

            bool first = true;
            for (const char*const*ptr = permissions; *ptr !=  NULL; ptr++)
            {
                if (!first)
                    strncat(temp, ",", 256);
                strncat(temp, *ptr, 256);
                first = false;
            }
            sprintf(g_StatusMessage, "`xee6666Permissions: %s", temp);
        }

        else if (pressed == g_ButtonReauthorize)
        {
            g_SessionStatus = FB_REQUESTING_PUBLISH_PERMISSIONS;
            sprintf(g_StatusMessage, "`x66ee66Facebook Reathorisation...");
            s3eFBSession_ReauthorizeWithPublishPermissions(
                g_CurrentSession,
                &g_publish_permissions[0],
                (int) g_publish_permissions.size(),
                s3eFBSessionDefaultAudienceEveryone, FBReauthorizeWithPublishPermissionsCallback);
        }

        else if (pressed == g_ButtonSLComposeViewControllerURL)

        {
            if (s3eFBSLComposeViewController_IsAvailable() && g_SLComposeViewController == NULL)
            {
                g_SLComposeViewController = s3eFBSLComposeViewController_Create();

                s3eFBSLComposeViewController_AddURL(g_SLComposeViewController, "http://www.madewithmarmalade.com");
                s3eFBSLComposeViewController_SetInitialText(g_SLComposeViewController, "Visit Marmalade Web Site");

                s3eFBSLComposeViewController_Show(g_SLComposeViewController, FBSLComposeViewControllerCallback);
            }
        }

        else if (pressed == g_ButtonSLComposeViewControllerImage)

        {
            if (s3eFBSLComposeViewController_IsAvailable() && g_SLComposeViewController == NULL)
            {
                g_SLComposeViewController = s3eFBSLComposeViewController_Create();

                const int w = 256;
                const int h = 256;
                const int bpp = 4;
                uint8_t* imageData = new uint8_t[w * h * bpp];
                for (int i = 0; i < w; i++)
                {
                    for (int j =0 ; j  < h; j++) {
                        imageData[(i + j * w) * bpp + 0] = (i + j) % 0xff;
                        imageData[(i + j * w) * bpp + 1] = (i + j * 10) % 0xff;
                        imageData[(i + j * w) * bpp + 2] = (i * 10 + j) % 0xff;
                        imageData[(i + j * w) * bpp + 3] = 0xff;
                    }
                }

                s3eFBSLComposeViewController_AddImageWithData(g_SLComposeViewController, imageData, w, h);
                delete[] imageData;

                s3eFBSLComposeViewController_SetInitialText(g_SLComposeViewController, "Image Sharing Example");

                s3eFBSLComposeViewController_Show(g_SLComposeViewController, FBSLComposeViewControllerCallback);
            }
        }
    }

    return true;
}

void ExampleRender()
{
    int y = GetYBelowButtons();
    int x = 20;

    s3eDebugPrintf(x, y, 1, "%s", g_StatusMessage);
}
