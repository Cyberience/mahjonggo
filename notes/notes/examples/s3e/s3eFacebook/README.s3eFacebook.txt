#category: Network
s3eFacebook
===========

Marmalade provides the s3eFacebook extension API that makes it simple to add
Facebook connectivity to an app.  The s3eFacebook example shows how to log in
to the Facebook servers and then make requests to the Facebook API such as
posting a message to the user's wall.  Note that the s3eFacebook API is
currently supported on Android, iOS, Windows Phone and Windows Store platforms.

