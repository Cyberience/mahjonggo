/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

#include "ExamplesMain.h"
#include "s3eDialog.h"

/**
 *
 * @page ExampleS3EDialog S3E Dialog Example
 *
 * The following example uses the S3E Dialog functionality to display
 * a model alert dialog box.
 *
 * The functions used to achieve this are:
 * - s3eDialogAvailable()
 * - s3eDialogAlertBox()
 *
 * @include s3eDialog.cpp
 */

static Button* g_ButtonOneButton = 0;
static Button* g_ButtonTwoButton = 0;
static Button* g_ButtonThreeButton = 0;
static int g_ButtonReturn = -1;

int32 DialogCallback(s3eDialogCallbackData* data, void* userData)
{
    g_ButtonReturn = data->m_ButtonPressed;
    return 0;
}

void ExampleInit()
{
    // Create the UI.
    g_ButtonOneButton = NewButton("One Button Alert Dialog");
    g_ButtonTwoButton  = NewButton("Two Button Alert Dialog");
    g_ButtonThreeButton  = NewButton("Three Button Alert Dialog");

    // Check for the availability of the Web View functionality.
    // If it is available then enable the button.
    bool avail = (s3eDialogAvailable() != 0);
    g_ButtonOneButton->m_Enabled = avail;
    g_ButtonTwoButton->m_Enabled = avail;
    g_ButtonThreeButton->m_Enabled = avail;

    s3eDialogRegister(S3E_DIALOG_FINISHED, (s3eCallback)DialogCallback, NULL);
}

void ExampleTerm()
{
    s3eDialogUnRegister(S3E_DIALOG_FINISHED, (s3eCallback)DialogCallback);
}

bool ExampleUpdate()
{
    // If the button has been selected, try to execute the OS command
    // and store the result.
    if (GetSelectedButton() == g_ButtonOneButton)
    {
        s3eDialogAlertInfo info;
        info.m_Message = "Click this button to continue...";
        info.m_Title = "One Button Alert Dialog";
        info.m_Button[0] = "Click Me!";

        s3eDialogAlertBox(&info);
    }
    else if (GetSelectedButton() == g_ButtonTwoButton)
    {
        s3eDialogAlertInfo info;
        info.m_Message = "Click this button to continue...";
        info.m_Title = "Two Button Alert Dialog";
        info.m_Button[0] = "Click Me!";
        info.m_Button[1] = "Or Me!";

        s3eDialogAlertBox(&info);
    }
    else if (GetSelectedButton() == g_ButtonThreeButton)
    {
        s3eDialogAlertInfo info;
        info.m_Message = "Click this button to continue...";
        info.m_Title = "Three Button Alert Dialog";
        info.m_Button[0] = "Click Me!";
        info.m_Button[1] = "Or Me!";
        info.m_Button[2] = "Or Even Me!";

        s3eDialogAlertBox(&info);
    }
    return true;
}

/*
 * The following function display the user's prompt and any error messages.
 * The s3eDebugPrint() function to print the strings.
 */
void ExampleRender()
{
    // Print results
    if (!s3eDialogAvailable())
    {
        s3eDebugPrint(50, GetYBelowButtons(), "`xff0000Dialog isn't available on this device.", 1);
    }

    char buffer[256];
    sprintf(buffer, "`xff0000Dialog returned %d.", g_ButtonReturn);
    s3eDebugPrint(50, GetYBelowButtons() + 10, buffer, 1);
}
