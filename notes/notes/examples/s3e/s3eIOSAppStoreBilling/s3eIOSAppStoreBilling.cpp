/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EIOSAppStoreBilling S3E iOS App Store Billing Example
 *
 * The following example demonstrates the use of the s3eIOSAppStoreBilling API
 * to perform in-app purchasing. It connects to the relevant app store, requests
 * information for a product, allows the user to purchase the product and then
 * displays the receipt for the transaction.
 *
 * This example and the S3E Micro-Transaction API it uses are currently only
 * supported on iPhone.
 *
 * Note that the API only handles purchasing of the product, not delivery of the
 * actual service/data that was paid for. It also does not provide a means for
 * querying and listing available products for the app (since the app store and
 * iPhone SDK do not provide this functionality). Typically, an application will
 * require it's own external server to perform these extra tasks. The
 * s3ePaymentTransaction and s3eTransactionReceipt data returned when a purchase is
 * completed should be sent to the application's server to provide the information
 * it requires to check the validity of the purchase and provide any required data
 * to the app. Providing data via a server is beyond the scope of this example.
 * See S3E Micro-Transaction Overview in the Marmalade API Reference for
 * more information.
 *
 * The main functions demonstrated in this example are:
 * - s3eIOSAppStoreBillingStart()
 * - s3eIOSAppStoreBillingGetInt()
 * - s3eIOSAppStoreBillingRequestProductInformation()
 * - s3eIOSAppStoreBillingCancelProductInformationRequests()
 * - s3eIOSAppStoreBillingRequestPayment()
 * - s3eIOSAppStoreBillingRestoreCompletedTransactions()
 * - s3eIOSAppStoreBillingCompleteTransaction()
 * - s3eIOSAppStoreBillingTerminate()
 *
 * The example uses s3eIOSAppStoreBillingStart to register callbacks to receive
 * product and transaction information. It then uses
 * s3eIOSAppStoreBillingRequestProductInformation to request information about available
 * products. Finally it uses s3eIOSAppStoreBillingRequestPayment to purchase a product.
 * Information about the product and transaction is sent to the relevant callbacks
 * once the product is found or the state of the transaction changes. Any errors that
 * occur are also reported via these callbacks.
 *
 * The example always "retains" the a purchased transaction supplied on TransactionUpdateCallback,
 * and completes using s3eIOSAppStoreBillingRestoreCompletedTransactions(). If
 * S3E_IOSAPPSTOREBILLING_WAITFORCOMPLETION is defined (see mkb) it will wait a while
 * before making that call - allowing the user to test scenarios where the app exists
 * in this period and thus continues on restart. (The callback will be made again.)
 *
 * To run this example and connect to the app store, you need to have registered an
 * iTunes Connect account and performed the following steps through iTunes Connect:
 * following steps:
 * - Add an application to your account to use for this example app.
 * - There is no need to upload a binary for this app but the pricing tier must be
 *   greater than 0.
 * - Register a bundle ID for the application.
 * - Add an In App Purchase to the application on iTunes Connect.
 * - Apple recommend that the In App purchase must has a Product ID of the format
 *   "[bundle_id].[product_id]", where bundle_id matches the one registered in step
 *   3 above, although this is not enforced.
 * - Add an In App Purchase Test User to test making payment requests.
 *
 * See S3E Micro-Transaction Overview in the Marmalade API Reference for
 * a detailed guide to setting up your iTunes Connect account to test micro-transactions.
 *
 * The following changes must then be made to this example:
 * - Replace the the dummy product ID string in the code (g_ProductID =
 *   "productid") with the one you registered with the store.
 * - Replace the dummy app and bundle IDs in the MKB (iphone-appid=
 *   "com.storeaccount.appid" etc) with the one you registered with the store.
 *
 * @include s3eIOSAppStoreBilling.cpp
 */

#include "ExamplesMain.h"
#include "s3eIOSAppStoreBilling.h"
#include "s3eDebug.h"

// Unique identifier for the product id to purchase.
const char* g_ProductID = "productid";

// Input product identifying request and output purchase complete receipt
static s3ePaymentRequest g_PaymentRequest;
static s3eTransactionReceipt g_TransactionReceipt;
static s3ePaymentTransaction* g_CompletedPayment = NULL;

// Product information sent back from s3eIOSAppStoreBillingStart
static s3eProductInformation* g_ProductInformation = NULL;
static s3ePaymentTransaction* g_PaymentTransaction = NULL;

// Buttons for getting product information and buying
static Button* g_ProductInfoButton = NULL;
static Button* g_ProductInfoStopButton = NULL;
static Button* g_ProductBuyButton = NULL;
static Button* g_ProductBuyImmediatelyButton = NULL;
static Button* g_TransactionReceiptButton = NULL;
static Button* g_TransactionRestoreButton = NULL;

// Status string to print at bottom of screen
static char g_StatusStr[8192]; // Can be quite a long string!

static int countdown;
#ifdef S3E_IOSAPPSTOREBILLING_WAITFORCOMPLETION
static const int kCountdownFrom = 500;
#else
static const int kCountdownFrom = 0; // 0 will equate to do immediately
#endif

void ResetButtons()
{
    DeleteButtons();
    g_ProductInfoButton = NULL;
    g_ProductInfoStopButton = NULL;
    g_ProductBuyButton = NULL;
    g_TransactionReceiptButton = NULL;
    g_TransactionRestoreButton = NULL;
    g_ProductBuyImmediatelyButton = NULL;
}

void SetButtonsToInitialMenu()
{
    ResetButtons();
    g_ProductInfoButton = NewButton("Request product info");
    g_TransactionRestoreButton = NewButton("Restore purchased products");
}

// Sets a string to be displayed beneath the buttons
static void SetStatus(const char* statusStr, ...)
{
    va_list args;
    va_start(args, statusStr);
    strcpy(g_StatusStr, "`x000000");
    vsprintf(g_StatusStr+strlen(g_StatusStr), statusStr, args);
    va_end(args);
}

// Appends to the string to be displayed beneath the buttons
static void AddStatus(const char* statusStr, ...)
{
    va_list args;
    va_start(args, statusStr);
    int currentLen = strlen(g_StatusStr);
    g_StatusStr[currentLen] = '\n';
    vsprintf(g_StatusStr + currentLen + 1, statusStr, args);
    va_end(args);
}

/*
 * Callback functions
 *
 * This callback is called after the call to s3eIOSAppStoreBillingRequestPayment when
 * the state of a transaction has changed (i.e. an item as been successfully bought,
 * or an error occurred)
 */
void TransactionUpdateCallback(s3ePaymentTransaction* transaction, void* userData)
{
    s3eDebugTracePrintf("! TransactionUpdateCallback");

    switch (transaction->m_TransactionStatus)
    {
        case S3E_PAYMENT_STATUS_PENDING:
            s3eDebugTracePrintf("Buying %s (transaction '%s') in progress...", transaction->m_Request->m_ProductID, transaction->m_TransactionID);
            SetStatus("Buying %s in progress...", transaction->m_Request->m_ProductID);
            break;

        case S3E_PAYMENT_STATUS_PURCHASED:
        case S3E_PAYMENT_STATUS_RESTORED:
            if (transaction->m_TransactionStatus == S3E_PAYMENT_STATUS_PURCHASED)
                s3eDebugTracePrintf("S3E_PAYMENT_STATUS_PURCHASED");
            else if (transaction->m_TransactionStatus == S3E_PAYMENT_STATUS_RESTORED)
                s3eDebugTracePrintf("S3E_PAYMENT_STATUS_RESTORED");

            s3eDebugTracePrintf("g_PaymentRequest.m_ProductID %s", g_PaymentRequest.m_ProductID);
            s3eDebugTracePrintf("g_PaymentTransaction->m_Request->m_ProductID %s", transaction->m_Request->m_ProductID);

            // Check product ID of completed transaction
            if (!strcmp(transaction->m_Request->m_ProductID, g_PaymentRequest.m_ProductID))
            {
                if (g_TransactionReceipt.m_ReceiptData)
                {
                    s3eFree(g_TransactionReceipt.m_ReceiptData);
                    g_TransactionReceipt.m_ReceiptData = NULL;
                }

                if (g_PaymentTransaction)
                {
                    s3eFree(g_PaymentTransaction);
                    g_PaymentTransaction = NULL;
                }

                s3eDebugTracePrintf("product ids match");

                // Retain the transaction so it can be verified and finalised
                // later. In this simple example, we only track 1 at a time and
                // just complete it on the next update callback.
                if (!g_CompletedPayment)
                {
                    transaction->m_Retain = S3E_TRUE;
                    countdown = kCountdownFrom;
                    g_CompletedPayment = transaction;
                }

                if (transaction->m_TransactionStatus == S3E_PAYMENT_STATUS_PURCHASED)
                    AddStatus("Buying %s (transaction '%s')", transaction->m_Request->m_ProductID, transaction->m_TransactionID);
                else
                    AddStatus("Restoring %s (new ID '%s', original ID '%s')!", transaction->m_Request->m_ProductID, transaction->m_TransactionID, transaction->m_OriginalTransactionID);

                // Copy transaction to local memory
                g_PaymentTransaction = (s3ePaymentTransaction*)s3eMalloc(sizeof(s3ePaymentTransaction));
                memcpy(g_PaymentTransaction, transaction, sizeof(s3ePaymentTransaction));

                // Point to local version of payment request identifier
                g_PaymentTransaction->m_Request = &g_PaymentRequest;

                // Copy receipt data to memory
                g_TransactionReceipt.m_ReceiptSize = transaction->m_TransactionReceipt->m_ReceiptSize;
                g_TransactionReceipt.m_ReceiptData = s3eMalloc(g_TransactionReceipt.m_ReceiptSize+1);
                memcpy(g_TransactionReceipt.m_ReceiptData, transaction->m_TransactionReceipt->m_ReceiptData, g_TransactionReceipt.m_ReceiptSize);
                g_PaymentTransaction->m_TransactionReceipt = &g_TransactionReceipt;

                // Make sure it's null terminated for convenient printing in this example
                char* endOfData = (char*)g_TransactionReceipt.m_ReceiptData + g_TransactionReceipt.m_ReceiptSize;
                *endOfData = '\0';

                SetButtonsToInitialMenu();
                g_TransactionReceiptButton = NewButton("Display receipt");

                // g_PaymentTransaction is now a pointer to a local copy of the complete transaction information
                break;
            }
            else
            {
                // The example only has one product, but we can get here if the example is run with one productID,
                // the product is bought, the example is rebuilt with another productID, that product is bougth and
                // then the products are restored.  In this situation both products will be restored.  A production
                // quality app would obviously be able to handle such situations.

                s3eDebugTracePrintf("Callback fired for product from previous build of application.  ProductID = %s", transaction->m_Request->m_ProductID);

                // The purchase of a different item should only happen if a purchase was requested and then the user
                // quits the app before the Apple server callback fires.  It's unlikely this will happen.
                if (transaction->m_TransactionStatus == S3E_PAYMENT_STATUS_PURCHASED)
                    AddStatus("Buying completed for unexpected product ID: %s.  It's possible that a product was "
                              "purchased with a previous build of the application but the application was quit "
                              "before the purchase could be processed!", transaction->m_Request->m_ProductID);
                else
                    AddStatus("Restoring completed for product ID from previous build of application: %s!", transaction->m_Request->m_ProductID);
            }
            break;

        case S3E_PAYMENT_STATUS_FAILED_CLIENT_INVALID:
            SetStatus("Buying %s FAILED. Client is not allowed to make the payment.", transaction->m_Request->m_ProductID);
            break;

        case S3E_PAYMENT_STATUS_FAILED_PAYMENT_CANCELLED:
            SetStatus("Buying %s FAILED. User cancelled the purchase.", transaction->m_Request->m_ProductID);
            break;

        case S3E_PAYMENT_STATUS_FAILED_PAYMENT_INVALID:
            SetStatus("Buying %s FAILED. Invalid parameter/purchase ID.", transaction->m_Request->m_ProductID);
            break;

        case S3E_PAYMENT_STATUS_FAILED_PAYMENT_NOT_ALLOWED:
            SetStatus("Buying %s FAILED. Device is not allowed to make the payment (check restriction settings).", transaction->m_Request->m_ProductID);
            break;

        case S3E_PAYMENT_STATUS_DEFERRED:
            SetStatus("Buying %s was deferred as this account is child account", transaction->m_Request->m_ProductID);
            break;

        default:
            SetStatus("Buying %s FAILED for unknown reason...", transaction->m_Request->m_ProductID);
            break;
    }
}

// This callback is called after the call to s3eIOSAppStoreBillingRequestProductInformation once
// product information is returned from the store.
void ProductInfoCallback(s3eProductInformation* productInfo, void* userData)
{
    s3eDebugTracePrintf("! ProductInfoCallback %p", productInfo);

    // Free info from any previous request
    if (g_ProductInformation)
    {
        s3eFree(g_ProductInformation);
        g_ProductInformation = NULL;
    }

    // Copy productInfo since it will no longer be valid once this function returns
    g_ProductInformation = (s3eProductInformation*)s3eMalloc(sizeof(s3eProductInformation));
    memcpy(g_ProductInformation, productInfo, sizeof(s3eProductInformation));

    s3eDebugTracePrintf("m_ProductID=%s, m_LocalisedTitle=%s", g_ProductInformation->m_ProductID, g_ProductInformation->m_LocalisedTitle);

    switch (g_ProductInformation->m_ProductStoreStatus)
    {
        case S3E_PRODUCT_STORE_STATUS_VALID:
        {
            if (s3eIOSAppStoreBillingGetInt(S3E_IOSAPPSTOREBILLING_CAN_MAKE_PAYMENTS))
                SetStatus("Got Product information for '%s': '%s', price '%s' (%f), locale '%s'. Press button to buy...", g_ProductInformation->m_LocalisedTitle, g_ProductInformation->m_LocalisedDescription, g_ProductInformation->m_FormattedPrice, (float)g_ProductInformation->m_Price * 0.01, g_ProductInformation->m_PriceLocale);
            else
                SetStatus("Got Product information for '%s': '%s', price '%s' (%f), locale '%s'. Cannot buy: Purchasing is disabled in device's Settings menu!", g_ProductInformation->m_LocalisedTitle, g_ProductInformation->m_LocalisedDescription, g_ProductInformation->m_FormattedPrice, (float)g_ProductInformation->m_Price * 0.01, g_ProductInformation->m_PriceLocale);
            ResetButtons();
            g_ProductInfoButton = NewButton("Re-check product info");

            if (s3eIOSAppStoreBillingGetInt(S3E_IOSAPPSTOREBILLING_CAN_MAKE_PAYMENTS))
            {
                char str[256];
                sprintf(str, "Buy ('%s')", g_ProductInformation->m_LocalisedTitle);
                g_ProductBuyButton = NewButton(str);
            }
            break;
        }
        case S3E_PRODUCT_STORE_STATUS_NO_CONNECTION:
            SetStatus("Could not connect to store! Ongoing requests have been cancelled. Please check connection.");
            SetButtonsToInitialMenu();
            break;

        case S3E_PRODUCT_STORE_STATUS_RESTORE_FAILED:
            SetStatus("Restore products failed! Please try again.");
            break;

        case S3E_PRODUCT_STORE_STATUS_RESTORE_COMPLETED:
            AddStatus("All products restored");
            break;

        case S3E_PRODUCT_STORE_STATUS_NOT_FOUND:
        default:
            SetStatus("Product not found in store: %s!", g_ProductInformation->m_ProductID);
            SetButtonsToInitialMenu();
    }

    s3eDebugTracePrintf("leaving ProductInfoCallback");
}

void ExampleInit()
{
    if (!s3eIOSAppStoreBillingAvailable())
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "This example only works on iPhone with a test store set up for in-app purchase");
        s3eDeviceExit(1);
    }

    // Set up product request
    strcpy(g_PaymentRequest.m_ProductID, g_ProductID);
    g_PaymentRequest.m_Quantity = 1;

    // Initialise the Micro-Transaction module and register for callbacks
    // when product & transaction information is available. Note that Micro-
    // Transaction has explicit Init & Terminate functions unlike most S3E
    // subdevices or EDK extensions.
    s3eIOSAppStoreBillingStart(ProductInfoCallback, TransactionUpdateCallback, NULL);

    //Add button for requesting product information and restoring already purchased products
    SetButtonsToInitialMenu();

    SetStatus("Waiting for user input to get product info for: %s", g_ProductID);

#ifdef S3E_IOSAPPSTOREBILLING_ENABLEQUICKBUY
    g_ProductBuyImmediatelyButton = NewButton("Quick buy!");
#endif
}

void ExampleTerm()
{
    if (g_ProductInformation)
        s3eFree(g_ProductInformation);

    if (g_TransactionReceipt.m_ReceiptData)
        s3eFree(g_TransactionReceipt.m_ReceiptData);

    if (g_PaymentTransaction)
        s3eFree(g_PaymentTransaction);

    s3eIOSAppStoreBillingTerminate();
}

bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    if (g_CompletedPayment && --countdown <= 0)
    {
        // In a real app, the receipt would be checked and a server might provide
        // data before this function is called
        s3eIOSAppStoreBillingCompleteTransaction(g_CompletedPayment, S3E_TRUE);
        g_CompletedPayment = NULL;
        AddStatus("Purchase Completed");
    }

    if (pressed)
    {
        if (g_ProductInfoButton == pressed)
        {
            // When the user presses the 'get product info' button, request product information
            // and set the status string accordingly.
            // s3eIOSAppStoreBillingRequestPayment will cause the ProductInfoCallback to be
            // called, which then fills in the g_ProductInformation structure
            if (s3eIOSAppStoreBillingRequestProductInformation(&g_ProductID, 1) == S3E_RESULT_SUCCESS)
            {
                SetStatus("Requesting product info for: %s...", g_ProductID);
                g_ProductInfoStopButton = NewButton("Cancel product request");
            }
            else
            {
                SetStatus("Requesting product info for %s FAILED", g_ProductID);
            }
        }
        else if (g_ProductInfoStopButton == pressed)
        {
            s3eIOSAppStoreBillingCancelProductInformationRequests();
            SetButtonsToInitialMenu();
            SetStatus("Product info request cancelled");
        }
        else if (g_ProductBuyButton == pressed || g_ProductBuyImmediatelyButton == pressed)
        {
            // Once the user has product information he/she can now buy the product.
            // Create a transaction request.
            // A new s3ePaymentRequest object is returned which the app should free when done with.
            // When the transaction has completed, the TransactionUpdateCallback will be called.

            if (s3eIOSAppStoreBillingRequestPayment(&g_PaymentRequest) == S3E_RESULT_SUCCESS)
                SetStatus("Purchasing %s...", g_ProductID);
            else
                SetStatus("Purchasing %s FAILED", g_ProductID);
        }
        else if (g_TransactionReceiptButton == pressed)
        {
            // Print receipt contents to the screen
            SetStatus("Receipt: %s", (char*)g_TransactionReceipt.m_ReceiptData);
        }
        else if (g_TransactionRestoreButton == pressed)
        {
            s3eIOSAppStoreBillingRestoreCompletedTransactions();
            SetStatus("Restoring products that were already purchased...");
        }
    }

    return true;
}

void ExampleRender()
{
    //Print status string just below the buttons
    s3eDebugPrint(0, GetYBelowButtons(), g_StatusStr, S3E_TRUE);
}
