/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
#include "ExamplesMain.h"
#include "s3ePermissions.h"

/**
 * @page ExampleS3EPermissions S3E Permissions Example
 *
 * The following demonstrates use of s3ePermissions APIs to request
 * and check camera and read external storage permissions.
 *
 * On android permission request dialog will be shown only on
 * Android 6.0+.
 *
 * For more information see s3ePermissions extension.
 *
 * Source:
 * @include s3ePermissions.cpp
 */

static Button* g_ButtonRequestPermission = 0;
static Button* g_ButtonCheckPermission = 0;
static Button* g_ButtonShouldShowPermissionRequestRationale = 0;

/*
 * Permission request result handler
 */
static int32 OnPermissionsRequestResult(void* systemData, void* userData)
{
    s3ePermissionsRequestResult* res = (s3ePermissionsRequestResult*)systemData;
    const int msgSize = 256;
    char msg[msgSize];
    snprintf(msg, msgSize, "Received permissions request result with request code: %i.", res->requestCode);
    AppendMessageColour(BLACK, msg);
    for (int i = 0; i < res->numPermissions; ++i)
    {
        snprintf(msg, msgSize, "Permission %s was %s", res->permissions[i], res->grantResults[i] == S3E_PERMISSION_GRANTED ? "granted" : "denied");
        AppendMessageColour(res->grantResults[i] == S3E_PERMISSION_GRANTED ? GREEN : RED, msg);
    }
    return 0;
}

void ExampleInit()
{
    InitMessages(10, 128);

    if (!s3ePermissionsAvailable())
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "s3ePermissions extension is not found");
        s3eDeviceExit(1);
    }

    g_ButtonRequestPermission = NewButton("Request Permission");
    g_ButtonCheckPermission   = NewButton("Check Permission");
    g_ButtonShouldShowPermissionRequestRationale = NewButton("Should Show Permission Request Rationale");

    s3ePermissionsRegister(S3E_PERMISSIONS_REQUEST_RESULT, (s3eCallback)OnPermissionsRequestResult, NULL);
}

/*
 * The following function frees memory that was alocated during the example.
 */
void ExampleTerm()
{
    s3ePermissionsUnRegister(S3E_PERMISSIONS_REQUEST_RESULT, (s3eCallback)OnPermissionsRequestResult);
}

/*
 * If a button has been pressed then start/pause/resume/stop playback.
 */
bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    if (pressed == g_ButtonRequestPermission)
    {
        // Request camera and storage permissions.
        // When running on Android 6.0+ system will show dialog to let user grant/deny permission.
        // On pre Android 6.0 all permissions defined in manifest will be granted automatically.
        const char* permissions[2];
        permissions[0] = s3ePermissionsGetAndroidStringById(S3E_PERMISSION_CAMERA);
        permissions[1] = s3ePermissionsGetAndroidStringById(S3E_PERMISSION_READ_EXTERNAL_STORAGE);
        s3ePermissionsRequest(permissions, 2, 1000);
    }

    if (pressed == g_ButtonCheckPermission)
    {
        if (s3ePermissionsIsGranted(s3ePermissionsGetAndroidStringById(S3E_PERMISSION_CAMERA)))
        {
            AppendMessageColour(GREEN, "Camera permission is granted.");
        }
        else
        {
            AppendMessageColour(RED, "App does not have Camera permission.");
        }
    }

    if (pressed == g_ButtonShouldShowPermissionRequestRationale)
    {
        if (s3ePermissionsShouldShowRequestPermissionRationale(s3ePermissionsGetAndroidStringById(S3E_PERMISSION_CAMERA)))
        {
            AppendMessageColour(BLACK, "It is time to show CAMERA permission request rationale.");
        }
        else
        {
            AppendMessageColour(BLACK, "CAMERA permission request rationale should not be shown now.");
        }
    }

    return true;
}

/*
 * The following function outputs a set of strings. A switch is used to
 * output a string indicating the status. Each string is output using the
 * s3eDebugPrint() function.
 */
void ExampleRender()
{
    int x = 20;
    int y = GetYBelowButtons();

    PrintMessages(x, y);
}
