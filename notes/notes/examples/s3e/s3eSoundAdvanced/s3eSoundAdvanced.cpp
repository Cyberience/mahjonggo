/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3ESoundAdvanced S3E Sound Advanced Example
 *
 * The following example uses S3E's sound functionality to play a raw file and
 * allow you to change its volume as in the S3E Basic Sound Example. It also
 * allows you to change the pitch of the sound playing using the keys or by
 * invoking an automatic pitch shifter.
 *
 * The functions used to achieve this are:
 *  - s3eSoundChannelGetInt()
 *  - s3eSoundChannelSetInt()
 *  - s3eSoundGetInt()
 *  - s3eSoundSetInt()
 *  - s3eSoundChannelRegister()
 *  - s3eSoundGetFreeChannel()
 *
 * The ExampleInit() function finds a free channel on which to play sound and
 * reads in a .raw file so that it can be played. It also sets up callback
 * function thats a global value indicating that a message
 * should be displayed saying playback has ended.
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eSoundAdvancedImage.png
 *
 * @include s3eSoundAdvanced.cpp
 */

#include "ExamplesMain.h"

#define PITCH_SHIFT_DELAY       50
#define PITCH_SHIFT_RANGE       19

void*   g_SoundData;                    // Buffer that holds the sound data

char    g_VolumeString[50];             // Contains info about the volume for displaying
char    g_PitchString[50];              // Contains info about the pitch for displaying

int32   g_FileSize;                     // Size of the sound file

uint64  g_PitchUpdateTime = 0;          // Timer for auto-wahwah effect
int32   g_StartingPitch;                // Remember starting pitch for reset

int     g_Channel;               // Free channel which is being used for the playing of the sample

bool    g_IsPlaying;                    // True if we're playing a sample

bool    g_UseWhaWha = false;            // Use auto-pitch shifting yes/no?
bool    g_DisplayEndofSample = false;   // SIplay end-of-sample message yes/no?
uint64  g_EndofSampleTime = 0;          // Make sure end-of-sample message is only displayed a set amount of time

Button* g_ButtonPlay = 0;
Button* g_ButtonStop = 0;
Button* g_ButtonToggleWaaWaa = 0;
Button* g_ButtonVolumeUp = 0;
Button* g_ButtonVolumeDown = 0;
Button* g_ButtonPitchUp = 0;
Button* g_ButtonPitchDown = 0;

bool    g_NeverPlayed = true;           // True if we've never played a sound

/*
 * The following function increases the volume in increments of ten. It uses
 * the s3eSoundGetInt() function to first get the current volume and then it
 * uses the s3eSoundSetIn() function to set the volume to a level of ten above
 * the original.
 */
void VolumeUp()
{
    int32 TempVolume = s3eSoundGetInt(S3E_SOUND_VOLUME);

    TempVolume += 10;

    if (TempVolume >S3E_SOUND_MAX_VOLUME)
        TempVolume = S3E_SOUND_MAX_VOLUME;

    s3eSoundSetInt(S3E_SOUND_VOLUME, TempVolume);

    sprintf(g_VolumeString, "`x666666Volume level: %d", TempVolume);
}

/*
 * This function decreases the volume in increments of ten. It uses
 * the s3eSoundGetInt() function to first get the current volume and then it
 * uses the s3eSoundSetIn() function to set the volume to a level of ten below
 * the original.
 */
void VolumeDown()
{
    int32 TempVolume = s3eSoundGetInt(S3E_SOUND_VOLUME);

    TempVolume -= 10;

    if (TempVolume < 0)
        TempVolume = 0;

    s3eSoundSetInt (S3E_SOUND_VOLUME, TempVolume);
    sprintf(g_VolumeString, "`x666666Volume level: %d", TempVolume);
}

/*
 * This function increases the pitch in increments of 0x400. It uses
 * the s3eSoundChannelGetInt() function to first the current pitch and then it
 * uses the s3eSoundChannelSetInt() function to set the pitch to a level of
 * 0x400 above the original.
 */
void PitchUp()
{
    int32 TempPitch = s3eSoundChannelGetInt (g_Channel, S3E_CHANNEL_PITCH);
    s3eSoundChannelSetInt(g_Channel, S3E_CHANNEL_PITCH, TempPitch+0x400);
    sprintf(g_PitchString, "`x666666Pitch level is: %d", s3eSoundChannelGetInt(g_Channel, S3E_CHANNEL_PITCH));
}

/*
 * This function decrements the pitch by 0x400. It uses
 * the s3eSoundChannelGetInt() function to first the current pitch and then it
 * uses the s3eSoundChannelSetInt() function to set the pitch to a level of
 * 0x400 above the original.
 */
void PitchDown()
{
    int32 TempPitch = s3eSoundChannelGetInt(g_Channel, S3E_CHANNEL_PITCH);
    s3eSoundChannelSetInt(g_Channel, S3E_CHANNEL_PITCH, TempPitch-0x400);
    sprintf(g_PitchString, "`x666666Pitch level is: %d", s3eSoundChannelGetInt(g_Channel, S3E_CHANNEL_PITCH));
}

/*
 * The following function cycles the pitch up and down using a timer.
 */
void WhaWha()
{
    static int  counter = 0;
    static bool shiftUp = false;

    if ((s3eTimerGetMs() - g_PitchUpdateTime) > PITCH_SHIFT_DELAY)
    {
        if (shiftUp == true)
            PitchUp();
        else
            PitchDown();

        g_PitchUpdateTime = s3eTimerGetMs();
        counter++;

        // After PITCH_SHIFT_RANGE reverse pitch changing direction
        if (counter > PITCH_SHIFT_RANGE)
        {
            counter = 0;
            shiftUp = !shiftUp;
        }
    }
}

/*
 * The following function is registered as a callback function to be called
 * when the S3E_CHANNEL_END_SAMPLE event has occurred.
 */
int32 OnEndOfSample(void * _systemData, void* userData)
{
    // When the end of the sample is reached, set a global variable to cause
    // a message to be displayed.
    g_DisplayEndofSample = true;

    // ExampleUpdate will start a timer to make sure that the sample is just
    // being displayed for a set period of time
    g_EndofSampleTime = 0;

    // Return 1 to indicate to the sound system that the sample should repeat
    return 1;
}

/*
 * The following function opens the sample raw file and loads it into memory.
 * It also finds a free channel on which the sound can be played using
 * s3eSoundGetFreeChannel(). A callback for the S3E_CHANNEL_END_SAMPLE event
 * is set using the s3eSoundChannelRegister() function.
 */
void ExampleInit()
{
    // Read in sound data
    s3eFile *fileHandle = s3eFileOpen("test.raw", "rb");

    g_FileSize = s3eFileGetSize(fileHandle);
    g_SoundData = s3eMallocBase(g_FileSize);
    memset(g_SoundData, 0, g_FileSize);
    s3eFileRead(g_SoundData, g_FileSize, 1, fileHandle);
    s3eFileClose(fileHandle);

    // Finds a free channel that we can use to play our raw file on.
    g_Channel = s3eSoundGetFreeChannel();

    // Remember original pitch
    g_StartingPitch = s3eSoundChannelGetInt(g_Channel, S3E_CHANNEL_PITCH);

    // Register callback for detecting end of sample
    s3eSoundChannelRegister(g_Channel, S3E_CHANNEL_END_SAMPLE, &OnEndOfSample, NULL);

    s3eSoundSetInt(S3E_SOUND_DEFAULT_FREQ, 8000);

    // Initializing output strings
    sprintf(g_VolumeString, "`x666666Volume level: %d", s3eSoundGetInt(S3E_SOUND_VOLUME));
    sprintf(g_PitchString, "`x666666Pitch level is: %d", s3eSoundChannelGetInt(g_Channel, S3E_CHANNEL_PITCH));

    g_ButtonPlay                = NewButton("Play");
    g_ButtonStop                = NewButton("Stop");
    g_ButtonToggleWaaWaa        = NewButton("Toggle WaaWaa");
    g_ButtonVolumeUp            = NewButton("Volume Up");
    g_ButtonVolumeDown          = NewButton("Volume Down");
    g_ButtonPitchUp             = NewButton("Pitch Up");
    g_ButtonPitchDown           = NewButton("Pitch Down");

    if (g_DeviceHasKeyboard)
    {
        g_ButtonVolumeUp->m_Display     = false;
        g_ButtonVolumeDown->m_Display   = false;
        g_ButtonPitchUp->m_Display      = false;
        g_ButtonPitchDown->m_Display    = false;
    }
}

void ExampleTerm()
{
}

/*
 * The following function checks which buttons where pressed and changes the state
 * of sound playback, the volume and the pitch based on it. Playback is
 * started using the s3eSoundChannelPlay() function and stopped using the
 * s3eSoundChannelStop() function.
 */
bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    // work out which button was pressed
    if (g_DeviceHasKeyboard)
    {
        if (s3eKeyboardGetState(s3eKeyUp) & S3E_KEY_STATE_PRESSED)
            pressed = g_ButtonVolumeUp;

        if (s3eKeyboardGetState(s3eKeyDown) & S3E_KEY_STATE_PRESSED)
            pressed = g_ButtonVolumeDown;

        if (s3eKeyboardGetState(s3eKeyRight) & S3E_KEY_STATE_PRESSED)
            pressed = g_ButtonPitchUp;

        if (s3eKeyboardGetState(s3eKeyLeft) & S3E_KEY_STATE_PRESSED)
            pressed = g_ButtonPitchDown;
    }

    if (pressed == g_ButtonPlay && !g_IsPlaying)
    {
        // Play the sound. Note that the 3rd paramater needs to be the number of samples to play, which
        // is the number of bytes divided by 2. A repeat count of 0 means 'play forever'.
        s3eSoundChannelPlay(g_Channel, (int16 *)g_SoundData, g_FileSize/2, 0, 0);
        g_IsPlaying = true;
        g_NeverPlayed = false;
    }

    if (pressed == g_ButtonStop && g_IsPlaying)
    {
        s3eSoundChannelStop(g_Channel);
        g_IsPlaying = false;
    }

    if (pressed == g_ButtonToggleWaaWaa)
    {
        // Start WahwahTimer
        g_PitchUpdateTime = s3eTimerGetMs();
        g_UseWhaWha = !g_UseWhaWha;

        // On stopping whawha effect reset to original value
        s3eSoundChannelSetInt(g_Channel, S3E_CHANNEL_PITCH, g_StartingPitch);
        sprintf(g_PitchString, "`x666666Pitch level is: %d", s3eSoundChannelGetInt(g_Channel, S3E_CHANNEL_PITCH));
    }

    if (pressed == g_ButtonVolumeUp && g_IsPlaying)
        VolumeUp();

    if (pressed == g_ButtonVolumeDown && g_IsPlaying)
        VolumeDown();

    if (pressed == g_ButtonPitchUp && g_IsPlaying)
        PitchUp();

    if (pressed == g_ButtonPitchDown && g_IsPlaying)
        PitchDown();

    if (g_UseWhaWha == true && g_IsPlaying)
        WhaWha();

    // Start the timer in order to make sure that the sample is just being displayed
    // for a set periodof time. Cannot use s3e API (e.g. s3eTimerGetMs) within the
    // callback as it is not thread-safe
    if (g_DisplayEndofSample == true && g_EndofSampleTime == 0)
        g_EndofSampleTime = s3eTimerGetMs();

    // Make sure the displaying of the "end-of-sample" message lasts only for a set amount of time
    if (s3eTimerGetMs() - g_EndofSampleTime > 2000)
    {
        g_DisplayEndofSample = false;
        g_EndofSampleTime = 0;
    }

    return true;
}

const char* GetKeyName(s3eKey key)
{
    static char name[64];
    s3eKeyboardGetDisplayName(name, key, 1);
    return name;
}

/*
 * The following function displays the controls that can be used to change
 * the state of playback and the current values for the pitch and the volume.
 */
void ExampleRender()
{
    const int textHeight = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);

    // Display menu
    int x = 10;

    g_ButtonPlay->m_Enabled = !g_IsPlaying;
    g_ButtonStop->m_Enabled = g_IsPlaying;
    g_ButtonToggleWaaWaa->m_Enabled = g_IsPlaying;
    g_ButtonVolumeUp->m_Enabled = g_IsPlaying;
    g_ButtonVolumeDown->m_Enabled = g_IsPlaying;
    g_ButtonPitchUp->m_Enabled = g_IsPlaying;
    g_ButtonPitchDown->m_Enabled = g_IsPlaying;

    // get a safe Y location below all the buttons
    int y = GetYBelowButtons();

    if (g_DeviceHasKeyboard)
    {
        y += textHeight;

        if (g_IsPlaying)
        {
            s3eDebugPrint(x, y, "`x666666Up/Down: Change volume", 0);
            y += textHeight;
            s3eDebugPrint(x, y, "`x666666Left/Right: Change pitch", 0);
        }
        else
        {
            s3eDebugPrint(x, y, "`xa0a0a0Up/Down: Change volume", 0);
            y += textHeight;
            s3eDebugPrint(x, y, "`xa0a0a0Left/Right: Change pitch", 0);
        }

        y += 2 * textHeight;
    }

    // Display info
    s3eDebugPrint(x, y, g_VolumeString, 0);
    y += textHeight;
    s3eDebugPrint(x, y, g_PitchString, 0);
    y += 2 * textHeight;

    if (g_DisplayEndofSample)
    {
        s3eDebugPrint(x, y, "`x666666End of sample!", 0);
    }
}
