/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3ERokuIAP S3E Roku IAP Example
 *
 * The following example demonstrates usage of the s3eRokuIAP API.
 *
 * It requests and outputs to debug traces containing user account information,
 * list of available items for the local channel (Channel Catalog).
 *
 * It also illustrates how to manipulate an order (add items, edit amount and
 * purchase).
 *
 * The functions used to achieve this are:
 *
 *  - s3eRokuIAPAvailable()
 *  - 3eRokuIAPGetUserData()
 *  - s3eRokuIAPGetUserPurchases()
 *  - s3eRokuIAPGetChannelCatalog()
 *  - s3eRokuIAPGetOrder()
 *  - s3eRokuIAPClearOrder()
 *  - s3eRokuIAPDoOrder()
 *  - s3eRokuIAPDeltaOrder()
 *
 * N.B.: The example uses the "fake server" test mode - do not use in production!
 *
 * @include s3eRokuIAP.cpp
 */

#include "ExamplesMain.h"
#include "s3eRokuIAP.h"

static Button* g_ButtonGetUserData = 0;
static Button* g_ButtonGetUserPurchases = 0;
static Button* g_ButtonGetChannelCatalog = 0;
static Button* g_ButtonGetOrder = 0;
static Button* g_ButtonGetClearOrder = 0;
static Button* g_ButtonDoOrder = 0;
static Button* g_ButtonAddToOrder = 0;

const int g_NumMessages = 50;
const int g_MessageLen = 160;

// Used to track status of test item purchase order.
s3eResult g_DoOrderResult = S3E_RESULT_ERROR;

// The test item code as defined in the data in the "csfake" extension subfolder.
const char* testItemCode = "TS1";

int32 UserDataCallback(void *systemData, void *userData)
{
    ClearMessages();
    s3eRokuIAPUserData* pData = (s3eRokuIAPUserData*) systemData;

    if (!pData)
        return S3E_RESULT_ERROR;

    AppendMessage("Firstname: %s", pData->m_Firstname);
    AppendMessage("Lastname: %s", pData->m_Lastname);
    AppendMessage("Email: %s", pData->m_Email);
    AppendMessage("Street1: %s", pData->m_Street1);
    AppendMessage("Street2: %s", pData->m_Street2);
    AppendMessage("City: %s", pData->m_City);
    AppendMessage("State: %s", pData->m_State);
    AppendMessage("Zip: %s", pData->m_Zip);
    AppendMessage("Country: %s", pData->m_Country);
    AppendMessage("Phone: %s", pData->m_Phone);

    return S3E_RESULT_SUCCESS;
}

int32 UserPurchasesCallback(void *systemData, void *userData)
{
    ClearMessages();
    s3eRokuIAPPurchaseList* pData = (s3eRokuIAPPurchaseList*) systemData;

    if (!pData)
        return S3E_RESULT_ERROR;

    for (int i = 0; i < pData->m_Count; i++)
    {
        AppendMessage("item(%s, %s, %s, %s, %s, %i)",
                        pData->m_List[i].m_Code,
                        pData->m_List[i].m_Name,
                        pData->m_List[i].m_Description,
                        pData->m_List[i].m_SDPosterUrl,
                        pData->m_List[i].m_HDPosterUrl,
                        pData->m_List[i].m_Count);
    }

    AppendMessage("example(code, name, description, SDPosterUrl, HDPosterUrl, count)");
    AppendMessage("UserPurchasesCallback got list of %i items", pData->m_Count);

    return S3E_RESULT_SUCCESS;
}

int32 ChannelCatalogCallback(void *systemData, void *userData)
{
    ClearMessages();
    s3eRokuIAPCatalogList* pData = (s3eRokuIAPCatalogList*) systemData;

    if (!pData)
        return S3E_RESULT_ERROR;

    for (int i = 0; i < pData->m_Count; i++)
    {
        AppendMessage("item(%s, %s, %s, %s, %s, %s)", pData->m_List[i].m_Code,
                        pData->m_List[i].m_Name,
                        pData->m_List[i].m_Description,
                        pData->m_List[i].m_SDPosterUrl,
                        pData->m_List[i].m_HDPosterUrl,
                        pData->m_List[i].m_Cost);
    }

    AppendMessage("example(code, name, description, SDPosterUrl, HDPosterUrl, cost)");
    AppendMessage("ChannelCatalogCallback got list of %i items", pData->m_Count);

    return S3E_RESULT_SUCCESS;
}

int32 OrderCallback(void *systemData, void *userData)
{
    ClearMessages();
    s3eRokuIAPOrderList* pData = (s3eRokuIAPOrderList*) systemData;

    if (!pData)
        return S3E_RESULT_ERROR;

    for (int i = 0; i < pData->m_Count; i++)
    {
        AppendMessage("item(%s, %i)", pData->m_List[i].m_Code, pData->m_List[i].m_Quantity);
    }

    AppendMessage("example(code, quantity)");
    AppendMessage("OrderCallback got list of %i items", pData->m_Count);

    return S3E_RESULT_SUCCESS;
}

void ExampleInit()
{
    InitMessages(g_NumMessages, g_MessageLen);

    if (!s3eRokuIAPAvailable())
    {
        AppendMessage("`xff0000Roku IAP API isn't available on this device.");
        ExampleTerm();
    }

    // Create the UI.
    g_ButtonGetUserData = NewButton("Get user data");
    g_ButtonGetUserPurchases = NewButton("Get user purchases");
    g_ButtonGetChannelCatalog = NewButton("Get channel catalog");
    g_ButtonGetOrder = NewButton("Get order");
    g_ButtonGetClearOrder = NewButton("Clear order");
    g_ButtonDoOrder = NewButton("Do order");
    g_ButtonAddToOrder = NewButton("Add to order");
    g_ButtonGetUserData->m_Enabled = true;
    g_ButtonGetUserPurchases->m_Enabled = true;
    g_ButtonGetChannelCatalog->m_Enabled = true;
    g_ButtonGetOrder->m_Enabled = true;
    g_ButtonGetClearOrder->m_Enabled = true;
    g_ButtonDoOrder->m_Enabled = true;
    g_ButtonAddToOrder->m_Enabled = true;

    s3eRokuIAPRegister(S3E_ROKUIAP_USER_DATA_RESPONSE, &UserDataCallback, NULL);
    s3eRokuIAPRegister(S3E_ROKUIAP_USER_PURCHASES_RESPONSE, &UserPurchasesCallback, NULL);
    s3eRokuIAPRegister(S3E_ROKUIAP_CHANNEL_CATALOG_RESPONSE, &ChannelCatalogCallback, NULL);
    s3eRokuIAPRegister(S3E_ROKUIAP_ORDER_RESPONSE, &OrderCallback, NULL);

    // N.B.: For test purposes only - use a local faked server response - do not use in production!
    s3eRokuIAPFakeServer(true);
}

void ExampleTerm()
{
    TerminateMessages();
}

bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    if (pressed == g_ButtonGetUserData)
    {
        s3eRokuIAPGetUserData();
    }
    else if (pressed == g_ButtonGetUserPurchases)
    {
        s3eRokuIAPGetUserPurchases();
    }
    else if (pressed == g_ButtonGetChannelCatalog)
    {
        s3eRokuIAPGetChannelCatalog();
    }
    else if (pressed == g_ButtonGetOrder)
    {
        s3eRokuIAPGetOrder();
    }
    else if (pressed == g_ButtonGetClearOrder)
    {
        s3eRokuIAPClearOrder();
    }
    else if (pressed == g_ButtonDoOrder)
    {
        if (s3eRokuIAPDoOrder() == S3E_RESULT_SUCCESS)
        {
            ClearMessages();
            AppendMessage("`x00ff00Test item purchased successfully.");
        }
    }
    else if (pressed == g_ButtonAddToOrder)
    {
        s3eRokuIAPDeltaOrder(testItemCode, 1);
    }

    return true;
}

void ExampleRender()
{
    PrintMessages(50, 420);
}
