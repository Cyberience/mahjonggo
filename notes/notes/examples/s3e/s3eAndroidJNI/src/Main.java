/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
package com.example.jniexample;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.LinearLayout;

import com.ideaworks3d.marmalade.LoaderActivity;

public class Main extends LoaderActivity
{
    public Main()
    {
        Log.i("SampleJava", "Created SampleJava");
    }

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // create a new button
        Button button = new Button(this);
        button.setText("Android Button");

        // create a new layout to put on top
        // of the main main view
        LinearLayout overlayGroup = new LinearLayout(this);

        // put the button in the new group
        overlayGroup.addView(button);

        // position button below example text
        overlayGroup.setPadding(0, 200, 0, 0);

        // overlay the group within the frame
        m_FrameLayout.addView(overlayGroup);
        Log.i("SampleJava", "added button");
    }
}
