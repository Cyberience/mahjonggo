/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
import com.ideaworks3d.marmalade.LoaderAPI;
import android.util.Log;

class SampleJava
{
    /*
     * Native method implemented and registered in s3eAndroidJNI.cpp
     */
    public native void HelloNative();

    /*
     * This java function gets called from the native code in s3eAndroidJNI.cpp
     */
    public void HelloJava()
    {
        /*
         * Standard android logging function call.
         */
        Log.i("SampleJava", "Hello Java!!");

        /*
         * Call the native s3eDebugTraceLine function
         */
        LoaderAPI.s3eDebugTraceLine("TESTING");

        /*
         * Get a value from s3eConfig using the native method
         */
        int result = LoaderAPI.s3eConfigGet("SysStackSwitch", -1);
        LoaderAPI.s3eDebugTraceLine("Got config setting: " + result);

        /*
         * Call back to native code
         */
        HelloNative();
    }
}
