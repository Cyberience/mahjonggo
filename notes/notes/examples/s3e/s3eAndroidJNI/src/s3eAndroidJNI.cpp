/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EAndroidJNI S3E Android JNI Example
 *
 * The following example demonstrates the Android JNI (Java Native Interface)
 * extension by implementing a simple Hello World app using Java.
 *
 * The following functions are used to achieve this:
 * - s3eAndroidJNIGetVM()
 *
 * The example invokes a Java API directly and also includes some compiled Java
 * code which is accessed via JNI. Two Java classes are packaged into a jar
 * file and included using the mkb option 'android-external-jars'. The first
 * is the SampleJava class which is accessed in the 'TestSampleJava' method
 * in this example. The second is the Main class which demonstrates extending
 * the LoaderActivity in order to overlay a button. The mkb option
 * 'android-custom-activity' must be set to the long name of this class. This
 * option means the Main class is used as the applications Main Activity when
 * when the application is deployed to Android.
 *
 *
 * @include s3eAndroidJNI.cpp
 */

#include "ExamplesMain.h"
#include "s3eAndroidJNI.h"
#include <stdlib.h>
#include <jni.h>

#define PACKAGE_LEN 100
char g_Package[PACKAGE_LEN];
const char* g_Status = "Failed";

static Button* g_ButtonGet = NULL;

void HelloNative(JNIEnv* env, jobject obj);


void TestActivity()
{
    //Get a pointer to the JVM using the JNI extension
    JavaVM* jvm = (JavaVM*)s3eAndroidJNIGetVM();

    if (!jvm)
    {
        g_Status = "Unable to get JVM";
        return;
    }

    s3eDebugTracePrintf("JVM: %p", jvm);

    //Get the environment from the pointer
    JNIEnv* env = NULL;
    int rtn = jvm->GetEnv((void**)&env, JNI_VERSION_1_6);

    s3eDebugTracePrintf("rtn: %d env: %p", rtn, env);

    //Find the LoaderActivity class using the environment
    jclass LoaderActivity = env->FindClass("com/ideaworks3d/marmalade/LoaderActivity");

    if (!LoaderActivity)
    {
        g_Status = "Unable to find the LoaderActivity class";
        return;
    }

    //Retrieve the Activity field
    jfieldID fActivity = env->GetStaticFieldID(LoaderActivity, "m_Activity", "Lcom/ideaworks3d/marmalade/LoaderActivity;");

    if (!LoaderActivity)
    {
        g_Status = "Unable to find the m_Activity field";
        return;
    }

    //Retrieve the Activity object
    jobject m_Activity = env->GetStaticObjectField(LoaderActivity, fActivity);

    if (!m_Activity)
    {
        g_Status = "Could not get Activity object";
        return;
    }

    //Find the getPackageName method
    jmethodID getPackageName = env->GetMethodID(LoaderActivity, "getPackageName", "()Ljava/lang/String;");

    //Fetch the package name as a java string
    jstring package = (jstring)env->CallObjectMethod(m_Activity, getPackageName);

    //Convert the java string into a c string
    const char* pPackage = env->GetStringUTFChars(package, 0);

    //Store the c string
    snprintf(g_Package, PACKAGE_LEN, "Package name: %s", pPackage);
    g_Status = g_Package;

    //Release the java string
    env->ReleaseStringUTFChars(package, pPackage);

    //Clear up - needed unless we are returning to Java code
    env->DeleteLocalRef(m_Activity);
    env->DeleteLocalRef(package);

}

void TestSampleJava()
{
    //Get a pointer to the JVM using the JNI extension
    JavaVM* jvm = (JavaVM*)s3eAndroidJNIGetVM();

    if (!jvm)
    {
        g_Status = "Unable to get JVM";
        return;
    }

    s3eDebugTracePrintf("JVM: %p", jvm);

    //Get the environment from the pointer
    JNIEnv* env = NULL;
    int rtn = jvm->GetEnv((void**)&env, JNI_VERSION_1_6);

    s3eDebugTracePrintf("rtn: %d env: %p", rtn, env);

    //Find the SampleJava class using the environment
    jclass SampleJava = env->FindClass("SampleJava");

    if (!SampleJava)
    {
        g_Status = "Unable to find the SampleJava class";
        return;
    }

    //Create a new SampleJava object
    jmethodID SampleConstruct = env->GetMethodID(SampleJava, "<init>", "()V");
    jmethodID HelloJava = env->GetMethodID(SampleJava, "HelloJava", "()V");
    jobject SampleObj = env->NewObject(SampleJava, SampleConstruct);

    if (!SampleObj)
    {
        g_Status = "Could not create object";
        return;
    }

    //Register the HelloNative method on the object
    static const JNINativeMethod jnm = { "HelloNative", "()V", (void*)&HelloNative };

    if (env->RegisterNatives(SampleJava, &jnm, 1))
    {
        g_Status = "Could not register native methods";
        return;
    }

    //Call the HelloJava method on the object
    env->CallVoidMethod(SampleObj, HelloJava);

    //Clear up
    env->DeleteLocalRef(SampleObj);

}

//This is the native method that will be called from the Java object
void HelloNative(JNIEnv* env, jobject obj)
{
    s3eDebugOutputString("Hello Native!!");

    g_Status = "Success!!";
}

void ExampleInit()
{
    if (!s3eAndroidJNIAvailable())
    {
        g_Status = "JNI Extension is not available";
        return;
    }

    TestSampleJava();

    g_ButtonGet = NewButton("Get package name");

}

void ExampleTerm()
{
}

bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    if (pressed && pressed == g_ButtonGet)
    {
        TestActivity();
    }

    return true;
}

/*
 * The following function outputs the status to screen. It uses
 * The s3eDebugPrint() function to print the string.
 */
void ExampleRender()
{
    // Print the status
    s3eDebugPrintf(50, 100, 0, "`x666666 %s", g_Status);
}
