/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EWindow S3E Window Extension Example
 *
 * The following example uses the S3E Window extension to
 * change the current display mode and go fullscreen.
 *
 * The main functions and structures used to achieve this are:
 * - s3eWindowDisplayMode
 * - s3eWindowGetDisplayModes()
 * - s3eWindowSetFullscreen()
 *
 * @include s3eWindow.cpp
 */
#include "ExamplesMain.h"
#include "s3eWindow.h"

#define MAX_MODES_BUTTONS 5

static int g_NumModes = 0;
static Button* g_ModesButtons[MAX_MODES_BUTTONS] = {0};
static s3eWindowDisplayMode g_ButtonModes[MAX_MODES_BUTTONS] = {{0}};
static Button* g_CustomResolutionButton = NULL;
static const int g_CustomResolutionWidth = 800;
static const int g_CustomResolutionHeight = 600;
static Button* g_ButtonToggleFullscreen = NULL;
static Button* g_ButtonTitle = NULL;
static Button* g_ButtonMin = NULL;
static s3eWindowDisplayMode g_FullscreenMode = {0};
static s3eBool g_IsFullscreen;

void SetResizeButtonsEnabled(bool enabled)
{
    for (int iMode = 0; iMode < g_NumModes; ++iMode)
    {
        g_ModesButtons[iMode]->m_Enabled = enabled;
    }
}

void ExampleInit()
{
    if (!s3eWindowAvailable())
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, ("Window extension is not available. This example requires a windowing environment."));
        s3eDeviceExit(-1);
    }

    // It's possible to start the application in fullscreen via icf settings so we need to check
    int configFullscreen = 0;
    s3eConfigGetInt("S3E", "WinFullScreen", (int*)&configFullscreen);
    g_IsFullscreen = configFullscreen ? S3E_TRUE : S3E_FALSE;

    //Get the number of display modes
    int numModes = 0;
    s3eWindowGetDisplayModes(NULL, &numModes);

    //Retrieve all possible display modes
    s3eWindowDisplayMode* modes = new s3eWindowDisplayMode[numModes];
    s3eWindowGetDisplayModes(modes, &numModes);

    // Pick an arbitrary default mode for fullscreen (unlikely not to have any)
    if (numModes > 0)
        g_FullscreenMode = modes[numModes - 1];

    int buttonIndex = 0;
    for (int i = numModes - 1; i >= 0; i--)
    {
        bool ok = true;
        for (int j = 0; j < buttonIndex; j++)
        {
            //skip modes with same width (so we get a good selection in our MAX_MODES_BUTTONS modes)
            if (g_ButtonModes[j].m_Width == modes[i].m_Width)
                ok = false;
        }

        if (ok && buttonIndex < MAX_MODES_BUTTONS)
        {
            char text[256];
            sprintf(text, "Set resolution %dx%d",
                modes[i].m_Width,
                modes[i].m_Height);
            g_ModesButtons[buttonIndex] = NewButton(text);
            g_ButtonModes[buttonIndex] = modes[i];
            buttonIndex++;
            g_NumModes++;
        }
    }

    // Windows 10 UWP supports switch to current fullscreen resolution only.
    // In windowed mode resizing to fullscreen resolution will fail.
    // Add extra resolution to test window resize.
    if (s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WIN10)
    {
        char customResolutionText[256];
        sprintf(customResolutionText, "Set resolution %dx%d", g_CustomResolutionWidth, g_CustomResolutionHeight);
        g_CustomResolutionButton = NewButton(customResolutionText);
    }
    g_ButtonToggleFullscreen = NewButton("Toggle Fullscreen");
    g_ButtonMin = NewButton("Minimise");
    // Minimise is not supported on Windows 10
    if (s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WIN10)
    {
        g_ButtonMin->m_Enabled = false;
    }
    g_ButtonTitle = NewButton("Set Title");

    delete[] modes;
}

void ExampleTerm()
{
}

bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    for (int i=0; i < MAX_MODES_BUTTONS; i++)
    {
        if (!g_ModesButtons[i])
            break;

        if (pressed == g_ModesButtons[i])
        {
            g_IsFullscreen = S3E_FALSE;
            g_FullscreenMode = g_ButtonModes[i];
            s3eWindowResize(g_ButtonModes[i].m_Width, g_ButtonModes[i].m_Height);
        }
    }

    if (g_CustomResolutionButton && pressed == g_CustomResolutionButton)
    {
        g_IsFullscreen = S3E_FALSE;
        s3eWindowResize(g_CustomResolutionWidth, g_CustomResolutionHeight);
    }

    if (pressed == g_ButtonToggleFullscreen)
    {
        if (g_IsFullscreen)
        {
            SetResizeButtonsEnabled(true);
            s3eWindowSetFullscreen(NULL);
        }
        else
        {
            SetResizeButtonsEnabled(false);
            s3eWindowSetFullscreen(&g_FullscreenMode);
        }

        g_IsFullscreen = g_IsFullscreen == S3E_TRUE ? S3E_FALSE : S3E_TRUE;
    }
    else if (pressed == g_ButtonTitle)
    {
        s3eWindowSetTitle("You changed my title!");
    }
    else if (pressed == g_ButtonMin)
    {
        s3eWindowMinimise();
    }

    return true;
}

void ExampleRender()
{
    s3eDebugPrintf(0, 0, 1, "`x666666s3eSurface reports width %d and height %d", s3eSurfaceGetInt(S3E_SURFACE_WIDTH), s3eSurfaceGetInt(S3E_SURFACE_HEIGHT));
}
