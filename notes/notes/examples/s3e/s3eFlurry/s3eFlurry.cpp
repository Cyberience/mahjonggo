/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EFlurry S3E Flurry Example
 *
 * The following example demonstrates using of the S3E Flurry extension
 *
 * It starts Flurry session and sends Flurry events in the loop (key=value, and key1=value1)
 * To check test you have to visit the Flurry developer site.
 *
 * The functions used to achieve this are:
 *  - s3eFlurryAvailable()
 *  - s3eFlurryStartSession()
 *  - s3eFlurrySetUserID()
 *  - s3eFlurrySetAge()
 *  - s3eFlurryLogEvent()
 *  - s3eFlurryLogEventTimed()
 *  - s3eFlurryLogEventParams()
 *  - s3eFlurryLogEventParamsTimed()
 *  - s3eFlurryCountPageView()
 *  - s3eFlurryLogError()
 *  - s3eFlurryEndSession()
 *
 * @include s3eFlurry.cpp
 */
#include "s3e.h"
#include "s3eFlurry.h"
#include <string.h>

//Visit the Flurry developer site to get keys for your applications.
//If you need more information how to use Flurry follow to https://developer.yahoo.com/flurry/docs/
#define FLURRY_API_KEY_IOS "ENTER_YOUR_IOS_API_KEY_HERE"
#define FLURRY_API_KEY_ANDROID "ENTER_YOUR_ANDROID_API_KEY_HERE"
#define FLURRY_API_KEY_WP8 "ENTER_YOUR_WP_API_KEY_HERE"

enum FLURRY_EXAMPLE_STATE
{
    FLURRY_START_FAILED,
    FLURRY_STARTED,
    FLURRY_WRONG_CREDENTIALS
};

// Main entry point for the application
int main()
{
    FLURRY_EXAMPLE_STATE res = FLURRY_START_FAILED;
    if (s3eFlurryAvailable())
    {
        res = FLURRY_STARTED;
        switch(s3eDeviceGetInt(S3E_DEVICE_OS))
        {
        case S3E_OS_ID_IPHONE:
            if (strstr(FLURRY_API_KEY_IOS, "ENTER_"))
            {
                res = FLURRY_WRONG_CREDENTIALS;
                break;
            }
            s3eFlurryStartSession(FLURRY_API_KEY_IOS);
            break;
        case S3E_OS_ID_ANDROID:
            if (strstr(FLURRY_API_KEY_ANDROID, "ENTER_"))
            {
                res = FLURRY_WRONG_CREDENTIALS;
                break;
            }
            s3eFlurryStartSession(FLURRY_API_KEY_ANDROID);
            break;
        case S3E_OS_ID_WP8:
            if (strstr(FLURRY_API_KEY_WP8, "ENTER_"))
            {
                res = FLURRY_WRONG_CREDENTIALS;
                break;
            }
            s3eFlurryStartSession(FLURRY_API_KEY_WP8);
            break;
        default:
            res = FLURRY_START_FAILED;
            break;
        }

        if(res == FLURRY_STARTED)
        {
            s3eFlurrySetUserID("userID");
            s3eFlurrySetAge(105);
            s3eFlurryLogEvent("eventName");
        }
    }
    // Wait for a quit request from the host OS
    while (!s3eDeviceCheckQuitRequest())
    {
        s3eFlurryLogEventTimed("whileTimedEvent");
        s3eFlurryLogEventParams("whileParamsEvent", "key|value");
        s3eFlurryLogEventParamsTimed("whileTimedParamsEvent", "key1|value1");
        // Fill background blue
        s3eSurfaceClear(0, 0, 255);

        // Print a line of debug text to the screen at top left (0,0)
        // Starting the text with the ` (backtick) char followed by 'x' and a hex value
        // determines the colour of the text.
        switch (res) {
            case FLURRY_STARTED:
                s3eDebugPrint(120, 150, "`xffffffFlurry is on!", 0);
                break;
            case FLURRY_WRONG_CREDENTIALS:
                s3eDebugPrint(120, 150, "`xffffffForget to enter credentials!", 0);
                break;
            default:
                s3eDebugPrint(120, 150, "`xffffffFlurry is off!", 0);
                break;
        }

        // Flip the surface buffer to screen
        s3eSurfaceShow();

        // Sleep for 0ms to allow the OS to process events etc.
        s3eDeviceYield(0);
    }
    s3eFlurryCountPageView();
    s3eFlurryLogError("error", "OMG!");
    s3eFlurryEndSession();
    return 0;
}
