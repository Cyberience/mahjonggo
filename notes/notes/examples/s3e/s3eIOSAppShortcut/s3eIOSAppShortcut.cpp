/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EIOSAppShortcut S3E IOS App Shortcut example
 *
 * The following example demonstrates how to handle iOS Application shortcuts
 * triggered from context menu on iOS devices with 3D Touch technology.
 *
 * The app uses Info.plist extension (in data/s3eAppShortcut.plist) to define
 * three application shortcuts. Each of them represents a different colour of the
 * screen
 *
 * The example, after checking that s3eIOSAppShortcut extension is available,
 * process the initial app shortcut if the application was launched with one.
 * Also it registers for S3E_IOSAPPSHORTCUT_RECEIVED callback and changes the
 * screen when an action is triggered once the app is started.
 *
 * @include s3eIOSAppShortcut.cpp
 */
#include "s3e.h"
#include "s3eIOSAppShortcut.h"
#include <string.h>
#include "IwDebug.h"

const char* kColourRed = "com.marmalade.action.red";
const char* kColourGreen = "com.marmalade.action.green";
const char* kColourBlue = "com.marmalade.action.blue";

char* g_Colour = (char*) kColourBlue;

int32 HandleAppShortcut(void* systemData, void* userData)
{
    s3eIOSAppShortcutInfo* info = (s3eIOSAppShortcutInfo*)systemData;
    IwDebugTraceLinePrintf("In callback got - %s.", info->m_Type);
    if(strcmp(info->m_Type, kColourRed) == 0)
    {
        g_Colour = (char*) kColourRed;
    }
    else if(strcmp(info->m_Type, kColourGreen) == 0)
    {
        g_Colour = (char*) kColourGreen;
    }
    else
    {
        g_Colour = (char*) kColourBlue;
    }
    return 1;
}


// Main entry point for the application
int main()
{
    if(s3eIOSAppShortcutAvailable())
    {
        // If there is any initial Shortcut data, process it
        s3eIOSAppShortcutInfo* initialInfo = s3eIOSAppShortcutGetInitialShortcut();

        if (initialInfo)
        {
            HandleAppShortcut((void*)initialInfo, NULL);
        }

        // Register for S3E_IOSAPPSHORTCUT_RECEIVED notification to handle App Shortcuts after the application has started
        s3eIOSAppShortcutRegister(S3E_IOSAPPSHORTCUT_RECEIVED, HandleAppShortcut, NULL);
    }

    // Wait for a quit request from the host OS
    while (!s3eDeviceCheckQuitRequest())
    {
        if (g_Colour == kColourRed)
        {
            // Fill background red
            s3eSurfaceClear(255, 0, 0);
        }
        else if (g_Colour == kColourGreen)
        {
            // Fill background green
            s3eSurfaceClear(0, 255, 0);
        }
        else
        {
            // Fill background blue
            s3eSurfaceClear(0, 0, 255);
        }

        // Flip the surface buffer to screen
        s3eSurfaceShow();

        // Sleep for 0ms to allow the OS to process events etc.
        s3eDeviceYield();
    }
    return 0;
}
