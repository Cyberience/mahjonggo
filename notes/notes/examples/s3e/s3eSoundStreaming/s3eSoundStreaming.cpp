/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3ESoundStreaming S3E Sound Streaming Example
 *
 * The following example uses S3E's sound functionality to play audio using
 * a custom generator through callbacks. It also demonstrates the use of stereo
 * functionality.
 *
 * The functions used to achieve this are:
 * - s3eSoundGetInt()
 * - s3eSoundChannelRegister()
 * - s3eSoundChannelPlay()
 * - s3eSoundChannelStop()
 *
 * Note that the sound generation function in this example expects the input
 * raw sound file to be in stereo at a specific frequency. The input frequency
 * is hardcoded since raw files do not normally contain this information,
 * although it could be added as a file header if required. S3E uses a standard
 * interleaved 16 bit stereo format: PCM, signed, 16 bits per sample per
 * channel. Samples are interleaved (LRLRLR etc) so that each L/R sample pair
 * spans 32 bits adjacent memory.
 *
 * Note that in S3E, all input and output sound buffers and the sample counts
 * for them are in 16 bits, with the exception of the m_NumSamples member of
 * s3eSoundGenAudioInfo. If using stereo mode (s3eSoundGenAudioInfo::m_Stereo
 * is true), m_NumSamples is the number of 32-bit pairs of 16 bit samples;
 * if in mono, it is the number of 16 bit samples as usual.
 *
 * The ExampleInit() function finds a free channel on which to play sound,
 * reads in a .raw file to play and registers callback functions for
 * S3E_CHANNEL_END_SAMPLE and S3E_CHANNEL_GEN_AUDIO. Once a function is
 * registered for S3E_CHANNEL_GEN_AUDIO - in this case AudioCallback() -
 * S3E will now call this function in order to generate and mix audio
 * for the given channel rather than using the default mixer functionality.
 * The systemData parameter passed to this function provides information
 * on the channel being played (see s3eSoundGenAudioInfo).
 *
 * ExampleUpdate() checks for button presses to stop/start audio and switch
 * between mono and sterio sound, which is done by registering/unregistering
 * our AudioCallback() function for S3E_CHANNEL_GEN_AUDIO_STEREO. The user can
 * also switch between left/right/both audio channels when in sterio mode,
 * which is handled by this custom mixing function.
 *
 * For more information on resampling topics see the following links:
 *
 * Comparison of interpolation algorithms in real-time sound processing
 * Vladimir Arnost http://radio.feld.cvut.cz/AES/atp2002/proc/paper07.pdf
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eSoundStreamingImage.png
 *
 * @include s3eSoundStreaming.cpp
 */

#include "ExamplesMain.h"
#include <stdlib.h>
#include <math.h>
#include "wsfir.h"

int16*  g_SoundData;        // Buffer that holds the sound data
int     g_SoundLen;         // Length of sound data
int     g_SoundSamples;     // Length of sound data in 16-bit samples
int     g_PlayingChannel;   // Free channel being used for playing of the sample
bool    g_Playing = false;  // True if we're playing a sample
int     g_InputFileID = 0;

int     g_SamplesPlayed;    // Number of samples played
// g_SamplesPlayed is 16 bit mono, 32 bitstereo. It is used to track the
// current playback position - the position in the input track is found by
// scaling by sample size and resample factor. Tracking is done by output
// position, not input, so that we can use a descreet cumulative value.

Button* g_PlayNoResampleButton;
Button* g_PlayZeroOrderButton;
Button* g_PlayLinearButton;
Button* g_PlayQuadraticButton;
Button* g_StopButton;
Button* g_StereoButton;
Button* g_OutputFilterButton;
Button* g_InputButton;

enum STEREO_MODE
{
    STEREO_MODE_MONO,
    STEREO_MODE_BOTH,
    STEREO_MODE_LEFT,
    STEREO_MODE_RIGHT,
    STEREO_MODE_COUNT
} g_OutputMode = STEREO_MODE_MONO;

enum SAMPLE_RATE_CONVERTER
{
    NO_RESAMPLE,
    ZERO_ORDER_HOLD,
    FIRST_ORDER_INTERPOLATION,
    QUADRATIC_INTERPOLATION,
} conversionType;

int g_InputRate  = 0;     // Sample rate of the raw file
int g_InputIsStereo = 0;  // Raw file is stereo (1) or mono (0)
int g_OutputRate = 0;     // Hardware output rate
int g_W, g_L;             // Interpolation and decimation factors

bool g_UseOutputFilter;
const int NUM_COEFFICIENTS = 129;
int16 g_FilterBuffer[NUM_COEFFICIENTS];
int16 g_FilterBufferR[NUM_COEFFICIENTS];
double g_FilterCoefficients[NUM_COEFFICIENTS];

#define PI 3.14159265
#define min(a,b) (a)<(b)?(a):(b)

// Print output
int g_RepsRemaining = 0;
#define NUM_REPEATS 5

/*
 * The following function is registered as a callback function to be called
 * when the S3E_CHANNEL_END_SAMPLE event has occurred.
 */
int32 EndSampleCallback(void* sys, void* user)
{
    s3eSoundEndSampleInfo* info = (s3eSoundEndSampleInfo*)sys;
    g_SamplesPlayed = 0;
    g_RepsRemaining = info->m_RepsRemaining;
    return info->m_RepsRemaining;
}

S3E_INLINE int16 ClipToInt16(int32 sval)
{
    enum
    {
        minval =  INT16_MIN,
        maxval =  INT16_MAX,
        allbits = UINT16_MAX
    };

    // quick overflow test, the addition moves valid range to 0-allbits
    if ((sval-minval) & ~allbits)
    {
        // we overflowed.
        if (sval > maxval)
            sval = maxval;
        else
            if (sval < minval)
                sval = minval;
    }

    return (int16)sval;
}

/*
 * The following function is registered as a callback function to be called
 * when the S3E_CHANNEL_GEN_AUDIO or S3E_CHANNEL_GEN_AUDIO_STEREO events occur.
 * It is called whenever more audio data is required and processes the raw
 * data loaded in ExampleInit(), mixing for mono/stereo depending on user
 * input from ExampleUpdate(). The function receives sample information as the
 * sys parameter and returns the number of samples generated.
 *
 * Note that the input raw file can be stereo or mono and use any sample
 * frequency. These values must be set using the example's custom
 * InputIsStereo and InputFrequency ICF options.
 */
int32 AudioCallback(void* sys, void* user)
{
    s3eTimerGetMs();
    s3eSoundGenAudioInfo* info = (s3eSoundGenAudioInfo*)sys;
    info->m_EndSample = 0;
    int16* target = (int16*)info->m_Target;

    // The user value is g_SamplesPlayed. This tracks the number of 16/32 bit
    // samples played, i.e. playback position in output space. To find position
    // in input track, we scale by sample size and resample factor.
    int inputStartPos = *((int*)user);
    int inputSampleSize = 1;
    if (g_InputIsStereo)
        inputSampleSize = 2;

    int samplesPlayed = 0;

    // For stereo output, info->m_NumSamples is number of l/r pairs (each sample is 32bit)
    // info->m_OrigNumSamples always measures the total number of 16 bit samples,
    // regardless of whether input was mono or stereo.

    int outputSampleSize = info->m_Stereo ? 2 : 1;
    memset(info->m_Target, 0, info->m_NumSamples * outputSampleSize * sizeof(int16));

    // Loop through samples (mono) or sample-pairs (stereo) required.
    // If stereo, we copy the 16bit sample for each l/r channel and do per
    // left/right channel processing on each sample for the pair. i needs
    // scaling when comparing to input sample count as that is always 16bit.
    for (uint i = 0; i < info->m_NumSamples; i++)
    {
        int16 yLeft = 0;  // or single sample if using mono input
        int16 yRight = 0;

        // For each sample (pair) required, we either do:
        //  * get mono sample if input is mono (output can be either)
        //  * get left sample if input is stereo (output can be either)
        //  * get right sample if input and output are both stereo

        int outPosLeft;

        // optionally scale by resample factor (g_W/g_L)
        if (conversionType == NO_RESAMPLE)
            outPosLeft = (inputStartPos + i) * inputSampleSize;
        else
            outPosLeft = ((inputStartPos + i) * g_W / g_L) * inputSampleSize;

        // Stop when hitting end of data. Must scale to 16bit if stereo
        // (m_OrigNumSamples is always 16bit) and by resample factor as we're
        // looping through output position, not input.
        if ((uint)outPosLeft >= info->m_OrigNumSamples)
        {
            info->m_EndSample = 1;
            break;
        }

        // Do resampling
        switch (conversionType)
        {
            case NO_RESAMPLE:
            {
                // copy left (and right) 16bit sample directly from input buffer
                yLeft = info->m_OrigStart[outPosLeft];

                if (g_InputIsStereo)
                {
                    if (g_OutputMode != STEREO_MODE_MONO)
                        yRight = info->m_OrigStart[outPosLeft + 1];
                }

                break;
            }
            case ZERO_ORDER_HOLD:
            {
                yLeft = info->m_OrigStart[outPosLeft];

                if (g_InputIsStereo)
                {
                    if (g_OutputMode != STEREO_MODE_MONO)
                        yRight = info->m_OrigStart[outPosLeft+1];
                }
                break;
            }
            case FIRST_ORDER_INTERPOLATION:
            {
                // Note, these can overflow if posMono directs to the first/last sample.
                // Ideally should do some bounds checking and re-use same sample if needed.

                int16 sample0, sample1;
                sample0 = info->m_OrigStart[outPosLeft];
                if (!g_InputIsStereo)
                {
                    sample1 = info->m_OrigStart[outPosLeft + 1];
                }
                else
                {
                    sample1 = info->m_OrigStart[outPosLeft + 2];

                    if (g_OutputMode != STEREO_MODE_MONO)
                    {
                        int sampleR0 = info->m_OrigStart[outPosLeft + 1];
                        int sampleR1 = info->m_OrigStart[outPosLeft + 3];
                        yRight = (sampleR0 + sampleR1) / 2;
                    }
                }

                yLeft = (sample0 + sample1) / 2;

                break;
            }
            case QUADRATIC_INTERPOLATION:
            {
                // place a parabolic curve between three points to approximate
                // interpolation value.
                float pf = 0.5f;
                int sample0, sample1, sample2;

                sample0 = info->m_OrigStart[outPosLeft];

                if (!g_InputIsStereo)
                {
                    sample1 = info->m_OrigStart[outPosLeft + 1];
                    sample2 = info->m_OrigStart[outPosLeft + 2];
                }
                else
                {
                    sample1 = info->m_OrigStart[outPosLeft + 2];
                    sample2 = info->m_OrigStart[outPosLeft + 4];
                    if (g_OutputMode != STEREO_MODE_MONO)
                    {
                        int sampleR0 = info->m_OrigStart[outPosLeft + 1];
                        int sampleR1 = info->m_OrigStart[outPosLeft + 3];
                        int sampleR2 = info->m_OrigStart[outPosLeft + 5];
                        yRight = sampleR0 + (int16)((pf/2.0f) * (pf * (sampleR0 - (2.0f * sampleR1) + sampleR2))
                                                        - 3.0f * sampleR0 + 4.0f * sampleR1 - sampleR2);
                    }
                }

                yLeft = sample0 + (int16)((pf/2.0f) * (pf * (sample0 - (2.0f * sample1) + sample2))
                                                    - 3.0f * sample0 + 4.0f * sample1 - sample2);

                break;
            }
        }

        if (g_UseOutputFilter)
        {
            // apply FIR filter to output to remove artifacts
            static int bufferPos = -1;
            bufferPos = (bufferPos + 1) % NUM_COEFFICIENTS;

            // use circular buffer to store previous inputs
            g_FilterBuffer[bufferPos] = yLeft;

            yLeft = 0;
            for (int i=0; i< NUM_COEFFICIENTS; i++)
            {
                yLeft += (int16)(g_FilterCoefficients[i] * g_FilterBuffer[(bufferPos + i) % NUM_COEFFICIENTS]);
            }

            if (g_InputIsStereo && g_OutputMode != STEREO_MODE_MONO)
            {
                g_FilterBufferR[bufferPos] = yRight;
                yRight = 0;
                for (int i=0; i< NUM_COEFFICIENTS; i++)
                {
                    yRight += (int16)(g_FilterCoefficients[i] * g_FilterBufferR[(bufferPos + i) % NUM_COEFFICIENTS]);
                }
            }
        }


        int16 orig = 0;
        int16 origR = 0;
        if (info->m_Mix)
        {
            orig = *target;
            origR = *(target+1);
        }


        switch (g_OutputMode)
        {
            case STEREO_MODE_BOTH:
                *target++ = ClipToInt16(yLeft + orig);

                if (g_InputIsStereo)
                    *target++ = ClipToInt16(yRight + origR);
                else
                    *target++ = ClipToInt16(yLeft + orig);

                break;

            case STEREO_MODE_LEFT:
                *target++ = ClipToInt16(yLeft + orig);

                if (g_InputIsStereo)
                    *target++ = ClipToInt16(origR);
                else
                    *target++ = ClipToInt16(orig);

                break;

            case STEREO_MODE_RIGHT:
                *target++ = ClipToInt16(orig);
                if (g_InputIsStereo)
                    *target++ = ClipToInt16(yRight +  origR);
                else
                    *target++ = ClipToInt16(yLeft +  orig);

                break;

            default: //Mono
                *target++ = ClipToInt16(yLeft + orig);
                break;
        }

        samplesPlayed++;
    }

    // Update global output pointer (track samples played in app)
    *((int*)user) += samplesPlayed;

    // Inform s3e sound how many samples we played
    return samplesPlayed;
}

// Greatest Common Divisor
int GCD(int a, int b)
{
    while (1)
    {
        a = a % b;
        if (a == 0)
            return b;
        b = b % a;

        if (b == 0)
            return a;
    }
}

// Read in sound data and set up resampling and filter values
void LoadSound()
{
    if (g_SoundData)
        free(g_SoundData);
    g_SoundData = 0;

    // Get values for current input file. Raw sound files have no header so we
    // use some custom ICF settings.
    char filePath[S3E_FILE_MAX_PATH];
    if (g_InputFileID == 0)
    {
        s3eConfigGetString("InputFile1", "FilePath",    filePath);
        s3eConfigGetInt("InputFile1", "InputFrequency", &g_InputRate);
        s3eConfigGetInt("InputFile1", "InputIsStereo",  &g_InputIsStereo);
    }
    else
    {
        s3eConfigGetString("InputFile2", "FilePath",    filePath);
        s3eConfigGetInt("InputFile2", "InputFrequency", &g_InputRate);
        s3eConfigGetInt("InputFile2", "InputIsStereo",  &g_InputIsStereo);
    }

    s3eFile *fileHandle = s3eFileOpen(filePath, "rb");
    g_SamplesPlayed = 0;
    g_SoundLen = s3eFileGetSize(fileHandle);
    g_SoundSamples = g_SoundLen/2; //or 4 if its 2 bytes per channel and 2 channels per sample
    g_SoundData = (int16*)malloc(g_SoundLen);
    memset(g_SoundData, 0, g_SoundLen);
    s3eFileRead(g_SoundData, g_SoundLen, 1, fileHandle);
    s3eFileClose(fileHandle);

    // Resampling and filtering...

    int fs = min(g_InputRate, g_OutputRate);
    double fc = (fs/2) / (double)g_OutputRate; // half the input sample rate (eg nyquist limit of input)

    // Generate filter coefficients
    wsfirLP(g_FilterCoefficients, NUM_COEFFICIENTS, W_BLACKMAN, fc);

    // Sample rate conversion parameters
    int gcd = GCD(g_InputRate, g_OutputRate);
    g_W = g_InputRate  / gcd;
    g_L = g_OutputRate / gcd;
    s3eDebugTracePrintf("Resampling by rational factor %d / %d", g_W, g_L);

    // Resample factor = (g_W / g_L). Multiply input pos by this to find output sample pos.
}

/*
 * The following function opens the sample raw file and loads it into memory.
 * It also finds a free channel on which the sound can be played using
 * s3eSoundGetFreeChannel() and registers callbacks for the
 * S3E_CHANNEL_END_SAMPLE and S3E_CHANNEL_GEN_AUDIO events using the
 * s3eSoundChannelRegister() function.
 */
void ExampleInit()
{
    g_PlayNoResampleButton = NewButton("Play (no resampling)");
    g_PlayZeroOrderButton = NewButton("Play (zero order hold)");
    g_PlayLinearButton = NewButton("Play (linear interpolation)");
    g_PlayQuadraticButton = NewButton("Play (quadratic interpolation)");
    g_StopButton = NewButton("Stop");

    g_InputButton = NewButton("Change input file");
    g_StereoButton = NewButton("Change Output (mono/stereo/L/R)");
    g_OutputFilterButton = NewButton("Enable Output Filter");

    if (!s3eSoundGetInt(S3E_SOUND_STEREO_ENABLED))
    {
        g_StereoButton->m_Enabled = false;
    }

    // Finds a free channel that we can use to play our raw file on.
    g_PlayingChannel = s3eSoundGetFreeChannel();
    s3eSoundChannelRegister(g_PlayingChannel, S3E_CHANNEL_GEN_AUDIO, AudioCallback, &g_SamplesPlayed);
    s3eSoundChannelRegister(g_PlayingChannel, S3E_CHANNEL_END_SAMPLE, EndSampleCallback, NULL);

    // We will be converting to the device's current output sample rate
    g_OutputRate = s3eSoundGetInt(S3E_SOUND_OUTPUT_FREQ);

    // For acceptable quality, output needs to be filtered:
    //  - if output frequency is lower than input frequency to remove aliasing.
    //  - if output frequency is higher than input frequency to remove copies of original
    //    spectrum around integer multiples sampling frequency.
    g_UseOutputFilter = false;

    memset(g_FilterBuffer, 0, sizeof(int16) * NUM_COEFFICIENTS);
    memset(g_FilterBufferR, 0, sizeof(int16) * NUM_COEFFICIENTS);

    LoadSound();
}

/*
 * Do cleanup work
 */
void ExampleTerm()
{
    free(g_SoundData);
}

/*
 * The following function checks which buttons where pressed and changes
 * between mono and sterio sound playback - by registering/unregistering
 * AudioCallback() as the S3E_CHANNEL_GEN_AUDIO_STEREO function - or
 * cycles through left/right/both stereo channels.
 */
bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    if (!g_Playing)
    {
        bool play = false;

        if (pressed == g_PlayNoResampleButton)
        {
            conversionType = NO_RESAMPLE;
            play = true;
        }
        else if (pressed == g_PlayZeroOrderButton)
        {
            conversionType = ZERO_ORDER_HOLD;
            play = true;
        }
        else if (pressed == g_PlayLinearButton)
        {
            conversionType = FIRST_ORDER_INTERPOLATION;
            play = true;
        }
        else if (pressed == g_PlayQuadraticButton)
        {
            conversionType = QUADRATIC_INTERPOLATION;
            play = true;
        }
        if (play)
        {
            g_SamplesPlayed = 0;
            s3eSoundChannelPlay(g_PlayingChannel, g_SoundData, g_SoundSamples, NUM_REPEATS, 0);
            g_Playing = true;
        }
    }
    else if (pressed == g_StopButton)
    {
        s3eSoundChannelStop(g_PlayingChannel);
        g_Playing = false;
    }
    if (pressed == g_StereoButton)
    {
        g_OutputMode = (STEREO_MODE)((g_OutputMode + 1) % STEREO_MODE_COUNT);

        if (g_OutputMode != STEREO_MODE_MONO)
        {
            s3eSoundChannelRegister(g_PlayingChannel, S3E_CHANNEL_GEN_AUDIO_STEREO, AudioCallback, &g_SamplesPlayed);
        }
        else
        {
            s3eSoundChannelUnRegister(g_PlayingChannel, S3E_CHANNEL_GEN_AUDIO_STEREO);
        }

    }
    if (pressed == g_OutputFilterButton)
    {
        g_UseOutputFilter = !g_UseOutputFilter;

        if (g_UseOutputFilter)
            SetButtonName(g_OutputFilterButton, "Disable Output Filter");
        else
            SetButtonName(g_OutputFilterButton, "Enable Output Filter");
    }
    if (pressed == g_InputButton)
    {
        g_InputFileID = g_InputFileID ? 0 : 1;
        LoadSound();
    }
    return true;
}

const char* GetKeyName(s3eKey key)
{
    static char name[64];
    s3eKeyboardGetDisplayName(name, key, 1);
    return name;
}

/*
 * The following function sets button states and displays playback type,
 * state, etc. plus the controls that can be used to change them.
 */
void ExampleRender()
{
    const int textHeight = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);

    if (g_Playing)
    {
        g_PlayNoResampleButton->m_Enabled = false;
        g_PlayZeroOrderButton->m_Enabled = false;
        g_PlayLinearButton->m_Enabled = false;
        g_PlayQuadraticButton->m_Enabled = false;
        g_StopButton->m_Enabled = true;
        g_InputButton->m_Enabled = false;
    }
    else
    {
        g_PlayNoResampleButton->m_Enabled = true;
        g_PlayZeroOrderButton->m_Enabled = true;
        g_PlayLinearButton->m_Enabled = true;
        g_PlayQuadraticButton->m_Enabled = true;
        g_StopButton->m_Enabled = false;
        g_InputButton->m_Enabled = true;
    }

    int y = GetYBelowButtons() + 3 * textHeight;
    const int x = 10;

    if (s3eSoundGetInt(S3E_SOUND_STEREO_ENABLED))
    {
        s3eDebugPrintf(x, y, 0, "Stereo sound output supported and enabled");
    }
    else
    {
        s3eDebugPrintf(x, y, 0, "Stereo sound output disabled or unsupported");
    }
    y += textHeight;

    if (g_InputIsStereo)
        s3eDebugPrintf(x, y, 0, "`x666666Input: Stereo");
    else
        s3eDebugPrintf(x, y, 0, "`x666666Input: Mono");
    y += textHeight;

    if (g_OutputMode == STEREO_MODE_MONO)
    {
        if (!g_InputIsStereo)
            s3eDebugPrintf(x, y, 0, "`x666666Output: Mono");
        else
            s3eDebugPrintf(x, y, 0, "`x666666Output: Mono (left channel of stereo input)");
    }
    else
    {
        const char* modeLRString;
        switch (g_OutputMode)
        {
            case STEREO_MODE_LEFT:
                modeLRString = "left only";
                break;
            case STEREO_MODE_RIGHT:
                modeLRString = "right only";
                break;
            default:
                modeLRString = "left and right";
                break;
        }

        s3eDebugPrintf(x, y, 0, "`x666666Output: Stereo (%s)", modeLRString);
    }
    y += textHeight;

    s3eDebugPrintf(x, y, 0, "`x666666Input freq: %d", g_InputRate);
    y += textHeight;

    s3eDebugPrintf(x, y, 0, "`x666666Output freq: %d", g_OutputRate);
    y += textHeight;

    s3eDebugPrintf(x, y, 0, "`x666666Use Output Filter: %s", g_UseOutputFilter ? "Yes" : "No");
    y += textHeight;

    s3eDebugPrintf(x, y, 0, "`x666666Plays Remaining: %d", g_RepsRemaining);
    y += textHeight;

    s3eDebugPrintf(x, y, 0, "`x666666Total Samples: %d", g_SoundSamples);
    y += textHeight;

    s3eDebugPrintf(x, y, 0, "`x666666Samples Played: %d", g_SamplesPlayed);
}
