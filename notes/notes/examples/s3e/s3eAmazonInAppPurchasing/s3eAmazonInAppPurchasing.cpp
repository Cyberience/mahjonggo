/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

//-----------------------------------------------------------------------------
// s3eAmazonInAppPurchasing
//-----------------------------------------------------------------------------

/**
 * @page ExampleS3EAmazonInAppPurchasing S3E Amazon In App Purchasing Example
 * The following example demonstrates a basic Amazon in-app purchasing application.
 *
 * This Application demonstrates usage of the  Amazon Marmalade extension on the Android
 * platform. The application is intended to be functional and demonstrate how to use the
 * extension code without the distraction of complex UI.
 * This example illustrates the use of the In App Billing functionality on the
 * Android platform, allowing items, products or services to be bought via Amazon.
 * It connects to request information for a product,allows the user to purchase the product
 * and then displays the receipt for the transaction.
 *
 * For more information on the general topic of Amazon In App Billing see
 * https://developer.amazon.com/public/apis/earn/in-app-purchasing
 *
 * For Amazon IAP testing refer to https://developer.amazon.com/public/apis/earn/in-app-purchasing/docs/testing-iap
 *
 * Note that the API only handles purchasing of the product, not delivery of the
 * actual service/data that was paid for.
 *
 * See S3E Amazon In-App Purchasing overview in the Marmalade API Reference for
 * more information.
 *
 * The main functions demonstrated in this example include:
 * - s3eAmazonInAppPurchasingAvailable()
 * - s3eAmazonInAppPurchasingRegister()
 * - s3eAmazonInAppPurchasingInit()
 * - s3eAmazonInAppPurchasingRequest()
 * - s3eAmazonInAppPurchasingGetItemData()
 * - s3eAmazonInAppPurchasingRestoreEntitlements()
 * - s3eAmazonInAppPurchasingGetUserId()
 *
 * Note that the extension is explicitly initialized and terminated unlike
 * some other extensions.
 *
 * @include s3eAmazonInAppPurchasing.cpp
 *
 */

#include "IwNUI.h"
#include "s3eAmazonInAppPurchasing.h"
#include <time.h>

using namespace IwNUI;


///////////////////////////////////////////////////////////////////////////////

int32 PurchasingAvailableCallback(void *systemData, void *userData);
int32 ItemDataCallback(void *systemData, void *userData);
int32 PurchaseResponseCallback(void *systemData, void *userData);
int32 RestoreEntitlementCallback(void *systemData, void *userData);

int32 ApplicationResumeCallback(void *systemData, void *userData);


///////////////////////////////////////////////////////////////////////////////

bool OnSkuClick(void* data, CButton* button);
bool OnItemInfoClick(void* data, CButton* button);
bool OnRestoreItemsClick(void* data, CButton* button);


///////////////////////////////////////////////////////////////////////////////

void DumpReceipt(s3eAmazonInAppPurchasingReceipt* pReceipt);

///////////////////////////////////////////////////////////////////////////////

static int control_yPos = 4;
CViewPtr view;

///////////////////////////////////////////////////////////////////////////////

const int numbSkus = 5;

const char* skuList[] =
{
    "com.amazon.buttonclicker.subscription.1mo",
    "com.amazon.buttonclicker.ten_clicks",
    "com.amazon.buttonclicker.purple_button",
    "com.amazon.buttonclicker.green_button",
    "com.amazon.buttonclicker.blue_button",
};

///////////////////////////////////////////////////////////////////////////////

struct CatalogEntry
{
    enum State
    {
        UNKNOWN,
        AVAILABLE,
        PURCHASED,
        RESTORED
    };

    const char*        m_sku;
    const char*        m_buttonCaption;
    const char*        m_text;
    bool (*m_ClickHandler)(void*, CButton*);
    State              m_state;
    CButtonPtr         m_pButton;
    CLabelPtr          m_pText;
};

///////////////////////////////////////////////////////////////////////////////

void CreateCatalogControl(CViewPtr view, CatalogEntry *pSku, int& yPos)
{
    CAttributes buttonAttrs = CAttributes()
        .Set("x1",          "2%")
        .Set("width",       "40%")
        .Set("height",      "8%");

    CAttributes textFieldAttrs = CAttributes()
        .Set("name",        "sourceTextField")
        .Set("x1",          "45%")
        .Set("width",       "50%")
        .Set("height",      "8%")
        .Set("font",        "serif")
        .Set("fontSize",    4)
        .Set("fontColour",  CColour(0xFF000000))
        .Set("backgroundColour", CColour(0xFFFFFFFF))
        .Set("fontAlignH",  "left");

    char szYpos[8];

    sprintf(szYpos, "%d%%", yPos);

    pSku->m_pButton = CreateButton(buttonAttrs);
    pSku->m_pButton->SetAttribute("name",    pSku->m_buttonCaption);
    pSku->m_pButton->SetAttribute("caption", pSku->m_buttonCaption);
    pSku->m_pButton->SetAttribute("y1",      szYpos);
    pSku->m_pButton->SetAttribute("enabled", 0);
     
    if (pSku->m_ClickHandler != NULL)
    {
        pSku->m_pButton->SetEventHandler("click", (void*)NULL, pSku->m_ClickHandler);
    }

    pSku->m_pText = CreateLabel(textFieldAttrs);
    pSku->m_pText->SetAttribute("caption",  pSku->m_text);
    pSku->m_pText->SetAttribute("y1",       szYpos);

    pSku->m_state = CatalogEntry::AVAILABLE;

    view->AddChild(pSku->m_pButton);
    view->AddChild(pSku->m_pText);

    yPos += 12;
}

///////////////////////////////////////////////////////////////////////////////

void CreateCatalogControls(CViewPtr view, CatalogEntry *pSkus)
{    
    CatalogEntry *pSku = pSkus;

    while(pSku->m_sku != NULL)
    {
        CreateCatalogControl(view, pSku, control_yPos);

        ++ pSku;
    }
}

///////////////////////////////////////////////////////////////////////////////
// Find the matching CatalogEntry

CatalogEntry* FindCatalogEntry(CatalogEntry* pCatalog, const char *szSku)
{
    CatalogEntry* pEntry  = pCatalog;

    while(pEntry->m_sku != NULL)
    {
        if (strcmp(pEntry->m_sku, szSku) == 0)
        {
            return pEntry;
        }

        ++ pEntry;
    }

    return NULL;
}


CatalogEntry* FindCatalogEntry(CatalogEntry* pCatalog, CButton* pButton)
{
    CatalogEntry*    pEntry  = pCatalog;

    while(pEntry->m_sku != NULL)
    {
        if (pEntry->m_pButton.get() == pButton)
        {
            return pEntry;
        }

        ++ pEntry;
    }

    return NULL;
}

///////////////////////////////////////////////////////////////////////////////

CatalogEntry catalog[] = 
{
    { "Update Catalog",                               "Update Catalog",          "Query the Amazon server for item SKU info", &OnItemInfoClick, CatalogEntry::AVAILABLE },
    { "Restore Entitlements",                         "Restore Entitlements",    "Query the Amazon server for previously purchased items", &OnRestoreItemsClick, CatalogEntry::AVAILABLE },
    { "com.amazon.buttonclicker.subscription.1mo",    "Purchase Sub",            "Subscription",    &OnSkuClick, CatalogEntry::UNKNOWN },
    { "com.amazon.buttonclicker.ten_clicks",          "Purchase Sku 1",          "Consumable",      &OnSkuClick, CatalogEntry::UNKNOWN },
    { "com.amazon.buttonclicker.purple_button",       "Purchase Sku 2",          "Entitlement",     &OnSkuClick, CatalogEntry::UNKNOWN },
    { "com.amazon.buttonclicker.green_button",        "Purchase Sku 3",          "Entitlement",     &OnSkuClick, CatalogEntry::UNKNOWN },
    { "com.amazon.buttonclicker.blue_button",         "Purchase Sku 4",          "Entitlement",     &OnSkuClick, CatalogEntry::UNKNOWN },
    { NULL },
};


///////////////////////////////////////////////////////////////////////////////

int main()
{
    
    if (s3eAmazonInAppPurchasingAvailable())
    {
        s3eAmazonInAppPurchasingRegister(S3E_AMAZONINAPPPURCHASING_AVAILABLE_RESPONSE,              &PurchasingAvailableCallback, NULL);
        s3eAmazonInAppPurchasingRegister(S3E_AMAZONINAPPPURCHASING_ITEM_DATA_RESPONSE,              &ItemDataCallback,            NULL);
        s3eAmazonInAppPurchasingRegister(S3E_AMAZONINAPPPURCHASING_PURCHASE_RESPONSE,               &PurchaseResponseCallback,    NULL);
        s3eAmazonInAppPurchasingRegister(S3E_AMAZONINAPPPURCHASING_RESTORE_ENTITLEMENTS_RESPONSE,   &RestoreEntitlementCallback,  NULL);
        s3eAmazonInAppPurchasingInit();

        // 
        s3eDeviceRegister(S3E_DEVICE_UNPAUSE, ApplicationResumeCallback, NULL); 
             

    }


    CAppPtr app = CreateApp();
    CWindowPtr window = CreateWindow();

    app->AddWindow(window);

    view = CreateView("canvas");

    control_yPos = 4;

    CreateCatalogControl(view, &catalog[0], control_yPos);
    CreateCatalogControl(view, &catalog[1], control_yPos);

    // Disable controls till the purchasing service is available
    catalog[0].m_pButton->SetAttribute("enabled", 0);
    catalog[1].m_pButton->SetAttribute("enabled", 0);
    
    control_yPos += 8;

    window->SetChild(view);

    app->ShowWindow(window);
    
    while(app->Update())
    {
        // Sleep for 0ms to allow the OS to process events etc.
        s3eDeviceYield(5);
    }

    return 0;
}

///////////////////////////////////////////////////////////////////////////////


bool OnSkuClick(void* data, CButton* button)
{
    if (s3eAmazonInAppPurchasingAvailable())
    {
        // Find the catalog entry associated with this button
        CatalogEntry* pEntry = FindCatalogEntry(catalog, button);

        if (pEntry != NULL)
        {
            s3eAmazonInAppPurchasingRequest(pEntry->m_sku);
        }
    }

    return true;
}

bool OnItemInfoClick(void* data, CButton* button)
{
    if (s3eAmazonInAppPurchasingAvailable())
    {
        s3eAmazonInAppPurchasingGetItemData(skuList, numbSkus);
    }
    return true;
}


bool OnRestoreItemsClick(void* data, CButton* button)
{
    if (s3eAmazonInAppPurchasingAvailable())
    {
        s3eAmazonInAppPurchasingRestoreEntitlements();
    }

    return true;
}

///////////////////////////////////////////////////////////////////////////////

int32 PurchasingAvailableCallback(void *systemData, void *userData)
{
    // Enable the catalog button
    catalog[0].m_pButton->SetAttribute("enabled", 1);

    // Enable the restore entitlements button
    catalog[1].m_pButton->SetAttribute("enabled", 1);

    return S3E_RESULT_SUCCESS;
}

///////////////////////////////////////////////////////////////////////////////

int32 ItemDataCallback(void *systemData, void *userData)
{
    s3eAmazonInAppPurchasingItemData* pItemData = (s3eAmazonInAppPurchasingItemData*) systemData;
    if (pItemData == NULL)
        return S3E_RESULT_SUCCESS;
    
    s3eDebugOutputString("AmazonInAppPurchasing: ItemDataCallback");

    // Find the matching CatalogEntry
    CatalogEntry* pEntry  = FindCatalogEntry(catalog, pItemData->m_sku);

    if (pEntry != NULL)
    {
        if (pEntry->m_state == CatalogEntry::UNKNOWN)
        {
            CreateCatalogControl(view, pEntry, control_yPos);
            pEntry->m_state = CatalogEntry::AVAILABLE;
        }

        if (pItemData->m_title != NULL)
            pEntry->m_pButton->SetAttribute("caption", (const char*)pItemData->m_title);

        switch(pItemData->m_itemType)
        {
            case S3E_AMAZONINAPPPURCHASING_ITEM_TYPE_CONSUMABLE:
            case S3E_AMAZONINAPPPURCHASING_ITEM_TYPE_ENTITLED:
            case S3E_AMAZONINAPPPURCHASING_ITEM_TYPE_SUBSCRIPTION:

                pEntry->m_pButton->SetAttribute("enabled", 1);
                if (pItemData->m_description != NULL)
                    pEntry->m_pText->SetAttribute("caption", (const char*)pItemData->m_description);
                break;

            case S3E_AMAZONINAPPPURCHASING_ITEM_TYPE_UNAVAILABLE:
            case S3E_AMAZONINAPPPURCHASING_ITEM_TYPE_UNKNOWN:

                pEntry->m_pButton->SetAttribute("enabled", 0);
                pEntry->m_pText->SetAttribute("caption", "Item is unavailable or unknown");
                break;

        }
    }

    return S3E_RESULT_SUCCESS;
}


///////////////////////////////////////////////////////////////////////////////


int32 PurchaseResponseCallback(void *systemData, void *userData)
{
    s3eAmazonInAppPurchasingReceipt* pReceipt = (s3eAmazonInAppPurchasingReceipt*) systemData;
    if (pReceipt == NULL)
        return S3E_RESULT_SUCCESS;

    s3eDebugOutputString("AmazonInAppPurchasing: PurchaseResponseCallback");

    DumpReceipt(pReceipt);

    CatalogEntry* pEntry  = FindCatalogEntry(catalog, pReceipt->m_sku);

    if (pEntry != NULL)
    {
        if (pEntry->m_state == CatalogEntry::UNKNOWN)
        {
            CreateCatalogControl(view, pEntry, control_yPos);
        }

        if (pEntry->m_state == CatalogEntry::AVAILABLE)
        {
            if (pReceipt->m_itemType == S3E_AMAZONINAPPPURCHASING_ITEM_TYPE_ENTITLED)
            {
                if (pReceipt->m_state == S3E_AMAZONINAPPPURCHASING_PURCHASE_STATE_SUCCESSFUL)
                {
                    pEntry->m_pButton->SetAttribute("enabled", 0);
                    pEntry->m_pText->SetAttribute("caption", "Purchased");
                    s3eAmazonInAppPurchasingNotifyFulfillment(pReceipt->m_sku, S3E_AMAZONINAPPPURCHASING_FULFILLMENT_RESULT_FULFILLED);
                }
                else if (pReceipt->m_state == S3E_AMAZONINAPPPURCHASING_PURCHASE_STATE_ALREADY_ENTITLED)
                {
                    pEntry->m_pButton->SetAttribute("enabled", 0);
                    pEntry->m_pText->SetAttribute("caption", "Already Purchased");
                }
            }
        }
    }


    return S3E_RESULT_SUCCESS;
}

///////////////////////////////////////////////////////////////////////////////

int32 RestoreEntitlementCallback(void *systemData, void *userData)
{
    s3eAmazonInAppPurchasingReceipt* pReceipt = (s3eAmazonInAppPurchasingReceipt*) systemData;
    if (pReceipt == NULL)
        return S3E_RESULT_SUCCESS;

    s3eDebugOutputString("AmazonInAppPurchasing: RestoreEntitlementCallback");

    DumpReceipt(pReceipt);

    // Find the matching CatalogEntry
    CatalogEntry* pEntry  = FindCatalogEntry(catalog, pReceipt->m_sku);

    // Cannot find by sku so try the subscription sku
    if ((pEntry == NULL) && (pReceipt->m_itemType == S3E_AMAZONINAPPPURCHASING_ITEM_TYPE_SUBSCRIPTION))
    {
        pEntry  = FindCatalogEntry(catalog, "com.amazon.buttonclicker.subscription.1mo");
    }
    
    if (pEntry != NULL)
    {
        if (pEntry->m_state == CatalogEntry::UNKNOWN)
        {
            CreateCatalogControl(view, pEntry, control_yPos);
        }

        pEntry->m_state = CatalogEntry::RESTORED;

        if (pReceipt->m_itemType == S3E_AMAZONINAPPPURCHASING_ITEM_TYPE_ENTITLED)
        {
            if (pEntry->m_pButton != NULL)
            {
                pEntry->m_pButton->SetAttribute("enabled", 0);
                pEntry->m_pText->SetAttribute("caption", "Purchase restored");
            }
        }

        if (pReceipt->m_itemType == S3E_AMAZONINAPPPURCHASING_ITEM_TYPE_SUBSCRIPTION)
        {
            if ((pReceipt->m_subscriptionStartUTC != 0) && (pReceipt->m_subscriptionEndUTC == 0))
            {
                // Subscription still valid...no end time!
                time_t startTime    = (time_t) (pReceipt->m_subscriptionStartUTC / 1000L);

                if (pEntry->m_pButton != NULL)
                {
                    char szBuff[128];
                    sprintf(szBuff, "Subscription active from %s", ctime(&startTime));

                    pEntry->m_pButton->SetAttribute("enabled", 0);
                    pEntry->m_pText->SetAttribute("caption", szBuff);
                }

            }
            else
            {
                if ((pReceipt->m_subscriptionStartUTC != 0))
                {
                    char szBuff[128];
                    time_t startTime    = (time_t) (pReceipt->m_subscriptionStartUTC / 1000L);
                    time_t endTime        = (time_t) (pReceipt->m_subscriptionEndUTC / 1000L);
                    time_t now;

                    // get the current time
                    time(&now);

                    // Check were in a valid subscription period
                    if ((difftime(startTime, now) > 0.0f) && (difftime(now, endTime)  > 0.0f))
                    {
                        if (pEntry->m_pButton != NULL)
                        {
                            sprintf(szBuff, "Subscription expires: %s", ctime(&endTime));

                            pEntry->m_pButton->SetAttribute("enabled", 0);
                            pEntry->m_pText->SetAttribute("caption", szBuff);
                        }
                    }
                    else
                    {
                        // Expired subscription
                        sprintf(szBuff, "Subscription expired: %s", ctime(&endTime));

                        pEntry->m_pButton->SetAttribute("enabled", 1);
                        pEntry->m_pText->SetAttribute("caption", szBuff);
                    }

                }
            }
        }
    }


    return S3E_RESULT_SUCCESS;
}

///////////////////////////////////////////////////////////////////////////////

int32 ApplicationResumeCallback(void *systemData, void *userData)
{
    // Restore the user id
    s3eAmazonInAppPurchasingGetUserId();

    return S3E_RESULT_SUCCESS;
}

///////////////////////////////////////////////////////////////////////////////

void DumpPurchaseState(s3eAmazonInAppPurchasingOrderStatus state)
{
    switch(state)
    {
    case S3E_AMAZONINAPPPURCHASING_PURCHASE_STATE_PENDING:
        s3eDebugOutputString("Status: PENDING");
        break;
    case S3E_AMAZONINAPPPURCHASING_PURCHASE_STATE_ALREADY_ENTITLED:
        s3eDebugOutputString("Status: ALREADY_ENTITLED");
        break;
    case S3E_AMAZONINAPPPURCHASING_PURCHASE_STATE_FAILED:
        s3eDebugOutputString("Status: FAILED");
        break;
    case S3E_AMAZONINAPPPURCHASING_PURCHASE_STATE_INVALID_SKU:
        s3eDebugOutputString("Status: INVALID_SKU");
        break;
    case S3E_AMAZONINAPPPURCHASING_PURCHASE_STATE_SUCCESSFUL:
        s3eDebugOutputString("Status: SUCCESSFUL");
        break;
    case S3E_AMAZONINAPPPURCHASING_PURCHASE_STATE_REVOKED:
        s3eDebugOutputString("Status: REVOKED");
        break;
    case S3E_AMAZONINAPPPURCHASING_PURCHASE_STATE_NOT_AVAILABLE:
        s3eDebugOutputString("Status: NOT_AVAILABLE");
        break;
    }
}


void DumpItemType(s3eAmazonInAppPurchasingItemType itemType)
{
    switch(itemType)
    {
    case S3E_AMAZONINAPPPURCHASING_ITEM_TYPE_CONSUMABLE:
        s3eDebugOutputString("Type: CONSUMABLE");
        break;
    case S3E_AMAZONINAPPPURCHASING_ITEM_TYPE_ENTITLED:
        s3eDebugOutputString("Type: ENTITLED");
        break;
    case S3E_AMAZONINAPPPURCHASING_ITEM_TYPE_SUBSCRIPTION:
        s3eDebugOutputString("Type: SUBSCRIPTION");
        break;
    case S3E_AMAZONINAPPPURCHASING_ITEM_TYPE_UNAVAILABLE:
        s3eDebugOutputString("Type: UNAVAILABLE");
        break;
    case S3E_AMAZONINAPPPURCHASING_ITEM_TYPE_UNKNOWN:
        s3eDebugOutputString("Type: UNKNOWN");
        break;
    }
}




void DumpReceipt(s3eAmazonInAppPurchasingReceipt* pReceipt)
{
    s3eDebugOutputString(pReceipt->m_sku);

    DumpPurchaseState(pReceipt->m_state);

    if ((pReceipt->m_state == S3E_AMAZONINAPPPURCHASING_PURCHASE_STATE_SUCCESSFUL)
        || (pReceipt->m_state == S3E_AMAZONINAPPPURCHASING_PURCHASE_STATE_ALREADY_ENTITLED))
    {
        DumpItemType(pReceipt->m_itemType);
    }

    if (pReceipt->m_purchaseToken != NULL)
    {
        s3eDebugOutputString(pReceipt->m_purchaseToken);
    }

    if (pReceipt->m_subscriptionStartUTC != 0)
    {
        char szBuff[128];
        time_t startTime = (time_t) (pReceipt->m_subscriptionStartUTC / 1000L);
        sprintf(szBuff, "Subscription Start: %s", ctime(&startTime));
        s3eDebugOutputString(szBuff);
    }

    if (pReceipt->m_subscriptionEndUTC != 0)
    {
        char szBuff[128];
        time_t endTime = (time_t) (pReceipt->m_subscriptionEndUTC / 1000L);
        sprintf(szBuff, "Subscription End: %s", ctime(&endTime));
        s3eDebugOutputString(szBuff);
    }

}


///////////////////////////////////////////////////////////////////////////////
