/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EIOSGameKit S3E iPhone Game Kit Example
 *
 * The following example demonstrates the IphoneGameKit functionality by
 * implementing a simple chat room. This example only runs on iPhone.
 *
 * The main functions used to achieve this are:
 * - s3eIOSGameKitStartSession()
 * - s3eIOSGameKitStopSession()
 * - s3eOSReadStringUTF8()
 * - s3eIOSGameKitSessionGetPeersWithConnectionState()
 * - s3eIOSGameKitConnectToPeer()
 * - s3eIOSGameKitSessionGetString()
 * - s3eIOSGameKitPeerGetString()
 *
 * A Bluetooth session is started in either server or client mode. As a server,
 * the device can recieve connection requests from other devices while as a
 * client it can make requests to connect to a server device. A fixed string,
 * SESSION_ID_STRING, is used to identify the session so that devices can
 * what service they are connecting to.
 *
 * A list of peers trying to be connected with is storred as a set of s3eIOSGameKitPeer
 * structs. Once successfully connected, on each update loop the list of peers
 * is updated using s3eIOSGameKitSessionGetPeersWithConnectionState(). The application
 * attempts to connected with any new peers found, using s3eIOSGameKitConnectToPeer().
 *
 * NB: Starting sessions and connecting to peers are asynchrnous operations.
 * Most functions return straight away and require callbacks to be to notify
 * the task has completed. The following callbacks are registered (through
 * s3eIOSGameKitStartSession):
 * - s3eIOSGameKitSessionRecieveDataCallbackFn
 * - s3eIOSGameKitSessionPeerAttemptedToConnectCallbackFn
 * - s3eIOSGameKitSessionConnectionCallbackFn
 * - s3eIOSGameKitSessionDisconnectionCallbackFn
 *
 * The application waits for buttons to be pressed before using any of the
 * IPhoneGameKit functionality. Detection of button presses is handled using
 * the generic code in ExamplesMain.cpp
 *
 * Messges are output to screen using s3eDebugPrint().
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eIOSGameKitImage.png
 *
 * @include s3eIOSGameKit.cpp
 */
#include "ExamplesMain.h"
#include "s3eOSReadString.h"
#include "s3eIOSGameKit.h"
#include "s3eIOSNotifications.h"

#include <set>

// Fixed maximums for messages and amount of peers supported.
#define NUM_MESSAGES 30
#define MESSAGE_LEN 60
#define MAX_PEERS_TO_CONNECT_TO 5

static char gUserName[20] = "Unknown User";

static Button*  g_ButtonServer = 0;
static Button*  g_ButtonClient = 0;
static Button*  g_ButtonPeer = 0;
static Button*  g_ButtonSend = 0;
static Button*  g_ButtonStop = 0;

static s3eIOSGameKitSession* g_GKSession = 0;
static bool g_Connected = false;
s3eIOSGameKitSessionMode g_ConnectionType;
static std::set<s3eIOSGameKitPeer*> gConnectingSet;

#define SESSION_ID_STRING "_s3echatexample._tcp"


// Callback functions to report connection and data events

static void GKSessionConnectionCallback(struct s3eIOSGameKitSession* session, s3eIOSGameKitSessionConnectResult* result, void* userData)
{
    char        message[MESSAGE_LEN];
    const char *pPeerName = s3eIOSGameKitPeerGetString(result->m_peer, S3E_GKPEER_DISPLAY_NAME);

    snprintf(message, sizeof(message), "`x66ee66GKSessionConnectionCallback %s %s", pPeerName, result->m_success ? "true" : "false" );

    AppendMessage(message);

    std::set<s3eIOSGameKitPeer*>::iterator iter = gConnectingSet.find(result->m_peer);

    if (iter != gConnectingSet.end())
        gConnectingSet.erase(iter);
}

static void GKSessionPeerAttemptedToConnectCallback(struct s3eIOSGameKitSession* session, s3eIOSGameKitSessionPeerConnectAttempt* connectInfo, void* userData)
{
    const char *pPeerName = s3eIOSGameKitPeerGetString(connectInfo->m_peer, S3E_GKPEER_DISPLAY_NAME);
    char        message[MESSAGE_LEN];

    connectInfo->m_accept = S3E_TRUE;

    snprintf(message, sizeof(message), "`x6666ee'%s' `x66ee66Connected", pPeerName);
    AppendMessage(message);
}

void GKSessionRecieveDataCallback(struct s3eIOSGameKitSession* session, s3eIOSGameKitSessionRecievedData* data, void* userData)
{
    char message[MESSAGE_LEN];
    const char *pPeerName = s3eIOSGameKitPeerGetString(data->m_peer, S3E_GKPEER_DISPLAY_NAME);

    snprintf(message, sizeof(message), "`x66ee66%s: `x666666%s", pPeerName, (char*)data->m_data);
    AppendMessage(message);
}

void GKSessionDisconnectionCallback(struct s3eIOSGameKitSession* session, s3eIOSGameKitSessionDisconnectInfo* info, void* userData)
{
    char message[MESSAGE_LEN];
    if (info->m_peer)
    {
        // A peer disconnected
        const char *pPeerName = s3eIOSGameKitPeerGetString(info->m_peer, S3E_GKPEER_DISPLAY_NAME);
        snprintf(message, sizeof(message), "`x6666ee'%s' `xee6666Disconnected", pPeerName);
    }
    else // An error occured with the session
    {
        if (info->m_error == S3E_IOSGAMEKIT_ERR_BLUETOOTH_DISABLED)
            snprintf(message, sizeof(message), "`x6666eeSession failed: bluetooth is disabled");
        else if (info->m_error == S3E_IOSGAMEKIT_ERR_BLUETOOTH_NOT_SUPPORTED)
            snprintf(message, sizeof(message), "`x6666eeSession failed: bluetooth is unsupported");
        else
            snprintf(message, sizeof(message), "`x6666eeSession failed with error '%d'", info->m_error);

        // Stop the session due to error and reset all the buttons
        s3eIOSGameKitStopSession(session);
        g_GKSession = NULL;
        g_Connected = false;
        g_ButtonServer->m_Enabled = true;
        g_ButtonClient->m_Enabled = true;
        g_ButtonPeer->m_Enabled = true;
    }
    AppendMessage(message);
}

void ExampleInit()
{
    if (!s3eIOSGameKitAvailable())
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "IPhoneGameKit extension not found");
        s3eDeviceExit(1);
    }

    g_ButtonServer    = NewButton("Create Server");
    g_ButtonClient    = NewButton("Create Client");
    g_ButtonPeer      = NewButton("Create Peer");
    g_ButtonSend      = NewButton("Send Message");
    g_ButtonStop      = NewButton("Disconnect");

    InitMessages(NUM_MESSAGES, MESSAGE_LEN);

    AppendMessage("`x6666ee            Welcome to s3e Chat! ");
    AppendMessage("`x6666ee-----------------------------------------------");
}

void ExampleTerm()
{
    if (g_GKSession)
        s3eIOSGameKitStopSession(g_GKSession);
}

bool ExampleUpdate()
{
    g_ButtonSend->m_Enabled = g_Connected;
    g_ButtonStop->m_Enabled = g_Connected;

    Button* pressed = GetSelectedButton();

    if (pressed)
    {
        // Connect as server, client or peer.
        if (pressed == g_ButtonServer || pressed == g_ButtonClient || pressed == g_ButtonPeer)
        {
            char        message[MESSAGE_LEN];
            const char* tokenID = s3eIOSNotificationsGetRemoteNotificationToken();

            if (pressed == g_ButtonServer)
            {
                g_ConnectionType = S3E_GKSESSION_MODE_SERVER;
                AppendMessage("Connecting as server:");
            }
            else if (pressed == g_ButtonClient)
            {
                g_ConnectionType = S3E_GKSESSION_MODE_CLIENT;
                AppendMessage("Connecting as client:");
            }
            else
            {
                g_ConnectionType = S3E_GKSESSION_MODE_PEER;
                AppendMessage("Connecting as peer:");
            }

            snprintf(message,  sizeof(message), "`x66ee66PNS ID '%s'", tokenID ? tokenID : "(null)");
            AppendMessage(message);

            g_GKSession = s3eIOSGameKitStartSession(SESSION_ID_STRING,
                                                       0,
                                                       g_ConnectionType,
                                                       GKSessionConnectionCallback,
                                                       GKSessionPeerAttemptedToConnectCallback,
                                                       GKSessionRecieveDataCallback,
                                                       GKSessionDisconnectionCallback,
                                                       0);
            const char* realUserName = s3eIOSGameKitSessionGetString(g_GKSession, S3E_GKSESSION_DISPLAY_NAME);

            if (realUserName)
                strncpy(gUserName, realUserName, sizeof(gUserName));

            g_Connected = true;
            g_ButtonServer->m_Enabled = false;
            g_ButtonClient->m_Enabled = false;
            g_ButtonPeer->m_Enabled = false;
        }

        // Open text entry pop-up to send message
        else if (pressed == g_ButtonSend)
        {
            if (s3eOSReadStringAvailable())
            {
                const char* usrMsg = s3eOSReadStringUTF8("Type a message");
                char        displayMsg[MESSAGE_LEN];

                snprintf(displayMsg, sizeof(displayMsg), "`xee3333%s: `x666666%s", gUserName, usrMsg);

                AppendMessage(displayMsg);

                // Send message as data to all connected peers
                s3eIOSGameKitSessionSendDataToAllPeers(g_GKSession, usrMsg, strlen(usrMsg) + 1, S3E_GK_SEND_DATA_RELIABLE);
            }
            else
                AppendMessage("`xee6666Error: Device does not support ExtOSReadUserString!");
        }

        // Stop current session
        else if (pressed == g_ButtonStop)
        {
            s3eIOSGameKitStopSession(g_GKSession);
            g_GKSession = NULL;
            g_Connected = false;
            g_ButtonServer->m_Enabled = true;
            g_ButtonClient->m_Enabled = true;
            g_ButtonPeer->m_Enabled = true;
            s3eDebugTracePrintf("app session g_GKSession = %p", g_GKSession);
        }
    }

    // Find peers and connect to any new ones found.
    if (g_Connected)
    {
        s3eIOSGameKitPeer* peers[MAX_PEERS_TO_CONNECT_TO];

        // Find all available peers (devices using the same session ID but not yet connected)
        // Servers cannot search for other peers to connect to (will return 0)
        uint32 num = s3eIOSGameKitSessionGetPeersWithConnectionState(g_GKSession,
                                                             S3E_GKPEER_STATE_AVAILABLE,
                                                             peers,
                                                             MAX_PEERS_TO_CONNECT_TO);

        // Yield substantially to allow OS to perform internal Bluetooth tasks
        s3eDeviceYield(500);

        if (num > MAX_PEERS_TO_CONNECT_TO)
            num = MAX_PEERS_TO_CONNECT_TO;

        // For each peer found, try to connect to it
        for (uint32 i = 0; i < num; i++)
        {
            if (gConnectingSet.find(peers[i]) == gConnectingSet.end())
            {
                char message[MESSAGE_LEN];

                snprintf(message, sizeof(message), "`x66ee66Connecting to server/peer %s", s3eIOSGameKitPeerGetString(peers[i], S3E_GKPEER_DISPLAY_NAME));
                AppendMessage(message);

                gConnectingSet.insert(peers[i]);
                s3eIOSGameKitConnectToPeer(g_GKSession, peers[i], 10);
            }
        }
    }

    return true;
}

void ExampleRender()
{
    int y = GetYBelowButtons();
    int x = 20;

    s3eDebugPrintf(x, y, 1, "`xee3333User = '%s'", gUserName);
    y += 20;

    // Print messages from all peers, newest first
    PrintMessages(x, y);
}
