/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EIOSBackgroundAudio S3E Background Audio Example
 *
 * The following example uses the S3E Background Audio extension to turn on
 * and off the mixing of audio from the application (using s3eSound) with
 * audio from the device (e.g. from as it's music player).
 *
 * It also demonstrates iOS' background audio mode using the
 * iphone-backgroundmode-audio mkb setting. This allows the app itself to
 * play audio while suspended. Note that this mkb option can be used with
 * any app and requires no code changes to use.
 *
 * The main functions used in this example are:
 * - s3eIOSBackgroundAudioAvailable()
 * - s3eIOSBackgroundAudioSetMix()
 * - s3eSoundChannelPlay()
 * - s3eSoundChannelStop()
 * - s3eAudioPlay()
 *
 * s3eSound is used to buffer a simple audio clip from file and play it
 * back in order to demonstrate how this can be mixed with the background
 * audio. On-screen buttons are used to swtich mixing and s3eSound playback
 * on and off. These are implemented in ExamplesMain.cpp. An s3eAudio
 * implementation is also provided to demonstrate how all 3 sound sources
 * can be mixed.
 *
 * The current background audio setting is output to screen using
 * s3eDebugPrint().
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eIOSBackgroundAudioImage.png
 *
 * @include s3eIOSBackgroundAudio.cpp
 */
#include "ExamplesMain.h"
#include <s3eIOSBackgroundAudio.h>
#include <set>

static int16*   g_SoundBuffer = 0;
static int32    g_SoundFileSize = 0;
static int      g_SoundChannel = 0;
static bool     g_SoundIsPlaying = false;
static Button*  g_ButtonMixAudio = 0;
static Button*  g_ButtonPlaySound = 0;
static Button*  g_ButtonPlayAudio = 0;
static Button*  g_ButtonStopAudio = 0;

static s3eAudioStatus g_AudioState = S3E_AUDIO_STOPPED;

void ExampleInit()
{
    if (s3eIOSBackgroundAudioAvailable())
    {
        g_ButtonMixAudio = NewButton(s3eIOSBackgroundAudioGetMix() ? "Disable iPod/s3e audio mixing" : "Enable iPod/s3e audio mixing");

        g_ButtonPlaySound = NewButton("Play s3eSound");
        g_ButtonPlayAudio = NewButton("Play s3eAudio");
        g_ButtonStopAudio = NewButton("Stop s3eAudio");
        g_ButtonStopAudio->m_Display = false;

        // Setup sound buffer to play audio - see s3eSoundBasic example
        s3eFile *fileHandle = s3eFileOpen("test.raw", "rb");
        g_SoundFileSize = s3eFileGetSize(fileHandle);
        g_SoundBuffer = (int16*)s3eMallocBase(g_SoundFileSize);
        memset(g_SoundBuffer, 0, g_SoundFileSize);
        s3eFileRead(g_SoundBuffer, g_SoundFileSize, 1, fileHandle);
        s3eFileClose(fileHandle);
        s3eSoundSetInt(S3E_SOUND_DEFAULT_FREQ, 8000);
    }
}

void ExampleTerm()
{
}

bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    if (pressed && s3eIOSBackgroundAudioAvailable())
    {
        if (pressed == g_ButtonMixAudio)
        {
            // Enable/disable mixing s3eSound with background audio from iPod/MusicPicker
            s3eIOSBackgroundAudioSetMix(!s3eIOSBackgroundAudioGetMix());
            SetButtonName(g_ButtonMixAudio, (s3eIOSBackgroundAudioGetMix() ? "Disable iPod/s3e audio mixing" : "Enable iPod/s3e audio mixing"));
        }

        else if (pressed == g_ButtonPlaySound)
        {
            // Play s3eSound to check if it can be mixed with background audio
            if (!g_SoundIsPlaying)
            {
                s3eSoundChannelPlay(g_SoundChannel, g_SoundBuffer, g_SoundFileSize/2, 0, 0);
                g_SoundIsPlaying = true;
                SetButtonName(g_ButtonPlaySound, "Stop s3eSound");
            }
            else
            {
                s3eSoundChannelStop(g_SoundChannel);
                g_SoundIsPlaying = false;
                SetButtonName(g_ButtonPlaySound, "Play s3eSound");
            }
        }

        else if (pressed == g_ButtonPlayAudio)
        {
            // Play s3eSound to check if it can be mixed with background audio
            if (g_AudioState == S3E_AUDIO_STOPPED)
            {
                if (s3eAudioPlay("test.mp3", 0) == S3E_RESULT_SUCCESS)
                {
                    g_AudioState = S3E_AUDIO_PLAYING;
                    SetButtonName(g_ButtonPlayAudio, "Pause s3eAudio");
                    g_ButtonStopAudio->m_Display = true;
                }
            }
            else if (g_AudioState == S3E_AUDIO_PLAYING)
            {
                if (s3eAudioPause() == S3E_RESULT_SUCCESS)
                {
                    g_AudioState = S3E_AUDIO_PAUSED;
                    SetButtonName(g_ButtonPlayAudio, "Resume s3eAudio");
                }
            }
            else if (g_AudioState == S3E_AUDIO_PAUSED)
            {
                if (s3eAudioResume() == S3E_RESULT_SUCCESS)
                {
                    g_AudioState = S3E_AUDIO_PLAYING;
                    SetButtonName(g_ButtonPlayAudio, "Pause s3eAudio");
                }
            }
        }

        else if (pressed == g_ButtonStopAudio)
        {
            s3eAudioStop();
            g_ButtonStopAudio->m_Display = false;
            g_AudioState = S3E_AUDIO_STOPPED;
            SetButtonName(g_ButtonPlayAudio, "Play s3eAudio");
        }
    }

    return true;
}

void ExampleRender()
{
    int y = GetYBelowButtons();
    int x = 20;
    int textSpacing = s3eDebugGetInt(S3E_DEBUG_FONT_HEIGHT) + 5;

    if (s3eIOSBackgroundAudioAvailable())
    {
        if (s3eIOSBackgroundAudioGetMix())
            s3eDebugPrintf(x, y, 1, "`x666666Mixing of background (iPod) & s3e audio/sound enabled");
        else
            s3eDebugPrintf(x, y, 1, "`x666666Mixing of background (iPod) & s3e audio/sound disabled");

        y += textSpacing*2;

        if (g_SoundIsPlaying)
            s3eDebugPrintf(x, y, 1, "`x666666s3eSound is playing");
        else
            s3eDebugPrintf(x, y, 1, "`x666666s3eSound is stopped");

        y += textSpacing;

        if (g_AudioState == S3E_AUDIO_PLAYING)
            s3eDebugPrintf(x, y, 1, "`x666666s3eAudio is playing");
        else if (g_AudioState == S3E_AUDIO_PAUSED)
            s3eDebugPrintf(x, y, 1, "`x666666s3eAudio is paused");
        else
            s3eDebugPrintf(x, y, 1, "`x666666s3eAudio is stopped");
    }
    else
    {
        s3eDebugPrint(0, 100, "`xee3333BackgroundAudio extension unavailable", 1);
    }
}
