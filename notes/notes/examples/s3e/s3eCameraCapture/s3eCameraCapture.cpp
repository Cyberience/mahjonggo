/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3ECameraCapture S3E Camera Capture Example
 *
 * The following example demonstrates S3E's Camera Capture functionality.
 *
 * The main functions used to achieve this are:
 * - s3eCameraCaptureIsFormatSupported()
 * - s3eCameraCaptureToFile()
 * - s3eCameraCaptureToBuffer()
 *
 * The example allows the user to launch the native camera application
 * which returns the captured image or video to the app as a data buffer
 * or a file path.
 *
 * Note that returning captured data by buffer is not currently supported
 * on all platforms.
 *
 * File paths are returned with the raw:// prefix,
 * followed by their full system path, rather than the normal local
 * path relative to the app's data folder. These raw paths can be passed to
 * S3E file APIs and will be resolved transparently.
 *
 * The example uses S3E Video to play/show videos and JPG images. JPGs are
 * shown in this way to keep the example code simple rather than pulling in
 * non-S3E functionality. Note that using s3eVideoPlay with a JPG is not
 * guaranteed to scale to the sizes given so you may only see the top left
 * corner of the returned image.
 *
 * @include s3eCameraCapture.cpp
 */

#include "ExamplesMain.h"
#include "s3eCameraCapture.h"
#include "s3eImagePicker.h"

static Button* g_ButtonCaptureImgToFile = NULL;
static Button* g_ButtonCaptureImgToBuffer = NULL;
static Button* g_ButtonCaptureVideoToFile = NULL;
static Button* g_ButtonCaptureVideoToBuffer = NULL;
static Button* g_ButtonFreeBuffer = NULL;

static Button* g_ButtonSaveBuffer = NULL;
static Button* g_ButtonSaveFile = NULL;

static Button* g_ButtonShowLastFileCapture = NULL;
static Button* g_ButtonShowLastBufferCapture = NULL;
static bool g_PlayingFile = false;

static char g_Filepath[256] = {0};
static s3eCameraCaptureResult* g_BufferedCapture = 0;
static bool g_FileIsVideo = false;
static bool g_JPGSupport = false;
static bool g_VideoSupport = false;
static bool g_CaptureToBufferSupport = false;

void ExampleInit()
{
    g_ButtonCaptureImgToFile = NewButton("Capture image to file");
    g_ButtonCaptureImgToBuffer = NewButton("Capture image to buffer");
    g_ButtonCaptureVideoToFile = NewButton("Capture video to file");
    g_ButtonCaptureVideoToBuffer = NewButton("Capture video to buffer");
    g_ButtonShowLastFileCapture = NewButton("Play last image/video from file");
    g_ButtonShowLastBufferCapture = NewButton("Play last image/video from buffer");
    g_ButtonFreeBuffer = NewButton("Free capture buffer");

    g_ButtonCaptureImgToFile->m_Enabled = false;
    g_ButtonCaptureImgToBuffer->m_Enabled = false;
    g_ButtonCaptureVideoToFile->m_Enabled = false;
    g_ButtonCaptureVideoToBuffer->m_Enabled = false;
    g_ButtonShowLastFileCapture->m_Enabled = false;
    g_ButtonShowLastBufferCapture->m_Enabled = false;
    g_ButtonFreeBuffer->m_Enabled = false;

    int32 deviceOS = s3eDeviceGetInt(S3E_DEVICE_OS);
    g_CaptureToBufferSupport = (deviceOS == S3E_OS_ID_IPHONE) ||
                               (deviceOS == S3E_OS_ID_NACL)   ||
                               (deviceOS == S3E_OS_ID_WIN10)  ||
                               (deviceOS == S3E_OS_ID_WP8)    ||
                               (deviceOS == S3E_OS_ID_WP81)   ||
                               (deviceOS == S3E_OS_ID_WS8)    ||
                               (deviceOS == S3E_OS_ID_WS81);
    if (deviceOS == S3E_OS_ID_IPHONE)
    {
        g_ButtonSaveBuffer = NewButton("Save buffer to gallery");
        g_ButtonSaveFile = NewButton("Save last file to gallery");
    }

    InitMessages(10, 128);

    //Check whether the CameraCapture extension is available
    if (s3eCameraCaptureAvailable())
    {
        if (!s3eCameraCaptureIsFormatSupported(S3E_CAMERACAPTURE_FORMAT_JPG))
        {
            AppendMessageColour(RED, "JPG image format not supported on this device");
        }
        else
        {
            g_JPGSupport = true;
        }

        if (!s3eCameraCaptureIsFormatSupported(S3E_CAMERACAPTURE_FORMAT_VIDEO))
        {
            AppendMessageColour(RED, "Video format not supported on this device");
        }
        else
        {
            g_VideoSupport = true;
        }

        if (!g_CaptureToBufferSupport)
        {
            AppendMessageColour(RED, "Capture to buffer not supported on this device");
        }
    }
    else
    {
        AppendMessageColour(RED, "CameraCapture extension not available");
    }
}

void ExampleTerm()
{
    if (g_BufferedCapture)
        s3eCameraCaptureRelease(g_BufferedCapture);
}

int32 TouchToStop(void* sysData, void* userData)
{
     g_PlayingFile = false;

    return 0;
}

void ItemSaved(s3eImagePickerResult* image, s3eImagePickerError* error)
{
    if (*error != S3E_IMAGEPICKER_ERR_NONE)
        AppendMessageColour(RED, "Failed to save item to gallery");
    else
        AppendMessageColour(GREEN, "Completed saving item to gallery");
}

bool ExampleUpdate()
{
    if (g_JPGSupport)
        g_ButtonCaptureImgToFile->m_Enabled = true;
    if (g_VideoSupport)
        g_ButtonCaptureVideoToFile->m_Enabled = true;

    g_ButtonCaptureImgToBuffer->m_Enabled = g_CaptureToBufferSupport && g_JPGSupport && !g_BufferedCapture;
    g_ButtonCaptureVideoToBuffer->m_Enabled = g_CaptureToBufferSupport && g_VideoSupport && !g_BufferedCapture;
    g_ButtonShowLastFileCapture->m_Enabled = g_Filepath[0] != 0;
    g_ButtonShowLastBufferCapture->m_Enabled = g_BufferedCapture != 0;
    g_ButtonFreeBuffer->m_Enabled = g_BufferedCapture != 0;

    if (g_ButtonSaveBuffer)
    {
        g_ButtonSaveFile->m_Enabled = g_Filepath[0] != 0;
        g_ButtonSaveBuffer->m_Enabled = g_BufferedCapture != 0;
    }

    Button* pressed = GetSelectedButton();
    if (!pressed)
        return true;

    if (pressed == g_ButtonCaptureImgToFile)
    {
        memset(g_Filepath, 0, sizeof(g_Filepath));

        if (s3eCameraCaptureToFile(g_Filepath, 256, S3E_CAMERACAPTURE_FORMAT_JPG) == S3E_RESULT_ERROR)
        {
            g_Filepath[0] = 0;
            s3eCameraCaptureError err = s3eCameraCaptureGetError();
            AppendMessageColour(RED, "Failed to capture JPG image to file - error %d", err);
        }
        else
        {
            g_FileIsVideo = false;
            AppendMessageColour(GREEN, "Captured JPG image to file: %s", g_Filepath);
        }
    }

    if (pressed == g_ButtonCaptureImgToBuffer)
    {
        g_BufferedCapture = s3eCameraCaptureToBuffer(S3E_CAMERACAPTURE_FORMAT_JPG);
        if (!g_BufferedCapture)
        {
            s3eCameraCaptureError err = s3eCameraCaptureGetError();
            AppendMessageColour(RED, "Failed to capture JPG image to buffer - error %d", err);
        }
        else
        {
            AppendMessageColour(GREEN, "Captured JPG image to buffer");
        }
    }

    if (pressed == g_ButtonCaptureVideoToFile)
    {
        memset(g_Filepath, 0, sizeof(g_Filepath));

        if (s3eCameraCaptureToFile(g_Filepath, 256, S3E_CAMERACAPTURE_FORMAT_VIDEO) == S3E_RESULT_ERROR)
        {
            g_Filepath[0] = 0;
            s3eCameraCaptureError err = s3eCameraCaptureGetError();
            AppendMessageColour(RED, "Failed to capture video to file - error %d", err);
        }
        else
        {
            g_FileIsVideo = true;
            AppendMessageColour(GREEN, "Captured video to file: %s", g_Filepath);
        }
    }

    if (pressed == g_ButtonCaptureVideoToBuffer)
    {
        g_BufferedCapture = s3eCameraCaptureToBuffer(S3E_CAMERACAPTURE_FORMAT_VIDEO);
        if (!g_BufferedCapture)
        {
            s3eCameraCaptureError err = s3eCameraCaptureGetError();
            AppendMessageColour(RED, "Failed to capture video to buffer - error %d", err);
        }
        else
        {
            AppendMessageColour(GREEN, "Captured Video to buffer");
        }
    }

    if (pressed == g_ButtonShowLastFileCapture || pressed == g_ButtonShowLastBufferCapture)
    {
        s3eResult err;

        bool isFile = pressed == g_ButtonShowLastFileCapture;
        bool isVideo = isFile ? g_FileIsVideo : (g_BufferedCapture->m_Format == S3E_CAMERACAPTURE_FORMAT_VIDEO ? true : false);

        if (pressed == g_ButtonShowLastFileCapture)
            err = s3eVideoPlay(g_Filepath, 1);
        else
            err = s3eVideoPlayFromBuffer(g_BufferedCapture->m_Buffer, g_BufferedCapture->m_BufferSize, 1);

        if (err == S3E_RESULT_ERROR)
        {
            s3eVideoError videoErr = s3eVideoGetError();
            if (videoErr == S3E_VIDEO_ERR_UNSUPPORTED)
                AppendMessageColour(RED, "Playing %s from %s not supported on this device/platform", isVideo ? "video" : "JPG", isFile ? "file" : "buffer");
            else
                AppendMessageColour(RED, "Failed to play %s from %s - error %d", isVideo ? "video" : "JPG", isFile ? "file" : "buffer", videoErr);
            return true;
        }

        g_PlayingFile = true;

        s3ePointerRegister(S3E_POINTER_BUTTON_EVENT, TouchToStop, NULL);

        // Wait until any user event.
        do
        {
            s3eKeyboardUpdate();
            s3ePointerUpdate();
            s3eDeviceYield(100);
            s3eSurfaceShow();
        }
        while (g_PlayingFile && !ExampleCheckQuit() && !s3eKeyboardAnyKey());

        // Clear input buffers and reset touch state if TouchToStop not already called
        s3eKeyboardUpdate();
        s3ePointerUpdate();

        s3eKeyboardUpdate();
        s3ePointerUpdate();
        s3eVideoStop();

        s3ePointerUnRegister(S3E_POINTER_BUTTON_EVENT, TouchToStop);
    }

    if (pressed == g_ButtonFreeBuffer)
    {
        s3eCameraCaptureRelease(g_BufferedCapture);
        g_BufferedCapture = 0;
    }

    if (pressed == g_ButtonSaveFile || pressed == g_ButtonSaveBuffer)
    {
        // This example only picks files so assume buffer is a file path
        AppendMessage("Saving buffered image to gallery");

        s3eImagePickerResult picRes;

        if (pressed == g_ButtonSaveBuffer)
        {
            picRes.m_Buffer = g_BufferedCapture->m_Buffer;
            picRes.m_BufferSize = g_BufferedCapture->m_BufferSize;
            picRes.m_BufferIsFilePath = false;

            switch (g_BufferedCapture->m_Format)
            {
                case S3E_CAMERACAPTURE_FORMAT_VIDEO:
                    picRes.m_Format = S3E_IMAGEPICKER_FORMAT_ANYVIDEO;
                    break;
                case S3E_CAMERACAPTURE_FORMAT_JPG:
                    picRes.m_Format = S3E_IMAGEPICKER_FORMAT_JPG;
                    break;
                case S3E_CAMERACAPTURE_FORMAT_PNG:
                    picRes.m_Format = S3E_IMAGEPICKER_FORMAT_PNG;
                    break;
                case S3E_CAMERACAPTURE_FORMAT_BMP:
                    picRes.m_Format = S3E_IMAGEPICKER_FORMAT_BMP;
                    break;
                default:
                    AppendMessage("Picked image type unknown, trying ANYIMAGE");
                    picRes.m_Format = S3E_IMAGEPICKER_FORMAT_ANYIMAGE;
                    break;
            }
        }
        else // using file path
        {
            picRes.m_Buffer = g_Filepath;
            picRes.m_BufferSize = strlen(g_Filepath)+1;
            picRes.m_BufferIsFilePath = true;

            if (g_FileIsVideo)
            {
                picRes.m_Format = S3E_IMAGEPICKER_FORMAT_ANYVIDEO;
            }
            else
            {
                picRes.m_Format = S3E_IMAGEPICKER_FORMAT_ANYIMAGE;
            }
        }

        if (s3eImagePickerSaveToGallery(&picRes, ItemSaved) == S3E_RESULT_SUCCESS)
        {
            AppendMessageColour(GREEN, "Saving to gallery...");
        }
        else
        {
            AppendMessageColour(RED, "Failed to start saving to gallery");
        }
    }

    return true;
}

/*
 * The ExampleRender function outputs a set of strings indicating the result of
 * A camera capture.
 */
void ExampleRender()
{
    int x = 20;
    int y = GetYBelowButtons();

    PrintMessages(x, y);
}
