/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EFlurryAppSpot S3E Flurry AppSpot example
 *
 * The following is an example of how to use the s3eFlurryAppSpot Extension,
 * requesting advertisements, registering for callbacks and processing responses.
 *
 * The main functions demonstrated in this example are:
 *
 * - s3eFlurryAppSpotInitialize()
 * - s3eFlurryAppSpotRegister()
 * - s3eFlurryAppSpotFetchAndDisplayAdForSpace()
 * - s3eFlurryAppSpotSetDisplayAdView()
 * - s3eFlurryAppSpotRemoveAdFromSpace()
 * - s3eFlurryAppSpotUnRegister()
 *
 * @include s3eFlurryAppSpot.cpp
 */

#include "s3e.h"
#include "s3eFlurryAppSpot.h"
#include <memory.h>
#include <string.h>
#include <stdio.h>
#include "ExamplesMain.h"

//Visit the Flurry developer site to get keys for your applications and spaces.
//If you need more information how to use FlurryAds follow to https://developer.yahoo.com/flurry/docs/
#define FLURRY_API_KEY_IOS  "ENTER_IOS_API_KEY"
#define INTERSTITIAL_SPACE_IOS  "ENTER_IOS_BANNER_SPACE"
#define TOP_BANNER_SPACE_IOS  "ENTER_IOS_BANNER_SPACE"
#define BOTTOM_BANNER_SPACE_IOS  "ENTER_IOS_BANNER_SPACE"

#define FLURRY_API_KEY_ANDROID "ENTER_ANDROID_API_KEY"
#define INTERSTITIAL_SPACE_ANDROID "ENTER_ANDROID_BANNER_SPACE"
#define TOP_BANNER_SPACE_ANDROID "ENTER_ANDROID_BANNER_SPACE"
#define BOTTOM_BANNER_SPACE_ANDROID "ENTER_ANDROID_BANNER_SPACE"

//Simple structure to track touches
struct CTouch
{
    int32 x; // position x
    int32 y; // position y
    bool active; // is touch active (currently down)
    int32 id; // touch's unique identifier
};

bool callback_received = false;
bool g_UseMultiTouch = false;
char g_TouchEventMsg[128] = {0};
char g_AltMsg[128] = {0};
#define MAX_TOUCHES 10

bool exit = false;
int bannerY = 0;

Button* showFeaturedAppBtn = 0;
Button* showBannerTopAppBtn = 0;
Button* showBannerBottomAppBtn = 0;
Button* hideBannerAdBtn = 0;
Button* exitBtn = 0;

const char* g_InterstitialSpace="";
const char* g_TopBannerSpace="";
const char* g_BottomBannerSpace="";

bool topBannerShown = false;
bool bottomBannerShown = false;

bool isPointInButton(s3ePointerEvent* event, Button* button)
{
    if (event->m_x < button->m_XPos)
        return false;

    if (event->m_x > button->m_XPos + button->m_Width)
        return false;

    if (event->m_y < button->m_YPos)
        return false;

    if (event->m_y > button->m_YPos + button->m_Height)
        return false;

    return true;
}

void SingleTouchButtonCB(s3ePointerEvent* event)
{
    // Don't register press events, actions will occur on release.
    if (event->m_Pressed)
    {
        return;
    }

    if (isPointInButton(event, showFeaturedAppBtn))
    {
        s3eDebugTracePrintf("showFeaturedApp");
        s3eFlurryAppSpotFetchAndDisplayAdForSpace(g_InterstitialSpace, S3E_FLURRYAPPSPOT_FULLSCREEN);
        SetButtonName(showFeaturedAppBtn, "Loading interstitial...");
        showFeaturedAppBtn->m_Enabled = false;
    }

    if (isPointInButton(event, showBannerTopAppBtn))
    {
        if(topBannerShown)
        {
            s3eDebugTracePrintf("removeBannerTopAppBtn");
            s3eFlurryAppSpotRemoveAdFromSpace(g_TopBannerSpace);
            topBannerShown = false;
            SetButtonName(showBannerTopAppBtn, "Show top banner");
        }
        else
        {
            s3eDebugTracePrintf("showBannerTopAppBtn");
            //banners can be moved via offset from left top edge of the screen
            //All values just for example
            s3eFlurryAppSpotSetDisplayAdView(10, 20, 500, 70);
            //fetch ad and display it asynchronously
            s3eFlurryAppSpotFetchAdForSpace(g_TopBannerSpace, S3E_FLURRYAPPSPOT_BANNER_TOP);
            SetButtonName(showBannerTopAppBtn, "Loading top banner...");
            showBannerTopAppBtn->m_Enabled = false;
        }
   }

    if (isPointInButton(event, showBannerBottomAppBtn))
    {
        if(bottomBannerShown)
        {
            s3eDebugTracePrintf("removeBannerBottomAppBtn");
            s3eFlurryAppSpotRemoveAdFromSpace(g_BottomBannerSpace);
            bottomBannerShown = false;
            SetButtonName(showBannerBottomAppBtn, "Show bottom banner");
        }
        else
        {
            s3eDebugTracePrintf("showBannerBottomAppBtn");
            //fetch and display ad in one command
            s3eFlurryAppSpotFetchAndDisplayAdForSpace(g_BottomBannerSpace, S3E_FLURRYAPPSPOT_BANNER_BOTTOM);
            SetButtonName(showBannerBottomAppBtn, "Loading bottom banner...");
            showBannerBottomAppBtn->m_Enabled = false;
        }
    }

    if (isPointInButton(event, exitBtn))
    {
        exit = true;
    }
}

int32 adClosed_Callback(void* system, void* user)
{
    s3eDebugOutputString("Marmalade-FlurryAdClosed_Callback");
    return 0;
}

int32 adSpaceReceived_Callback(void* system, void* user)
{
    if(!strcmp((char*)system, g_InterstitialSpace))
    {
        SetButtonName(showFeaturedAppBtn, "Show interstitial");
        showFeaturedAppBtn->m_Enabled = true;
    }
    if(!strcmp((char*)system, g_TopBannerSpace))
    {
        //display ad right after fetch or later
        s3eFlurryAppSpotDisplayAdForSpace(g_TopBannerSpace);
        SetButtonName(showBannerTopAppBtn, "Remove top banner");
        showBannerTopAppBtn->m_Enabled = true;
        topBannerShown = true;
    }
    if(!strcmp((char*)system, g_BottomBannerSpace))
    {
        SetButtonName(showBannerBottomAppBtn, "Remove bottom banner");
        showBannerBottomAppBtn->m_Enabled = true;
        bottomBannerShown = true;
    }
    s3eDebugOutputString("Marmalade-FlurryAdSpaceReceived_Callback");
    return 0;
}

int32 applicationExit_Callback(void* system, void* user)
{
    s3eDebugOutputString("Marmalade-FlurryApplicationExit_Callback");
    return 0;
}

int32 adSpaceReceivedFail_Callback(void* system, void* user)
{
    if(!strcmp((char*)system, g_InterstitialSpace))
    {
        SetButtonName(showFeaturedAppBtn, "Interstitial fetch failed! Try again!");
        showFeaturedAppBtn->m_Enabled = true;
    }
    if(!strcmp((char*)system, g_TopBannerSpace))
    {
        SetButtonName(showBannerTopAppBtn, "Top banner fetch failed! Try again!");
        showBannerTopAppBtn->m_Enabled = true;
        topBannerShown = false;
    }
    if(!strcmp((char*)system, g_BottomBannerSpace))
    {
        SetButtonName(showBannerBottomAppBtn, "Bottom banner fetch failed! Try again!");
        showBannerBottomAppBtn->m_Enabled = true;
        bottomBannerShown = false;
    }

    s3eDebugOutputString("Marmalade-FlurryAdSpaceReceivedFail_Callback");
    return 0;
}

int32 adClicked_Callback(void* system, void* user)
{
    s3eDebugOutputString("Marmalade-FlurryAdClicked_Callback");
    return 0;
}

int32 adOpened_Callback(void* system, void* user)
{
    s3eDebugOutputString("Marmalade-FlurryAdOpened_Callback");
    return 0;
}

int32 adVideoCompleted_Callback(void* system, void* user)
{
    s3eDebugOutputString("Marmalade-FlurryAdVideoCompleted_Callback");
    return 0;
}

int32 adRenderFail_Callback(void* system, void* user)
{
    s3eDebugOutputString("Marmalade-FlurryAdRenderFail_Callback");
    return 0;
}

void SingleTouchMotionCB(s3ePointerMotionEvent* event)
{

}

void notAvaibleLock(const char* text)
{
    sprintf(g_TouchEventMsg, text);
    SetButtonName(showFeaturedAppBtn, text);
    showFeaturedAppBtn->m_Enabled = false;
    SetButtonName(showBannerTopAppBtn, text);
    showBannerTopAppBtn->m_Enabled = false;
    SetButtonName(showBannerBottomAppBtn, text);
    showBannerBottomAppBtn->m_Enabled = false;
}

//--------------------------------------------------------------------------
void ExampleInit()
{
    sprintf(g_TouchEventMsg, "Registering Flurry...");

    //Register for standard pointer events
    s3ePointerRegister(S3E_POINTER_BUTTON_EVENT, (s3eCallback)SingleTouchButtonCB, NULL);
    s3ePointerRegister(S3E_POINTER_MOTION_EVENT, (s3eCallback)SingleTouchMotionCB, NULL);

    s3eFlurryAppSpotRegister(S3E_FLURRYAPPSPOT_CALLBACK_ADCLOSED, (s3eCallback)adClosed_Callback, NULL);
    s3eFlurryAppSpotRegister(S3E_FLURRYAPPSPOT_CALLBACK_ADSPACE_RECEIVED, (s3eCallback)adSpaceReceived_Callback, NULL);
    s3eFlurryAppSpotRegister(S3E_FLURRYAPPSPOT_CALLBACK_APPLICATION_EXIT, (s3eCallback)applicationExit_Callback, NULL);
    s3eFlurryAppSpotRegister(S3E_FLURRYAPPSPOT_CALLBACK_ADSPACE_RECEIVED_FAIL, (s3eCallback)adSpaceReceivedFail_Callback, NULL);
    s3eFlurryAppSpotRegister(S3E_FLURRYAPPSPOT_CALLBACK_ADCLICKED, (s3eCallback)adClicked_Callback, NULL);
    s3eFlurryAppSpotRegister(S3E_FLURRYAPPSPOT_CALLBACK_ADOPENED, (s3eCallback)adOpened_Callback, NULL);
    s3eFlurryAppSpotRegister(S3E_FLURRYAPPSPOT_CALLBACK_ADVIDEOCOMPLETED, (s3eCallback)adVideoCompleted_Callback, NULL);
    s3eFlurryAppSpotRegister(S3E_FLURRYAPPSPOT_CALLBACK_ADRENDERFAIL, (s3eCallback)adRenderFail_Callback, NULL);

    // Init buttons.
    setButtonsBasicY(200);
    showFeaturedAppBtn = NewButton("Show interstitial");
    showBannerTopAppBtn = NewButton("Show top banner");
    showBannerBottomAppBtn = NewButton("Show bottom banner");
    //hideBannerAdBtn = NewButton("Hide Banner Ad");
    exitBtn = NewButton("Exit");
    SetButtonScale(3);

    switch(s3eDeviceGetInt(S3E_DEVICE_OS))
    {
    case S3E_OS_ID_IPHONE:
        if (strstr(FLURRY_API_KEY_IOS, "ENTER_"))
        {
            notAvaibleLock("You forget to enter your iOS credentials!");
            return;
        }
        s3eFlurryAppSpotInitialize(FLURRY_API_KEY_IOS );
        g_InterstitialSpace = INTERSTITIAL_SPACE_IOS ;
        g_TopBannerSpace = TOP_BANNER_SPACE_IOS ;
        g_BottomBannerSpace = BOTTOM_BANNER_SPACE_IOS ;
        break;
    case S3E_OS_ID_ANDROID:
        if (strstr(FLURRY_API_KEY_ANDROID, "ENTER_"))
        {
            notAvaibleLock("You forget to enter your Android credentials!");
            return;
        }
        s3eFlurryAppSpotInitialize(FLURRY_API_KEY_ANDROID);
        g_InterstitialSpace = INTERSTITIAL_SPACE_ANDROID;
        g_TopBannerSpace = TOP_BANNER_SPACE_ANDROID;
        g_BottomBannerSpace = BOTTOM_BANNER_SPACE_ANDROID;
        break;
    default:
            notAvaibleLock("FlurryAds is not avaible on this platform!");
        break;
    }

    //you can use test mode or set up options for Flurry ads. For more information visit documentation on Flurry website
    bool IwantToTest = true;
    if(IwantToTest == true)
        s3eFlurryAppSpotTestAds(true);
    else
    {
        //those options will affect all fetched ads.
        //If you need different options - use s3eFlurryAppSpotClearOptions() to reset this one and fill them before next fetch
        //All values just for example
        s3eFlurryAppSpotSetAge(30);
        s3eFlurryAppSpotSetLocation(48.464717,35.046181);
        s3eFlurryAppSpotAddOption("keywords","gender","male");
        s3eFlurryAppSpotAddOption("keywords","subscriber","0");
        s3eFlurryAppSpotAddOption("cookies","app_id", "game_x");
    }
}


//--------------------------------------------------------------------------
void ExampleTerm()
{
    s3ePointerUnRegister(S3E_POINTER_BUTTON_EVENT, (s3eCallback)SingleTouchButtonCB);
    s3ePointerUnRegister(S3E_POINTER_MOTION_EVENT, (s3eCallback)SingleTouchMotionCB);

    s3eFlurryAppSpotUnRegister(S3E_FLURRYAPPSPOT_CALLBACK_ADCLOSED, (s3eCallback)adClosed_Callback);
    s3eFlurryAppSpotUnRegister(S3E_FLURRYAPPSPOT_CALLBACK_ADSPACE_RECEIVED, (s3eCallback)adSpaceReceived_Callback);
    s3eFlurryAppSpotUnRegister(S3E_FLURRYAPPSPOT_CALLBACK_APPLICATION_EXIT, (s3eCallback)applicationExit_Callback);
    s3eFlurryAppSpotUnRegister(S3E_FLURRYAPPSPOT_CALLBACK_ADSPACE_RECEIVED_FAIL, (s3eCallback)adSpaceReceivedFail_Callback);
    s3eFlurryAppSpotUnRegister(S3E_FLURRYAPPSPOT_CALLBACK_ADCLICKED, (s3eCallback)adClicked_Callback);
    s3eFlurryAppSpotUnRegister(S3E_FLURRYAPPSPOT_CALLBACK_ADOPENED, (s3eCallback)adOpened_Callback);
    s3eFlurryAppSpotUnRegister(S3E_FLURRYAPPSPOT_CALLBACK_ADVIDEOCOMPLETED, (s3eCallback)adVideoCompleted_Callback);
    s3eFlurryAppSpotUnRegister(S3E_FLURRYAPPSPOT_CALLBACK_ADRENDERFAIL, (s3eCallback)adRenderFail_Callback);

    DeleteButtons();
}


//--------------------------------------------------------------------------
bool ExampleUpdate()
{
    s3ePointerRegister(S3E_POINTER_BUTTON_EVENT, (s3eCallback)SingleTouchButtonCB, NULL);
    s3ePointerRegister(S3E_POINTER_MOTION_EVENT, (s3eCallback)SingleTouchMotionCB, NULL);

    if (exit)
        return false;
    else
        return true;
}

void ExampleRender()
{
    // Get pointer to the screen surface
    // (pixel depth is 2 bytes by default)
    uint16* screen = (uint16*)s3eSurfacePtr();
    int height = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT);
    int width = s3eSurfaceGetInt(S3E_SURFACE_WIDTH);
    int pitch = s3eSurfaceGetInt(S3E_SURFACE_PITCH);

    // Clear screen to white
    for (int i=0; i < height; i++)
    {
        memset((char*)screen + pitch * i, 255, (width * 2));
    }

    // This was causing an error to pop up.
    s3ePointerUpdate();

    s3eDebugPrint(20, 140, g_TouchEventMsg, 1);
    s3eDebugPrint(20, 165, g_AltMsg, 1);

    ButtonsRender();
}
