/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EAdDuplex S3E Ad Duplex
 *
 * The following example demonstrates S3E's Ad Duplex functionality.
 *
 * The main functions used to achieve this are:
 *
 * @include s3eAdDuplex.cpp
 */
#include "ExamplesMain.h"
#include "s3eAdDuplex.h"
#include "s3eDebug.h"

#include "IwImage.h"

#include <sys/param.h>

// Control buttons
static Button* g_CreateButton = 0;
static Button* g_CreateExtButton = 0;
static Button* g_DestroyButton = 0;
static Button* g_ShowButton = 0;
static Button* g_HideButton = 0;

static Button* g_LeftButton = 0;
static Button* g_RightButton = 0;
static Button* g_CenterButton = 0;
static Button* g_TestButton = 0;
static Button* g_ChangeSizeButton = 0;

s3eAdDuplexHandle g_Handle = NULL;

int g_Size = 0;

static int32 OnAdCreatedFunc(void* systemData, void* userData)
{
    s3eAdDuplexHandle Handle = reinterpret_cast<s3eAdDuplexHandle>(systemData);
    g_Handle = Handle;
    g_DestroyButton->m_Enabled = true;
    g_ShowButton->m_Enabled = true;
    g_HideButton->m_Enabled = true;
    g_TestButton->m_Enabled = true;
    if (g_ChangeSizeButton)
        g_ChangeSizeButton->m_Enabled = true;

    s3eAdDuplexPropertySet(g_Handle, S3E_AD_DUPLEX_SIZE, S3E_AD_DUPLEX_SIZE_250X125);

    AppendMessageColour(BLUE, "On Ad created event.");
    return 0;
}

static int32 OnAdClickedFunc(void* systemData, void* userData)
{
    AppendMessageColour(BLUE, "On Ad click event.");
    return 0;
}

static int32 OnAdLoadedFunc(void* systemData, void* userData)
{
    AppendMessageColour(BLUE, "On Ad loaded event.");
    return 0;
}

static int32 OnAdLoadedErrorFunc(void* systemData, void* userData)
{
    s3eAdDuplexLoadError* pErrorInfo = reinterpret_cast<s3eAdDuplexLoadError*>(systemData);

    char szBuf[1024] = {'\0'};
    snprintf(szBuf, sizeof(szBuf), "On Ad load error: HRES: %i, Text: %s", pErrorInfo->nHResult, pErrorInfo->szErrorText);
    szBuf[sizeof(szBuf) - 1] = '\0';
    AppendMessageColour(RED, szBuf);

    return 0;
}

static int32 OnCloseClickedFunc(void* systemData, void* userData)
{
    //see duplicate in ExampleUpdate
    g_ShowButton->m_Enabled = true;
    g_HideButton->m_Enabled = false;
    g_LeftButton->m_Enabled = false;
    g_RightButton->m_Enabled = false;
    g_CenterButton->m_Enabled = false;
    if (g_ChangeSizeButton)
        g_ChangeSizeButton->m_Enabled = false;

    AppendMessageColour(BLUE, "On Close click event.");
    return 0;
}

void ExampleInit()
{
    InitMessages(10, 128);
    bool enabled = false;

    g_CreateButton = NewButton("Create AdDuplex.");
    g_CreateButton->m_Enabled = true;
    if (s3eDeviceGetInt(S3E_DEVICE_OS) != S3E_OS_ID_WP8 &&
        s3eDeviceGetInt(S3E_DEVICE_OS) != S3E_OS_ID_WS8
       )
    {
        g_CreateExtButton = NewButton("Create Ext AdDuplex.");
        g_CreateExtButton->m_Enabled = true;
    }
    g_DestroyButton = NewButton("Destroy AdDuplex.");
    g_DestroyButton->m_Enabled = false;
    g_ShowButton = NewButton("Show AdDuplex.");
    g_ShowButton->m_Enabled = false;
    g_HideButton = NewButton("Hide AdDuplex.");
    g_HideButton->m_Enabled = false;
    g_LeftButton = NewButton("Show AdDuplex in left bottom corner.");
    g_LeftButton->m_Enabled = false;
    g_RightButton = NewButton("Show AdDuplex in right top corner.");
    g_RightButton->m_Enabled = false;
    g_CenterButton = NewButton("Show AdDuplex in center.");
    g_CenterButton->m_Enabled = false;
    g_TestButton = NewButton("Set property 'Test'.");
    g_TestButton->m_Enabled = false;
    if (s3eDeviceGetInt(S3E_DEVICE_OS) != S3E_OS_ID_WP8 &&
        s3eDeviceGetInt(S3E_DEVICE_OS) != S3E_OS_ID_WP81
       )
    {
        g_ChangeSizeButton = NewButton("Change size.");
        g_ChangeSizeButton->m_Enabled = false;
    }

    s3eAdDuplexRegister(S3E_ADDUPLEX_CALLBACK_ON_LOAD_ERROR, OnAdLoadedErrorFunc, NULL);
    s3eAdDuplexRegister(S3E_ADDUPLEX_CALLBACK_ON_CLICK, OnAdClickedFunc, NULL);
    s3eAdDuplexRegister(S3E_ADDUPLEX_CALLBACK_ON_LOADED, OnAdLoadedFunc, NULL);
    s3eAdDuplexRegister(S3E_ADDUPLEX_CALLBACK_ON_CREATED, OnAdCreatedFunc, NULL);
    s3eAdDuplexRegister(S3E_ADDUPLEX_CALLBACK_ON_CLOSE, OnCloseClickedFunc, NULL);
}

void ExampleTerm()
{
}

bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();
    if (pressed == NULL)
    {
        return true;
    }

    if (pressed == g_CreateButton)
    {
        g_CreateButton->m_Enabled = false;
        if (g_CreateExtButton)
            g_CreateExtButton->m_Enabled = false;

        s3eAdDuplexCreate("0"); // !!! Set your id there. !!!
    }
    else if (g_CreateExtButton && pressed == g_CreateExtButton)
    {
        g_CreateButton->m_Enabled = false;
        g_CreateExtButton->m_Enabled = false;

        s3eAdDuplexCreateExt("0", "0", true); // !!! Set your id there. !!!
    }
    else if (pressed == g_DestroyButton)
    {
        g_CreateButton->m_Enabled = true;
        if (g_CreateExtButton)
            g_CreateExtButton->m_Enabled = true;
        g_DestroyButton->m_Enabled = false;
        g_ShowButton->m_Enabled = false;
        g_HideButton->m_Enabled = false;
        g_LeftButton->m_Enabled = false;
        g_RightButton->m_Enabled = false;
        g_CenterButton->m_Enabled = false;
        g_TestButton->m_Enabled = false;
        if (g_ChangeSizeButton)
            g_ChangeSizeButton->m_Enabled = false;
        s3eAdDuplexDestroy(g_Handle);
    }
    else if (pressed == g_ShowButton)
    {
        g_ShowButton->m_Enabled = false;
        g_HideButton->m_Enabled = true;
        g_LeftButton->m_Enabled = true;
        g_RightButton->m_Enabled = true;
        g_CenterButton->m_Enabled = true;
        if (g_ChangeSizeButton)
            g_ChangeSizeButton->m_Enabled = true;
        s3eAdDuplexShow(g_Handle);
    }
    else if (pressed == g_HideButton)
    {
        //see duplicate in OnCloseClickedFunc
        g_ShowButton->m_Enabled = true;
        g_HideButton->m_Enabled = false;
        g_LeftButton->m_Enabled = false;
        g_RightButton->m_Enabled = false;
        g_CenterButton->m_Enabled = false;
        if (g_ChangeSizeButton)
            g_ChangeSizeButton->m_Enabled = false;

        s3eAdDuplexHide(g_Handle);
    }
    else if (pressed == g_LeftButton)
    {
        s3eAdDuplexPropertySet(g_Handle, S3E_AD_DUPLEX_H_ALIGN, S3E_AD_DUPLEX_HALIGN_LEFT);
        s3eAdDuplexPropertySet(g_Handle, S3E_AD_DUPLEX_V_ALIGN, S3E_AD_DUPLEX_VALIGN_BOTTOM);
    }
    else if (pressed == g_RightButton)
    {
        s3eAdDuplexPropertySet(g_Handle, S3E_AD_DUPLEX_H_ALIGN, S3E_AD_DUPLEX_HALIGN_RIGHT);
        s3eAdDuplexPropertySet(g_Handle, S3E_AD_DUPLEX_V_ALIGN, S3E_AD_DUPLEX_VALIGN_TOP);
    }
    else if (pressed == g_CenterButton)
    {
        s3eAdDuplexPropertySet(g_Handle, S3E_AD_DUPLEX_H_ALIGN, S3E_AD_DUPLEX_HALIGN_CENTER);
        s3eAdDuplexPropertySet(g_Handle, S3E_AD_DUPLEX_V_ALIGN, S3E_AD_DUPLEX_VALIGN_CENTER);
    }
    else if (pressed == g_TestButton)
    {
        s3eAdDuplexPropertySet(g_Handle, S3E_AD_DUPLEX_IS_TEST, (s3eAdDuplexPropertyGet(g_Handle, S3E_AD_DUPLEX_IS_TEST) + 1) % 2);
        if (s3eAdDuplexPropertyGet(g_Handle, S3E_AD_DUPLEX_IS_TEST) == 1)
        {
            AppendMessageColour(BLUE, "Test mode has turned on.");
        }
        else
        {
            AppendMessageColour(BLUE, "Test mode has turned off.");
        }
    }
    else if (g_ChangeSizeButton && pressed == g_ChangeSizeButton)
    {
        ++g_Size;
        if (g_Size > S3E_AD_DUPLEX_SIZE_728X90)
        {
            g_Size = 0;
        }
        s3eAdDuplexPropertySet(g_Handle, S3E_AD_DUPLEX_SIZE, (s3eAdDuplexSize)g_Size);
    }

    return true;
}

/*
 * The ExampleRender function outputs a set of strings indicating the result
 * image picker.
 */
void ExampleRender()
{
    int x = 20;
    int y = GetYBelowButtons();

    PrintMessages(x, y);
}
