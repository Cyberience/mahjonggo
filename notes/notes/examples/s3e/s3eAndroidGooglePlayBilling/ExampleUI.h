/*
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This document is protected by copyright, and contains information
 * proprietary to Marmalade.
 *
 * This file consists of source code released by Marmalade under
 * the terms of the accompanying End User License Agreement (EULA).
 * Please do not use this program/source code before you have read the
 * EULA and have agreed to be bound by its terms.
 */

#ifndef MARMALADENUI_H
#define MARMALADENUI_H

#define WHISPER_BLOB // comment out to use Multi File WhisperSync - don't mix the two

using namespace std;
using namespace IwNUI;

// GooglePlayBilling static responses which can be tested without submitting
// More info: https://developer.android.com/google/play/billing/billing_testing.html?hl=ru#billing-testing-static
// First will buy the product once, other will return errors
static const char *inAppStaticResponses[] =
{
    "android.test.purchased",
    "android.test.canceled",
    "android.test.refunded",
    "android.test.item_unavailable"
};

class ExampleUI
{
public:
    ExampleUI();
    void Update();
    void SetStatusText(const string &msg);
    void SetAnimatingText(const string &msg);
    void SetConsumableText(const string &msg);
    void Log(const string &msg);

    void EnableAllButtons(bool enable);
private:
    CAppPtr app;
    CViewPtr view;
    CLabelPtr statusText;
    CLabelPtr animatingText;
    CLabelPtr consumableText;
    CListBoxPtr logText;
    CStringArray listBoxItems;

    std::vector<CButtonPtr> buttons;
};

// helper function
extern std::string string_format(const std::string fmt, ...);

// NUI takes C style callbacks to handle events
extern bool OnButton1Click(void* data, CButton* button);
extern bool OnButton2Click(void* data, CButton* button);
extern bool OnButton3Click(void* data, CButton* button);
extern bool OnButton4Click(void* data, CButton* button);
extern bool OnStaticResponseButtonClick(void* data, CButton* button);
extern bool OnStaticConsumeButtonClick(void* data, CButton* button);

#endif // defined MARMALADENUI_H
