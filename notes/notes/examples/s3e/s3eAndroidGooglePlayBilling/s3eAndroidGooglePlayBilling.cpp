/*
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This document is protected by copyright, and contains information
 * proprietary to Marmalade.
 *
 * This file consists of source code released by Marmalade under
 * the terms of the accompanying End User License Agreement (EULA).
 * Please do not use this program/source code before you have read the
 * EULA and have agreed to be bound by its terms.
 */
/**
 * @page ExampleS3EAndroidGooglePlayBilling S3E Google Play In App Billing Example
 *
 * This Application demonstrates usage of the Android Google Play v3 Billing Marmalade
 * extension on the Android platform. The application is intended to be functional
 * and demonstrate how to use the extension code without the distraction of complex UI.
 * This example illustrates the use of the In App Billing functionality on the
 * Android platform, allowing items, products or services to be bought via
 * Google Play. It connects to requests information for a product,allows the user to
 * purchase the product and then displays the receipt for the transaction. It also shows
 * how to consume a consumable product so that it can be purchased again in the future.
 *
 * For more information on the general topic of Android In App Billing see
 * http://developer.android.com/google/play/billing/api.html
 *
 * Note that the API only handles purchasing of the product, not delivery of the
 * actual service/data that was paid for. It also does not provide a means for
 * querying and listing available products for the app (since Google Play and
 * Android SDK do not provide this functionality). Typically, an application will
 * require its own external server to perform these extra tasks. The
 * order data returned when a purchase is completed should be sent to the
 * application's server to provide the information it requires to check the
 * validity of the purchase and provide any required data to the app.
 * Providing data via a server is beyond the scope of this example.
 *
 * See S3E Android Google Play Billing overview in the Marmalade API Reference for
 * more information.
 *
 * The main functions demonstrated in this example are:
 * - s3eAndroidGooglePlayBillingStart()
 * - s3eAndroidGooglePlayBillingRegister()
 * - s3eAndroidGooglePlayBillingIsSupported()
 * - s3eAndroidGooglePlayBillingRequestProductInformation()
 * - s3eAndroidGooglePlayBillingRestoreTransactions()
 * - s3eAndroidGooglePlayBillingRequestPurchase
 * - s3eAndroidGooglePlayBillingConsumeItem()
 *
 * Note that the extension is explicitly initialized and terminated unlike
 * some other extensions.
 *
 * @note Google has changed the process of Google Play Billing testing.
 * "Draft" applications cannot be used anymore for purchase testing purposes.
 * Instead you have to submit the app to Google Play at least to Alpha channel.
 * However Google allows you to verify most cases with static responses without application publishing. They are:
 *  - "android.test.purchased"
 *  - "android.test.canceled"
 *  - "android.test.refunded"
 *  - "android.test.item_unavailable"
 * More detailed information can be found here: https://developer.android.com/google/play/billing/billing_testing.html
 *
 * @include s3eAndroidGooglePlayBilling.cpp
 */

#include "s3eAndroidGooglePlayBilling.h"
#include "IwDebug.h"
#include "s3e.h"
#include "IwNUI.h"
#include "IwRandom.h"

#include "ExampleUI.h"
//#include "UnitTests.h"

//UnitTests *gTests;

// Button click handlers for the UI
// Add you own SKU's
const char *inAppSkus[] =
{
    "product1",
    "product2",
    "product3",
};

// Add you own SKU's
const char *subSkus[] =
{
    "subscription1",
    "subscription2"
};

// Query Shop
bool OnButton1Click(void* data, CButton* button)
{
    ExampleUI *ui = (ExampleUI*)data;
    ui->Log("Query Shop Items");
    s3eAndroidGooglePlayBillingRequestProductInformation(inAppSkus,sizeof(inAppSkus)/sizeof(const char*),subSkus,sizeof(subSkus)/sizeof(const char*));
    return true;
}

// Restore Purchases
bool OnButton2Click(void* data, CButton* button)
{
    ExampleUI *ui = (ExampleUI*)data;
    ui->Log("Attempting to Restore Purchases");
    s3eAndroidGooglePlayBillingRestoreTransactions();

    return true;
}

// Purchase product
bool OnButton3Click(void* data, CButton* button)
{
    ExampleUI *ui = (ExampleUI*)data;
    ui->Log("Attempting to purchase product");
    string randomPayload = string_format("TestPayload%d",IwRandMinMax(1,10000)); // let's be clear this is a test - in your code either don't set it or use something sensible you can check later on a different device
    s3eAndroidGooglePlayBillingRequestPurchase(inAppSkus[0],true,randomPayload.c_str());
    //s3eAndroidGooglePlayBillingRequestPurchase(subSkus[1],false,randomPayload.c_str());

    return true;
}

string hundredCoinsID;
string staticRequestID;

// Consume product
bool OnButton4Click(void* data, CButton* button)
{
    ExampleUI *ui = (ExampleUI*)data;
    ui->Log("Attempting to consume product");
    if (hundredCoinsID.length() == 0)
        ui->Log("Error: no item to consume, try Restore if you restarted the Example app");
    else
        s3eAndroidGooglePlayBillingConsumeItem(hundredCoinsID.c_str());

    return true;
}

// Purchase product from static response list
bool OnStaticResponseButtonClick(void* data, CButton* button)
{
    ExampleUI *ui = (ExampleUI*)data;
    CString atr_val;
    button->GetAttribute("name", atr_val);
    const char* btnName = atr_val.Get();
    for (size_t index = 0; index < sizeof(inAppStaticResponses)/sizeof(const char*); ++index)
    {
        if (!strcmp(btnName, inAppStaticResponses[index]))
        {
            ui->Log(string_format("Static response for %s",inAppStaticResponses[index]));
            s3eAndroidGooglePlayBillingRequestPurchase(inAppStaticResponses[index],true,"");
            return true;
        }
    }
    ui->Log(string_format("There is no such static responce: %s", btnName));
    return false;
}

// Consume product from static response list
bool OnStaticConsumeButtonClick(void* data, CButton* button)
{
    ExampleUI *ui = (ExampleUI*)data;
    if (staticRequestID.length())
    {
        s3eAndroidGooglePlayBillingConsumeItem(staticRequestID.c_str());
        staticRequestID.clear();
    }
    else
        ui->Log(string_format("There is nothing to consume!"));
    return false;
}

int32 ListCallback(void *systemData,void *userData)
{
    if ((systemData) && (userData))
    {
        ExampleUI* ui = (ExampleUI*)userData; // this is a pointer passed through from when the callback was registered
        s3eAndroidGooglePlayBillingSkuResponse *skus = (s3eAndroidGooglePlayBillingSkuResponse*)systemData;
        string str;
        if (skus->m_ErrorMsg) {
            str = string_format("List Sku returned : %d, %s", (int)skus->m_Status, skus->m_ErrorMsg);
            ui->Log(str);
        }
        if (skus->m_Status == S3E_ANDROIDGOOGLEPLAYBILLING_RESULT_OK)
        {
            str = string_format("%d items returned",skus->m_NumProducts);
            ui->Log(str);
            for (int i=0;i<skus->m_NumProducts;i++)
            {
                ui->Log("{");
                s3eAndroidGooglePlayBillingItemInfo *item = &skus->m_Products[i];
                ui->Log(string_format(" m_ProductID     : %s",item->m_ProductID));
                ui->Log(string_format(" m_Type          : %s",item->m_Type));
                ui->Log(string_format(" m_Price         : %s",item->m_Price));
                ui->Log(string_format(" m_Title         : %s",item->m_Title));
                ui->Log(string_format(" m_Description   : %s",item->m_Description));
                ui->Log(string_format(" m_PriceMicros   : %s",item->m_PriceMicros));
                ui->Log("}");
            }
        }
    }
    return true;
}

void LogAndSavePurchaseInfo(ExampleUI* ui, s3eAndroidGooglePlayBillingPurchase *item)
{
    ui->Log(string_format(" m_OrderID           : %s",item->m_OrderID));
    ui->Log(string_format(" m_PackageID         : %s",item->m_PackageID));
    ui->Log(string_format(" m_ProductId         : %s",item->m_ProductId));
    ui->Log(string_format(" m_PurchaseTime      : %lu",item->m_PurchaseTime));
    ui->Log(string_format(" m_PurchaseState     : %d",item->m_PurchaseState));
    ui->Log(string_format(" m_PurchaseToken     : %s",item->m_PurchaseToken));
    ui->Log(string_format(" m_DeveloperPayload  : %s",item->m_DeveloperPayload));
    ui->Log(string_format(" m_JSON              : %s",item->m_JSON));
    ui->Log(string_format(" m_Signature         : %s",item->m_Signature));

    if (!strcmp(item->m_ProductId,inAppSkus[0]))
        hundredCoinsID = item->m_PurchaseToken;
    if (!strcmp(item->m_ProductId,inAppStaticResponses[0]))
        staticRequestID = item->m_PurchaseToken;
    ui->SetConsumableText(item->m_PurchaseToken);
}

int32 RestoreCallback(void *systemData,void *userData)
{
    if ((systemData) && (userData))
    {
        ExampleUI* ui = (ExampleUI*)userData; // this is a pointer passed through from when the callback was registered
        s3eAndroidGooglePlayBillingRestoreResponse *rr = (s3eAndroidGooglePlayBillingRestoreResponse*)systemData;
        string str;
        if (rr->m_ErrorMsg) {
            str = string_format("Restore returned : %d, %s", (int)rr->m_Status, rr->m_ErrorMsg);
            ui->Log(str);
        }
        if (rr->m_Status == S3E_ANDROIDGOOGLEPLAYBILLING_RESULT_OK)
        {
            str = string_format("%d items returned",rr->m_NumPurchases);
            ui->Log(str);
            for (int i=0;i<rr->m_NumPurchases;i++)
            {
                ui->Log("{");
                s3eAndroidGooglePlayBillingPurchase *item = &rr->m_Purchases[i];
                LogAndSavePurchaseInfo(ui, item);
                ui->Log("}");
            }
        }
    }
    return true;
}

int32 PurchaseCallback(void *systemData,void *userData)
{
    if ((systemData) && (userData))
    {
        ExampleUI* ui = (ExampleUI*)userData; // this is a pointer passed through from when the callback was registered
        s3eAndroidGooglePlayBillingPurchaseResponse *pr = (s3eAndroidGooglePlayBillingPurchaseResponse*)systemData;
        string str;
        if (pr->m_ErrorMsg) {
            str = string_format("Purchase returned : %d, %s", (int)pr->m_Status, pr->m_ErrorMsg);
            ui->Log(str);
        }
        if (pr->m_Status == S3E_ANDROIDGOOGLEPLAYBILLING_RESULT_OK)
        {
            s3eAndroidGooglePlayBillingPurchase *item = pr->m_PurchaseDetails;
            LogAndSavePurchaseInfo(ui, item);
        }
    }
    return true;
}

int32 ConsumeCallback(void *systemData,void *userData)
{
    if ((systemData) && (userData))
    {
        ExampleUI* ui = (ExampleUI*)userData; // this is a pointer passed through from when the callback was registered
        s3eAndroidGooglePlayBillingConsumeResponse *cr = (s3eAndroidGooglePlayBillingConsumeResponse*)systemData;
        string str;
        if (cr->m_ErrorMsg) {
            str = string_format("Purchase returned : %d, %s", (int)cr->m_Status, cr->m_ErrorMsg);
            ui->Log(str);
        }
        if (cr->m_Status == S3E_ANDROIDGOOGLEPLAYBILLING_RESULT_OK)
            ui->SetConsumableText("None");
    }
    return true;
}

// note this is the public license key provided by Google, not the one you sign this app with, it's in the developer console under Services & APIs

const char *publicKey = "Put your public key here / if you dont do this / payments will succeed but the extension will still report them as having failed / also this string needs to be Base64 clean";

static bool g_Initialized = false;

int main()
{
    char animatingText[] = "... Some Animating Text ...";
    uint64 animatingTextTimer;

    // seed RNG
    int32 ms = (int32)s3eTimerGetMs();
    IwRandSeed(ms);

    // create our Marmalade UI interface
    ExampleUI *ui = new ExampleUI();
    ui->Log("main()");
    //ui->EnableAllButtons(false);

    // Attempt to start up the Store interface
    s3eAndroidGooglePlayBillingStart(publicKey);

    // register callbacks and pass in our UI pointer which the callback
    s3eAndroidGooglePlayBillingRegister(S3E_ANDROIDGOOGLEPLAYBILLING_LIST_PRODUCTS_CALLBACK,ListCallback,ui);
    s3eAndroidGooglePlayBillingRegister(S3E_ANDROIDGOOGLEPLAYBILLING_RESTORE_CALLBACK,RestoreCallback,ui);
    s3eAndroidGooglePlayBillingRegister(S3E_ANDROIDGOOGLEPLAYBILLING_PURCHASE_CALLBACK,PurchaseCallback,ui);
    s3eAndroidGooglePlayBillingRegister(S3E_ANDROIDGOOGLEPLAYBILLING_CONSUME_CALLBACK,ConsumeCallback,ui);

    // create the Unit Test singleton
    //gTests = new UnitTests(ui); // DH: Not implemented for this extension yet

    animatingTextTimer = s3eTimerGetMs();

    // run the app
    while (1)
    {
        //gTests->Update(); // update the tests if they're running

        //s3eAndroidGooglePlayBillingIsSupported()

        // animate the text
        if (s3eTimerGetMs() > animatingTextTimer + 20)
        {
            int len = strlen(animatingText);
            char c = animatingText[0];
            memmove(animatingText,animatingText+1,len-1);
            animatingText[len-1] = c;
            ui->SetAnimatingText(animatingText);
            animatingTextTimer = s3eTimerGetMs();
        }

        // Initialization is async, so we need to do this
        if (g_Initialized != (s3eAndroidGooglePlayBillingIsSupported() == S3E_RESULT_SUCCESS))
        {
            g_Initialized = s3eAndroidGooglePlayBillingIsSupported() == S3E_RESULT_SUCCESS;
            ui->SetStatusText(g_Initialized ? "Initialized" : "Not initialized");
        }

        ui->Update(); // update the UI
        s3eDeviceYield();
    }

    return 0;
}
