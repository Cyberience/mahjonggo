/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EBackgroundMusic S3E Background Music Example
 *
 * The following example uses the S3E Background Music extension to launch
 * the external music picker interface for the selection and queueing-up of
 * tracks. Playback of the list of tracks selected can then be controlled
 * using on-screen buttons. The same functionality found in the Background
 * Audio example (s3eIOSBackgroundAudio) is also reproduced here to demonstrate
 * interaction between music from the music picker and s3eSound. See the
 * Background Audio example for more information.
 *
 * The Background Music extension is currently only supported for devices
 * running on iOS v4.2 and greater and Windows Phone 8.
 *
 * The main functions used from Background Music are:
 * - s3eBackgroundMusicGetInt()
 * - s3eBackgroundMusicPickMusicItems()
 * - s3eBackgroundMusicPlay()
 * - s3eBackgroundMusicPause()
 * - s3eBackgroundMusicStop()
 * - s3eBackgroundMusicGetCurrentItem()
 *
 * On-screen buttons are used to control the music playback and music picker.
 * These are implemented in ExamplesMain.cpp.
 *
 * The state of playback is output to screen using the s3eDebugPrint()
 * function.
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eBackgroundMusicImage.png
 *
 * @include s3eBackgroundMusic.cpp
 */

#include "ExamplesMain.h"
#include <s3eBackgroundMusic.h>
#include <s3eIOSBackgroundAudio.h>
#include <set>

static int16*   g_SoundBuffer = 0;
static int32    g_SoundFileSize = 0;
static int      g_SoundChannel = 0;
static bool     g_SoundIsPlaying = false;

static Button*  g_ButtonOpenMusicPicker = 0;
static Button*  g_ButtonPlay = 0;
static Button*  g_ButtonPause = 0;
static Button*  g_ButtonStop = 0;
static Button*  g_ButtonMixAudio = 0;
static Button*  g_ButtonPlaySound = 0;
static Button*  g_ButtonPlayAudio = 0;

static const char* g_Status = NULL;

void ExampleInit()
{
    if (s3eBackgroundMusicAvailable())
    {
        g_ButtonOpenMusicPicker = NewButton("Open Music Picker");
        g_ButtonOpenMusicPicker->m_Enabled = s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_IPHONE;
        g_ButtonPlay = NewButton("Start Playback");
        g_ButtonPause = NewButton("Pause Playback");
        g_ButtonStop = NewButton("Stop Playback");
        if (s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WP8)
        {
            s3eBackgroundMusicShowPauseBackgroundMediaDialog();
        }
    }

    if (s3eIOSBackgroundAudioAvailable())
    {
        g_ButtonMixAudio = NewButton("iPod/s3e audio mixing");

        // Setup sound buffer to play audio - see s3eSoundBasic example
        s3eFile* fileHandle = s3eFileOpen("test.raw", "rb");
        g_SoundFileSize = s3eFileGetSize(fileHandle);
        g_SoundBuffer = (int16*)s3eMallocBase(g_SoundFileSize);
        memset(g_SoundBuffer, 0, g_SoundFileSize);
        s3eFileRead(g_SoundBuffer, g_SoundFileSize, 1, fileHandle);
        s3eFileClose(fileHandle);
        s3eSoundSetInt(S3E_SOUND_DEFAULT_FREQ, 8000);
    }

    g_ButtonPlaySound = NewButton("Play s3eSound");
    g_ButtonPlayAudio = NewButton("Play s3eAudio");
}

void ExampleTerm()
{
    s3eBackgroundMusicTerminate();
}

bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    // track playback controls
    if (s3eBackgroundMusicAvailable())
    {
        int playBackState = s3eBackgroundMusicGetInt(S3E_BACKGROUNDMUSIC_PLAYBACK_STATE);
        g_ButtonPlay->m_Enabled  = playBackState != S3E_BACKGROUNDMUSIC_PLAYBACK_PLAYING;
        g_ButtonPause->m_Enabled = playBackState != S3E_BACKGROUNDMUSIC_PLAYBACK_PAUSED;
        g_ButtonStop->m_Enabled  = playBackState != S3E_BACKGROUNDMUSIC_PLAYBACK_STOPPED;

        if (pressed)
        {
            if (pressed == g_ButtonOpenMusicPicker)
            {
                if (s3eBackgroundMusicPickMusicItems(S3E_TRUE, g_ButtonOpenMusicPicker->m_XPos, g_ButtonOpenMusicPicker->m_YPos, g_ButtonOpenMusicPicker->m_Width, g_ButtonOpenMusicPicker->m_Height) == S3E_RESULT_SUCCESS)
                    g_Status = "`x66ee66Pick Music: Success";
                else
                {
                    if (s3eBackgroundMusicGetError() == S3E_BACKGROUNDMUSIC_ERR_CANCELLED)
                        g_Status = "`xee6666Pick Music: User cancelled picking items";
                    else
                        g_Status = "`xee6666Pick Music: Failed";
                }
            }

            else if (pressed == g_ButtonPlay)
            {
                s3eBackgroundMusicPlay();
            }

            else if (pressed == g_ButtonPause)
            {
                s3eBackgroundMusicPause();
            }

            else if (pressed == g_ButtonStop)
            {
                s3eBackgroundMusicStop();
            }
        }
    }

    if (s3eIOSBackgroundAudioAvailable())
    {
        if (pressed)
        {
            if (pressed == g_ButtonMixAudio)
            {
                // Enable/disable mixing s3eSound with background audio from iPod/MusicPicker
                s3eIOSBackgroundAudioSetMix(!s3eIOSBackgroundAudioGetMix());
            }
        }
    }

    if (pressed == g_ButtonPlaySound)
    {
        // Play s3eSound to check if it can be mixed with background audio
        if (!g_SoundIsPlaying)
        {
            s3eSoundChannelPlay(g_SoundChannel, g_SoundBuffer, g_SoundFileSize/2, 0, 0);
            g_SoundIsPlaying = true;
            SetButtonName(g_ButtonPlaySound, "Stop s3eSound");
        }
        else
        {
            s3eSoundChannelStop(g_SoundChannel);
            g_SoundIsPlaying = false;
            SetButtonName(g_ButtonPlaySound, "Play s3eSound");
        }
    }

    if (pressed == g_ButtonPlayAudio)
    {
        // Play s3eSound to check if it can be mixed with background audio
        if (s3eAudioGetInt(S3E_AUDIO_STATUS) != S3E_AUDIO_PLAYING)
        {
            s3eAudioPlay("test.mp3", 0);
            SetButtonName(g_ButtonPlayAudio, "Stop s3eAudio");
        }
        else
        {
            s3eAudioStop();
            SetButtonName(g_ButtonPlayAudio, "Play s3eAudio");
        }
    }

    return true;
}

void ExampleRender()
{
    int y = GetYBelowButtons();
    int x = 20;
    int textSpacing = s3eDebugGetInt(S3E_DEBUG_FONT_HEIGHT);

    if (s3eIOSBackgroundAudioAvailable())
    {
        if (s3eIOSBackgroundAudioGetMix())
            s3eDebugPrintf(x, y, 1, "`x666666Mixing of background (iPod) & s3e audio enabled");
        else
            s3eDebugPrintf(x, y, 1, "`x666666Mixing of background (iPod) & s3e audio disabled");

        y += textSpacing;

        if (g_SoundIsPlaying)
            s3eDebugPrintf(x, y, 1, "`x666666s3eSound is playing");
        else
            s3eDebugPrintf(x, y, 1, "`x666666s3eSound is stopped");
    }
    else
    {
        s3eDebugPrint(0, y, "`xee3333IOSBackgroundAudio extension unavailable", 1);
    }

    y += textSpacing * 2;

    if (s3eBackgroundMusicAvailable())
    {
        switch (s3eBackgroundMusicGetInt(S3E_BACKGROUNDMUSIC_PLAYBACK_STATE))
        {
            case S3E_BACKGROUNDMUSIC_PLAYBACK_STOPPED:
                s3eDebugPrintf(x, y, 1, "`x666666Background Music Stopped");
                break;

            case S3E_BACKGROUNDMUSIC_PLAYBACK_PLAYING:
                s3eDebugPrintf(x, y, 1, "`x666666Background Music Playing");
                break;

            case S3E_BACKGROUNDMUSIC_PLAYBACK_PAUSED:
                s3eDebugPrintf(x, y, 1, "`x666666Background Music Paused");
                break;

            case S3E_BACKGROUNDMUSIC_PLAYBACK_INTERRUPTED:
                s3eDebugPrintf(x, y, 1, "`x666666Background Music Interrupted");
                break;

            case S3E_BACKGROUNDMUSIC_PLAYBACK_SEEKING_FORWARD:
                s3eDebugPrintf(x, y, 1, "`x666666Background Music Seeking Forward");
                break;

            case S3E_BACKGROUNDMUSIC_PLAYBACK_SEEKING_BACKWARD:
                s3eDebugPrintf(x, y, 1, "`x666666Background Music Seeking Backward");
                break;
        }

        y += textSpacing * 2;

        s3eBackgroundMusicItem* item = s3eBackgroundMusicGetCurrentItem();

        if (item)
        {
            const char* trackName = s3eBackgroundMusicItemGetString(item, S3E_BACKGROUNDMUSICITEM_TITLE);
            s3eDebugPrintf(x, y, 1, "`x333333iPod Track = '%s'", trackName ? trackName : "?");
        }
    }
    else
    {
        s3eDebugPrint(0, y, "`xee3333BackgroundMusic extension unavailable", 1);
    }

    y += textSpacing*2;

    if (g_Status)
        s3eDebugPrint(x, y, g_Status, 1);
}
