/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EVibra S3E Vibra Example
 * The following example causes the device to vibrate using S3E's vibra
 * functionality.
 *
 * The main functions used to achieve this are:
 *  - s3eKeyboardAnyKey()
 *  - s3ePointerGetState()
 *  - s3eVibraVibrate()
 *  - s3eVibraStop()
 *
 * The application waits for the user to press any key or press Toggle button
 * and then causes the device to start/stop it's vibration motor.
 *
 * Instructions to change the vibration state are output to screen using the
 * s3eDebugPrint() function.
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eVibraImage.png
 *
 * @include s3eVibra.cpp
 */
#include "ExamplesMain.h"

static bool g_VibraAvailable;
static bool g_Vibrate = false;
static Button *g_EnableButton;
static Button *g_DisableButton;
static Button *g_ToggleButton;

void ExampleInit()
{
    g_VibraAvailable = (s3eVibraGetInt(S3E_VIBRA_AVAILABLE) != 0);
    if (!g_VibraAvailable)
        return;
    g_EnableButton = NewButton("Enable vibra");
    g_DisableButton = NewButton("Disable vibra");
    g_ToggleButton = NewButton("Toggle");

    bool enabled = s3eVibraGetInt(S3E_VIBRA_ENABLED) ? true : false;
    g_ToggleButton->m_Enabled = enabled;
    g_EnableButton->m_Enabled = enabled;
}

void ExampleTerm()
{
    s3eVibraStop();
}

/*
 * If a button has been pressed, or the screen touched for devices which
 * support a touch screen interface, then start/stop vibration.
 */
bool ExampleUpdate()
{
    // If vibra is not available on device
    // we have nothing to do
    if (!g_VibraAvailable)
        return true;

    Button* pressed = GetSelectedButton();

    if (pressed == g_EnableButton)
    {
        s3eVibraSetInt(S3E_VIBRA_ENABLED, true);
        g_EnableButton->m_Enabled = false;
        g_DisableButton->m_Enabled = true;
        g_Vibrate = false;
        return true;
    }

    if (pressed == g_DisableButton)
    {
        s3eVibraSetInt(S3E_VIBRA_ENABLED, false);
        g_EnableButton->m_Enabled = true;
        g_DisableButton->m_Enabled = false;
        return true;
    }

    if (pressed == g_ToggleButton || s3eKeyboardAnyKey())
    {
        g_Vibrate = !g_Vibrate;
        if (g_Vibrate)
            s3eVibraVibrate(255, 10000);
        else
            s3eVibraStop();
    }
    return true;
}

/*
 * The following function outputs a set of strings. A toggle is used to
 * output a string indicating the status. Each string is output using the
 * s3eDebugPrint() function.
 */
void ExampleRender()
{
    int x = 20;
    int y = GetYBelowButtons();

    if (!g_VibraAvailable)
    {
        s3eDebugPrint(x, y, "`xa0a0a2s3eVibra is not available on this device", 1);
        return;
    }

    if (!s3eVibraGetInt(S3E_VIBRA_ENABLED))
    {
        s3eDebugPrint(x, y, "`xa0a0a2s3eVibra is currently disabled", 1);
        return;
    }

    if (g_DeviceHasKeyboard)
    {
        if (g_Vibrate)
        {
            s3eDebugPrint(x, y, "`xa0a0a2Press any key or Toggle button to stop vibration", 1);
        }
        else
        {
            s3eDebugPrint(x, y, "`xa0a0a2Press any key or Toggle button to start vibration", 1);
        }
    }
    else
    {
        if (g_Vibrate)
        {
            s3eDebugPrint(x, y, "`xa0a0a2Press Toggle button to stop vibration", 1);
        }
        else
        {
            s3eDebugPrint(x, y, "`xa0a0a2Press Toggle button to start vibration", 1);
        }
    }
}
