#version 310 es

in highp vec4 position;
out mediump vec2 texCoord;

void main()
{
    gl_Position = position;
	texCoord = position.xy*0.5f + 0.5f;
}