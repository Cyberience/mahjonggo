/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EGLES2 S3E OpenGL ES 2.0 Example
 * The following example demonstrates OpenGL ES 2.0.
 *
 * The core of this example is based on an example from the maemo
 * project and was updated by the guys at webos-internals:
 * http://www.webos-internals.org/wiki/OpenGL_Triangle_of_Doom
 *
 * @include s3eGLES2.cpp
 */

#include <stdlib.h>
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <string>

#include <GLES2/gl2.h>
#include <EGL/egl.h>

#include "s3e.h"

static EGLSurface g_EGLSurface = NULL;
static EGLDisplay g_EGLDisplay = NULL;
static EGLContext g_EGLContext = NULL;

#define MAX_CONFIG 32

#define BINARY_FORMAT 0x93B0

bool g_glIsInitialized = false;
static char* g_vertexSrc = NULL;
static char* g_fragmentSrc = NULL;

void printConfig(int num, EGLConfig cfg)
{
    int type[32] = {EGL_RED_SIZE, EGL_GREEN_SIZE, EGL_BLUE_SIZE, EGL_ALPHA_SIZE, EGL_BUFFER_SIZE, //r g b a n
        EGL_DEPTH_SIZE, EGL_STENCIL_SIZE, EGL_LUMINANCE_SIZE, EGL_ALPHA_MASK_SIZE, //d s l am
        EGL_SAMPLE_BUFFERS, EGL_SAMPLES, //sb s
        EGL_COLOR_BUFFER_TYPE, EGL_RENDERABLE_TYPE, EGL_SURFACE_TYPE, //cbt rt st
        EGL_BIND_TO_TEXTURE_RGB, EGL_BIND_TO_TEXTURE_RGBA, //bt3 bt4
        EGL_CONFIG_CAVEAT, EGL_CONFIG_ID, EGL_CONFORMANT, EGL_LEVEL, //cc cid c l
        EGL_MAX_PBUFFER_WIDTH, EGL_MAX_PBUFFER_HEIGHT, EGL_MAX_PBUFFER_PIXELS, //mpw mph mpp
        EGL_MAX_SWAP_INTERVAL, EGL_MIN_SWAP_INTERVAL, //>si <si
        EGL_NATIVE_RENDERABLE, EGL_NATIVE_VISUAL_ID, EGL_NATIVE_VISUAL_TYPE, //nr nid nvt
        EGL_TRANSPARENT_TYPE, EGL_TRANSPARENT_RED_VALUE, EGL_TRANSPARENT_GREEN_VALUE, EGL_TRANSPARENT_BLUE_VALUE}; //tt tr tg tb
    const char* id[32] = {"r", "g", "b", "a", "n", "d", "s", "l", "am",
        "sb", "s", "cbt", "rt", "st", "bt3", "bt4", "cc", "cid", "c", "l", "mpw", "mph", "mpp",
        ">si", "<si", "nr", "nid", "nvt", "tt", "tr", "tg", "tb"};
    const char format[] = "cccci" "icii" "ci" "exx" "bb" "eixi" "iii" "ii" "bix" "eccc";
    int enumValue[] = {EGL_SLOW_CONFIG, EGL_NON_CONFORMANT_CONFIG, EGL_TRANSPARENT_RGB, EGL_RGB_BUFFER, EGL_LUMINANCE_BUFFER, EGL_NONE};
    const char* enumid[] = {"s", "n", "t", "r", "l"};
    const char* surface[] = {"p", "x", "w", "", "l", "a", "", "m", "s", NULL};//st
    const char* conform[] = {"1", "v", "2", "d", NULL};//c

    std::string configs;
    for (int i=0; i<32; i++)
    {
        int value = 0;
        char config[256];
        eglGetConfigAttrib(g_EGLDisplay, cfg, type[i], &value);
        if (type[i] == EGL_SURFACE_TYPE)
        {
            std::string eid;
            for (int j=0; surface[j] != NULL; j++)
                if ((value & (1<<j)) == (1<<j))
                    eid.append(surface[j]);
            sprintf(config, " %s:%s", id[i], eid.c_str());
        } else if (type[i] == EGL_CONFORMANT || type[i] == EGL_RENDERABLE_TYPE)
        {
            std::string eid;
            for (int j=0; conform[j] != NULL; j++)
                if ((value & (1<<j)) == (1<<j))
                    eid.append(conform[j]);
                else if (conform[j][0] != 0)
                    eid.append("-");
            sprintf(config, " %s:%s", id[i], eid.c_str());
        } else if (format[i] == 'b')
            sprintf(config, " %s:%s", id[i], value == EGL_TRUE ? "t" : "f");
        else if (format[i] == 'x')
            sprintf(config, " %s:%04x", id[i], value);
        else if (format[i] == 'e')
        {
            const char* eid = "-";
            for (int j=0; enumValue[j] != EGL_NONE; j++)
                if (enumValue[j] == value)
                    eid = enumid[j];
            sprintf(config, " %s:%s", id[i], eid);
        } else if (format[i] == 'c')
            sprintf(config, " %s:%d", id[i], value);
        else
            sprintf(config, " %s:%02d", id[i], value);
        configs.append(config);
    }

    printf("config: %02d%s\n", num, configs.c_str());
}

static int eglInit()
{
    EGLint major;
    EGLint minor;
    EGLint numFound = 0;
    EGLConfig configList[MAX_CONFIG];

    g_EGLDisplay = eglGetDisplay(EGL_DEFAULT_DISPLAY);
    if (!g_EGLDisplay)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "eglGetDisplay failed");
        return 1;
    }

    EGLBoolean res = eglInitialize(g_EGLDisplay, &major, &minor);
    if (!res)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "eglInitialize failed");
        return 1;
    }

    eglGetConfigs(g_EGLDisplay, configList, MAX_CONFIG, &numFound);
    if (!numFound)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "eglGetConfigs failed to find any configs");
        return 1;
    }

    int requiredRedDepth = 8;
    bool requiredFound = s3eConfigGetInt("GL", "EGL_RED_SIZE", &requiredRedDepth) == S3E_RESULT_SUCCESS;

    int config = -1;
    int actualRedDepth = 0;
    printf("found %d configs\n", numFound);
    for (int i = 0; i < numFound; i++)
    {
        EGLint renderable = 0;
        EGLint surfacetype = 0;
        eglGetConfigAttrib(g_EGLDisplay, configList[i], EGL_RENDERABLE_TYPE, &renderable);
        eglGetConfigAttrib(g_EGLDisplay, configList[i], EGL_RED_SIZE, &actualRedDepth);
        eglGetConfigAttrib(g_EGLDisplay, configList[i], EGL_SURFACE_TYPE, &surfacetype);

        printConfig(i, configList[i]);

        if ((!requiredFound || (actualRedDepth == requiredRedDepth)) && (renderable & EGL_OPENGL_ES2_BIT) && (surfacetype & EGL_WINDOW_BIT))
        {
            config = i;
            break;
        }
    }

    if (config == -1)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "No GLES2 configs reported.  Trying random config");
        config = 0;
    }

    int version = s3eGLGetInt(S3E_GL_VERSION)>>8;
    printf("requesting GL version: %d\n", version);

    int configid;
    eglGetConfigAttrib(g_EGLDisplay, configList[config], EGL_CONFIG_ID, &configid);
    printf("choosing config with EGL ID: %d\n", configid);

    EGLint attribs[] = { EGL_CONTEXT_CLIENT_VERSION, version, EGL_NONE, };
    g_EGLContext = eglCreateContext(g_EGLDisplay, configList[config], NULL, attribs);
    if (!g_EGLContext)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "eglCreateContext failed");
        return 1;
    }

    version = s3eGLGetInt(S3E_GL_VERSION)>>8;
    if (version != 2)
    {
        printf("reported GL version: %d", version);
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "This example requires GLES v2.x");
        return 1;
    }

    void* nativeWindow = s3eGLGetNativeWindow();
    g_EGLSurface = eglCreateWindowSurface(g_EGLDisplay, configList[config], nativeWindow, NULL);
    eglMakeCurrent(g_EGLDisplay, g_EGLSurface, g_EGLSurface, g_EGLContext);
    g_glIsInitialized = true;
    return 0;
}

static void eglTerm()
{
    if (g_EGLDisplay)
    {
        eglMakeCurrent(g_EGLDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
        eglDestroySurface(g_EGLDisplay, g_EGLSurface);
        eglDestroyContext(g_EGLDisplay, g_EGLContext);
    }
    eglTerminate(g_EGLDisplay);
    g_EGLDisplay = 0;
    g_glIsInitialized = false;
}

void printShaderInfoLog(GLuint shader)
{
    GLint length;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
    if (length)
    {
        char* buffer = (char*)malloc( sizeof(char) * length ) ;
        glGetShaderInfoLog(shader, length, NULL, buffer);
        printf("shader error: %s", buffer);
        free(buffer);
        GLint success;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
        if (success != GL_TRUE) {
            exit(1);
        }
    }
}

size_t readFile(const char* filename, char** buffer, bool binary)
{
    FILE* fp = fopen(filename, binary ? "rb" : "r");
    if (fp == NULL) return 0;

    fseek(fp, 0, SEEK_END);
    size_t len = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    (*buffer) = new char[len + 1];

    len = fread(*buffer, 1, len, fp);

    fclose(fp);

    (*buffer)[len] = 0;
    return len;
}

GLuint createShader(GLenum type, const char* source, const char* binary)
{
    int hasCompiler;
    glGetIntegerv(GL_SHADER_COMPILER, &hasCompiler);

    char* buffer = NULL;
    int len = 0;
    if (hasCompiler)
    {
        char** tmp;
        if (type == GL_VERTEX_SHADER)
            tmp = &g_vertexSrc;
        else
            tmp = &g_fragmentSrc;

        if (*tmp == NULL)
        {
           len = (int) readFile(source, tmp, false);
        }

        buffer = *tmp;
        len = (int) strlen(buffer);
    }
    else
    {
        len = (int) readFile(binary, &buffer, true);
    }

    if (len == 0)
    {
        s3eDebugErrorPrintf("failed to load shader: %s", source);
        return 1;
    }

    GLuint shader = glCreateShader(type);

    if (hasCompiler)
    {
        glShaderSource(shader, 1, (const GLchar**)&buffer, &len);
        glCompileShader(shader);
    }
    else
    {
        glShaderBinary(1, &shader, BINARY_FORMAT, buffer, len);
        delete[] buffer;
    }
    printShaderInfoLog(shader);

    return shader;
}

int phaseLocation;

int compileShaders()
{
    GLuint shaderProgram = glCreateProgram();
    GLuint vertexShader = 0;
    GLuint fragmentShader = 0;

    if ((s3eDeviceGetInt(S3E_DEVICE_OS) != S3E_OS_ID_WS8 &&
         s3eDeviceGetInt(S3E_DEVICE_OS) != S3E_OS_ID_WS81 &&
         s3eDeviceGetInt(S3E_DEVICE_OS) != S3E_OS_ID_WP81 &&
         s3eDeviceGetInt(S3E_DEVICE_OS) != S3E_OS_ID_WIN10
        ) ||
        s3eDeviceGetInt(S3E_DEVICE_DX_FEATURE_LEVEL) < 93
       )
    {
        vertexShader = createShader(GL_VERTEX_SHADER, "shaders/doom.vshader", "shaders/doom.vbinary");
        fragmentShader = createShader(GL_FRAGMENT_SHADER, "shaders/doom.pshader", "shaders/doom.pbinary");
    }
    else
    {
        vertexShader = createShader(GL_VERTEX_SHADER, "shaders/doom.93vshader", "shaders/doom.93vbinary");
        fragmentShader = createShader(GL_FRAGMENT_SHADER, "shaders/doom.93pshader", "shaders/doom.93pbinary");
    }

    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);

    glLinkProgram(shaderProgram);

    int linked = 0;
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &linked);

    if (!linked)
    {
        GLint infoLen = 0;
        glGetProgramiv(shaderProgram, GL_INFO_LOG_LENGTH, &infoLen);

        char* infoLog = NULL;
        if (infoLen > 1)
        {
            infoLog = (char*) malloc(sizeof(char) * infoLen);
            glGetProgramInfoLog(shaderProgram, infoLen, NULL, infoLog );
        }

        s3eDebugErrorPrintf("Failed to link program: %s", infoLog);

        free(infoLog);

        glDeleteProgram(shaderProgram);
        return 1;
    }

    glUseProgram(shaderProgram);

    phaseLocation = glGetUniformLocation(shaderProgram, "phase");

    if (phaseLocation < 0)
    {
        s3eDebugErrorPrintf("Unable to get uniform location");
        glDeleteProgram(shaderProgram);
        return 1;
    }

    return 0;
}

const float vertexArray[] = {
    0, -1, 0, 1,
    1, 1, 0, 1,
    -1, 1, 0, 1
};

void render()
{
    static float offset = 0;
    int w = s3eSurfaceGetInt(S3E_SURFACE_WIDTH);
    int h = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT);
    glViewport(0, 0, w, h) ;
    glClearColor(0, 1, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT);

    glUniform1f(phaseLocation, offset);

    glVertexAttribPointer(0, 4, GL_FLOAT, false, 0, vertexArray);
    glEnableVertexAttribArray(0);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 3);

    eglSwapBuffers(g_EGLDisplay, g_EGLSurface);

    offset = fmodf(offset + 0.2f, 2*3.141f);
}

int main()
{
    if (eglInit())
       return 1;

    printf("Screen BPP: %d\n", s3eSurfaceGetInt(S3E_SURFACE_PIXEL_TYPE) & S3E_SURFACE_PIXEL_SIZE_MASK);
    printf("\n");
    printf("Vendor     : %s\n", (const char*)glGetString( GL_VENDOR ) );
    printf("Renderer   : %s\n", (const char*)glGetString( GL_RENDERER ) );
    printf("Version    : %s\n", (const char*)glGetString( GL_VERSION ) );
    printf("Extensions : %s\n", (const char*)glGetString( GL_EXTENSIONS ) );
    printf("Resolution : %dx%d\n", s3eSurfaceGetInt(S3E_SURFACE_WIDTH), s3eSurfaceGetInt(S3E_SURFACE_HEIGHT));
    printf("\n");

    if (compileShaders())
        return 1;

    // some platforms require the egl context to be destroyed and restored arbitrarily
    if (s3eGLGetInt(S3E_GL_MUST_SUSPEND))
    {
        // when a suspend is requested, terminate EGL
        s3eGLRegister(S3E_GL_SUSPEND, (s3eCallback)eglTerm, NULL);

        // when a resume is requested, re-initialise EGL...
        s3eGLRegister(S3E_GL_RESUME, (s3eCallback)eglInit, NULL);
        // ...and restore the shaders
        s3eGLRegister(S3E_GL_RESUME, (s3eCallback)compileShaders, NULL);
    }

    bool quit = false;

    int numFrames = 0;

    while (!quit) {
                s3eKeyboardUpdate();
                s3eDeviceYield(0);
                if (s3eDeviceCheckQuitRequest())
                    quit = 1;
                if (s3eKeyboardGetState(s3eKeyEsc) & S3E_KEY_STATE_PRESSED)
                    quit = 1;
                // Only try and render if gl is initialized.  It may get
                // terminated due to a S3E_GL_SUSPEND callback.
                if (g_glIsInitialized)
                    render();
                numFrames++;
    }

    eglTerm();

    delete[] g_fragmentSrc;
    delete[] g_vertexSrc;

    return 0;
}
