/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EGLES31 S3E OpenGL ES 3.1 with Android Extension Pack Example
 * The following example demonstrates OpenGL ES 3.1 with Android Extension Pack.
 *
 * @include s3eGLES31AEP.cpp
 */

#include <stdlib.h>
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <GLES3/gl31.h>
#include <GLES2/gl2ext.h>
#include <EGL/egl.h>
#include <EGL/eglext.h>

#include "s3e.h"

static EGLSurface g_EGLSurface = NULL;
static EGLDisplay g_EGLDisplay = NULL;
static EGLContext g_EGLContext = NULL;

#define MAX_CONFIG 32

#define BINARY_FORMAT 0x93B0

bool g_glIsInitialized = false;
static char* g_vertexSrc = NULL;
static char* g_fragmentSrc = NULL;
static char* g_tessCtrlSrc = NULL;
static char* g_tessEvalSrc = NULL;
static char* g_computeSrc = NULL;

bool hasEGLExtension(const char* ext)
{
    const char* extensions = eglQueryString(g_EGLDisplay, EGL_EXTENSIONS);
    char extension[256];
    int j=0;
    for (int i=0; extensions[i] != 0; i++)
    {
        if (extensions[i] == ' ')
        {
            extension[j] = 0;
            if (!strcmp(ext, extension))
                return true;
            j=0;
        } else
            extension[j++] = extensions[i];
    }
    if (j > 0)
    {
        extension[j] = 0;
        if (!strcmp(ext, extension))
            return true;
    }

    return false;
}

static int eglInit()
{
    EGLint major;
    EGLint minor;
    EGLint numFound = 0;
    EGLConfig configList[MAX_CONFIG];

    g_EGLDisplay = eglGetDisplay(EGL_DEFAULT_DISPLAY);
    if (!g_EGLDisplay)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "eglGetDisplay failed");
        return 1;
    }

    EGLBoolean res = eglInitialize(g_EGLDisplay, &major, &minor);
    if (!res)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "eglInitialize failed");
        return 1;
    }

    eglGetConfigs(g_EGLDisplay, configList, MAX_CONFIG, &numFound);
    if (!numFound)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "eglGetConfigs failed to find any configs");
        return 1;
    }

    int requiredRedDepth = 8;
    bool requiredFound = s3eConfigGetInt("GL", "EGL_RED_SIZE", &requiredRedDepth) == S3E_RESULT_SUCCESS;

    int config = -1;
    int actualRedDepth = 0;
    printf("found %d configs\n", numFound);
    for (int i = 0; i < numFound; i++)
    {
        EGLint renderable = 0;
        EGLint surfacetype = 0;
        eglGetConfigAttrib(g_EGLDisplay, configList[i], EGL_RENDERABLE_TYPE, &renderable);
        eglGetConfigAttrib(g_EGLDisplay, configList[i], EGL_RED_SIZE, &actualRedDepth);
        eglGetConfigAttrib(g_EGLDisplay, configList[i], EGL_SURFACE_TYPE, &surfacetype);

        if ((!requiredFound || (actualRedDepth == requiredRedDepth)) && (renderable & EGL_OPENGL_ES2_BIT) && (surfacetype & EGL_WINDOW_BIT))
        {
            config = i;
            break;
        }
    }

    if (config == -1)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "No GLES3 configs reported.  Trying random config");
        config = 0;
    }

    int version = s3eGLGetInt(S3E_GL_VERSION);
    major = version>>8;
    minor = (s3eGLGetInt(S3E_GL_VERSION)&0xff)>>4;
    printf("requesting GL version: %d.%d\n", major, minor);

    int configid;
    eglGetConfigAttrib(g_EGLDisplay, configList[config], EGL_CONFIG_ID, &configid);
    printf("choosing config with EGL ID: %d\n", configid);

    EGLint attribs[] = { EGL_CONTEXT_CLIENT_VERSION, major, EGL_NONE, minor, EGL_NONE};

    if (minor>0 && hasEGLExtension("EGL_KHR_create_context"))
        attribs[2] = EGL_CONTEXT_MINOR_VERSION_KHR;

    g_EGLContext = eglCreateContext(g_EGLDisplay, configList[config], NULL, attribs);
    if (!g_EGLContext)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "eglCreateContext failed");
        return 1;
    }

    version = s3eGLGetInt(S3E_GL_VERSION);
    major = version>>8;
    minor = (s3eGLGetInt(S3E_GL_VERSION)&0xff)>>4;
    printf("S3E_GL_VERSION: %d.%d\n", major, minor);

    if (major < 3 || minor < 1)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "This example requires GLES v3.x");
        return 1;
    }

    void* nativeWindow = s3eGLGetNativeWindow();
    g_EGLSurface = eglCreateWindowSurface(g_EGLDisplay, configList[config], nativeWindow, NULL);
    eglMakeCurrent(g_EGLDisplay, g_EGLSurface, g_EGLSurface, g_EGLContext);
    g_glIsInitialized = true;
    return 0;
}

static void eglTerm()
{
    if (g_EGLDisplay)
    {
        eglMakeCurrent(g_EGLDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
        eglDestroySurface(g_EGLDisplay, g_EGLSurface);
        eglDestroyContext(g_EGLDisplay, g_EGLContext);
    }
    eglTerminate(g_EGLDisplay);
    g_EGLDisplay = 0;
    g_glIsInitialized = false;
}

PFNGLGETDEBUGMESSAGELOGKHRPROC glGetDebugMessageLog = NULL;

void FetchDebugMessages()
{
    if (glGetDebugMessageLog == NULL)
        return;

    while (true)
    {
        GLenum source, type, severity;
        GLuint id;
        GLsizei length;
        char message[1024];

        if (glGetDebugMessageLog(1, 1024, &source, &type, &id, &severity, &length, message) < 1)
            break;

        printf("GL Error(%d): %s\n", id, message);
    }
}

void printShaderInfoLog(GLuint shader)
{
    GLint length;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
    if (length > 1)
    {
        char* buffer = (char*)s3eMalloc( sizeof(char) * length ) ;
        glGetShaderInfoLog(shader, length, NULL, buffer);
        printf("shader error: %s", buffer);
        s3eFree(buffer);
        GLint success;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
        if (success != GL_TRUE) {
            exit(1);
        }
    }
}

int readFile(const char* filename, char** buffer, bool binary)
{
    FILE* fp = fopen(filename, binary ? "rb" : "r");
    if (fp == NULL) return 0;

    fseek(fp, 0, SEEK_END);
    int len = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    (*buffer) = new char[len + 1];

    len = fread(*buffer, 1, len, fp);

    fclose(fp);

    (*buffer)[len] = 0;
    return len;
}

GLuint createShader(GLenum type, const char* source)
{
    char* buffer = NULL;
    int len = 0;
    char** tmp;
    if (type == GL_COMPUTE_SHADER)
        tmp = &g_computeSrc;
    else if (type == GL_VERTEX_SHADER)
        tmp = &g_vertexSrc;
    else if (type == GL_TESS_CONTROL_SHADER_EXT)
        tmp = &g_tessCtrlSrc;
    else if (type == GL_TESS_EVALUATION_SHADER_EXT)
        tmp = &g_tessEvalSrc;
    else
        tmp = &g_fragmentSrc;

    if (*tmp == NULL)
    {
        len = readFile(source, tmp, false);
    }

    buffer = *tmp;
    len = strlen(buffer);

    if (len == 0)
    {
        s3eDebugErrorPrintf("failed to load shader: %s", source);
        return 1;
    }

    GLuint shader = glCreateShader(type);

    glShaderSource(shader, 1, (const GLchar**)&buffer, &len);
    glCompileShader(shader);
    printShaderInfoLog(shader);

    return shader;
}

int texLocation;
int imageLocation;
int rollLocation;
GLuint texHandle;
GLuint shaderProgram;
GLuint shaderCProgram;

int compileShaders()
{
    shaderProgram = glCreateProgram();
    GLuint vertexShader = createShader(GL_VERTEX_SHADER, "shaders/patch.vshader");
    GLuint tessCtrlShader = createShader(GL_TESS_CONTROL_SHADER_EXT, "shaders/patch.tcshader");
    GLuint tessEvalShader = createShader(GL_TESS_EVALUATION_SHADER_EXT, "shaders/patch.teshader");
    GLuint fragmentShader = createShader(GL_FRAGMENT_SHADER, "shaders/patch.pshader");

    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, tessCtrlShader);
    glAttachShader(shaderProgram, tessEvalShader);
    glAttachShader(shaderProgram, fragmentShader);

    glLinkProgram(shaderProgram);

    int linked = 0;
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &linked);

    if (!linked)
    {
        GLint infoLen = 0;
        glGetProgramiv(shaderProgram, GL_INFO_LOG_LENGTH, &infoLen);

        char* infoLog = NULL;
        if (infoLen > 1)
        {
            infoLog = (char*) s3eMalloc(sizeof(char) * infoLen);
            glGetProgramInfoLog(shaderProgram, infoLen, NULL, infoLog );
        }

        s3eDebugErrorPrintf("Failed to link program: %s", infoLog);

        s3eFree(infoLog);

        glDeleteProgram(shaderProgram);
        return 1;
    }

    glUseProgram(shaderProgram);

    texLocation = glGetUniformLocation(shaderProgram, "srcTex");

    if (texLocation < 0)
    {
        s3eDebugErrorPrintf("Unable to get uniform location");
        glDeleteProgram(shaderProgram);
        return 1;
    }

    glGenTextures(1, &texHandle);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texHandle);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    //glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, 512, 512, 0, GL_RED, GL_FLOAT, NULL);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_R32F, 512, 512);

    GLint MaxPatchVertices = 0;
    glGetIntegerv(GL_MAX_PATCH_VERTICES_EXT, &MaxPatchVertices);
    printf("Max supported patch vertices %d\n", MaxPatchVertices);

    PFNGLPATCHPARAMETERIEXTPROC glPatchParameteri = (PFNGLPATCHPARAMETERIEXTPROC)eglGetProcAddress("glPatchParameteriEXT");
    glPatchParameteri(GL_PATCH_VERTICES_EXT, 3);

    return 0;
}

int compileCompute()
{
    shaderCProgram = glCreateProgram();
    GLuint computeShader = createShader(GL_COMPUTE_SHADER, "shaders/test.cshader");

    glAttachShader(shaderCProgram, computeShader);

    glLinkProgram(shaderCProgram);

    int linked = 0;
    glGetProgramiv(shaderCProgram, GL_LINK_STATUS, &linked);

    if (!linked)
    {
        GLint infoLen = 0;
        glGetProgramiv(shaderCProgram, GL_INFO_LOG_LENGTH, &infoLen);

        char* infoLog = NULL;
        if (infoLen > 1)
        {
            infoLog = (char*) s3eMalloc(sizeof(char) * infoLen);
            glGetProgramInfoLog(shaderCProgram, infoLen, NULL, infoLog );
        }

        s3eDebugErrorPrintf("Failed to link program: %s", infoLog);

        s3eFree(infoLog);

        glDeleteProgram(shaderCProgram);
        return 1;
    }

    glUseProgram(shaderCProgram);

    glBindImageTexture(0, texHandle, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R32F);

    imageLocation = glGetUniformLocation(shaderCProgram, "destTex");

    if (imageLocation < 0)
    {
        s3eDebugErrorPrintf("Unable to get uniform location");
        glDeleteProgram(shaderCProgram);
        return 1;
    }

    glUniform1i(imageLocation, 0);

    rollLocation = glGetUniformLocation(shaderCProgram, "roll");

    if (rollLocation < 0)
    {
        s3eDebugErrorPrintf("Unable to get uniform location");
        glDeleteProgram(shaderCProgram);
        return 1;
    }

    return 0;
}

const float vertexArray[] = {
    0, -1, 0, 1,
    1, 1, 0, 1,
    -1, 1, 0, 1
};

int frame = 0;

void render()
{
    int w = s3eSurfaceGetInt(S3E_SURFACE_WIDTH);
    int h = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT);
    glViewport(0, 0, w, h) ;
    glClearColor(0, 1, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT);

    glUseProgram(shaderCProgram);
    glUniform1f(rollLocation, (float)frame*0.01f);

    glDispatchCompute(512/16, 512/16, 1);

    glUseProgram(shaderProgram);
    glUniform1i(texLocation, 0);

    glVertexAttribPointer(0, 4, GL_FLOAT, false, 0, vertexArray);
    glEnableVertexAttribArray(0);
    glDrawArrays(GL_PATCHES_EXT, 0, 3);

    eglSwapBuffers(g_EGLDisplay, g_EGLSurface);

    frame++;
}

int main()
{
    if (eglInit())
       return 1;

    printf("Screen BPP: %d\n", s3eSurfaceGetInt(S3E_SURFACE_PIXEL_TYPE) & S3E_SURFACE_PIXEL_SIZE_MASK);
    printf("\n");
    printf("Vendor     : %s\n", (const char*)glGetString( GL_VENDOR ) );
    printf("Renderer   : %s\n", (const char*)glGetString( GL_RENDERER ) );
    printf("Version    : %s\n", (const char*)glGetString( GL_VERSION ) );
    printf("Extensions :\n");
    for (int i=0; ; i++)
    {
        const char* str = (const char*)glGetStringi( GL_EXTENSIONS, i);
        if (str == NULL) break;

        printf("    %s\n", str);
    }
    printf("\n");

    PFNGLDEBUGMESSAGECONTROLKHRPROC glDebugMessageControl = (PFNGLDEBUGMESSAGECONTROLKHRPROC)eglGetProcAddress("glDebugMessageControlKHR");
    if (glDebugMessageControl == NULL)
        glDebugMessageControl = (PFNGLDEBUGMESSAGECONTROLKHRPROC)eglGetProcAddress("glDebugMessageControl");
    glGetDebugMessageLog = (PFNGLGETDEBUGMESSAGELOGKHRPROC)eglGetProcAddress("glGetDebugMessageLogKHR");
    if (glGetDebugMessageLog == NULL)
        glGetDebugMessageLog = (PFNGLGETDEBUGMESSAGELOGKHRPROC)eglGetProcAddress("glGetDebugMessageLog");

    if (glGetDebugMessageLog != NULL)
        glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL, true);

    FetchDebugMessages();

    if (compileShaders())
        return 1;

    if (compileCompute())
        return 1;

    FetchDebugMessages();

    // some platforms require the egl context to be destroyed and restored arbitrarily
    if (s3eGLGetInt(S3E_GL_MUST_SUSPEND))
    {
        // when a suspend is requested, terminate EGL
        s3eGLRegister(S3E_GL_SUSPEND, (s3eCallback)eglTerm, NULL);

        // when a resume is requested, re-initialise EGL...
        s3eGLRegister(S3E_GL_RESUME, (s3eCallback)eglInit, NULL);
        // ...and restore the shaders
        s3eGLRegister(S3E_GL_RESUME, (s3eCallback)compileShaders, NULL);
    }

    bool quit = false;
    bool first = true;
    int numFrames = 0;

    while (!quit) {
        s3eKeyboardUpdate();
        s3eDeviceYield(0);
        if (s3eDeviceCheckQuitRequest())
            quit = 1;
        if (s3eKeyboardGetState(s3eKeyEsc) & S3E_KEY_STATE_PRESSED)
            quit = 1;
        // Only try and render if gl is initialized.  It may get
        // terminated due to a S3E_GL_SUSPEND callback.
        if (g_glIsInitialized)
            render();

        if (first)
        {
            FetchDebugMessages();
            first = false;
        }

        numFrames++;
    }

    eglTerm();

    delete[] g_computeSrc;
    delete[] g_fragmentSrc;
    delete[] g_vertexSrc;

    return 0;
}
