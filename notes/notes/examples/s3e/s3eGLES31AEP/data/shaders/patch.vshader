#version 310 es

in highp vec4 position;
out mediump vec4 position_tc;

void main()
{
    position_tc = position;
}