/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EImagePicker S3E Image Picker Example
 *
 * The following example demonstrates S3E's Image Picker functionality.
 *
 * The main functions used to achieve this are:
 * - s3eImagePickerToFilePath()
 * - s3eImagePickerGetCount()
 * - s3eImagePickerGetIndex()
 *
 * The example allows the user to launch the native gallery/file picker,
 * which populates an internal list with the paths to the selected images
 * and/or videos. It then get the file path list and displays the
 * images/videos.
 *
 * File paths are returned with the raw:// prefix,
 * followed by their full system path, rather than the normal local
 * path relative to the app's data folder. These raw paths can be passed to
 * S3E file APIs and will be resolved transparently.
 *
 * The example uses S3E Video to play/show videos and JPG images. JPGs are
 * shown in this way to keep the example code simple rather than pulling in
 * non-S3E functionality. Note that using s3eVideoPlay with a JPG is not
 * guaranteed to scale to the sizes given so you may only see the top left
 * corner of the returned image.
 *
 * @include s3eImagePicker.cpp
 */
#include "ExamplesMain.h"
#include "s3eImagePicker.h"
#include "s3eVideo.h"
#include "s3eFile.h"
#include "s3eDebug.h"

#include "IwImage.h"

#include <sys/param.h>

int32 OnScreenRotation(void* sysData, void* userData);
static void ShowImages();

// Array of buttons, one for each valid image/video type
static struct CImgButton
{
    Button* button;
    const char* name;
    s3eImagePickerFormat format;
} g_Buttons[] =
{
    {NULL, "Pick JPG",  S3E_IMAGEPICKER_FORMAT_JPG },
    {NULL, "Pick PNG",  S3E_IMAGEPICKER_FORMAT_PNG },
    {NULL, "Pick GIF",  S3E_IMAGEPICKER_FORMAT_GIF },
    {NULL, "Pick BMP",  S3E_IMAGEPICKER_FORMAT_BMP },
    {NULL, "Pick Video",S3E_IMAGEPICKER_FORMAT_ANYVIDEO },
    {NULL, "Pick Image",S3E_IMAGEPICKER_FORMAT_ANYIMAGE },
    {NULL, "Pick Any",  S3E_IMAGEPICKER_FORMAT_ANY }
};

static const int num_buttons = sizeof(g_Buttons) / sizeof(CImgButton);

// Control buttons
static Button* g_ShowButton = 0;
static Button* g_ResetButton = 0;
static Button* g_SaveButton = 0;

// Global states
static bool g_ShowingImages = false;
static bool g_CanSave = false;
static bool g_GotImages = false;
static bool g_ShowImagesOnRotation = false;
static bool g_HasCallbacksRegistered = false;

static bool g_bIsVideoPlaying = false;

static int32 OnImagesSelected(void* sysData, void* userData)
{
    int filesCount = *(int*)sysData;
    if (filesCount <= 0)
    {
        if (filesCount == 0)
            AppendMessage("Image picking was cancelled");
        else
            AppendMessageColour(RED, "Error retrieving files - %d", s3eImagePickerGetError());

        return 0;
    }

    g_GotImages = true;
    ShowImages();

    return 0;
}

static int32 VideoStoppedCallback(void* systemData, void* userData)
{
    g_bIsVideoPlaying = false;
    return true;
}

void ExampleInit()
{
    InitMessages(10, 128);
    bool enabled = false;

    //Check whether the Image Picker extension is available
    if (s3eImagePickerAvailable())
    {
        enabled = true;

        // Save only suppored on iOS and Win 8 platforms
        if (s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_IPHONE ||
            s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WP8 ||
            s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WP81 ||
            s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WS8 ||
            s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WS81 ||
            s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WIN10
           )
        {
            g_CanSave = true;
        }
        else
        {
            AppendMessage("Save to gallery not supported on this device");
        }
    }
    else
    {
        AppendMessageColour(RED, "Image Picker extension not available");
    }

    g_SaveButton = NewButton("Save last image to gallery");
    g_ResetButton = NewButton("Clear image list");
    g_ShowButton = NewButton("(Re)show picked images");

    for (int i=0; i< num_buttons; i++)
    {
        g_Buttons[i].button = NewButton(g_Buttons[i].name);
        g_Buttons[i].button->m_Enabled = enabled;
    }
    s3eVideoRegister(S3E_VIDEO_STOP, &VideoStoppedCallback, NULL);
}

void ExampleTerm()
{
}

// Callback
int32 OnScreenTouched(void* sysData, void* userData)
{
    g_ShowingImages = false;
    if (g_HasCallbacksRegistered)
    {
        s3ePointerUnRegister(S3E_POINTER_BUTTON_EVENT, OnScreenTouched);
        s3eSurfaceUnRegister(S3E_SURFACE_SCREENSIZE, OnScreenRotation);
        g_HasCallbacksRegistered = false;
    }
    return 0;
}

int32 OnScreenRotation(void* sysData, void* userData)
{
    g_ShowImagesOnRotation = true;

    return 0;
}

void WaitForUserInput()
{
    g_ShowingImages = true;
    if (!g_HasCallbacksRegistered)
    {
        s3ePointerRegister(S3E_POINTER_BUTTON_EVENT, OnScreenTouched, NULL);
        s3eSurfaceRegister(S3E_SURFACE_SCREENSIZE, OnScreenRotation, NULL);
        g_HasCallbacksRegistered = true;
    }

    // Wait until any user event.
    do
    {
        s3eKeyboardUpdate();
        s3ePointerUpdate();
        s3eDeviceYield(100);
        s3eSurfaceShow();
    }
    while (g_ShowingImages && !ExampleCheckQuit() && !s3eKeyboardAnyKey() && !s3eDeviceCheckPauseRequest() && !g_ShowImagesOnRotation);

    // Clear input buffers and reset touch state if TouchToStop not already called
    s3eKeyboardUpdate();
    s3ePointerUpdate();
    s3eVideoStop();

    // If showing images was stopped after rotation event, restart it again.
    if (g_ShowImagesOnRotation)
    {
        g_ShowImagesOnRotation = false;
        ShowImages();
    }
}

static void ShowImages()
{
    const int count = s3eImagePickerGetCount();

    if (!count)
        return;

    for (int i = 0; i < count; i++)
    {
        const s3eImagePickerResult* p = s3eImagePickerGetIndex(i);
        const char* path = (const char*)p->m_Buffer;
        AppendMessage("Path: %s", path);

        // Images are drawn in rows down the screen with one wait loop at the end

        if (p->m_Format == S3E_IMAGEPICKER_FORMAT_ANYVIDEO)
        {
            while (g_bIsVideoPlaying == true)
            {
                s3eDeviceYield(10);
            }
            s3eSurfaceShow();
            s3eDeviceYield(100);
            g_bIsVideoPlaying = true;
            // for JPG decompression use s3eVideo
            if (s3eVideoPlay(path, 1, 0,  0, s3eSurfaceGetInt(S3E_SURFACE_WIDTH), s3eSurfaceGetInt(S3E_SURFACE_HEIGHT)) == S3E_RESULT_ERROR)
            {
                s3eVideoError err = s3eVideoGetError();
                AppendMessage("Failed play video - error %d", (int)err);
            }
        }
        else if (p->m_Format == S3E_IMAGEPICKER_FORMAT_JPG)
        {
            // for JPG decompression use s3eVideo
            if (s3eVideoPlay(path, 1, 0,  i * (s3eSurfaceGetInt(S3E_SURFACE_HEIGHT)/count), s3eSurfaceGetInt(S3E_SURFACE_WIDTH), s3eSurfaceGetInt(S3E_SURFACE_HEIGHT)/count) == S3E_RESULT_ERROR)
            {
                s3eVideoError err = s3eVideoGetError();
                AppendMessage("Failed play JPG - error %d", (int)err);
            }
        }
        else if (p->m_Format == S3E_IMAGEPICKER_FORMAT_UNKNOWN)
        {
            AppendMessage("Unknown file format - %s", path);
        }
        else
        {
            // Convert image to rgb565 format
            // We set our converted image to be the same width, height and pitch as the surface
            // So we can blit directly to it
            CIwImage *image = new CIwImage();
            image->LoadFromFile(path);

            CIwImage *g_Image = new CIwImage();
            g_Image->SetFormat(CIwImage::RGB_565);

            g_Image->SetWidth(image->GetWidth());
            g_Image->SetHeight(image->GetHeight());

            image->ConvertToImage(g_Image);
            delete image;

            uint8* screen = (uint8*)s3eSurfacePtr();
            uint8* buffer = (uint8*)g_Image->GetTexels();

            const int width  = MIN((int)g_Image->GetWidth(),  s3eSurfaceGetInt(S3E_SURFACE_WIDTH));
            const int height = MIN((int)g_Image->GetHeight(), s3eSurfaceGetInt(S3E_SURFACE_HEIGHT) / count);
            const int pitch = s3eSurfaceGetInt(S3E_SURFACE_PITCH);

            for (int y = 0; y < height; y++)
            {
                memcpy(&screen[(y + i * height) * pitch], &buffer[y * g_Image->GetPitch()], width*2);
            }

            s3eSurfaceShow();

            delete g_Image;
        }
    }

    WaitForUserInput();
}

static void ItemSaved(s3eImagePickerResult* image, s3eImagePickerError* error)
{
    if (*error != S3E_IMAGEPICKER_ERR_NONE)
        AppendMessageColour(RED, "Failed to save item to gallery");
    else
        AppendMessageColour(GREEN, "Completed saving item to gallery");
}


bool ExampleUpdate()
{
    g_SaveButton->m_Enabled = g_CanSave && g_GotImages;
    g_ResetButton->m_Enabled = g_GotImages;
    g_ShowButton->m_Enabled = g_GotImages;

    Button* pressed = GetSelectedButton();

    if (pressed == NULL)
    {
        return true;
    }

    s3eImagePickerFormat f = S3E_IMAGEPICKER_FORMAT_UNKNOWN;

    // check if a button is pressed
    for (int i = 0; i < num_buttons; i++)
    {
        if (pressed == g_Buttons[i].button)
        {
            f = g_Buttons[i].format;
            break;
        }
    }

    if (f != S3E_IMAGEPICKER_FORMAT_UNKNOWN)
    {
        s3eImagePickerToFilePath(f, 0, pressed->m_XPos, pressed->m_YPos, pressed->m_Width, pressed->m_Height, OnImagesSelected);
    }

    if (pressed == g_ShowButton)
    {
        ShowImages();
    }

    if (pressed == g_SaveButton)
    {
        int count = s3eImagePickerGetCount();
        const s3eImagePickerResult* p = s3eImagePickerGetIndex(count-1);

        // This example only picks files so assume buffer is a file path
        AppendMessage("Saving file to gallery: %s", p->m_Buffer);

        if (s3eImagePickerSaveToGallery(p, ItemSaved) == S3E_RESULT_SUCCESS)
        {
            AppendMessageColour(GREEN, "Saving to gallery...");
        }
        else
        {
            AppendMessageColour(RED, "Failed to start saving to gallery");
        }
    }

    if (pressed == g_ResetButton)
    {
        // Clear internal buffer
        s3eImagePickerReset();
        g_GotImages = false;
    }

    return true;
}

/*
 * The ExampleRender function outputs a set of strings indicating the result
 * image picker.
 */
void ExampleRender()
{
    int x = 20;
    int y = GetYBelowButtons();

    PrintMessages(x, y);
}
