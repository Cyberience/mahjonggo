/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
#include "ExamplesMain.h"

/**
 * @page ExampleS3EDeviceInfo S3E Device Info Example
 *
 * The following example uses S3E's Device Interface API to display details
 * of the device that the application is currently running on.
 *
 * The main functions used to achieve this are:
 * - s3eDeviceGetInt()
 * - s3eDeviceGetString()
 *
 * This application is useful for quickly obtain information about a new
 * device.
 *
 * The following graphic illustrates the example output:
 *
 * @image html s3eDeviceInfoImage.png
 *
 * @include s3eDeviceInfo.cpp
 */
const int g_NumMessages = 50;
const int g_MessageLen = 160;

void ExampleInit()
{
    InitMessages(g_NumMessages, g_MessageLen);
}

void ExampleTerm()
{
    TerminateMessages();
}

bool ExampleUpdate()
{
    return true;
}

void ExampleRender()
{
    ClearMessages();

    // Add lines in reversed order, as PrintMessages() is used to log messages.
    const char* runtime = s3eDeviceGetString(S3E_DEVICE_RUNTIME);

    AppendMessage("SURFACE resolution     : %dx%d", s3eSurfaceGetInt(S3E_SURFACE_WIDTH), s3eSurfaceGetInt(S3E_SURFACE_HEIGHT));
    AppendMessage("SURFACE unscaled res.  : %dx%d", s3eSurfaceGetInt(S3E_SURFACE_WIDTH_UNSCALED), s3eSurfaceGetInt(S3E_SURFACE_HEIGHT_UNSCALED));
    AppendMessage("PPI (LOGICAL)          : %d", s3eDeviceGetInt(S3E_DEVICE_PPI_LOGICAL));
    AppendMessage("PPI                    : %d", s3eDeviceGetInt(S3E_DEVICE_PPI));
    AppendMessage("RUNTIME                : %s", (runtime && strlen(runtime)>0) ? runtime : "unsupported");
    AppendMessage("SUPPORTS_SUSPEND_RESUME: %d", s3eDeviceGetInt(S3E_DEVICE_SUPPORTS_SUSPEND_RESUME));
    AppendMessage("NUM_CPU_CORES          : %d", s3eDeviceGetInt(S3E_DEVICE_NUM_CPU_CORES));
    AppendMessage("LOCALE                 : %s", s3eDeviceGetString(S3E_DEVICE_LOCALE));
    AppendMessage("LANGUAGE CODE          : %d", s3eDeviceGetInt(S3E_DEVICE_LANGUAGE));
    AppendMessage("LANGUAGE               : %s", s3eDeviceGetString(S3E_DEVICE_LANGUAGE));
    AppendMessage("MEM_FREE               : %d", s3eDeviceGetInt(S3E_DEVICE_MEM_FREE));
    AppendMessage("MEM_TOTAL              : %d", s3eDeviceGetInt(S3E_DEVICE_MEM_TOTAL));
    AppendMessage("BATTERY_LEVEL          : %d", s3eDeviceGetInt(S3E_DEVICE_BATTERY_LEVEL));
    AppendMessage("MAINS_POWER            : %d", s3eDeviceGetInt(S3E_DEVICE_MAINS_POWER));
    AppendMessage("CHIPSET                : %s", s3eDeviceGetString(S3E_DEVICE_CHIPSET));
    AppendMessage("IMSI                   : %s", s3eDeviceGetString(S3E_DEVICE_IMSI));
    AppendMessage("PHONE_NUMBER           : %s", s3eDeviceGetString(S3E_DEVICE_PHONE_NUMBER));
    AppendMessage("NAME                   : %s", s3eDeviceGetString(S3E_DEVICE_NAME));
    AppendMessage("UNIQUE_ID              : %s", s3eDeviceGetString(S3E_DEVICE_UNIQUE_ID));
    AppendMessage("ID                     : %s", s3eDeviceGetString(S3E_DEVICE_ID));
    AppendMessage("ARCHITECTURE           : %s", s3eDeviceGetString(S3E_DEVICE_ARCHITECTURE));
    AppendMessage("OS_VERSION             : %s", s3eDeviceGetString(S3E_DEVICE_OS_VERSION));
    AppendMessage("OS                     : %s", s3eDeviceGetString(S3E_DEVICE_OS));

    PrintMessages(10, 50);
}
