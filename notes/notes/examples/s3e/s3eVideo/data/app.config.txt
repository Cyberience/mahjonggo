# This .config.txt file documents configuration settings for your
# application
# The syntax is similar to that in .icf files:
#
# [GroupName]
# Setting     Documentation for setting
#
# e.g.
# [MyApplicationGroup]
# MySetting   Description of what MySetting is for, its default values, etc

[APP]
FreezeRotationWhenPlaying If set to 1, will stop rotation when video plays. Default 0.

