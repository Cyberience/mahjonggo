/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EVideo S3E Video Example
 * The following example uses S3E's video functionality to play a video file
 * from buffer.
 *
 * The following functions are used to achieve this:
 * - s3eFileOpen()
 * - s3eFileGetSize()
 * - s3eMallocBase()
 * - s3eFree()
 * - s3eFileRead()
 * - s3eVideoRegister()
 * - s3eVideoPlay()
 * - s3eVideoPause()
 * - s3eVideoResume()
 * - s3eVideoStop()
 *
 * This example uses the s3eFileOpen(), s3eMallocBase() and s3eFileRead()
 * functions to open, allocate memory for, and read a file. The keyboard
 * is used to control the state of playback:
 * - pressing s3eKey1 will call the s3eVideoPlay() function to play the video from buffer.
 * - pressing s3eKey2 will call the s3eVideoPause() function to pause video playback.
 * - pressing s3eKey3 will call the s3eVideoResume() function to resume video playback.
 * - pressing s3eKey4 will call the s3eVideoStop() function to stop video playback.
 *
 * The example registers a callback function using the s3eVideoRegister() function.
 * This callback is called when the S3E_VIDEO_STOP event occurs. The registered
 * callback function, videoStopped() changes the playback status to be eot.
 *
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eVideoImage.png
 *
 * @include s3eVideo.cpp
 */
#include "ExamplesMain.h"
#include "s3eBackgroundMusic.h"
#include "s3eSurface.h"
#include "s3eConfig.h"

#define TEST_VIDEO_3GP "example.3gp"
#define TEST_VIDEO_MP4 "example.mp4"
// Video h.264, Audio AAC - intended to play on all Windows Phone 8 devices and simulator
// See http://msdn.microsoft.com/en-us/library/windowsphone/develop/ff462087%28v=vs.105%29.aspx for futher information
#define TEST_VIDEO_MOV "example.mov"

static Button* g_ButtonPlay = NULL;
static Button* g_ButtonPlayFull = NULL;
static Button* g_ButtonPause = NULL;
static Button* g_ButtonResume = NULL;
static Button* g_ButtonStop = NULL;
static Button* g_ButtonVolUp = NULL;
static Button* g_ButtonVolDown = NULL;
static s3eResult g_RegisterRes = S3E_RESULT_ERROR;     //Result of Registering end of track callback
static const char* g_TestVideo;
static bool g_ShouldResumeBackgroundMusic = false;
static int g_FreezeRotationWhenPlaying = 0;
/*
 * To pass Microsoft submission requirenments app have to satisfy rules for background music management.
 * On Windows Phone 8 application should prompt to pause background music during video playback.
 */
void pauseBackgroundMusic()
{
    if (s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WP8 &&
        s3eBackgroundMusicAvailable())
    {
        g_ShouldResumeBackgroundMusic = s3eBackgroundMusicGetInt(S3E_BACKGROUNDMUSIC_PLAYBACK_STATE) == S3E_BACKGROUNDMUSIC_PLAYBACK_PLAYING;
        s3eBackgroundMusicPause();
    }
}

/*
 * On Windows Phone 8 application should resume background music after video playback is complete
 */
void resumeBackgroundMusic()
{
    if (g_ShouldResumeBackgroundMusic)
    {
        s3eBackgroundMusicPlay();
        g_ShouldResumeBackgroundMusic = false;
    }
}

/*
 * The following function is called when a video file has been played in its
 * entirety. The function sets the state of playback to be the end of track, or
 * eot.
 */
static int32 videoStopped(void* systemData, void* userData)
{
    s3eDebugTracePrintf("videoStopped");
    if (g_FreezeRotationWhenPlaying)
    {
        s3eSurfaceSetInt(S3E_SURFACE_DEVICE_ORIENTATION_LOCK, S3E_SURFACE_ORIENTATION_FREE);
    }
    return true;
}

static void UpdateButtons()
{
    s3eVideoStatus state = (s3eVideoStatus)s3eVideoGetInt(S3E_VIDEO_STATUS);
    g_ButtonPause->m_Enabled = false;
    g_ButtonResume->m_Enabled = false;
    g_ButtonStop->m_Enabled = false;
    g_ButtonPlay->m_Enabled = false;
    g_ButtonPlayFull->m_Enabled = false;

    switch (state)
    {
        case S3E_VIDEO_FAILED:
        case S3E_VIDEO_STOPPED:
            g_ButtonPlay->m_Enabled = true;
            g_ButtonPlayFull->m_Enabled = true;
            break;
        case S3E_VIDEO_PAUSED:
            g_ButtonResume->m_Enabled = true;
            g_ButtonStop->m_Enabled = true;
            break;
        case S3E_VIDEO_PLAYING:
            g_ButtonPause->m_Enabled = true;
            g_ButtonStop->m_Enabled = true;
            break;
    }
}


/*
 * The following function reads a file into memory and registers an videoStopped
 * callback function to be called when the S3E_VIDEO_STOP event occurs.
 */
void ExampleInit()
{
    s3eConfigGetInt("APP", "FreezeRotationWhenPlaying", &g_FreezeRotationWhenPlaying);

    g_ButtonPlay     = NewButton("Play");
    g_ButtonPlayFull = NewButton("Play Fullscreen");
    g_ButtonPause    = NewButton("Pause");
    g_ButtonResume   = NewButton("Resume");
    g_ButtonStop     = NewButton("Stop");

    // Setting video volume programatically is not supported on iPhone and tvOS
    if (s3eDeviceGetInt(S3E_DEVICE_OS) != S3E_OS_ID_IPHONE &&
        s3eDeviceGetInt(S3E_DEVICE_OS) != S3E_OS_ID_TVOS)
    {
        g_ButtonVolUp    = NewButton("Volume Up");
        g_ButtonVolDown  = NewButton("Volume Down");
    }

    UpdateButtons();

    // Register a callback function for when a track has finished playing.
    g_RegisterRes = s3eVideoRegister(S3E_VIDEO_STOP, &videoStopped, NULL);

    if (s3eVideoIsCodecSupported(S3E_VIDEO_CODEC_3GPP_VIDEO_H263))
        g_TestVideo = TEST_VIDEO_3GP;
    else if (s3eVideoIsCodecSupported(S3E_VIDEO_CODEC_MPEG4_VIDEO_MPEG4))
        g_TestVideo = TEST_VIDEO_MP4;
    else if (s3eVideoIsCodecSupported(S3E_VIDEO_CODEC_MPEG4_VIDEO_H264))
        g_TestVideo = TEST_VIDEO_MOV;
}

/*
 * The following function frees the memory allocated by the example code.
 */
void ExampleTerm()
{
}

int32 buttonPressed(void* sysData, void* userData)
{
    s3ePointerUnRegister(S3E_POINTER_BUTTON_EVENT, buttonPressed);
    s3eVideoStop();
    return 0;
}

/*
 * This function locks the screen orientation based on the current aspect ratio.
 */
void lockScreenOrientation()
{
    if (g_FreezeRotationWhenPlaying)
    {
        bool isLandscape = s3eSurfaceGetInt(S3E_SURFACE_WIDTH) > s3eSurfaceGetInt(S3E_SURFACE_HEIGHT);
        s3eSurfaceSetInt(S3E_SURFACE_DEVICE_ORIENTATION_LOCK, isLandscape ?
                         S3E_SURFACE_LANDSCAPE : S3E_SURFACE_PORTRAIT);
    }
}

/*
 * The following function checks the state of the keyboard and calls the
 * relevant keyboard functionality.
 * Press s3eKey1 to play; s3eKey2 to pause; s3eKey3 to resume; and s3eKey4,
 * s3eKeyEsc or s3eKeyLS to stop.
 */
bool ExampleUpdate()
{
    if (!g_TestVideo)
    {
        return true;
    }

    Button* pressed = GetSelectedButton();
    if (pressed == g_ButtonPlayFull)
    {
        lockScreenOrientation();
        pauseBackgroundMusic();
        s3eVideoPlay(g_TestVideo);
        s3ePointerRegister(S3E_POINTER_BUTTON_EVENT, buttonPressed, NULL);
    }
    else if (pressed == g_ButtonPlay)
    {
        lockScreenOrientation();
        pauseBackgroundMusic();
        //play, setting display size and position.
        int y = GetYBelowButtons();
        int x = 10;
        int width = s3eSurfaceGetInt(S3E_SURFACE_WIDTH) - 20;
        int font_height = s3eDebugGetInt(S3E_DEBUG_FONT_HEIGHT);
        int height = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT) - GetYBelowButtons() - 4 * font_height;
        //prevent negative values. in case of negative heigth play video as one pixel strip.
        s3eVideoPlay(g_TestVideo, 1, x, y, width, (height < 0) ? 1 : height);
    }
    else if (pressed == g_ButtonPause)
    {
        resumeBackgroundMusic();
        s3eVideoPause();
    }
    else if (pressed == g_ButtonResume)
    {
        pauseBackgroundMusic();
        s3eVideoResume();
    }
    else if (pressed == g_ButtonStop)
    {
        resumeBackgroundMusic();
        s3eVideoStop();
    }

    if (pressed && pressed == g_ButtonVolUp)
    {
        int vol = s3eVideoGetInt(S3E_VIDEO_VOLUME);
        s3eVideoSetInt(S3E_VIDEO_VOLUME, vol+10);
    }

    if (pressed && pressed == g_ButtonVolDown)
    {
        int vol = s3eVideoGetInt(S3E_VIDEO_VOLUME);
        s3eVideoSetInt(S3E_VIDEO_VOLUME, vol-10);
    }

    return true;
}

#define TEXT_COLOR "`x666666"
#define CENTER_STRING(STR) s3eDebugPrint((width - ((int32)strlen(STR))*font_width)/2,  y,  TEXT_COLOR STR, 1)

/*
 * The following function prints out the current state of video playback.
 */
void ExampleRender()
{
    UpdateButtons();

    const int leftOffset = 10;
    const int rightOffset = 20;
    const uint8 red = 100;
    const uint8 green = 100;
    const uint8 blue = 100;

    int32 width = s3eSurfaceGetInt(S3E_SURFACE_WIDTH);
    int32 height = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT);
    int font_width = s3eDebugGetInt(S3E_DEBUG_FONT_WIDTH);
    int font_height = s3eDebugGetInt(S3E_DEBUG_FONT_HEIGHT);

    int bottom_border = 4 * font_height;
    int rectHeight = height - GetYBelowButtons() - bottom_border;
    int rectBottom = height-bottom_border;

    // prevent overturning of the video.
    if (rectHeight < 0)
    {
        rectHeight = 1;
        rectBottom = GetYBelowButtons();
    }

    DrawRect(leftOffset, GetYBelowButtons(), (width - rightOffset), 1, red, green, blue);
    DrawRect(leftOffset, GetYBelowButtons(), 1, rectHeight, red, green, blue);
    DrawRect(leftOffset + (width - rightOffset), GetYBelowButtons(), 1, rectHeight, red, green, blue);
    DrawRect(leftOffset, rectBottom, (width - rightOffset), 1, red, green, blue);

    if (g_RegisterRes == S3E_RESULT_ERROR)
        s3eDebugPrint((width - ((int32)strlen("videoStopped Failed to register"))*6)/2,
                      height-50, "`xfb0b0cvideoStopped Failed to register", 0);

    int y = height - 3*font_height;
    int pos = 0;

    if (g_TestVideo)
    {
        s3eVideoStatus state = (s3eVideoStatus)s3eVideoGetInt(S3E_VIDEO_STATUS);
        switch (state)
        {
            case S3E_VIDEO_PLAYING:
                CENTER_STRING("Video Playing");
                break;
            case S3E_VIDEO_PAUSED:
                CENTER_STRING("Video Paused");
                break;
            case S3E_VIDEO_STOPPED:
                resumeBackgroundMusic();
                CENTER_STRING("Video Stopped");
                break;
            case S3E_VIDEO_FAILED:
                CENTER_STRING("Video Playback Failed");
                break;
        }
        pos = s3eVideoGetInt(S3E_VIDEO_POSITION);
    }
    else
    {
        y = height - 50;
        CENTER_STRING("Neither 3GPP nor MPEG4 are supported");
    }

    y = height - 2*font_height;
    s3eDebugPrintf(100, y, 0, "`x666666Position: %02d.%02d    Volume: %d", pos / 1000, pos % 1000, s3eVideoGetInt(S3E_VIDEO_VOLUME));
}
