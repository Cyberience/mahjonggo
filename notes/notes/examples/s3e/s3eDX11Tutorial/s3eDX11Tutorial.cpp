/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EDX11Tutorial S3E DX11 Tutorial Example
 * The following example is converted from the DirectX 11 Tutorial04 example.
 *
 * This only works on platforms that support DX11 (Windows Desktop, Windows Store and Windows Phone).
 * This example will not work on Windows Phone 8 and Windows Store 8 as the sahders have not been pre-compiled.
 *
 * @include s3eDX11.cpp
 */

#include "s3eDX11.h"
#include "BasicMaths.h"
#include <string.h>

//--------------------------------------------------------------------------------------
// Structures
//--------------------------------------------------------------------------------------
struct SimpleVertex
{
    float3 Pos;
    float4 Color;
};


struct ConstantBuffer
{
    float4x4 mWorld;
    float4x4 mView;
    float4x4 mProjection;
};


//--------------------------------------------------------------------------------------
// Global Variables
//--------------------------------------------------------------------------------------
//HINSTANCE               g_hInst = NULL;
//HWND                    g_hWnd = NULL;
//D3D_DRIVER_TYPE         g_driverType = D3D_DRIVER_TYPE_NULL;
D3D_FEATURE_LEVEL       g_featureLevel = D3D_FEATURE_LEVEL_11_0;
ID3D11Device*           g_pd3dDevice = NULL;
ID3D11DeviceContext*    g_pImmediateContext = NULL;
//IDXGISwapChain*         g_pSwapChain = NULL;
ID3D11RenderTargetView* g_pRenderTargetView = NULL;
ID3D11VertexShader*     g_pVertexShader = NULL;
ID3D11PixelShader*      g_pPixelShader = NULL;
ID3D11InputLayout*      g_pVertexLayout = NULL;
ID3D11Buffer*           g_pVertexBuffer = NULL;
ID3D11Buffer*           g_pIndexBuffer = NULL;
ID3D11Buffer*           g_pConstantBuffer = NULL;
float4x4        g_World;
float4x4        g_View;
float4x4        g_Projection;


//--------------------------------------------------------------------------------------
// Forward declarations
//--------------------------------------------------------------------------------------
HRESULT InitDevice();
void CleanupDevice();
//LRESULT CALLBACK    WndProc( HWND, UINT, WPARAM, LPARAM );
void Render();

bool g_SizeChanged = false;

// on resize called signal size changed (handled at end of frame)
int32 ResizeCalled(void* systemData, void* userData)
{
    g_SizeChanged = true;

    return S3E_RESULT_SUCCESS;
}

// on pause release all dx11 resources
int32 PauseCalled(void* systemData, void* userData)
{
    CleanupDevice();

    return S3E_RESULT_SUCCESS;
}

// on resume re-create all dx11 resources
int32 UnPauseCalled(void* systemData, void* userData)
{
    InitDevice();

    return S3E_RESULT_SUCCESS;
}

void CreateView(int width, int height)
{
    // Create a render target view
    ID3D11Texture2D* pBackBuffer = s3eDX11GetBackBuffer();
    if(pBackBuffer == NULL)
        return;

    HRESULT hr = g_pd3dDevice->CreateRenderTargetView( pBackBuffer, NULL, &g_pRenderTargetView );
    pBackBuffer->Release();
    if(FAILED( hr) )
        return;

    g_pImmediateContext->OMSetRenderTargets( 1, &g_pRenderTargetView, NULL );

    // Setup the viewport
    D3D11_VIEWPORT vp;
    vp.Width = (FLOAT)width;
    vp.Height = (FLOAT)height;
    vp.MinDepth = 0.0f;
    vp.MaxDepth = 1.0f;
    vp.TopLeftX = 0;
    vp.TopLeftY = 0;
    g_pImmediateContext->RSSetViewports( 1, &vp );
    ID3D11Texture2D* buffer = s3eDX11GetBackBuffer();

    // Initialize the projection matrix
    g_Projection = PerspectiveFovLH( (float)M_PI_2, width / (float)height, 0.01f, 100.0f );
}

// check for size or fullscreen changed and handle
// fullscreen has specified size, windowed takes size from surface
void Check()
{
    if (!g_SizeChanged)
        return;

    int width = s3eSurfaceGetInt(S3E_SURFACE_DEVICE_WIDTH);
    int height = s3eSurfaceGetInt(S3E_SURFACE_DEVICE_HEIGHT);

    g_pRenderTargetView->Release();
    g_pRenderTargetView = NULL;

    g_pImmediateContext->OMSetRenderTargets(0, NULL, NULL);

    s3eDX11Resize(width, height);

    CreateView(width, height);

    g_SizeChanged = false;
}

//--------------------------------------------------------------------------------------
// Entry point to the program. Initializes everything and goes into a message processing
// loop. Idle time is used to render the scene.
//--------------------------------------------------------------------------------------
int main()
{
    if(FAILED( InitDevice() ) )
    {
        CleanupDevice();
        return 0;
    }

    // register callbacks
    s3eSurfaceRegister(S3E_SURFACE_SCREENSIZE, ResizeCalled, NULL);
    s3eDeviceRegister(S3E_DEVICE_PAUSE, PauseCalled, NULL);
    s3eDeviceRegister(S3E_DEVICE_UNPAUSE, UnPauseCalled, NULL);

    while (!s3eDeviceCheckQuitRequest())
    {
        // check for Marmalade events
        s3eDeviceYield(0);
        Render();

        Check();

        // swap buffers
        s3eDX11Present();
    }

    // un-register callbacks
    s3eDeviceUnRegister(S3E_DEVICE_PAUSE, PauseCalled);
    s3eDeviceUnRegister(S3E_DEVICE_UNPAUSE, UnPauseCalled);
    s3eSurfaceUnRegister(S3E_SURFACE_SCREENSIZE, ResizeCalled);

    CleanupDevice();

    return 0;
}


//--------------------------------------------------------------------------------------
// Helper for compiling shaders with D3DX11
//--------------------------------------------------------------------------------------
HRESULT CompileShaderFromFile(const char* szFileName, const char* szEntryPoint, const char* szShaderModel, ID3DBlob** ppBlobOut )
{
    HRESULT hr = S_OK;

    s3eDX11Compiler* compiler = s3eDX11GetCompiler();
    if (compiler == NULL)
        return S_FALSE;

    s3eFile* fp = s3eFileOpen(szFileName, "rb");
    if (fp == NULL) return S_FALSE;

    int size = s3eFileGetSize(fp);
    char* buffer = new char[size];

    s3eFileRead(buffer, 1, size, fp);
    s3eFileClose(fp);

    UINT dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined(DEBUG) || defined( _DEBUG )
    // Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
    // Setting this flag improves the shader debugging experience, but still allows
    // the shaders to be optimized and to run exactly the way they will run in
    // the release configuration of this program.
    dwShaderFlags |= D3DCOMPILE_DEBUG;
#endif

    ID3DBlob* pErrorBlob;
    hr = compiler->Compile(buffer, size, szFileName, NULL, NULL, szEntryPoint, szShaderModel,
        dwShaderFlags, 0, ppBlobOut, &pErrorBlob );

    delete[] buffer;

    if(FAILED(hr) )
    {
        if(pErrorBlob != NULL)
            s3eDebugErrorShow(S3E_MESSAGE_CONTINUE_STOP, (char*)pErrorBlob->GetBufferPointer());
        if(pErrorBlob) pErrorBlob->Release();
        return hr;
    }
    if(pErrorBlob) pErrorBlob->Release();

    return S_OK;
}


//--------------------------------------------------------------------------------------
// Create Direct3D device and swap chain
//--------------------------------------------------------------------------------------
HRESULT InitDevice()
{
    HRESULT hr = S_OK;

    D3D_FEATURE_LEVEL level = s3eDX11Initialize(0, 0);

    if(level == D3D_FEATURE_LEVEL_UNKNOWN)
        return S_FALSE;

    g_pd3dDevice = s3eDX11GetDevice();
    if(g_pd3dDevice == NULL)
        return S_FALSE;

    g_pd3dDevice->GetImmediateContext(&g_pImmediateContext);

    // Create a render target view
    ID3D11Texture2D* pBackBuffer = s3eDX11GetBackBuffer();
    if(pBackBuffer == NULL)
        return S_FALSE;

    D3D11_TEXTURE2D_DESC desc;
    pBackBuffer->GetDesc(&desc);

    hr = g_pd3dDevice->CreateRenderTargetView( pBackBuffer, NULL, &g_pRenderTargetView );
    pBackBuffer->Release();
    if(FAILED( hr) )
        return hr;

    g_pImmediateContext->OMSetRenderTargets( 1, &g_pRenderTargetView, NULL );

    // Setup the viewport
    D3D11_VIEWPORT vp;
    vp.Width = (FLOAT)desc.Width;
    vp.Height = (FLOAT)desc.Height;
    vp.MinDepth = 0.0f;
    vp.MaxDepth = 1.0f;
    vp.TopLeftX = 0;
    vp.TopLeftY = 0;
    g_pImmediateContext->RSSetViewports( 1, &vp );

    // Compile the vertex shader
    ID3DBlob* pVSBlob = NULL;
    hr = CompileShaderFromFile( "Tutorial04.fx", "VS", "vs_4_0_level_9_1", &pVSBlob );
    if(FAILED( hr) )
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE_STOP, "The FX file cannot be compiled." );
        return hr;
    }

    // Create the vertex shader
    hr = g_pd3dDevice->CreateVertexShader( pVSBlob->GetBufferPointer(), pVSBlob->GetBufferSize(), NULL, &g_pVertexShader );
    if(FAILED( hr) )
    {
        pVSBlob->Release();
        return hr;
    }

    // Define the input layout
    D3D11_INPUT_ELEMENT_DESC layout[] =
    {
        { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
        { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
    };
    UINT numElements = 2;

    // Create the input layout
    hr = g_pd3dDevice->CreateInputLayout( layout, numElements, pVSBlob->GetBufferPointer(),
                                          pVSBlob->GetBufferSize(), &g_pVertexLayout );
    pVSBlob->Release();
    if(FAILED( hr) )
        return hr;

    // Set the input layout
    g_pImmediateContext->IASetInputLayout( g_pVertexLayout );

    // Compile the pixel shader
    ID3DBlob* pPSBlob = NULL;
    hr = CompileShaderFromFile( "Tutorial04.fx", "PS", "ps_4_0_level_9_1", &pPSBlob );
    if(FAILED( hr) )
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE_STOP, "The FX file cannot be compiled." );
        return hr;
    }

    // Create the pixel shader
    hr = g_pd3dDevice->CreatePixelShader( pPSBlob->GetBufferPointer(), pPSBlob->GetBufferSize(), NULL, &g_pPixelShader );
    pPSBlob->Release();
    if(FAILED( hr) )
        return hr;

    // Create vertex buffer
    SimpleVertex vertices[] =
    {
        { float3( -1.0f, 1.0f, -1.0f ), float4( 0.0f, 0.0f, 1.0f, 1.0f ) },
        { float3( 1.0f, 1.0f, -1.0f ), float4( 0.0f, 1.0f, 0.0f, 1.0f ) },
        { float3( 1.0f, 1.0f, 1.0f ), float4( 0.0f, 1.0f, 1.0f, 1.0f ) },
        { float3( -1.0f, 1.0f, 1.0f ), float4( 1.0f, 0.0f, 0.0f, 1.0f ) },
        { float3( -1.0f, -1.0f, -1.0f ), float4( 1.0f, 0.0f, 1.0f, 1.0f ) },
        { float3( 1.0f, -1.0f, -1.0f ), float4( 1.0f, 1.0f, 0.0f, 1.0f ) },
        { float3( 1.0f, -1.0f, 1.0f ), float4( 1.0f, 1.0f, 1.0f, 1.0f ) },
        { float3( -1.0f, -1.0f, 1.0f ), float4( 0.0f, 0.0f, 0.0f, 1.0f ) },
    };
    D3D11_BUFFER_DESC bd;
    memset( &bd, 0, sizeof(bd) );
    bd.Usage = D3D11_USAGE_DEFAULT;
    bd.ByteWidth = sizeof( SimpleVertex ) * 8;
    bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    bd.CPUAccessFlags = 0;
    D3D11_SUBRESOURCE_DATA InitData;
    memset( &InitData, 0, sizeof(InitData) );
    InitData.pSysMem = vertices;
    hr = g_pd3dDevice->CreateBuffer( &bd, &InitData, &g_pVertexBuffer );
    if(FAILED( hr) )
        return hr;

    // Set vertex buffer
    UINT stride = sizeof( SimpleVertex );
    UINT offset = 0;
    g_pImmediateContext->IASetVertexBuffers( 0, 1, &g_pVertexBuffer, &stride, &offset );

    // Create index buffer
    unsigned short indices[] =
    {
        3,1,0,
        2,1,3,

        0,5,4,
        1,5,0,

        3,4,7,
        0,4,3,

        1,6,5,
        2,6,1,

        2,7,6,
        3,7,2,

        6,4,5,
        7,4,6,
    };
    bd.Usage = D3D11_USAGE_DEFAULT;
    bd.ByteWidth = sizeof(unsigned short) * 36;        // 36 vertices needed for 12 triangles in a triangle list
    bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
    bd.CPUAccessFlags = 0;
    InitData.pSysMem = indices;
    hr = g_pd3dDevice->CreateBuffer( &bd, &InitData, &g_pIndexBuffer );
    if(FAILED( hr) )
        return hr;

    // Set index buffer
    g_pImmediateContext->IASetIndexBuffer( g_pIndexBuffer, DXGI_FORMAT_R16_UINT, 0 );

    // Set primitive topology
    g_pImmediateContext->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );

    // Create the constant buffer
    bd.Usage = D3D11_USAGE_DEFAULT;
    bd.ByteWidth = sizeof(ConstantBuffer);
    bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    bd.CPUAccessFlags = 0;
    hr = g_pd3dDevice->CreateBuffer( &bd, NULL, &g_pConstantBuffer );
    if(FAILED( hr) )
        return hr;

    // Initialize the world matrix
    g_World = identity();

    // Initialize the view matrix
    float3 Eye = float3( 0.0f, 1.0f, -5.0f );
    float3 At = float3( 0.0f, 1.0f, 0.0f );
    float3 Up = float3( 0.0f, 1.0f, 0.0f );
    g_View = LookAtLH( Eye, At, Up );

    // Initialize the projection matrix
    g_Projection = PerspectiveFovLH( (float)M_PI_2, desc.Width / (float)desc.Height, 0.01f, 100.0f );

    return S_OK;
}


//--------------------------------------------------------------------------------------
// Clean up the objects we've created
//--------------------------------------------------------------------------------------
void CleanupDevice()
{
    if(g_pImmediateContext) g_pImmediateContext->ClearState();

    if(g_pConstantBuffer) g_pConstantBuffer->Release();
    if(g_pVertexBuffer) g_pVertexBuffer->Release();
    if(g_pIndexBuffer) g_pIndexBuffer->Release();
    if(g_pVertexLayout) g_pVertexLayout->Release();
    if(g_pVertexShader) g_pVertexShader->Release();
    if(g_pPixelShader) g_pPixelShader->Release();
    if(g_pRenderTargetView) g_pRenderTargetView->Release();
    //if(g_pSwapChain) g_pSwapChain->Release();
    if(g_pImmediateContext) g_pImmediateContext->Release();
    if(g_pd3dDevice) g_pd3dDevice->Release();

    s3eDX11Shutdown();
}


//--------------------------------------------------------------------------------------
// Render a frame
//--------------------------------------------------------------------------------------
void Render()
{
    // Update our time
    static float t = 0.0f;
    static int64 dwTimeStart = 0;
    int64 dwTimeCur = s3eTimerGetUST();
    if(dwTimeStart == 0)
        dwTimeStart = dwTimeCur;
    t = ( dwTimeCur - dwTimeStart ) / 1000.0f;

    //
    // Animate the cube
    //
    g_World = rotationY( t * 100.0f );

    //
    // Clear the back buffer
    //
    float ClearColor[4] = { 0.0f, 0.125f, 0.3f, 1.0f }; // red,green,blue,alpha
    g_pImmediateContext->ClearRenderTargetView( g_pRenderTargetView, ClearColor );

    //
    // Update variables
    //
    ConstantBuffer cb;
    cb.mWorld = transpose( g_World );
    cb.mView = transpose( g_View );
    cb.mProjection = transpose( g_Projection );
    g_pImmediateContext->UpdateSubresource( g_pConstantBuffer, 0, NULL, &cb, 0, 0 );

    //
    // Renders a triangle
    //
    g_pImmediateContext->VSSetShader( g_pVertexShader, NULL, 0 );
    g_pImmediateContext->VSSetConstantBuffers( 0, 1, &g_pConstantBuffer );
    g_pImmediateContext->PSSetShader( g_pPixelShader, NULL, 0 );
    g_pImmediateContext->DrawIndexed(36, 0, 0);        // 36 vertices needed for 12 triangles in a triangle list
}
