/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3ELauncher S3E Launcher Example
 *
 * The following example demonstrates how to launch other applications from a
 * base application using S3E's device interface functionality.
 *
 * The main functions used to achieve this are:
 *  - s3eDeviceExecPushNext()
 *  - s3eDeviceExit()
 *
 * The applications builds a list of sub applications by looking for folders
 * in .\data\launcher. It then assigns buttons to launch each of these and
 * displays this to the user. S3E uses a push/pop stack to queue
 * applicationsto be launched after the current one exits. This example waits
 * for the relevant button to be pressed and then pushes itself followed by
 * the relative sub-application to the stack using s3eDeviceExecPushNext(). It
 * then exits using s3eDeviceExit(), at which point the subapp is popped from
 * the stack and run. On exiting the subapp, the original application is
 * popped and re-run.
 *
 * Detection of button presses is handled using generic code in
 * ExamplesMain.cpp. The state of playback is output to screen using the
 * s3eDebugPrint() function.
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eLauncherImage.png
 *
 * @include s3eLauncher.cpp
 */
#include "ExamplesMain.h"
#include "IwPath.h"
#include "IwDebug.h"
#include <sys/stat.h>
#include <dirent.h>

#include <vector>
#include <string>

//Base directory were launch applications subfolders are situated
#define APP_FOLDER "."

static std::vector<std::string>   g_Applist;  //List of launch applications
static std::vector<char*>         g_Desclist; //List of matching descriptions
static unsigned int               g_App = 0;  //Index into list of applications

/*
 * Reads in a list of applications from the data\launcher folder for
 * subsequent launching
 */
void ExampleInit()
{
    // Navigate to basedirectory were launch applications are situated
    if (chdir(APP_FOLDER))
    {
        IwError(("launcher folder not found: %s", APP_FOLDER));
        return;
    }

    // Open directory for reading in all subfolders in which the launch applications are situated
    DIR* dir = opendir(".");

    struct dirent *dirent;

    // Read in a list of applications for launching
    while ((dirent = readdir(dir)))
    {
        if (dirent->d_type == DT_DIR)
        {
            g_Applist.push_back(dirent->d_name);

            // Store user friendly descriptions to display. The button
            // code needs these strings to persist in memory
            char* description = new char[256];
            sprintf(description, "launch subapp: %s", dirent->d_name);
            g_Desclist.push_back(description);
            NewButton(g_Desclist[g_Desclist.size()-1]);
        }
    }

    // dirent operation is finished so directory can be closed now
    closedir(dir);
}

/*
 * Do cleanup work
 */
void ExampleTerm()
{
    for (std::vector<char*>::iterator i = g_Desclist.begin(); i < g_Desclist.end(); i++)
    {
        delete[] (*i);
    }
}

/*
 * Assemble filename of application to launch and push it onto the stack.
 * Beforehand push this application onto the stack so it gets restarted
 * after the launched application has quit.
 */
void LaunchGame()
{
    // Get basefolder in which the applications subfolders are situated
    char folder[S3E_FILE_MAX_PATH];
    strcpy(folder, APP_FOLDER);

    // Append folder name in which application resides
    IwPathJoin(folder, g_Applist[g_App].c_str(), S3E_FILE_MAX_PATH);

    // Append filename of application
    IwPathJoin(folder, g_Applist[g_App].c_str(), S3E_FILE_MAX_PATH);

    // Push the current application onto the device exec stack so it
    // will be launched after sample application has quit.
    s3eDeviceExecPushNext(NULL);

    // Specify which application to launch after s3eLauncher has quit
    if (s3eDeviceExecPushNext(folder) == S3E_RESULT_ERROR)
    {
        s3eDeviceError err = s3eDeviceGetError();

        if (err == S3E_DEVICE_ERR_UNSUPPORTED)
        {
            IwError(("s3eDeviceExecPushNext unsupported on this platform!") );
        }
        else
        {
            IwError(("failed to launch app: %s", s3eDeviceGetErrorString()));
        }
    }

    // Quit this application in order to start the next one on the stack
    s3eDeviceExit();
}

/*
 * Process button state and launch subapp
 */
bool ExampleUpdate()
{
    Button *pressed = GetSelectedButton();

    if (pressed)
    {
        g_App = pressed->m_Index;
        LaunchGame();
    }

    return true;
}

/*
 * Display launch application text
 */
void ExampleRender()
{
    const int textHeight = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);
    int ypos = GetYBelowButtons() + textHeight * 2;

    s3eDebugPrint((s3eSurfaceGetInt(S3E_SURFACE_WIDTH)-strlen("Launch an application")*6)/2, ypos, "`x666666Launch an application", 0);
}
