/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3ETimerAdvanced S3E Timer Advanced Example
 *
 * The following example uses S3E's timer functionality to create an alarm clock.
 * The alarm clock has a snooze function that, when called, turns the alarm off
 * for ten seconds before it is turned back on again.
 *
 * The functions used to achieve this are:
 *  - s3eTimerGetMs()
 *  - s3eTimerSetTimer()
 *  - s3eAudioPlay()
 *  - s3eAudioStop()
 *
 * The ExampleInit() function sets the Alarm() function as a callback function,
 * which will be called after the period specified by @e ALARM_DELAY. This
 * is done using the s3eTimerSetTimer() function.
 *
 * The ExampleUpdate() function calculates the current time in hours,
 * minutes and seconds and compares that time with the time you have set for
 * the alarm, thus enabling it to print to screen the amount of time left till
 * the alarm goes off.
 *
 * When the alarm sounds you can hit the s3eKey0 button to call the snooze
 * function to stop the alarm for a further ten seconds. The snooze function
 * uses the s3eTimerSetTimer() function, which, once again, sets the alarm
 * function as a callback that will be called after the period specified
 * by @e ALARM_DELAY.
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eTimerAdvancedImage.png
 *
 * @include s3eTimerAdvanced.cpp
 */

#include "ExamplesMain.h"

#define ALARM_DELAY 10000

char    g_TimeString[64];
bool    g_AlarmOn = false;
uint64  g_AlarmStart = 0;

/*
 * The following function plays the loaded wav file on a free channel 100 times,
 * or until you quit the application.
 * It is a callback function passed to s3eTimerSetTimer (see Snooze()).
 */
static int32 Alarm(void *systemData, void *userData)
{
    s3eAudioPlay("siren.wav",1);
    g_AlarmStart = s3eTimerGetMs()+ALARM_DELAY;
    s3eTimerSetTimer(ALARM_DELAY, &Alarm, NULL);
    g_AlarmOn = true;
    return 0;
}

/*
 * The following function, when called, stops the alarm using the s3eAudioStop()
 * function. It resets the callback to the alarm function using the
 * s3eTimerSetTimer(). This will restart the alarm after the amount of time
 * after the period specified by ALARM_DELAY.
 */
static void Snooze()
{
    s3eAudioStop();
    g_AlarmStart = s3eTimerGetMs()+ALARM_DELAY;
    s3eTimerSetTimer(ALARM_DELAY, &Alarm, NULL);
    g_AlarmOn = false;
}

/*
 * The following function sets the time at which the alarm by using the
 * s3eTimerSetTimer() function to call Alarm() after after the period specified
 * by ALARM_DELAY.
 */
void ExampleInit()
{
    g_AlarmStart = s3eTimerGetMs()+ALARM_DELAY;
    s3eTimerSetTimer(ALARM_DELAY, &Alarm, NULL);
}

void ExampleTerm()
{
}

/*
 * The following function gets the current time and converts it to hours,
 * minutes and seconds. It also checks the keyboard state to see if you have
 * pressed a key to snooze.
 */
bool ExampleUpdate()
{
    int16 timeTillAlarm = (int16) (g_AlarmStart - s3eTimerGetMs());

    if (g_AlarmOn)
        snprintf(g_TimeString, 63, "`x666666ALARM!!!");
    else
        snprintf(g_TimeString, 63, "`x666666Time till Alarm %i", timeTillAlarm);

    bool userTrigger = false;

    // Press 1 to start decompression
    if (s3eKeyboardGetState(s3eKey0) & S3E_KEY_STATE_PRESSED)
        userTrigger = true;

    // touch the screen to start decompression
    if (s3ePointerGetState(S3E_POINTER_BUTTON_SELECT) & S3E_POINTER_STATE_RELEASED)
        userTrigger = true;

    if (userTrigger && (g_AlarmOn == true))
        Snooze();

    return true;
}


/*
 * The following function displays the current state of the alarm and
 * instructions on how to turn on the snooze function.
 */
void ExampleRender()
{
    const int textWidth = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_WIDTH);
    const int textHeight = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);
    const int width = s3eSurfaceGetInt(S3E_SURFACE_WIDTH);
    const int height = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT);

    // Displaying clock centered vertically and horizontally
    s3eDebugPrint((width-(strlen(g_TimeString)-8)*textWidth)/2,
                  (height/2) - textHeight, g_TimeString, 1);

    static int64 sprintf_speed = -1;

    if (s3eTimerGetUSTNanoseconds() == -1)
    {
        s3eDebugPrint(10, height-100, "`x666666High resolution timer is not present", 1);
    }
    else
    {
        if (sprintf_speed == -1)
        {
            char test[100];
            uint64 start = s3eTimerGetUSTNanoseconds();
            sprintf(test, "just a %d test", 430);
            uint64 end = s3eTimerGetUSTNanoseconds();
            sprintf_speed = end - start;
        }
        s3eDebugPrintf(10, height-100, 1, "`x666666High res timer measured speed of sprintf: %lldns", sprintf_speed);
    }

    if (g_AlarmOn)
    {
        if (g_DeviceHasKeyboard)
            s3eDebugPrint((width-strlen("Press zero to snooze")*textWidth)/2,
                          (height/2) + textHeight, "`x666666Press zero to snooze", 1);
        else
            s3eDebugPrint((width-strlen("Touch screen to snooze")*textWidth)/2,
                          (height/2) + textHeight, "`x666666Touch screen to snooze", 1);
    }
}
