/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EMemory S3E Memory Example
 *
 * The example application allows you to create up to two memory heaps by
 * pressing s3eKey1. The application also allows you to upload a file multiple
 * times to fill each heap, this can be done by pressing s3eKey3. You can swap
 * between heaps by pressing s3eKey4. Finally, you can destroy each heap and
 * free up the memory it uses by pressing s3eKey2.
 *
 * The main functions used to achieve this are:
 *  - s3eMemoryHeapCreate()
 *  - s3eMemoryHeapDestroy()
 *  - s3eFree()
 *  - s3eMalloc()
 *  - s3eMemorySetInt()
 *
 * This example application does not rely on the icf file to initialise a memory
 * heap. Instead the icf file is used to define two memory heaps that will be
 * created by the example application and used to allocate memory dynmaically.
 * The possible heaps are listed in the ICF file as 0 and 1.
 *
 * Each heap is defined using the <code>MemSizeX; = size</code> icf setting,
 * which is placed under the <code>[S3E]</code> configuration group in the icf file.
 * <code>X</code> defines the heap number, and <code>size</code> defines the
 * size of the specified heap. The following is used in this examples icf:
 *
 * @code
 * [S3E]
 * MemSize0 = 6000000
 * MemFlags0 = INACTIVE
 * MemSize1 = 2000000
 * MemFlags1 = INACTIVE
 * @endcode
 *
 * @note There can be a maximum of eight memory heaps.
 *
 * In the example application the <code>INACTIVE</code> flag is attached to each
 * heap, using the <code>MemFlags<x> = flagsList...</code>, to indicate that
 * the heap should not be initialised on startup, but should only be created when
 * the application makes a call to s3eMemoryHeapCreate().
 *
 * It is good practice to free all the pointers that are present in a non-empty
 * heap before you destroy it.
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eMemoryImage.png
 *
 * @include s3eMemory.cpp
 */

#include "ExamplesMain.h"

static char     g_ResultString[256];    // Stores operation results for displaying
static int32    g_CurrentHeap;          // Keeps track which heap is currently used for operating on
static int8     g_NumHeap;              // Keeps track of the number of created heaps

s3eMemoryError  g_MemError;             // Checks errors for displaying information

// list of pointers which are used for uploading files
static void*    g_PtrList0[10];
static void*    g_PtrList1[10];

// Indices into the pointer lists for uploading files
static int      g_Index0;
static int      g_Index1;

// File handle for file upload.
static s3eFile* g_FileHandle;

static Button*  g_ButtonCreate;
static Button*  g_ButtonDelete;
static Button*  g_ButtonUpload;
static Button*  g_ButtonSwap;

void ExampleInit()
{
    g_MemError = S3E_MEMORY_ERR_NONE;

    g_FileHandle = s3eFileOpen("test.mp3", "rb");

    g_ButtonCreate  = NewButton("Create a new heap");
    g_ButtonDelete  = NewButton("Delete heap");
    g_ButtonUpload  = NewButton("Upload file");
    g_ButtonSwap    = NewButton("Swap heap");
}

void ExampleTerm()
{
    s3eFileClose(g_FileHandle);
}

/*
 * The following function, based on which button was pressed, creates a new heap,
 * destroys a heap, swaps between heaps, uploads a file
 * to the selected heap.
 *
 * When the contents of a file are uploaded
 * to a heap, the pointer to the position in memory where the file was
 * loaded is stored in an array, which is kept so that the memory can be freed
 * upon calling of s3eMemoryHeapDestroy().
 */
bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    // Reset error, so old errors don't get displayed any more
    g_MemError = S3E_MEMORY_ERR_NONE;

    // create a new heap
    if (pressed == g_ButtonCreate)
    {
        // In this example only one extra heap can be created
        if (g_NumHeap != 0)
        {
            sprintf(g_ResultString, "`x666666Maximum heap number reached");
            return true;
        }

        if (s3eMemoryHeapCreate (g_NumHeap+1) == S3E_RESULT_ERROR)
        {
            sprintf(g_ResultString, "`x666666Creating a new heap failed");
            g_MemError = s3eMemoryGetError();
        }
        else
        {
            g_NumHeap++;
            sprintf(g_ResultString, "`x666666Heap successfully created");
        }

        return true;
    }

    // destroy a heap
    if (pressed == g_ButtonDelete)
    {
        if (g_CurrentHeap == 0)
        {
            sprintf(g_ResultString, "`x666666Can't destroy heap 0");
            return true;
        }

        // In destroying a heap, each pointer in the relevant pointer
        // array and the memory it points to is freed using s3eFree().
        if (g_CurrentHeap == 1 && g_Index1 > 0)
        {
            while (g_Index1)
            {
                s3eFree(g_PtrList1[g_Index1]);
                g_Index1--;
            }
        }

        if (s3eMemoryHeapDestroy (g_CurrentHeap, true) == S3E_RESULT_SUCCESS)
        {
            sprintf(g_ResultString, "`x666666Heap destroying successful");
            g_NumHeap--;

            //Change displaying of active heap
            s3eMemorySetInt(S3E_MEMORY_HEAP, g_NumHeap);
        }
        else
        {
            sprintf(g_ResultString, "`x666666Error destroying Heap");
            g_MemError = s3eMemoryGetError();
        }
    }

    // upload a file
    if (pressed == g_ButtonUpload)
    {
        // Prepare data for uploading
        int32 fileSize = s3eFileGetSize(g_FileHandle);

        // Check for free heap space
        if (s3eMemoryGetInt (S3E_MEMORY_FREE) < fileSize)
        {
            sprintf(g_ResultString, "`x666666Not enough memory left");
            return true;
        }

        // Reserve memory for storing data
        void *ptr = s3eMalloc(fileSize);
        s3eFileRead (ptr, fileSize, 1, g_FileHandle);

        // Store data in heap
        if (g_CurrentHeap == 0)
        {
            g_Index0++;
            g_PtrList0[g_Index0] = ptr;
        }

        if (g_CurrentHeap == 1)
        {
            g_Index1++;
            g_PtrList1[g_Index1] = ptr;
        }

        sprintf(g_ResultString, "`x666666File uploaded correctly");
        return true;
    }

    // Press button to swap heap
    if (pressed == g_ButtonSwap)
    {
        if (g_NumHeap != 0)
            s3eMemorySetInt(S3E_MEMORY_HEAP, !g_CurrentHeap);
    }

    return true;
}

/*
 * The following function outputs a set of strings describing the state of
 * the heaps that have been created and the amount of memory that is free as
 * well as the amount of memory that is in use.
 */
void ExampleRender()
{
    const int textHeight = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);

    // Displaying Heap information
    g_CurrentHeap = s3eMemoryGetInt(S3E_MEMORY_HEAP);  // Current Heap
    int32 heapSize = s3eMemoryGetInt(S3E_MEMORY_SIZE); // Total size of the current heap
    int32 heapFree = s3eMemoryGetInt(S3E_MEMORY_FREE); // Total amount of free space left on the current heap
    int32 heapUsed = s3eMemoryGetInt(S3E_MEMORY_USED); // Free memory minus heap size
    int ypos = GetYBelowButtons() + 4 * textHeight;

    if (g_NumHeap != 0)
    {
        g_ButtonCreate->m_Enabled = false;
        g_ButtonDelete->m_Enabled = true;
        g_ButtonUpload->m_Enabled = true;
        g_ButtonSwap->m_Enabled = true;
    }
    else
    {
        g_ButtonCreate->m_Enabled = true;
        g_ButtonDelete->m_Enabled = false;
        g_ButtonUpload->m_Enabled = true;
        g_ButtonSwap->m_Enabled = false;
    }

    s3eDebugPrintf(10, ypos, 0, "`x666666Number Heaps : %i\nCurrent Heap : %d\nHeap size    : %d\nFree memory  : %d\nUsed memory  : %d",
                   g_NumHeap, g_CurrentHeap, heapSize, heapFree, heapUsed);

    ypos += 6 * textHeight;

    // Displaying error status
    s3eDebugPrint(10, ypos, g_ResultString, 1);

    ypos += 2 * textHeight;

    if (g_MemError != S3E_MEMORY_ERR_NONE)
    {
        switch (g_MemError)
        {
            case S3E_MEMORY_ERR_PARAM:          s3eDebugPrint(10, ypos, "`x666666S3E_MEMORY_ERR_PARAM", 0);         break;
            case S3E_MEMORY_ERR_INVALID_PTR:    s3eDebugPrint(10, ypos, "`x666666S3E_MEMORY_ERR_INVALID_PTR", 0);   break;
            case S3E_MEMORY_ERR_NO_MEM:         s3eDebugPrint(10, ypos, "`x666666S3E_MEMORY_ERR_NO_MEM", 0);            break;
            case S3E_MEMORY_ERR_INVALID_STATE:  s3eDebugPrint(10, ypos, "`x666666S3E_MEMORY_ERR_INVALID_STATE", 0); break;
            default:                            s3eDebugPrint(10, ypos, "`x666666S3E_MEMORY_ERR_UNKNOWN", 0);       break;
        }
    }
}
