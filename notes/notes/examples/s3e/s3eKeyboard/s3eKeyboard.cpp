/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EKeyboard S3E Keyboard Example
 *
 * This example reads key presses from the device's keyboard/keypad using
 * s3eKeyboard APIs and displays them on screen.
 *
 * The main functions and structs used to achieve this are:
 * - s3eKey
 * - s3eKeyboardAnyKey()
 * - s3eKeyboardGetState()
 * - s3eKeyboardGetChar()
 * - s3eKeyboardGetInt()
 * - s3eKeyboardRegister()
 *
 * S3E supports detecting key presses in two main ways:
 *
 * - Characters: S3E will generate standard characters as wide chars
 *   (s3eWChar) from key-presses and buffer them internally.
 *   This is useful for text entry but is not supported on all devices.
 *
 * - Key State: Physical keys, represented by the s3eKey type, can have states
 *   of up (not pressed), down (being held down), or pressed (was down and has
 *   been released since the last keyboard update). s3eKeyboardGetState() is
 *   used to check the sate of a key any given moment, while s3eKeyboardAnyKey()
 *   is used to return the last key that was pressed.
 *
 * This example reads in characters pressed, using s3eKeyboardGetChar().
 * Repeated calls to this function will return characters from the internal
 * buffer until it is empty.
 *
 * It also displays keys pressed and released. These could be be polled by
 * calling s3eKeyboardGetState() for every key. Instead, this example registers
 * a callback function using s3eKeyboardRegister(S3E_KEYBOARD_KEY_EVENT...)
 * which will be called whenever a key state changes.
 *
 * The last 5 characters returned and the last 5 changes of key state
 * are displayed using s3eDebugPrint().
 *
 * Note that not all devices support character input, in which case the call
 * to enable it will simply fail.
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eKeyboardImage.png
 *
 * @include s3eKeyboard.cpp
 */

#include "ExamplesMain.h"
#include <string.h>

// Display last 5 characters generated
#define NUM_CHARS 5
static s3eWChar g_Chars[NUM_CHARS];
static int g_NumChars = 0;

// Last 5 keys pressed down
static s3eKey g_Keys[NUM_CHARS];
static int g_NumKeys = 0;

// Last 5 keys released
static s3eKey g_KeysReleased[NUM_CHARS];
static int g_NumKeysReleased = 0;

// Last key that was "pressed" (has just been released)
static s3eKey g_LastKeyPressed = s3eKeyFirst;

static int g_CharEnabled = 0;
static Button* g_ButtonChar = 0;

// Callback event gets called whenever a key state changes
static int32 handler(void* sys, void*)
{
    s3eKeyboardEvent* event = (s3eKeyboardEvent*)sys;

    if (event->m_Pressed) // a key state changed from up to down
    {
        if (g_NumKeys < NUM_CHARS)
            g_NumKeys++;

        // Move previous entries down through the array and add new one at end
        memmove(g_Keys+1, g_Keys, (NUM_CHARS - 1) * sizeof(s3eKey));
        g_Keys[0] = event->m_Key;
    }
    else // state changed from down to up
    {
        if (g_NumKeysReleased < NUM_CHARS)
            g_NumKeysReleased++;

        memmove(g_KeysReleased+1, g_KeysReleased, (NUM_CHARS - 1) * sizeof(s3eKey));
        g_KeysReleased[0] = event->m_Key;
    }

    return 0;
}

void ExampleInit()
{
    s3eKeyboardRegister(S3E_KEYBOARD_KEY_EVENT, handler, NULL);

    g_ButtonChar = NewButton("enable char input");
    s3eKeyboardSetInt(S3E_KEYBOARD_GET_CHAR, 0);
}

void ExampleTerm()
{
}

bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    // Check for character input being terminated by the platform/user
    int newCharState = s3eKeyboardGetInt(S3E_KEYBOARD_GET_CHAR);

    // Enable/disable reading in wide characters through GetChar interface.
    // On some devices this may activate/disable a text entry GUI.
    if (pressed && pressed == g_ButtonChar)
    {
        newCharState = !newCharState;
        s3eKeyboardSetInt(S3E_KEYBOARD_GET_CHAR, newCharState);
    }

    if (newCharState != g_CharEnabled)
    {
        g_CharEnabled = newCharState;
        DeleteButtons();

        if (g_CharEnabled)
            g_ButtonChar = NewButton("disable char input");
        else
            g_ButtonChar = NewButton("enable char input");
    }

    s3eWChar ch;

    if (g_CharEnabled)
    {
        // Get all chars that were internally buffered since the last update
        while ((ch = s3eKeyboardGetChar()) != S3E_WEOF)
        {
            if (g_NumChars < NUM_CHARS)
                g_NumChars++;
            memmove(g_Chars+1, g_Chars, (NUM_CHARS - 1) * sizeof(s3eWChar));
            g_Chars[0] = ch;
        }
    }

    // Get the last key that was pressed. Will be return s3eKeyFirst if no key
    // was pressed between the previous calls to s3eKeyboardUpdate().
    g_LastKeyPressed = s3eKeyboardAnyKey();

    return true;
}

void ExampleRender()
{
    int fontHeight = s3eDebugGetInt(S3E_DEBUG_FONT_HEIGHT);
    int fontWidth = s3eDebugGetInt(S3E_DEBUG_FONT_WIDTH);
    int lineHeight = fontHeight + 2;
    int y = GetYBelowButtons();
    int x = 10;

    // Find and display keyboard related device capabilities/settings
    s3eDebugPrintf(x, y, 1, "`x666666Has querty: %d", s3eKeyboardGetInt(S3E_KEYBOARD_HAS_ALPHA));
    y += lineHeight;
    s3eDebugPrintf(x, y, 1, "`x666666Has numpad: %d", s3eKeyboardGetInt(S3E_KEYBOARD_HAS_NUMPAD));
    y += lineHeight;
    s3eDebugPrintf(x, y, 1, "`x666666Has direction: %d", s3eKeyboardGetInt(S3E_KEYBOARD_HAS_DIRECTION));
    y += lineHeight;
    s3eDebugPrintf(x, y, 1, "`x666666Numpad orient: %d", s3eKeyboardGetInt(S3E_KEYBOARD_NUMPAD_ORIENTATION));
    y += lineHeight * 2;

    if (g_CharEnabled)
        s3eDebugPrintf(x, y, 1, "`x666666Character input enabled");
    else
        s3eDebugPrintf(x, y, 1, "`x666666Character input disabled");

    y += lineHeight * 2;
    s3eDebugPrint(x, y, "`x666666Please press keys:", 1);
    y += lineHeight * 2;

    int listStartY = y;

    // Display characters read in through s3eKeyboardGetChar()
    s3eDebugPrint(x, y, "`x000066Characters:", 0);
    y += lineHeight;
    for (int i = g_NumChars - 1; i >= 0; i--)
    {
        s3eWChar ch = g_Chars[i];
        char message[128];

        if (y > s3eSurfaceGetInt(S3E_SURFACE_HEIGHT))
            continue;
        strcpy(message, "`x666666");

        if ((int)ch > 128)
        {
            sprintf(message+strlen(message), "Char: u%u", (uint)ch);
        }
        else
        {
            sprintf(message+strlen(message), "Char: %c (u%u)", (char)ch, (uint)ch);
        }

        s3eDebugPrint(x, y, message, 1);
        y += lineHeight;
    }

    y = listStartY;
    x += 18 * fontWidth;
    char name[128];

    // Display last few keys that were pressed down
    s3eDebugPrint(x, y, "`x000066Keys pressed:", 0);
    y += lineHeight;
    for (int j = g_NumKeys-1; j >= 0; j--)
    {
        s3eKey key = g_Keys[j];
        s3eKeyboardGetDisplayName(name, key);

        if (key >= s3eKeyAbsGameA && key <= s3eKeyAbsBSK)
            s3eDebugPrintf(x, y, 1, "`x666666 Key: %s (%d - abstract %d)", name, key, key - s3eKeyAbsGameA);
        else
            s3eDebugPrintf(x, y, 1, "`x666666 Key: %s (%d)", name, key);

        y += lineHeight;
    }

    y += lineHeight;

    // Display last few keys that were released
    s3eDebugPrint(x, y, "`x000066Keys released:", 0);
    y += lineHeight;
    for (int k = g_NumKeysReleased-1; k >= 0; k--)
    {
        s3eKey key = g_KeysReleased[k];
        s3eKeyboardGetDisplayName(name, key);

        if (key >= s3eKeyAbsGameA && key <= s3eKeyAbsBSK)
            s3eDebugPrintf(x, y, 1, "`x666666 Key: %s (%d - abstract %d)", name, key, key - s3eKeyAbsGameA);
        else
            s3eDebugPrintf(x, y, 1, "`x666666 Key: %s (%d)", name, key);

        y += lineHeight;
    }

    // Display the last key pressed at bottom of screen.
    // Note that it disappears after 1 frame (because s3eKeyboardUpdate() is
    // called called in the main loop (in ExamplesMain.cpp).

    x = 10;
    y = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT) - (fontHeight*2);
    s3eDebugPrintf(x, y, 1, "`x666666Last key pressed:");
    x += 16 * fontWidth;
    if (g_LastKeyPressed != s3eKeyFirst)
    {
        s3eKeyboardGetDisplayName(name, g_LastKeyPressed);
        s3eDebugPrintf(x, y, 1, "`x666666  %s (%d)", name, g_LastKeyPressed);
    }
}
