/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/*
 * This file provided to demonstrate how to use the s3eChartBoost Extension
 * which is a wrapper for www.chartboost.com sdk
 *
 */

/**
 * @page ExampleS3EChartBoost S3E Chartboost example
 *
 * The following is an example of how to use the s3eChartBoost Extension,
 * requesting advertisements, registering for callbacks and processing responses
 *
 * The main functions demonstrated in this example are:
 * - s3eChartBoostRegister()
 * - s3eChartBoostStartSession()
 * - s3eChartBoostSetAppID()
 * - s3eChartBoostSetAppSignature()
 * - s3eChartBoostUnRegister()
 * - s3eChartBoostCacheInterstitial()
 * - s3eChartBoostShowInterstitial()
 *
 * @include s3eChartboostExample.cpp
 */

#include "ExamplesMain.h"
#include "s3e.h"
#include "s3eDebug.h"

#include "s3eChartBoost.h"

// Buttons
static Button* g_CacheInterButton = NULL;
static Button* g_ShowInterButton = NULL;
static Button* g_CacheRewardButton = NULL;
static Button* g_ShowRewardButton = NULL;
static Button* g_CacheMoreAppsButton = NULL;
static Button* g_ShowMoreAppsButton = NULL;

// Status string to print at bottom of screen
static char g_StatusStr[8192]; // Can be quite a long string!

// Sets a string to be displayed beneath the buttons
static void SetStatus(const char* statusStr, ...)
{
    va_list args;
    va_start(args, statusStr);
    strcpy(g_StatusStr, "`x000000");
    vsprintf(g_StatusStr+strlen(g_StatusStr), statusStr, args);
    va_end(args);
}

// Appends to the string to be displayed beneath the buttons
static void AddStatus(const char* statusStr, ...)
{
    va_list args;
    va_start(args, statusStr);
    size_t currentLen = strlen(g_StatusStr);
    g_StatusStr[currentLen] = '\n';
    vsprintf(g_StatusStr + currentLen + 1, statusStr, args);
    va_end(args);
}

void RequestCB(void* systemData, void* userData)
{
    AddStatus("RequestCB with value %d", *((int*)systemData));
}


void AdvertisementClosed(void* System, void* User)
{
    AddStatus("AdvertisementClosed");
}

void AdvertisementDismissed(void* System, void* User)
{
    AddStatus("AdvertisementDismissed");
}

void AdvertisementClicked(void* System, void* User)
{
    AddStatus("AdvertisementClicked");
}

void RewardComplete(void* System, void* User)
{
    AddStatus("RewardComplete: %d coins", *static_cast<int*>(System));
}

void ExampleInit()
{
    // Set the Applicatiosn ID and Signature
    // APP_ID and APP_SECRET is taken automatically from ICF Settings under
    // [s3eChartBoost]
    // AndroidAppID = ""
    // AndroidAppSecret = ""
    // IOSAppID  = ""
    // IOSAppSecret = ""
    //
    // Alternatively, You can call s3eChartBoostSetAppID() or s3eChartBoostSetAppSignature
    // Remember to setup your Chartboost Account and your package id in the deployment tool or mkb
    // s3eChartBoostSetAppID("chartboost_app_id");
    // s3eChartBoostSetAppSignature("chartboost_app_signature");
    SetStatus("s3eChartboost init");
    g_CacheInterButton = NewButton("Cache interstitial ad");
    g_ShowInterButton = NewButton("Show interstitial ad");
    g_CacheRewardButton = NewButton("Cache reward video");
    g_ShowRewardButton = NewButton("Show reward video");
    g_CacheMoreAppsButton = NewButton("Cache MoreApps ad");
    g_ShowMoreAppsButton = NewButton("Show MoreApps ad");

    // Register for callbacks
    s3eChartBoostRegister( S3E_CHARTBOOST_CALLBACK_REQUEST_RESPONSE, (s3eCallback)RequestCB, NULL);
    s3eChartBoostRegister( S3E_CHARTBOOST_CALLBACK_AD_CLOSED, (s3eCallback)AdvertisementClosed, NULL);
    s3eChartBoostRegister( S3E_CHARTBOOST_CALLBACK_AD_DISMISSED, (s3eCallback)AdvertisementDismissed, NULL);
    s3eChartBoostRegister( S3E_CHARTBOOST_CALLBACK_AD_CLICKED,(s3eCallback) AdvertisementClicked, NULL);
    s3eChartBoostRegister( S3E_CHARTBOOST_CALLBACK_REWARD_COMPLETE,(s3eCallback) RewardComplete, NULL);

    // Start the Chartboost Session
    s3eChartBoostStartSession();
}

void ExampleTerm()
{
    // UnRegister Callbacks
    s3eChartBoostUnRegister( S3E_CHARTBOOST_CALLBACK_REQUEST_RESPONSE, (s3eCallback)RequestCB);
    s3eChartBoostUnRegister( S3E_CHARTBOOST_CALLBACK_AD_CLOSED, (s3eCallback)AdvertisementClosed);
    s3eChartBoostUnRegister( S3E_CHARTBOOST_CALLBACK_AD_DISMISSED, (s3eCallback)AdvertisementDismissed);
    s3eChartBoostUnRegister( S3E_CHARTBOOST_CALLBACK_AD_CLICKED,(s3eCallback) AdvertisementClicked);
    s3eChartBoostUnRegister( S3E_CHARTBOOST_CALLBACK_REWARD_COMPLETE,(s3eCallback) RewardComplete);
}

bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    if (pressed)
    {
        if (g_CacheInterButton == pressed)
        {
            s3eChartBoostCacheInterstitial(S3E_CHARTBOOST_LOCATION(HOME_SCREEN));
        }
        else if (g_ShowInterButton == pressed)
        {
            s3eChartBoostShowInterstitial(S3E_CHARTBOOST_LOCATION(HOME_SCREEN));
        }
        else if (g_CacheRewardButton == pressed)
        {
            s3eChartBoostCacheRewardedVideo(S3E_CHARTBOOST_LOCATION(HOME_SCREEN));
        }
        else if (g_ShowRewardButton == pressed)
        {
            s3eChartBoostShowRewardedVideo(S3E_CHARTBOOST_LOCATION(HOME_SCREEN));
        }
        else if (g_CacheMoreAppsButton == pressed)
        {
            s3eChartBoostCacheMoreApps(S3E_CHARTBOOST_LOCATION(HOME_SCREEN));
        }
        else if (g_ShowMoreAppsButton == pressed)
        {
            s3eChartBoostShowMoreApps(S3E_CHARTBOOST_LOCATION(HOME_SCREEN));
        }
    }

    return true;
}

void ExampleRender()
{
    // Print status string just below the buttons
    s3eDebugPrint(0, GetYBelowButtons(), g_StatusStr, S3E_TRUE);
}

