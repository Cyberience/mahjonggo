/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3ETizenIAP S3E Tizen In Application Purchase Example
 *
 * The following example call the s3eTizenIAP API to allow the user
 * to sell his own items in application.
 *
 * The functions required to achieve this are:
 * - s3eTizenIAPAvailable()
 * - s3eTizenIAPGetCountryList()
 * - s3eTizenIAPCountryListGetResult()
 * - s3eTizenIAPGetItemList()
 * - s3eTizenIAPPurchaseConfirm()
 *
 * Following example demonstration of InApp Purchase.
 * It outputs list of countries, list of available items.
 * It also demonstrates purchase process of items.
 * See device traces.
 *
 * @include s3eTizenIAP.cpp
 */

#include "string.h"
#include "ExamplesMain.h"
#include "s3eTizenIAP.h"
#include "IwDebug.h"

// Sample test data
const char* g_GroupID = "100000000017";
const char* g_ItemID = "000000000086";
const char* g_MCC= "250";
const char* g_MNC= "01";
const char* g_DeviceModel = "GT-I8800";

const int g_NumMessages = 50;
const int g_MessageLen = 160;

static int s_TransactionID = 0;

static Button* s_ButtonBack = 0;
static Button* s_ButtonCountryList = 0;
static Button* s_ButtonItemList = 0;
static Button* s_ButtonPurchase = 0;

static int32 CountryListCallback(void* sys, void*)
{
    IwAssert(DEBUG, sys);
    IwTrace(DEBUG, ("CountryListCallback."));

    s3eTizenIAPCallbackCountryList* list = (s3eTizenIAPCallbackCountryList*)sys;

    if (s3eTizenIAPCountryListGetResult(list) != S3E_TIZENIAP_CODE_SUCCEED)
    {
        IwTrace(DEBUG, ("Invalid result returned."));
        return -1;
    }

    int rangeStart = s3eTizenIAPCountryListGetRangeFrom(list);
    int rangeEnd = s3eTizenIAPCountryListGetRangeTo(list);

    if (rangeStart == -1 || rangeEnd == -1)
    {
        IwTrace(DEBUG, ("Provided range is invalid."));
        return -1;
    }

    for (int i = rangeStart; i <= rangeEnd; ++i)
    {
        AppendMessage("`x666666Country name is [%s], mcc is [%s]", s3eTizenIAPCountryListGetCountryName(list, i), s3eTizenIAPCountryListGetCountryMcc(list, i));
    }

    return 0;
}

static int32 ItemListCallback(void* sys, void* user)
{
    IwAssert(DEBUG, sys);
    IwTrace(DEBUG, ("ItemListCallback."));

    s3eTizenIAPCallbackItemList* itemList = (s3eTizenIAPCallbackItemList*)sys;

    if (s3eTizenIAPItemListGetResult(itemList) != S3E_TIZENIAP_CODE_SUCCEED)
    {
        IwTrace(DEBUG, ("Invalid result returned."));
        return -1;
    }

    int rangeStart = s3eTizenIAPItemListGetRangeFrom(itemList);
    int rangeEnd = s3eTizenIAPItemListGetRangeTo(itemList);

    if (rangeStart == -1 || rangeEnd == -1)
    {
        IwTrace(DEBUG, ("Provided range is invalid."));
        return -1;
    }

    for (int i = rangeStart; i <= rangeEnd; ++i)
    {
        s3eTizenIAPItem* item = s3eTizenIAPItemListGetItem(itemList, i);
        if (!item)
        {
            IwTrace(DEBUG, ("Failed to get TizenIAP item."));
            continue;
        }
        AppendMessage("`x666666Item name is [%s], price is [%s]", s3eTizenIAPItemGetName(item), s3eTizenIAPItemGetPrice(item));
    }

    return 0;
}

static int32 PurchaseItemInitCallback(void* sys, void* user)
{
    IwAssert(DEBUG, sys);
    IwTrace(DEBUG, ("PurchaseItemInitCallback."));

    s3eTizenIAPCallbackPurchase* purchase = (s3eTizenIAPCallbackPurchase*)sys;

    if (s3eTizenIAPPurchaseGetResult(purchase) != S3E_TIZENIAP_CODE_SUCCEED)
    {
        IwTrace(DEBUG, ("Invalid result returned."));
        return -1;
    }

    if (s3eTizenIAPPurchaseConfirm(s3eTizenIAPPurchaseGetTransactionID(purchase)) != S3E_RESULT_SUCCESS)
    {
        IwTrace(DEBUG, ("Failed to confirm purchase."));
        return -1;
    }

    return 0;
}

static int32 PurchaseItemFinalizeCallback(void* sys, void* user)
{
    IwAssert(DEBUG, sys);
    IwTrace(DEBUG, ("PurchaseItemFinalizeCallback."));

    s3eTizenIAPCallbackPurchase* purchase = (s3eTizenIAPCallbackPurchase*)sys;

    if (s3eTizenIAPPurchaseGetResult(purchase) != S3E_TIZENIAP_CODE_SUCCEED)
    {
        IwTrace(DEBUG, ("Invalid result returned."));
        return -1;
    }

    return 0;
}

void ExampleInit()
{
    s_ButtonBack = NewButton("Back");
    s_ButtonCountryList = NewButton("Show country list");
    s_ButtonItemList = NewButton("Show item list");
    s_ButtonPurchase = NewButton("Purchase");

    s_ButtonBack->m_Display = false;
    InitMessages(g_NumMessages, g_MessageLen);

    s3eTizenIAPRegister(S3E_TIZENIAP_CALLBACK_COUNTRY_LIST, CountryListCallback, NULL);
    s3eTizenIAPRegister(S3E_TIZENIAP_CALLBACK_ITEM_LIST, ItemListCallback, NULL);
    s3eTizenIAPRegister(S3E_TIZENIAP_CALLBACK_PURCHASE_INITIALAIZE, PurchaseItemInitCallback, NULL);
    s3eTizenIAPRegister(S3E_TIZENIAP_CALLBACK_PURCHASE_FINALIZE, PurchaseItemFinalizeCallback, NULL);
    s3eTizenIAPSetDevMode(true, g_MCC, g_MNC, g_DeviceModel);
}

void ExampleTerm()
{
    s3eTizenIAPUnRegister(S3E_TIZENIAP_CALLBACK_COUNTRY_LIST, CountryListCallback);
    s3eTizenIAPUnRegister(S3E_TIZENIAP_CALLBACK_ITEM_LIST, ItemListCallback);
    s3eTizenIAPUnRegister(S3E_TIZENIAP_CALLBACK_PURCHASE_INITIALAIZE, PurchaseItemInitCallback);
    s3eTizenIAPUnRegister(S3E_TIZENIAP_CALLBACK_PURCHASE_FINALIZE, PurchaseItemFinalizeCallback);
    TerminateMessages();
}

bool ExampleUpdate()
{
    if (!s3eTizenIAPAvailable())
        return true;

    Button* pressed = GetSelectedButton();

    if (pressed == s_ButtonCountryList)
    {
        if (s3eTizenIAPGetCountryList(++s_TransactionID) != S3E_RESULT_SUCCESS)
        {
            AppendMessage("`x666666Failed to get country list.");
        }
    }
    else if (pressed == s_ButtonItemList)
    {
        if (s3eTizenIAPGetItemList(++s_TransactionID, 1, 15, g_GroupID) != S3E_RESULT_SUCCESS)
        {
            AppendMessage("`x666666Failed to get item list.");
        }
    }
    else if (pressed == s_ButtonPurchase)
    {
        if (s3eTizenIAPPurchaseStart(++s_TransactionID, g_ItemID, g_GroupID) != S3E_RESULT_SUCCESS)
        {
            AppendMessage("`x666666Failed to start purchase process.");
        }
    }
    else if (pressed == s_ButtonBack)
    {
        ClearMessages();
    }

    if (pressed)
    {
        s_ButtonCountryList->m_Display = (pressed == s_ButtonBack);
        s_ButtonItemList->m_Display = (pressed == s_ButtonBack);
        s_ButtonPurchase->m_Display = (pressed == s_ButtonBack);
        s_ButtonBack->m_Display = (pressed != s_ButtonBack);
    }

    return true;
}

void ExampleRender()
{
    if (!s3eTizenIAPAvailable())
    {
        s3eDebugPrint(50, 50, "`x666666TizenIAP API isn't available on this device.", 1);
        return;
    }

    PrintMessages(50, 100);
}
