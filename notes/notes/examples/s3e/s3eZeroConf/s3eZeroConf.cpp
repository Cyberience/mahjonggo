/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EZeroConf S3E ZeroConf Example
 *
 * The following example demonstrate the use of the s3eZeroConf API
 * to connect to the network and list the available services.
 *
 * ZeroConf is an s3e extension and is currently only supported on iPhone.
 *
 * The main functions used in this example are:
 * - s3eZeroConfStartSearch()
 * - s3eZeroConfStopSearch()
 * - s3eZeroConfPublish()
 * - s3eZeroConfUnpublish()
 * - s3eZeroConfUpdateTxtRecord()
 *
 * The application uses s3eZeroConf to makes a connection to the network
 * then enumerated the devices and their services that was found on the network.
 *
 * The results list is output to screen using the s3eDebugPrint()
 * function.
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eZeroConfImage.png
 *
 * @include s3eZeroConf.cpp
 */

#include "ExamplesMain.h"
#include "s3e.h"
#include "s3eZeroConf.h"
#include <string.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

/*
 * Used to record infomation about the services we've discovered
 */
struct Record
{
    char*           m_Name;
    char*           m_Txt;
    char*           m_IP;
    void*           m_Ref;
    s3eInetAddress  m_Addr;
    Record*         m_Next;
    Record*         m_Prev;

    void Free()
    {
        if (m_Name)
            s3eFree(m_Name);
        m_Name = NULL;

        if (m_Txt)
            s3eFree(m_Txt);
        m_Txt = NULL;

        if (m_IP)
            s3eFree(m_IP);
        m_IP = NULL;
    }
};

static Record*               g_ResultList = 0;
static s3eZeroConfPublished* g_Service = 0;
static s3eZeroConfSearch*    g_Search = 0;
static uint32                g_Counter = 0;
static uint32                g_Timer = 75;
static const unsigned char*  g_TxtRecord = 0;

// this call back is issued when a new service (matching the query) is discovered
static int32 SearchAddCallBack(s3eZeroConfSearch* search, s3eZeroConfAddRecord* rec, void* userData)
{
    char buf[512];
    char buf2[64];

    s3eDebugTracePrintf("SearchAddCallBack: %s %s %d", rec->m_Name, rec->m_Hostname, rec->m_Port);

    // push a new record onto the list
    Record  *result = (Record*) s3eMalloc(sizeof(Record));

    result->m_Prev = 0;
    result->m_Ref = rec->m_RecordRef;
    result->m_Next = g_ResultList;

    if (g_ResultList)
        g_ResultList->m_Prev = result;

    g_ResultList = result;

    // fill it out
    uint32 len = snprintf(buf, sizeof(buf), "`x666666txt '%s'", rec->m_NumTxtRecords ? rec->m_TxtRecords[0] : "") + 1;
    result->m_Txt = (char*) s3eMalloc(len);
    memcpy(result->m_Txt, buf, len);

    len = snprintf(buf, sizeof(buf), "`x666666ip %s", s3eInetNtoa(rec->m_HostIP, buf2, sizeof(buf2))) + 1;
    result->m_IP = (char*) s3eMalloc(len);
    memcpy(result->m_IP, buf, len);

    len = snprintf(buf, sizeof(buf), "`x666666%s\n@ %s:%d", rec->m_Name, rec->m_Hostname, rec->m_Port) + 1;
    result->m_Name = (char*) s3eMalloc(len);
    memcpy(result->m_Name, buf, len);

    return 0;
}

// this call back is issued when a previously discovered service updates it's txtRecord
static int32 SearchUpdateCallBack(s3eZeroConfSearch* search, s3eZeroConfUpdateRecord* rec, void* userData)
{
    Record  *record;
    char    buf[512];

    for (record = g_ResultList; record; record = record->m_Next)
        if (record->m_Ref == rec->m_RecordRef)
            break;

    if (!record)
        return 0;

    // fill it out
    uint32 len = snprintf(buf, sizeof(buf), "`x666666txt '%s'", rec->m_NumTxtRecords ? rec->m_TxtRecords[0] : "") + 1;

    s3eFree(record->m_Txt);

    record->m_Txt = (char*) s3eMalloc(len);
    memcpy(record->m_Txt, buf, len);
    return 0;
}

// this call back is issued when a previously discovered service is unregisted
static int32 SearchRemoveCallBack(s3eZeroConfSearch* search, void* userRef, void* userData)
{
    Record* record;

    for (record = g_ResultList; record; record = record->m_Next)
        if (record->m_Ref == userRef)
            break;

    if (!record)
        return 0;

    // remove result from the linked list
    if (record->m_Next)
        record->m_Next->m_Prev = record->m_Prev;

    if (record->m_Prev)
        record->m_Prev->m_Next = record->m_Next;
    else
        g_ResultList = record->m_Next;

    // free record
    record->Free();
    s3eFree(record);

    return 0;
}

void ExampleInit()
{
    const char* string = "A test txt record!";
    const char* argv[] = { string };

    // Not all implementations handle name collisions so it's a
    // good idea to try and ensure some level of uniqueness to your
    // individual service name
    srand(time(NULL));
    char serviceName[255];
    snprintf(serviceName, 255, "s3eDummyService (%d)", rand());

    // create a dummy service
    g_Service = s3eZeroConfPublish(
      8080,
      serviceName,
      "_http._tcp", NULL, 1, argv
    );

    // search for "_http._tcp" services in domain "local"
    g_Search = s3eZeroConfStartSearch(
      "_http._tcp",
      "local",
      SearchAddCallBack,
      SearchUpdateCallBack,
      SearchRemoveCallBack,
      0
 );
}

void ExampleTerm()
{
    s3eDebugTracePrintf("Stopping search: %p", g_Search);
    s3eZeroConfStopSearch(g_Search);
    s3eZeroConfUnpublish(g_Service);

    Record* next;

    // free the results
    for (Record* record = g_ResultList; record; record = next)
    {
        next = record->m_Next;

        // free record
        record->Free();
        s3eFree(record);
    }

    g_ResultList = 0;
}

bool ExampleUpdate()
{
    if (g_Timer == 0)
    {
        g_Timer = 75;

        // create a new txtRecord
        char txtRec[100];

        snprintf(txtRec, sizeof(txtRec), "txt record #%u!", g_Counter++);

        const char* argv[] = { txtRec };

        if (g_TxtRecord)
            s3eFree((void*)g_TxtRecord);
        g_TxtRecord = 0;

        // update the txt record
        s3eZeroConfUpdateTxtRecord(g_Service, 1, argv);
    }
    else
    {
        g_Timer--;
    }

    return true;
}

void ExampleRender()
{
    const int   textHeight = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);
    int         y = 50;

    for (Record* record = g_ResultList; record; record = record->m_Next)
    {
        if (record->m_Name)
        {
            s3eDebugPrint(10, y, record->m_Name, 0);
            y += textHeight * 2;
        }

        if (record->m_IP)
        {
            s3eDebugPrint(10, y, record->m_IP, 0);
            y += textHeight;
        }

        if (record->m_Txt)
        {
            s3eDebugPrint(10, y, record->m_Txt, 0);
            y += textHeight;
        }

        y += textHeight;
    }
}
