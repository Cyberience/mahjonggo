/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EGLES1 S3E OpenGL ES 1.x Example
 *
 * This example demonstrates the use of S3EGL with an OpenGL ES 1.x codebase.
 *
 * This example is based on s3eGLES1, which is in turn based on the SDL gltest
 * code and was updated to support GLES by the guys at webos-internals:
 * http://www.webos-internals.org/wiki/GLES_1.1_3D_Cube_Demo
 *
 * This code was updated to use S3E and EGL rather than SDL to initialise
 * the GL context.
 *
 * @include s3eGLES1.cpp
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <GLES/gl.h>
#include <GLES/egl.h>

#include "s3e.h"

static EGLSurface g_EGLSurface = NULL;
static EGLDisplay g_EGLDisplay = NULL;
static EGLDisplay g_EGLContext = NULL;

bool g_glIsInitialized = false;

void RunGLTest(int logo, int logocursor, int slowly, int bpp, float gamma, int noframe, int fsaa, int sync, int accel)
{
    int w = s3eSurfaceGetInt(S3E_SURFACE_WIDTH);
    int h = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT);
    int done = 0;
    int frames;
    int start_time, this_time;
    GLfloat color[96] = {
        1.0,  1.0,  0.0, 1.0,  // 0
        1.0,  0.0,  0.0,  1.0, // 1
        0.0,  1.0,  0.0, 1.0,  // 3
        0.0,  0.0,  0.0,  1.0, // 2

        0.0,  1.0,  0.0, 1.0,  // 3
        0.0,  1.0,  1.0,  1.0, // 4
        0.0,  0.0,  0.0, 1.0,  // 2
        0.0,  0.0,  1.0, 1.0,  // 7

        1.0,  1.0,  0.0, 1.0,  // 0
        1.0,  1.0,  1.0, 1.0,  // 5
        1.0,  0.0,  0.0, 1.0,  // 1
        1.0,  0.0,  1.0, 1.0,  // 6

        1.0,  1.0,  1.0, 1.0,  // 5
        0.0,  1.0,  1.0, 1.0,  // 4
        1.0,  0.0,  1.0, 1.0,  // 6
        0.0,  0.0,  1.0, 1.0,  // 7

        1.0,  1.0,  1.0, 1.0,  // 5
        1.0,  1.0,  0.0, 1.0,  // 0
        0.0,  1.0,  1.0, 1.0,  // 4
        0.0,  1.0,  0.0, 1.0,  // 3

        1.0,  0.0,  1.0, 1.0,  // 6
        1.0,  0.0,  0.0, 1.0,  // 1
        0.0,  0.0,  1.0, 1.0,  // 7
        0.0,  0.0,  0.0, 1.0,  // 2
    };

    GLfloat cube[72] = {

        0.5,  0.5, -0.5,   // 0
        0.5, -0.5, -0.5,   // 1
        -0.5,  0.5, -0.5,   // 3
        -0.5, -0.5, -0.5,   // 2

        -0.5,  0.5, -0.5,   // 3
        -0.5,  0.5,  0.5,   // 4
        -0.5, -0.5, -0.5,   // 2
        -0.5, -0.5,  0.5,   // 7

        0.5,  0.5, -0.5,   // 0
        0.5,  0.5,  0.5,   // 5
        0.5, -0.5, -0.5,   // 1
        0.5, -0.5,  0.5,   // 6

        0.5,  0.5,  0.5,   // 5
        -0.5,  0.5,  0.5,   // 4
        0.5, -0.5,  0.5,   // 6
        -0.5, -0.5,  0.5,   // 7

        0.5,  0.5,  0.5,   // 5
        0.5,  0.5, -0.5,   // 0
        -0.5,  0.5,  0.5,   // 4
        -0.5,  0.5, -0.5,   // 3

        0.5, -0.5,  0.5,   // 6
        0.5, -0.5, -0.5,   // 1
        -0.5, -0.5,  0.5,   // 7
        -0.5, -0.5, -0.5,   // 2
    };

    printf("Screen BPP  : %d\n", s3eSurfaceGetInt(S3E_SURFACE_PIXEL_TYPE) & S3E_SURFACE_PIXEL_SIZE_MASK);
    printf("Screen Size : %dx%d\n", w, h);
    printf("\n");
    printf("Vendor     : %s\n", (const char*)glGetString( GL_VENDOR ) );
    printf("Renderer   : %s\n", (const char*)glGetString( GL_RENDERER ) );
    printf("Version    : %s\n", (const char*)glGetString( GL_VERSION ) );
    printf("Extensions : %s\n", (const char*)glGetString( GL_EXTENSIONS ) );
    printf("\n");

    /* Loop until done. */
    start_time = (int)s3eTimerGetMs();
    frames = 0;
    while (!done)
    {
        GLenum gl_error;
        // Only try and render if gl is initialized.  It may get
        // terminated due to a S3E_GL_SUSPEND callback.
        if (g_glIsInitialized)
        {
            /* Do our drawing, too. */
            glClearColor(0.0, 0.0, 0.0, 1.0);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            glEnableClientState(GL_VERTEX_ARRAY);
            glEnableClientState(GL_COLOR_ARRAY);
            glColorPointer(4, GL_FLOAT, 0, color);
            glVertexPointer(3, GL_FLOAT, 0, cube);
            glDrawArrays(GL_TRIANGLE_STRIP, 0, 24);
            glDisableClientState(GL_VERTEX_ARRAY);
            glDisableClientState(GL_COLOR_ARRAY);
            glMatrixMode(GL_MODELVIEW);
            glRotatef(5.0, 1.0, 1.0, 1.0);

            eglSwapBuffers(g_EGLDisplay, g_EGLSurface);
        }

        /* Check for error conditions. */
        gl_error = glGetError();

        if (gl_error != GL_NO_ERROR)
        {
            fprintf(stderr, "testgl: OpenGL error: %#x\n", gl_error);
        }

        /* Allow the user to see what's happening */
        if (slowly)
        {
            s3eDeviceYield(20);
        }
        else
        {
            s3eDeviceYield(0);
        }

        s3eKeyboardUpdate();

        if (s3eDeviceCheckQuitRequest())
            done = 1;

        if (s3eKeyboardGetState(s3eKeyEsc) & S3E_KEY_STATE_PRESSED)
            done = 1;

        ++frames;
    }

    /* Print out the frames per second */
    this_time = (int)s3eTimerGetMs();
    if (this_time != start_time)
    {
        printf("%2.2f FPS\n", ((float)frames/(this_time-start_time))*1000.0);
    }
}
#define MAX_CONFIGS 30

static int eglInit()
{
    EGLint major;
    EGLint minor;
    EGLint numFound = 0;
    EGLConfig configList[MAX_CONFIGS];

    int version = s3eGLGetInt(S3E_GL_VERSION);
    if ((version >> 8) != 1)
    {
        s3eDebugTracePrintf("reported GL version is %d", version);
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "This example required GLES v1.x");
        return 1;
    }

    g_EGLDisplay = eglGetDisplay(EGL_DEFAULT_DISPLAY);
    if (!g_EGLDisplay)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "eglGetDisplay failed");
        return 1;
    }

    EGLBoolean res = eglInitialize(g_EGLDisplay, &major, &minor);
    if (!res)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "eglInitialize failed");
        return 1;
    }

    if (!eglGetConfigs(g_EGLDisplay, configList, MAX_CONFIGS, &numFound))
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "eglGetConfigs failed");
        return 1;
    }

    int requiredRedDepth = 8;
    bool requiredFound = s3eConfigGetInt("GL", "EGL_RED_SIZE", &requiredRedDepth) == S3E_RESULT_SUCCESS;

    int config = -1;

    printf("found %d configs\n", numFound);
    for (int i = 0; i < numFound; i++)
    {
        EGLint surfacetype = 0;
        EGLint actualRedDepth = 0;
        EGLint depth = 0;

        eglGetConfigAttrib(g_EGLDisplay, configList[i], EGL_SURFACE_TYPE, &surfacetype);
        eglGetConfigAttrib(g_EGLDisplay, configList[i], EGL_RED_SIZE, &actualRedDepth);
        eglGetConfigAttrib(g_EGLDisplay, configList[i], EGL_DEPTH_SIZE, &depth);

        printf("config %d: depth=%d\n", i, depth);
        if (depth && (surfacetype & EGL_WINDOW_BIT) && ((!requiredFound || (actualRedDepth == requiredRedDepth))))
        {
            config = i;
            break;
        }
    }

    if (config == -1)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "No suitable config found.  Trying random config");
        config = 0;
    }

    printf("using config %d\n", config);

    g_EGLContext = eglCreateContext(g_EGLDisplay, configList[config], NULL, NULL);
    if (!g_EGLContext)
    {
        s3eDebugErrorPrintf("eglCreateContext failed: %#x", eglGetError());
        return 1;
    }

    void* nativeWindow = s3eGLGetNativeWindow();
    g_EGLSurface = eglCreateWindowSurface(g_EGLDisplay, configList[config], nativeWindow, NULL);
    if (!g_EGLSurface)
    {
        s3eDebugErrorPrintf("eglCreateWindowSurface failed: %#x", eglGetError());
        return 1;
    }

    res = eglMakeCurrent(g_EGLDisplay, g_EGLSurface, g_EGLSurface, g_EGLContext);
    if (!res)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "eglMakeCurrent failed");
        return 1;
    }

    int w = s3eSurfaceGetInt(S3E_SURFACE_WIDTH);
    int h = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT);

    //Prepare the GL State
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrthof(-2.0, 2.0, -2.0, 2.0, -20.0, 20.0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glEnable(GL_DEPTH_TEST);

    glDepthFunc(GL_LESS);

    glShadeModel(GL_SMOOTH);

    g_glIsInitialized = true;
    return 0;
}

static void eglTerminate()
{
    g_glIsInitialized = false;

    eglMakeCurrent(g_EGLDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);

    if (g_EGLContext && g_EGLDisplay)
    {
        eglDestroyContext(g_EGLDisplay, g_EGLContext);
        g_EGLContext = NULL;
    }
    if (g_EGLSurface && g_EGLDisplay)
    {
        eglDestroySurface(g_EGLDisplay, g_EGLSurface);
        g_EGLSurface = NULL;
    }

    eglTerminate(g_EGLDisplay);

}

int32 pauseHandler(void *systemData, void *userData)
{
    eglTerminate();

    return 0;
}

int32 unpauseHandler(void *systemData, void *userData)
{
    if (eglInit())
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "eglInit failed");
    }
    return 0;
}

int main(int argc, char* argv[])
{
    int logo = 0;
    int logocursor = 0;
    int bpp = 0;
    int slowly = 1;
    float gamma = 0.0;
    int noframe = 0;
    int fsaa = 0;
    int accel = 0;
    int sync = 0;

    s3eGLRegister (S3E_GL_SUSPEND, pauseHandler, NULL);
    s3eGLRegister (S3E_GL_RESUME, unpauseHandler, NULL);

    if (eglInit())
        return 1;
    RunGLTest(logo, logocursor, slowly, bpp, gamma, noframe, fsaa, sync, accel);

    eglTerminate();

    return 0;
}
