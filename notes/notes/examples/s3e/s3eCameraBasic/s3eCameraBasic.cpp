/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3ECamera S3E Camera Example
 *
 * The following example demonstrates S3E's camera streaming functionality.
 *
 * The main functions used to achieve this are:
 * - s3eCameraAvailable()
 * - s3eCameraRegister()
 * - s3eCameraStart()
 * - s3eCameraStop()
 *
 * This example allows the user to start camera streaming at various sizes.
 * As frames are received from the camera they are copied to a buffer for
 * later display. The camera can be stopped and restarted at a different size.
 *
 * Frame data is provided in a s3eCameraFrameData structure passed to a
 * callback registered using s3eCameraRegister(). The buffer pointer passed in
 * the s3eCameraFrameData structure should not be stored. It is only guaranteed
 * to exist for the duration of the callback. Hence, the frame data must be
 * copied for later use.
 *
 * No guarantees about the image size are provided. Sizes are just hints
 * relative to the device's screen size. This is due to most cameras only
 * providing a fixed number of sizes which varies by device.
 *
 * @include s3eCameraBasic.cpp
 */

#include "ExamplesMain.h"
#include "s3eCamera.h"
#include "s3eDebug.h"
#include <sys/param.h>

enum CameraState
{
    kStarted,
    kStopped,
    kUnSupported
};

static CameraState g_Status = kUnSupported;
static Button* g_ButtonStartMax = 0;
static Button* g_ButtonStartLarge = 0;
static Button* g_ButtonStartMedium = 0;
static Button* g_ButtonStartSmall = 0;
static Button* g_ButtonTakeImageToFile = 0;
static Button* g_ButtonTakeImageToBuffer = 0;
static Button* g_ButtonCameraSwitch = 0;
static Button* g_ButtonAutoFocus = 0;
static Button* g_ButtonStop = 0;
static s3eBool g_CameraSupported = S3E_FALSE;
static s3eCameraStreamingSizeHint g_CameraSizeHint = S3E_CAMERA_STREAMING_SIZE_HINT_LARGEST;
// Block app from flooding user with error messages when Out-of-memory occurred.
static s3eBool g_OutOfMem = S3E_FALSE;

static uint16* g_FrameData = 0;
static int32 g_FrameWidth = 0;
static int32 g_FrameHeight = 0;
static int32 g_FramePitch = 0;

static bool g_StartCameraOnResume = false;
static bool g_AutoFocusActive = false;
static s3eResult g_AutoFocusResult = S3E_RESULT_SUCCESS;
static s3eCameraError g_CameraError = S3E_CAMERA_ERR_NONE;

static s3eCameraTakePictureResult g_PictureInfo;

// Start Windows Phone 8 camera on application resume
int32 HandleResume(void* sysData, void* userData)
{
    if (g_StartCameraOnResume)
    {
        g_StartCameraOnResume = false;
        if (s3eCameraStart(g_CameraSizeHint, S3E_CAMERA_PIXEL_TYPE_RGB565_CONVERTED) == S3E_RESULT_SUCCESS)
            g_Status = kStarted;
    }

    return 0;
}

int32 HandleSuspend(void* sysData, void* userData)
{
    if (g_Status == kStarted)
    {
        s3eCameraStop();
        g_StartCameraOnResume = true;
    }

    return 0;
}

// Callback that gets filled with camera buffer data
static int32 cameraUpdate(void* systemData, void* userData)
{
    // Frame data is no longer valid once s3eCameraStop is called
    if (g_Status != kStarted)
        return 0;

    if (g_OutOfMem)
        return 0;

    s3eCameraFrameData* m_Frame = (s3eCameraFrameData*)systemData;

    // Copying frame data since it is not guaranteed to persist and will be
    // overwritten with next buffer. Using the data directly will increase
    // tearing but improve responsiveness. Note that s3eCamera beta does not
    // lock the frame buffer so cannot guarantee no tearing. This will be
    // addressed in the final version.
    if (!g_FrameData ||
        m_Frame->m_Width != g_FrameWidth ||
        m_Frame->m_Height != g_FrameHeight)
    {
        g_FrameWidth = m_Frame->m_Width;
        g_FrameHeight = m_Frame->m_Height;
        g_FramePitch = m_Frame->m_Pitch;

        g_FrameData = (uint16*)s3eRealloc(g_FrameData, g_FrameHeight * g_FramePitch);
        if (!g_FrameData)
        {
            g_OutOfMem = S3E_TRUE;
        }
    }

    if (g_FrameData)
        memcpy(g_FrameData, m_Frame->m_Data, g_FrameHeight * g_FramePitch);

    return 0;
}

static int32 cameraStop(void* systemData, void* userData)
{
    g_Status = kStopped;
    return 0;
}

void ButtonCallbackStart(struct Button* button)
{
    if (g_Status != kStopped)
    {
        return;
    }

    if (button == g_ButtonStartSmall)
    {
        g_CameraSizeHint = S3E_CAMERA_STREAMING_SIZE_HINT_SMALLEST;
    }
    else if (button == g_ButtonStartMedium)
    {
        g_CameraSizeHint = S3E_CAMERA_STREAMING_SIZE_HINT_MEDIUM;
    }
    else if (button == g_ButtonStartLarge)
    {
        g_CameraSizeHint = S3E_CAMERA_STREAMING_SIZE_HINT_LARGEST;
    }
    else if (button == g_ButtonStartMax)
    {
        g_CameraSizeHint = S3E_CAMERA_STREAMING_SIZE_HINT_MAXIMUM;
    }

    if (s3eCameraStart(g_CameraSizeHint, S3E_CAMERA_PIXEL_TYPE_RGB565_CONVERTED) == S3E_RESULT_SUCCESS)
    {
        g_Status = kStarted;
    }
    g_CameraError = s3eCameraGetError();

    //Update autofocus button text
    g_AutoFocusActive = s3eCameraGetInt(S3E_CAMERA_AUTO_FOCUS) ? true : false;
    SetButtonName(g_ButtonAutoFocus, (g_AutoFocusActive ? "Disable AutoFocus" : "Enable AutoFocus"));
    s3eCameraGetError();

    // Reset g_OutOfMem just in case there is enough memory for smaller resolution.
    g_OutOfMem = S3E_FALSE;
}

void RefreshStartButtons()
{
    s3eCameraType currentStatus = (s3eCameraType)s3eCameraGetInt(S3E_CAMERA_TYPE);
    if (currentStatus == S3E_CAMERA_TYPE_REAR)
    {
        SetButtonName(g_ButtonStartMax, "Start (Maximum) Rear");
        SetButtonName(g_ButtonStartLarge, "Start (Large) Rear");
        SetButtonName(g_ButtonStartMedium, "Start (Medium) Rear");
        SetButtonName(g_ButtonStartSmall, "Start (Small) Rear");
    }
    else
    {
        SetButtonName(g_ButtonStartMax, "Start (Maximum) Front");
        SetButtonName(g_ButtonStartLarge, "Start (Large) Front");
        SetButtonName(g_ButtonStartMedium, "Start (Medium) Front");
        SetButtonName(g_ButtonStartSmall, "Start (Small) Front");
    }
}

void ButtonCallbackCameraSwitch(struct Button* button)
{
    s3eCameraType currentStatus = (s3eCameraType)s3eCameraGetInt(S3E_CAMERA_TYPE);
    s3eCameraSetInt(S3E_CAMERA_TYPE, currentStatus == S3E_CAMERA_TYPE_FRONT ? S3E_CAMERA_TYPE_REAR : S3E_CAMERA_TYPE_FRONT);
    g_CameraError = s3eCameraGetError();
    RefreshStartButtons();
}

void ButtonCallbackAutoFocus(struct Button* button)
{
    g_AutoFocusResult = s3eCameraSetInt(S3E_CAMERA_AUTO_FOCUS, (g_AutoFocusActive ? 0 : 1));

    if (g_AutoFocusResult == S3E_RESULT_SUCCESS)
        g_AutoFocusActive = s3eCameraGetInt(S3E_CAMERA_AUTO_FOCUS) ? true : false;

    g_CameraError = s3eCameraGetError();
    SetButtonName(g_ButtonAutoFocus, (g_AutoFocusActive ? "Disable AutoFocus" : "Enable AutoFocus"));

    // Reset g_OutOfMem just in case there is enough memory for smaller resolution.
    g_OutOfMem = S3E_FALSE;
}

void ButtonCallbackStop(struct Button* button)
{
    if (s3eCameraStop() == S3E_RESULT_SUCCESS)
    {
        g_Status = kStopped;
        s3eFree(g_FrameData);
        g_FrameData = NULL;
    }
    g_AutoFocusResult = S3E_RESULT_SUCCESS;
    g_CameraError = s3eCameraGetError();
}

int32 OnCameraTakeImage(void* sysData, void* userData)
{
    ButtonCallbackStop(NULL);
    s3eCameraUnRegister(S3E_CAMERA_TAKE_IMAGE, OnCameraTakeImage);

    s3eCameraTakePictureResult* pResult = reinterpret_cast<s3eCameraTakePictureResult*>(sysData);
    if (pResult->m_Error == S3E_CAMERA_ERR_NONE)
    {
        g_PictureInfo = *pResult;
        g_PictureInfo.m_Buffer = new unsigned char[g_PictureInfo.m_BufferSize];
        memcpy(g_PictureInfo.m_Buffer, pResult->m_Buffer, g_PictureInfo.m_BufferSize);
    }
    else
    {
        g_CameraError = pResult->m_Error;
    }
    return 0;
}

void ButtonCallbackTakeImageToFile(struct Button* button)
{
    s3eCameraRegister(S3E_CAMERA_TAKE_IMAGE, OnCameraTakeImage, NULL);
    s3eCameraTakePictureToFile(S3E_CAMERA_FORMAT_JPG, S3E_CAMERA_SAVE_PATH_GALLERY);
}

void ButtonCallbackTakeImageToBuffer(struct Button* button)
{
    s3eCameraRegister(S3E_CAMERA_TAKE_IMAGE, OnCameraTakeImage, NULL);
    s3eCameraTakePictureToBuffer(S3E_CAMERA_FORMAT_JPG);
}

void ExampleInit()
{
    g_ButtonStartMax        = NewButton("Start (Maximum)",      ButtonCallbackStart);
    g_ButtonStartLarge      = NewButton("Start (Large)",        ButtonCallbackStart);
    g_ButtonStartMedium     = NewButton("Start (Medium)",       ButtonCallbackStart);
    g_ButtonStartSmall      = NewButton("Start (Small)",        ButtonCallbackStart);
    g_ButtonCameraSwitch    = NewButton("Switch Camera",        ButtonCallbackCameraSwitch);
    g_ButtonAutoFocus         = NewButton("Enable AutoFocus",     ButtonCallbackAutoFocus);
    g_ButtonStop              = NewButton("Stop",                 ButtonCallbackStop);
    if (s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_ANDROID)
    {
        g_ButtonTakeImageToFile   = NewButton("Take image to file",   ButtonCallbackTakeImageToFile);
        g_ButtonTakeImageToBuffer = NewButton("Take image to buffer", ButtonCallbackTakeImageToBuffer);
    }

    g_ButtonStartMax->m_Enabled   = false;
    g_ButtonStartLarge->m_Enabled   = false;
    g_ButtonStartMedium->m_Enabled  = false;
    g_ButtonStartSmall->m_Enabled   = false;
    g_ButtonCameraSwitch->m_Enabled = false;
    g_ButtonAutoFocus->m_Enabled    = false;
    g_ButtonStop->m_Enabled         = false;
    if (g_ButtonTakeImageToFile)
        g_ButtonTakeImageToFile->m_Enabled = false;
    if (g_ButtonTakeImageToBuffer)
        g_ButtonTakeImageToBuffer->m_Enabled = false;

    g_CameraSupported = s3eCameraAvailable();

    if (g_CameraSupported)
    {
        if (S3E_RESULT_ERROR == s3eCameraRegister(S3E_CAMERA_UPDATE_STREAMING, cameraUpdate, 0))
        {
            AppendMessage("Could not register frame receiver callback");
        }
        if (S3E_RESULT_ERROR == s3eCameraRegister(S3E_CAMERA_STOP_STREAMING, cameraStop, 0))
        {
            AppendMessage("Could not register stop camera callback");
        }
        g_Status = kStopped;
        g_ButtonAutoFocus->m_Enabled = true;
        RefreshStartButtons();
    }
    else
    {
        AppendMessage("s3eCamera not available");
    }

    // Handle suspend/resume on Windows Phone 8 to start/stop
    // camera depending on application state
    if (s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WP8)
    {
        s3eDeviceRegister(S3E_DEVICE_PAUSE, HandleSuspend, NULL);
        s3eDeviceRegister(S3E_DEVICE_UNPAUSE, HandleResume, NULL);
    }
}

void ExampleTerm()
{
    if (s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WP8)
    {
        s3eDeviceUnRegister(S3E_DEVICE_PAUSE, HandleSuspend);
        s3eDeviceUnRegister(S3E_DEVICE_UNPAUSE, HandleResume);
    }

    s3eFree(g_FrameData);
}


int32 FreeImageData(void* sysData, void* userData)
{
    delete[] (unsigned char*)g_PictureInfo.m_Buffer;
    g_PictureInfo.m_Buffer = 0;

    return 0;
}

void PlayImage()
{
    if (g_PictureInfo.m_Buffer == 0)
        return;

    g_CameraError = S3E_CAMERA_ERR_NONE;
    if (g_PictureInfo.m_Type == S3E_CAMERA_TAKE_PICTURE_TYPE_FILE)
    {
        if (s3eVideoPlay((char*)g_PictureInfo.m_Buffer, 1) == S3E_RESULT_ERROR)
        {
            g_CameraError = S3E_CAMERA_ERR_IO;
        }
    }
    else
    {
        if (s3eVideoPlayFromBuffer(g_PictureInfo.m_Buffer, g_PictureInfo.m_BufferSize, 1) == S3E_RESULT_ERROR)
        {
            g_CameraError = S3E_CAMERA_ERR_IO;
        }
    }
    if (g_CameraError == S3E_CAMERA_ERR_NONE)
    {
        s3ePointerRegister(S3E_POINTER_BUTTON_EVENT, FreeImageData, NULL);

        // Wait until any user event.
        do
        {
            s3eKeyboardUpdate();
            s3ePointerUpdate();
            s3eDeviceYield(100);
            s3eSurfaceShow();
        }
        while (g_PictureInfo.m_Buffer && !ExampleCheckQuit() && !s3eKeyboardAnyKey());
        s3ePointerUnRegister(S3E_POINTER_BUTTON_EVENT, FreeImageData);
    }
    else
    {
        FreeImageData(NULL, NULL);
    }
}

// Start/stop playback
bool ExampleUpdate()
{
    if (g_CameraSupported)
    {
        g_ButtonStartMax->m_Enabled = (g_Status == kStopped);
        g_ButtonStartLarge->m_Enabled = (g_Status == kStopped);
        g_ButtonStartMedium->m_Enabled = (g_Status == kStopped);
        g_ButtonStartSmall->m_Enabled = (g_Status == kStopped);
        g_ButtonCameraSwitch->m_Enabled = (g_Status == kStopped);
        g_ButtonAutoFocus->m_Enabled = (g_Status == kStarted);
        g_ButtonStop->m_Enabled = (g_Status == kStarted);
        if (g_ButtonTakeImageToFile)
            g_ButtonTakeImageToFile->m_Enabled = (g_Status == kStarted);
        if (g_ButtonTakeImageToBuffer)
            g_ButtonTakeImageToBuffer->m_Enabled = (g_Status == kStarted);
    }

    PlayImage();
    return true;
}

void ExampleRender()
{
    // Copy frame data to screen
    if (g_Status == kStarted && g_FrameData)
    {
        uint8* screen = (uint8*)s3eSurfacePtr();
        uint8* buffer = (uint8*)g_FrameData;

        const int screen_height = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT);
        const int screen_pitch  = s3eSurfaceGetInt(S3E_SURFACE_PITCH);
        const int screen_width  = s3eSurfaceGetInt(S3E_SURFACE_WIDTH);

        const int height = MIN(g_FrameHeight, screen_height);
        const int width  = MIN(g_FrameWidth,  screen_width);

        for (int y = 0; y < height; y++)
        {
            // Surface and Camera are set to RGB565,
            // that is width by 2 bytes need to be copied per each line.
            // Note: avoid copying full screen_pitch bytes per each line,
            // as this may cause screen buffer overflow,
            // in case pitch exceeds width due to alignment.
            memcpy(&screen[y * screen_pitch],
                   &buffer[y * g_FramePitch], width * 2);
        }
    }

    // Display status info
    int32 camStatus;
    const int textHeight = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);

    int ypos = GetYBelowButtons() + textHeight * 2;

    if (g_Status == kStarted)
    {
        camStatus = s3eCameraGetInt(S3E_CAMERA_STATUS);
        if (camStatus == S3E_CAMERA_IDLE)
            s3eDebugPrint(10, ypos, "`x666666Camera is idle", 0);
        else if (camStatus == S3E_CAMERA_STREAMING)
            s3eDebugPrint(10, ypos, "`x666666Camera is streaming", 0);
        else if (camStatus == S3E_CAMERA_FAILED)
            s3eDebugPrint(10, ypos, "`x666666Error occurred during capture", 0);
        else if (camStatus == S3E_CAMERA_SUSPENDED)
            s3eDebugPrint(10, ypos, "`x666666Camera is paused due to device suspend", 0);
        else if (camStatus == S3E_CAMERA_RESTARTING)
            s3eDebugPrint(10, ypos, "`x666666Camera is being restarted after a device suspend", 0);

        if (g_OutOfMem)
            s3eDebugPrint(10, ypos += textHeight * 2, "`x666666Out of Memory occurred. Cannot render camera frame", 0);
    }
    else if (g_Status == kStopped)
        s3eDebugPrint(10, ypos, "`x666666Camera Stopped", 0);
    else
        s3eDebugPrint(10, ypos, "`x666666The camera is not available", 0);

    if (g_AutoFocusResult == S3E_RESULT_ERROR)
    {
        s3eDebugPrint(10, ypos += textHeight * 2, "`x666666Autofocus is not supported", 0);
    }

    switch (g_CameraError)
    {
    case S3E_CAMERA_ERR_UNAVAIL:
        s3eDebugPrint(10, ypos += textHeight * 2, "`x666666Device not found.", 0);
        break;
    case S3E_CAMERA_ERR_UNSUPPORTED:
        s3eDebugPrint(10, ypos += textHeight * 2, "`x666666Unsupported feature.", 0);
        break;
    case S3E_CAMERA_ERR_ACCESS:
        s3eDebugPrint(10, ypos += textHeight * 2, "`x666666Camera access error.", 0);
        break;
    case S3E_CAMERA_ERR_FORMAT:
        s3eDebugPrint(10, ypos += textHeight * 2, "`x666666Unsupported stream format.", 0);
        break;
    case S3E_CAMERA_ERR_OPEN_FILE:
        s3eDebugPrint(10, ypos += textHeight * 2, "`x666666Can not open file to save.", 0);
        break;
    case S3E_CAMERA_ERR_IO:
        s3eDebugPrint(10, ypos += textHeight * 2, "`x666666Input or output error.", 0);
        break;
    case S3E_CAMERA_ERR_UNKNOWN:
        s3eDebugPrint(10, ypos += textHeight * 2, "`x666666Unknown error.", 0);
        break;
    case S3E_CAMERA_ERR_OPERATION_NA:
        s3eDebugPrint(10, ypos += textHeight * 2, "`x666666Operation is not available.", 0);
        break;
    default:
        if (g_CameraError != S3E_CAMERA_ERR_NONE)
            s3eDebugPrintf(10, ypos += textHeight * 2, 0, "`x666666Camera error: %d", g_CameraError);
        break;
    }
}
