/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EIOSIAd S3E iAd Example
 * The following example demonstrates the iAd extension.
 *
 * The following functions are used to achieve this:
 * - s3eIOSIAdStart()
 * - s3eIOSIAdStop()
 * - s3eIOSIAdSetInt()
 * - s3eIOSIAdGetInt()
 *
 * @include s3eIOSIAd.cpp
 */

#include "ExamplesMain.h"
#include "s3eIOSIAd.h"

static Button* g_ButtonStart = NULL;
static Button* g_ButtonStop = NULL;
static Button* g_ButtonShow = NULL;
static Button* g_ButtonHide = NULL;

static int bh = 0;
static int bw = 0;
static int failcode = 0;

int32 iADMinimised(void* systemData, void* userData)
{
    //Application resume information here
    return 1;
}

int32 iADMaximised(void* systemData, void* userData)
{
    //Application pause information here
    return 1;
}

int32 iADFailed(void* systemData, void* userData)
{
    //Application failed information here
    failcode = *(int *)systemData;
    return 1;
}

void ExampleInit()
{
    g_ButtonStart = NewButton("Start iAd");
    g_ButtonStop = NewButton("Stop iAd");
    g_ButtonShow = NewButton("Show Banner");
    g_ButtonHide = NewButton("Hide Banner");

    //Setup application callbacks
    s3eIOSIAdRegister(S3E_IOSIAD_CALLBACK_AD_STARTING, iADMaximised, NULL);
    s3eIOSIAdRegister(S3E_IOSIAD_CALLBACK_AD_FINISHED, iADMinimised, NULL);
    s3eIOSIAdRegister(S3E_IOSIAD_CALLBACK_FAILED, iADFailed, NULL);
}

void ExampleTerm()
{
}

bool ExampleUpdate()
{
    if (!s3eIOSIAdAvailable())
        return true;

    g_ButtonStart->m_Enabled = !s3eIOSIAdGetInt(S3E_IOSIAD_RUNNING);
    g_ButtonStop->m_Enabled = s3eIOSIAdGetInt(S3E_IOSIAD_RUNNING) != 0;
    g_ButtonShow->m_Enabled = !s3eIOSIAdGetInt(S3E_IOSIAD_BANNER_SHOW) && s3eIOSIAdGetInt(S3E_IOSIAD_RUNNING);
    g_ButtonHide->m_Enabled = s3eIOSIAdGetInt(S3E_IOSIAD_BANNER_SHOW) != 0;

    Button* pressed = GetSelectedButton();
    if (pressed == g_ButtonStart)
    {
        s3eIOSIAdStart();
    }
    else if (pressed == g_ButtonStop)
    {
        s3eIOSIAdStop();
    }
    else if (pressed == g_ButtonHide)
    {
        s3eIOSIAdSetInt(S3E_IOSIAD_BANNER_SHOW, 0);
    }
    else if (pressed == g_ButtonShow)
    {
        s3eIOSIAdSetInt(S3E_IOSIAD_BANNER_SHOW, 1);
        bh = s3eIOSIAdGetInt(S3E_IOSIAD_BANNER_HEIGHT);
        bw = s3eIOSIAdGetInt(S3E_IOSIAD_BANNER_WIDTH);
    }

    return true;
}

#define FONT_COLOR "`x666666"

void ExampleRender()
{
    if (!s3eIOSIAdAvailable())
    {
        s3eDebugPrint(50, 100, "`xff0000iAd extension not found.", 1);
        return;
    }

    int lineHeight = s3eDebugGetInt(S3E_DEBUG_FONT_HEIGHT);
    lineHeight += lineHeight / 4;
    int y = GetYBelowButtons() + lineHeight;

    if (s3eIOSIAdGetInt(S3E_IOSIAD_RUNNING))
    {
        s3eDebugPrintf(10, y, 1, FONT_COLOR "iAd is running");
        y += lineHeight;
    }
    if (s3eIOSIAdGetInt(S3E_IOSIAD_BANNER_LOADED))
    {
        s3eDebugPrintf(10, y, 1, FONT_COLOR "Banner is loaded");
        y += lineHeight;
    }
    if (s3eIOSIAdGetInt(S3E_IOSIAD_BANNER_SHOW))
    {
        s3eDebugPrintf(10, y, 1, FONT_COLOR "Banner is shown");
        y += lineHeight;
    }
    if (s3eIOSIAdGetInt(S3E_IOSIAD_BANNER_SHOW) && s3eIOSIAdGetInt(S3E_IOSIAD_BANNER_LOADED))
    {
        s3eDebugPrintf(10, y, 1, FONT_COLOR "Banner width: %d", bw);
        y += lineHeight;
        s3eDebugPrintf(10, y, 1, FONT_COLOR "Banner Height: %d", bh);
        y += lineHeight;
    }

    s3eDebugPrintf(10, y, 1, FONT_COLOR "Last Fail Code: %d", failcode);
    y += lineHeight;

    s3eDebugPrintf(10, y, 1, FONT_COLOR "Surface size: w=%d ,h=%d", s3eSurfaceGetInt(S3E_SURFACE_WIDTH), s3eSurfaceGetInt(S3E_SURFACE_HEIGHT));
    y += lineHeight;
}
