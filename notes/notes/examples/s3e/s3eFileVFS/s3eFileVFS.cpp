/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EFileVFS S3E File VFS Example
 *
 * The following example demonstrates virtual file system functionality including
 * playing media from a mounted zip file
 *
 * The main functions used to achieve this are:
 * - s3eVFSMount()
 * - s3eAudioPlay()
 * - s3eAudioStop()
 *
 * The application waits for buttons to be pressed before using any of the
 * virtual file system functionality. Detection of button presses are handled using the
 * generic code in ExamplesMain.cpp
 *
 * MKB File:
 * @include s3eFileVFS.mkb
 *
 * ICF File:
 * @include data/app.icf
 *
 * Source:
 * @include s3eFileVFS.cpp
 */

#include "ExamplesMain.h"
#include "IwFile.h"
#include "s3eVFS.h"

#include <dirent.h>
#include <errno.h>

static Button* g_ButtonList;
static Button* g_ButtonCreate;
static Button* g_ButtonMountZip;
static Button* g_ButtonPlayAudio;
static Button* g_ButtonStopAudio;

void ExampleInit()
{
    g_ButtonList = NewButton("List Contents");
    g_ButtonCreate = NewButton("Create File");
    g_ButtonMountZip = NewButton("Mount Zip");
    g_ButtonPlayAudio = NewButton("Play Audio From Zip");
    g_ButtonStopAudio = NewButton("Stop Audio");

    g_ButtonPlayAudio->m_Enabled = false;
    g_ButtonStopAudio->m_Enabled = false;

    InitMessages(30, 128);
}

void ExampleTerm()
{
}

bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();
    if (pressed == g_ButtonList)
    {
        ClearMessages();
        DIR* dir = opendir("rst://");
        if (!dir)
        {
            AppendMessageColour(RED, "error listing rst:// (%d)", s3eFileGetError());
            return true;
        }
        struct dirent* entry;
        int count = 0;
        while ((entry = readdir(dir)))
        {
            AppendMessageColour(BLACK, "%d: %s", ++count, entry->d_name);
        }
        closedir(dir);
    }

    if (pressed == g_ButtonCreate)
    {
        FILE* f = fopen("rst://testfile.txt", "w");
        if (!f)
        {
            AppendMessageColour(RED, "fopen failed: %s", strerror(errno));
        }
        else
        {
            fputs("hello world\n", f);
            fclose(f);
        }
    }

    if (pressed == g_ButtonMountZip)
    {
        const char* zipname = "test.zip";
        if (s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_ANDROID)
        {
            // copy the zip file to the sd card to demonstrate mounting of a zip file not within the apk
            IwFileCopy("raw:///sdcard/test.zip", zipname);
            zipname = "raw:///sdcard/test.zip";
        }
        if (s3eVFSMount(zipname, "zipmount://", 0) == S3E_RESULT_SUCCESS)
        {
            AppendMessageColour(BLACK, "Mounted test.zip at zipmount://");
            g_ButtonMountZip->m_Enabled = false;
            g_ButtonPlayAudio->m_Enabled = true;
        }
        else
            AppendMessageColour(RED, "Error mounting zip file %s", zipname);
    }

    if (pressed == g_ButtonPlayAudio)
    {
        if (s3eAudioPlay("zipmount://test.mp3",0) == S3E_RESULT_SUCCESS)
        {
            g_ButtonPlayAudio->m_Enabled = false;
            g_ButtonStopAudio->m_Enabled = true;
        }
    }

    if (pressed == g_ButtonStopAudio)
    {
        s3eAudioStop();
        g_ButtonStopAudio->m_Enabled = false;
        g_ButtonPlayAudio->m_Enabled = true;
    }

    return true;
}

/*
 * The following function outputs any errors that have occurred and the
 * state of the File System.
 */
void ExampleRender()
{
    char path[512] = "";
    s3eFileGetFileString("rst://", S3E_FILE_REAL_PATH, path, 512);
    int height = s3eDebugGetInt(S3E_DEBUG_FONT_HEIGHT);

    int y = GetYBelowButtons();
    y += 2*height;
    s3eDebugPrintf(10, y, true, "`x666666Storage Root: %s", path);
    y += 2*height;
    PrintMessages(10, y);
}
