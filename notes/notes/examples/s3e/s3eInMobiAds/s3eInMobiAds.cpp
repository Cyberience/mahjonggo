//
//  InMobiSampleApp.cpp
//
//  Copyright (c) 2013 InMobi. All rights reserved.
//
/**
 * @page ExampleS3EInMobiAds S3E InMobi Ads Example
 *
 * This Application demonstrates usage of the InMobiAds Marmalade
 * extension on the Android and iOS platforms. The application is intended to be functional
 * and demonstrate how to use the extension code without the distraction of complex UI.
 * This example demonstrates how to display banner and interstitial ads as well as 
 * set up callbacks to be notified of specific events such as when an ad has finished 
 * loading and is ready to be displayed.
 *
 * See S3E InMobiAds overview in the Marmalade API Reference for more information.
 *
 * The main functions demonstrated in this example are:
 * - inmobi_set_loglevel()
 * - inmobi_initialize()
 * - inmobi_banner_init()
 * - inmobi_banner_disable_hardware_acceleration()
 * - inmobi_banner_release()
 * - inmobi_banner_load()
 * - inmobi_banner_show()
 * - inmobi_banner_hide()
 * - inmobi_banner_move()
 * - inmobi_interstitial_init()
 * - inmobi_interstitial_release()
 * - inmobi_interstitial_load()
 * - inmobi_interstitial_show()
 * - analytics_beginSection()
 * - analytics_endSection()
 *
 * @include s3eInMobiAds.cpp
 */
 
#include "s3e.h"
#include "ExamplesMain.h"
#include "s3eInMobiAds.h"
#include <string>

static Button* g_InitOrReleaseBanners;
static Button* g_LoadBannerTop;
static Button* g_LoadBannerBottom;
static Button* g_HideOrShowBanners;
static Button* g_ExchangeBanners;
static Button* g_InitOrReleaseInterstitial;
static Button* g_LoadInterstitial;
static Button* g_ShowInterstitial;

const char *test_publisher_id = "your_in_mobi_property_id_here";
static bool banners_initialized, banners_hidden, int_initialized;
const char  *AdRequestParams = "age:25,education:EDUCATION_HIGHSCHOOLORLESS";
static InMobiAd *top_ad;
static int top_ad_pos_x, top_ad_pos_y;
static InMobiAd *bottom_ad;
static int bottom_ad_pos_x, bottom_ad_pos_y;
static int width, height, orientation;

/**
 * Banner Ad Callbacks.
 */
static int ad_request_completed(InMobiAd* ad, void *systemData, void *userData)
{
    s3eDebugOutputString("Callback function - INMOBIADS_CALLBACK_AD_REQUEST_COMPLETED");
    g_HideOrShowBanners->m_Enabled = true;
    return S3E_RESULT_SUCCESS;
}
static int ad_request_failed(InMobiAd* ad, void *systemData, void *userData)
{
    char *error = (char *)systemData;
    std::string str = "Callback function - INMOBIADS_CALLBACK_AD_REQUEST_FAILED - Error: ";
    str.append(error);
    const char * outputString = str.c_str();
    s3eDebugOutputString(outputString);
    g_HideOrShowBanners->m_Enabled = false;
    return S3E_RESULT_SUCCESS;
}

static int ad_show_adscreen(InMobiAd* ad, void *systemData, void *userData)
{
    s3eDebugOutputString("Callback function - INMOBIADS_CALLBACK_AD_SHOW_ADSCREEN");
    return S3E_RESULT_SUCCESS;
}
static int ad_dismiss_adscreen(InMobiAd* ad, void *systemData, void *userData) {
    s3eDebugOutputString("Callback function - INMOBIADS_CALLBACK_AD_DISMISS_ADSCREEN");
    return S3E_RESULT_SUCCESS;
}
static int ad_leave_application(InMobiAd* ad, void *systemData, void *userData) {
    s3eDebugOutputString("Callback function - INMOBIADS_CALLBACK_AD_LEAVE_APPLICATION");
    return S3E_RESULT_SUCCESS;
}
static int ad_interacted(InMobiAd* ad, void *systemData, void *userData){ 
    s3eDebugOutputString("Callback function - INMOBIADS_CALLBACK_AD_INTERACTED");
    return S3E_RESULT_SUCCESS;
}
/**
 * Interstitial Ad Callbacks.
 */
static int int_request_completed(void *systemData, void *userData)
{
    s3eDebugOutputString("Callback function() - INMOBIADS_CALLBACK_INT_REQUEST_COMPLETED;");
    g_ShowInterstitial->m_Enabled = true;
    return S3E_RESULT_SUCCESS;
}
static int int_request_failed(void *systemData, void *userData)
{
    const char *error = (const char *)systemData;
    std::string str = "Callback function() - INMOBIADS_CALLBACK_AD_REQUEST_FAILED - Error: ";
    str.append(error);
    const char * outputString = str.c_str();
    s3eDebugOutputString(outputString);
    g_ShowInterstitial->m_Enabled = false;
    return S3E_RESULT_SUCCESS;
}
static int int_show_adscreen(void *systemData, void *userData) {
    s3eDebugOutputString("Callback function() - INMOBIADS_CALLBACK_INT_SHOW_ADSCREEN;");
    return S3E_RESULT_SUCCESS;
}
static int int_dismiss_adscreen(void *systemData, void *userData)
{
    g_ShowInterstitial->m_Enabled = false;
    s3eDebugOutputString("Callback function() - INMOBIADS_CALLBACK_INT_DISMISS_ADSCREEN;");
    return S3E_RESULT_SUCCESS;
}
static int int_leave_application(void *systemData, void *userData) {
    s3eDebugOutputString("Callback function() - INMOBIADS_CALLBACK_INT_LEAVE_APPLICATION;");
    return S3E_RESULT_SUCCESS;
}

static int int_ad_interacted(void *systemData, void *userData)
{
   s3eDebugOutputString("Callback function() - INMOBIADS_CALLBACK_INT_INTERACTED");
   const char* params = (const char*) systemData;
   AppendMessageColour(BLUE, "INTERSTITIAL COMPLETED");
   AppendMessageColour(BLUE, params);
   return S3E_RESULT_SUCCESS;
}

static int int_ad_rewarded(void *systemData, void *userData)
{
    s3eDebugOutputString("Callback function() - INMOBIADS_CALLBACK_INT_REWARDED_AD_COMPLETED");
    const char* params = (const char*) systemData;
    char output[256] = {0};
    sprintf (output, "REWARD received: %s", params);
    AppendMessageColour(BLUE, output);
    s3eDebugOutputString(output);
    return S3E_RESULT_SUCCESS;
}

static int ScreenSizeChangeCallback(void *systemData, void *userData)
{
    s3eSurfaceOrientation *obj = (s3eSurfaceOrientation *)systemData;

    if (obj->m_OrientationChanged)
    {
        orientation = obj->m_DeviceBlitDirection;
    }
    width       = obj->m_Width;
    height      = obj->m_Height;

    if (top_ad)
    {
        top_ad_pos_x = 0;
        top_ad_pos_y = 0;
        inmobi_banner_move(top_ad, top_ad_pos_x, top_ad_pos_y, orientation);
    }

    if (bottom_ad)
    {
        bottom_ad_pos_x = 0;
        bottom_ad_pos_y = height * 8;
        inmobi_banner_move(bottom_ad, bottom_ad_pos_x, bottom_ad_pos_y, orientation);
    }
    
    return S3E_RESULT_SUCCESS;
}

static int DeviceStateChangeCallback(void *systemData, void *userData)
{
    /*
     *write code to handle INMOBIADS_CALLBACK_AD/INT_SHOW_ADSCREEN in case of interstitial/two-piece on Android devices
     */
    return S3E_RESULT_SUCCESS;
}

void ExampleInit()
{
    InitMessages(10,60);
    if (!s3eInMobiAdsAvailable()) {
        s3eDebugOutputString("Error - InMobiAds extension not available.");
    }
    /**
     * Initializing variables.
     */
    top_ad = 0;
    bottom_ad = 0;
    banners_initialized = false;
    banners_hidden = false;
    int_initialized = false;
    orientation = 0;
    
    width       = s3eSurfaceGetInt(S3E_SURFACE_WIDTH);
    height      = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT);
    orientation = s3eSurfaceGetInt(S3E_SURFACE_DEVICE_BLIT_DIRECTION);
    
    top_ad_pos_x = 0;
    top_ad_pos_y = 0;
    bottom_ad_pos_x = 0;
    bottom_ad_pos_y = height * 4 / 5;
    
    s3eSurfaceRegister(S3E_SURFACE_SCREENSIZE, ScreenSizeChangeCallback, NULL);
    
    /*
     * Register for this callback to receive S3E_DEVICE_PAUSE callback in place of INMOBIADS_CALLBACK_AD/INT_SHOW_ADSCREEN in case of interstitial/ two piece ads on android devices
     */
    s3eDeviceRegister(S3E_DEVICE_PAUSE, DeviceStateChangeCallback, NULL);
    
    /**
     * Initializing buttons.
     */
    (void) NewButton("");
    g_InitOrReleaseBanners      = NewButton("Init/Load BannerAds");
    g_LoadBannerTop             = NewButton("Show BannerAd (Top)");
    g_LoadBannerBottom          = NewButton("Show BannerAd (Bottom)");
    g_HideOrShowBanners         = NewButton("Hide BannerAds");
    g_ExchangeBanners           = NewButton("Exchange Banner Ads");
    g_InitOrReleaseInterstitial = NewButton("Init Interstitial Ads");
    g_LoadInterstitial          = NewButton("Load InterstitialAd");
    g_ShowInterstitial          = NewButton("Show InterstitialAd");

    g_LoadInterstitial->m_Enabled = false;
    g_ShowInterstitial->m_Enabled = false;
    g_LoadBannerTop->m_Enabled = false;
    g_LoadBannerBottom->m_Enabled = false;
    g_HideOrShowBanners->m_Enabled = false;
    g_ExchangeBanners->m_Enabled = false;

    /**
     * Initialize Ad Request Parameters here.
     */
    // Setting InMobi log level
    inmobi_set_loglevel(3);
    // Initializing the InMobi SDK
    inmobi_initialize(test_publisher_id);
    
    // Starting a section
    analytics_beginSection("testSection");
    
    // Ending the section
    analytics_endSection("testSection");
    
}

void ExampleTerm() {
    DeleteButtons();
}

void registerAdCallbacks(InMobiAd* ad)
{
    InMobiAdsRegisterAdCallback(INMOBIADS_CALLBACK_AD_REQUEST_COMPLETED, ad_request_completed, NULL, ad);
    InMobiAdsRegisterAdCallback(INMOBIADS_CALLBACK_AD_REQUEST_FAILED, ad_request_failed, NULL, ad);
    InMobiAdsRegisterAdCallback(INMOBIADS_CALLBACK_AD_SHOW_ADSCREEN, ad_show_adscreen, NULL, ad);
    InMobiAdsRegisterAdCallback(INMOBIADS_CALLBACK_AD_DISMISS_ADSCREEN, ad_dismiss_adscreen, NULL, ad);
    InMobiAdsRegisterAdCallback(INMOBIADS_CALLBACK_AD_LEAVE_APPLICATION, ad_leave_application, NULL, ad);
    InMobiAdsRegisterAdCallback(INMOBIADS_CALLBACK_AD_INTERACTED, ad_interacted, NULL, ad);
}

void unregisterAdCallbacks(InMobiAd* ad)
{
    InMobiAdsUnRegisterAdCallback(INMOBIADS_CALLBACK_AD_REQUEST_COMPLETED, ad_request_completed, ad);
    InMobiAdsUnRegisterAdCallback(INMOBIADS_CALLBACK_AD_REQUEST_FAILED, ad_request_failed, ad);
    InMobiAdsUnRegisterAdCallback(INMOBIADS_CALLBACK_AD_SHOW_ADSCREEN, ad_show_adscreen, ad);
    InMobiAdsUnRegisterAdCallback(INMOBIADS_CALLBACK_AD_DISMISS_ADSCREEN, ad_dismiss_adscreen, ad);
    InMobiAdsUnRegisterAdCallback(INMOBIADS_CALLBACK_AD_LEAVE_APPLICATION, ad_leave_application, ad);
    InMobiAdsUnRegisterAdCallback(INMOBIADS_CALLBACK_AD_INTERACTED, ad_interacted, ad);
}

bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();
    if (pressed && pressed == g_InitOrReleaseBanners) {
        if (!banners_initialized)
        {
            SetButtonName(g_InitOrReleaseBanners, "Release Banner Ads");
            g_LoadBannerTop->m_Enabled = true;
            g_LoadBannerBottom->m_Enabled = true;
            g_ExchangeBanners->m_Enabled = true;
            banners_initialized = true;
            top_ad = inmobi_banner_init(test_publisher_id, INMOBIADS_BANNER_SIZE_320x50,
                                        top_ad_pos_x, top_ad_pos_y);
            inmobi_banner_disable_hardware_acceleration(top_ad); //Only applicable for Android
            registerAdCallbacks(top_ad);
            bottom_ad = inmobi_banner_init(test_publisher_id, INMOBIADS_BANNER_SIZE_320x50,
                                           bottom_ad_pos_x, bottom_ad_pos_y);
            inmobi_banner_disable_hardware_acceleration(bottom_ad); //Only applicable for Android
            registerAdCallbacks(bottom_ad);
        } else
        {
            SetButtonName(g_InitOrReleaseBanners, "Init Banner Ads");
            g_LoadBannerTop->m_Enabled = false;
            g_LoadBannerBottom->m_Enabled = false;
            g_HideOrShowBanners->m_Enabled = false;
            g_ExchangeBanners->m_Enabled = false;
            banners_initialized = false;
            inmobi_banner_release(top_ad);
            top_ad = 0;
            unregisterAdCallbacks(top_ad);
            inmobi_banner_release(bottom_ad);
            bottom_ad = 0;
            unregisterAdCallbacks(bottom_ad);
        }
    }
    else if(pressed && pressed == g_LoadBannerTop)
    {
        inmobi_banner_load(top_ad, AdRequestParams);
    }
    else if(pressed && pressed == g_LoadBannerBottom)
    {
        inmobi_banner_load(bottom_ad, AdRequestParams);
    }
    else if (pressed && pressed == g_HideOrShowBanners)
    {
        if (banners_hidden)
        {
            s3eResult top_hide = inmobi_banner_show(top_ad);
            s3eResult bottom_hide = inmobi_banner_show(bottom_ad);
            if (top_hide == S3E_RESULT_SUCCESS || bottom_hide == S3E_RESULT_SUCCESS)
            {
                SetButtonName(g_HideOrShowBanners, "Hide Banner Ads");
                banners_hidden = false;
            }
        } else {
            s3eResult top_show = inmobi_banner_hide(top_ad);
            s3eResult bottom_show = inmobi_banner_hide(bottom_ad);
            if (top_show == S3E_RESULT_SUCCESS || bottom_show == S3E_RESULT_SUCCESS)
            {
                SetButtonName(g_HideOrShowBanners, "Show Banner Ads");
                banners_hidden = true;
            }
        }
    }
    else if (pressed && pressed == g_ExchangeBanners) {
        inmobi_banner_move(top_ad, bottom_ad_pos_x, bottom_ad_pos_y, orientation);
        inmobi_banner_move(bottom_ad, top_ad_pos_x, top_ad_pos_y, orientation);
        InMobiAd *temp = bottom_ad; bottom_ad = top_ad; top_ad = temp;
    }
    else if(pressed && pressed == g_InitOrReleaseInterstitial) {
        if (!int_initialized)
        {
            if (inmobi_interstitial_init(test_publisher_id) == S3E_RESULT_SUCCESS)
            {
                SetButtonName(g_InitOrReleaseInterstitial, "Release Interstitial Ads");
                g_LoadInterstitial->m_Enabled = true;

                int_initialized = true;
                InMobiAdsRegisterIntCallback(INMOBIADS_CALLBACK_INT_REQUEST_COMPLETED, int_request_completed, NULL);
                InMobiAdsRegisterIntCallback(INMOBIADS_CALLBACK_INT_REQUEST_FAILED, int_request_failed, NULL);
                InMobiAdsRegisterIntCallback(INMOBIADS_CALLBACK_INT_SHOW_ADSCREEN, int_show_adscreen, NULL);
                InMobiAdsRegisterIntCallback(INMOBIADS_CALLBACK_INT_DISMISS_ADSCREEN, int_dismiss_adscreen, NULL);
                InMobiAdsRegisterIntCallback(INMOBIADS_CALLBACK_INT_LEAVE_APPLICATION, int_leave_application, NULL);
                InMobiAdsRegisterIntCallback(INMOBIADS_CALLBACK_INT_INTERACTED, int_ad_interacted, NULL);
                InMobiAdsRegisterIntCallback(INMOBIADS_CALLBACK_INT_REWARDED_AD_COMPLETED, int_ad_rewarded, NULL);
            }
        }
        else
        {
            if(inmobi_interstitial_release() == S3E_RESULT_SUCCESS)
            {
                SetButtonName(g_InitOrReleaseInterstitial, "Init Interstitial Ads");
                int_initialized = false;
                g_LoadInterstitial->m_Enabled = false;
                g_ShowInterstitial->m_Enabled = false;
                inmobi_interstitial_release();
                InMobiAdsUnRegisterIntCallback(INMOBIADS_CALLBACK_INT_REQUEST_COMPLETED, int_request_completed);
                InMobiAdsUnRegisterIntCallback(INMOBIADS_CALLBACK_INT_REQUEST_FAILED, int_request_failed);
                InMobiAdsUnRegisterIntCallback(INMOBIADS_CALLBACK_INT_SHOW_ADSCREEN, int_show_adscreen);
                InMobiAdsUnRegisterIntCallback(INMOBIADS_CALLBACK_INT_DISMISS_ADSCREEN, int_dismiss_adscreen);
                InMobiAdsUnRegisterIntCallback(INMOBIADS_CALLBACK_INT_LEAVE_APPLICATION, int_leave_application);
                InMobiAdsUnRegisterIntCallback(INMOBIADS_CALLBACK_INT_INTERACTED, int_ad_interacted);
                InMobiAdsUnRegisterIntCallback(INMOBIADS_CALLBACK_INT_REWARDED_AD_COMPLETED, int_ad_interacted);
            }
        }
    }
    else if(pressed && pressed == g_LoadInterstitial) {
        inmobi_interstitial_load(AdRequestParams);
    }
    else if(pressed && pressed == g_ShowInterstitial) {
       g_ShowInterstitial->m_Enabled = false;
       inmobi_interstitial_show();
    }
    return true;
}

void ExampleRender()
{
    int x = 20;
    int y = GetYBelowButtons();
    int lineHeight = s3eDebugGetInt(S3E_DEBUG_FONT_HEIGHT) + 2;
    
    y += lineHeight * 2;
    y += lineHeight*2;
    
    PrintMessages(x, y);
}
