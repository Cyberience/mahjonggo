/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
#pragma once

#include "s3e.h"
#include "s3eMetal.h"
#include <math.h>
#include <stdio.h>

#include "AAPLQuad.h"
#include "AAPLTexture.h"
#include "AAPLView.h"

class AAPLRenderer
{
public:
    AAPLRenderer();
    bool Configure(AAPLView* view);
    void Encode(CMetalRenderCommandEncoder* encoder);
    void ReShape(int x, int y);
    void Render(AAPLView* view);
protected:
    bool LoadShaders();
    bool PreparePipelineState();
    bool PrepareTexturedQuad();
    bool PrepareDepthStencilState();
    bool PrepareTransformBuffer();
    void PrepareTransforms();

public:
    CMetalPtr<CMetalDevice> m_Device;
    unsigned int m_ConstantDataBufferIndex;
    CMetalTextureDescriptor::PixelFormat m_DepthPixelFormat;
    CMetalTextureDescriptor::PixelFormat m_StencilPixelFormat;
    unsigned int m_SampleCount;
private:
    CMetalPtr<CMetalCommandQueue> m_CommandQueue;
    CMetalPtr<CMetalLibrary> m_ShaderLibrary;
    CMetalPtr<CMetalDepthStencilState> m_DepthState;
    CMetalPtr<CMetalRenderPipelineState> m_PipelineState;

    AAPLQuad* m_Quad;
    AAPLTexture* m_Texture;

    CGSize m_Size;
    CGMatrix4x4 m_Transform;
    CMetalPtr<CMetalBuffer> m_TransformBuffer;
};
