/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

#include "AAPLView.h"

void AAPLView::SetupRenderPassDescriptorForTexture(CMetalTexture* texture)
{
    m_RenderPassDescriptor.m_ColorAttachments[0].m_Texture = texture;
    m_RenderPassDescriptor.m_ColorAttachments[0].m_LoadAction = CMetalRenderPassAttachmentColorDescriptor::LOAD_ACTION_CLEAR;
    m_RenderPassDescriptor.m_ColorAttachments[0].m_ClearColor = CMetalRenderPassAttachmentColorDescriptor::ClearColor(0.65, 0.65, 0.65, 1);

    if (m_SampleCount > 1)
    {
        bool doUpdate = !m_MSAATex || (m_MSAATex->Width() != texture->Width()) ||
            (m_MSAATex->Height() != texture->Height() || (m_MSAATex->SampleCount() != m_SampleCount));
        if (!m_MSAATex || (m_MSAATex && doUpdate))
        {
            CMetalTextureDescriptor desc(CMetalTextureDescriptor::BGRA8Unorm, texture->Width(), texture->Height(), false);
            desc.m_TextureType = CMetalTextureDescriptor::TYPE_2D_MULTISAMPLE;
            desc.m_SampleCount = m_SampleCount;

            m_MSAATex = m_Device->NewTexture(desc);
        }

        m_RenderPassDescriptor.m_ColorAttachments[0].m_Texture = m_MSAATex;
        m_RenderPassDescriptor.m_ColorAttachments[0].m_ResolveTexture = texture;
        m_RenderPassDescriptor.m_ColorAttachments[0].m_StoreAction = CMetalRenderPassAttachmentColorDescriptor::STORE_ACTION_MULTISAMPLE_RESOLVE;
    } else {
        m_RenderPassDescriptor.m_ColorAttachments[0].m_StoreAction = CMetalRenderPassAttachmentColorDescriptor::STORE_ACTION_STORE;
    }

    if (m_DepthPixelFormat != CMetalTextureDescriptor::Invalid)
    {
        bool doUpdate = !m_DepthTex || (m_DepthTex->Width() != texture->Width()) ||
            (m_DepthTex->Height() != texture->Height() || (m_DepthTex->SampleCount() != m_SampleCount));

        if (doUpdate)
        {
            CMetalTextureDescriptor desc(m_DepthPixelFormat, texture->Width(), texture->Height(), false);
            desc.m_TextureType = (m_SampleCount > 1) ? CMetalTextureDescriptor::TYPE_2D_MULTISAMPLE : CMetalTextureDescriptor::TYPE_2D;
            desc.m_SampleCount = m_SampleCount;

            m_DepthTex = m_Device->NewTexture(desc);

            m_RenderPassDescriptor.m_DepthAttachment.m_Texture = m_DepthTex;
            m_RenderPassDescriptor.m_DepthAttachment.m_LoadAction = CMetalRenderPassAttachmentColorDescriptor::LOAD_ACTION_CLEAR;
            m_RenderPassDescriptor.m_DepthAttachment.m_StoreAction = CMetalRenderPassAttachmentColorDescriptor::STORE_ACTION_DONT_CARE;
            m_RenderPassDescriptor.m_DepthAttachment.m_ClearDepth = 1.0;
        }
    }

    if (m_StencilPixelFormat != CMetalTextureDescriptor::Invalid)
    {
        bool doUpdate = !m_StencilTex || (m_StencilTex->Width() != texture->Width()) ||
            (m_StencilTex->Height() != texture->Height() || (m_StencilTex->SampleCount() != m_SampleCount));

        if (doUpdate)
        {
            CMetalTextureDescriptor desc(m_StencilPixelFormat, texture->Width(), texture->Height(), false);
            desc.m_TextureType = (m_SampleCount > 1) ? CMetalTextureDescriptor::TYPE_2D_MULTISAMPLE : CMetalTextureDescriptor::TYPE_2D;
            desc.m_SampleCount = m_SampleCount;

            m_StencilTex = m_Device->NewTexture(desc);

            m_RenderPassDescriptor.m_StencilAttachment.m_Texture = m_StencilTex;
            m_RenderPassDescriptor.m_StencilAttachment.m_LoadAction = CMetalRenderPassAttachmentColorDescriptor::LOAD_ACTION_CLEAR;
            m_RenderPassDescriptor.m_StencilAttachment.m_StoreAction = CMetalRenderPassAttachmentColorDescriptor::STORE_ACTION_DONT_CARE;
            m_RenderPassDescriptor.m_StencilAttachment.m_ClearStencil = 0;
        }
    }
}

CMetalRenderPassDescriptor& AAPLView::RenderPassDescriptor()
{
    CMetalDrawable* drawable = CurrentDrawable();
    SetupRenderPassDescriptorForTexture(drawable->Texture());
    return m_RenderPassDescriptor;
}
CMetalDrawable* AAPLView::CurrentDrawable()
{
    if (!m_Drawable)
        m_Drawable = s3eMetalNextDrawable();
    return m_Drawable.get();
}

void AAPLView::Display()
{
    m_Drawable = NULL;
}
