/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
#pragma once

#include "s3e.h"
#include "s3eMetal.h"
#include <math.h>
#include <stdio.h>

class AAPLTexture
{
public:
    AAPLTexture();
    ~AAPLTexture();

    bool Finalize(CMetalDevice* device);

public:
    CMetalPtr<CMetalTexture> m_Texture;
    unsigned int m_Width;
    unsigned int m_Height;
    CMetalTextureDescriptor::PixelFormat m_Format;
    unsigned int* m_Buffer;
};
