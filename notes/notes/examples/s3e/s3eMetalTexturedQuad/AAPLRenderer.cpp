/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

#include "AAPLRenderer.h"

static const float kUIInterfaceOrientationLandscapeAngle = 35.0f;
static const float kUIInterfaceOrientationPortraitAngle  = 50.0f;

static const float kPrespectiveNear = 0.1f;
static const float kPrespectiveFar  = 100.0f;

static const uint32_t kSzSIMDFloat4x4         = sizeof(CGMatrix4x4);
static const uint32_t kSzBufferLimitsPerFrame = kSzSIMDFloat4x4;

static const uint32_t kInFlightCommandBuffers = 3;

bool AAPLRenderer::LoadShaders()
{
    CMetalError* error = NULL;
    m_ShaderLibrary = m_Device->NewLibrary(
        "#include <metal_graphics>\n"
        "#include <metal_matrix>\n"
        "#include <metal_geometric>\n"
        "#include <metal_math>\n"
        "#include <metal_texture>\n"
        "\n"
        "using namespace metal;\n"
        "\n"
        "struct VertexInOut\n"
        "{\n"
        "   float4 m_Position [[position]];\n"
        "   float2 m_TexCoord [[user(texturecoord)]];\n"
        "};\n"
        "\n"
        "vertex VertexInOut texturedQuadVertex(\n"
        "   constant float4         *pPosition   [[ buffer(0) ]],\n"
        "   constant packed_float2  *pTexCoords  [[ buffer(1) ]],\n"
        "   constant float4x4       *pMVP        [[ buffer(2) ]],\n"
        "   uint                     vid         [[ vertex_id ]])\n"
        "{\n"
        "   VertexInOut outVertices;\n"
        "\n"
        "    outVertices.m_Position = *pMVP * pPosition[vid];\n"
        "   outVertices.m_TexCoord = pTexCoords[vid];\n"
        "\n"
        "   return outVertices;\n"
        "}\n"
        "\n"
        "fragment half4 texturedQuadFragment(\n"
        "   VertexInOut     inFrag    [[ stage_in ]],\n"
        "   texture2d<half>  tex2D     [[ texture(0) ]])\n"
        "{\n"
        "   constexpr sampler quad_sampler;\n"
        "   half4 color = tex2D.sample(quad_sampler, inFrag.m_TexCoord);\n"
        "\n"
        "   return color;\n"
        "}\n", CMetalCompileOptions(), &error);

    if (error != NULL)
        return false;
    if (!m_ShaderLibrary)
        return false;
    return true;
}

AAPLRenderer::AAPLRenderer()
{
    m_SampleCount = 1;
    m_DepthPixelFormat = CMetalTextureDescriptor::Depth32Float;
    m_StencilPixelFormat = CMetalTextureDescriptor::Invalid;
    m_ConstantDataBufferIndex = 0;

    m_Device = s3eMetalGetDevice(true);
    if (!m_Device)
        return;

    m_CommandQueue = m_Device->NewCommandQueue();

    LoadShaders();
}

bool AAPLRenderer::Configure(AAPLView* view)
{
    view->m_DepthPixelFormat = m_DepthPixelFormat;
    view->m_StencilPixelFormat = m_StencilPixelFormat;
    view->m_SampleCount = m_SampleCount;

    if (!PreparePipelineState())
        return false;
    if (!PrepareTexturedQuad())
        return false;
    if (!PrepareDepthStencilState())
        return false;
    if (!PrepareTransformBuffer())
        return false;

    PrepareTransforms();
    return true;
}

bool AAPLRenderer::PreparePipelineState()
{
    CMetalError* error = NULL;
    CMetalFunction* fragmentProgram = m_ShaderLibrary->NewFunction("texturedQuadFragment");
    CMetalFunction* vertexProgram = m_ShaderLibrary->NewFunction("texturedQuadVertex");

    CMetalRenderPipelineDescriptor desc;
    desc.m_DepthAttachmentPixelFormat = m_DepthPixelFormat;
    desc.m_StencilAttachmentPixelFormat = m_StencilPixelFormat;
    desc.m_ColourAttachments[0].m_PixelFormat = CMetalTextureDescriptor::BGRA8Unorm;

    desc.m_SampleCount = m_SampleCount;
    desc.m_VertexFunction = vertexProgram;
    desc.m_FragmentFunction = fragmentProgram;

    m_PipelineState = m_Device->NewRenderPipelineState(desc, &error);

    return m_PipelineState;
}
bool AAPLRenderer::PrepareTexturedQuad()
{
    m_Texture = new AAPLTexture;

    if (!m_Texture->Finalize(m_Device.get()))
        return false;

    m_Size.width = m_Texture->m_Width;
    m_Size.height = m_Texture->m_Height;

    m_Quad = new AAPLQuad(m_Device.get());
    m_Quad->m_Size = m_Size;

    return true;
}
bool AAPLRenderer::PrepareDepthStencilState()
{
    CMetalDepthStencilDescriptor desc;

    desc.m_DepthCompareFunction = CMetalStencilDescriptor::COMPARE_ALWAYS;
    desc.m_DepthWriteEnabled = true;

    m_DepthState = m_Device->NewDepthStencilState(desc);

    return m_DepthState;
}
bool AAPLRenderer::PrepareTransformBuffer()
{
    m_TransformBuffer = m_Device->NewBuffer(kSzBufferLimitsPerFrame, CMetalBuffer::OPTION_CPU_CACHE_MODE_DEFAULT);
    return m_TransformBuffer;
}
void AAPLRenderer::PrepareTransforms()
{
    m_Transform.m00 = 1; m_Transform.m01 = 0; m_Transform.m02 = 0; m_Transform.m03 = 0;
    m_Transform.m10 = 0; m_Transform.m11 = 1; m_Transform.m12 = 0; m_Transform.m13 = 0;
    m_Transform.m20 = 0; m_Transform.m21 = 0; m_Transform.m22 = 1; m_Transform.m23 = 0;
    m_Transform.m30 = 0; m_Transform.m31 = -0.25f; m_Transform.m32 = 2; m_Transform.m33 = 1;
}

void AAPLRenderer::Encode(CMetalRenderCommandEncoder* encoder)
{
    encoder->PushDebugGroup("encode quad");
    encoder->SetFrontFaceWinding(CMetalRenderCommandEncoder::WINDING_COUNTER_CLOCKWISE);
    encoder->SetDepthStencilState(m_DepthState.get());

    encoder->SetRenderPipelineState(m_PipelineState.get());

    encoder->SetVertexBuffer(m_TransformBuffer.get(), 0, 2);

    encoder->SetFragmentTexture(m_Texture->m_Texture.get(), 0);

    m_Quad->Encode(encoder);

    encoder->DrawPrimitives(CMetalRenderCommandEncoder::PRIMITIVE_TRIANGLE, 0, 6, 1);

    encoder->EndEncoding();
    encoder->PopDebugGroup();
}

CGMatrix4x4 frustum_oc(float left, float right, float bottom, float top, float near, float far)
{
    float sWidth  = 1.0f / (right - left);
    float sHeight = 1.0f / (top   - bottom);
    float sDepth  = far  / (far   - near);
    float dNear   = 2.0f * near;

    CGMatrix4x4 m;

    m.m00 = dNear * sWidth;
    m.m01 = 0;
    m.m02 = 0;
    m.m03 = 0;

    m.m10 = 0;
    m.m11 = dNear * sHeight;
    m.m12 = 0;
    m.m13 = 0;

    m.m20 = -sWidth  * (right + left);
    m.m21 = -sHeight * (top   + bottom);
    m.m22 = sDepth;
    m.m23 = 1;

    m.m30 = 0;
    m.m31 = 0;
    m.m32 = -sDepth * near;
    m.m33 = 0;

    return m;
} // frustum_oc

CGMatrix4x4 multiply(CGMatrix4x4 a, CGMatrix4x4 b)
{
    CGMatrix4x4 m;

    m.m00 = a.m00*b.m00 + a.m01*b.m10 + a.m02*b.m20 + a.m03*b.m30;
    m.m01 = a.m00*b.m01 + a.m01*b.m11 + a.m02*b.m21 + a.m03*b.m31;
    m.m02 = a.m00*b.m02 + a.m01*b.m12 + a.m02*b.m22 + a.m03*b.m32;
    m.m03 = a.m00*b.m03 + a.m01*b.m13 + a.m02*b.m23 + a.m03*b.m33;

    m.m10 = a.m10*b.m00 + a.m11*b.m10 + a.m12*b.m20 + a.m13*b.m30;
    m.m11 = a.m10*b.m01 + a.m11*b.m11 + a.m12*b.m21 + a.m13*b.m31;
    m.m12 = a.m10*b.m02 + a.m11*b.m12 + a.m12*b.m22 + a.m13*b.m32;
    m.m13 = a.m10*b.m03 + a.m11*b.m13 + a.m12*b.m23 + a.m13*b.m33;

    m.m20 = a.m20*b.m00 + a.m21*b.m10 + a.m22*b.m20 + a.m23*b.m30;
    m.m21 = a.m20*b.m01 + a.m21*b.m11 + a.m22*b.m21 + a.m23*b.m31;
    m.m22 = a.m20*b.m02 + a.m21*b.m12 + a.m22*b.m22 + a.m23*b.m32;
    m.m23 = a.m20*b.m03 + a.m21*b.m13 + a.m22*b.m23 + a.m23*b.m33;

    m.m30 = a.m30*b.m00 + a.m31*b.m10 + a.m32*b.m20 + a.m33*b.m30;
    m.m31 = a.m30*b.m01 + a.m31*b.m11 + a.m32*b.m21 + a.m33*b.m31;
    m.m32 = a.m30*b.m02 + a.m31*b.m12 + a.m32*b.m22 + a.m33*b.m32;
    m.m33 = a.m30*b.m03 + a.m31*b.m13 + a.m32*b.m23 + a.m33*b.m33;

    return m;
}

void AAPLRenderer::ReShape(int x, int y)
{
    if (m_Quad->m_Bounds.size.width != x || m_Quad->m_Bounds.size.height != y)
    {
        m_Quad->SetBounds(CGRect(0, 0, x, y));

        float dangle = 0;
        if (x > y)
            dangle = kUIInterfaceOrientationLandscapeAngle;
        else
            dangle = kUIInterfaceOrientationPortraitAngle;

        const float near = kPrespectiveNear;
        const float far = kPrespectiveFar;
        const float rangle = dangle * (float)M_PI / 180.0f;
        const float length = near * tanf(rangle);

        float right = length / m_Quad->m_Aspect;
        float left = - right;
        float top = length;
        float bottom = -top;

        CGMatrix4x4 persp = frustum_oc(left, right, bottom, top, near, far);
        CGMatrix4x4 transform = multiply(m_Transform, persp);

        float* pTransform = (float*)m_TransformBuffer->Contents();
        memcpy(pTransform, &transform, kSzSIMDFloat4x4);
    }
}

void AAPLRenderer::Render(AAPLView* view)
{
    CMetalPtr<CMetalCommandBuffer> commandBuffer = m_CommandQueue->CommandBuffer();

    CMetalRenderPassDescriptor& desc = view->RenderPassDescriptor();
    CMetalPtr<CMetalRenderCommandEncoder> encoder = commandBuffer->RenderCommandEncoder(desc);

    Encode(encoder.get());
    commandBuffer->PresentDrawable(view->CurrentDrawable());
    commandBuffer->Commit();

    view->Display();
}
