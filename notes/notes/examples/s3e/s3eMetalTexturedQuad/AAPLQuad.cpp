/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

#include "AAPLQuad.h"

static const uint32_t kCntQuadTexCoords = 6;
static const uint32_t kSzQuadTexCoords  = kCntQuadTexCoords * sizeof(CGPoint);

static const uint32_t kCntQuadVertices = kCntQuadTexCoords;
static const uint32_t kSzQuadVertices  = kCntQuadVertices * sizeof(CGVector4);

static const CGVector4 kQuadVertices[kCntQuadVertices] =
{
    { -1.0f,  -1.0f, 0.0f, 1.0f },
    {  1.0f,  -1.0f, 0.0f, 1.0f },
    { -1.0f,   1.0f, 0.0f, 1.0f },

    {  1.0f,  -1.0f, 0.0f, 1.0f },
    { -1.0f,   1.0f, 0.0f, 1.0f },
    {  1.0f,   1.0f, 0.0f, 1.0f }
};
static const CGVector2 kQuadTexCoords[kCntQuadTexCoords] =
{
    { 0.0f, 0.0f },
    { 1.0f, 0.0f },
    { 0.0f, 1.0f },

    { 1.0f, 0.0f },
    { 0.0f, 1.0f },
    { 1.0f, 1.0f }
};

AAPLQuad::AAPLQuad(CMetalDevice* device)
{
    m_VertexBuffer = device->NewBuffer((const void*)kQuadVertices, (unsigned int)kSzQuadVertices, CMetalBuffer::OPTION_CPU_CACHE_MODE_DEFAULT);
    if(!m_VertexBuffer)
        return;

    m_TexCoordBuffer = device->NewBuffer((const void*)kQuadTexCoords, (unsigned int)kSzQuadTexCoords, CMetalBuffer::OPTION_CPU_CACHE_MODE_DEFAULT);
    if(!m_TexCoordBuffer)
        return;

    m_VertexIndex   = 0;
    m_TexCoordIndex = 1;
    m_SamplerIndex  = 0;

    m_Size   = CGSize(0.0, 0.0);
    m_Bounds = CGRect(0.0, 0.0, 0.0, 0.0);
    m_Aspect = 1.0f;
    m_Scale = CGPoint(1.0f, 1.0f);
}
void AAPLQuad::SetBounds(CGRect bounds)
{
    m_Bounds = bounds;
    m_Aspect = fabsf(bounds.size.width / bounds.size.height);

    float aspect = 1.0f/m_Aspect;
    CGPoint scale;

    scale.x = aspect * m_Size.width / m_Bounds.size.width;
    scale.y = m_Size.height / m_Bounds.size.height;

    bool bNewScale = (scale.x != m_Scale.x) || (scale.y != m_Scale.y);

    if (bNewScale)
    {
        m_Scale = scale;

        CGVector4* pVertices = (CGVector4*)m_VertexBuffer->Contents();

        if (pVertices != NULL)
        {
            pVertices[0].x = -m_Scale.x;
            pVertices[0].y = -m_Scale.y;
            pVertices[1].x =  m_Scale.x;
            pVertices[1].y = -m_Scale.y;
            pVertices[2].x = -m_Scale.x;
            pVertices[2].y =  m_Scale.y;

            pVertices[3].x =  m_Scale.x;
            pVertices[3].y = -m_Scale.y;
            pVertices[4].x = -m_Scale.x;
            pVertices[4].y =  m_Scale.y;
            pVertices[5].x =  m_Scale.x;
            pVertices[5].y =  m_Scale.y;
        }
    }
}
void AAPLQuad::Encode(CMetalRenderCommandEncoder* encoder)
{
    encoder->SetVertexBuffer(m_VertexBuffer.get(), 0, m_VertexIndex);
    encoder->SetVertexBuffer(m_TexCoordBuffer.get(), 0, m_TexCoordIndex);
}
