/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
#pragma once

#include "s3e.h"
#include "s3eMetal.h"
#include <math.h>
#include <stdio.h>

class AAPLView
{
public:
    AAPLView(CMetalDevice* device) : m_Device(device) {}

    void Display();
    void SetupRenderPassDescriptorForTexture(CMetalTexture* texture);
    CMetalRenderPassDescriptor& RenderPassDescriptor();
    CMetalDrawable* CurrentDrawable();

public:
    CMetalDevice* m_Device;

    CMetalPtr<CMetalDrawable> m_Drawable;

    CMetalRenderPassDescriptor m_RenderPassDescriptor;

    CMetalTextureDescriptor::PixelFormat m_DepthPixelFormat;
    CMetalTextureDescriptor::PixelFormat m_StencilPixelFormat;
    unsigned int m_SampleCount;
private:
    bool m_LayerSizeDidUpdate;
    CMetalPtr<CMetalTexture> m_DepthTex;
    CMetalPtr<CMetalTexture> m_StencilTex;
    CMetalPtr<CMetalTexture> m_MSAATex;
};
