/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
#pragma once

#include "s3e.h"
#include "s3eMetal.h"
#include <math.h>
#include <stdio.h>

struct CGSize
{
    float width;
    float height;

    CGSize(float w, float h) : width(w), height(h) {}
    CGSize() : width(0), height(0) {}
};
struct CGPoint
{
    float x;
    float y;

    CGPoint(float _x, float _y) : x(_x), y(_y) {}
    CGPoint() : x(0), y(0) {}
};
struct CGVector4
{
    float x;
    float y;
    float z;
    float w;
};
struct CGMatrix4x4
{
    float m00;
    float m01;
    float m02;
    float m03;
    float m10;
    float m11;
    float m12;
    float m13;
    float m20;
    float m21;
    float m22;
    float m23;
    float m30;
    float m31;
    float m32;
    float m33;
};
struct CGVector2
{
    float x;
    float y;
};
struct CGRect
{
    CGPoint origin;
    CGSize size;

    CGRect(float x, float y, float w, float h) : origin(x, y), size(w, h) {}
    CGRect() {}
};

class AAPLQuad
{
public:
    AAPLQuad(CMetalDevice* device);
    void SetBounds(CGRect bounds);
    void Encode(CMetalRenderCommandEncoder* encoder);

public:
    int m_VertexIndex;
    int m_TexCoordIndex;
    int m_SamplerIndex;

    CGSize m_Size;
    CGRect m_Bounds;
    float m_Aspect;
private:
    CMetalPtr<CMetalBuffer>  m_VertexBuffer;
    CMetalPtr<CMetalBuffer>  m_TexCoordBuffer;
    CGPoint m_Scale;
};
