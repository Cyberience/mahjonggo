/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

#include "AAPLTexture.h"

AAPLTexture::AAPLTexture()
{
    m_Width = 800;
    m_Height = 533;
    m_Format = CMetalTextureDescriptor::RGBA8Unorm;

    m_Buffer = new unsigned int[m_Width * m_Height];
    for (int j=0; j<m_Height; j++)
        for (int i=0; i<m_Width; i++)
            m_Buffer[j*m_Width + i] = 0xff0000ff | ((i*255/m_Width) << 16) | ((j*255/m_Height) << 8);
}

AAPLTexture::~AAPLTexture()
{
    delete[] m_Buffer;
}

bool AAPLTexture::Finalize(CMetalDevice* device)
{
    CMetalTextureDescriptor desc(m_Format, m_Width, m_Height, false);
    m_Texture = device->NewTexture(desc);

    CMetalRegion region = CMetalRegion(0, 0, m_Width, m_Height);
    m_Texture->ReplaceRegion(region, 0, m_Buffer, m_Width * sizeof(unsigned int));

    return m_Texture;
}
