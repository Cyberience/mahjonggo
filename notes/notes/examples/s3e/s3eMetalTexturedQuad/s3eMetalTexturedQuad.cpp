/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EMetalTexturedQuad S3E Metal Textured Quad Example
 *
 * This example is a straight port from the iOS example MetalTexturedQuad:
 * https://developer.apple.com/library/ios/samplecode/MetalTexturedQuad/Introduction/Intro.html
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eMetalTexturedQuadImage.png
 *
 * Please look at examples/s3e/s3eMetalTexturedQuad in the SDK for the code of this example
 */

#include "AAPLView.h"
#include "AAPLRenderer.h"

AAPLView* g_View;
AAPLRenderer* g_Renderer;

int32 SizeChangedCallback(void* systemData, void* userData)
{
    int x = s3eSurfaceGetInt(S3E_SURFACE_WIDTH);
    int y = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT);
    g_Renderer->ReShape(x, y);

    return S3E_RESULT_SUCCESS;
}

bool init()
{
    g_Renderer = new AAPLRenderer;
    g_View = new AAPLView(g_Renderer->m_Device.get());

    s3eSurfaceRegister(S3E_SURFACE_SCREENSIZE, SizeChangedCallback, NULL);

    if (!g_Renderer->Configure(g_View))
        return false;

    SizeChangedCallback(NULL, NULL);
    return true;
}

void render()
{
    g_Renderer->Render(g_View);
}
void Shutdown()
{
    s3eSurfaceUnRegister(S3E_SURFACE_SCREENSIZE, SizeChangedCallback);

    delete g_View;
    delete g_Renderer;
}

int main()
{
    bool quit = false;

    if (!init())
        return -1;

    int numFrames = 0;

    // Frame loop
    while (!quit) {
        if (s3eDeviceCheckQuitRequest())
            quit = 1;

        render();

        s3eKeyboardUpdate();
        s3eDeviceYield(0);
        numFrames++;
    }

    Shutdown();
    return 0;
}
