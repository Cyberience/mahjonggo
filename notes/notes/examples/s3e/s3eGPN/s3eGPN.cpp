//
//  CrossPromotionApp.cpp
//  CrossPromotionApp
//
//  Copyright 2015 GameHouse, a division of RealNetworks, Inc.
// 
//  The GameHouse Promotion Network SDK is licensed under the Apache License, 
//  Version 2.0 (the "License"); you may not use this file except in compliance 
//  with the License. You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

#include "IwUI.h"

#include "gpn_helper.h"

#include "ExamplesMain.h"

CIwUIButton* s_RequestButton = NULL;
CIwUIButton* s_PresentButton = NULL;
CIwUILabel*  s_StatusLabel = NULL;

typedef void (*t_buttonHandler)(void);

static const char * const kGpnUnavailableMessage = "GPN is not available";
static const s3eBool kDebugMode = false;

static void SetStatus(const char* status);

//-----------------------------------------------------------------------------
class MyButtonEventHandler : public CIwUIElementEventHandler
{
private:
    t_buttonHandler m_handler;
    
public:
    MyButtonEventHandler(t_buttonHandler handler)
        : m_handler(handler)
    {
    }
    
public:
    //HandleEvent is passed up the element hierarchy
    virtual bool HandleEvent(CIwEvent* pEvent)
    {
        if (pEvent->GetID() == IWUI_EVENT_BUTTON)
        {
            m_handler();
            return true;
        }
        
        return false;
    }
    
    //FilterEvent is passed down the element hierarchy
    virtual bool FilterEvent(CIwEvent* pEvent)
    {
        //return false, since filtering is not necessary
        return false;
    }
};

void requestButtonClick()
{
    if (s3eGPNAvailable())
    {
        if (gpnHelperStartRequest())
        {
            SetStatus("Requesting interstitial...");
        }
        else
        {
            SetStatus("Can't request interstitial");
        }
    }
    else
    {
        SetStatus(kGpnUnavailableMessage);
    }
}

void presentButtonClick()
{
    if (s3eGPNAvailable())
    {
        const char* paramStr = NULL; // "position=startup";
        
        s3eGPNInterstitialResult result = gpnHelperPresent(paramStr);
        if (S3E_GPN_INTERSTITIAL_RESULT_PRESENTED != result)
        {
            SetStatus("Not presented");
        }
    }
    else
    {
        SetStatus(kGpnUnavailableMessage);
    }
}

//-----------------------------------------------------------------------------

void gpnOnReceived()
{
    SetStatus("Interstitial received");
}

void gpnOnFailed()
{
    SetStatus("Interstitial failed");
}

void gpnOnOpened()
{
    SetStatus("");
}

void gpnOnClosed()
{
    SetStatus("Waiting...");
}

void gpnOnKilledLowMemory()
{
    SetStatus("Killed low memory");
}

//-----------------------------------------------------------------------------
void ExampleInit()
{
    //Initialise the IwUI module
    IwUIInit();
    
    //Instantiate the view and controller singletons.
    //IwUI will not instantiate these itself, since they can be subclassed to add functionality.
    new CIwUIController;
    new CIwUIView;
    
    //Load group which contains the ui specifications
    IwGetResManager()->LoadGroup("IwUIBasicLayoutVertical.group");
    
    //Load root element
    CIwUIElement* pRoot =  CIwUIElement::CreateFromResource("RootElement");
    
    //Add the root element and therefore the whole element hierarchy to the UIView singleton
    IwGetUIView()->AddElementToLayout(pRoot);
    
    s_RequestButton = (CIwUIButton*) pRoot->GetChildNamed("RequestButton");
    s_PresentButton = (CIwUIButton*) pRoot->GetChildNamed("PresentButton");
    s_StatusLabel = (CIwUILabel*) pRoot->GetChildNamed("StatusLabel");

    //Attach eventhandler to button
    s_RequestButton->AddEventHandler(new MyButtonEventHandler(requestButtonClick));
    s_PresentButton->AddEventHandler(new MyButtonEventHandler(presentButtonClick));
    
    //Set initial status
    s_StatusLabel->SetCaption("");

    if (s3eGPNAvailable())
    {
        const char* appIDs[S3E_GPN_APP_ID_INDEX_MAX] = { NULL };
        appIDs[S3E_GPN_APP_ID_INDEX_IOS]       = "0"; // your iOS app ID here: may remove the line is you don't target iOS
        appIDs[S3E_GPN_APP_ID_INDEX_ANDROID]   = "0"; // your Android app ID here: : may remove the line is you don't target Android
        
        gpnHelperInit(appIDs, kDebugMode);
        
        // cpSetShouldKillOnLowMemory(false);     // uncomment this line if you don't want the interstitial to be killed on a low memory warning
    }
    else
    {
        SetStatus(kGpnUnavailableMessage);
    }
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    gpnHelperDestroy();
    
    delete IwGetUIController();
    delete IwGetUIView();
    
    //Terminate the IwUI module
    IwUITerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    //Update the controller (this will generate control events etc.)
    IwGetUIController()->Update();
    
    //Update the view (this will do animations etc.) The example framework has
    //a fixed framerate of 20fps, so we pass that duration to the update function.
    IwGetUIView()->Update(1000/20);
    
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    //Render the UI
    IwGetUIView()->Render();
    
    //Flush IwGx
    IwGxFlush();
    
    //Display the rendered frame
    IwGxSwapBuffers();
}
//-----------------------------------------------------------------------------
void SetStatus(const char* status)
{
    if (s_StatusLabel != NULL)
    {
        s_StatusLabel->SetCaption(status);
    }
}
