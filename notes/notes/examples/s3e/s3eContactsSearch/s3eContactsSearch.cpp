/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EContactsSearch S3E Contacts Search Example
 *
 * This example demonstrates the SimpleSearch functionality of s3eContacts.
 * and also NUI usage
 *
 * The example provides an interactive interface to s3eContactsSimpleSearch().
 * User can enter substring to be searched and select contact record fields to be
 * searched and gets a list of contacts matching criteria.
 *
 * @image html s3eContactsSearchUIImage.png
 * @image html s3eContactsSearchiOSImage.png
 * @image html s3eContactsSearchAndroidImage.png
 *
 * @include s3eContactsSearch.cpp
 */

#include "IwNUI.h"

#include "s3e.h"
#include "s3eContacts.h"
#include <string>
#include <map>
#include <vector>

struct FieldLink
{
    s3eContactsField m_FieldID;
    bool             m_Checked;
};

using namespace IwNUI;

const CColour COLOUR_PAGE (0xffececec);
const CColour COLOUR_BLACK(0xff000000);
const CColour COLOUR_WHITE(0xffFFFFFF);

class Handler
{
private:
    typedef std::map<CCheckBox*, FieldLink> FieldsMap;
    FieldsMap   m_FieldsMap;
    std::string m_SearchPattern;

    CAppPtr         m_App;
    CViewPtr        m_SearchView;
    CViewPtr        m_ContactsView;
    CTablePtr       m_ContactsTable;
    CAlertDialogPtr m_AlertDialog;

    CViewPtr CreateSearchView();
    CViewPtr CreateContactsView();
    bool     UpdateContacts();
    void     AddContactLine(const char* line);
    void     ShowMessage(const char* title, const char* message);

    bool onSearchTextChanged(CTextField* field, const char* str)
    {
        m_SearchPattern = str;
        return true;
    }

    bool onFieldChecked(CCheckBox* pCheck, bool checked)
    {
        m_FieldsMap[pCheck].m_Checked = checked;
        return true;
    }

    bool onSearch(CButton* button)
    {
        if (UpdateContacts())
        {
            m_SearchView->SetAttribute("visible", false);
            m_ContactsView->SetAttribute("visible", true);
        }
        return true;
    }

    bool onClose(CButton* button)
    {
        m_ContactsView->SetAttribute("visible", false);
        m_SearchView->SetAttribute("visible", true);

        while (m_ContactsTable->GetNumChildren() > 0)
        {
            m_ContactsTable->RemoveChild(m_ContactsTable->GetChild(0));
        }

        return true;
    }

    bool onViewClick(CView* view)
    {
        return m_App->DeactivateInput();
    }

public:

    Handler(CAppPtr app, CWindowPtr window)
    {
        m_App = app;
        m_SearchView = CreateSearchView();
        m_ContactsView = CreateContactsView();
        m_ContactsView->SetAttribute("visible", false);
        CViewPtr containerView = CreateView();
        containerView->AddChild(m_SearchView);
        containerView->AddChild(m_ContactsView);
        window->SetChild(containerView);
        m_AlertDialog = CreateAlertDialog(CAttributes()
            .Set("name",                  "m_AlertDialog")
            .Set("positiveButtonCaption", "OK")
            );
    }
};

static
CViewPtr CreatePageView()
{
    return CreateView(CAttributes()
        .Set("x1",      0)
        .Set("y1",      0)
        .Set("width",   "100%")
        .Set("height",  "100%")
        .Set("backgroundColour", COLOUR_PAGE)
        );
}

void Handler::ShowMessage(const char* title, const char* message)
{
    m_AlertDialog->SetAttribute("message", message);
    m_AlertDialog->SetAttribute("title", title);
    m_AlertDialog->Show();
}

void Handler::AddContactLine(const char* line)
{
    CTableItemPtr tableItem = CreateTableItem(CAttributes()
        .Set("width",   "100%")
        .Set("height",  "40")
    );
    CLabelPtr label = CreateLabel(CAttributes()
        .Set("name",    "contact")
        .Set("caption", line)
        .Set("x1",      "5%")
        .Set("y1",      0)
        .Set("width",   "90%")
        .Set("height",  "100%")
        );
    tableItem->AddChild(label);
    m_ContactsTable->AddChild(tableItem);
}

bool Handler::UpdateContacts()
{
    std::vector<s3eContactsField> fields;
    for (FieldsMap::const_iterator i = m_FieldsMap.begin(); i != m_FieldsMap.end(); ++i)
    {
        if (i->second.m_Checked)
        {
            fields.push_back(i->second.m_FieldID);
        }
    }
    if (fields.size() == 0)
    {
        ShowMessage("Select search fields", "no fields selected");
        return false;
    }

    if (s3eContactsSimpleSearch(m_SearchPattern.c_str(), (s3eContactsField*)fields.begin(), fields.size()) != S3E_RESULT_SUCCESS)
    {
        char errorBuf[128];
        snprintf(errorBuf, sizeof(errorBuf), "error code is: %d", s3eContactsGetError());
        ShowMessage("Error occurred", errorBuf);
        return false;
    }

    int cntFound = s3eContactsGetNumRecords();
    for (int i = 0; i < cntFound; ++i)
    {
        int32 UID = s3eContactsGetUID(i);
        char lastName[S3E_CONTACTS_STRING_MAX] = { 0 };
        char firstName[S3E_CONTACTS_STRING_MAX] = { 0 };
        char output[S3E_CONTACTS_STRING_MAX] = { 0 };
        s3eContactsGetField(UID, S3E_CONTACTS_FIELD_LAST_NAME, lastName, sizeof(lastName));
        s3eContactsGetField(UID, S3E_CONTACTS_FIELD_FIRST_NAME, firstName, sizeof(firstName));
        snprintf(output, sizeof(output), "%d: %s, %s", (int)UID, lastName[0] != '\0' ? lastName : "<no name>",
                                                                 firstName[0] != '\0' ? firstName : "<no name>");
        output[S3E_CONTACTS_STRING_MAX - 1] = '\0';
        AddContactLine(output);
    }
    if (cntFound == 0)
    {
        AddContactLine("no records found");
    }
    return true;
}

CViewPtr Handler::CreateSearchView()
{
    CViewPtr view = CreatePageView();

    CLabelPtr entryLabel = CreateLabel(CAttributes()
        .Set("name",       "entryLabel")
        .Set("x1",         "5%")
        .Set("y1",         "0%")
        .Set("width",      "35%")
        .Set("height",     "10%")
        .Set("caption",    "search entry")
        .Set("fontSize",   "small")
        .Set("fontStyle",  "bold")
        );
    CTextFieldPtr searchEntry = CreateTextField(CAttributes()
        .Set("name",    "searchEntry")
        .Set("x1",      "45%")
        .Set("y1",      "2%")
        .Set("width",   "50%")
        .Set("height",  "6%")
        .Set("fontStyle",  "bold")
        .Set("fontSize",   "large")
        .Set("fontColour",  COLOUR_BLACK)
        .Set("backgroundColour", COLOUR_WHITE)
        );
    CButtonPtr buttonSearch = CreateButton(CAttributes()
        .Set("name",    "searchbutton")
        .Set("caption", "Search")
        .Set("x1",      "5%")
        .Set("y1",      "10%")
        .Set("width",   "90%")
        .Set("height",  "10%")
        );
    CTablePtr fieldsTable = CreateTable(CAttributes()
        .Set("name",    "fieldsTable")
        .Set("x1",      0)
        .Set("y1",      "20%")
        .Set("width",   "100%")
        .Set("height",  "80%")
        );

    for (int i = 0 ; i < S3E_CONTACTS_FIELD_COUNT; i++)
    {
        s3eContactsField fieldId = (s3eContactsField)i;

        if (!s3eContactsFieldSupported(fieldId))
        {
            continue;
        }

        CTableItemPtr tableItem = CreateTableItem(CAttributes()
            .Set("width",   "100%")
            .Set("height",  "40")
        );
        CLabelPtr label = CreateLabel(CAttributes()
            .Set("name",    CString(s3eContactsGetFieldName(fieldId)))
            .Set("caption", CString(s3eContactsGetFieldName(fieldId)))
            .Set("x1",      "5%")
            .Set("y1",      0)
            .Set("width",   "70%")
            .Set("height",  "100%")
            );
        CCheckBoxPtr checkBox = CreateCheckBox(CAttributes()
            .Set("name",    "CheckBox")
            .Set("check",   false)
            .Set("x1",      "75%")
            .Set("y1",      "20%")
            .Set("width",   "25%")
            .Set("height",  "100%")
            );

        tableItem->AddChild(label);
        tableItem->AddChild(checkBox);
        fieldsTable->AddChild(tableItem);
        checkBox->SetEventHandler("checked", this, &Handler::onFieldChecked);

        FieldLink item = {fieldId, false};
        m_FieldsMap[checkBox.get()] = item;
    }

    view->AddChild(entryLabel);
    view->AddChild(searchEntry);
    view->AddChild(buttonSearch);
    view->AddChild(fieldsTable);
    view->SetEventHandler("clickview", this, &Handler::onViewClick);
    searchEntry->SetEventHandler("textchanged", this, &Handler::onSearchTextChanged);
    buttonSearch->SetEventHandler("click", this, &Handler::onSearch);

    return view;
}

CViewPtr Handler::CreateContactsView()
{
    CViewPtr view = CreatePageView();

    CButtonPtr buttonClose = CreateButton(CAttributes()
        .Set("name",    "closeButton")
        .Set("caption", "search again")
        .Set("x1",      "5%")
        .Set("y1",      "5%")
        .Set("width",   "90%")
        .Set("height",  "10%")
        );
    m_ContactsTable = CreateTable(CAttributes()
        .Set("name",    "m_ContactsTable")
        .Set("x1",      "0")
        .Set("y1",      "15%")
        .Set("width",   "100%")
        .Set("height",  "85%")
        );
    view->AddChild(buttonClose);
    view->AddChild(m_ContactsTable);
    buttonClose->SetEventHandler("click", this, &Handler::onClose);

    return view;
}

int main(int, char** argv)
{
    CAppPtr app = CreateApp();
    CWindowPtr window = CreateWindow();
    app->AddWindow(window);

    Handler handler(app, window);

    app->ShowWindow(window);
    app->Run();

    return 0;
}
