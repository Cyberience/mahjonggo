/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

#include "ExamplesMain.h"
#include "s3eDolbyAudio.h"

/**
 * @page ExampleS3EDolbyAudio S3E Dolby Audio Example
 *
 * The following example plays a music file using S3E's audio functionality.
 * It also demonstrates activating the dolby audio system
 *
 * The main functions used to achieve this are:
 * - s3eDebugPrint()
 * - s3eKeyboardGetState()
 * - s3eAudioPlay()
 * - s3eAudioPause()
 * - s3eAudioResume()
 * - s3eAudioStop()
 * - s3eAudioPlayFromBuffer()
 * - s3eFileOpen()
 * - s3eFileRead()
 * - s3eFileGetSize()
 *
 * The application waits for buttons to be pressed before using any of the
 * Audio functionality. Detection of button presses are handled using the
 * generic code in ExamplesMain.cpp
 *
 * The state of playback is output to screen using the s3eDebugPrint()
 * function.
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eAudioImage.png
 *
 * MKB File:
 * @include s3eDolbyAudio.mkb
 *
 * ICF File:
 * @include data/app.icf
 *
 * Source:
 * @include s3eDolbyAudio.cpp
 */

enum PlayerState
{
    STATE_PLAYING,
    STATE_PAUSED,
    STATE_STOPPED
};

static s3eBool g_bDolbySupported = false;
static char g_LogString[256];

static PlayerState g_Status = STATE_STOPPED;        //player status information
static Button* g_ButtonPlay = 0;
static Button* g_ButtonPlayBuf = 0;
static Button* g_ButtonPause = 0;
static Button* g_ButtonVolUp = 0;
static Button* g_ButtonVolDown = 0;
static Button* g_ButtonResume = 0;
static Button* g_ButtonStop = 0;
static s3eBool g_MP3Supported = S3E_FALSE;
static s3eDolbyAudioProfile g_DolbyProfile = MOVIE;

static int32 applicationPauseCallback(void* systemData, void* userData)
{
    sprintf(g_LogString, "On Application Paused!");
    s3eDebugOutputString(g_LogString);

    if (g_bDolbySupported)
    {
        s3eDolbyAudioSuspendSession();
    }
    return 0;
}

static int32 applicationResumeCallback(void* systemData, void* userData)
{
    sprintf(g_LogString, "On Application Resumed!");
    s3eDebugOutputString(g_LogString);

    if (g_bDolbySupported)
    {
        s3eDolbyAudioRestartSession();
    }
    return 0;
}

int32 AudioStopCallback(void* systemData, void* userData)
{
    g_Status = STATE_STOPPED;
    return 0;
}

void ExampleInit()
{
    if (S3E_TRUE == s3eDolbyAudioAvailable())
    {
        g_bDolbySupported = s3eDolbyAudioSupported();
        sprintf(g_LogString, "Dolby audio processing is supported:%d\n", g_bDolbySupported);
        s3eDebugOutputString(g_LogString);
    }
    else
    {
        g_bDolbySupported = S3E_FALSE;
    }

    s3eDeviceRegister(S3E_DEVICE_PAUSE, (s3eCallback)applicationPauseCallback, NULL);
    s3eDeviceRegister(S3E_DEVICE_UNPAUSE, (s3eCallback)applicationResumeCallback, NULL);

    g_ButtonPlay    = NewButton("Play");
    g_ButtonPlayBuf = NewButton("Set Profile");
    g_ButtonPause   = NewButton("Pause");
    g_ButtonResume  = NewButton("Resume");
    g_ButtonStop    = NewButton("Stop");
    g_ButtonVolUp   = NewButton("Volume Up");
    g_ButtonVolDown = NewButton("Volume Down");

    g_MP3Supported = s3eAudioIsCodecSupported(S3E_AUDIO_CODEC_MP3);
    s3eAudioRegister(S3E_AUDIO_STOP, AudioStopCallback, NULL);

    if (g_bDolbySupported)
    {
        s3eDolbyAudioInitialize();
        //s3eDolbyAudioSetProfile(MOVIE);
    }
}

void ExampleTerm()
{
    s3eAudioStop();

    // Un-register application pause event
    s3eDeviceUnRegister(S3E_DEVICE_PAUSE, (s3eCallback)applicationPauseCallback);

    // Un-register application resume event
    s3eDeviceUnRegister(S3E_DEVICE_UNPAUSE, (s3eCallback)applicationResumeCallback);

    if (g_bDolbySupported)
    {
        s3eDolbyAudioRelease();
    }
}

bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    if (!g_MP3Supported)
        return true;

    if (pressed == g_ButtonPlay && (g_Status == STATE_STOPPED))
    {
        if (s3eAudioPlay("test.mp3") == S3E_RESULT_SUCCESS)
            g_Status = STATE_PLAYING;
    }

    if (pressed == g_ButtonPlayBuf)
    {
        g_DolbyProfile = (s3eDolbyAudioProfile)(g_DolbyProfile + 1);
        if (g_DolbyProfile >= DOLBY_PRIVATE_PROFILE)
            g_DolbyProfile = MOVIE;

        if (g_bDolbySupported)
            s3eDolbyAudioSetProfile(g_DolbyProfile);
    }

    if (pressed == g_ButtonPause && (g_Status == STATE_PLAYING))
    {
        if (s3eAudioPause() == S3E_RESULT_SUCCESS)
            g_Status = STATE_PAUSED;
    }

    if (pressed == g_ButtonVolUp)
    {
        int vol = s3eAudioGetInt(S3E_AUDIO_VOLUME);
        s3eAudioSetInt(S3E_AUDIO_VOLUME, vol+10);
    }

    if (pressed == g_ButtonVolDown)
    {
        int vol = s3eAudioGetInt(S3E_AUDIO_VOLUME);
        s3eAudioSetInt(S3E_AUDIO_VOLUME, vol-10);
    }

    if (pressed == g_ButtonResume && (g_Status == STATE_PAUSED))
    {
        if (s3eAudioResume() == S3E_RESULT_SUCCESS)
            g_Status = STATE_PLAYING;
    }

    if (pressed == g_ButtonStop)
    {
        s3eAudioStop();
        g_Status = STATE_STOPPED;
    }

    return true;
}

/*
 * The following function display the user's prompt and any error messages.
 * The s3eDebugPrint() function to print the strings.
 */
void ExampleRender()
{
    const int textHeight = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);

    g_ButtonPlay->m_Enabled = (g_Status == STATE_STOPPED);
    g_ButtonPause->m_Enabled = (g_Status == STATE_PLAYING);
    g_ButtonResume->m_Enabled = (g_Status == STATE_PAUSED);
    g_ButtonStop->m_Enabled = (g_Status == STATE_PLAYING);

    int ypos = GetYBelowButtons() + textHeight * 2;

    if (!g_MP3Supported)
    {
        g_ButtonPlay->m_Enabled = false;
        s3eDebugPrint(10, ypos, "`x666666Device does not support mp3 playback", 0);
        return;
    }

    // Display profile
    if (!g_bDolbySupported)
    {
        s3eDebugPrint(10, ypos, "`x666666Dolby Unsupported", 0);
    } else
        switch (g_DolbyProfile)
        {
            case MOVIE:
                s3eDebugPrint(10, ypos, "`x666666Profile: MOVIE", 0);
                break;
            case MUSIC:
                s3eDebugPrint(10, ypos, "`x666666Profile: MUSIC", 0);
                break;
            case GAME:
                s3eDebugPrint(10, ypos, "`x666666Profile: GAME", 0);
                break;
            case VOICE:
                s3eDebugPrint(10, ypos, "`x666666Profile: VOICE", 0);
                break;
            default:
                s3eDebugPrint(10, ypos, "`x666666ERROR", 0);
                break;
        }

    ypos += 2 * textHeight;
    switch (g_Status)
    {
        case STATE_PLAYING:
            s3eDebugPrint(10, ypos, "`x666666Music Playing", 0);
            break;
        case STATE_PAUSED:
            s3eDebugPrint(10, ypos, "`x666666Music Paused", 0);
            break;
        case STATE_STOPPED:
            s3eDebugPrint(10, ypos, "`x666666Music Stopped", 0);
            break;
    }

    ypos += 2 * textHeight;
    int32 pos = s3eAudioGetInt(S3E_AUDIO_POSITION);
    s3eDebugPrintf(10, ypos, 0, "`x666666Position: %d.%d", pos / 1000, pos % 1000);
    ypos += 2 * textHeight;
    s3eDebugPrintf(10, ypos, 0, "`x666666Volume: %d", s3eAudioGetInt(S3E_AUDIO_VOLUME));
}
