/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EAccelerometer S3E Accelerometer Example
 * The following example demonstrates S3E's accelerometer functionality.
 *
 * The main functions used to achieve this are:
 *  - s3eAccelerometerStart()
 *  - s3eAccelerometerStop()
 *  - s3eAccelerometerGetX()
 *  - s3eAccelerometerGetY()
 *  - s3eAccelerometerGetZ()
 *
 * The example allows the user to stop and start the accelerometer subdevice
 * and displays the last reported X, Y & Z values it generated.
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eAccelerometerImage.png
 *
 * @include s3eAccelerometer.cpp
 */

#include "ExamplesMain.h"
#include "s3eAccelerometer.h"
#include <math.h>

static bool g_Enabled = false;
static Button* g_ButtonStart = NULL;
static Button* g_ButtonStop = NULL;
static Button* g_ButtonRotate = NULL;

void ExampleInit()
{
    g_ButtonStart = NewButton("Start Accelerometer");
    g_ButtonStop = NewButton("Stop Accelerometer");
    g_ButtonRotate = NewButton("Rotate");
    g_ButtonStart->m_Enabled = true;
    g_ButtonStop->m_Enabled = false;
    if (s3eAccelerometerStart() == S3E_RESULT_SUCCESS)
    {
        g_Enabled = true;
        g_ButtonStart->m_Enabled = false;
        g_ButtonStop->m_Enabled = true;
    }
}

void ExampleTerm()
{
    s3eAccelerometerStop();
}

bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    // Press 1 to play.
    if (pressed == g_ButtonStart)
    {
        if (s3eAccelerometerStart() == S3E_RESULT_SUCCESS)
        {
            g_Enabled = true;
            g_ButtonStart->m_Enabled = false;
            g_ButtonStop->m_Enabled = true;
        }
    }

    if (pressed == g_ButtonStop)
    {
        s3eAccelerometerStop();

        g_Enabled = false;
        g_ButtonStart->m_Enabled = true;
        g_ButtonStop->m_Enabled = false;
    }

    if (pressed == g_ButtonRotate)
    {
        int dir = s3eSurfaceGetInt(S3E_SURFACE_BLIT_DIRECTION);
        dir += 1;
        dir = dir % 4;
        s3eSurfaceSetup(S3E_SURFACE_PIXEL_TYPE_RGB565, 0, NULL, (s3eSurfaceBlitDirection)dir);
    }
    return true;
}

static void DrawLineHoriz(int x, int y, int length, int16 color)
{
    int16* ptr = (int16*)s3eSurfacePtr();
    int32 pitch = s3eSurfaceGetInt(S3E_SURFACE_PITCH)/2;
    int32 width = s3eSurfaceGetInt(S3E_SURFACE_WIDTH);

    if (length < 0)
    {
        for (int32 x1 = x; x1 > x+length; x1--)
        {
            if (x1 > width || x1 < 0)
                return;
            ptr[y*pitch+x1] = color;
        }
    }
    else
    {
        for (int32 x1 = x; x1 < x+length; x1++)
        {
            if (x1 > width || x1 < 0)
                return;
            ptr[y*pitch+x1] = color;
        }
    }
}

static void DrawLine(int x1, int y1, int x2, int y2, int16 color)
{
    int16* ptr = (int16*)s3eSurfacePtr();
    int32 pitch = s3eSurfaceGetInt(S3E_SURFACE_PITCH)/2;
    int ix, iy;
    // if x1 == x2 or y1 == y2, then it does not matter what we set here
    int delta_x = (x2 > x1?(ix = 1, x2 - x1):(ix = -1, x1 - x2)) << 1;
    int delta_y = (y2 > y1?(iy = 1, y2 - y1):(iy = -1, y1 - y2)) << 1;


    int sw = s3eSurfaceGetInt(S3E_SURFACE_WIDTH);
    int sh = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT);

    if (y1 > 0 && y1 < sh && x1 >0 && x1 < sw)
        ptr[y1*pitch+x1] = color;


    if (delta_x >= delta_y)
    {
        // error may go below zero
        int error = delta_y - (delta_x >> 1);

        while (x1 != x2)
        {
            if (error >= 0)
            {
                if (error || (ix > 0))
                {
                    y1 += iy;
                    error -= delta_x;
                }
                // else do nothing
            }
            // else do nothing

            x1 += ix;
            error += delta_y;

            if (y1 > 0 && y1 < sh && x1 >0 && x1 < sw)
                ptr[y1*pitch+x1] = color;
        }
    }
    else
    {
        // error may go below zero
        int error = delta_x - (delta_y >> 1);

        while (y1 != y2)
        {
            if (error >= 0)
            {
                if (error || (iy > 0))
                {
                    x1 += ix;
                    error -= delta_y;
                }
                // else do nothing
            }
            // else do nothing

            y1 += iy;
            error += delta_x;

            if (y1 > 0 && y1 < sh && x1 >0 && x1 < sw)
                ptr[y1*pitch+x1] = color;
        }
    }
}

static void DrawArrow(int x, int y, int height, int width, int ypos, int maxValue)
{
    float xy = (float)sqrt((x*x) + (y*y));
    if (xy < 1 || xy*xy < y*y)
        return;

    float angle = - acosf(y / xy);
    if (x > 0)
        angle = 2 * (float)M_PI - angle;

    float lineLength = width / 2.f;
    if ((ypos + width) > height)
        lineLength = (height - ypos) / 2.f;
    ypos += (int)lineLength;

    lineLength *= xy / maxValue;

    int endx = (int)(width/2+lineLength*sinf(angle));
    int endy = (int)(ypos-lineLength*cosf(angle));
    DrawLine(width/2, ypos, endx, endy, (int16)s3eSurfaceConvertRGB(0, 0, 0xff));
    angle += (float)M_PI/8;
    DrawLine((int)(endx-lineLength*sinf(angle)/8), (int)(endy+lineLength*cosf(angle)/8), endx, endy, (int16)s3eSurfaceConvertRGB(0, 0, 0xff));
    angle -= (float)M_PI/4;
    DrawLine((int)(endx-lineLength*sinf(angle)/8), (int)(endy+lineLength*cosf(angle)/8), endx, endy, (int16)s3eSurfaceConvertRGB(0, 0, 0xff));
}

/*
 * The ExampleRender function outputs a set of strings indicating the state of
 * playback. The additional output that can occur outputs whether the callback
 * was registered successfully or not.
 */
void ExampleRender()
{
    const int textHeight = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);
    const int maxValue = 1000;
    int ypos = GetYBelowButtons() + textHeight * 2;

    //Displaying status
    if (g_Enabled)
    {
        int32 width = s3eSurfaceGetInt(S3E_SURFACE_WIDTH);
        int32 height = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT);
        int32 x = s3eAccelerometerGetX();
        int32 y = s3eAccelerometerGetY();
        int32 z = s3eAccelerometerGetZ();
        float scale = 2.0f * maxValue / width;
        //Calculate magnitude as square root of squares of the components
        float mag = (float)sqrt((x*x) + (y*y) + (z*z));
        s3eDebugPrintf(10, ypos, 1, "`x666666X: %3d Y: %3d Z: %d", x, y, z);
        ypos += textHeight;
        s3eDebugPrintf(10, ypos, 1, "`x666666Magnitude: %.2f", mag);
        ypos += 2 * textHeight;
        DrawLineHoriz(width/2, ypos, (int)(x/scale), (int16)s3eSurfaceConvertRGB(0xff, 0, 0));
        ypos += textHeight;
        DrawLineHoriz(width/2, ypos, (int)(y/scale), (int16)s3eSurfaceConvertRGB(0, 0xff, 0));
        ypos += textHeight;
        DrawLineHoriz(width/2, ypos, (int)(z/scale), (int16)s3eSurfaceConvertRGB(0, 0, 0xff));
        ypos += textHeight;


        DrawArrow(x, y, height, width, ypos, maxValue);


    }
    else
    {
        s3eDebugPrintf(10, ypos, 1, "`x666666Accelerometer disabled");
    }
}
