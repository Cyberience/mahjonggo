/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EPointerMulti S3E Pointer Multi-Touch Example
 *
 * The following example uses the S3E Pointer functionality to display
 * multiple touches to the screen if supported by the device and to show/hide
 * cursor of mouse or any other pointing device.
 *
 * The functions used to achieve this are:
 * - s3ePointerGetInt()
 * - s3ePointerSetInt()
 * - s3ePointerRegister()
 * - s3ePointerUnRegister()
 *
 * A device with multi-touch support is required to take full advanctage of
 * this example. The app draws a coloured circle under each contact point.
 * If the device supports only one touch point at a time, then just one circle
 * will be drawn. Note that in the s3e examples, regular example UI elements
 * such as the exit button can only be pressed with the first touch on a multi-
 * touch supporting device.
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3ePointerMultiImage.png
 *
 * @include s3ePointerMulti.cpp
 */

#include "ExamplesMain.h"
#include "IwDebug.h"
#include "algorithm"

//Simple structure to track touches
class CTouch
{
public:
    CTouch(int _id = -1): id(_id) {}

    int32 id; // touch's unique identifier
    int32 x; // position x
    int32 y; // position y
    int32 pressure;
    bool active; // is touch active (currently down)
};

bool g_UseMultiTouch = false;
bool g_Pressure = false;
Button* g_MultiTouchButton = 0;
char g_TouchEventMsg[128] = { 0 };
char g_ButtonEventMsg[128] = { 0 };
char g_MoveEventMsg[128] = { 0 };
uint32 g_CursorHide = 0;
Button* g_CursorHideButton = 0;
char g_CursorStatusMsg[128] = {0};

#define MAX_TOUCHES 10
CTouch g_Touches[MAX_TOUCHES];
char* g_TouchMoveEventMsgs[MAX_TOUCHES];

//Colour to render touch in
uint8 g_TouchColours[3*MAX_TOUCHES] =
{
    0xff, 0x00, 0x00,
    0x00, 0xff, 0x00,
    0x00, 0x00, 0xff,
    0xff, 0x00, 0xff,
    0x00, 0xff, 0xff,
    0xff, 0xff, 0x00,
    0x80, 0x00, 0x00,
    0x00, 0x80, 0x00,
    0x00, 0x00, 0x80,
    0x80, 0x00, 0x80,
};

//Find an active touch with the specified id, or allocate a free one from the list
CTouch* GetTouch(int32 id)
{
    CTouch* pInActive = NULL;

    for (uint32 i = 0; i < MAX_TOUCHES; i++)
    {
        if (id == g_Touches[i].id)
            return &g_Touches[i];
        if (!pInActive && g_Touches[i].id == -1)
            pInActive = &g_Touches[i];
    }

    //Return first unused touch entry
    if (pInActive)
    {
        pInActive->id = id;
        return pInActive;
    }

    //No more touches, give up.
    return NULL;
}

void MultiTouchCB(s3ePointerTouchEvent* event)
{
    if (!event->m_Pressed)
        strcpy(g_TouchMoveEventMsgs[event->m_TouchID], "");
    CTouch* touch = GetTouch(event->m_TouchID);
    if (touch)
    {
        touch->active = event->m_Pressed != 0;
        touch->x = event->m_x;
        touch->y = event->m_y;
        touch->pressure = event->m_Pressure;
    }
    sprintf(g_TouchEventMsg, "`x666666Touch %u %s",event->m_TouchID, event->m_Pressed ? "PRESSED" : "RELEASED");
    if (!event->m_Pressed)
        strcpy(g_TouchMoveEventMsgs[event->m_TouchID], "");
}

void MultiTouchMotionCB(s3ePointerTouchMotionEvent* event)
{
    CTouch* touch = GetTouch(event->m_TouchID);
    if (touch)
    {
        touch->x = event->m_x;
        touch->y = event->m_y;
        touch->pressure = event->m_Pressure;

        if (touch->id < MAX_TOUCHES)
        {
            sprintf(g_TouchMoveEventMsgs[event->m_TouchID], "`x666666Touch %d moved: %d %d", event->m_TouchID, event->m_x, event->m_y);
            if (s3ePointerGetInt(S3E_POINTER_PRESSURE_AVAILABLE))
            {
                sprintf(g_TouchMoveEventMsgs[event->m_TouchID], "%s pressure: %d", g_TouchMoveEventMsgs[event->m_TouchID], touch->pressure);
            }
        }
    }
}

void SingleTouchButtonCB(s3ePointerEvent* event)
{
    if (!event->m_Pressed)
    {
        for(int i = 0; i < MAX_TOUCHES; ++i)
            strcpy(g_TouchMoveEventMsgs[i], "");
    }
    g_Touches[0].active = event->m_Pressed != 0;
    g_Touches[0].x = event->m_x;
    g_Touches[0].y = event->m_y;
    char ButtonName[128] = "";
    switch (event->m_Button)
    {
    case S3E_POINTER_BUTTON_LEFTMOUSE:
        strcpy(ButtonName, "Left mouse button / Touch ");
        break;
    case S3E_POINTER_BUTTON_RIGHTMOUSE:
        strcpy(ButtonName, "Right mouse button");
        break;
    case S3E_POINTER_BUTTON_MIDDLEMOUSE:
        strcpy(ButtonName, "Middle mouse button");
        break;
    default:
        break;
    }
    if (strlen(ButtonName))
        sprintf(g_ButtonEventMsg, "`x666666%s %s", ButtonName, event->m_Pressed ? "PRESSED" : "RELEASED");
}

void PointerMotionCB(s3ePointerMotionEvent* event)
{
    g_Touches[0].x = event->m_x;
    g_Touches[0].y = event->m_y;
    sprintf(g_MoveEventMsg, "`x666666Pointer moved: %d %d", event->m_x, event->m_y);
}

void ExampleInit()
{
    // An OS which supports multi-touch will return TOUCH_EVENTs if they are registered for.
    // An OS which does not will only raise standard pointer events.
    g_UseMultiTouch = s3ePointerGetInt(S3E_POINTER_MULTI_TOUCH_AVAILABLE) ? true : false;

    if (s3ePointerGetInt(S3E_POINTER_PRESSURE_AVAILABLE))
    {
        s3ePointerSetInt(S3E_POINTER_PRESSURE_SENSITIVITY, 10);
    }

    if (g_UseMultiTouch)
    {
        //Register for multi touch events on platforms where the functionality is available.
        s3ePointerRegister(S3E_POINTER_TOUCH_EVENT, (s3eCallback)MultiTouchCB, NULL);
        s3ePointerRegister(S3E_POINTER_TOUCH_MOTION_EVENT, (s3eCallback)MultiTouchMotionCB, NULL);
        g_MultiTouchButton = NewButton("Enable button handling");
    }
    else
    {
        //Register for standard pointer events
        s3ePointerRegister(S3E_POINTER_BUTTON_EVENT, (s3eCallback)SingleTouchButtonCB, NULL);
        s3ePointerRegister(S3E_POINTER_MOTION_EVENT, (s3eCallback)PointerMotionCB, NULL);
    }

    g_CursorHideButton = NewButton("Hide cursor");
    sprintf(g_CursorStatusMsg, "`x666666Cursor %s", s3ePointerGetInt(S3E_POINTER_HIDE_CURSOR) ? "hidden" : "shown");
    for(int i = 0; i < MAX_TOUCHES; ++i)
    {
        g_TouchMoveEventMsgs[i] = new char[128];
        strcpy(g_TouchMoveEventMsgs[i], "");
    }
}

void ExampleTerm()
{
    for(int i = 0; i < MAX_TOUCHES; ++i)
        delete[] g_TouchMoveEventMsgs[i];
    //Clear up
    if (g_UseMultiTouch)
    {
        s3ePointerUnRegister(S3E_POINTER_TOUCH_EVENT, (s3eCallback)MultiTouchCB);
        s3ePointerUnRegister(S3E_POINTER_TOUCH_MOTION_EVENT, (s3eCallback)MultiTouchMotionCB);
    }
    else
    {
        s3ePointerUnRegister(S3E_POINTER_BUTTON_EVENT, (s3eCallback)SingleTouchButtonCB);
        s3ePointerUnRegister(S3E_POINTER_MOTION_EVENT, (s3eCallback)PointerMotionCB);
    }
    DeleteButtons();
}

bool ExampleUpdate()
{
    if (g_MultiTouchButton && g_MultiTouchButton == GetSelectedButton())
    {
        if (g_UseMultiTouch)
        {
            SetButtonName(g_MultiTouchButton, "Enable touch handling");
            strcpy(g_TouchEventMsg, "");
            s3ePointerUnRegister(S3E_POINTER_TOUCH_EVENT, (s3eCallback)MultiTouchCB);
            s3ePointerUnRegister(S3E_POINTER_TOUCH_MOTION_EVENT, (s3eCallback)MultiTouchMotionCB);
            //Register for standard pointer events
            s3ePointerRegister(S3E_POINTER_BUTTON_EVENT, (s3eCallback)SingleTouchButtonCB, NULL);
            s3ePointerRegister(S3E_POINTER_MOTION_EVENT, (s3eCallback)PointerMotionCB, NULL);
        }
        else
        {
            SetButtonName(g_MultiTouchButton, "Enable button handling");
            strcpy(g_ButtonEventMsg, "");
            strcpy(g_MoveEventMsg, "");
            s3ePointerUnRegister(S3E_POINTER_BUTTON_EVENT, (s3eCallback)SingleTouchButtonCB);
            s3ePointerUnRegister(S3E_POINTER_MOTION_EVENT, (s3eCallback)PointerMotionCB);
            //Register for multi touch events.
            s3ePointerRegister(S3E_POINTER_TOUCH_EVENT, (s3eCallback)MultiTouchCB, NULL);
            s3ePointerRegister(S3E_POINTER_TOUCH_MOTION_EVENT, (s3eCallback)MultiTouchMotionCB, NULL);
        }

        g_UseMultiTouch = !g_UseMultiTouch;
    }

    if (g_CursorHideButton && g_CursorHideButton == GetSelectedButton())
    {
        g_CursorHide = (g_CursorHide + 1) % 2;
        s3ePointerSetInt(S3E_POINTER_HIDE_CURSOR, g_CursorHide);
        if (0 == g_CursorHide)
            SetButtonName(g_CursorHideButton, "Hide cursor");
        else
            SetButtonName(g_CursorHideButton, "Show cursor");
        sprintf(g_CursorStatusMsg, "`x666666Cursor %s", s3ePointerGetInt(S3E_POINTER_HIDE_CURSOR) ? "hidden" : "shown");
    }

    return true;
}

/*
 * Render a coloured circle at the specified coordinates
 */
void RenderTouch(int32 x, int32 y, uint16 col, int32 radius)
{
    uint16* surfaceData = (uint16*)s3eSurfacePtr();
    int32 width = s3eSurfaceGetInt(S3E_SURFACE_WIDTH);
    int32 height = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT);
    int32 pitch = s3eSurfaceGetInt(S3E_SURFACE_PITCH)/2;

    // clip circular region
    uint32 startx = std::max(x - radius, 0);
    uint32 endx = std::min(width, x + radius);
    uint32 starty = std::max(0, y - radius);
    uint32 endy = std::min(height, y + radius);

    const int32 radiusSq = radius * radius;

    for (uint32 iy = starty; iy < endy; iy++)
    {
        uint16* out = surfaceData + iy * pitch + startx;
        for (uint32 ix = startx; ix < endx; ix++)
        {
            int32 distx = x - ix;
            int32 disty = y - iy;
            // if length of this line is under the radius paint the pixel
            if (distx * distx + disty * disty < radiusSq)
                *out = col;

            out++;
        }
    }
}

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

void ExampleRender()
{
    for (uint32 i = 0; i < MAX_TOUCHES; i++)
    {
        // For each active touch
        if (g_Touches[i].active)
        {
            uint32 radius = 40;
            if (s3ePointerGetInt(S3E_POINTER_PRESSURE_AVAILABLE))
            {
                static float pressureScale = 150.0f / s3ePointerGetInt(S3E_POINTER_PRESSURE_MAXIMUM);
                radius = (uint32)(g_Touches[i].pressure * pressureScale);
                radius = MAX(radius, 5);
            }

            // Render a circle
            uint16 col = (uint16)s3eSurfaceConvertRGB(g_TouchColours[i*3], g_TouchColours[i*3+1], g_TouchColours[i*3+2]);
            RenderTouch(g_Touches[i].x, g_Touches[i].y, col, radius);
        }
    }

    int y = GetYBelowButtons();
    s3eDebugPrint(20, y += 16, g_TouchEventMsg, 1);
    s3eDebugPrint(20, y += 16, g_ButtonEventMsg, 1);
    s3eDebugPrint(20, y += 16, g_CursorStatusMsg, 1);
    s3eDebugPrint(20, y += 16, g_MoveEventMsg, 1);
    for(int i = 0; i < MAX_TOUCHES; ++i)
    {
        if (strlen(g_TouchMoveEventMsgs[i]))
            s3eDebugPrint(20, y += 16, g_TouchMoveEventMsgs[i], 1);
    }
}
