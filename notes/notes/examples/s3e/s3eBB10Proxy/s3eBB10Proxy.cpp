/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2014 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EBB10Proxy S3E BlackBerry 10 Proxy Example
 * The following example demonstrates BlackBerry 10 Proxy.
 *
 * The core of this example is based on an IwHTTP example.
 * It detects if BlackBerry Proxy available and applies it to the connection.
 *
 * The main functions demonstrated in this example are:
 *
 * - s3eBlackBerryProxyAvailable
 * - s3eBlackBerryProxyGetProxy
 *
 * @include s3eBB10Proxy.cpp
 */

//-----------------------------------------------------------------------------

#include <string>
#include "IwHTTP.h"
#include "s3eMemory.h"
#include "ExamplesMain.h"

#include "s3eConfig.h"
#include "s3eBlackBerryProxy.h"

static const unsigned char base64_table[65] =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

/**
 * base64_encode - Base64 encode
 * @src: Data to be encoded
 * @len: Length of the data to be encoded
 * @out_len: Pointer to output length variable, or %NULL if not used
 * Returns: Allocated buffer of out_len bytes of encoded data,
 * or %NULL on failure
 *
 * Caller is responsible for freeing the returned buffer. Returned buffer is
 * nul terminated to make it easier to use as a C string. The nul terminator is
 * not included in out_len.
 */
char * base64_encode(const char *src, size_t len, size_t *out_len)
{
        char *out, *pos;
        const char *end, *in;
        size_t olen;
#ifdef BASE64_ENCODE_ADD_NEWLINES
        int line_len;
#endif

        olen = len * 4 / 3 + 4; /* 3-byte blocks to 4-byte */
        olen += olen / 72; /* line feeds */
        olen++; /* nul termination */
        out = (char *)malloc(olen);
        if (out == NULL)
                return NULL;

        end = src + len;
        in = src;
        pos = out;
#ifdef BASE64_ENCODE_ADD_NEWLINES
        line_len = 0;
#endif
        while (end - in >= 3) {
                *pos++ = base64_table[in[0] >> 2];
                *pos++ = base64_table[((in[0] & 0x03) << 4) | (in[1] >> 4)];
                *pos++ = base64_table[((in[1] & 0x0f) << 2) | (in[2] >> 6)];
                *pos++ = base64_table[in[2] & 0x3f];
                in += 3;
#ifdef BASE64_ENCODE_ADD_NEWLINES
                line_len += 4;
                if (line_len >= 72) {
                        *pos++ = '\n';
                        line_len = 0;
                }
#endif
        }

        if (end - in) {
                *pos++ = base64_table[in[0] >> 2];
                if (end - in == 1) {
                        *pos++ = base64_table[(in[0] & 0x03) << 4];
                        *pos++ = '=';
                } else {
                        *pos++ = base64_table[((in[0] & 0x03) << 4) |
                                              (in[1] >> 4)];
                        *pos++ = base64_table[(in[1] & 0x0f) << 2];
                }
                *pos++ = '=';
#ifdef BASE64_ENCODE_ADD_NEWLINES
               line_len += 4;
#endif
        }

#ifdef BASE64_ENCODE_ADD_NEWLINES
        if (line_len)
               *pos++ = '\n';
#endif

        *pos = '\0';
        if (out_len)
                *out_len = pos - out;
        return out;
}
enum HTTPStatus
{
    kNone,
    kDownloading,
    kOK,
    kError,
};
enum HTTPTest
{
    kHead,
    kGet,
    kSecure,
};

#define HTTP_HOST "sdktest.madewithmarmalade.com"
#define HTTP_URI "http://sdktest.madewithmarmalade.com/example.txt"
#define HTTPS_URI "https://sdktest.madewithmarmalade.com/example.txt"
#define HTTPS_AUTH "user:pass"

CIwHTTP *theHttpObject = NULL;
char* result = NULL, *secureResult = NULL;
uint32 len;
HTTPStatus status = kNone;
HTTPTest testType = kHead;
bool headResult = false;
char bb10Proxy[128] = "";
s3eBlackBerryProxyResult bb10ProxyResult;

static Button* g_ButtonStart = 0;


static int32 GotData(void*, void*)
{
    // This is the callback indicating that a ReadContent call has
    // completed.  Either we've finished, or a bigger buffer is
    // needed.  If the correct amount of data was supplied initially,
    // then this will only be called once. However, it may well be
    // called several times when using chunked encoding.

    // Firstly see if there's an error condition.
    if (theHttpObject->GetStatus() == S3E_RESULT_ERROR)
    {
        // Something has gone wrong
        status = kError;
    }
    else if (theHttpObject->ContentReceived() != theHttpObject->ContentLength())
    {
        // We have some data but not all of it. We need more space.
        uint32 oldLen = len;
        // If iwhttp has a guess how big the next bit of data is (this
        // basically means chunked encoding is being used), allocate
        // that much space. Otherwise guess.
        if (len < theHttpObject->ContentExpected())
            len = theHttpObject->ContentExpected();
        else
            len += 1024;

        // Allocate some more space and fetch the data.
        result = (char*)s3eRealloc(result, len);
        theHttpObject->ReadContent(&result[oldLen], len - oldLen, GotData);
    }
    else
    {
        // We've got all the data. Display it.
        status = kOK;
    }
    return 0;
}

//-----------------------------------------------------------------------------
// Called when the response headers have been received
//-----------------------------------------------------------------------------
static int32 GotHeaders(void*, void*)
{
    if (theHttpObject->GetStatus() == S3E_RESULT_ERROR)
    {
        // Something has gone wrong
        status = kError;
    }
    else if (theHttpObject->GetType() == CIwHTTP::HEAD)
    {
        // this is a HEAD request, we have got the header so return
        headResult = true;
        status = kOK;
    }
    else
    {
        // Depending on how the server is communicating the content
        // length, we may actually know the length of the content, or
        // we may know the length of the first part of it, or we may
        // know nothing. ContentExpected always returns the smallest
        // possible size of the content, so allocate that much space
        // for now if it's non-zero. If it is of zero size, the server
        // has given no indication, so we need to guess. We'll guess at 1k.
        len = theHttpObject->ContentExpected();
        if (!len)
        {
            len = 1024;
        }

        char **resultBuffer = (testType == kSecure) ? &secureResult: &result;

        s3eFree(*resultBuffer);
        *resultBuffer = (char*)s3eMalloc(len + 1);
        (*resultBuffer)[len] = 0;
        theHttpObject->ReadContent(*resultBuffer, len, GotData, NULL);
    }

    return 0;
}

//-----------------------------------------------------------------------------
// Detect and apply BlackBerry proxy
//-----------------------------------------------------------------------------

static void setHttpProxy(const char* host, const char* url)
{
    if (!s3eBlackBerryProxyAvailable())
        return;

    if (s3eBlackBerryProxyGetProxy(host, url, bb10Proxy, sizeof(bb10Proxy)/sizeof(HTTP_HOST[0]), &bb10ProxyResult) != S3E_RESULT_SUCCESS)
        return;

    if (bb10ProxyResult == S3E_BLACKBERRYPROXY_RESULT_PROXY)
    {
        theHttpObject->SetProxy(bb10Proxy);
    }
    else
    {
        theHttpObject->SetProxy(NULL);
    }
}

//-----------------------------------------------------------------------------
void ExampleInit()
{
    g_ButtonStart = NewButton("RUN TEST");
}

//-----------------------------------------------------------------------------
void ExampleTerm()
{
    if (theHttpObject)
        theHttpObject->Cancel();
    delete theHttpObject;
    s3eFree(result);
    s3eFree(secureResult);

}

// From this point on, the code is general housekeeping functions
// common to other examples.

bool ExampleUpdate()
{

    if ((status == kNone) && (GetSelectedButton() == g_ButtonStart))
    {
        g_ButtonStart->m_Enabled = false;

        if (!theHttpObject)
            theHttpObject = new CIwHTTP;

        setHttpProxy(HTTP_HOST, HTTP_URI);
        if (theHttpObject->Head(HTTP_URI, GotHeaders, NULL) == S3E_RESULT_SUCCESS)
            status = kDownloading;
    }
    else if (status == kOK && testType == kHead)
    {
        testType = kGet;

        setHttpProxy(HTTP_HOST, HTTP_URI);
        if (theHttpObject->Get(HTTP_URI, GotHeaders, NULL) == S3E_RESULT_SUCCESS)
            status = kDownloading;
    }
    else if (status == kOK && testType == kGet)
    {
        testType = kSecure;

        char buf[256];
        const char *passString = HTTPS_AUTH;
        char *base64;
        base64 = base64_encode(passString, strlen(passString), NULL);
        snprintf(buf, 256, "Basic %s", base64);
        free(base64);

        theHttpObject->SetRequestHeader("Authorization", buf);
        theHttpObject->SetRequestHeader("Cache-Control", "max-age=0");
        theHttpObject->SetRequestHeader("Accept", "application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5");
        theHttpObject->SetRequestHeader("Accept-Encoding", "gzip,deflate,sdch");
        theHttpObject->SetRequestHeader("Accept-Language", "en-GB");
        theHttpObject->SetRequestHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.3");


        setHttpProxy(HTTP_HOST, HTTPS_URI);
        if (theHttpObject->Post(HTTPS_URI, NULL, 0, GotHeaders, NULL) == S3E_RESULT_SUCCESS)
            status = kDownloading;
    }

    return true;
}

//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Render text
    char buf[512];

    const int textHeight = 2 * s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);
    int ypos = GetYBelowButtons() + textHeight;

    if (headResult)
    {
        snprintf(buf, 512, "`x666666http:// -> Got header");
        s3eDebugPrintf(10, ypos, 1, buf);
        ypos += textHeight;
    }

    if (result)
    {
        snprintf(buf, 512, "`x666666http:// -> %s", result);
        s3eDebugPrintf(10, ypos, 1, buf);
        ypos += textHeight;

        std::string header;
        theHttpObject->GetHeader("Last-Modified", header);
        snprintf(buf, 512, "`x666666Last-Modified: %s", header.c_str());
        s3eDebugPrintf(10, ypos, 1, buf);
        ypos += textHeight;
    }

    if (secureResult)
    {
        snprintf(buf, 512, "`x666666https:// -> %s", secureResult);
        s3eDebugPrintf(10, ypos, 1, buf);
        ypos += textHeight;
    }
    if (status == kError)
    {
        switch (testType)
        {
            case kHead:
                s3eDebugPrintf(10, ypos, 1, "`x666666HEAD Error!");
                break;
            case kGet:
                s3eDebugPrintf(10, ypos, 1, "`x666666GET Error!");
                break;
            case kSecure:
                s3eDebugPrintf(10, ypos, 1, "`x666666SECURE Error!");
                break;
            default:
                break;
        }
        ypos += textHeight;
    }
    if (status != kNone)
    {
        switch (testType)
        {
            case kHead:
                if (!headResult)
                    s3eDebugPrintf(10, ypos, 1, "`x666666HEAD Connecting...");
                break;
            case kGet:
                if (!result)
                    s3eDebugPrintf(10, ypos, 1, "`x666666GET Connecting...");
                break;
            case kSecure:
                if (!secureResult)
                    s3eDebugPrintf(10, ypos, 1, "`x666666SECURE Connecting...");
                break;
            default:
                break;
        }
        ypos += textHeight;
    }

    // print HTTP proxy information
    {
        char tmpstr[S3E_CONFIG_STRING_MAX];
        tmpstr[0] = '\0';
        if (!s3eConfigGetString("connection", "httpproxy", tmpstr))
        {
            s3eDebugPrintf(10, ypos, 1, "`x666666 .ICF HTTP PROXY='%s'", tmpstr);
        }else
        {
            s3eDebugPrintf(10, ypos, 1, "`x666666NO .ICF HTTP PROXY");
        }
        ypos += textHeight;

        if (!s3eBlackBerryProxyAvailable())
        {
            s3eDebugPrintf(10, ypos, 1, "`xee3333s3eBlackBerryProxy extension unavailable.");
            ypos += textHeight;
        }

        if (theHttpObject)
        {
            const char* proxy = NULL;

            if (theHttpObject)
                proxy = theHttpObject->GetProxy();

            if (proxy && proxy[0])
                s3eDebugPrintf(10, ypos, 1, "`x666666HTTP CONNECTION PROXY = '%s'", proxy);
            else
                s3eDebugPrintf(10, ypos, 1, "`x666666HTTP CONNECTION W/O PROXY");
            ypos += textHeight;

        }
    }

}
