/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3ELocation S3E Location Example
 *
 * The following example uses S3E's Location API to display the user's current
 * lattitude, longitude and altitude. It also displays the accuracy of the
 * location in meters.
 *
 * The following example plays a music file using S3E's audio functionality.
 *
 * The main functions and structures used to achieve this are:
 * - s3eLocationAvailable()
 * - s3eLocationStart()
 * - s3eLocationStop()
 * - s3eLocationGet()
 * - s3eLocationGetGPSData()
 * - s3eLocationGetCourse()
 *
 * If the device supports returning a location, the application queries the
 * current location using s3eLocationGet() and displays the lattitude,
 * longitude, altitude and accuracy to the screen using s3eDebugPrint().
 *
 * On some devices you can retrieve further information about the location
 * such as the Satellite data and the course.
 *
 * On iOS it is necessary to define NSLocationWhenInUseUsageDescription
 * or NSLocationUsageDescription keys. See the data/s3eLocation.plist file.
 *
 * The following graphic illustrates the example output:
 *
 * @image html s3eLocationImage.png
 *
 * @include s3eLocation.cpp
 */
#include "ExamplesMain.h"
#include "s3e.h"
#include "s3eLocation.h"
#include "s3eCompass.h"

static s3eLocation g_Location;
static s3eResult g_Error = S3E_RESULT_ERROR;

static s3eLocationGPSData g_GPSData;
static s3eResult g_GPSError = S3E_RESULT_ERROR;

static s3eLocationCourseData g_CourseData;
static s3eResult g_CourseError = S3E_RESULT_ERROR;

void ExampleInit()
{
    s3eLocationStart();
}

void ExampleTerm()
{
    s3eLocationStop();
}

bool ExampleUpdate()
{
    g_Error = s3eLocationGet(&g_Location);

    g_GPSError = s3eLocationGetGPSData(&g_GPSData);

    g_CourseError = s3eLocationGetCourse(&g_CourseData);

    return true;
}

/*
 * The following function displays the current time.
 */
void ExampleRender()
{
    const int   textHeight = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);
    int         y = 50;

    s3eDebugPrintf(10, y, 0, "`x666666Started %d", s3eLocationGetInt(S3E_LOCATION_STARTED));
    y += textHeight;

    s3eDebugPrintf(10, y, 0, "`x666666Has position %d", s3eLocationGetInt(S3E_LOCATION_HAS_POSITION));
    y += textHeight;

    s3eDebugPrintf(10, y, 0, "`x666666Location type %d", s3eLocationGetInt(S3E_LOCATION_TYPE));
    y += textHeight;

    if (g_Error == S3E_RESULT_ERROR)
    {
        s3eDebugPrintf(10, y, 0, "`x666666Could not determine location!");
        return;
    }

    s3eDebugPrintf(10, y, 0, "`x666666Latitude %f", (float)g_Location.m_Latitude);
    y += textHeight;

    s3eDebugPrintf(10, y, 0, "`x666666Longitude %f", (float)g_Location.m_Longitude);
    y += textHeight;

    s3eDebugPrintf(10, y, 0, "`x666666Altitude %f", (float)g_Location.m_Altitude);
    y += textHeight;

    s3eDebugPrintf(10, y, 0, "`x666666H accuracy %f", (float)g_Location.m_HorizontalAccuracy);
    y += textHeight;

    s3eDebugPrintf(10, y, 0, "`x666666V accuracy %f", (float)g_Location.m_VerticalAccuracy);
    y += textHeight;


    if (g_GPSError != S3E_RESULT_ERROR)
    {
        s3eDebugPrintf(10, y, 0, "`x666666Satellites in view %d", g_GPSData.m_NumSatellitesInView);
        y += textHeight;

        s3eDebugPrintf(10, y, 0, "`x666666Satellites used %d", g_GPSData.m_NumSatellitesUsed);
        y += textHeight;
    }


    if (g_CourseError != S3E_RESULT_ERROR)
    {
        s3eDebugPrintf(10, y, 0, "`x666666Course %g", g_CourseData.m_Course);
        y += textHeight;

        s3eDebugPrintf(10, y, 0, "`x666666Heading %g%s", g_CourseData.m_Heading, g_CourseData.m_Heading < 0 ? " (unsupported)" : "");
        y += textHeight;

        s3eDebugPrintf(10, y, 0, "`x666666Speed %g", g_CourseData.m_Speed);
        y += textHeight;
    }
}
