/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EWindowsAd S3E Windows Advertising Example
 *
 * The following demonstrates use of s3eWindowsAd API to display ads
 * provided by Microsoft Windows 8 Advertising API.
 *
 * The functions required to achieve this are:
 * - s3eWindowsAdAvailable()
 * - s3eWindowsAdCreate()
 * - s3eWindowsAdSetIntProperty()
 * - s3eWindowsAdShow()
 * - s3eWindowsAdHide()
 *
 * @include s3eWindowsAd.cpp
 */

#include "ExamplesMain.h"
#include "s3eWindowsAd.h"

static Button* s_ButtonShow = NULL;
static Button* s_ButtonVAlignBottom = NULL;
static Button* s_ButtonVAlignCenter = NULL;
static Button* s_ButtonHide = NULL;
static bool    s_AdVisible = false;

static s3eWindowsAd* s_AdControl = NULL;

struct SOSInfo
{
    const char* szAppId;
    const char* szAdId;
    unsigned int unWidth;
    unsigned int unHeight;
};
//This is Test Mode Values for Microsoft Ads. In order to receive
//real (not test) ads, the ApplicationId and the AdUnitId properties
//must be set to the application ID and ad unit ID provided by
//Windows Dev Center.
static SOSInfo g_WP80 = {"test_client", "Image480_80", 480, 80};
static SOSInfo g_WS   = {"d25517cb-12d4-4699-8bdc-52040c712cab", "10043105", 250, 250};
static SOSInfo g_WP81 = {"test_client", "TextAd", 480, 80 };
static SOSInfo g_WIN10 = {"d25517cb-12d4-4699-8bdc-52040c712cab", "10043121", 300, 250};

static struct SOSInfo* GetOSInfoId()
{
    switch (s3eDeviceGetInt(S3E_DEVICE_OS))
    {
    case S3E_OS_ID_WP8:
        return &g_WP80;
    case S3E_OS_ID_WS8:
    case S3E_OS_ID_WS81:
        return &g_WS;
    case S3E_OS_ID_WP81:
        return &g_WP81;
    case S3E_OS_ID_WIN10:
        return &g_WIN10;
    }
    return NULL;
}

void ExampleInit()
{
    s_ButtonShow = NewButton("Show Windows Ad");
    s_ButtonVAlignBottom = NewButton("Align Ad Bottom");
    s_ButtonVAlignCenter = NewButton("Align Ad Center");
    s_ButtonHide = NewButton("Hide Windows Ad");

    s_ButtonVAlignBottom->m_Enabled = false;
    s_ButtonVAlignCenter->m_Enabled = false;
    s_ButtonHide->m_Enabled = false;

    if (s3eWindowsAdAvailable())
        s_AdControl = s3eWindowsAdCreate(GetOSInfoId()->szAppId, GetOSInfoId()->szAdId);

    if (!s_AdControl)
    {
        s_ButtonShow->m_Enabled = false;
        return;
    }


    // set banner sizes and alignment
    s3eWindowsAdSetIntProperty(s_AdControl, S3E_WINDOWS_ADCONTROL_WIDTH, GetOSInfoId()->unWidth);
    s3eWindowsAdSetIntProperty(s_AdControl, S3E_WINDOWS_ADCONTROL_HEIGHT, GetOSInfoId()->unHeight);
    s3eWindowsAdSetIntProperty(s_AdControl, S3E_WINDOWS_ADCONTROL_VALIGN, S3E_WINDOWS_ADCONTROL_VALIGN_BOTTOM);
    s3eWindowsAdSetIntProperty(s_AdControl, S3E_WINDOWS_ADCONTROL_HALIGN, S3E_WINDOWS_ADCONTROL_HALIGN_CENTER);
}

void ExampleTerm()
{
}

bool ExampleUpdate()
{
    // Check for the availability of the s3eWindowsAd API.
    if (!s3eWindowsAdAvailable())
        return true;

    Button* pressed = GetSelectedButton();
    if (pressed == s_ButtonShow)
    {
        s3eWindowsAdShow(s_AdControl);
        s_AdVisible = true;
    }
    else if (pressed == s_ButtonVAlignBottom)
    {
        s3eWindowsAdSetIntProperty(s_AdControl, S3E_WINDOWS_ADCONTROL_VALIGN, S3E_WINDOWS_ADCONTROL_VALIGN_BOTTOM);
    }
    else if (pressed == s_ButtonVAlignCenter)
    {
        s3eWindowsAdSetIntProperty(s_AdControl, S3E_WINDOWS_ADCONTROL_VALIGN, S3E_WINDOWS_ADCONTROL_VALIGN_CENTER);
    }
    else if (pressed == s_ButtonHide)
    {
        s3eWindowsAdHide(s_AdControl);
        s_AdVisible = false;
    }

    s_ButtonShow->m_Enabled = !s_AdVisible;
    s_ButtonVAlignBottom->m_Enabled = s_AdVisible;
    s_ButtonVAlignCenter->m_Enabled = s_AdVisible;
    s_ButtonHide->m_Enabled = s_AdVisible;

    return true;
}

/*
 * The following function outputs the strings entered by the user.
 * The s3eDebugPrint() function to print the strings.
 */
void ExampleRender()
{
    // Print results
    if (!s3eWindowsAdAvailable())
    {
        int y = GetYBelowButtons();
        s3eDebugPrint(50, y + 10, "`xff0000Windows Advertising API isn't available on this device.", 1);
        return;
    }
}
