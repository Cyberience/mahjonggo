/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EMetal S3E Metal Example
 *
 * This example is a simple graphics example using the Metal
 * framework. It is the equivalent of the s3eGLES31 example.
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eMetalImage.png
 *
 * @include s3eMetal.cpp
 */

#include "s3e.h"
#include "s3eMetal.h"
#include <math.h>
#include <stdio.h>

// global veriables
CMetalPtr<CMetalDevice> g_Device;
CMetalPtr<CMetalDrawable> g_Drawable;
CMetalPtr<CMetalCommandQueue> g_CommandQueue;
CMetalPtr<CMetalRenderPipelineState> g_Effect;
CMetalPtr<CMetalComputePipelineState> g_Compute;
CMetalPtr<CMetalBuffer> g_Position;
CMetalPtr<CMetalBuffer> g_TexCoord;
CMetalPtr<CMetalBuffer> g_Phase;
CMetalViewport g_Viewport;

CMetalPtr<CMetalTexture> g_Texture;
CMetalPtr<CMetalSamplerState> g_Sampler;

bool init()
{
    CMetalError* error = NULL;

    //create library of Metal Shader functions
    CMetalPtr<CMetalLibrary> lib = g_Device->NewLibrary(
"using namespace metal;\n"
"struct VertexInOut\n"
"{\n"
"    float4  pos [[position]];\n"
"    float2  texCoord;\n"
"};\n"
"vertex VertexInOut textVertex(uint vid [[ vertex_id ]],\n"
"                              constant packed_float4* position  [[ buffer(0) ]],\n"
"                              constant packed_float2* texCoord  [[ buffer(1) ]])\n"
"{\n"
"    VertexInOut outVertex;\n"
"    outVertex.pos = position[vid];\n"
"    outVertex.texCoord = texCoord[vid];\n"
"    return outVertex;\n"
"};\n"
"fragment half4 textFragment(VertexInOut       inFrag [[stage_in]],\n"
"                            texture2d<float>  tex2D     [[ texture(0) ]],\n"
"                            sampler           sampler2D [[ sampler(0) ]])\n"
"{\n"
"   float color = tex2D.sample(sampler2D, inFrag.texCoord).x;\n"
"   return half4(color, 1, 1, 1);\n"
"};\n"
"kernel void textCompute(constant float& phase [[ buffer(0) ]], texture2d<float,access::write> out [[ texture(0) ]],\n"
"                        uint2 localPos [[thread_position_in_threadgroup]], uint2 groupPos [[threadgroup_position_in_grid]],\n"
"                        uint2 globalPos [[thread_position_in_grid]])\n"
"{\n"
"    float localCoef = length(float2(int2(localPos) - 8) / 8.0);\n"
"    float globalCoef = sin(float(groupPos.x + groupPos.y)*0.1 + phase) * 0.5;\n"
"    out.write(float4(1.0 - globalCoef*localCoef, 0.0, 0.0, 1.0), globalPos);\n"
"}\n", CMetalCompileOptions(), &error);

    if (error != NULL)
        s3eDebugErrorPrintf("Metal Compile Error: %s", error->m_Description);
    if (lib.get() == NULL)
        return false;

    // Create render pipeline state
    CMetalRenderPipelineDescriptor desc;
    desc.m_ColourAttachments[0].m_PixelFormat = CMetalTextureDescriptor::BGRA8Unorm;
    desc.m_SampleCount = 1;
    desc.m_VertexFunction = lib->NewFunction("textVertex");
    desc.m_FragmentFunction = lib->NewFunction("textFragment");

    //CMetalReflection* reflect = NULL;
    //g_Effect = g_Device->NewRenderPipelineState(&desc, CMetalReflection::BUFFER_TYPE_INFO, &reflect, &error);
    g_Effect = g_Device->NewRenderPipelineState(desc, &error);

    if (error != NULL)
        s3eDebugErrorPrintf("Metal Render Error: %s", error->m_Description);
    //if (reflect != NULL)
    //    reflect->Release();

    // Create compute pipeline state
    CMetalPtr<CMetalFunction> computeFn = lib->NewFunction("textCompute");
    g_Compute = g_Device->NewComputePipelineState(computeFn.get(), &error);

    if (error != NULL)
        s3eDebugErrorPrintf("Metal Compute Error: %s", error->m_Description);

    // Create position buffer
    g_Position = g_Device->NewBuffer(3*4*sizeof(float), CMetalBuffer::OPTION_CPU_CACHE_MODE_DEFAULT);
    float* pos = (float*)g_Position->Contents();
    pos[ 0] =  0; pos[ 1] = -1; pos[ 2] = 0; pos[ 3] = 1;
    pos[ 4] =  1; pos[ 5] =  1; pos[ 6] = 0; pos[ 7] = 1;
    pos[ 8] = -1; pos[ 9] =  1; pos[10] = 0; pos[11] = 1;

    // Create text coord buffer
    g_TexCoord = g_Device->NewBuffer(3*2*sizeof(float), CMetalBuffer::OPTION_CPU_CACHE_MODE_DEFAULT);
    float* uv = (float*)g_TexCoord->Contents();
    uv[0] = 0.5f; uv[1] = 0;
    uv[2] = 1; uv[3] = 1;
    uv[4] = 0; uv[5] = 1;

    // Create shader variable buffer
    g_Phase = g_Device->NewBuffer(sizeof(float), CMetalBuffer::OPTION_CPU_CACHE_MODE_DEFAULT);

    // Setup viewport
    g_Viewport.m_OriginX = 0;
    g_Viewport.m_OriginY = 0;
    g_Viewport.m_Width = s3eSurfaceGetInt(S3E_SURFACE_WIDTH);
    g_Viewport.m_Height = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT);
    g_Viewport.m_ZNear = 0;
    g_Viewport.m_ZFar = 1;

    // Create texture to pass results from compute shader to pixel shader
    CMetalTextureDescriptor tDesc(CMetalTextureDescriptor::R8Unorm, 512, 512, false);
    g_Texture = g_Device->NewTexture(tDesc);

    // Create sampler
    CMetalSamplerDescriptor sDesc;
    sDesc.m_MinFilter = CMetalSamplerDescriptor::MIN_MAG_FILTER_LINEAR;
    sDesc.m_MagFilter = CMetalSamplerDescriptor::MIN_MAG_FILTER_LINEAR;
    g_Sampler = g_Device->NewSamplerState(sDesc);

    return true;
}

void render()
{
    // Get next frame to render
    g_Drawable = s3eMetalNextDrawable();
    CMetalPtr<CMetalCommandBuffer> commandBuffer = g_CommandQueue->CommandBuffer();

    // update shader variable
    static float offset = 0;
    float* phase = (float*)g_Phase->Contents();
    phase[0] = offset;
    offset += 0.01f;

    {
        // Run compute shader
        CMetalPtr<CMetalComputeCommandEncoder> compute = commandBuffer->ComputeCommandEncoder();

        compute->SetComputePipelineState(g_Compute.get());
        compute->SetTexture(g_Texture.get(), 0);
        compute->SetBuffer(g_Phase.get(), 0, 0);

        compute->Dispatch(512/16, 512/16, 16, 16);

        compute->EndEncoding();
    }

    // Setup render pass
    CMetalRenderPassDescriptor desc;
    desc.m_ColorAttachments[0].m_Texture = g_Drawable->Texture();
    desc.m_ColorAttachments[0].m_LoadAction = CMetalRenderPassAttachmentDescriptor::LOAD_ACTION_CLEAR;
    desc.m_ColorAttachments[0].m_ClearColor = CMetalRenderPassAttachmentColorDescriptor::ClearColor(0, 0.5, 0.5);
    desc.m_ColorAttachments[0].m_StoreAction = CMetalRenderPassAttachmentDescriptor::STORE_ACTION_STORE;

    {
        // Do Draw
        CMetalPtr<CMetalRenderCommandEncoder> encoder = commandBuffer->RenderCommandEncoder(desc);

        encoder->SetViewport(g_Viewport);

        encoder->SetRenderPipelineState(g_Effect.get());

        encoder->SetVertexBuffer(g_Position.get(), 0, 0);
        encoder->SetVertexBuffer(g_TexCoord.get(), 0, 1);

        encoder->SetFragmentSamplerState(g_Sampler.get(), 0);
        encoder->SetFragmentTexture(g_Texture.get(), 0);

        encoder->DrawPrimitives(CMetalRenderCommandEncoder::PRIMITIVE_TRIANGLE, 0, 3);

        encoder->EndEncoding();
    }
    commandBuffer->PresentDrawable(g_Drawable.get());
    commandBuffer->Commit();
}

int main()
{
    bool quit = false;

    // Setup Metal
    g_Device = s3eMetalGetDevice(true);

    g_CommandQueue = g_Device->NewCommandQueue();

    if (!init())
        return -1;

    int numFrames = 0;

    // Frame loop
    while (!quit) {
        if (s3eDeviceCheckQuitRequest())
            quit = 1;

        printf("Frame Start\n");
        render();
        printf("Frame End\n");

        s3eKeyboardUpdate();
        s3eDeviceYield(0);
        numFrames++;
    }

    // remove globals
    g_Effect = NULL;
    g_Compute = NULL;
    g_Position = NULL;
    g_TexCoord = NULL;
    g_Phase = NULL;

    g_Texture = NULL;
    g_Sampler = NULL;

    g_CommandQueue = NULL;
    g_Device = NULL;
    g_Drawable = NULL;

    return 0;
}
