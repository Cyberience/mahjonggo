# This .config.txt file documents configuration settings for your
# application
# The syntax is similar to that in .icf files:
#
# [GroupName]
# Setting     Documentation for setting
#
# e.g.
# [MyApplicationGroup]
# MySetting   Description of what MySetting is for, its default values, etc
[ThreadExample]
UseRenderThread <Boolean> Use separate thread for rendering to demonstrate threading features. This feature has limited platfrom support.

