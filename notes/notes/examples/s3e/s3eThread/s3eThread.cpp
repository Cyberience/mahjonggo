/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EThread S3E Thread Example
 *
 * The following example demonstrates S3E's threading functionality.
 *
 * It creates two threads and allows the user to lock and unlock one
 * via a button.
 *
 * @note Drawing from non-main thread can be limited by platfrom
 *
 * @include s3eThread.cpp
 */

#include "s3e.h"
#include "s3eThread.h"
#include "ExamplesMain.h"

#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#define MS_PER_SECOND   1000
#define MS_PER_MINUTE   (MS_PER_SECOND * 60)
#define MS_PER_HOUR     (MS_PER_MINUTE * 60)
#define MS_PER_DAY      (MS_PER_HOUR * 24)

static int g_Counter = 0;
static s3eThread* render_thread;
static s3eThread* counter_thread;;
static s3eThreadLock* g_Lock;
static bool g_Locked;
static Button* g_ButtonLock;

extern "C"
{
    s3eErrorShowResult s3eDebugErrorPrintf(const char* fmt, ...);
}

void ExampleRender()
{
    //s3eDebugTracePrintf("Render");
    int height = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT);

    //Clear screen
    const int textHeight = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);

    // Returns the time since the Epoch (00:00:00 UTC, January 1, 1970),
    // measured in milliseconds.
    uint64 timeMilli = s3eTimerGetUTC() % MS_PER_DAY;

    // Assemble string for displaying of time.
    uint8 hr =  (uint8)  (timeMilli / MS_PER_HOUR);
    uint8 min = (uint8) ((timeMilli / MS_PER_MINUTE) % 60);
    uint8 sec = (uint8) ((timeMilli / MS_PER_SECOND) % 60);
    uint  ms = (uint)(timeMilli % 1000);

    int y = height/3;
    // Displaying clock centered vertically and horizontally.
    s3eDebugPrintf(20, y, 1, "`x666666%02d:%02d:%02d:%03u", hr, min, sec, ms);

    s3eDebugPrintf(20, y+textHeight*2, 1, "`x666666Counter = %02d", g_Counter);

    //s3eDebugTracePrintf("Render done");
}

void* CounterMain(void* arg)
{
    s3eDebugTracePrintf("in CounterMain");
    while (!s3eDeviceCheckQuitRequest())
    {
        sleep(1);
        s3eThreadLockAcquire(g_Lock);
        g_Counter++;
        s3eThreadLockRelease(g_Lock);
    }
    return NULL;
}

void* RenderMain(void* arg)
{
    s3eDebugTracePrintf("in RenderMain");
    while (!s3eDeviceCheckQuitRequest())
    {
        if (!ExamplesMainUpdate())
            break;
        ExampleRender();
        //exit(1);
    }
    ExamplesMainTerm();
    return NULL;
}

int main()
{
    int useRenderThread = true;
    s3eConfigGetInt("ThreadExample", "UseRenderThread", &useRenderThread);

    s3eDebugTracePrintf("in main: %p", RenderMain);
    ExamplesMainInit();
    if (!s3eThreadAvailable())
    {
        s3eDebugErrorPrintf("s3eThread extension not present");
        return 1;
    }

    if (useRenderThread)
    {
        render_thread = s3eThreadCreate(RenderMain, NULL, NULL);
        if (!render_thread)
        {
            s3eDebugErrorPrintf("s3eThreadCreate failed: %s", s3eThreadGetErrorString());
            return 1;
        }
    }

    g_Lock = s3eThreadLockCreate();
    counter_thread = s3eThreadCreate(CounterMain, NULL, NULL);
    if (!counter_thread)
    {
        s3eDebugErrorPrintf("s3eThreadCreate failed: %s", s3eThreadGetErrorString());
        return 1;
    }

    if (!useRenderThread)
    {
        RenderMain(NULL);
    }
    else
    {
        s3eThreadJoin(render_thread, NULL);
    }

    s3eThreadJoin(counter_thread, NULL);
    s3eThreadLockDestroy(g_Lock);
    return 0;
}

bool ExampleUpdate()
{
    return true;
}

void LockCounter(Button* button)
{
    if (g_Locked)
    {
        g_Locked = false;
        s3eThreadLockRelease(g_Lock);
        SetButtonName(g_ButtonLock, "Lock Counter");
    }
    else
    {
        g_Locked = true;
        s3eThreadLockAcquire(g_Lock);
        SetButtonName(g_ButtonLock, "Unlock Counter");
    }
}

void ExampleInit()
{
    g_ButtonLock = NewButton("Lock counter", LockCounter);
}

void ExampleTerm()
{
    if (g_Locked)
    {
        g_Locked = false;
        s3eThreadLockRelease(g_Lock);
    }
}
