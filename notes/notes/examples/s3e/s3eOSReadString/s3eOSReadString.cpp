/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EOSReadString S3E OS Read String Example
 *
 * The following example call the s3eOSReadString API to allow the user
 * to use the phone's native string input dialog to enter a Password, an Email address and an URL
 * then display the resulting texts.
 *
 * The functions required to achieve this are:
 * - s3eOSReadStringAvailable()
 * - s3eOSReadStringUTF8()
 * - s3eDebugPrintf()
 *
 * All examples will follow this basic pattern; a brief description of what
 * the example does will be given followed by a list of all the important
 * functions and, perhaps, classes.
 *
 * Should the example be more complex, a more detailed explanation of what the
 * example does and how it does it will be added.
 *
 *
 * @include s3eOSReadString.cpp
 */

#include "ExamplesMain.h"
#include "s3eOSReadString.h"

static char g_Password[256];
static char g_Email[512];
static char g_URL[1024];
static char g_Number[1024];

static Button* g_Button = 0;

void ExampleInit()
{
    g_Button = NewButton("Get User Input");
    g_Password[0] = '\0';
    g_Email[0] = '\0';
    g_URL[0] = '\0';
    g_Number[0] = '\0';
}

void ExampleTerm()
{
}

bool ExampleUpdate()
{
    // Check for the availabilty of the device's strings type input functionality.
    if (!s3eOSReadStringAvailable())
        return true;

    Button* pressed = GetSelectedButton();
    if (pressed != g_Button)
        return true;

    // For every string type, prompt for the user's input...

    // Note use of a long prompt string to demonstrate OS handling of this situation
    const char* readString = s3eOSReadStringUTF8("Please enter a password (characters should be hidden)", S3E_OSREADSTRING_FLAG_PASSWORD);
    if (readString)
        strcpy(g_Password, readString);
    else
        strcpy(g_Password, "<User failed to enter a string!>");


    readString = s3eOSReadStringUTF8("Enter an email address", S3E_OSREADSTRING_FLAG_EMAIL);
    if (readString)
        strcpy(g_Email, readString);
    else
        strcpy(g_Email, "<User failed to enter a string!>");


    readString = s3eOSReadStringUTF8("Enter a URL link", S3E_OSREADSTRING_FLAG_URL);
    if (readString)
        strcpy(g_URL, readString);
    else
        strcpy(g_URL, "<User failed to enter a string!>");

    readString = s3eOSReadStringUTF8WithDefault("Edit the previous URL string", g_URL, S3E_OSREADSTRING_FLAG_URL);
    if (readString)
        strcpy(g_URL, readString);

    readString = s3eOSReadStringUTF8("Enter a number", S3E_OSREADSTRING_FLAG_NUMBER);
    if (readString)
        strcpy(g_Number, readString);
    else
        strcpy(g_Number, "<User failed to enter a number!>");

    return true;
}

/*
 * The following function outputs the strings entered by the user.
 * The s3eDebugPrint() function to print the strings.
 */
void ExampleRender()
{
    // Print results
    if (!s3eOSReadStringAvailable())
    {
        s3eDebugPrint(50, 100, "`xff0000Strings reading API isn't available on this device.", 1);
    }
    else
    {
        const int fontHeight = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);
        int y = GetYBelowButtons() + fontHeight;;

        s3eDebugPrintf(10, y, 1, "`x0000ffPassword: %s", g_Password);
        y += fontHeight * 3;

        s3eDebugPrintf(10, y, 1, "`x0000ffEmail: %s", g_Email);
        y += fontHeight * 3;

        s3eDebugPrintf(10, y, 1, "`x0000ffURL: %s", g_URL);
        y += fontHeight * 3;

        s3eDebugPrintf(10, y, 1, "`x0000ffNumber: %s", g_Number);
    }
}
