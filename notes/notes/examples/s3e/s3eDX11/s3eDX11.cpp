/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EDX11 S3E DX11 Example
 * The following example demonstrates using DX11 from Marmalade.
 *
 * This only works on platforms that support DX11 (Windows Desktop, Windows Store and Windows Phone).
 *
 * @include s3eDX11.cpp
 */

#include "s3eDX11.h"

#include <string.h>
#include <s3eDebug.h>
#include <s3eSurface.h>
#include <math.h>
#include "s3eWindow.h"

// dx11 interface globals
ID3D11Device* g_Device;
ID3D11DeviceContext* g_DeviceContext;
ID3D11VertexShader* g_VSShader;
ID3D11PixelShader* g_PSShader;
ID3D11InputLayout* g_Layout;
ID3D11RenderTargetView* g_RenderView;
ID3D11Buffer* g_VBuffer;
ID3D11Buffer* g_PhaseBuffer;

float phaseData[] = {0.0f};

// current state
bool g_Active = false;
bool g_Fullscreen = false;
int g_Width = 0;
int g_height = 0;
bool g_FullscreenChanged = false;
bool g_SizeChanged = false;

// compile passed in source, return NULL if failed or compilation not available
ID3D10Blob* Compile(const char* source, const char* entry, const char* target)
{
    s3eDX11Compiler* compiler = s3eDX11GetCompiler();
    if (compiler == NULL)
        return NULL;

    UINT dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;

    ID3D10Blob* shader;
    ID3DBlob* error;
    HRESULT hr = compiler->Compile(source, strlen(source),  entry, NULL, NULL, entry, target, dwShaderFlags, 0, &shader, &error);

    if (error)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE_STOP, (char*)error->GetBufferPointer());
        error->Release();
    }
    if (FAILED(hr)) return NULL;

    return shader;
}

// load a binary shader from file
char* Load(const char* filename, int& size)
{
    s3eFile* fp = s3eFileOpen(filename, "rb");
    if (fp == NULL) return NULL;

    size = s3eFileGetSize(fp);
    char* buffer = new char[size];

    s3eFileRead(buffer, 1, size, fp);
    s3eFileClose(fp);

    return buffer;
}

// compile vertex shader from source or load binary if shader compilation is not available
// also create input layout from shader and passed in layout
bool CompileVS(const char* source, D3D11_INPUT_ELEMENT_DESC *layout, int layoutSize)
{
    HRESULT hr = 0;
    ID3D10Blob* shader = Compile(source, "VS", "vs_4_0_level_9_1");

    if (shader != NULL)
    {
        hr = g_Device->CreateVertexShader(shader->GetBufferPointer(), shader->GetBufferSize(), NULL, &g_VSShader);

        g_Device->CreateInputLayout(layout, layoutSize, shader->GetBufferPointer(), shader->GetBufferSize(), &g_Layout);

        shader->Release();
    } else {
        int size;
        char* buffer = Load("shaders/doom.vc", size);
        if (buffer == NULL) return false;

        hr = g_Device->CreateVertexShader(buffer, size, NULL, &g_VSShader);

        g_Device->CreateInputLayout(layout, layoutSize, buffer, size, &g_Layout);

        delete[] buffer;
    }

    return SUCCEEDED(hr);
}

// compile pixel shader from source or load binary if shader compilation is not available
bool CompilePS(const char* source)
{
    HRESULT hr = 0;
    ID3D10Blob* shader = Compile(source, "PS", "ps_4_0_level_9_1");

    if (shader != NULL)
    {
        hr = g_Device->CreatePixelShader(shader->GetBufferPointer(), shader->GetBufferSize(), NULL, &g_PSShader);

        shader->Release();
    } else {
        int size;
        char* buffer = Load("shaders/doom.pc", size);
        if (buffer == NULL) return false;

        hr = g_Device->CreatePixelShader(buffer, size, NULL, &g_PSShader);

        delete[] buffer;
    }

    return SUCCEEDED(hr);
}

// create view to render into
void CreateViews()
{
    ID3D11Texture2D* buffer = s3eDX11GetBackBuffer();

    D3D11_TEXTURE2D_DESC desc;
    buffer->GetDesc(&desc);
    s3eDebugTracePrintf("CreateView %d %d", desc.Width, desc.Height);

    HRESULT hr = g_Device->CreateRenderTargetView(buffer, NULL, &g_RenderView);
    buffer->Release();

    g_DeviceContext->OMSetRenderTargets(1, &g_RenderView, NULL);
}

// remove the view
void ReleaseViews()
{
    g_RenderView->Release();
    g_RenderView = NULL;

    g_DeviceContext->OMSetRenderTargets(0, NULL, NULL);
}

// on resize, release the view, resize, recreate the view at the new size and then change the viewport
void Resize()
{
    ReleaseViews();

    s3eDX11Resize(g_Width, g_height);

    CreateViews();

    D3D11_VIEWPORT vp;
    vp.Width = (float)g_Width;
    vp.Height = (float)g_height;
    vp.MinDepth = 0.0f;
    vp.MaxDepth = 1.0f;
    vp.TopLeftX = 0;
    vp.TopLeftY = 0;
    g_DeviceContext->RSSetViewports( 1, &vp );
}

// on entry to fullscreen signal fullscreen changed (handled at end of frame)
int32 EnterFS(void* systemData, void* userData)
{
    g_Fullscreen = true;
    g_FullscreenChanged = true;

    return S3E_RESULT_SUCCESS;
}

// on exit from fullscreen signal fullscreen changed (handled at end of frame)
int32 ExitFS(void* systemData, void* userData)
{
    g_Fullscreen = false;
    g_FullscreenChanged = true;

    return S3E_RESULT_SUCCESS;
}

// on resize called signal size changed (handled at end of frame)
int32 ResizeCalled(void* systemData, void* userData)
{
    g_SizeChanged = true;

    return S3E_RESULT_SUCCESS;
}

// check for size or fullscreen changed and handle
// fullscreen has specified size, windowed takes size from surface
void Check()
{
    if (g_Fullscreen)
    {
        g_Width = 1024;
        g_height = 768;
    } else {
        g_Width = s3eSurfaceGetInt(S3E_SURFACE_DEVICE_WIDTH);
        g_height = s3eSurfaceGetInt(S3E_SURFACE_DEVICE_HEIGHT);
    }

    if (g_FullscreenChanged)
    {
        if (g_Fullscreen)
        {
            s3eDebugTracePrintf("EnterFS %d %d", g_Width, g_height);
            s3eDX11SetFullScreen(true, g_Width, g_height);
        } else {
            s3eDebugTracePrintf("ExitFS %d %d", g_Width, g_height);
            s3eDX11SetFullScreen(false, g_Width, g_height);
        }

        g_SizeChanged = true;
        g_FullscreenChanged = false;
    }
    if (g_SizeChanged)
    {
        s3eDebugTracePrintf("Resize %d %d", g_Width, g_height);
        Resize();

        g_SizeChanged = false;
    }
}

// create all dx11 resources
void init()
{
    D3D_FEATURE_LEVEL level = s3eDX11Initialize(0, 0);

    g_Device = s3eDX11GetDevice();

    g_Device->GetImmediateContext(&g_DeviceContext);

    CreateViews();

    ID3D11Texture2D* buffer = s3eDX11GetBackBuffer();
    D3D11_TEXTURE2D_DESC desc;
    buffer->GetDesc(&desc);
    buffer->Release();

    D3D11_VIEWPORT vp;
    vp.Width = (float)desc.Width;
    vp.Height = (float)desc.Height;
    vp.MinDepth = 0.0f;
    vp.MaxDepth = 1.0f;
    vp.TopLeftX = 0;
    vp.TopLeftY = 0;

    g_DeviceContext->RSSetViewports( 1, &vp );

    D3D11_INPUT_ELEMENT_DESC layout[] = {
        {"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0},
    };

    CompileVS(
        "struct VertexShaderOutput\n"
        "{\n"
        "    float4 Pos : SV_POSITION;\n"
        "    float2 Pos2 : TEXCOORD;\n"
        "};\n"
        "VertexShaderOutput VS(float4 Pos : POSITION)\n"
        "{\n"
        "    VertexShaderOutput output;\n"
        "    output.Pos = Pos;\n"
        "    output.Pos2 = Pos.xy;\n"
        "    return output;\n"
        "}\n", layout, 1);

    CompilePS("cbuffer ConstantBuffer : register( b0 )\n"
        "{\n"
        "    float Phase;\n"
        "}\n"
        "struct VertexShaderOutput\n"
        "{\n"
        "    float4 Pos : SV_POSITION;\n"
        "    float2 Pos2 : TEXCOORD;\n"
        "};\n"
        "float4 PS( VertexShaderOutput input ) : SV_Target\n"
        "{\n"
        "    return float4( 1.0f, 1.0f, 1.0f, 1.0f ) * sin((input.Pos2.x * input.Pos2.x + input.Pos2.y * input.Pos2.y) * 4.0 + Phase);\n"
        "}\n");

    g_DeviceContext->IASetInputLayout( g_Layout );

    float verts[] = {
         0,  1, 1,
         1, -1, 1,
        -1, -1, 1,
    };
    D3D11_BUFFER_DESC bd;
    bd.Usage = D3D11_USAGE_DEFAULT;
    bd.ByteWidth = 3*3*sizeof(float);
    bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    bd.CPUAccessFlags = 0;
    bd.MiscFlags = 0;
    bd.StructureByteStride = 0;

    D3D11_SUBRESOURCE_DATA init;
    init.pSysMem = verts;
    init.SysMemPitch = 0;
    init.SysMemSlicePitch = 0;

    g_Device->CreateBuffer(&bd, &init, &g_VBuffer);

    UINT stride = sizeof(float)*3;
    UINT offset = 0;
    g_DeviceContext->IASetVertexBuffers(0, 1, &g_VBuffer, &stride, &offset);

    bd.ByteWidth = sizeof(float)*4;
    bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

    g_Device->CreateBuffer(&bd, NULL, &g_PhaseBuffer);

    g_DeviceContext->PSSetConstantBuffers(0, 1, &g_PhaseBuffer);

    g_DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

    g_Active = true;
}

// release all dx11 resources
void shutdown()
{
    g_Active = false;

    g_PhaseBuffer->Release();
    g_VBuffer->Release();
    g_VSShader->Release();
    g_PSShader->Release();
    g_Layout->Release();

    ReleaseViews();

    g_DeviceContext->Release();
    g_Device->Release();

    s3eDX11Shutdown();
}

// on pause release all dx11 resources
int32 PauseCalled(void* systemData, void* userData)
{
    shutdown();

    return S3E_RESULT_SUCCESS;
}

// on resume re-create all dx11 resources
int32 UnPauseCalled(void* systemData, void* userData)
{
    init();

    return S3E_RESULT_SUCCESS;
}

// main routine
int main()
{
    init();

    // register callbacks
    s3eSurfaceRegister(S3E_SURFACE_SCREENSIZE, ResizeCalled, NULL);
    s3eDeviceRegister(S3E_DEVICE_PAUSE, PauseCalled, NULL);
    s3eDeviceRegister(S3E_DEVICE_UNPAUSE, UnPauseCalled, NULL);
    if (s3eWindowAvailable())
    {
        s3eWindowRegister(S3E_WINDOW_ENTERED_FULLSCREEN, EnterFS, NULL);
        s3eWindowRegister(S3E_WINDOW_EXITED_FULLSCREEN, ExitFS, NULL);
    }

    // main loop
    float col[] = {0.0f, 0.5f, 0.5f, 1.0f};
    while (!s3eDeviceCheckQuitRequest())
    {
        // check for Marmalade events
        s3eDeviceYield(0);

        if (!g_Active) continue;

        // clear the display
        col[0] += 1.0f/255.0f;
        if (col[0] > 1.0f)
            col[0] = 0.0f;

        g_DeviceContext->ClearRenderTargetView(g_RenderView, col);

        //update shader parameter
        phaseData[0] = fmodf(phaseData[0] + 0.2f, 2*3.141f);

        g_DeviceContext->UpdateSubresource(g_PhaseBuffer, 0, NULL, phaseData, 0, 0);

        // draw
        g_DeviceContext->VSSetShader(g_VSShader, NULL, 0);
        g_DeviceContext->PSSetShader(g_PSShader, NULL, 0);
        g_DeviceContext->Draw(3, 0);

        // check for changes to size or fullscreen
        Check();

        // swap buffers
        s3eDX11Present();
    }

    // un-register callbacks
    if (s3eWindowAvailable())
    {
        s3eWindowUnRegister(S3E_WINDOW_ENTERED_FULLSCREEN, EnterFS);
        s3eWindowUnRegister(S3E_WINDOW_EXITED_FULLSCREEN, ExitFS);
    }
    s3eDeviceUnRegister(S3E_DEVICE_PAUSE, PauseCalled);
    s3eDeviceUnRegister(S3E_DEVICE_UNPAUSE, UnPauseCalled);
    s3eSurfaceUnRegister(S3E_SURFACE_SCREENSIZE, ResizeCalled);

    // remove dx11 resources
    shutdown();

    return 0;
}
