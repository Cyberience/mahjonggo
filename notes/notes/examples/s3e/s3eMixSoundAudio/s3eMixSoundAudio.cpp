/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EMixSoundAudio S3E Mix Sound Audio Example
 *
 * The s3eMixSoundAudio example shows how it is possible to simultaneously output
 * both compressed audio file formats such as MP3 using the s3eAudio API and
 * uncompressed PCM sound samples using the s3eSound API.
 *
 * The functions used to achieve this are:
 *  - s3eSoundGetInt()
 *  - s3eSoundSetInt()
 *  - s3eSoundChannelRegister()
 *  - s3eSoundGetFreeChannel()
 *  - s3eSoundChannelPlay()
 *  - s3eSoundChannelStop()
 *  - s3eAudioIsCodecSupported()
 *  - s3eAudioPlay()
 *  - s3eAudioPause()
 *  - s3eAudioResume()
 *  - s3eAudioStop()
 *
 * The ExampleInit() function finds a free channel on which to play sound and
 * reads in a .raw file so that it can be played. It also sets up callback
 * function thats a global value indicating that a message
 * should be displayed saying playback has ended.
 * The ExampleUpdate() function will start, pause and stop sound and music.
 * Also it can change sound volume.
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eMixSoundAudioImage.png
 *
 * @include s3eMixSoundAudio.cpp
 */

#include "ExamplesMain.h"

void*   g_SoundData;                    // Buffer that holds the sound data
void*   g_BeepData;                     // Buffer that holds the beep sound data

char    g_VolumeString[50];             // Contains info about the volume for displaying

int32   g_FileSize;                     // Size of the sound file
int32   g_BeepFileSize;                 // Size of the beep sound file

int     g_Channel;                      // Free channel which is being used for the playing of the sample
int     g_BeepChannel;                  // Free channel which is being used for the playing of the beep sample

bool    g_IsPlaying;                    // True if we're playing a sample

bool    g_DisplayEndofSample = false;   // SIplay end-of-sample message yes/no?
uint64  g_EndofSampleTime = 0;          // Make sure end-of-sample message is only displayed a set amount of time

Button* g_ButtonPlay = 0;
Button* g_ButtonStop = 0;
Button* g_ButtonPlayBeep = 0;
Button* g_ButtonVolumeUp = 0;
Button* g_ButtonVolumeDown = 0;

bool    g_NeverPlayed = true;           // True if we've never played a sound

enum PlayerState
{
    kPlaying,
    kPaused,
    kStopped
};

static PlayerState g_StatusAudio = kStopped;        //player status information
static Button* g_ButtonPlayAudio = 0;
static Button* g_ButtonPauseAudio = 0;
static Button* g_ButtonResumeAudio = 0;
static Button* g_ButtonStopAudio = 0;
static s3eBool g_MP3Supported = S3E_FALSE;

/*
 * The following function increases the volume in increments of ten. It uses
 * the s3eSoundGetInt() function to first get the current volume and then it
 * uses the s3eSoundSetIn() function to set the volume to a level of ten above
 * the original.
 */
void VolumeUp()
{
    int32 TempVolume = s3eSoundGetInt(S3E_SOUND_VOLUME);

    TempVolume += 10;

    if (TempVolume >S3E_SOUND_MAX_VOLUME)
        TempVolume = S3E_SOUND_MAX_VOLUME;

    s3eSoundSetInt(S3E_SOUND_VOLUME, TempVolume);

    sprintf(g_VolumeString, "`x666666Volume level: %d", TempVolume);
}

/*
 * This function decreases the volume in increments of ten. It uses
 * the s3eSoundGetInt() function to first get the current volume and then it
 * uses the s3eSoundSetIn() function to set the volume to a level of ten below
 * the original.
 */
void VolumeDown()
{
    int32 TempVolume = s3eSoundGetInt(S3E_SOUND_VOLUME);

    TempVolume -= 10;

    if (TempVolume < 0)
        TempVolume = 0;

    s3eSoundSetInt (S3E_SOUND_VOLUME, TempVolume);
    sprintf(g_VolumeString, "`x666666Volume level: %d", TempVolume);
}

/*
 * The following function is registered as a callback function to be called
 * when the S3E_CHANNEL_END_SAMPLE event has occurred.
 */
int32 OnEndOfSample(void * _systemData, void* userData)
{
    // When the end of the sample is reached, set a global variable to cause
    // a message to be displayed.
    g_DisplayEndofSample = true;

    // ExampleUpdate will start a timer to make sure that the sample is just
    // being displayed for a set period of time
    g_EndofSampleTime = 0;

    // Return 1 to indicate to the sound system that the sample should repeat
    return 1;
}

/*
 * The following function opens the sample raw file and loads it into memory.
 * It also finds a free channel on which the sound can be played using
 * s3eSoundGetFreeChannel(). A callback for the S3E_CHANNEL_END_SAMPLE event
 * is set using the s3eSoundChannelRegister() function.
 */
void ExampleInit()
{
    // WP8.0, WS8.0, WS8.1, WP8.1 do not have ability to check if it have keyboard or not.
    // WS8.0, WS8.1 you can show virtual keyboard in any app but in most cases it does not have buttons up/down.
    // So we set g_DeviceHasKeyboard = false for this platforms.
    if (s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WS8 ||
        s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WS81 ||
        s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WP8 ||
        s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WP81 ||
        s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WIN10
       )
    {
        g_DeviceHasKeyboard = false;
    }

    // Read in sound data
    // 8000hz, 16bit Mono, 16-bit Intel PCM (LSB, MSB)
    s3eFile *fileHandle = s3eFileOpen("test.raw", "rb");

    g_FileSize = s3eFileGetSize(fileHandle);
    g_SoundData = s3eMallocBase(g_FileSize);
    memset(g_SoundData, 0, g_FileSize);
    s3eFileRead(g_SoundData, g_FileSize, 1, fileHandle);
    s3eFileClose(fileHandle);

    // Finds a free channel that we can use to play our raw file on.
    g_Channel = s3eSoundGetFreeChannel();

    // Register callback for detecting end of sample
    s3eSoundChannelRegister(g_Channel, S3E_CHANNEL_END_SAMPLE, &OnEndOfSample, NULL);

    s3eSoundSetInt(S3E_SOUND_DEFAULT_FREQ, 8000);

    fileHandle = s3eFileOpen("testLM.raw", "rb");

    // Read in beep data
    // 44100hz, 16bit Mono, 16-bit Intel PCM (LSB, MSB)
    g_BeepFileSize = s3eFileGetSize(fileHandle);
    g_BeepData = s3eMallocBase(g_BeepFileSize);
    memset(g_BeepData, 0, g_BeepFileSize);
    s3eFileRead(g_BeepData, g_BeepFileSize, 1, fileHandle);
    s3eFileClose(fileHandle);

    g_BeepChannel = g_Channel + 1;
    // Setup different rate and volume for the beep channel
    s3eSoundChannelSetInt(g_BeepChannel, S3E_CHANNEL_RATE, 24000);
    s3eSoundChannelSetInt(g_BeepChannel, S3E_CHANNEL_VOLUME, 50);

    // Initializing output strings
    sprintf(g_VolumeString, "`x666666Volume level: %d", s3eSoundGetInt(S3E_SOUND_VOLUME));

    g_ButtonPlay                = NewButton("Play Sound");
    g_ButtonStop                = NewButton("Stop Sound");
    g_ButtonPlayBeep            = NewButton("Play Beep");
    g_ButtonVolumeUp            = NewButton("Volume Up");
    g_ButtonVolumeDown          = NewButton("Volume Down");

    g_ButtonPlayAudio    = NewButton("Play Audio");
    g_ButtonPauseAudio   = NewButton("Pause Audio");
    g_ButtonResumeAudio  = NewButton("Resume Audio");
    g_ButtonStopAudio    = NewButton("Stop Audio");

    g_MP3Supported = s3eAudioIsCodecSupported(S3E_AUDIO_CODEC_MP3);

    if (g_DeviceHasKeyboard)
    {
        g_ButtonVolumeUp->m_Display     = false;
        g_ButtonVolumeDown->m_Display   = false;
    }
}

void ExampleTerm()
{
}

/*
 * The following function checks which buttons where pressed and changes the state
 * of sound playback, the volume and the pitch based on it. Playback is
 * started using the s3eSoundChannelPlay() function and stopped using the
 * s3eSoundChannelStop() function.
 */
bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    // work out which button was pressed
    if (g_DeviceHasKeyboard)
    {
        if (s3eKeyboardGetState(s3eKeyUp) & S3E_KEY_STATE_PRESSED)
            pressed = g_ButtonVolumeUp;

        if (s3eKeyboardGetState(s3eKeyDown) & S3E_KEY_STATE_PRESSED)
            pressed = g_ButtonVolumeDown;
    }

    if (pressed == g_ButtonPlay && !g_IsPlaying)
    {
        // Play the sound. Note that the 3rd paramater needs to be the number of samples to play, which
        // is the number of bytes divided by 2. A repeat count of 0 means 'play forever'.
        s3eSoundChannelPlay(g_Channel, (int16 *)g_SoundData, g_FileSize/2, 0, 0);
        g_IsPlaying = true;
        g_NeverPlayed = false;
    }

    if (pressed == g_ButtonPlayBeep)
    {
        // Play the beep once.
        s3eSoundChannelPlay(g_BeepChannel, (int16 *)g_BeepData, g_BeepFileSize/2, 1, 0);
    }

    if (pressed == g_ButtonStop && g_IsPlaying)
    {
        s3eSoundChannelStop(g_Channel);
        g_IsPlaying = false;
    }

    if (pressed == g_ButtonVolumeUp && g_IsPlaying)
        VolumeUp();

    if (pressed == g_ButtonVolumeDown && g_IsPlaying)
        VolumeDown();

    // Start the timer in order to make sure that the sample is just being displayed
    // for a set periodof time. Cannot use s3e API (e.g. s3eTimerGetMs) within the
    // callback as it is not thread-safe
    if (g_DisplayEndofSample == true && g_EndofSampleTime == 0)
        g_EndofSampleTime = s3eTimerGetMs();

    // Make sure the displaying of the "end-of-sample" message lasts only for a set amount of time
    if (s3eTimerGetMs() - g_EndofSampleTime > 2000)
    {
        g_DisplayEndofSample = false;
        g_EndofSampleTime = 0;
    }

    if (pressed == g_ButtonPlayAudio && (g_StatusAudio == kStopped))
    {
        if (s3eAudioPlay("sample.mp3", 0) == S3E_RESULT_SUCCESS)
            g_StatusAudio = kPlaying;
    }

    if (pressed == g_ButtonPauseAudio && (g_StatusAudio == kPlaying))
    {
        if (s3eAudioPause() == S3E_RESULT_SUCCESS)
            g_StatusAudio = kPaused;
    }

    if (pressed == g_ButtonResumeAudio && (g_StatusAudio == kPaused))
    {
        if (s3eAudioResume() == S3E_RESULT_SUCCESS)
            g_StatusAudio = kPlaying;
    }

    if (pressed == g_ButtonStopAudio)
    {
        s3eAudioStop();
        g_StatusAudio = kStopped;
    }

    return true;
}

const char* GetKeyName(s3eKey key)
{
    static char name[64];
    s3eKeyboardGetDisplayName(name, key, 1);
    return name;
}

/*
 * The following function displays the controls that can be used to change
 * the state of playback and the current values for the pitch and the volume.
 */
void ExampleRender()
{
    const int textHeight = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);

    // Display menu
    int x = 10;

    g_ButtonPlay->m_Enabled = !g_IsPlaying;
    g_ButtonStop->m_Enabled = g_IsPlaying;
    g_ButtonVolumeUp->m_Enabled = g_IsPlaying;
    g_ButtonVolumeDown->m_Enabled = g_IsPlaying;

    g_ButtonPlayAudio->m_Enabled = (g_StatusAudio == kStopped);
    g_ButtonPauseAudio->m_Enabled = (g_StatusAudio == kPlaying);
    g_ButtonResumeAudio->m_Enabled = (g_StatusAudio == kPaused);
    g_ButtonStopAudio->m_Enabled = (g_StatusAudio == kPlaying);

    // get a safe Y location below all the buttons
    int y = GetYBelowButtons();

    if (g_DeviceHasKeyboard)
    {
        y += textHeight;

        if (g_IsPlaying)
        {
            s3eDebugPrint(x, y, "`x666666Up/Down: Change volume", 0);
            y += textHeight;
        }
        else
        {
            s3eDebugPrint(x, y, "`xa0a0a0Up/Down: Change volume", 0);
            y += textHeight;
        }

        y += 2 * textHeight;
    }

    // Display info
    s3eDebugPrint(x, y, g_VolumeString, 0);
    y += 2 * textHeight;

    if (g_DisplayEndofSample)
    {
        s3eDebugPrint(x, y, "`x666666End of sample!", 0);
    }
}
