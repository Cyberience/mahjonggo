/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EEvent S3E Event Example
 *
 * This example explains how an event based application can be implemented.
 * For more complex examples about how to use keyboard and pointer events
 * please see @ref ExampleS3EKeyboard and @ref ExampleS3EPointerMulti
 * The main functions used to achieve this are:
 *  - s3eDeviceYield()
 *  - s3eDeviceUnYield()
 *  - s3ePointerRegister()
 *  - s3eKeyboardRegister()
 *  - s3eDebugPrint()
 *
 * Instead of having a loop with the following characteristics:
 * @code
 * loop until exit
 * {
 *    update
 *    render
 *    yield
 * }
 * @endcode
 * the application yields to the mobile operating system and reacts to events.
 * In this case we register a keyboard callback. Every time a key is pressed
 * the application unyields and performs it's operation (in this case it
 * queries the key, displays the result and then yields to the OS again).
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eEventImage.png
 *
 * @include s3eEvent.cpp
 */

#include "s3e.h"
#include <string.h>
#include <stdio.h>

void ExampleRender();
void ExampleUpdate(s3eKey keyPressed);

static int32 KeyEventHandler(void* systemData, void* userData);
static int32 PointerEventHandler(void* systemData, void* userData);

//information about errors
static char g_ErrorString[256];
//player keyboard status information
static char g_StatusString[256];

//exit button proporties
static int g_ExitX = -1;
static int g_ExitY = -1;
static int g_ExitWidth = -1;
static int g_ExitHeight = -1;

/*
 * Initialises the screen & status and registers handler for key events
 */
void ExampleInit()
{
    // Initialise string for display
    strcpy(g_ErrorString, "");
    strcpy(g_StatusString, "`x666666Press any directional key or click by mouse");

    // Update screen
    ExampleRender();

    // Register pointer event handler
    s3ePointerRegister(S3E_POINTER_BUTTON_EVENT, &PointerEventHandler, NULL);

    // Register keyboard event handler
    s3eKeyboardRegister(S3E_KEYBOARD_KEY_EVENT, &KeyEventHandler, NULL);
}

/*
 * Unregisters a callbacks for a keyboard and pointer event
 */
void ExampleTerm()
{
    s3ePointerUnRegister(S3E_POINTER_BUTTON_EVENT, &PointerEventHandler);
    s3eKeyboardUnRegister(S3E_KEYBOARD_KEY_EVENT, &KeyEventHandler);
}

static void ExitButtonRender()
{
    static const bool disableExit =
        s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WS8 ||
        s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WS81 ||
        s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WIN10;

    if (!disableExit)
    {
        g_ExitWidth = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_WIDTH);
        g_ExitHeight = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);

        const char* text = "`x666666Exit";
        g_ExitWidth *= strlen(text) - 8;
        g_ExitX = s3eSurfaceGetInt(S3E_SURFACE_WIDTH) - g_ExitWidth;
        g_ExitY = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT) - g_ExitHeight;

        s3eDebugPrint(g_ExitX, g_ExitY, text, 0);
    }
}

/*
 * Main entry point.
 */
int main()
{
    ExampleInit();

    // Yield to the operating system until the user/loader/OS has requested a shutdown
    while (!s3eDeviceCheckQuitRequest())
    {
        // Yield until unyield is called or a quit request is recieved
        s3eDeviceYield(S3E_DEVICE_YIELD_FOREVER);
    }

    strcpy(g_ErrorString, "`x666666Exiting example");

    ExampleTerm();
    return 0;
}

/*
 * Key event handler:
 * systemData contains a structure of type s3eKeyboardEvent which contains
 * information about the pressed key.
 * Note the generic makeup of the callback function, which always supplies
 * the parameters:
 * systemData - callback-specific system supplied data.
 * userData - data supplied by user at callback registration (in this case
 * NULL) and returns int32 as success staus.
 */
static int32 KeyEventHandler(void* systemData, void* userData)
{
    // Read in the type of key that has been pressed
    s3eKeyboardEvent* pkeyPressed = (s3eKeyboardEvent*)systemData;
    s3eKey keyPressed = (*pkeyPressed).m_Key;

    // Update internal state
    if (keyPressed == s3eKeyAbsLeft)
        strcpy(g_StatusString, "`x666666Left key pressed");
    else if (keyPressed == s3eKeyAbsUp)
        strcpy(g_StatusString, "`x666666Up key pressed");
    else if (keyPressed == s3eKeyAbsRight)
        strcpy(g_StatusString, "`x666666Right key pressed");
    else if (keyPressed == s3eKeyAbsDown)
        strcpy(g_StatusString, "`x666666Down key pressed");
    else if (keyPressed == s3eKeyEsc || keyPressed == s3eKeyLSK)
        // An exit request has occurred
        s3eDeviceRequestQuit();

    // Update screen representation
    ExampleRender();

    // Wake up and take control from the OS
    s3eDeviceUnYield();
    return 0;
}

/*
 * Pointer event handler:
 * systemData contains a structure of type s3ePointerEvent which contains
 * information about the pressed key.
 * Note the generic makeup of the callback function, which always supplies
 * the parameters:
 * systemData - callback-specific system supplied data.
 * userData - data supplied by user at callback registration (in this case
 * NULL) and returns int32 as success staus.
 */
static int32 PointerEventHandler(void* systemData, void* userData)
{
    // Read in the type of pointer button that has been queried
    s3ePointerEvent* pointerEvent = (s3ePointerEvent*)systemData;
    s3ePointerButton pointerButton = (*pointerEvent).m_Button;

    int x = 0;
    int y = 0;

    // Update internal state
    if (pointerButton == S3E_POINTER_BUTTON_LEFTMOUSE)
    {
        x = pointerEvent->m_x;
        y = pointerEvent->m_y;
        sprintf(g_StatusString, "`x666666Left mouse click (or tap on touchpad). x=%d, y=%d", x, y);
    }
    else if (pointerButton == S3E_POINTER_BUTTON_RIGHTMOUSE)
        sprintf(g_StatusString, "`x666666Right mouse click. x=%d, y=%d", pointerEvent->m_x, pointerEvent->m_y);
    else if (pointerButton == S3E_POINTER_BUTTON_MIDDLEMOUSE)
    {
        x = pointerEvent->m_x;
        y = pointerEvent->m_y;
        sprintf(g_StatusString, "`x666666Middle mouse click. x=%d, y=%d", pointerEvent->m_x, pointerEvent->m_y);
    }
    else if (pointerButton == S3E_POINTER_BUTTON_MOUSEWHEELUP)
        strcpy(g_StatusString, "`x666666Mouse wheel up.");
    else if (pointerButton == S3E_POINTER_BUTTON_MOUSEWHEELDOWN)
        strcpy(g_StatusString, "`x666666Mouse wheel down.");

    if (pointerEvent->m_Pressed == 0 && (x != 0 || y != 0))
        if (x >= g_ExitX && x <= g_ExitX + g_ExitWidth && y >= g_ExitY && y <= g_ExitY + g_ExitHeight)
            s3eDeviceRequestQuit();

    // Update screen representation
    ExampleRender();

    // Wake up and take control from the OS
    s3eDeviceUnYield();
    return 0;
}

/*
 * Update screen representation
 */
void ExampleRender()
{
    const int textWidth = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_WIDTH);
    const int textHeight = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);
    const int width = s3eSurfaceGetInt(S3E_SURFACE_WIDTH);
    const int height = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT);

    // Clear to white
    s3eSurfaceClear(0xff, 0xff, 0xff);

    ExitButtonRender();

    // Print centered information about keypresses
    if (!s3eKeyboardGetInt(S3E_KEYBOARD_HAS_DIRECTION))
        strcpy(g_ErrorString, "`x666666Device has no direction keys.");

    s3eDebugPrint((width - (strlen(g_ErrorString) - 8) * textWidth) / 2, 8 * textHeight, g_ErrorString, 1);
    s3eDebugPrint((width - (strlen(g_StatusString) - 8) * textWidth) / 2, 10 * textHeight, g_StatusString, 1);

    static const bool disableExit =
        s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WS8 ||
        s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WS81 ||
        s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WIN10;

    if (!disableExit)
    {
        s3eDebugPrint((width - strlen("Press Esc or LS to Exit") * textWidth) / 2,
            (height - 1 * textHeight), "`xfb0b0cPress Esc or LSK to Exit", 0);
    }

    // Redraw screen
    s3eSurfaceShow();
}
