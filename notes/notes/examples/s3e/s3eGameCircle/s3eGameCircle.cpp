/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EGameCircle S3E Amazon GameCircle
 *
 * The following example demonstrates usage of s3eGameCircle extension
 *
 * @include s3eGameCircle.cpp
 */

#include "ExamplesMain.h"
#include "s3eGameCircle.h"
#include "s3eOSReadString.h"

#include <time.h>
#include <stdlib.h>

const char* g_TestAchievementID = "your achievement ID";
const char* g_TestLeaderboardID = "your leaderboard ID";

const char* g_WhisperField = "FieldName";
const char* g_WhisperString = "some user data";

Button* g_ButtonShowSignIn;
Button* g_ButtonShowAchievements;
Button* g_ButtonUpdateAchievement;
Button* g_ButtonGetAchievementsList;
Button* g_ButtonGetAchievement;
Button* g_ButtonShowLeaderboards;
Button* g_ButtonGetLeaderboardsList;
Button* g_ButtonSubmitScore;
Button* g_ButtonWhispersyncSet;
Button* g_ButtonWhispersyncGet;
Button* g_ButtonWhispersyncListAdd;
Button* g_ButtonWhispersyncListGet;
Button* g_ButtonWhispersyncSynchronize;


// Callback functions.
int32 onGetDataList(void* system, void* user)
{
    s3eGameCircleWhispersyncDataList* list = (s3eGameCircleWhispersyncDataList*)system;
    if (!list)
        return -1;

    if (list->m_Type == S3E_GAMEDATA_LATEST_NUMBER_LIST)
    {
        double* array = (double*)list->m_Items;
        for (int i = 0; i < list->m_Count; ++i)
        {
            AppendMessage("item %f", array[i]);
        }
    }

    AppendMessage("Got list size %i. List type %i", list->m_Count, list->m_Type);

    return 0;
}

int32 onGetPlayerAlias(void* system, void* user)
{
    if (system)
        AppendMessage("Player alias: %s", (char*)system);

    return 0;
}

int32 onGetPlayerID(void* system, void* user)
{
    if (system)
        AppendMessage("Player ID: %s", (char*)system);

    return 0;
}

int32 onInitialize(void* system, void* user)
{
    AppendMessage("Initialization status: %d", system);
    if ((s3eGameCircleStatus)(size_t)system == S3E_GAMECIRCLE_SERVICE_CONNECTED)
    {
        // Allow user to interact
        g_ButtonShowSignIn->m_Enabled = true;
        g_ButtonShowAchievements->m_Enabled = true;
        g_ButtonGetAchievementsList->m_Enabled = true;
        g_ButtonGetAchievement->m_Enabled = true;
        g_ButtonUpdateAchievement->m_Enabled = true;
        g_ButtonShowLeaderboards->m_Enabled = true;
        g_ButtonGetLeaderboardsList->m_Enabled = true;
        g_ButtonSubmitScore->m_Enabled = true;
        g_ButtonWhispersyncSet->m_Enabled = true;
        g_ButtonWhispersyncGet->m_Enabled = true;
        g_ButtonWhispersyncListAdd->m_Enabled = true;
        g_ButtonWhispersyncListGet->m_Enabled  = true;

        g_ButtonWhispersyncSynchronize->m_Enabled = true;

        // Following calls are just for testing.
        s3eGameCircleSetPopUpLocation(S3E_GAMECIRCLE_LOCATION_TOP_CENTER);
        s3eGameCircleWhispersyncSynchronize();
    }
    return 0;
}

int32 onAchievementUpdated(void* system, void* user)
{
    if (system)
    {
        s3eGameCircleAchievementUpdateResult result;
        memcpy(&result, system, sizeof(result));
        AppendMessage("Achievement update status: %i; Achievement newlyUnloked: %s", result.m_Result, (result.m_NewlyUnlocked) ? "true" : "false");
    }

    return 0;
}

int32 onGetAchievementsList(void* system, void* user)
{
    if (system)
    {
        s3eGameCircleAchievementList* list = (s3eGameCircleAchievementList*)system;
        for (int i = list->m_Count -1; i >= 0; i--)
        {
            AppendMessage("Achievement %i id: %s", i + 1, list->m_AchievementsList[i].m_ID);
        }
        AppendMessage("Got list of %i achievements", list->m_Count);
    }

    return 0;
}

int32 onGetLeaderboarsList(void* system, void* user)
{
    if (system)
    {
        s3eGameCircleLeaderboardList* list = (s3eGameCircleLeaderboardList*)system;
        for (int i = list->m_Count -1; i >= 0; i--)
        {
            AppendMessage("Achievement %i id: %s", i + 1, list->m_LeaderboardsList[i].m_ID);
        }
        AppendMessage("Got list of %i leaderboards", list->m_Count);
    }

    return 0;
}

int32 onScoreSubmitted(void* system, void* user)
{
    if (system)
    {
        AppendMessage("New score submited");
    }

    return 0;
}

void ExampleInit()
{
    InitMessages(25, 256);

    g_ButtonShowSignIn              = NewButton("Show GameCircle");
    g_ButtonShowAchievements        = NewButton("Show achievements");
    g_ButtonUpdateAchievement       = NewButton("Update achievement");
    g_ButtonGetAchievementsList     = NewButton("Get achievements list");
    g_ButtonGetAchievement          = NewButton("Get achievement");
    g_ButtonShowLeaderboards        = NewButton("Show leaderboards");
    g_ButtonGetLeaderboardsList     = NewButton("Get leaderboards list");
    g_ButtonWhispersyncSet          = NewButton("Whispersync set string");
    g_ButtonWhispersyncGet          = NewButton("Whispersync get string");
    g_ButtonWhispersyncListAdd      = NewButton("Whispersync add to list");
    g_ButtonWhispersyncListGet      = NewButton("Whispersync get list");
    g_ButtonSubmitScore             = NewButton("Submit Score");
    g_ButtonWhispersyncSynchronize  = NewButton("Force Sync");

    g_ButtonShowAchievements->m_Enabled = false;
    g_ButtonUpdateAchievement->m_Enabled = false;
    g_ButtonGetAchievementsList->m_Enabled = false;
    g_ButtonGetAchievement->m_Enabled = false;
    g_ButtonShowLeaderboards->m_Enabled = false;
    g_ButtonGetLeaderboardsList->m_Enabled = false;
    g_ButtonShowSignIn->m_Enabled = false;
    g_ButtonSubmitScore->m_Enabled = false;
    g_ButtonWhispersyncSet->m_Enabled = false;
    g_ButtonWhispersyncGet->m_Enabled = false;
    g_ButtonWhispersyncSynchronize->m_Enabled = false;
    g_ButtonWhispersyncListAdd->m_Enabled = false;
    g_ButtonWhispersyncListGet->m_Enabled = false;

    SetButtonScale(1);

    if (!s3eGameCircleAvailable())
    {
        AppendMessageColour(RED, "s3eGameCircle extension is unavailable");
        return;
    }

    // Register callback functions.
    s3eGameCircleRegister(S3E_GAMECIRCLE_PLAYER_ALIAS, &onGetPlayerAlias, NULL);
    s3eGameCircleRegister(S3E_GAMECIRCLE_PLAYER_ID, &onGetPlayerID, NULL);
    s3eGameCircleRegister(S3E_GAMECIRCLE_INITIALIZATION, &onInitialize, NULL);
    s3eGameCircleRegister(S3E_GAMECIRCLE_ACHIEVEMENT_UPDATE_PROGRESS, &onAchievementUpdated, NULL);
    s3eGameCircleRegister(S3E_GAMECIRCLE_GET_ACHIEVEMENTS, &onGetAchievementsList, NULL);
    s3eGameCircleRegister(S3E_GAMECIRCLE_GET_LEADERBOARDS, &onGetLeaderboarsList, NULL);
    s3eGameCircleRegister(S3E_GAMECIRCLE_LEADERBOARD_SUBMIT_SCORE, &onScoreSubmitted, NULL);
    s3eGameCircleRegister(S3E_GAMECIRCLE_WHISPERSYNC_DATA_LIST, &onGetDataList, NULL);

    s3eGameCircleInitialize(S3E_GAMECIRCLE_ACHIEVEMENTS | S3E_GAMECIRCLE_LEADERBOARDS | S3E_GAMECIRCLE_WHISPERSYNC);
}

void ExampleTerm()
{
    if (s3eGameCircleAvailable())
    {
        s3eGameCircleTerminate();
    }
}

bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();
    if (pressed == g_ButtonShowAchievements)
    {
        s3eGameCircleShowAchievementsOverlay();
    }
    else if (pressed == g_ButtonUpdateAchievement)
    {
        const char* achievePercentsText = s3eOSReadStringUTF8WithDefault("Enter percents", "15");
        if (achievePercentsText == NULL)
            return true;

        int percent = atoi(achievePercentsText);
        s3eGameCircleUpdateAchievementProgress(s3eOSReadStringUTF8WithDefault("Enter achievement ID to update", g_TestAchievementID), percent);
    }
    else if (pressed == g_ButtonGetAchievementsList)
    {
        s3eGameCircleGetAllAchievements();
    }
    else if (pressed == g_ButtonGetAchievement)
    {
        s3eGameCircleGetAchievement(s3eOSReadStringUTF8WithDefault("Enter achievement ID", g_TestAchievementID));
    }
    else if (pressed == g_ButtonShowLeaderboards)
    {
        s3eGameCircleShowLeaderboardOverlay(NULL);
    }
    else if (pressed == g_ButtonGetLeaderboardsList)
    {
        s3eGameCircleGetLeaderboardsList();
    }
    else if (pressed == g_ButtonShowSignIn)
    {
        s3eGameCircleShowSignInPage();
    }
    else if (pressed == g_ButtonSubmitScore)
    {
        const char* scoresTxt = s3eOSReadStringUTF8WithDefault("Enter a scores to submit", "10");
        if (scoresTxt == NULL)
            return true;
        int scores = atoi(scoresTxt);
        s3eGameCircleSubmitScore(s3eOSReadStringUTF8WithDefault("Enter leaderboard ID to update", g_TestLeaderboardID), scores);
    }
    else if (pressed == g_ButtonWhispersyncSet)
    {
        s3eGameCircleWhispersyncSetLatestString(g_WhisperField, s3eOSReadStringUTF8WithDefault("Enter data to save", g_WhisperString));
    }
    else if (pressed == g_ButtonWhispersyncGet)
    {
        const char* data = s3eGameCircleWhispersyncGetLatestString(g_WhisperField);
        AppendMessage("Whispersync got data: %s", data);
    }
    else if (pressed == g_ButtonWhispersyncSynchronize)
    {
        s3eGameCircleWhispersyncSynchronize();
    }
    else if (pressed == g_ButtonWhispersyncListAdd)
    {
        const char* valueTxt = s3eOSReadStringUTF8WithDefault("Enter a number", "10");
        if (valueTxt == NULL)
            return true;

        double value = atof(valueTxt);
        s3eGameCircleWhispersyncSetLatestNumberList(s3eOSReadStringUTF8WithDefault("Enter list name", "some list"), &value);
    }
    else if (pressed == g_ButtonWhispersyncListGet)
    {
        s3eGameCircleWhispersyncGetLatestNumberList(s3eOSReadStringUTF8WithDefault("Enter list name", "some list"));
    }

    return true;
}

void ExampleRender()
{
    ButtonsRender();
    PrintMessages(10, GetYBelowButtons() + 20);
}
