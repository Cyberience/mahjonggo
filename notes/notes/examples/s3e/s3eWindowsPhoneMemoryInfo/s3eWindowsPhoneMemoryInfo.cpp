/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EWindowsPhoneMemoryInfo S3E Windows Phone Memory Info Example
 *
 * The following demonstrates use of s3eWindowsPhoneMemoryInfo API to display
 * current device memory information.
 *
 * The functions required to achieve this are:
 * - s3eWindowsPhoneMemoryInfoAvailable()
 * - s3eWindowsPhoneMemoryInfoGetApplicationCurrentMemoryUsage()
 * - s3eWindowsPhoneMemoryInfoGetApplicationMemoryUsageLimit()
 * - s3eWindowsPhoneMemoryInfoGetApplicationPeakMemoryUsage()
 * - s3eWindowsPhoneMemoryInfoGetDeviceTotalMemory()
 * - s3eWindowsPhoneMemoryInfoGetApplicationWorkingSetLimit()
 * - s3eWindowsPhoneMemoryInfoIsLowMemDevice()
 *
 * @include s3eWindowsPhoneMemoryInfo.cpp
 */

#include "ExamplesMain.h"
#include "s3eWindowsPhoneMemoryInfo.h"

static s3eBool s_IsLowMemDevice = false;
static int64 s_ApplicationCurrentMemoryUsage = 0;
static int64 s_ApplicationMemoryUsageLimit = 0;
static int64 s_ApplicationPeakMemoryUsage = 0;
static int64 s_DeviceTotalMemory = 0;
static int64 s_ApplicationWorkingSetLimit = 0;

static const char* s_LowMemMsg = "Application is running on lower-memory device";
static const char* s_HighMemMsg = "Application is running on higher-memory device";

void ExampleInit()
{
    if (s3eWindowsPhoneMemoryInfoAvailable())
        s_IsLowMemDevice = s3eWindowsPhoneMemoryInfoIsLowMemDevice();
}

void ExampleTerm()
{
}

bool ExampleUpdate()
{
    s_ApplicationCurrentMemoryUsage = s3eWindowsPhoneMemoryInfoGetApplicationCurrentMemoryUsage() / 1024;
    s_ApplicationMemoryUsageLimit = s3eWindowsPhoneMemoryInfoGetApplicationMemoryUsageLimit() / 1024;
    s_ApplicationPeakMemoryUsage = s3eWindowsPhoneMemoryInfoGetApplicationPeakMemoryUsage() / 1024;
    s_DeviceTotalMemory = s3eWindowsPhoneMemoryInfoGetDeviceTotalMemory() / 1024;
    s_ApplicationWorkingSetLimit = s3eWindowsPhoneMemoryInfoGetApplicationWorkingSetLimit() / 1024;
    return true;
}

/*
 * The following function outputs current device memory information.
 * The s3eDebugPrint() function to print the strings.
 */
void ExampleRender()
{
    // Print results
    if (!s3eWindowsPhoneMemoryInfoAvailable())
    {
        s3eDebugPrint(5, 100, "`xff0000Windows Phone Memory Info API isn't available on this device.", 1);
        return;
    }
    int lineHeight = s3eDebugGetInt(S3E_DEBUG_FONT_HEIGHT) * 2;
    int y = lineHeight / 4;
    s3eDebugPrintf(5, y, S3E_TRUE, "`x000000ApplicationCurrentMemoryUsage - %dkB", s_ApplicationCurrentMemoryUsage);
    y += lineHeight;
    s3eDebugPrintf(5, y, S3E_TRUE, "`x000000ApplicationMemoryUsageLimit - %dkB", s_ApplicationMemoryUsageLimit);
    y += lineHeight;
    s3eDebugPrintf(5, y, S3E_TRUE, "`x000000ApplicationPeakMemoryUsage - %dkB", s_ApplicationPeakMemoryUsage);
    y += lineHeight;
    s3eDebugPrintf(5, y, S3E_TRUE, "`x000000DeviceTotalMemory - %dkB", s_DeviceTotalMemory);
    y += lineHeight;
    s3eDebugPrintf(5, y, S3E_TRUE, "`x000000ApplicationWorkingSetLimit - %dkB", s_ApplicationWorkingSetLimit);
    y += lineHeight;
    s3eDebugPrintf(5, y, S3E_TRUE, "`x000000%s", s_IsLowMemDevice ? s_LowMemMsg : s_HighMemMsg);
}
