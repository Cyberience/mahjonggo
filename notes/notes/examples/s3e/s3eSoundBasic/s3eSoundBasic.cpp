/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3ESoundBasic S3E Sound Basic Example
 *
 * The following example uses S3E's sound functionality to play a raw
 * file and allow you to control the volume using the keyboard.
 *
 * The main functions used to achieve this are:
 *  - s3eFileOpen()
 *  - s3eFileGetSize()
 *  - s3eMallocBase()
 *  - s3eFileRead()
 *  - s3eSoundGetFreeChannel()
 *  - s3eSoundSetInt()
 *  - s3eSoundChannelPlay()
 *  - s3eSoundChannelStop()
 *  - s3eSoundGetInt()
 *  - s3eSoundSetInt()
 *
 * This example loads a sound file and reserves a sound channel for playback.
 * Based on which buttons are pressed you can change playback and the volume.
 *
 * The volume is changed by first getting the value representing the volume
 * using the s3eSoundGetInt() function and passing it the S3E_SOUND_VOLUME
 * value, and then changing the value by the required amount and passing it
 * to the s3eSoundSetInt() function.
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eSoundBasicImage.png
 *
 * @include s3eSoundBasic.cpp
 */


#include "ExamplesMain.h"

static int16*   g_SoundBuffer;
static char     g_VolumeString[256];
static int32    g_FileSize;
static int      g_Channel;
static bool     g_IsPlaying; // True if we're playing a sample
static Button*  g_ButtonPlay = 0;
static Button*  g_ButtonStop = 0;
static Button*  g_ButtonVolumeUp = 0;
static Button*  g_ButtonVolumeDown = 0;

/*
 * This function increases the volume.
 */
void VolumeUp()
{
    int32 temp = s3eSoundGetInt(S3E_SOUND_VOLUME);

    temp += 10;

    if (temp >S3E_SOUND_MAX_VOLUME)
        temp = S3E_SOUND_MAX_VOLUME;

    s3eSoundSetInt(S3E_SOUND_VOLUME, temp);

    sprintf(g_VolumeString, "`x666666Volume level: %d", temp);
}

/*
 * This function decreases the volume.
 */
void VolumeDown()
{
    int32 temp = s3eSoundGetInt(S3E_SOUND_VOLUME);

    temp -= 10;

    if (temp < 0)
        temp = 0;

    s3eSoundSetInt(S3E_SOUND_VOLUME, temp);

    sprintf(g_VolumeString, "`x666666Volume level: %d", temp);
}


/*
 * The following function opens and reads the specified file into memory. It
 * also reserves a channel on which to play sound and sets the frequency at
 * which the sound will be played.
 */
void ExampleInit()
{
    // Read in sound data
    s3eFile *fileHandle = s3eFileOpen("test.raw", "rb");

    g_FileSize = s3eFileGetSize(fileHandle);
    g_SoundBuffer = (int16*)s3eMallocBase(g_FileSize);
    memset(g_SoundBuffer, 0, g_FileSize);
    s3eFileRead(g_SoundBuffer, g_FileSize, 1, fileHandle);
    s3eFileClose(fileHandle);

    // Finds a free channel that we can use to play our raw file on.
    g_Channel = s3eSoundGetFreeChannel();

    // Setting default frequency at which all channels will play at, in Hz.
    s3eSoundSetInt(S3E_SOUND_DEFAULT_FREQ, 8000);

    g_ButtonPlay                = NewButton("Play");
    g_ButtonStop                = NewButton("Stop");
    g_ButtonVolumeUp            = NewButton("Volume Up");
    g_ButtonVolumeDown          = NewButton("Volume Down");
}

void ExampleTerm()
{
}

/*
 * The following function checks which buttons are pressed and changes sound playback
 * and the volume accordingly. The volume is controlled using the volumeUp()
 * and volumeDown() functions.
 */
bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    if (pressed == g_ButtonPlay && !g_IsPlaying)
    {
        // Play the sound. Note that the 3rd paramater needs to be the number of samples to play, which
        // is the number of bytes divided by 2. A repeat count of 0 means 'play forever'.
        if (s3eSoundChannelPlay(g_Channel, g_SoundBuffer, g_FileSize/2, 0, 0) == S3E_RESULT_ERROR)
            sprintf(g_VolumeString, "Error in s3eSoundChannelPlay");
        else
            g_IsPlaying = true;
    }

    if (pressed == g_ButtonStop && g_IsPlaying)
    {
        if (s3eSoundChannelStop(g_Channel) == S3E_RESULT_ERROR)
            sprintf(g_VolumeString, "Error in s3eSoundChannelStop");
        else
            g_IsPlaying = false;
    }

    // Press up to increase volume
    if (pressed == g_ButtonVolumeUp && g_IsPlaying)
        VolumeUp();

    // Press down to decrease volume
    if (pressed == g_ButtonVolumeDown && g_IsPlaying)
        VolumeDown();

    return true;
}

/*
 * The following function displays the details of sound playback on screen by
 * indicating the controls that can be used to change the state of playback
 * and volume.
 */
void ExampleRender()
{
    const int textHeight = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);

    // Display menu
    int y = GetYBelowButtons() + 2 * textHeight;
    int x = 10;

    g_ButtonPlay->m_Enabled = !g_IsPlaying;
    g_ButtonStop->m_Enabled = g_IsPlaying;
    g_ButtonVolumeUp->m_Enabled = g_IsPlaying;
    g_ButtonVolumeDown->m_Enabled = g_IsPlaying;

    // Display info
    s3eDebugPrint(x, y, g_VolumeString, 0);
    y += 2 * textHeight;

    if (g_IsPlaying)
        s3eDebugPrint(10, y, "`x666666Sound is playing", 0);
}
