/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3ECompressionBasic S3E Compression Basic Example
 *
 * This example decompresses a text file and prints the decompressed text to
 * screen.
 *
 * The main functions used to achieve this are:
 *  - s3eCompressionDecomp()
 *  - s3eFree()
 *
 * The ExampleInit() function opens and reads in the compressed contents of the
 * specified file. This means the file must first be opened and loaded into a
 * buffer, and this is done using S3E IO functionality.
 *
 * @note For more information on using the S3E IO functionality, see S3E File
 * IO or the "File System" section of the S3E API Reference documentation.
 *
 * The ExampleUpdate() function waits for a button to be pressed before calling
 * s3eCompressionDecomp(). This function decompresses the data contained in an
 * input buffer and then writes it to an output buffer.
 *
 * ExampleUpdate() outputs the example controls and the contents of the
 * the specified file once it has been decompressed.
 *
 * ExampleTerm() uses the s3eFree() function to free the buffers used in
 * the application before it exits.
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eCompressionBasicImage.png
 *
 * @include s3eCompressionBasic.cpp
 */

#include "ExamplesMain.h"

static void*   g_BufferPointer;                // Contains compressed data
static char*   g_OutBufferPointer;             // Contains decompressed data
static char*   g_Output;                       // Contains colour specifier and decompressed data
static char    g_ErrorString[256] = {'\0'};    // Contains error info for displaying

static int32   g_FileSize;                     // Size of comressed data file
static bool    g_OutputContents = false;       // Set when output needs to be printed
static uint32  g_OutputDataSize = 1000;        // Size of uncompressed data
static Button* g_ButtonDecompress = 0;

static const char* kTestFile = "test.txt.gz";

/*
 * The following function opens and reads in the compressed contents of the
 * specified file.
 */
void ExampleInit()
{
    // Read in compressed file
    s3eFile * FileHandle = s3eFileOpen(kTestFile, "rb");
    if (FileHandle == NULL)
    {
        sprintf(g_ErrorString, "Failed to open %s", kTestFile);
    }
    else
    {
        g_FileSize = s3eFileGetSize(FileHandle);

        if (g_FileSize == -1)
        {
            sprintf(g_ErrorString, "s3eFileGetSize of %s failed(%d)", kTestFile, (int) s3eFileGetError());
        }
        else
        {
            g_BufferPointer = s3eMallocBase(g_FileSize);

            // Reading in compressed data
            s3eFileRead(g_BufferPointer, g_FileSize, 1, FileHandle);
        }

        s3eFileClose(FileHandle);
    }

    g_OutBufferPointer = (char *)s3eMallocBase(g_OutputDataSize);
    g_Output = (char *)s3eMallocBase(g_OutputDataSize+8);

    g_ButtonDecompress = NewButton("Decompress file");
    if (strlen(g_ErrorString) > 0)
    {
        // if errors in init, show button disabled.s
        g_ButtonDecompress->m_Enabled = false;
    }
}

/*
 * The following function uses the s3eFree() function to free the buffers used
 * in the applictation before it exits.
 */
void ExampleTerm()
{
    // Freeing data buffers
    s3eFree(g_OutBufferPointer);
    s3eFree(g_BufferPointer);
    s3eFree(g_Output);
}

/*
 * The following function checks the state of the keyboard for input. If the
 * s3eKey1 has been pressed, it creates a buffer for the decompressed data.
 * The s3eCompressionDecomp() function is called, which takes the compressed
 * data, decompresses it and puts it into the new output buffer.
 *
 * If there is no keyboard, the pointer state is checked instead.
 */
bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    if ((pressed == g_ButtonDecompress) && g_OutputContents == false)
    {
        // Initialise buffer space for decompressed data
        memset(g_OutBufferPointer, 0, g_OutputDataSize);

        // The s3eCompressionDecomp() function is used to decompress a compressed txt
        // file and place the contents of that file into a buffer.
        if (s3eCompressionDecomp(g_BufferPointer, g_FileSize, (void**)&g_OutBufferPointer, &g_OutputDataSize, S3E_COMPRESSION_ALG_GZIP) == S3E_RESULT_ERROR)
        {
            // An error occurred, update error string for displaying
            strcpy(g_ErrorString, s3eCompressionGetErrorString());
        }
        else
        {
            memset(g_Output, 0, g_OutputDataSize+8);

            // decompression successful
            g_OutputContents = true;

            strcpy(g_Output, "`x666666");
            strcat(g_Output, g_OutBufferPointer);
        }
    }

    return true;
}

/*
 * The following function outputs the example controls and the contents of the
 * specified file (should it have been decompressed) to the screen.
 */
void ExampleRender()
{
    const int textHeight = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);
    int ypos = GetYBelowButtons() + textHeight * 2;

    // Display contents
    if (g_OutputContents)
    {
        s3eDebugPrintf(10, ypos, S3E_TRUE, "`x666666Contents:");

        ypos += textHeight;

        s3eDebugPrintf(10, ypos, S3E_TRUE, "%s", g_Output);
    }

    if (strlen(g_ErrorString) > 0)
    {
       ypos += textHeight * 3;

       s3eDebugPrintf(10, ypos, S3E_TRUE, "`xff0000%s", g_ErrorString);
    }
}
