/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

#include "ExamplesMain.h"
#include "s3eWebView.h"

/**
 *
 * @page ExampleS3EWebViewBasic S3E Web View Basic Example
 *
 * The following example uses the S3E Web View functionality to manipulate
 * a web view, rendering an URL from the internet on the device web browser,
 * embedded within an s3e application.
 *
 * The functions used to achieve this are:
 * - s3eWebViewAvailable()
 * - s3eWebViewCreateModal()
 * - s3eWebViewCreate()
 * - s3eWebViewShow()
 * - s3eWebViewNavigate()
 * - s3eWebViewHide()
 * - s3eWebViewDestroy()
 *
 * This example uses the simple full screen modal capabilities of s3eWebView.
 * On all platforms there will be a method for the user to signal that they
 * want to return to the application, usually by a navigation bar or back
 * button. Until this event, the s3eWebViewModal method will block the
 * application thread.
 *
 * It also uses the show and hide methods to render a non full screen web view
 * over an s3e Surface view. Multiple web views can be used simultaneously in
 * this way.
 * See s3eWebViewAdvanced for an example of this, rendering on top of an IwGx
 * (GL) surface.
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eWebViewBasicImage.png
 *
 * @include s3eWebViewBasic.cpp
 */

static const char* g_URL = "https://www.madewithmarmalade.com/";
static Button* g_ButtonModal = 0;
static Button* g_ButtonShow = 0;
static Button* g_ButtonHide = 0;
static Button* g_ButtonDestroy = 0;
static s3eResult g_Result = S3E_RESULT_SUCCESS;
static s3eWebView* g_WebView = NULL;

int32 ScreenSizeChangeCallback(void*, void*)
{
    if (g_WebView)
    {
        int y = GetYBelowButtons() + 10;
        s3eWebViewResize(
            g_WebView,
            0,
            y,
            s3eSurfaceGetInt(S3E_SURFACE_WIDTH),
            s3eSurfaceGetInt(S3E_SURFACE_HEIGHT) - y - 10);
    }
    return 0;
}


void ExampleInit()
{
    // Create the UI.
    g_ButtonModal = NewButton("Modal Web View");
    g_ButtonShow  = NewButton("Show Web View");
    g_ButtonHide  = NewButton("Hide Web View");
    g_ButtonDestroy  = NewButton("Destroy Web View");

    // Check for the availability of the Web View functionality.
    // If it is available then enable the button.
    bool avail = (s3eWebViewAvailable() != 0);
    g_ButtonHide->m_Enabled = false;
    g_ButtonDestroy->m_Enabled = false;
    g_ButtonModal->m_Enabled = avail;
    g_ButtonShow->m_Enabled = avail;

    s3eSurfaceRegister(S3E_SURFACE_SCREENSIZE, ScreenSizeChangeCallback, NULL);
}

void ExampleTerm()
{
    s3eSurfaceUnRegister(S3E_SURFACE_SCREENSIZE, ScreenSizeChangeCallback);
    if (g_WebView)
    {
        s3eWebViewDestroy(g_WebView);
        g_WebView = NULL;
    }
}

bool ExampleUpdate()
{
    // If the button has been selected, try to execute the OS command
    // and store the result.
    if (GetSelectedButton() == g_ButtonModal)
    {
        g_Result = s3eWebViewCreateModal(g_URL);
    }
    else if (GetSelectedButton() == g_ButtonShow)
    {
        if (!g_WebView)
        {
            g_WebView = s3eWebViewCreate(true);
            s3eWebViewNavigate(g_WebView, g_URL);
        }

        int y = GetYBelowButtons() + 15;
        s3eWebViewShow(g_WebView, 0, y, s3eSurfaceGetInt(S3E_SURFACE_WIDTH), s3eSurfaceGetInt(S3E_SURFACE_HEIGHT) - y - 15);

        g_ButtonShow->m_Enabled = false;
        g_ButtonHide->m_Enabled = true;
        g_ButtonDestroy->m_Enabled = true;
    }
    else if (GetSelectedButton() == g_ButtonHide)
    {
        s3eWebViewHide(g_WebView);
        g_ButtonShow->m_Enabled = true;
        g_ButtonHide->m_Enabled = false;
    }
    else  if (GetSelectedButton() == g_ButtonDestroy)
    {
        s3eWebViewDestroy(g_WebView);
        g_ButtonDestroy->m_Enabled = false;
        g_ButtonHide->m_Enabled = false;
        g_ButtonShow->m_Enabled = true;
        g_WebView = NULL;
    }
    return true;
}

/*
 * The following function display the user's prompt and any error messages.
 * The s3eDebugPrint() function to print the strings.
 */
void ExampleRender()
{
    // Print results
    if (!s3eWebViewAvailable())
    {
        s3eDebugPrint(50, GetYBelowButtons(), "`xff0000Web view isn't available on this device.", 1);
    }

    if (g_Result == S3E_RESULT_ERROR)
    {
        s3eDebugPrint(50, GetYBelowButtons()    , "`xff0000Failed to open the URL.", 1);
    }
}
