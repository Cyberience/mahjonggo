/*
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This document is protected by copyright, and contains information
 * proprietary to Marmalade.
 *
 * This file consists of source code released by Marmalade under
 * the terms of the accompanying End User License Agreement (EULA).
 * Please do not use this program/source code before you have read the
 * EULA and have agreed to be bound by its terms.
 */

/**
 * @page ExampleS3EGooglePlayServices S3E Google Play Services Example
 * The following example demonstrates a basic application using the following portions of Google Play Service:
 * - Leaderboards
 * - Achievements
 *
 * Features demonstrated include:
 * - Sign in / out
 * - Loading the current players score
 * - Submitting a new score
 * - Loading player centric and top leaderboards
 * - Show leaderboards using standard GUI
 * - Unlocking achievements
 * - Incrementing achievements
 * - Revealing hidden achievements
 * - Loading achievements
 * - Show achievements using standard GUI
 *
 * For more information on the general topic of Google Play Services  see
 * https://developer.android.com/google/play-services/index.html
 *
 * Also, see S3E Google Play Services overview in the Marmalade API Reference for
 * more information.
 *
 * The main functions demonstrated in this example include:
 * - s3eGooglePlayServicesAvailable()
 * - s3eGooglePlayServicesRegister()
 * - s3eGooglePlayServicesUnRegister()
 * - s3eGooglePlayServicesInit()
 * - s3eGooglePlayServicesTerminate()
 * - s3eGooglePlayServicesSignIn()
 * - s3eGooglePlayServicesSignOut()
 * - s3eGooglePlayServicesUnlockAchievement()
 * - s3eGooglePlayServicesIncrementAchievement()
 * - s3eGooglePlayServicesRevealAchievement()
 * - s3eGooglePlayServicesLoadAchievements()
 * - s3eGooglePlayServicesShowAchievements()
 * - s3eGooglePlayServicesLoadTopScores()
 * - s3eGooglePlayServicesLoadPlayerCenteredScores()
 * - s3eGooglePlayServicesSubmitScore()
 * - s3eGooglePlayServicesLoadCurrentPlayerLeaderboardScore()
 * - s3eGooglePlayServicesShowAllLeaderboards()
 * - s3eGooglePlayServicesShowLeaderboard()
 *
 * Note that the extension is explicitly initialized and terminated unlike
 * some other extensions.
 *
 * @include s3eGooglePlayServices.cpp
 *
 */

#include "ExamplesMain.h"
#include "s3eGooglePlayServices.h"

// ID's
const char* g_LeaderboardID = "your_leaderboard_id";
const char* g_AchievementID = "your_achievement_id";
const char* g_HiddenAchievementID = "your_hidden_achievement_id";
const char* g_IncrementalAchievementID = "your_incremental_achievement_id";

const bool kButtonAlwaysOn = false;
   // enable for resiliance testing to check does not crash when commands given and logged off, etc

// Buttons
Button* g_SignInButton;
Button* g_SignOutButton;
Button* g_LoadPlayerScoreButton;
Button* g_SubmitScoreButton;
Button* g_UnlockAchievementButton;
Button* g_IncrementAchievementButton;
Button* g_RevealAchievementButton;
Button* g_LoadAchievementsButton;
Button* g_LoadPlayerCenteredScoresButton;
Button* g_LoadTopScoresButton;
Button* g_ShowAchievementsButton;
Button* g_ShowLeaderboardsButton;
Button* g_ShowEasyLeaderboardButton;

int32 ErrorCallback(void* systemData, void* userData)
{
    s3eGooglePlayServicesErrorInfo* error = (s3eGooglePlayServicesErrorInfo*)systemData;
    s3eDebugTracePrintf(">>>>>>>>>>> ErrorCallback, error = %d (%s)", error->error, error->errorString);
    AppendMessage("s3eGooglePlayServices error has occurred:");
    return 0;
}

void SetButtons(bool signedIn)
{
    g_SignInButton->m_Enabled = !signedIn || kButtonAlwaysOn;
    g_SignOutButton->m_Enabled = true; // sign out always enabled
    g_LoadPlayerScoreButton->m_Enabled = signedIn || kButtonAlwaysOn;
    g_SubmitScoreButton->m_Enabled = signedIn || kButtonAlwaysOn;
    g_UnlockAchievementButton->m_Enabled = signedIn || kButtonAlwaysOn;
    g_IncrementAchievementButton->m_Enabled = signedIn || kButtonAlwaysOn;
    g_RevealAchievementButton->m_Enabled = signedIn || kButtonAlwaysOn;
    g_LoadAchievementsButton->m_Enabled = signedIn || kButtonAlwaysOn;
    g_LoadPlayerCenteredScoresButton->m_Enabled = signedIn || kButtonAlwaysOn;
    g_LoadTopScoresButton->m_Enabled = signedIn || kButtonAlwaysOn;
    g_ShowAchievementsButton->m_Enabled = signedIn || kButtonAlwaysOn;
    g_ShowLeaderboardsButton->m_Enabled = signedIn || kButtonAlwaysOn;
    g_ShowEasyLeaderboardButton->m_Enabled = signedIn || kButtonAlwaysOn;
}

int32 SignedInCallback(void* systemData, void* userData)
{
    s3eDebugTracePrintf(">>>>>>>>>>> SignedInCallback");
    AppendMessage("Signed In");
    SetButtons(true);

    return 0;
}

int32 SignedOutCallback(void* systemData, void* userData)
{
    s3eDebugTracePrintf(">>>>>>>>>>> SignedOutCallback");
    AppendMessage("Signed Out");
    SetButtons(false);

    return 0;
}

int32 AchievementsLoadedCallback(void* systemData, void* userData)
{
    s3eGooglePlayAchievements* achs = (s3eGooglePlayAchievements*)systemData;
    s3eDebugTracePrintf(">>>>>>>>>>> AchievementsLoadedCallback, count = %d", achs->count);
    AppendMessage("Achievements Loaded Successfully: count = %d", achs->count);
    for (int t = 0; t < achs->count; t++)
    {
        s3eGooglePlayAchievement *ach = achs->achievements + t;
        s3eDebugTracePrintf("-----------------------------------");
        s3eDebugTracePrintf("\tid = %s", ach->id);
        s3eDebugTracePrintf("\tname = %s", ach->name);
        s3eDebugTracePrintf("\tdescription = %s", ach->description);
        s3eDebugTracePrintf("\ttype = %d", ach->type);
        s3eDebugTracePrintf("\tstatus = %d", ach->status);
        s3eDebugTracePrintf("\tcurrentSteps = %d", ach->currentSteps);
        s3eDebugTracePrintf("\ttotalSteps = %d", ach->totalSteps);
        s3eDebugTracePrintf("\tlastUpdate = %d", ach->lastUpdate);
    }

    return 0;
}
int32 AchievementUnlockedCallback(void* systemData, void* userData)
{
    s3eDebugTracePrintf(">>>>>>>>>>> AchievementUnlockedCallback - %s", (const char*)systemData);
    AppendMessage("Achievements Unlocked: %s", (const char*)systemData);
    return 0;
}
int32 AchievementRevealedCallback(void* systemData, void* userData)
{
    s3eDebugTracePrintf(">>>>>>>>>>> AchievementRevealedCallback - %s", (const char*)systemData);
    AppendMessage("Achievement Revealed: %s", (const char*)systemData);
    return 0;
}
int32 PlayerScoreLoadedCallback(void* systemData, void* userData)
{
    s3eGooglePlayLeaderboardScore* sc = (s3eGooglePlayLeaderboardScore*)systemData;
    s3eDebugTracePrintf(">>>>>>>>>>> PlayerScoreLoadedCallback");
    AppendMessage("Player Score Loaded Successfully:");
    AppendMessage("\trank = %s", sc->rank);
    AppendMessage("\tscore = %d", (int)sc->score);
    AppendMessage("\tdisplayScore = %s", sc->displayScore);
    AppendMessage("\tname = %s", sc->name);
    AppendMessage("\tplayerID = %s", sc->playerID);
    AppendMessage("\ttimestamp = %d", sc->timestamp);

    return 0;
}
int32 PlayerleaderboardLoadedCallback(void* systemData, void* userData)
{
    s3eGooglePlayLeaderboard* lb = (s3eGooglePlayLeaderboard*)systemData;
    s3eDebugTracePrintf(">>>>>>>>>>> PlayerleaderboardLoadedCallback, count = %d", lb->count);
    AppendMessage("Player Leaderboard Loaded Successfully:");
    AppendMessage("\tid = %s", lb->id);
    AppendMessage("\tname = %s", lb->name);
    AppendMessage("\tscoreOrder = %d", lb->scoreOrder);
    for (int t = 0; t < lb->count; t++)
    {
        s3eGooglePlayLeaderboardScore* sc = lb->scores + t;
        s3eDebugTracePrintf("-----------------------------------");
        s3eDebugTracePrintf("\trank = %s", sc->rank);
        s3eDebugTracePrintf("\tscore = %d", (int)sc->score);
        s3eDebugTracePrintf("\tdisplayScore = %s", sc->displayScore);
        s3eDebugTracePrintf("\tname = %s", sc->name);
        s3eDebugTracePrintf("\tplayerID = %s", sc->playerID);
        s3eDebugTracePrintf("\ttimestamp = %d", sc->timestamp);
    }

    return 0;
}
int32 TopScoresLeaderboardLoadedCallback(void* systemData, void* userData)
{
    s3eGooglePlayLeaderboard* lb = (s3eGooglePlayLeaderboard*)systemData;
    s3eDebugTracePrintf(">>>>>>>>>>> TopScoresLeaderboardLoadedCallback, count = %d", lb->count);
    AppendMessage("Top Scores Leaderboard Loaded Successfully:");
    AppendMessage("\tid = %s", lb->id);
    AppendMessage("\tname = %s", lb->name);
    AppendMessage("\tscoreOrder = %d", lb->scoreOrder);
    for (int t = 0; t < lb->count; t++)
    {
        s3eGooglePlayLeaderboardScore* sc = lb->scores + t;
        s3eDebugTracePrintf("-----------------------------------");
        s3eDebugTracePrintf("\trank = %s", sc->rank);
        s3eDebugTracePrintf("\tscore = %d", (int)sc->score);
        s3eDebugTracePrintf("\tdisplayScore = %s", sc->displayScore);
        s3eDebugTracePrintf("\tname = %s", sc->name);
        s3eDebugTracePrintf("\tplayerID = %s", sc->playerID);
        s3eDebugTracePrintf("\ttimestamp = %d", sc->timestamp);
    }

    return 0;
}
int32 ScoreSubmittedCallback(void* systemData, void* userData)
{
    s3eDebugTracePrintf(">>>>>>>>>>> ScoreSubmittedCallback, id = %s", (const char*)systemData);
    AppendMessage("Score Submitted %s", (const char*)systemData);
    return 0;
}


void ExampleInit()
{
    InitMessages(25, 256);

    // create UI buttons
    g_SignInButton  = NewButton("Sign In");
    g_SignOutButton  = NewButton("Sign Out");
    g_UnlockAchievementButton = NewButton("Unlock achievement");
    g_IncrementAchievementButton = NewButton("Increment achievement");
    g_RevealAchievementButton = NewButton("Reveal achievement");
    g_LoadAchievementsButton = NewButton("Load achievements");
    g_ShowAchievementsButton = NewButton("Show Achievements");
    g_LoadTopScoresButton = NewButton("Load top scores");
    g_LoadPlayerCenteredScoresButton = NewButton("Load player centered scores");
    g_SubmitScoreButton = NewButton("Submit score");
    g_LoadPlayerScoreButton = NewButton("Load player score");
    g_ShowLeaderboardsButton = NewButton("Show Leaderboards");
    g_ShowEasyLeaderboardButton = NewButton("Show single leaderboard");
    SetButtons(false);

    // Check whether or not the extension is available and is ready to use
    if (!s3eGooglePlayServicesAvailable())
    {
        AppendMessage("s3eGooglePlayService extension unavailable");
        return;
    }
    g_SignInButton->m_Enabled = true;

    s3eGooglePlayServicesRegister(S3E_GOOGLEPLAYSERVICES_SIGNIN_CALLBACK, SignedInCallback, NULL);
    s3eGooglePlayServicesRegister(S3E_GOOGLEPLAYSERVICES_SIGNOUT_CALLBACK, SignedOutCallback, NULL);
    s3eGooglePlayServicesRegister(S3E_GOOGLEPLAYSERVICES_ERROR_CALLBACK, ErrorCallback, NULL);
    s3eGooglePlayServicesRegister(S3E_GOOGLEPLAYSERVICES_ACHIEVEMENTS_LOADED_CALLBACK, AchievementsLoadedCallback, NULL);
    s3eGooglePlayServicesRegister(S3E_GOOGLEPLAYSERVICES_ACHIEVEMENT_UNLOCKED_CALLBACK, AchievementUnlockedCallback, NULL);
    s3eGooglePlayServicesRegister(S3E_GOOGLEPLAYSERVICES_ACHIEVEMENT_REVEALED_CALLBACK, AchievementRevealedCallback, NULL);
    s3eGooglePlayServicesRegister(S3E_GOOGLEPLAYSERVICES_PLAYER_SCORE_LOADED_CALLBACK, PlayerScoreLoadedCallback, NULL);
    s3eGooglePlayServicesRegister(S3E_GOOGLEPLAYSERVICES_PLAYER_LEADERBOARD_LOADED_CALLBACK, PlayerleaderboardLoadedCallback, NULL);
    s3eGooglePlayServicesRegister(S3E_GOOGLEPLAYSERVICES_TOP_SCORES_LEADERBOARD_LOADED_CALLBACK, TopScoresLeaderboardLoadedCallback, NULL);
    s3eGooglePlayServicesRegister(S3E_GOOGLEPLAYSERVICES_SCORE_SUBMITTED_CALLBACK, ScoreSubmittedCallback, NULL);
    s3eGooglePlayServicesInit();
}

void ExampleTerm()
{
    // Cleanup
    // Buttons are automatically deallocated by example framework
    // Unregister callbacks
    s3eGooglePlayServicesUnRegister(S3E_GOOGLEPLAYSERVICES_SIGNIN_CALLBACK, SignedInCallback);
    s3eGooglePlayServicesUnRegister(S3E_GOOGLEPLAYSERVICES_ERROR_CALLBACK, ErrorCallback);
    s3eGooglePlayServicesTerminate();
}

bool ExampleUpdate()
{
    Button* selectedButton = GetSelectedButton();

    if (selectedButton == g_SignInButton)
    {
        s3eGooglePlayServicesSignIn();
    }
    else if (selectedButton == g_SignOutButton)
    {
        s3eGooglePlayServicesSignOut();
    }
    else if (selectedButton == g_UnlockAchievementButton)
    {
        s3eGooglePlayServicesUnlockAchievement(g_AchievementID, S3E_TRUE);
    }
    else if (selectedButton == g_IncrementAchievementButton)
    {
        s3eGooglePlayServicesIncrementAchievement(g_IncrementalAchievementID, 1, S3E_TRUE);
    }
    else if (selectedButton == g_RevealAchievementButton)
    {
        s3eGooglePlayServicesRevealAchievement(g_HiddenAchievementID, S3E_TRUE);
    }
    else if (selectedButton == g_LoadAchievementsButton)
    {
        s3eGooglePlayServicesLoadAchievements();
    }
    else if (selectedButton == g_ShowAchievementsButton)
    {
        s3eGooglePlayServicesShowAchievements();
    }
    else if (selectedButton == g_LoadTopScoresButton)
    {
        s3eGooglePlayServicesLoadTopScores(g_LeaderboardID, (int)s3eGooglePlayTimeSpan_AllTime, (int)s3eGooglePlayScoreCollection_Public, 25, S3E_TRUE);
    }
    else if (selectedButton == g_LoadPlayerCenteredScoresButton)
    {
        s3eGooglePlayServicesLoadPlayerCenteredScores(g_LeaderboardID, (int)s3eGooglePlayTimeSpan_AllTime, (int)s3eGooglePlayScoreCollection_Public, 25, S3E_TRUE);
    }
    else if (selectedButton == g_SubmitScoreButton)
    {
        s3eGooglePlayServicesSubmitScore(g_LeaderboardID, 5502, S3E_TRUE);
    }
    else if (selectedButton == g_LoadPlayerScoreButton)
    {
        s3eGooglePlayServicesLoadCurrentPlayerLeaderboardScore(g_LeaderboardID, (int)s3eGooglePlayTimeSpan_AllTime, (int)s3eGooglePlayScoreCollection_Public);
    }
    else if (selectedButton == g_ShowLeaderboardsButton)
    {
        s3eGooglePlayServicesShowAllLeaderboards();
    }
    else if (selectedButton == g_ShowEasyLeaderboardButton)
    {
        s3eGooglePlayServicesShowLeaderboard(g_LeaderboardID);
    }
    return true;
}

void ExampleRender()
{
    ButtonsRender();

    int y = GetYBelowButtons();
    y +=  20;
    PrintMessages(10, y);
}
