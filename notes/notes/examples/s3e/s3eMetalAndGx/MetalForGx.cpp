/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

#include "s3e.h"
#include "s3eMetal.h"
#include <math.h>

// global variables
CMetalPtr<CMetalDevice> g_Device;
CMetalPtr<CMetalCommandQueue> g_CommandQueue;
CMetalPtr<CMetalComputePipelineState> g_Compute;
CMetalPtr<CMetalBuffer> g_Phase;
CMetalPtr<CMetalTexture> g_Texture;

static unsigned char* s_MetalBuffer;

bool MetalInit()
{
    // Make metal device without display layer
    g_Device = s3eMetalGetDeviceNoDisplay();
    g_CommandQueue = g_Device->NewCommandQueue();

    // Make compute shader library
    CMetalError* error = NULL;
    CMetalPtr<CMetalLibrary> lib = g_Device->NewLibrary(
        "using namespace metal;\n"
        "kernel void textCompute(constant float& phase [[ buffer(0) ]], texture2d<float,access::write> out [[ texture(0) ]],\n"
        "                        uint2 localPos [[thread_position_in_threadgroup]], uint2 groupPos [[threadgroup_position_in_grid]],\n"
        "                        uint2 globalPos [[thread_position_in_grid]])\n"
        "{\n"
        "    float localCoef = length(float2(int2(localPos) - 8) / 8.0);\n"
        "    float globalCoef = sin(float(groupPos.x + groupPos.y)*0.1 + phase) * 0.5;\n"
        "    out.write(float4(1.0 - globalCoef*localCoef, 0.0, 0.0, 1.0), globalPos);\n"
        "}\n", CMetalCompileOptions(), &error);

    if (error != NULL)
        s3eDebugErrorPrintf("Metal Compile Error: %s", error->m_Description);
    if (lib.get() == NULL)
        return false;

    // Make Compute Pipeline State from Compute shader
    CMetalPtr<CMetalFunction> computeFn = lib->NewFunction("textCompute");
    g_Compute = g_Device->NewComputePipelineState(computeFn.get(), &error);

    if (error != NULL)
        s3eDebugErrorPrintf("Metal Compute Error: %s", error->m_Description);

    // Shader Buffer for input parameter
    g_Phase = g_Device->NewBuffer(sizeof(float), CMetalBuffer::OPTION_CPU_CACHE_MODE_DEFAULT);

    // Texture to generate
    CMetalTextureDescriptor tDesc(CMetalTextureDescriptor::R8Unorm, 512, 512, false);
    g_Texture = g_Device->NewTexture(tDesc);

    s_MetalBuffer = new unsigned char[512*512];

    return true;
}

unsigned char* MetalCompute()
{
    CMetalPtr<CMetalCommandBuffer> commandBuffer = g_CommandQueue->CommandBuffer();

    // update input parameter
    static float offset = 0;
    float* phase = (float*)g_Phase->Contents();
    phase[0] = offset;
    offset += 0.01f;

    {
        // run compute shader
        CMetalPtr<CMetalComputeCommandEncoder> compute = commandBuffer->ComputeCommandEncoder();

        compute->SetComputePipelineState(g_Compute.get());
        compute->SetTexture(g_Texture.get(), 0);
        compute->SetBuffer(g_Phase.get(), 0, 0);

        compute->Dispatch(512/16, 512/16, 16, 16);

        compute->EndEncoding();
    }
    commandBuffer->Commit();

    // get the texture
    g_Texture->GetBytes(s_MetalBuffer, 512, CMetalRegion(0,0,512,512), 0);
    return s_MetalBuffer;
}

void MetalShutdown()
{
    delete[] s_MetalBuffer;

    // remove globals
    g_Texture = NULL;
    g_Phase = NULL;
    g_Compute = NULL;
    g_CommandQueue = NULL;
    g_Device = NULL;
}
