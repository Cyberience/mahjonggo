/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EMetalAndGx s3eMetal and IwGx Example
 *
 * This example is based on the IwGxTexture example, but runs a MetalCompute
 * compute shader to generate the texture.
 *
 * The following graphics illustrates the example output.
 *
 * @image html s3eMetalAndGxImage.png
 *
 * @include s3eMetalAndGx.cpp
 *
 * @include MetalForGx.cpp
 */

#include "IwGx.h"
#include "IwMaterial.h"
#include "IwTexture.h"

bool MetalInit();
unsigned char* MetalCompute();
void MetalShutdown();

// Texture object
CIwTexture* s_Texture = NULL;

float g_scale = 2.0f;

// Vertex data
const float s = 0x80/g_scale;
CIwFVec3    s_Verts[8] =
{
    CIwFVec3(-s, -s, -s),
    CIwFVec3( s, -s, -s),
    CIwFVec3( s,  s, -s),
    CIwFVec3(-s,  s, -s),
    CIwFVec3(-s, -s,  s),
    CIwFVec3( s, -s,  s),
    CIwFVec3( s,  s,  s),
    CIwFVec3(-s,  s,  s),
};

// UV data
CIwFVec2    s_UVs[8] =
{
    CIwFVec2(0, 0),
    CIwFVec2(1.0f, 0),
    CIwFVec2(1.0f, 1.0f),
    CIwFVec2(0, 1.0f),
    CIwFVec2(0, 0),
    CIwFVec2(1.0f, 0),
    CIwFVec2(1.0f, 1.0f),
    CIwFVec2(0, 1.0f),
};

// Index stream for textured material
uint16      s_QuadStrip[4] =
{
    0, 3, 1, 2,
};

// Index stream for untextured material
uint16      s_TriStrip[20] =
{
    1, 2, 5, 6, 4, 7, 0, 3,
    3, 4, // degenerates
    4, 0, 5, 1,
    1, 3, // degenerates
    3, 7, 2, 6,
};

// Angles
CIwFVec3    s_Angles;

// Local matrix
CIwFMat      s_ModelMatrix;

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise Metal
    if (!MetalInit())
        return;

    // Initialise
    IwGxInit();

    // Set screen clear colour
    IwGxSetColClear(0x40, 0x40, 0x40, 0x00);

    // Turn all lighting off
    IwGxLightingOff();

    // Set field of view
    IwGxSetPerspMul(0xa0);

    // Set near and far planes
    IwGxSetFarZNearZ(0x1000, 0x10);

    // Create empty texture object
    s_Texture = new CIwTexture;

    // Load image data from disk into texture
    unsigned int *buffer = new unsigned int[128*128];
    for (int i=0; i<128; i++)
        for (int j=0; j<128; j++)
            buffer[i*128+j] = 0x7f7f7fff;
    s_Texture->CopyFromBuffer(128, 128, CIwImage::RGBA_8888, 128*4, (unsigned char*)buffer, NULL);
    s_Texture->SetModifiable(true);
    delete[] buffer;

    // "Upload" texture to VRAM
    s_Texture->Upload();

    // Initialise angles
    s_Angles = CIwFVec3::g_Zero;

    // Set the view matrix along the -ve z axis
    CIwFMat view = CIwFMat::g_Identity;
    view.t.z = -0x200/g_scale;

    IwGxSetViewMatrix(&view);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // Destroy texture
    delete s_Texture;

    // Terminate
    IwGxTerminate();

    // Shutdown Metal
    MetalShutdown();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // Update angles
    s_Angles.x += (PI*0x10/2048.0);
    s_Angles.y += (PI*0x20/2048.0);
    s_Angles.z += (PI*0x30/2048.0);

    // Build model matrix from angles
    CIwFMat  rotX, rotY, rotZ;
    rotX.SetRotX(s_Angles.x);
    rotY.SetRotY(s_Angles.y);
    rotZ.SetRotZ(s_Angles.z);
    s_ModelMatrix = rotX * rotY * rotZ;

    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Run Metal Compute Shader
    unsigned char* from = MetalCompute();
    if (from != NULL)
    {
        // copy texture from Metal to IwGx
        unsigned int* to = (unsigned int*)s_Texture->GetTexels();
        for (int i=0; i<128; i++)
            for (int j=0; j<128; j++)
                to[i*128+j] = 0x000000ff + from[i*512+j]*256;
        s_Texture->ChangeTexels(s_Texture->GetTexels(), CIwImage::RGBA_8888);
    }

    CIwMaterial* pMat;

    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Allocate and initialise material from the IwGx global cache
    pMat = IW_GX_ALLOC_MATERIAL();

    // Set this as the active material
    IwGxSetMaterial(pMat);

    // Set the diffuse map
    pMat->SetTexture(s_Texture);

    // Set the model matrix
    IwGxSetModelMatrix(&s_ModelMatrix);

    // Set the (modelspace) vertex stream
    IwGxSetVertStreamModelSpace(s_Verts, 8);

    // Set the vertex UV stream
    IwGxSetUVStream(s_UVs);

    // Draw the quat primitives
    IwGxDrawPrims(IW_GX_QUAD_STRIP, s_QuadStrip, 4);

    // Draw the strip primitives
    IwGxDrawPrims(IW_GX_TRI_STRIP, s_TriStrip, 20);

    // End drawing
    IwGxFlush();

    // Swap buffers
    IwGxSwapBuffers();
}

int main()
{
    // Example main loop
    ExampleInit();

    // Set screen clear colour
    IwGxSetColClear(0xff, 0xff, 0xff, 0xff);
    IwGxPrintSetColour(128, 128, 128);

    while (1)
    {
        s3eDeviceYield(0);

        ExampleUpdate();

        // Clear the screen
        IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

        ExampleRender();
    }

    ExampleShutDown();
    return 0;
}
