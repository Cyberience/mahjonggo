/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3ESoundRecord S3E Sound Record Example
 * The following example uses S3E's Sound Record API to record sound from
 * the phone's microphone and play it back to the user.
 *
 * The example registers a callback that will be called periodically containing
 * audio data. This callback stores the data to a buffer that will be saved as a
 * .wav file when the recording is finished.
 *
 * When the recording is finished the example can play the raw recording back to
 * the user using s3eSoundChannelPlay().
 *
 * The following functions are used to achieve this:
 * - s3eSoundRecordStart()
 * - s3eSoundRecordStop()
 * - s3eSoundRecordSetInt()
 * - s3eSoundChannelPlay()
 *
 * @include s3eSoundRecord.cpp
 */



#include "ExamplesMain.h"
#include "s3eSoundRecord.h"
#include "s3eDebug.h"
#include "s3eSound.h"

//The number of samples to record
#define REC_BUFFER_SAMPLES 500000

static Button* g_ButtonRecord = NULL;
static Button* g_ButtonStop = NULL;
static Button* g_ButtonPlay = NULL;
static int g_RecChannel;

static int16* g_RecordBuf = NULL;
static int g_RecordedSamples = 0;
static bool g_RecordingInterrupted = false;
static int g_nChannelsCount = 1;

//The header for the output wave file
struct RiffHeader
{
    char m_ChunkID[4];
    int32 m_ChunkSize;
    char m_RIFFType[4];

    RiffHeader()
    {
        memcpy(m_ChunkID, "RIFF", 4);
        memcpy(m_RIFFType, "WAVE", 4);
    }
};

struct ChunkHeader
{
    char m_ChunkID[4];
    uint32 m_ChunkSize;
    uint16 m_CompressionCode;
    uint16 m_NumberOfChannels;
    uint32 m_SampleRate;
    uint32 m_BytesPerSecond;
    uint16 m_BlockAlign;
    uint16 m_SignificantBits;
};

void ExampleRender()
{
    if (!s3eSoundRecordAvailable())
    {
        s3eDebugPrint(10, 200, "`xff0000Sound recording API is not available on this device.", 1);
        return;
    }
}

void ExampleInit()
{
    g_ButtonRecord = NewButton("Record");
    g_ButtonStop   = NewButton("Stop and Save");
    g_ButtonPlay   = NewButton("Playback Recording");
    g_ButtonStop->m_Enabled = false;
    g_ButtonPlay->m_Enabled = false;
    if (!s3eSoundRecordAvailable())
        g_ButtonRecord->m_Enabled = false;
    s3eSoundRecordSetInt(S3E_SOUND_RECORD_VOLUME, 0x100);

    int overrideBufferSize = 0;
    s3eConfigGetInt("SoundRecordExample", "SoundBufferHint", &overrideBufferSize);
    if (overrideBufferSize)
    {
        s3eSoundRecordSetInt(S3E_SOUND_RECORD_HINT_MILLISECONDS, overrideBufferSize);
    }

    g_RecChannel = s3eSoundGetFreeChannel();

    g_RecordBuf = (int16*)s3eMalloc(REC_BUFFER_SAMPLES*2);
}

static int GetFrequency()
{
    if (s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WS81 ||
        s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WS8 ||
        s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WP81 ||
        s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WIN10
       )
    {
        return s3eSoundRecordGetInt(S3E_SOUND_RECORD_FREQUENCY);
    }
    return S3E_SOUND_RECORD_FREQ_44KHZ;
}

/*
 * Write a wave file (.wav) containing all recorded sound data
 */
void WriteWav()
{
    if (!g_RecordedSamples)
        return;

    FILE* waveOut = fopen("recording.wav", "w");

    if (waveOut)
    {
        // write RIFF header
        RiffHeader header;
        header.m_ChunkSize = g_RecordedSamples * 2 + sizeof(RiffHeader) + sizeof(ChunkHeader);
        fwrite(&header, 1, sizeof(header), waveOut);

        // write CHUNK header
        ChunkHeader chunk;
        memcpy(chunk.m_ChunkID, "fmt ", 4);
        chunk.m_ChunkSize = 16;
        chunk.m_CompressionCode = 1;
        chunk.m_NumberOfChannels = g_nChannelsCount;
        chunk.m_SampleRate = GetFrequency();
        chunk.m_BytesPerSecond = chunk.m_SampleRate * chunk.m_NumberOfChannels * sizeof(int16);
        chunk.m_BlockAlign = 2;
        chunk.m_SignificantBits = 16;
        fwrite(&chunk, 1, sizeof(chunk), waveOut);

        // write DATA header
        fwrite("data", 1, 4, waveOut);
        int dataSize = g_RecordedSamples * 2;
        fwrite(&dataSize, 1, 4, waveOut);
        fwrite(g_RecordBuf, 2, g_RecordedSamples, waveOut);

        fclose(waveOut);
    }
}

/*
 * Stop sound recording and write out a wave file of all recorded data
 */
void StopAndWriteWav()
{
    if (s3eSoundRecordStop() == S3E_RESULT_SUCCESS)
        WriteWav();
}

/*
 * This callback is called periodically during recording with new recorded audio data.
 * data->m_Data contains the recorded data, data->m_Samples indicated how many samples
 * are stored there.
 * The data is copied to a buffer which will be written to a wave file once recording
 * is stopped
 */
int32 callback(void* sys, void* user)
{
    s3eSoundRecordData* data = (s3eSoundRecordData*)sys;

    // Check for error/recording cancelled due to loss of input
    if (data->m_Error != S3E_SOUND_RECORD_ERR_NONE)
    {
        // Save recorded data if we have any. Note that we are not calling
        // s3e API here as this function may be called from another thread.
        g_RecordingInterrupted = true;
        return 0;
    }

    //work out space remaining in our buffer
    int bufferRemain = REC_BUFFER_SAMPLES - g_RecordedSamples;
    int samplesToSave = data->m_NumSamples < bufferRemain ? data->m_NumSamples : bufferRemain;

    //save data to buffer
    memcpy(g_RecordBuf + g_RecordedSamples, data->m_Data, samplesToSave*2);
    g_RecordedSamples += samplesToSave;
    return 0;
}

/*
 * Playback the recorded data at 44khz
 */
void PlaybackSound()
{
    s3eSoundChannelSetInt(g_RecChannel, S3E_CHANNEL_RATE, GetFrequency());
    s3eSoundChannelPlay(g_RecChannel, g_RecordBuf, g_RecordedSamples, 1, 0);
    g_ButtonPlay->m_Enabled = false;
}

bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();
    bool isRecording = s3eSoundRecordGetInt(S3E_SOUND_RECORD_STATUS) == S3E_SOUND_RECORD_RECORDING;
    if (pressed == g_ButtonRecord && !isRecording)
    {
        //Start recording and register callback
        if (s3eSoundRecordStart(callback, NULL) == S3E_RESULT_SUCCESS)
        {
            g_ButtonPlay->m_Enabled = false;
            g_RecordedSamples = 0;
        }
    }
    else if (pressed == g_ButtonStop && isRecording)
    {
        StopAndWriteWav();
    }

    if (pressed == g_ButtonPlay)
    {
        PlaybackSound();
    }

    if (isRecording && g_RecordedSamples == REC_BUFFER_SAMPLES)
    {
        StopAndWriteWav();
    }

    if (g_RecordingInterrupted)
    {
        WriteWav();
        g_RecordingInterrupted = false;
    }

    //Enable the playback button if we have some samples and we're not recording
    if (!s3eSoundChannelGetInt(g_RecChannel, S3E_CHANNEL_STATUS) && g_RecordedSamples && !isRecording)
    {
        g_ButtonPlay->m_Enabled = true;
    }

    g_ButtonStop->m_Enabled = isRecording;
    g_ButtonRecord->m_Enabled = s3eSoundRecordGetInt(S3E_SOUND_RECORD_INPUT_AVAILABLE) && !isRecording;

    return true;
}

void ExampleTerm()
{
    s3eFree(g_RecordBuf);
}
