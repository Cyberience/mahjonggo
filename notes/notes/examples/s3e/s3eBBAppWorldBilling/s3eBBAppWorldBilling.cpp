/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EBBAppWorldBilling S3E Blackberry App World Billing Example
 * This example illustrates the use of the In App Billing functionality on the
 * BlackBerry platform, allowing items, products or services to be bought via
 * BlackBerry App World. It  allows the user to purchase the products,
 * obtain information about them and cancel subscriptions.
 *
 * This example and the S3E App World Billing API it uses are currently only
 * supported on BlackBerry.
 *
 * For more information on the general topic of BlackBerry App World Billing see
 * http://developer.blackberry.com/native/reference/core/com.qnx.doc.bps.lib_ref/topic/about_paymentservice_8h.html
 *
 * Note that the API only handles purchasing of the product, not delivery of the
 * actual service/data that was paid for.
 *
 * See S3E In App Billing Overview in the Marmalade API Reference for
 * more information.
 *
 * The main functions demonstrated in this example are:
 * - s3eBBAppWorldBillingStart()
 * - s3eBBAppWorldBillingStop()
 * - s3eBBAppWorldBillingRequestPurchase()
 * - s3eBBAppWorldBillingGetPrice()
 * - s3eBBAppWorldBillingCheckExisting()
 * - s3eBBAppWorldBillingGetExistingPurchases()
 * - s3eBBAppWorldBillingCancelSubsription()
 * - s3eBBAppWorldBillingSetConnectionLocalMode()
 * - s3eBBAppWorldBillingItemStateToStr()
 *
 * Note that the extension is explicitly started and stopped.
 *
 * The example uses s3eBBAppWorldBillingRegister to register callbacks to receive
 * request response information.
 * Any errors that occur are also reported via productErrorCallback .
 *
 * @include s3eBBAppWorldBilling.cpp
 */

#include "ExamplesMain.h"
#include "s3eBBAppWorldBilling.h"
#include "s3eOSReadString.h"
#include <algorithm>

// buttons to set different IDs and SKU
static Button* g_ButtonSetID = NULL;
static Button* g_ButtonSetSKU = NULL;
static Button* g_ButtonSetPurchID = NULL;

// buttons to toggle boolean values
static Button* g_ButtonPurchaseMode = NULL;
static Button* g_ButtonServerUpdate = NULL;

// buttons to buy digital goods
static Button* g_ButtonBuy = NULL;

// buttons to get purchased items and navigate on them
static Button* g_ButtonGetPurchases = NULL;
static Button* g_ButtonListPREV = NULL;
static Button* g_ButtonListNEXT = NULL;

// buttons for other requests :
static Button* g_ButtonGetPrice = NULL;
static Button* g_ButtonCheckExisting = NULL;
static Button* g_ButtonCancelSubscription = NULL;

// whether BlackBerry AppWorld Billing is avaliable on device
static bool g_HasAppWorld;

// whether BlackBerry AppWorld Billing is in local mode
static bool g_IsLocalMode = true;

// get purchased items from device or server
static bool g_IsServerUpdate = false;

// global struct used for requests
static s3eBBAppWorldBillingPurchaseRequest g_ProductRequest;
// ID of Pucrhcased subscription to be cancelled
char g_CancellationPurchaseID[S3E_BBAPPWORLDBILLING_STRING_MAX];

// how much sec we waited for response
static int g_waiting = -1;
static uint64 g_waitMs;
// type of callback we received on response
static int g_callbacktype = -1;
// result of s3eBBAppWorldBilling function call
static s3eResult g_CallResult = S3E_RESULT_SUCCESS;

// information of successful Digital Good purchase
static s3eBBAppWorldBillingPurchaseInformation g_Purchase;

// list of purchased items
static int g_PurchaseIdx;
static s3eBBAppWorldBillingPurchases g_Purchases;

// Data of Get Price, Check Exist and Subscription Cancellation callbacks
static s3eBBAppWorldBillingProductPrice g_productPrice;
static s3eBBAppWorldBillingProductExists g_productExists;
static s3eBBAppWorldBillingSubscriptionCancellation g_cancelInfo;

// Purchase Error Data
static s3eBBAppWorldBillingPurchaseError g_Error;

// HELPER FUNCTIONS:
// - copy purchase information
static void copyPurchaseInfo(s3eBBAppWorldBillingPurchaseInformation& dst, s3eBBAppWorldBillingPurchaseInformation* src)
{
#define COPYFIELD(name) \
    strlcpy(dst.m_ ## name, src->m_ ## name, sizeof(dst.m_ ## name))

    COPYFIELD(DigitalGoodId);
    COPYFIELD(DigitalGoodSKU);
    COPYFIELD(LicenseKey);
    COPYFIELD(MetaData);
    COPYFIELD(PurchaseId);
    COPYFIELD(Date);
    COPYFIELD(StartDate);
    COPYFIELD(EndDate);
    COPYFIELD(InitialPeriod);
    dst.m_ItemState = src->m_ItemState;

#undef COPYFIELD
}

// - convert CallBack type into string
static const char* BBAppWorldCallBackType2Str(int callbackType)
{
#define CASE_CALLBACKTYPE(type) \
    case S3E_BBAPPWORLDBILLING_CALLBACK_ ## type: return #type

    switch (callbackType)
    {
        CASE_CALLBACKTYPE(PURCHASE_ERROR);
        CASE_CALLBACKTYPE(PURCHASE_RESPONSE);
        CASE_CALLBACKTYPE(PURCHASE_EXISTING);
        CASE_CALLBACKTYPE(PURCHASE_PRICE);
        CASE_CALLBACKTYPE(PURCHASE_EXISTS);
        CASE_CALLBACKTYPE(CANCEL_SUBSCRIPTION);
    default:
        return "N/A";
    }

#undef CASE_CALLBACKTYPE
}

// - print information about purchased item
static void PrintPurchase(int xpos, int ypos, int textHeight, s3eBBAppWorldBillingPurchaseInformation &purchase)
{
    s3eDebugPrintf(xpos, ypos, 1, "`x666666 callback type :%s", BBAppWorldCallBackType2Str(g_callbacktype));
    ypos += textHeight;
    s3eDebugPrintf(xpos, ypos, 1, "`x666666 Purchase Data -------------------------------");
    ypos += textHeight;
    s3eDebugPrintf(xpos, ypos, 1, "`x666666 ID            : %s", purchase.m_DigitalGoodId);
    ypos += textHeight;
    s3eDebugPrintf(xpos, ypos, 1, "`x666666 SKU           : %s", purchase.m_DigitalGoodSKU);
    ypos += textHeight;
    s3eDebugPrintf(xpos, ypos, 1, "`x666666 Key           : %s", purchase.m_LicenseKey);
    ypos += textHeight;
    s3eDebugPrintf(xpos, ypos, 1, "`x666666 Metadata      : %s", purchase.m_MetaData);
    ypos += textHeight;
    s3eDebugPrintf(xpos, ypos, 1, "`x666666 Purchase ID   : %s", purchase.m_PurchaseId);
    ypos += textHeight;
    s3eDebugPrintf(xpos, ypos, 1, "`x666666 Date          : %s", purchase.m_Date);
    ypos += textHeight;
    s3eDebugPrintf(xpos, ypos, 1, "`x666666 Start Date    : %s", purchase.m_StartDate);
    ypos += textHeight;
    s3eDebugPrintf(xpos, ypos, 1, "`x666666 End Date      : %s", purchase.m_EndDate);
    ypos += textHeight;
    s3eDebugPrintf(xpos, ypos, 1, "`x666666 Initial Period: %s", purchase.m_InitialPeriod);
    ypos += textHeight;
    s3eDebugPrintf(xpos, ypos, 1, "`x666666 Item State    : %s", s3eBBAppWorldBillingItemStateToStr(purchase.m_ItemState));
}

// - clear and (re)init controls and data structures for get Purchased Items request
static void initPurchasesInfoConrols()
{
    g_callbacktype = -1;

    // disable buttons
    g_ButtonListPREV->m_Enabled = false;
    g_ButtonListNEXT->m_Enabled = false;

    // clean & release allocated memory
    g_PurchaseIdx = 0;
    if (g_Purchases.m_Purchases)
    {
        delete[] g_Purchases.m_Purchases;
        g_Purchases.m_Purchases = NULL;
    }
}

// - fetch user input
static void GetOSStringValue(char value[], size_t valueBufferSize, const char *prompt, int flags)
{
    const char* input = s3eOSReadStringUTF8(prompt, flags);
    if (input)
    {
        strlcpy(value, input, valueBufferSize);
    }
    else
    {
        value[0] = '\0';
    }

}

// - start waiting for response
static void startWaiting()
{
    g_waiting = 0;
    g_waitMs = s3eTimerGetMs();
    initPurchasesInfoConrols();
}

// CALLBACKS:
// on purchase error callback
int32 productErrorCallback(s3eBBAppWorldBillingPurchaseError* purchaseError, void* userData)
{
    g_callbacktype = S3E_BBAPPWORLDBILLING_CALLBACK_PURCHASE_ERROR;

    if (!purchaseError)
        return 0;

    g_Error.m_ErrorId = purchaseError->m_ErrorId;
    strlcpy(g_Error.m_ErrorText, purchaseError->m_ErrorText, sizeof(g_Error.m_ErrorText));

    return 0;
}

// on purchase success callback
int32 purchaseSuccessCallback(s3eBBAppWorldBillingPurchaseInformation* productInfo, void* userData)
{
    g_callbacktype = S3E_BBAPPWORLDBILLING_CALLBACK_PURCHASE_RESPONSE;

    if (!productInfo)
        return 0;

    copyPurchaseInfo(g_Purchase, productInfo);
    return 0;
}

// on get Purchased Items success callback
int32 getPurchasesSuccessCallback(s3eBBAppWorldBillingPurchases* purchasesInfo, void* userData)
{
    g_callbacktype = S3E_BBAPPWORLDBILLING_CALLBACK_PURCHASE_EXISTING;

    if (!purchasesInfo)
        return 0;

    int num = purchasesInfo->m_NumberPurchases;
    g_Purchases.m_NumberPurchases = num;
    g_Purchases.m_Purchases = new s3eBBAppWorldBillingPurchaseInformation[num];

    for (int i = 0; i < num; i++)
    {

        copyPurchaseInfo(g_Purchases.m_Purchases[i], &(purchasesInfo->m_Purchases[i]));
    }
    return 0;
}

// on get Price success callback
int32 getPriceSuccessCallback(s3eBBAppWorldBillingProductPrice* priceInfo, void* userData)
{
    g_callbacktype = S3E_BBAPPWORLDBILLING_CALLBACK_PURCHASE_PRICE;

    if (!priceInfo)
        return 0;

#define COPYFIELD(name) \
    strlcpy(g_productPrice.m_ ## name, priceInfo->m_ ## name, sizeof(g_productPrice.m_ ## name))

    COPYFIELD(Price);
    COPYFIELD(InitialPeriod);
    COPYFIELD(RenewalPrice);
    COPYFIELD(RenewalPeriod);

#undef COPYFIELD
    return 0;
}

// on check Item Exists success callback
int32 checkExistsSuccessCallback(s3eBBAppWorldBillingProductExists* existsInfo, void* userData)
{
    g_callbacktype = S3E_BBAPPWORLDBILLING_CALLBACK_PURCHASE_EXISTS;

    if (!existsInfo)
        return 0;

    g_productExists.m_Subscription = existsInfo->m_Subscription;

    return 0;
}

// on Subscription Cancellation success callback
int32 cancelSubscriptionSuccessCallback(s3eBBAppWorldBillingSubscriptionCancellation* cancellationInfo, void* userData)
{
    g_callbacktype = S3E_BBAPPWORLDBILLING_CALLBACK_CANCEL_SUBSCRIPTION;

    if (!cancellationInfo)
        return 0;

    g_cancelInfo.m_Cancelled = cancellationInfo->m_Cancelled;
    strlcpy(g_cancelInfo.m_CancelledPurchaseId, cancellationInfo->m_CancelledPurchaseId, sizeof(g_cancelInfo.m_CancelledPurchaseId));

    return 0;
}

// functions for ExamplesMain subproject:

void ExampleInit()
{
    g_ButtonSetID        = NewButton("Set Digital Good ID");
    g_ButtonSetSKU       = NewButton("Set Digital Good SKU");
    g_ButtonSetPurchID   = NewButton("Set Cancel Purch. ID");

    g_ButtonPurchaseMode = NewButton("Switch AppWorld mode");
    g_ButtonServerUpdate = NewButton("Switch server update");

    g_ButtonBuy          = NewButton("<Buy Digital Good>");
    g_ButtonGetPurchases = NewButton("<Get Existing Purchases>");
    g_ButtonListPREV     = NewButton("  Prev. Item");
    g_ButtonListNEXT     = NewButton("   Next Item");

    g_ButtonGetPrice           = NewButton("<Get Price>");
    g_ButtonCheckExisting      = NewButton("<Check Existing>");
    g_ButtonCancelSubscription = NewButton("<Cancel Subscription>");

    // check that s3eOSReadString is avaliable on device
    if (s3eOSReadStringAvailable() == S3E_FALSE)
    {
        g_ButtonSetID->m_Enabled = false;
        g_ButtonSetSKU->m_Enabled = false;
        g_ButtonSetPurchID->m_Enabled = false;
    }

    // check that s3eBBAppWorldBilling is avaliable on device
    g_HasAppWorld = s3eBBAppWorldBillingAvailable() == S3E_TRUE;

    // initialize data structures
    g_Purchases.m_Purchases = NULL;
    initPurchasesInfoConrols();

    if (g_HasAppWorld)
    {
        // start BlackBerry AppWorld Billing and register callbacks
        s3eBBAppWorldBillingStart();
        s3eBBAppWorldBillingRegister(S3E_BBAPPWORLDBILLING_CALLBACK_PURCHASE_ERROR, (s3eCallback)&productErrorCallback, NULL);

        s3eBBAppWorldBillingRegister(S3E_BBAPPWORLDBILLING_CALLBACK_PURCHASE_RESPONSE, (s3eCallback)&purchaseSuccessCallback, NULL);
        s3eBBAppWorldBillingRegister(S3E_BBAPPWORLDBILLING_CALLBACK_PURCHASE_EXISTING, (s3eCallback)&getPurchasesSuccessCallback, NULL);

        s3eBBAppWorldBillingRegister(S3E_BBAPPWORLDBILLING_CALLBACK_PURCHASE_PRICE, (s3eCallback)&getPriceSuccessCallback, NULL);
        s3eBBAppWorldBillingRegister(S3E_BBAPPWORLDBILLING_CALLBACK_PURCHASE_EXISTS, (s3eCallback)&checkExistsSuccessCallback, NULL);
        s3eBBAppWorldBillingRegister(S3E_BBAPPWORLDBILLING_CALLBACK_CANCEL_SUBSCRIPTION, (s3eCallback)&cancelSubscriptionSuccessCallback, NULL);

        s3eBBAppWorldBillingSetConnectionLocalMode(g_IsLocalMode);
    }
    else
    {
        // otherwise disable buttons
        g_ButtonSetPurchID->m_Enabled = false;
        g_ButtonBuy->m_Enabled = false;
        g_ButtonGetPurchases->m_Enabled = false;
        g_ButtonGetPrice->m_Enabled = false;
        g_ButtonCheckExisting->m_Enabled = false;
    }

    // disabled until non-empty Purchase ID
    g_ButtonCancelSubscription->m_Enabled = false;

    // fill with your predefined values
    strcpy(g_ProductRequest.m_DigitalGoodId, "");
    strcpy(g_ProductRequest.m_DigitalGoodSKU, "");
    strcpy(g_ProductRequest.m_DigitalGoodName, "");
    strcpy(g_ProductRequest.m_MetaData, "");
    strcpy(g_ProductRequest.m_AppName, "");
    strcpy(g_ProductRequest.m_AppIcon, "");
    // empty string
    g_CancellationPurchaseID[0] = '\0';
}

void ExampleTerm()
{
    // stop BlackBery AppWorld Billing and release memory
    s3eBBAppWorldBillingStop();
    if (g_Purchases.m_Purchases)
        delete []g_Purchases.m_Purchases;
}

bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    // get string values from user:
    // - Digital Good ID
    if (pressed == g_ButtonSetID)
    {
        GetOSStringValue(g_ProductRequest.m_DigitalGoodId, sizeof(g_ProductRequest.m_DigitalGoodId), "Enter BlackBerry digital good ID", S3E_OSREADSTRING_FLAG_NUMBER);
    }

    // - Digital Good SKU
    if (pressed == g_ButtonSetSKU)
    {
        GetOSStringValue(g_ProductRequest.m_DigitalGoodSKU, sizeof(g_ProductRequest.m_DigitalGoodSKU), "Enter your digital good SKU", 0);
    }

    // - Purchase ID (for cancellation)
    if (pressed == g_ButtonSetPurchID)
    {
        GetOSStringValue(g_CancellationPurchaseID, sizeof(g_CancellationPurchaseID) - 1, "Enter your purchase ID to be cancelled", S3E_OSREADSTRING_FLAG_NUMBER);
        g_ButtonCancelSubscription->m_Enabled = strlen(g_CancellationPurchaseID) > 0;

    }

    // toggle modes:
    // - whole BlackBerry AppWorld Billing mode
    if (pressed == g_ButtonPurchaseMode)
    {
        g_IsLocalMode = !g_IsLocalMode;
        s3eBBAppWorldBillingSetConnectionLocalMode(g_IsLocalMode);
    }

    // - update on Get Purchased Items
    if (pressed == g_ButtonServerUpdate)
    {
        g_IsServerUpdate = !g_IsServerUpdate;
    }

    // buttons for different BlackBerry AppWorld Billing requests:
    // - Purchase
    if (pressed == g_ButtonBuy)
    {
        startWaiting();
        g_CallResult = s3eBBAppWorldBillingRequestPurchase(g_ProductRequest);
    }

    // - Get Purchased
    if (pressed == g_ButtonGetPurchases)
    {
        startWaiting();
        g_CallResult = s3eBBAppWorldBillingGetExistingPurchases(g_IsServerUpdate);
    }

    if (pressed == g_ButtonListNEXT)
    {
        g_PurchaseIdx++;
    }

    if (pressed == g_ButtonListPREV)
    {
        g_PurchaseIdx--;
    }

    // - Get Price
    if (pressed == g_ButtonGetPrice)
    {
        startWaiting();
        g_CallResult = s3eBBAppWorldBillingGetPrice(g_ProductRequest);
    }

    // - Check Existing
    if (pressed == g_ButtonCheckExisting)
    {
        startWaiting();
        g_CallResult = s3eBBAppWorldBillingCheckExisting(g_ProductRequest);
    }

    // - Cancel Subscription
    if (pressed == g_ButtonCancelSubscription)
    {
        startWaiting();
        g_CallResult = s3eBBAppWorldBillingCancelSubsription(g_CancellationPurchaseID);
    }
    return true;
}

void ExampleRender()
{
    const int textHeight = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);

    int xpos = g_ButtonSetSKU->m_XPos + 10 + std::max(g_ButtonSetID->m_Width, g_ButtonSetSKU->m_Width);
    // print auxiliary data
    s3eDebugPrintf(xpos, g_ButtonSetID->m_YPos,        1, "`x666666:%s", g_ProductRequest.m_DigitalGoodId);
    s3eDebugPrintf(xpos, g_ButtonSetSKU->m_YPos,       1, "`x666666:%s", g_ProductRequest.m_DigitalGoodSKU);
    s3eDebugPrintf(xpos, g_ButtonSetPurchID->m_YPos,   1, "`x666666:%s", g_CancellationPurchaseID);
    s3eDebugPrintf(xpos, g_ButtonPurchaseMode->m_YPos, 1, "`x666666%s MODE", g_IsLocalMode ? "LOCAL" : "NETWORK");
    s3eDebugPrintf(xpos, g_ButtonServerUpdate->m_YPos, 1, "`x666666%s", g_IsServerUpdate ? "SERVER UPDATE" : "LOCAL CACHE");

    if (!strlen(g_CancellationPurchaseID))
    {
        s3eDebugPrintf(xpos, g_ButtonCancelSubscription->m_YPos, 1, "`x666666 Set Purchase id for cancellation!");
    }

    int ypos = GetYBelowButtons() + textHeight;
    xpos = 10;

    // print API call result
    if (g_waiting > -1)
    {
        s3eDebugPrintf(xpos, ypos, 1, "`x666666 API call result: S3E_RESULT_%s", (g_CallResult == S3E_RESULT_SUCCESS) ? "SUCCESS" : "ERROR");
        ypos += textHeight;
    }

    // print information depending of callback type
    switch (g_callbacktype)
    {
    case S3E_BBAPPWORLDBILLING_CALLBACK_PURCHASE_ERROR:
        {
            s3eDebugPrintf(xpos, ypos, 1, "`x666666 Purchase Error:");
            ypos += textHeight;

            s3eDebugPrintf(xpos, ypos, 1, "`x666666 ID      : %d", g_Error.m_ErrorId);
            ypos += textHeight;
            s3eDebugPrintf(xpos, ypos, 1, "`x666666 Text    : %s", g_Error.m_ErrorText);
            break;
        }
    case  S3E_BBAPPWORLDBILLING_CALLBACK_PURCHASE_RESPONSE:
        {
            PrintPurchase(xpos, ypos, textHeight, g_Purchase);
            break;
        }
    case  S3E_BBAPPWORLDBILLING_CALLBACK_PURCHASE_EXISTING:
        {
            if (g_Purchases.m_NumberPurchases == 0)
            {
                s3eDebugPrintf(xpos, ypos, 1, "`x666666 callback type : %s", BBAppWorldCallBackType2Str(g_callbacktype));
                ypos += textHeight;
                s3eDebugPrintf(xpos, ypos, 1, "`x666666 No purchased items!");
            }
            else
            {
                g_ButtonListPREV->m_Enabled = (g_PurchaseIdx > 0);
                g_ButtonListNEXT->m_Enabled = (g_PurchaseIdx < (g_Purchases.m_NumberPurchases - 1));

                s3eDebugPrintf(xpos, ypos, 1, "`x666666 Purchase Item #%d of %d:", g_PurchaseIdx + 1, g_Purchases.m_NumberPurchases);
                ypos += textHeight;
                PrintPurchase(xpos, ypos, textHeight, g_Purchases.m_Purchases[g_PurchaseIdx]);
            }
            break;
        }
    case  S3E_BBAPPWORLDBILLING_CALLBACK_PURCHASE_PRICE:
        {
            s3eDebugPrintf(xpos, ypos, 1, "`x666666 callback type :%s", BBAppWorldCallBackType2Str(g_callbacktype));
            ypos += textHeight;
            s3eDebugPrintf(xpos, ypos, 1, "`x666666 Get Digital Good Price ----------------------");
            ypos += textHeight;
            s3eDebugPrintf(xpos, ypos, 1, "`x666666 Price          : %s", g_productPrice.m_Price);
            ypos += textHeight;
            s3eDebugPrintf(xpos, ypos, 1, "`x666666 Initial Period : %s", g_productPrice.m_InitialPeriod);
            ypos += textHeight;
            s3eDebugPrintf(xpos, ypos, 1, "`x666666 Renewal Price  : %s", g_productPrice.m_RenewalPrice);
            ypos += textHeight;
            s3eDebugPrintf(xpos, ypos, 1, "`x666666 Renewal Period : %s", g_productPrice.m_RenewalPeriod);
            break;
        }
    case  S3E_BBAPPWORLDBILLING_CALLBACK_PURCHASE_EXISTS:
        {
            s3eDebugPrintf(xpos, ypos, 1, "`x666666 callback type :%s", BBAppWorldCallBackType2Str(g_callbacktype));
            ypos += textHeight;
            s3eDebugPrintf(xpos, ypos, 1, "`x666666 Check Digital Good exists -------------------");
            ypos += textHeight;
            s3eDebugPrintf(xpos, ypos, 1, "`x666666 EXISTS");
            ypos += textHeight;
            s3eDebugPrintf(xpos, ypos, 1, "`x666666 Subscription : %s", g_productExists.m_Subscription ? "YES" : "NO");
            break;
        }
    case  S3E_BBAPPWORLDBILLING_CALLBACK_CANCEL_SUBSCRIPTION:
        {
            s3eDebugPrintf(xpos, ypos, 1, "`x666666 callback type :%s", BBAppWorldCallBackType2Str(g_callbacktype));
            ypos += textHeight;
            s3eDebugPrintf(xpos, ypos, 1, "`x666666 Cancellation Data ---------------------------");
            ypos += textHeight;
            s3eDebugPrintf(xpos, ypos, 1, "`x666666 Cancelled             : %s", g_cancelInfo.m_Cancelled ? "YES" : "NO");
            ypos += textHeight;
            s3eDebugPrintf(xpos, ypos, 1, "`x666666 Cancelled Purchase ID : %s", g_cancelInfo.m_CancelledPurchaseId);
            break;
        }
    default:
        {
            if (g_waiting > -1) // if we waiting for responses
            {
                uint64 diffSec =  (s3eTimerGetMs() - g_waitMs) / 1000;
                if (diffSec > 0)
                {
                    g_waiting += (int)diffSec;
                    g_waitMs += 1000 * diffSec; // add second in milliseconds
                }
                s3eDebugPrintf(xpos, ypos, 1, "`x666666 waiting for response:(%d)", g_waiting);
            }
        }
    } // switch (g_callbacktype)
}
