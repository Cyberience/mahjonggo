/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EAudioMultiChannel S3E Audio Multi-Channel Example
 *
 * The following allows one to play a music file using S3E's audio functionality
 * on multiple audio channels simultaneously.  It demonstrates how each channel
 * operates independently so that the audio status of one channel, e.g position,
 * play state or volume, can be set without affecting another.
 *
 * The main functions used to achieve this are:
 * - s3eDebugPrint()
 * - s3eKeyboardGetState()
 * - s3eAudioPlay()
 * - s3eAudioPause()
 * - s3eAudioResume()
 * - s3eAudioStop()
 * - s3eAudioSetInt()
 * - s3eAudioGetInt()
 *
 * The application waits for buttons to be pressed before using any of the
 * Audio functionality. Detection of button presses are handled using the
 * generic code in ExamplesMain.cpp
 *
 * The state of each channel is output to screen using the s3eDebugPrint()
 * function.
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eAudioMultiChannel.png
 *
 * MKB File:
 * @include s3eAudioMultiChannel.mkb
 *
 * ICF File:
 * @include data/app.icf
 *
 * Source:
 * @include s3eAudioMultiChannel.cpp
 */

#include "ExamplesMain.h"

enum PlayerState
{
    STATE_PLAYING,
    STATE_PAUSED,
    STATE_STOPPED
};

// Maxed at 5 for purely astetic reasons
static int g_MaxChannels = 5;
static int g_CurrentChannel = 0;

static Button* g_ButtonPlay = 0;
static Button* g_ButtonPause = 0;
static Button* g_ButtonVolUp = 0;
static Button* g_ButtonVolDown = 0;
static Button* g_ButtonResume = 0;
static Button* g_ButtonStop = 0;
static Button* g_ButtonChannelUp = 0;
static Button* g_ButtonChannelDown = 0;
static s3eBool g_MP3Supported = S3E_FALSE;

int32 AudioStopCallback(void* systemData, void* userData)
{
    s3eAudioCallbackData* audioCBData = (s3eAudioCallbackData*)systemData;
    s3eDebugTracePrintf("Audio channel %d stopped", audioCBData->m_ChannelID);
    return 0;
}

void ExampleInit()
{
    // Have only 5 for purposes of writing debug info to screen
    int maxC = s3eAudioGetInt(S3E_AUDIO_NUM_CHANNELS);
    if (maxC < g_MaxChannels)
        g_MaxChannels   = maxC;

    g_ButtonPlay        = NewButton("Play");
    g_ButtonPause       = NewButton("Pause");
    g_ButtonResume      = NewButton("Resume");
    g_ButtonStop        = NewButton("Stop");
    g_ButtonVolUp       = NewButton("Volume Up");
    g_ButtonVolDown     = NewButton("Volume Down");
    g_ButtonChannelUp   = NewButton("Channel Up");
    g_ButtonChannelDown = NewButton("Channel Down");

    g_MP3Supported = s3eAudioIsCodecSupported(S3E_AUDIO_CODEC_MP3);
    s3eAudioRegister(S3E_AUDIO_STOP, AudioStopCallback, NULL);

    // disable channel buttons if only 1 channel is available
    if (g_MaxChannels == 1)
    {
        g_ButtonChannelUp->m_Enabled = false;
        g_ButtonChannelDown->m_Enabled = false;
    }
}

void ExampleTerm()
{
    s3eDebugTracePrintf("Audio playback finished");
    s3eAudioStop();
}

/*
 * If a button has been pressed then start/pause/resume/stop playback.
 */
bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    if (!g_MP3Supported)
        return true;

    s3eAudioStatus status = (s3eAudioStatus)s3eAudioGetInt(S3E_AUDIO_STATUS);

    if (pressed == g_ButtonPlay)
    {
        s3eAudioPlay("test.mp3");
    }

    if (pressed == g_ButtonPause && (status == S3E_AUDIO_PLAYING))
    {
        s3eAudioPause();
    }

    if (pressed == g_ButtonVolUp)
    {
        int vol = s3eAudioGetInt(S3E_AUDIO_VOLUME);
        s3eAudioSetInt(S3E_AUDIO_VOLUME, vol+10);
    }

    if (pressed == g_ButtonVolDown)
    {
        int vol = s3eAudioGetInt(S3E_AUDIO_VOLUME);
        s3eAudioSetInt(S3E_AUDIO_VOLUME, vol-10);
    }

    if (pressed == g_ButtonResume && (status == S3E_AUDIO_PAUSED))
    {
        s3eAudioResume();
    }

    if (pressed == g_ButtonStop)
    {
        s3eAudioStop();
    }

    if (pressed == g_ButtonChannelUp)
    {
        ++g_CurrentChannel;
        if (g_CurrentChannel >= g_MaxChannels)
        {
            // channels are from 0 to MAX-1
            g_CurrentChannel = g_MaxChannels - 1;
        }
        else
        {
            s3eAudioSetInt(S3E_AUDIO_CHANNEL, g_CurrentChannel);
        }
    }

    if (pressed == g_ButtonChannelDown)
    {
        --g_CurrentChannel;
        if (g_CurrentChannel < 0)
            g_CurrentChannel = 0;
        else
            s3eAudioSetInt(S3E_AUDIO_CHANNEL, g_CurrentChannel);
    }

    return true;
}

/*
 * The following function outputs the status of each audio channel.
 * It iterates through each channel and outputs the status, position
 * and volume. Each string is output using the s3eDebugPrint() function.
 */
void ExampleRender()
{
    const int textHeight = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);

    int ypos = GetYBelowButtons() + textHeight * 2;

    if (!g_MP3Supported)
    {
        g_ButtonPlay->m_Enabled = false;
        s3eDebugPrint(10, ypos, "`x666666Device does not support mp3 playback", 0);
        return;
    }

    s3eDebugPrintf(10, ypos, false, "`x666666Current channgel: %d", g_CurrentChannel);

    ypos += 3 * textHeight;

    for (int iCh = 0; iCh < g_MaxChannels; ++iCh)
    {
        // Change channels before getting current state
        s3eAudioSetInt(S3E_AUDIO_CHANNEL, iCh);

        // Display status
        int32 pos = s3eAudioGetInt(S3E_AUDIO_POSITION);
        int32 vol = s3eAudioGetInt(S3E_AUDIO_VOLUME);

        switch ((s3eAudioStatus)s3eAudioGetInt(S3E_AUDIO_STATUS))
        {
            case S3E_AUDIO_PLAYING:
                s3eDebugPrintf(10, ypos, false, "`x666666Channel %d: Playing: Pos %d.%d: Vol: %d", iCh, pos / 1000, pos % 1000, vol);
                break;
            case S3E_AUDIO_PAUSED:
                s3eDebugPrintf(10, ypos, false, "`x666666Channel %d: Paused: Pos %d.%d: Vol: %d", iCh, pos / 1000, pos % 1000, vol);
                break;
            case S3E_AUDIO_STOPPED:
                s3eDebugPrintf(10, ypos, false, "`x666666Channel %d: Stopped: Pos %d.%d: Vol: %d", iCh, pos / 1000, pos % 1000, vol);
                break;
            default:
                break;
        }

        ypos += 2 * textHeight;
    }

    // Restore the channel
    s3eAudioSetInt(S3E_AUDIO_CHANNEL, g_CurrentChannel);
}
