/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EContactsWrite S3E Contacts Write Example
 *
 * This example demonstrates the full functionality of s3eContacts.
 * It allows the user to add, modify and delete contact records.
 *
 * The Button functionality from ExamplesMain is used to provide a simple UI
 * for selecting records, fields and entries.
 *
 * The AppendMessage() functionality from ExamplesMain is used to display
 * useful information to the user as a scrolling list of text.
 *
 * Note that to maintain simplicity of code, this example uses only s3e APIs.
 * A real contacts application would probably make use of higher level APIs for
 * UI display, e.g. IwUI.
 *
 * The example accepts input for new values via s3eKeyboard and the
 * OS Read String extension. In case the device does not support either of these
 * - i.e. it does not have a keypad or support OS Read String - the example
 * presents a 'GENERATE new value' button to generate random test values.
 *
 * WARNING: Running this example on a device and changing contact values will
 * permanently alter the actual contacts database on the device. s3eContacts
 * performs actions live to the database and does not provide any built-in undo
 * functionality.
 *
 * @image html s3eContactsWriteImage.png
 *
 * @include s3eContactsWrite.cpp
 */

#include "s3e.h"
#include "s3eContacts.h"
#include "ExamplesMain.h"

#include "s3eOSReadString.h" // For text entry through iPhone native input
#include <stdlib.h>

// Contacts display globals
#define NUM_MESSAGES 5
#define MESSAGE_LEN 60
#define BUTTON_STRING_MAX 20
#define BUTTONS_PER_SCREEN 5

static int g_NumRecords = -1;
static int g_CurrentPage = 0;
static char g_RecordsString[64];
static char g_PageString[40];
static bool g_Writable = false;
static char g_Message[MESSAGE_LEN];
static int g_CurrentRecordIdx = -1;
static int g_CurrentField = -1;
static bool g_EditField = false;
static int g_EditingEntry = -1;

// For character input (see s3eTextInput example)
static int g_CharEnabled = 0;
static char g_EntryString[S3E_CONTACTS_STRING_MAX];
static int g_NumEntryChars = 0;

// Buttons for browsing contacts
static Button* g_ButtonGetContacts = 0;
static Button* g_ButtonAddContact = 0;
static Button* g_ButtonNextPage = 0;
static Button* g_ButtonPrevPage = 0;
static Button* g_ButtonContact[BUTTONS_PER_SCREEN];

// Buttons for editing a contact
static Button* g_ButtonBack = 0;
static Button* g_ButtonDelete = 0;
static Button* g_ButtonEditField = 0;
static Button* g_ButtonDone = 0;
static Button* g_ButtonGenerateValue = 0;
static Button* g_ButtonFieldEntries[BUTTONS_PER_SCREEN]; //Limit the amount of values displayed for the example

void ResetDisplay()
{
    DeleteButtons();

    g_ButtonGetContacts = 0;
    g_ButtonAddContact = 0;
    g_ButtonNextPage = 0;
    g_ButtonPrevPage = 0;
    g_ButtonBack = 0;
    g_ButtonDelete = 0;
    g_ButtonEditField = 0;
    g_ButtonDone = 0;
    g_ButtonGenerateValue = 0;

    for (int i=0; i < BUTTONS_PER_SCREEN; i++)
    {
        g_ButtonContact[i] = 0;
        g_ButtonFieldEntries[i] = 0;
    }
}

// Remove all buttons and create new ones based on current state of pages and contacts
void SetupRecordButtons()
{
    ResetDisplay();

    if (g_NumRecords == -1)
    {
        g_ButtonGetContacts = NewButton("Get Contacts List");
        snprintf(g_RecordsString, sizeof(g_RecordsString), "`x666666- press button to load contacts -");
    }
    else
    {
        // Get page count
        int32 pages = g_NumRecords / BUTTONS_PER_SCREEN;
        if (g_NumRecords % BUTTONS_PER_SCREEN)
            pages += 1;

        if (g_CurrentPage > 0 && g_CurrentPage >= pages)
            g_CurrentPage = pages-1;

        // Add a button for each contact on the current page. Display ID and name if available.
        for (int i=0; i < BUTTONS_PER_SCREEN; i++)
        {
            int32 contactIdx = g_CurrentPage * BUTTONS_PER_SCREEN + i;
            if (contactIdx >= g_NumRecords)
                break;

            int32 contactId = s3eContactsGetUID(contactIdx);

            // Find the most useful label for the record or leave blank if no fields are set
            char buttonString[BUTTON_STRING_MAX] = "";
            char contactIdString[S3E_CONTACTS_STRING_MAX] = "";

            // Try these fields in order until a non-blank one is found
            s3eContactsField fieldsToTry[] = { S3E_CONTACTS_FIELD_FIRST_NAME,
                                               S3E_CONTACTS_FIELD_DISPLAY_NAME,
                                               S3E_CONTACTS_FIELD_NICKNAME,
                                               S3E_CONTACTS_FIELD_LAST_NAME,
                                               S3E_CONTACTS_FIELD_MOBILE_PHONE,
                                               S3E_CONTACTS_FIELD_HOME_PHONE,
                                               S3E_CONTACTS_FIELD_WORK_PHONE,
                                               S3E_CONTACTS_FIELD_EMAIL_ADDR };
            const int FIELDS = sizeof(fieldsToTry)/sizeof(*fieldsToTry);
            bool foundValue = false;

            for (int j = 0; j < FIELDS && !foundValue; j++)
            {
                int numEntries = s3eContactsGetNumEntries(contactId, fieldsToTry[j]);
                for (int k = 0; k < numEntries && !foundValue; k++)
                {
                    if (s3eContactsGetField(contactId, fieldsToTry[j], contactIdString, S3E_CONTACTS_STRING_MAX, k) == S3E_RESULT_ERROR)
                    {
                        snprintf(g_Message, sizeof(g_Message), "`x666666Error (%d) accessing field %d at index %d for record %d!", (int)s3eContactsGetError(), fieldsToTry[j], k, (int)contactId);
                        AppendMessage(g_Message);
                    }
                    else
                    {
                        foundValue = (strcmp(contactIdString, "") != 0);
                    }
                }
            }

            // No useful information was found
            if (!foundValue)
            {
                snprintf(contactIdString, sizeof(contactIdString), "<no name/num>");
            }

            snprintf(buttonString, sizeof(buttonString)-1, "(%d) %s", (int)contactId, contactIdString);
            g_ButtonContact[i] = NewButton(buttonString);
        }

        // Allow to reload in case of external changes
        g_ButtonGetContacts = NewButton("Reload Contacts List");
        snprintf(g_RecordsString, sizeof(g_RecordsString), "`x000066Amount of contacts: %d", g_NumRecords);

        if (g_Writable)
        {
            g_ButtonAddContact = NewButton("Append Blank Contact");
        }

        // Buttons and text for pages
        if (g_CurrentPage > 0)
            g_ButtonPrevPage = NewButton("Prev Page");

        if (g_NumRecords > BUTTONS_PER_SCREEN * (g_CurrentPage+1))
            g_ButtonNextPage = NewButton("Next Page");

        snprintf(g_PageString, sizeof(g_PageString), "`x000066Page: %d/%d", g_CurrentPage+1, (int)pages);
    }
}

// Count contacts and set-up display values. Internally, new UIDs will be assigned.
void ReloadContacts()
{
    g_CurrentRecordIdx = -1;

    // Refresh internal UID list
    s3eContactsUpdate();
    g_NumRecords = s3eContactsGetNumRecords();
    g_Writable = s3eContactsGetInt(S3E_CONTACTS_WRITABLE) == 1;

    if (g_NumRecords == -1)
    {
        snprintf(g_Message, sizeof(g_Message), "`x660000Failed to get amount of contact records!");
        AppendMessage(g_Message);
        snprintf(g_Message, sizeof(g_Message), "`x660000Error: %d - %s", s3eContactsGetError(), s3eContactsGetErrorString());
    }
    else
    {
        snprintf(g_Message, sizeof(g_Message), "`x666666Got amount of contact records: %d", (int)g_NumRecords);
        g_CurrentPage = 0;
    }

    AppendMessage(g_Message);
    SetupRecordButtons();
}

// replace new-lines with comas, to make sure line is printed printed proporly
void mergeLinesInplace(char* str)
{
    char* pNewLineChar;
    while ((pNewLineChar = strchr(str, '\n')) != NULL)
    {
        *pNewLineChar = ',';
    }
}

// Setup buttons for displaying fields of a selected record
void SetupFieldButtons()
{
    ResetDisplay();
    ClearMessages();

    g_ButtonBack = NewButton("<- BACK to contacts");
    g_ButtonNextPage = NewButton("Next Field ->");
    g_ButtonPrevPage = NewButton("Prev Field <-");
    g_ButtonDelete = NewButton("DELETE contact");

    // Print field name to screen through Message system...
    // Display record and field information
    int32 uid = s3eContactsGetUID(g_CurrentRecordIdx);
    snprintf(g_RecordsString, sizeof(g_RecordsString), "`x000066Record UID: %d", (int)uid);
    snprintf(g_PageString, sizeof(g_PageString), "`x000066Field(%d): %s", g_CurrentField, s3eContactsGetFieldName((s3eContactsField)g_CurrentField));

    // Check if platform supports the current field
    if (!s3eContactsFieldSupported((s3eContactsField)g_CurrentField))
    {
        snprintf(g_Message, sizeof(g_Message), "`x660000<field not supported on current platform>");
        AppendMessage(g_Message);
    }
    else
    {
        if (g_Writable)
            g_ButtonEditField = NewButton("EDIT this field");

        int32 entries = s3eContactsGetNumEntries(uid, (s3eContactsField)g_CurrentField);
        char fieldValueString[S3E_CONTACTS_STRING_MAX];

        if (entries < 0)
        {
            AppendMessage("`x666666<entry is unavailable>");
        }
        else if (entries == 0)
        {
            AppendMessage("`x666666<no values set>");
        }
        else
        {
            // Print field's values (printing in reverse to display nicely with AppendMessage())
            for (int i = entries-1; i >= 0; i--)
            {
                if (i >= NUM_MESSAGES-1)
                    continue;

                if (s3eContactsGetField(uid, (s3eContactsField)g_CurrentField, fieldValueString, S3E_CONTACTS_STRING_MAX, i) == S3E_RESULT_ERROR)
                {
                    snprintf(g_Message, sizeof(g_Message)-1, "`x660000Error (%d) accessing value", (int)s3eContactsGetError());
                }
                else
                {
                    mergeLinesInplace(fieldValueString);
                    snprintf(g_Message, sizeof(g_Message), "`x666666(%d) %s", i, strlen(fieldValueString) > 0 ? fieldValueString : "<empty value>");
                    g_Message[sizeof(g_Message)-1] = 0;
                }

                AppendMessage(g_Message);
            }
        }
    }
}

// Setup buttons for displaying entries of a selected record and field
void SetupEditEntriesButtons()
{
    ResetDisplay();
    ClearMessages();

    g_ButtonBack = NewButton("<- BACK to fields");

    int32 uid = s3eContactsGetUID(g_CurrentRecordIdx);
    int32 entries = s3eContactsGetNumEntries(uid, (s3eContactsField)g_CurrentField);

    if (entries)
        AppendMessage("`x000066Select an entry to edit it...");
    else
        AppendMessage("`x000066No entries for this field. Add new?");

    // Add buttons for editing/adding value for the field (example only supports editing BUTTONS_PER_SCREEN values)
    for (int i=0; i < BUTTONS_PER_SCREEN; i++)
    {
        int32 maxValues = s3eContactsGetMaxEntries((s3eContactsField)g_CurrentField);

        // Run out of entries
        if (i >= entries)
        {
            // Add new
            if (i == 0 || maxValues == S3E_CONTACTS_UNLIMITED_MAX_ENTRIES || i < maxValues)
            {
                // Button for adding a new value
                g_ButtonFieldEntries[i] = NewButton("ADD new value");
            }

            break;
        }

        // Edit existing
        char buttonString[BUTTON_STRING_MAX];
        char entryString[S3E_CONTACTS_STRING_MAX];

        if (s3eContactsGetField(uid, (s3eContactsField)g_CurrentField, entryString, S3E_CONTACTS_STRING_MAX, i) == S3E_RESULT_ERROR)
        {
            snprintf(g_Message, sizeof(g_Message), "`x660000Error (%d) accessing field %d at index %d for record %d!", (int)s3eContactsGetError(), g_CurrentField, i, (int)uid);
            AppendMessage(g_Message);
            AppendMessage("`x660000Contact may have been updated externally - please reload contacts");
            break;
        }

        snprintf(buttonString, sizeof(buttonString), "(%d) %s", i, entryString);
        buttonString[BUTTON_STRING_MAX-1] = 0;
        g_ButtonFieldEntries[i] = NewButton(buttonString);
    }
}

// Setup buttons for editing the value of a selected entry
void SetupSetEntryValueButtons()
{
    ResetDisplay();
    ClearMessages();

    g_EntryString[0] = 0;
    g_NumEntryChars = 0;
    g_ButtonBack = NewButton("<- Cancel");
    g_ButtonEditField = NewButton("SET value...");
    g_ButtonGenerateValue = NewButton("GENERATE new value");

    // Option to delete if there is already a value set
    if (g_EditingEntry < s3eContactsGetNumEntries(s3eContactsGetUID(g_CurrentRecordIdx), (s3eContactsField)g_CurrentField))
        g_ButtonDelete = NewButton("DELETE value");

    AppendMessage("`x000066'Generate new value' will make up value for you");
    if (!s3eOSReadStringAvailable() && !s3eKeyboardGetInt(S3E_KEYBOARD_HAS_NUMPAD) && !s3eKeyboardGetInt(S3E_KEYBOARD_HAS_ALPHA))
        AppendMessage("`x000066Device doesn't appear to have keypad or OSRead string support:");
    AppendMessage("`x000066Choose 'SET value' to enter new text for the entry");
}

// Generate random data from numbers and a small set of values
// This is useful for demonstrating SetField() for devices that do not have a keyboard and
// do dot support Os Read String. In a real app, more advanced functionality than just s3e
// APIs should be used, for example an IwUI touch keyboard.
const char* GetFormatLine(s3eContactsField field)
{
    const char *FAKE_NAMES[] = { "John", "Dave", "Jane", "Jack", "Bill", "Anne", "Greg", "Suzi", "Iris", "Jill" };
    const char *FAKE_NICKNAMES[] = { "Doc", "Dopey", "Happy", "Sleepy", "Grumpy", "Sneezy", "Hefty", "Tiny", "Frumpy", "Buddy" };
    const int NUM_NAMES = sizeof(FAKE_NAMES)/sizeof(*FAKE_NAMES);
    const int NUM_NICKNAMES = sizeof(FAKE_NICKNAMES)/sizeof(*FAKE_NICKNAMES);

    switch (field)
    {
        case S3E_CONTACTS_FIELD_DISPLAY_NAME:
        case S3E_CONTACTS_FIELD_FIRST_NAME:
        case S3E_CONTACTS_FIELD_MIDDLE_NAME:       return FAKE_NAMES[rand() % NUM_NAMES];
        case S3E_CONTACTS_FIELD_NICKNAME:          return FAKE_NICKNAMES[rand() % NUM_NICKNAMES];
        case S3E_CONTACTS_FIELD_LAST_NAME:         return "McTestcontact";
        case S3E_CONTACTS_FIELD_PHONE:
        case S3E_CONTACTS_FIELD_HOME_PHONE:
        case S3E_CONTACTS_FIELD_MOBILE_PHONE:
        case S3E_CONTACTS_FIELD_WORK_PHONE:        return "+01234%06d";
        case S3E_CONTACTS_FIELD_EMAIL_ADDR:        return "test%d@testcontactworld.com";
        case S3E_CONTACTS_FIELD_URL:               return "www.testcontactworld.com/home/user%d";
        case S3E_CONTACTS_FIELD_ADDRESS:           return "%d Testcontact Ave";
        case S3E_CONTACTS_FIELD_POSTAL_CODE:       return "AA%dZZ";
        case S3E_CONTACTS_FIELD_COUNTRY:           return "UK";
        case S3E_CONTACTS_FIELD_PHOTO_URL:         return "www.testcontactphotos.com/%d.jpg";
        case S3E_CONTACTS_FIELD_HONORIFIC_PREFIX:  return "Mr.";
        case S3E_CONTACTS_FIELD_HONORIFIC_SUFFIX:  return "Sr.";
        case S3E_CONTACTS_FIELD_REGION:            return "Greater London";
        case S3E_CONTACTS_FIELD_PHONE_TYPE:
        case S3E_CONTACTS_FIELD_EMAIL_ADDR_TYPE:   return "work";
        case S3E_CONTACTS_FIELD_IM:                return "%d";
        case S3E_CONTACTS_FIELD_IM_TYPE:           return "ICQ";
        case S3E_CONTACTS_FIELD_ORGANISATION:      return "Office-%d Ltd.";
        case S3E_CONTACTS_FIELD_ORGANISATION_DEPT: return "AREA #%d";
        case S3E_CONTACTS_FIELD_BIRTHDAY:          return "22 feb 19%d";
        case S3E_CONTACTS_FIELD_NOTE:              return "some notes are available\nabout current person";
        default:                                   return NULL;
    }
    return NULL;
}

void GenerateFieldValue(s3eContactsField field, char* strBuf, size_t bufSize)
{
    *strBuf = '\0';
    srand((int)s3eTimerGetMs());
    int randRange = 100;

    const char* format = GetFormatLine(field);
    if (format == NULL)
        return;

    switch(field)
    {
        case S3E_CONTACTS_FIELD_DISPLAY_NAME:
            snprintf(strBuf, bufSize, "%s%d", format, rand() % randRange);
            break;
        case S3E_CONTACTS_FIELD_IM:
        case S3E_CONTACTS_FIELD_PHONE:
        case S3E_CONTACTS_FIELD_HOME_PHONE:
        case S3E_CONTACTS_FIELD_MOBILE_PHONE:
        case S3E_CONTACTS_FIELD_WORK_PHONE:
            randRange = 10000000;
        default:
            snprintf(strBuf, bufSize, format, rand() % randRange);
            break;
    }
}

// Edit an entry: Process buttons and keyboard text entry then update the
// contact record accordingly. For more information on keyboard use and
// text input, see the s3eTextInput and s3eOSReadString examples.
void EnterText(Button* pressed)
{
    // Allow to save once a string has been entered
    if (!g_ButtonDone && g_NumEntryChars)
        g_ButtonDone = NewButton("SAVE to entry");

    if (pressed)
    {
        if (pressed == g_ButtonEditField)
        {
            g_EntryString[0] = '\0';
            g_NumEntryChars = 0;

            if (s3eOSReadStringAvailable())
            {
                // Use native text entry where supported (e.g. on iPhone - see s3eOSReadString example)
                strlcpy(g_EntryString, s3eOSReadStringUTF8("Enter new string for entry"), S3E_CONTACTS_STRING_MAX);
                g_NumEntryChars = strlen(g_EntryString);

                if (g_NumEntryChars)
                {
                    ClearMessages();
                    snprintf(g_Message, sizeof(g_Message), "`x666666%s", g_EntryString);
                    AppendMessage(g_Message);
                    AppendMessage("`x000066New field entry string:");
                }
            }
            else
            {
                // Read characters one at a time through s3eKeyboard (see s3eTextInput example)
                // Note that, in this example, pressing a number key may cause the correspondingly
                // numbered button to be pressed!
                g_CharEnabled = true;
                s3eKeyboardSetInt(S3E_KEYBOARD_GET_CHAR, g_CharEnabled);

                // Remove SET button for clarity
                ResetDisplay();
                ClearMessages();
                g_ButtonBack = NewButton("<- Cancel");
                if (g_EditingEntry < s3eContactsGetNumEntries(s3eContactsGetUID(g_CurrentRecordIdx), (s3eContactsField)g_CurrentField))
                    g_ButtonDelete = NewButton("DELETE value");

                if (!s3eKeyboardGetInt(S3E_KEYBOARD_HAS_NUMPAD) && !s3eKeyboardGetInt(S3E_KEYBOARD_HAS_ALPHA))
                    AppendMessage("`x000066This device does not appear to support keyboard entry...");

                AppendMessage("`x000066Type to enter a new value");

                // Clear any chracters that are already internally buffered
                s3eWChar ch;
                while ((ch = s3eKeyboardGetChar()) != S3E_WEOF && !s3eDeviceCheckQuitRequest()){}
            }
        }
        else if (pressed == g_ButtonGenerateValue)
        {
            GenerateFieldValue((s3eContactsField)g_CurrentField, g_EntryString, sizeof(g_Message));
            g_NumEntryChars = strlen(g_EntryString);

            ClearMessages();
            AppendMessage("`x000066Choose 'Save Entry' to save this value");
            snprintf(g_Message, sizeof(g_Message), "`x666666%s", g_EntryString);
            AppendMessage(g_Message);
            AppendMessage("`x000066New field entry string:");
        }
        else if (pressed == g_ButtonBack)
        {
            // Return to displaying list of entries
            g_CharEnabled = false;
            g_EditingEntry = -1;
            SetupEditEntriesButtons();
        }
        else
        {
            if (pressed == g_ButtonDone)
            {
                // Set record using accumulated string from text entry
                if (s3eContactsSetField(s3eContactsGetUID(g_CurrentRecordIdx), (s3eContactsField)g_CurrentField, g_EntryString, g_EditingEntry) == S3E_RESULT_ERROR)
                    AppendMessage("`x660000Failed to set entry value!");
                else
                    AppendMessage("`x006600Entry value set!");
            }
            else if (pressed == g_ButtonDelete)
            {
                ClearMessages();
                // Set record to empty string or NULL to delete it
                if (s3eContactsSetField(s3eContactsGetUID(g_CurrentRecordIdx), (s3eContactsField)g_CurrentField, "", g_EditingEntry) == S3E_RESULT_ERROR)
                    AppendMessage("`x660000Failed to delete entry!");
                else
                    AppendMessage("`x006600Entry deleted!");
            }

            g_CharEnabled = false;
            g_NumEntryChars = 0;
            ResetDisplay();
            g_ButtonBack = NewButton("<- Back");
        }
    }

    // Read in chars. For simplicity, the example just truncates from wide to regular chars.
    // An application should convert to UTF8.
    if (g_CharEnabled)
    {
        s3eWChar ch;

        if (g_NumEntryChars < S3E_CONTACTS_STRING_MAX-1)
        {
            while ((ch = s3eKeyboardGetChar()) != S3E_WEOF && !s3eDeviceCheckQuitRequest())
            {
                g_EntryString[g_NumEntryChars] = (char)ch;
                g_EntryString[g_NumEntryChars+1] = 0;
                g_NumEntryChars++;

                ClearMessages();
                snprintf(g_Message, sizeof(g_Message), "`x666666%s", g_EntryString);
                g_Message[sizeof(g_Message)-1] = 0;
                AppendMessage(g_Message);
                AppendMessage("`x000066New field entry string:");
            }
        }
    }
}

void DeleteRecord(uint32 recordIdx)
{
    if (s3eContactsDelete(s3eContactsGetUID(recordIdx)) == S3E_RESULT_SUCCESS)
    {
        snprintf(g_Message, sizeof(g_Message), "`x006600Record %d deleted", (int)recordIdx);
        AppendMessage(g_Message);
        ReloadContacts();
    }
    else
    {
        snprintf(g_Message, sizeof(g_Message), "`x660000Failed to delete record %d", (int)recordIdx);
        AppendMessage(g_Message);
    }
}

void ExampleInit()
{
    if (!s3eContactsAvailable())
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "s3eContacts extension not found");
        s3eDeviceExit(1);
    }

    // Determine if records are writable
    g_Writable = s3eContactsGetInt(S3E_CONTACTS_WRITABLE) == 1;

    SetupRecordButtons();
    InitMessages(NUM_MESSAGES, MESSAGE_LEN);
}

void ExampleTerm()
{
}

bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    if (pressed)
    {
        // Connect as server, client or peer.
        if (pressed == g_ButtonGetContacts)
        {
            ReloadContacts();
        }

        else if (pressed == g_ButtonAddContact)
        {
            int32 newRecordId = s3eContactsCreate();

            if (newRecordId == -1)
            {
                snprintf(g_Message, sizeof(g_Message), "`x660000Failed to add new record!");
                AppendMessage(g_Message);
                snprintf(g_Message, sizeof(g_Message), "`x660000Error: %d - %s", s3eContactsGetError(), s3eContactsGetErrorString());
                AppendMessage(g_Message);
            }
            else
            {
                snprintf(g_Message, sizeof(g_Message), "`x006600New record added with UID %d", (int)newRecordId);
                AppendMessage(g_Message);
                ReloadContacts();
            }
        }

        // Selecting contact records
        else if (g_CurrentRecordIdx == -1)
        {
            if (pressed == g_ButtonNextPage)
            {
                g_CurrentPage += 1;
                SetupRecordButtons();
            }
            else if (pressed == g_ButtonPrevPage)
            {
                g_CurrentPage -= 1;
                SetupRecordButtons();
            }
            else
            {
                // Select a contact
                for (int i = 0; i < BUTTONS_PER_SCREEN; i++)
                {
                    if (pressed == g_ButtonContact[i])
                    {
                        g_CurrentRecordIdx = g_CurrentPage * BUTTONS_PER_SCREEN + i;
                        snprintf(g_Message, sizeof(g_Message), "`x006600Record %d selected", g_CurrentRecordIdx);
                        AppendMessage(g_Message);

                        // Set to display the first field for this contact
                        g_CurrentField = 0;
                        SetupFieldButtons();
                        break; // since buttons will have changed
                    }
                }
            }
        }
        else // A record is already selected
        {
            if (!g_EditField) // Viewing a record
            {
                // Scroll through fields for the current record
                if (pressed == g_ButtonNextPage)
                {
                    g_CurrentField = (g_CurrentField + 1) % S3E_CONTACTS_FIELD_COUNT;
                    SetupFieldButtons();
                }
                else if (pressed == g_ButtonPrevPage)
                {
                    g_CurrentField = (g_CurrentField + S3E_CONTACTS_FIELD_COUNT - 1) % S3E_CONTACTS_FIELD_COUNT;
                    SetupFieldButtons();
                }
                else if (pressed == g_ButtonBack)
                {
                    ReloadContacts();
                }
                else if (pressed == g_ButtonDelete)
                {
                    DeleteRecord(g_CurrentRecordIdx);
                }
                else if (pressed == g_ButtonEditField)
                {
                    g_EditField = true;
                    SetupEditEntriesButtons();
                }
            }
            else if (g_EditingEntry == -1) // A field is selected
            {
                if (pressed == g_ButtonBack)
                {
                    g_EditField = false;
                    SetupFieldButtons();
                }

                else
                {
                    // Select a field to edit
                    for (int i = 0; i < BUTTONS_PER_SCREEN; i ++)
                    {
                        if (pressed == g_ButtonFieldEntries[i])
                        {
                            g_EditingEntry = i;
                            SetupSetEntryValueButtons();
                            return true; // to avoid calling EnterText after buttons have changed
                        }
                    }
                }
            }
        }
    }

    if (g_EditingEntry != -1)
    {
        EnterText(pressed);
    }

    return true;
}

void ExampleRender()
{
    int x = 20;
    int y = GetYBelowButtons();

    s3eDebugPrintf(x, y, 0, "%s", g_RecordsString);

    if (g_NumRecords > 0)
    {
        y += 12;
        s3eDebugPrintf(x, y, 0, "%s", g_PageString);
    }

    if (!g_Writable)
    {
        y += 12;
        s3eDebugPrintf(x, y, 0, "`x660000Device reports contacts are not writable!");
    }

    y += 24;

    PrintMessages(x, y);
}
