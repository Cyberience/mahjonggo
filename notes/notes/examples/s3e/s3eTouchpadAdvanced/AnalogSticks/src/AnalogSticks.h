/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
#ifndef IW_ANALOG_STICKS_H
#define IW_ANALOG_STICKS_H

#include "IwGeomFVec2.h"
#include <vector>

/**
 * Class for converting touchpad events to dual analog stick coordinates
 * Provides on screen touch pad functionality for cross platform use
 *
 * All coordinates are normalized, and thus rotation insensitive
 */
class CAnalogSticks
{
public:
   /**
    * \return the left stick position, normalized x and y coordinates (-1.0 to 1.0)
    */
    CIwFVec2 GetLeftStickPos();

   /**
    * \return the right stick position, normalized x and y coordinates (-1.0 to 1.0)
    */
    CIwFVec2 GetRightStickPos();

   /**
    * \return the left stick position as a normalized polar coordinate
    */
    void GetLeftStickPolar(float *angle, float* mag);

   /**
    * \return the right stick position as a normalized polar coordinate
    */
    void GetRightStickPolar(float *angle, float* mag);

    /**
     * Sets the normalized center point of the left stick (0.0 to 1.0)
     */
    void SetLeftCenter(CIwFVec2 pos);

    /**
     * Sets the normalized center coordinate of the right stick (0.0 to 1.0)
     */
    void SetRightCenter(CIwFVec2 pos);

    /**
     * Returns true if the analog stick hardware is available.
     */
    static bool IsAvailable();

    enum AbstractDirectionKeyUse
    {
        MAP_DIRECTIONS_TO_LEFT_ANALOG,
        MAP_DIRECTIONS_TO_RIGHT_ANALOG,
        MAP_DIRECTIONS_NONE,
    };

    /**
     * Listens for the abstract direction buttons, mapping them to either the left or right analogue axis.
     */
    void SetUseAbstractDirections(AbstractDirectionKeyUse);

    /**
     * \return the current astract direction key axis mapping
     */
    AbstractDirectionKeyUse GetUseAbstractDirections();

    /**
     * Call this method in the game loop in order to update the analog stick positions
     */
    void Update();

    /**
     * Initializes the analog sticks
     */
    CAnalogSticks();

    /**
     * Releases the analog sticks
     */
    ~CAnalogSticks();

    /**
     * Use an on screen axis with a first touch in the circular area of radius r, centered on (x,y)
     * Use r = 0 to turn off the on screen axis
     * Coordinates are normalized to screen size
     * R normalized to screen width
     * Maps to the left stick position
     */
    void SetOnScreenStickLeft (float x, float y, float r, float maxShift = 0.0);

    /**
     * Use an on screen axis with a first touch in the circular area of radius r, centered on (x,y)
     * Use r = 0 to turn off the on screen axis
     * Coordinates are normalized to screen size
     * R normalized to screen width
     * Maps to the right stick position
     */
    void SetOnScreenStickRight(float x, float y, float r, float maxShift = 0.0);

    /**
     * Draws debug primitives showing on screen axes wth touch interaction
     * (Uses Iw2D for rendering)
     */
    void DrawDebugScreenAxes();

private:
    static bool m_available;
    CIwFVec2 m_LeftStick, m_RightStick;
    CIwFVec2 m_LeftCenter, m_RightCenter;

    CIwFVec2 m_LeftTouchpad, m_RightTouchpad;
    CIwFVec2 m_DirectionalPad;

    std::vector<int32> *m_LeftTouches, *m_RightTouches;

    AbstractDirectionKeyUse m_UseAbstractDirections;

    struct SScreenStick
    {
        // initial position of stick (to surface width/height)
        CIwFVec2 m_Position;
        // normalized size of stick touch area (to surface width)
        float m_Radius;
        // max distance the touch area can be shifted
        float m_MaxShift;

        // current normalized coordinate of touch area (to surface width/height)
        CIwFVec2 m_TouchPos;
        // touch id we are tracking
        uint32 m_TouchId;
        // normalized position of stick within radius of touch area
        CIwFVec2 m_StickPos;

        // where the stick output is routed
        CIwFVec2* m_MappedStick;
    } m_OnScreenStick[2];

    friend int32 TouchCallback(void* sysData, void* userData);
    friend int32 MotionCallback(void* sysData, void* userData);

    friend int32 PointerCallback(void* sysData, void* userData);
    friend int32 PointerMotionCallback(void* sysData, void* userData);

    friend int32 SuspendCallback(void* sysData, void* userData);
};

#endif
