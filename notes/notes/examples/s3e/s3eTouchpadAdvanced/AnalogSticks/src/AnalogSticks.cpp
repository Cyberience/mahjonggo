/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
#include "AnalogSticks.h"
#include "s3eTouchpad.h"
#include "s3eKeyboard.h"
#include "s3eSurface.h"
#include "s3ePointer.h"

#include <math.h>
#include <assert.h>
#include <algorithm>

#define INVALID_TOUCH_ID 0xFFFFFFFF

#define ANALOG_STICKS_DEBUG_AXES
#ifdef ANALOG_STICKS_DEBUG_AXES
#include "Iw2D.h"
#endif

#include <sys/param.h>

int32 TouchCallback(void* sysData, void* userData);
int32 MotionCallback(void* sysData, void* userData);

int32 PointerCallback(void* sysData, void* userData);
int32 PointerMotionCallback(void* sysData, void* userData);

int32 SuspendCallback(void* sysData, void* userData);

bool CAnalogSticks::m_available = false;

bool CAnalogSticks::IsAvailable()
{
   m_available = S3E_TRUE == s3eTouchpadAvailable();
   return (s3eBool)m_available;
}

CAnalogSticks::CAnalogSticks()
{
    // initialize directional pad
    m_UseAbstractDirections = MAP_DIRECTIONS_NONE;

    // intialize on screen sticks
    for (int i=0; i<2; i++)
    {
        m_OnScreenStick[i].m_Radius = 0;
        m_OnScreenStick[i].m_TouchId = INVALID_TOUCH_ID;
    }

    m_OnScreenStick[0].m_MappedStick = &m_LeftStick;
    m_OnScreenStick[1].m_MappedStick = &m_RightStick;

    s3ePointerRegister(S3E_POINTER_TOUCH_EVENT, PointerCallback, this);
    s3ePointerRegister(S3E_POINTER_TOUCH_MOTION_EVENT, PointerMotionCallback, this);

    // initialize touchpad sticks
    if (!IsAvailable())
       return;

    m_LeftTouches = new std::vector<int32>();
    m_RightTouches = new std::vector<int32>();

    s3eTouchpadRegister(S3E_TOUCHPAD_EVENT, TouchCallback, this);
    s3eTouchpadRegister(S3E_TOUCHPAD_MOTION_EVENT, MotionCallback, this);

    s3eDeviceRegister(S3E_DEVICE_PAUSE, SuspendCallback, this);
}

CAnalogSticks::~CAnalogSticks()
{
    s3ePointerUnRegister(S3E_POINTER_TOUCH_EVENT, PointerCallback);
    s3ePointerUnRegister(S3E_POINTER_TOUCH_MOTION_EVENT, PointerMotionCallback);

    if (!m_available)
       return;

    s3eTouchpadUnRegister(S3E_TOUCHPAD_EVENT, TouchCallback);
    s3eTouchpadUnRegister(S3E_TOUCHPAD_MOTION_EVENT, MotionCallback);

    s3eDeviceUnRegister(S3E_DEVICE_PAUSE, SuspendCallback);

    delete m_LeftTouches;
    delete m_RightTouches;
}

CIwFVec2 CAnalogSticks::GetLeftStickPos()
{
   if (!m_available)
       return CIwFVec2();

   return m_LeftStick;
}

CIwFVec2 CAnalogSticks::GetRightStickPos()
{
    if (!m_available)
       return CIwFVec2();

    return m_RightStick;
}

void CAnalogSticks::GetLeftStickPolar(float *angle, float* mag)
{
     float x = m_LeftStick.x;
     float y = m_LeftStick.y;
     *mag   =  (float) sqrt((x*x)+(y*y));
     *angle = (float) atan2(y,x);
}

void CAnalogSticks::GetRightStickPolar(float *angle, float* mag)
{
     float x = m_RightStick.x;
     float y = m_RightStick.y;
     *mag   = (float) sqrt((x*x)+(y*y));
     *angle = (float) atan2(y,x);
}

void CAnalogSticks::SetLeftCenter(CIwFVec2 pos)
{
    assert(pos.x <= 1.0f && pos.x >= -1.0f && pos.y <= 1.0f && pos.y >= -1.0f);
    m_LeftCenter = pos;
}

void CAnalogSticks::SetRightCenter(CIwFVec2 pos)
{
    assert(pos.x <= 1.0f && pos.x >= -1.0f && pos.y <= 1.0f && pos.y >= -1.0f);
    m_RightCenter = pos;
}

int32 TouchCallback(void* sysData, void* userData)
{
    s3eTouchpadEvent *e = static_cast<s3eTouchpadEvent*>(sysData);
    CAnalogSticks* s = static_cast<CAnalogSticks*>(userData);

    const int width  = s3eTouchpadGetInt(S3E_TOUCHPAD_WIDTH);

    if (e->m_Pressed)
    {
        if (e->m_X > width / 2)
        {
            s->m_RightTouches->push_back(e->m_TouchID);
        }
        else
        {
            s->m_LeftTouches->push_back(e->m_TouchID);
        }
    }
    else
    {
        std::vector<int32>::iterator it = std::find(s->m_RightTouches->begin(), s->m_RightTouches->end(), e->m_TouchID);
        if (it != s->m_RightTouches->end())
        {
            s->m_RightTouches->erase(it);
            s->m_RightTouchpad.x = 0;
            s->m_RightTouchpad.y = 0;
        }
        else
        {
            it = std::find(s->m_LeftTouches->begin(), s->m_LeftTouches->end(), e->m_TouchID);
            if (it != s->m_LeftTouches->end())
            {
                s->m_LeftTouches->erase(it);
                s->m_LeftTouchpad.x = 0;
                s->m_LeftTouchpad.y = 0;
            }
        }
    }

    return 0;
}

int32 SuspendCallback(void* sysData, void* userData)
{
    // We are suspending, so release any touches we have and reset axes
    CAnalogSticks* s = static_cast<CAnalogSticks*>(userData);
    s->m_LeftTouches->clear();
    s->m_RightTouches->clear();

    for (int i=0; i<2; i++)
    {
        s->m_OnScreenStick[i].m_TouchId = INVALID_TOUCH_ID;
    }
    s->m_RightTouchpad.x = 0;
    s->m_RightTouchpad.y = 0;
    s->m_LeftTouchpad.x = 0;
    s->m_LeftTouchpad.y = 0;
    return 0;
}

int32 MotionCallback(void *sysData, void* userData)
{
    s3eTouchpadMotionEvent *e = static_cast<s3eTouchpadMotionEvent*>(sysData);
    CAnalogSticks* s = static_cast<CAnalogSticks*>(userData);

    const int width  = s3eTouchpadGetInt(S3E_TOUCHPAD_WIDTH);
    const int height = s3eTouchpadGetInt(S3E_TOUCHPAD_HEIGHT);

    // determine whether it is a right or left touch event (based on start of touch)
    if (std::find(s->m_RightTouches->begin(),s-> m_RightTouches->end(), e->m_TouchID) != s->m_RightTouches->end())
    {
         // normalize touchpad coordinates (-1 -> 1.0)
        float x = (width  - e->m_X) / ((1.0f - s->m_RightCenter.x) * width ) - 1.0f;
        float y = (height - e->m_Y) / ((1.0f - s->m_RightCenter.y) * height) - 1.0f;
        x *= -1;
        y *= -1;

        x = MIN(1.0f, x);
        x = MAX(-1.0f, x);
        y = MIN(1.0f, y);
        y = MAX(-1.0f, y);

        s->m_RightTouchpad.x = x;
        s->m_RightTouchpad.y = y;
    }
    else
    {
        // normalize touchpad coordinates (-1 -> 1.0)
        float x = (e->m_X / (s->m_LeftCenter.x * width)) - 1.0f;
        float y = (e->m_Y / (s->m_LeftCenter.y * height)) - 1.0f;

        x = MIN(1.0f, x);
        x = MAX(-1.0f,x);
        y = MIN(1.0f, y);
        y = MAX(-1.0f,y);

        s->m_LeftTouchpad.x = x;
        s->m_LeftTouchpad.y = y;
    }

    return 0;
}

int32 PointerCallback(void* sysData, void* userData)
{
    s3ePointerTouchEvent *e = static_cast<s3ePointerTouchEvent*>(sysData);
    CAnalogSticks* s = static_cast<CAnalogSticks*>(userData);

    //Disable on-screen dual stick if touchpad is available
    if (s->m_available && s3eTouchpadGetInt(S3E_TOUCHPAD_AVAILABLE))
        return 0;

    for (int i=0; i<2; i++)
    {
        if (e->m_Pressed != S3E_POINTER_STATE_DOWN)
        {
            // button
            if (s->m_OnScreenStick[i].m_TouchId == e->m_TouchID)
            {
                s->m_OnScreenStick[i].m_TouchId = INVALID_TOUCH_ID;
                continue;
            }
        }

        //normalize coordinates to surface
        const int width  = s3eSurfaceGetInt(S3E_SURFACE_WIDTH);
        const int height = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT);
        const int dir    = s3eSurfaceGetInt(S3E_SURFACE_DEVICE_BLIT_DIRECTION);
        int w,h;

        if (dir == S3E_SURFACE_BLIT_DIR_ROT90 || dir == S3E_SURFACE_BLIT_DIR_ROT270)
        {
            w = height;
            h = width;
        }
        else
        {
            w = width;
            h = height;
        }

        const float norm_x = e->m_x / (float)w;
        const float norm_y = e->m_y / (float)h;

        const float stickX = norm_x - s->m_OnScreenStick[i].m_Position.x;
        const float stickY = norm_y - s->m_OnScreenStick[i].m_Position.y;

        const float deltaR = (float) sqrt((float)(stickX*stickX + stickY*stickY));

        if (deltaR <= s->m_OnScreenStick[i].m_Radius)
        {
            // touch was inside on screen touchpad
            s->m_OnScreenStick[i].m_TouchPos = CIwFVec2(norm_x, norm_y);
            s->m_OnScreenStick[i].m_TouchId = e->m_TouchID;
        }
    }

    return 0;
}

int32 PointerMotionCallback(void* sysData, void* userData)
{
    s3ePointerTouchMotionEvent *e = static_cast<s3ePointerTouchMotionEvent*>(sysData);
    CAnalogSticks* s = static_cast<CAnalogSticks*>(userData);

    //Disable on-screen dual stick if touchpad is available
    if (s->m_available && s3eTouchpadGetInt(S3E_TOUCHPAD_AVAILABLE))
        return 0;

    for (int i=0; i<2; i++)
    {
        if (s->m_OnScreenStick[i].m_TouchId != e->m_TouchID)
            continue;

        const int width  = s3eSurfaceGetInt(S3E_SURFACE_WIDTH);
        const int height = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT);
        const int dir = s3eSurfaceGetInt(S3E_SURFACE_DEVICE_BLIT_DIRECTION);
        int w,h;

        if (dir == S3E_SURFACE_BLIT_DIR_ROT90 || dir == S3E_SURFACE_BLIT_DIR_ROT270)
        {
            w = height;
            h = width;
        }
        else
        {
            w = width;
            h = height;
        }

        const float norm_x = e->m_x / (float)w;
        const float norm_y = e->m_y / (float)h;

        // normalized stick positions from center
        const float stickX = (norm_x - s->m_OnScreenStick[i].m_TouchPos.x) / (float)s->m_OnScreenStick[i].m_Radius;
        const float stickY = (norm_y - s->m_OnScreenStick[i].m_TouchPos.y) / (float)s->m_OnScreenStick[i].m_Radius;

        // angle of stick position (remember y axis increases down the page!)
        float angle = (float)atan2(stickX,stickY);

        // map atan2 to 0 -> 2PI
        if (angle < 0)
            angle += 2 * PI;

        // maximize the touch to the radius of the circle
        const float maxX = (float) sin(angle);
        const float maxY = (float) cos(angle);

        float x = MIN(ABS(stickX), ABS(maxX) );
        float y = MIN(ABS(stickY), ABS(maxY) );

        if (stickX < 0)
            x *= -1.0f;

        if (stickY < 0)
            y *= -1.0f;

        IwTrace(TOUCHPAD_VERBOSE, ("Angle: %f, maxX %f maxY %f", (180.0 * angle / PI), maxX, maxY));
        IwTrace(TOUCHPAD_VERBOSE, ("Touchpad delta %f, %f (corrected) %f, %f", stickX, stickY, x, y));

        s->m_OnScreenStick[i].m_StickPos = CIwFVec2(x, y);

        // If the user dragged outside the touch area, drag the touch area a bit
        // This makes the interface little more flexible
        // Commenting out this if so the shift is always applied leads to an interesting effect
        // quite like using an actual analog stick
        if (ABS(x - stickX) > 0.15 || ABS(y - stickY) > 0.15)
        {
            const float dst = (float)sqrt(pow(ABS(x - stickX), 2) + pow(ABS(y - stickY), 2)) - 0.001f;
            const float hyp = (float)MIN(0.01f, dst);

            // calculate shift magnitudes for touch circle
            const float dx = (float)sin(angle) * hyp;
            const float dy = (float)cos(angle) * hyp;

            // add the shift to the current position and find difference from stick center
            const float newTouchCenterDiffX = s->m_OnScreenStick[i].m_TouchPos.x - s->m_OnScreenStick[i].m_Position.x + dx;
            const float newTouchCenterDiffY = s->m_OnScreenStick[i].m_TouchPos.y - s->m_OnScreenStick[i].m_Position.y + dy;

            // calculate furthest points allowed from stick centre at the angle of the destination point
            // (the angle of the new point to the touch center plus the angle of the touch center to the stick center)
            float combinedAngle = (float)atan2(newTouchCenterDiffX, newTouchCenterDiffY);
            if (combinedAngle < 0)
                combinedAngle += 2 * PI;
            const float maxTouchX = s->m_OnScreenStick[i].m_MaxShift * s->m_OnScreenStick[i].m_Radius * (float)sin(combinedAngle);
            const float maxTouchY = s->m_OnScreenStick[i].m_MaxShift * s->m_OnScreenStick[i].m_Radius * (float)cos(combinedAngle);

            const float magShift = (float)sqrt(pow(ABS(newTouchCenterDiffX), 2) + pow(ABS(newTouchCenterDiffY), 2));
            const float magClamp = (float)sqrt(pow(ABS(maxTouchX), 2) + pow(ABS(maxTouchY), 2));

            // take smallest value out of magnitudes
            if (magShift < magClamp)
            {
                s->m_OnScreenStick[i].m_TouchPos = CIwFVec2(newTouchCenterDiffX, newTouchCenterDiffY)  + s->m_OnScreenStick[i].m_Position;
            }
            else
            {
                s->m_OnScreenStick[i].m_TouchPos = CIwFVec2(maxTouchX, maxTouchY) + s->m_OnScreenStick[i].m_Position;
            }
        }
    }

    return 0;
}

CAnalogSticks::AbstractDirectionKeyUse CAnalogSticks::GetUseAbstractDirections()
{
    return m_UseAbstractDirections;
}

void CAnalogSticks::SetUseAbstractDirections(AbstractDirectionKeyUse b)
{
    m_UseAbstractDirections = b;
}

void CAnalogSticks::Update()
{
    // check abstract key mapping
    if (m_UseAbstractDirections == MAP_DIRECTIONS_NONE)
    {
        m_LeftStick = m_LeftTouchpad;
        m_RightStick = m_LeftTouchpad;
        return;
    }

    const float change = 0.6f;

    bool handledX = false;
    bool handledY = false;
    if (s3eKeyboardGetState(s3eKeyAbsUp) & S3E_KEY_STATE_DOWN)
    {
        handledY = true;
        m_DirectionalPad.y -= change;
        m_DirectionalPad.y = MAX(-1.0f, m_DirectionalPad.y);
    }
    if (s3eKeyboardGetState(s3eKeyAbsDown) & S3E_KEY_STATE_DOWN)
    {
        handledY = true;
        m_DirectionalPad.y += change;
        m_DirectionalPad.y = MIN(1.0f, m_DirectionalPad.y);
    }
    if (s3eKeyboardGetState(s3eKeyAbsRight) & S3E_KEY_STATE_DOWN)
    {
        handledX = true;
        m_DirectionalPad.x += change;
        m_DirectionalPad.x = MIN(1.0f, m_DirectionalPad.x);
    }
    if (s3eKeyboardGetState(s3eKeyAbsLeft) & S3E_KEY_STATE_DOWN)
    {
        handledX = true;
        m_DirectionalPad.x -= change;
        m_DirectionalPad.x = MAX(-1.0f, m_DirectionalPad.x);
    }

    // tend back toward 0
    if (!handledX)
        m_DirectionalPad.x = 0.0f;

    if (!handledY)
        m_DirectionalPad.y = 0.0f;

    // map directional pad to analog axis
    CIwFVec2 * mapped_analog_touchpad = & m_LeftTouchpad;
    CIwFVec2 * mapped_analog_stick = &m_LeftStick;

    if (m_UseAbstractDirections == MAP_DIRECTIONS_TO_RIGHT_ANALOG)
    {
        mapped_analog_touchpad = & m_RightTouchpad;
        mapped_analog_stick = &m_RightStick;
        m_LeftStick = m_LeftTouchpad;
    }
    else
    {
        m_RightStick = m_RightTouchpad;
    }

    if (ABS(mapped_analog_touchpad->x) > ABS(m_DirectionalPad.x))
    {
        mapped_analog_stick->x = mapped_analog_touchpad->x;
    }
    else
    {
        mapped_analog_stick->x = m_DirectionalPad.x;
    }

    if (ABS(mapped_analog_touchpad->y) > ABS(m_DirectionalPad.y))
    {
        mapped_analog_stick->y = mapped_analog_touchpad->y;
    }
    else
    {
        mapped_analog_stick->y = m_DirectionalPad.y;
    }

    // copy screen sticks to axes
    for (int i=0; i<2; i++)
    {
        if (m_OnScreenStick[i].m_TouchId != INVALID_TOUCH_ID)
            *(m_OnScreenStick[i].m_MappedStick) = m_OnScreenStick[i].m_StickPos;
    }
}

void CAnalogSticks::SetOnScreenStickLeft (float x, float y, float r, float maxShift)
{
    m_OnScreenStick[0].m_Position = CIwFVec2(x, y);
    m_OnScreenStick[0].m_Radius = r;
    m_OnScreenStick[0].m_MaxShift = maxShift;
}

void CAnalogSticks::SetOnScreenStickRight(float x, float y, float r, float maxShift)
{
    m_OnScreenStick[1].m_Position = CIwFVec2(x, y);
    m_OnScreenStick[1].m_Radius = r;
    m_OnScreenStick[1].m_MaxShift = maxShift;
}

#ifdef ANALOG_STICKS_DEBUG_AXES
void CAnalogSticks::DrawDebugScreenAxes()
{
    //Disable on-screen dual stick if touchpad is available
    if (m_available && s3eTouchpadGetInt(S3E_TOUCHPAD_AVAILABLE))
        return;

    const int width  = s3eSurfaceGetInt(S3E_SURFACE_WIDTH);
    const int height = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT);
    const int dir = s3eSurfaceGetInt(S3E_SURFACE_DEVICE_BLIT_DIRECTION);
    int w,h;

    if (dir == S3E_SURFACE_BLIT_DIR_ROT90 || dir == S3E_SURFACE_BLIT_DIR_ROT270)
    {
        w = height;
        h = width;
    }
    else
    {
        w = width;
        h = height;
    }

    for (int i=0; i<2; i++)
    {
        Iw2DSetColour(0xFF00FF00);

        CIwFVec2 radius     ((m_OnScreenStick[i].m_Radius * width), (m_OnScreenStick[i].m_Radius * width));
        CIwFVec2 pos        ((m_OnScreenStick[i].m_Position.x * w), (m_OnScreenStick[i].m_Position.y * h));
        CIwFVec2 touchPos   ((m_OnScreenStick[i].m_TouchPos.x * w), (m_OnScreenStick[i].m_TouchPos.y * h));

        Iw2DDrawArc(pos , radius,
            IW_ANGLE_FROM_DEGREES(0), IW_ANGLE_FROM_DEGREES(360), 30);

        // draw touch area
        if (m_OnScreenStick[i].m_TouchId != INVALID_TOUCH_ID)
        {
            Iw2DSetColour(0xFFFFFF00);
            Iw2DDrawArc(touchPos, radius, IW_ANGLE_FROM_DEGREES(0), IW_ANGLE_FROM_DEGREES(360), 30);
        }
    }
}
#endif
