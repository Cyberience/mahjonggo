/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3ETouchpadAdvanced S3E Touchpad Advanced Example
 *
 * The following example uses S3E's Touchpad functionality. It displays a set
 * of thumb-stick style controls on the screen and allows them to be
 * controlled by two touchpads.
 *
 * It includes a simple subproject, AnalogSticks, which implements the on-screen
 * control sticks and uses Iw2D to render them.
 *
 * @include s3eTouchpadAdvanced.cpp
 */

#include "ExamplesMain.h"
#include "algorithm"
#include "AnalogSticks.h"
#include "Iw2D.h"

static CAnalogSticks *g_AnalogSticks = NULL;

void ExampleInit()
{
    Iw2DInit();

    g_AnalogSticks = new CAnalogSticks();
    g_AnalogSticks->SetUseAbstractDirections(CAnalogSticks::MAP_DIRECTIONS_TO_LEFT_ANALOG);

    // This is the setup for the Xperia Play
    g_AnalogSticks->SetLeftCenter (CIwFVec2(0.2f, 0.5f));
    g_AnalogSticks->SetRightCenter(CIwFVec2(0.8f, 0.5f));

    g_AnalogSticks->SetOnScreenStickLeft (0.2f, 0.85f, 0.12f, 0.7f);
    g_AnalogSticks->SetOnScreenStickRight(0.8f, 0.85f, 0.12f, 0.7f);
}

void ExampleShutDown()
{
    delete g_AnalogSticks;
    g_AnalogSticks = NULL;

    Iw2DTerminate();
}

bool ExampleUpdate()
{
    g_AnalogSticks->Update();

    return true;
}

CIwColour g_Colours[] =
{
    { 0x00, 0x20, 0xff, 0xff },
    { 0x00, 0x40, 0xff, 0xff },
    { 0x00, 0x60, 0xff, 0xff },
    { 0x00, 0x80, 0xff, 0xff },
    { 0x00, 0xa0, 0xff, 0xff },
    { 0x00, 0xc0, 0xff, 0xff },
    { 0x00, 0xe0, 0xff, 0xff },
    { 0x00, 0xff, 0xff, 0xff },
};
/*
 * Render an analog stick at the specified polar coordinates
 */
void RenderAnalogStick(int32 x, int32 y, float angle, float mag, uint32 col)
{
    Iw2DSetColour(g_Colours[1]);
    Iw2DFillArc(CIwFVec2(x,y), CIwFVec2(50,50), IW_ANGLE_FROM_DEGREES(0), IW_ANGLE_FROM_DEGREES(360), 0);

    Iw2DSetColour(g_Colours[3]);
    float yPos = (float)( mag * sin(angle) * 50);
    float xPos = (float)( mag * cos(angle) * 50);
    Iw2DFillArc(CIwFVec2( (int16)xPos + x, (int16)yPos + y ), CIwFVec2(5,5), IW_ANGLE_FROM_DEGREES(0), IW_ANGLE_FROM_DEGREES(360), 0);
}

void ExampleRender()
{
    float angle, mag;
    g_AnalogSticks->GetLeftStickPolar(&angle, &mag);
    RenderAnalogStick(Iw2DGetSurfaceWidth()/4, Iw2DGetSurfaceHeight()/2, angle, mag, 0);

    g_AnalogSticks->GetRightStickPolar(&angle, &mag);
    RenderAnalogStick(3*Iw2DGetSurfaceWidth()/4, Iw2DGetSurfaceHeight()/2, angle, mag, 0);

    g_AnalogSticks->DrawDebugScreenAxes();

    Iw2DSurfaceShow();
}
