/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EEmail S3E Email Example
 * The following example demonstrates the Email extension.
 *
 * The main functions used to achieve this are:
 * - s3eEMailSendMail()
 *
 * Functions from S3E Keyboard and the OS Read String extension are used
 * to enter text. See relevant examples for more information.
 *
 *
 * @include s3eEMail.cpp
 */

#include "ExamplesMain.h"
#include "s3eEMail.h"
#include "s3eOSReadString.h" // For text entry through native input

const int EXAMPLE_EMAIL_STRING_MAX_LEN = 255;
const int NUM_MESSAGES = 5;
const int MESSAGE_LEN = 255;

static Button*  s_ButtonTo = NULL;
static Button*  s_ButtonSubject = NULL;
static Button*  s_ButtonBody = NULL;
static Button*  s_ButtonSend = NULL;

static char s_Subject[EXAMPLE_EMAIL_STRING_MAX_LEN] = "Default EMail Subject";
static char s_Message[EXAMPLE_EMAIL_STRING_MAX_LEN] = "Default EMail Message";
static char s_ToAddress[EXAMPLE_EMAIL_STRING_MAX_LEN] = "example@example.com";

void ExampleInit()
{
    s_ButtonTo = NewButton("Set Recipient");
    s_ButtonSubject = NewButton("Set Subject");
    s_ButtonBody = NewButton("Set Email Body");
    s_ButtonSend = NewButton("Send");

    InitMessages(NUM_MESSAGES, MESSAGE_LEN);

    if (!s3eEMailAvailable())
    {
        s_ButtonSend->m_Enabled = false;
        AppendMessage("`x666666EMail extension not available");
    }

    if (!s3eOSReadStringAvailable())
    {
        s_ButtonTo->m_Enabled = false;
        s_ButtonSubject->m_Enabled = false;
        s_ButtonBody->m_Enabled = false;
        AppendMessage("`x666666OSReadString extension is not available");
    }
}

void ExampleTerm()
{
    TerminateMessages();
}

bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    if (pressed == s_ButtonTo)
    {
        //Use OSReadString to set Recipient
        const char* pTo = s3eOSReadStringUTF8("Set Recipient:", S3E_OSREADSTRING_FLAG_EMAIL);
        if (pTo && strlen(pTo) > 0)
            strlcpy(s_ToAddress, pTo, sizeof s_ToAddress);
    }

    if (pressed == s_ButtonSubject)
    {
        //Use OSReadString to set Subject
        const char* pSubject = s3eOSReadStringUTF8("Set Subject:");
        if (pSubject && strlen(pSubject) > 0)
            strlcpy(s_Subject, pSubject, sizeof s_Subject);
    }

    if (pressed == s_ButtonBody)
    {
        //Use OSReadString to set Email Body
        const char* pMessage = s3eOSReadStringUTF8("Set Email Body:");
        if (pMessage && strlen(pMessage) > 0)
            strlcpy(s_Message, pMessage, sizeof s_Message);
    }

    if (pressed == s_ButtonSend)
    {
        s3eEMail *mail = new s3eEMail();
        memset(mail, 0, sizeof(s3eEMail));

        mail->m_subject = s_Subject;
        mail->m_numToRecipients = 1;
        const char* toAddressList[1] = {s_ToAddress};
        mail->m_toRecipients = toAddressList;
        mail->m_messageBody = s_Message;

        if (s3eEMailSendMail(mail) == S3E_RESULT_SUCCESS)
            AppendMessage("`x666666Email sent");
        else
            AppendMessage("`x666666Email sending failed");
    }

    return true;
}

void ExampleRender()
{
    int x = 20;
    int y = GetYBelowButtons();
    int lineGap = s3eDebugGetInt(S3E_DEBUG_FONT_HEIGHT) + 2;

    s3eDebugPrintf(x, y, 1, "`x666666To: %s", s_ToAddress);
    y += lineGap;
    s3eDebugPrintf(x, y, 1, "`x666666Subject: %s", s_Subject);
    y += lineGap;
    s3eDebugPrintf(x, y, 1, "`x666666Message body: %s", s_Message);
    y += lineGap;

    PrintMessages(x, y);
}
