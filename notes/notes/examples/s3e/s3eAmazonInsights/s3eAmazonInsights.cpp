/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EAmazonInsights S3E Amazon Insights
 *
 * The following example demonstrates S3E's Amazon Insights functionality.
 *
 * The main functions used to achieve this are:
 *
 * @include s3eAmazonInsights.cpp
 */
#include "ExamplesMain.h"
#include "s3eAmazonInsights.h"
#include "s3eDebug.h"


// Credentials for Amazon Insights
static const char* s_AppKey = "";
static const char* s_PrivateKey = "";


// Control buttons
static Button* g_CreateSession = 0;
static Button* g_RecordEventA = 0;
static Button* g_RecordEventB = 0;
static Button* g_SubmitEvents = 0;

static int32 OnInsightsSessionCB(void* systemData, void* userData)
{
    if (systemData != 0)
    {
        g_RecordEventA->m_Enabled = true;
        g_RecordEventB->m_Enabled = true;
        g_SubmitEvents->m_Enabled = true;
    }
    return 0;
}

void ExampleInit()
{
    InitMessages(10, 128);

    g_CreateSession = NewButton("Start Amazon Insights session");
    g_RecordEventA  = NewButton("Record simple event A");
    g_RecordEventB  = NewButton("Record simple event B");
    g_SubmitEvents  = NewButton("Submit recorded events to server");

    g_RecordEventA->m_Enabled = false;
    g_RecordEventB->m_Enabled = false;
    g_SubmitEvents->m_Enabled = false;

    if (s3eAmazonInsightsAvailable())
    {
        g_CreateSession->m_Enabled = true;
        s3eAmazonInsightsRegister(S3E_AMAZONINSIGHTS_CALLBACK_SESSION_CREATED, OnInsightsSessionCB, NULL);
    }
    else
    {
        g_CreateSession->m_Enabled = false;
        AppendMessageColour(RED, "s3eAmazonInsights extension unavailable");
    }
}

void ExampleTerm()
{
    s3eAmazonInsightsUnRegister(S3E_AMAZONINSIGHTS_CALLBACK_SESSION_CREATED, OnInsightsSessionCB);
}

bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();
    if (pressed == NULL)
    {
        return true;
    }

    if (pressed == g_CreateSession)
    {
        g_CreateSession->m_Enabled = false;

        if (s3eAmazonInsightsCreateSession(s_AppKey, s_PrivateKey) != S3E_RESULT_SUCCESS)
        {
            AppendMessageColour(RED, "s3eAmazonInsightsCreateSession failed");
        };
    }
    else if (pressed == g_RecordEventA)
    {
        s3eAmazonInsightsRecordSimpleEvent("eventA");
    }
    else if (pressed == g_RecordEventB)
    {
        s3eAmazonInsightsRecordSimpleEvent("eventB");
    }
    else if (pressed == g_SubmitEvents)
    {
        s3eAmazonInsightsSubmitEvents();
    }

    return true;
}

/*
 * The ExampleRender function outputs a set of strings indicating the result
 * image picker.
 */
void ExampleRender()
{
    int x = 20;
    int y = GetYBelowButtons();

    PrintMessages(x, y);
}
