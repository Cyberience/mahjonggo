/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

CustomReceiver = function ()
{
    // List of players
    this.players = [];
    
    // Callbacks used to notify UI
    this.onPlayerJoin = null;
    this.onPlayerLeft = null;
    this.onPlayerMessageRecv = null;
    this.onPlayerDisconnected = null;
};

CustomReceiver.Namespace = "urn:x-cast:com.marmalade.test";
CustomReceiver.castReceiverManager = null;
CustomReceiver.customMessageBus = null;

// Messages broadcast to senders from receiver
CustomReceiver.SENDER_JOINED        = "0";  // Player joined chat               - format is 0 + name
CustomReceiver.SENDER_LEFT          = "1";  // Player left chat                 - format is 1 + name
CustomReceiver.SENDER_NEW_MESSAGE   = "2";  // New message available            - format is 2 + name
CustomReceiver.SENDER_DISCONNECTED  = "3";  // Player disconnected              - format is 3 + name

// Messages sent to receiver to senders
CustomReceiver.PLAYER_JOINED        = "0";  // 0 - Player joined chat               - format is 0 + name
CustomReceiver.PLAYER_LEFT          = "1";  // 1 - Player left chat                 - format is 1
CustomReceiver.PLAYER_NEW_MESSAGE   = "2";  // 2 - Player sent message to receiver  - format is 2 + text

CustomReceiver.prototype = 
{
    initialise: function() {
        var _this = this;
        var appConfig = new cast.receiver.CastReceiverManager.Config();
        appConfig.statusText = 'Cast Chat';
        appConfig.maxInactivity = 10; // 10 seconds

        // Get instance of caster receiver manager
        CustomReceiver.castReceiverManager = cast.receiver.CastReceiverManager.getInstance();
        
        CustomReceiver.customMessageBus = CustomReceiver.castReceiverManager.getCastMessageBus(CustomReceiver.Namespace);

        CustomReceiver.castReceiverManager.onReady = function(event) {
            console.log("Cast Receiver Manager is READY: " + JSON.stringify(event));
        },

        CustomReceiver.castReceiverManager.onSenderConnected = function(event) {
            console.log('Received Sender Connected event: ' + event.data);
        },

        CustomReceiver.castReceiverManager.onSenderDisconnected = function(event) {
            console.log('Received Sender Disconnected event: ' + event.data);
            _this.playerDisconnected(event.senderId);

            // If last sender explicitly disconnects then exit app
            var senders = _this.getSenders();
            if (senders.length == 0 && event.reason == cast.receiver.system.DisconnectReason.REQUESTED_BY_SENDER)
                window.close();
        },
    
        CustomReceiver.castReceiverManager.onVisibilityChanged = function(event) {
            console.log("Visibility Changed: " + JSON.stringify(event));
        },

        CustomReceiver.customMessageBus.onMessage = function(event) {
            console.log('received message from ' + event.senderId);
            console.log(JSON.stringify(event.data));
            
            // New message received from a sender
            // First character of message is the message command and rest is command data
            var command = event.data.charAt(0);
            var data = event.data.slice(1);
            
            if (command === CustomReceiver.PLAYER_JOINED)
                _this.playerJoined(event.senderId, data);
            else
            if (command === CustomReceiver.PLAYER_LEFT)
                _this.playerLeft(event.senderId);
            else
            if (command === CustomReceiver.PLAYER_NEW_MESSAGE)
                _this.playerMessageRecv(event.senderId, data);
        };
        
        // Start cast receiver manager
        CustomReceiver.castReceiverManager.start(appConfig);
    },
    
    // Broadcast a message to all connected senders
    broadcastMessage: function(message) {
        CustomReceiver.customMessageBus.broadcast(message);
        console.log("Broadcasting message: '" + message + "'");
    },
    
    // Send a message to specific connected sender
    sendMessage: function(sender_id, message) {
        CustomReceiver.customMessageBus.send(sender_id, message);
        console.log("Sending message: '" + message + "' to " + sender_id);
    },
    
    // Return list of senders
    getSenders: function() {
        return CustomReceiver.castReceiverManager.getSenders();
    },
    
    findPlayer: function(sender_id) {
        for (var t = 0; t < this.players.length; t++)
        {
            if (this.players[t].senderId === sender_id)
                return this.players[t];
        }
        return null;
    },

    // playerJoined is called in response to a player joining the chat
    // Notifies all other users that a player joined
    playerJoined: function(sender_id, name) {
        // Ensure that player doesn't already exist
        var player = this.findPlayer(sender_id);
        if (player !== null)
            return;

        // Create and add a new player
        player = { senderId: sender_id, playerName: name }
        this.players.push(player);
        if (this.onPlayerJoin != null) this.onPlayerJoin(player);

        //  Notify all users that player joined
        this.broadcastMessage(CustomReceiver.SENDER_JOINED + name);
    },

    // playerLeft is called in response to a player manually leaving the chat
    // Notifies all other users that a player left
    playerLeft: function(sender_id) {
        for (var t = 0; t < this.players.length; t++)
        {
            if (this.players[t].senderId === sender_id)
            {
                // Notify all users that player left
                this.broadcastMessage(CustomReceiver.SENDER_LEFT + this.players[t].playerName);
        
                // Remove player
                if (this.onPlayerLeft !== null) this.onPlayerLeft(this.players[t]);
                this.players.splice(t, 1);
                return;
            }
        }
    },

    // playerDisconnected is called in response to a player being disconnected
    // Notifies all other users that a player was disconnected
    playerDisconnected: function(sender_id) {
        for (var t = 0; t < this.players.length; t++)
        {
            if (this.players[t].senderId === sender_id)
            {
                // Notify all users that player left
                this.broadcastMessage(CustomReceiver.SENDER_DISCONNECTED + this.players[t].playerName);

                // Remove player
                if (this.onPlayerDisconnected !== null) this.onPlayerDisconnected(this.players[t]);
                this.players.splice(t, 1);
                return;
            }
        }
    },

    // playerMessageRecv is called in response to a player sending a new message
    // Notifies all other users that a new message was sent
    playerMessageRecv: function(sender_id, text) {
        var player = this.findPlayer(sender_id);
        if (player === null)
            return;
        
        if (this.onPlayerMessageRecv !== null) this.onPlayerMessageRecv(player, text);
        
        //  Notify all users that new message available
        this.broadcastMessage(CustomReceiver.SENDER_NEW_MESSAGE + player.playerName);
    },
    
    
};
