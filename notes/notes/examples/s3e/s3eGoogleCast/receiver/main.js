/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

// Create instance of custom receiver
var customReciever = new CustomReceiver();

// Set up the custom receiver
function setupCastReceiver()
{
    // Initialise custom receiver
    customReciever.initialise();
    
    // Updates the chat log
    customReciever.updateChatLog = function(class_name, text)
    {
        var chatlog = document.getElementById("chatlog");
        var elem = document.createElement("div");
        elem.className = class_name;
        elem.innerHTML = text;
        chatlog.appendChild(elem);
        chatlog.scrollTop = chatlog.scrollHeight;
    };
    
    // Updates the chat log with a player joined message
    // Adds the new user to the list of available users
    customReciever.onPlayerJoin = function(player)
    {
        // Add player joined message to chat
        customReciever.updateChatLog("playerJoined", player.playerName + " has joined the chat");

        // Add player to list of users in the chat
        var users = document.getElementById("users");
        elem = document.createElement("div");
        elem.className = "userName";
        elem.id = player.senderId;
        elem.innerHTML = player.playerName;
        users.appendChild(elem);
    };

    // Updates the chat log with a player left message 
    // Removes the user from the list of available users
    customReciever.onPlayerLeft = function(player)
    {
        // Add player left  message to chat
        customReciever.updateChatLog("playerLeft", player.playerName + " has left the chat");
        
        // Remove player from list of users in the chat
        var users = document.getElementById("users");
        elem = document.getElementById(player.senderId);
        if (elem !== null)
            users.removeChild(elem);
    };

    // Updates the chat log with a player disconnected message 
    // Removes the user from the list of available users
    customReciever.onPlayerDisconnected = function(player)
    {
        // Add player disconnected message to chat
        customReciever.updateChatLog("playerLeft", player.playerName + " was disconnected from the chat");
        
        // Remove player from list of users in the chat
        var users = document.getElementById("users");
        elem = document.getElementById(player.senderId);
        if (elem !== null)
            users.removeChild(elem);
    };

    // Updates chat log with new message
    customReciever.onPlayerMessageRecv = function(player, text)
    {
        // Add received message to chat
        customReciever.updateChatLog("playerMessage", player.playerName + " says " + text);
    };

}

