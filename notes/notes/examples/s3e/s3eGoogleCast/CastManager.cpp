/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

#include "s3e.h"
#include "s3eGoogleCast.h"
#include <string.h>
#include <stdio.h>
#include "CastManager.h"

#define CAST_MANAGER_CALL_CALLBACK(manager, idx, system_data) if ((manager)->UserCallbacks[idx] != NULL) (manager)->UserCallbacks[idx](system_data, (manager)->UserCallbacksUserData[idx]);

CastManager::CastManager() : Available(false), DevicesChanged(false), Connected(false), AppLaunched(false), ConnectedDevice(NULL),
        Volume(-1.0f), UpdateCancelled(false)
{
    for (int t = 0; t < S3E_GOOGLECAST_CALLBACK_MAX; t++)
    {
        UserCallbacks[t] = NULL;
        UserCallbacksUserData[t] = NULL;
    }
}

//-------------------------------------------------------------------------------------------------
// >>>>> [ Callbacks ]
// These callbacks are called in response to a variety of changes that take place between the
// sender device and the Gooogle Cast device. See s3eGoogleCastCallback for a complete list of
// all possible callbacks.

// Device online callback from the extension
int32 CastManager::CastDeviceOnlineCallback(void* systemData, void* userData)
{
    s3eDebugTracePrintf("**** CastDeviceOnlineCallback received\n");

    s3eGoogleCastCallbackData* data = (s3eGoogleCastCallbackData*)systemData;
    CastManager* manager = static_cast<CastManager*>(userData);

    if (data != NULL)
    {
        s3eDebugTracePrintf("**** data->deviceId: %s\n", data->deviceId);

        // Add the new cast device with all its data
        CastDevice* device = new CastDevice();
        device->DeviceId = data->deviceId;
        device->DisplayName = data->displayName;
        device->DeviceName = data->deviceName;
        device->DeviceDescription = data->deviceDescription;
        manager->_AddDevice(device);
    }

    CAST_MANAGER_CALL_CALLBACK(manager, S3E_GOOGLECAST_CALLBACK_DEVICE_ONLINE, systemData);

    return 0;
}

//-------------------------------------------------------------------------------------------------

// Device changed callback from the extension
int32 CastManager::CastDevicePropertiesChangedCallback(void* systemData, void* userData)
{
    s3eDebugTracePrintf("**** CastDevicePropertiesChangedCallback received\n");

    s3eGoogleCastCallbackData* data = (s3eGoogleCastCallbackData*)systemData;
    CastManager* manager = static_cast<CastManager*>(userData);

    if (data != NULL)
    {
        s3eDebugTracePrintf("**** data->deviceId: %s\n", data->deviceId);

        CastDevice* device = manager->FindDevice(data->deviceId);
        if (device)
        {
            // Update device information
            device->DisplayName = data->displayName;
            device->DeviceName = data->deviceName;
            device->DeviceDescription = data->deviceDescription;
            manager->DevicesChanged = true;
        }
        else
        {
            // Add the new cast device
            CastDevice* device = new CastDevice();
            device->DeviceId = data->deviceId;
            device->DisplayName = data->displayName;
            device->DeviceName = data->deviceName;
            device->DeviceDescription = data->deviceDescription;
            manager->_AddDevice(device);
        }
    }

    CAST_MANAGER_CALL_CALLBACK(manager, S3E_GOOGLECAST_CALLBACK_DEVICE_PROPERTIES_CHANGED, systemData);

    return 0;
}

//-------------------------------------------------------------------------------------------------
// Device offline callback from the extension
int32 CastManager::CastDeviceOfflineCallback(void* systemData, void* userData)
{
    s3eDebugTracePrintf("**** CastDeviceOfflineCallback received\n");

    s3eGoogleCastCallbackData* data = (s3eGoogleCastCallbackData*)systemData;
    CastManager* manager = static_cast<CastManager*>(userData);

    if (data != NULL)
    {
        // Remove the cast device by deviceId
        (static_cast<CastManager*>(userData))->_RemoveDevice(data->deviceId);
    }

    CAST_MANAGER_CALL_CALLBACK(manager, S3E_GOOGLECAST_CALLBACK_DEVICE_OFFLINE, systemData);

    return 0;
}

//-------------------------------------------------------------------------------------------------

// Device connected callback from the extension
int32 CastManager::CastDeviceConnectedCallback(void* systemData, void* userData)
{
    s3eDebugTracePrintf("**** CastDeviceConnectedCallback received\n");

    s3eGoogleCastCallbackData* data = (s3eGoogleCastCallbackData*)systemData;
    CastManager* manager = static_cast<CastManager*>(userData);

    if (data != NULL)
    {
        s3eDebugTracePrintf("**** data->deviceId: %s\n", data->deviceId);

        manager->Connected = true;
    }

    CAST_MANAGER_CALL_CALLBACK(manager, S3E_GOOGLECAST_CALLBACK_DEVICE_CONNECTED, systemData);

    return 1;
}

//-------------------------------------------------------------------------------------------------

// Device disconnected callback from the extension
int32 CastManager::CastDeviceDisconnectedCallback(void* systemData, void* userData)
{
    s3eDebugTracePrintf("**** CastDeviceDisconnectedCallback received\n");

    s3eGoogleCastCallbackData* data = (s3eGoogleCastCallbackData*)systemData;
    CastManager* manager = static_cast<CastManager*>(userData);

    if (data != NULL)
    {
        s3eDebugTracePrintf("**** data->deviceId: %s\n", data->deviceId);

        manager->ConnectedDevice = NULL;
        manager->AppLaunched = false;
        manager->Connected = false;
    }

    CAST_MANAGER_CALL_CALLBACK(manager, S3E_GOOGLECAST_CALLBACK_DEVICE_DISCONNECTED, systemData);

    return 1;
}

//-------------------------------------------------------------------------------------------------

// Application launched callback from the extension
int32 CastManager::CastApplicationLaunchedCallback(void* systemData, void* userData)
{
    s3eDebugTracePrintf("**** CastReceiverLaunchedCallback received\n");

    s3eGoogleCastCallbackData* data = (s3eGoogleCastCallbackData*)systemData;
    CastManager* manager = static_cast<CastManager*>(userData);

    if (data != NULL)
    {
        s3eDebugTracePrintf("**** CastReceiverLaunchedCallback App Running!\n");

        // Adding communication channel
        s3eDebugTracePrintf("**** CastReceiverLaunchedCallback Adding channel...\n");

        if (s3eGoogleCastAddChannel(manager->Channel.c_str()) == S3E_RESULT_ERROR)
        {
            s3eDebugTracePrintf("**** CastReceiverLaunchedCallback ERROR Adding channel\n");
        }

        manager->AppLaunched = true;
    }

    CAST_MANAGER_CALL_CALLBACK(manager, S3E_GOOGLECAST_CALLBACK_APPLICATION_LAUNCHED, systemData);

    return 1;
}

//-------------------------------------------------------------------------------------------------

// Application left callback from the extension
int32 CastManager::CastApplicationLeftCallback(void* systemData, void* userData)
{
    s3eDebugTracePrintf("**** CastApplicationLeftCallback received");

    s3eGoogleCastCallbackData* data = (s3eGoogleCastCallbackData*)systemData;
    CastManager* manager = static_cast<CastManager*>(userData);

    if (data != NULL)
    {
        s3eDebugTracePrintf("**** CastApplicationLeftCallback App NOT running!");

        manager->AppLaunched = false;
    }

    CAST_MANAGER_CALL_CALLBACK(manager, S3E_GOOGLECAST_CALLBACK_APPLICATION_LEFT, systemData);

    return 1;
}

//-------------------------------------------------------------------------------------------------

// Application message received from the extension
int32 CastManager::CastChannelMessageReceived(void* systemData, void* userData)
{
    s3eDebugTracePrintf("**** CastChannelMessageReceived");

    s3eGoogleCastChannelCallbackData* data = static_cast< s3eGoogleCastChannelCallbackData* >(systemData);
    CastManager* manager = static_cast<CastManager*>(userData);

    if (data != NULL)
        s3eDebugTracePrintf("**** CastChannelMessageReceived Message: %s", data->textMessage);

    CAST_MANAGER_CALL_CALLBACK(manager, S3E_GOOGLECAST_CALLBACK_CHANNEL_MESSAGES, systemData);

    return 1;
}

//-------------------------------------------------------------------------------------------------

// Application message disconnected from the extension (the app may have stopped)
int32 CastManager::CastChannelDisconnected(void* systemData, void* userData)
{
    s3eDebugTracePrintf("**** CastChannelDisconnected");

    s3eGoogleCastChannelCallbackData* data = static_cast< s3eGoogleCastChannelCallbackData* >(systemData);
    CastManager* manager = static_cast<CastManager*>(userData);

    if (data != NULL)
        s3eDebugTracePrintf("**** CastChannelDisconnected, the receiver app may have stopped?");

    CAST_MANAGER_CALL_CALLBACK(manager, S3E_GOOGLECAST_CALLBACK_CHANNEL_DISCONNECTED, systemData);

    return 1;
}

//-------------------------------------------------------------------------------------------------

// Application message connected
int32 CastManager::CastChannelConnected(void* systemData, void* userData)
{
    s3eDebugTracePrintf("**** CastChannelConnected");

    CastManager* manager = static_cast<CastManager*>(userData);

    CAST_MANAGER_CALL_CALLBACK(manager, S3E_GOOGLECAST_CALLBACK_CHANNEL_CONNECTED, systemData);

    return 1;
}

//-------------------------------------------------------------------------------------------------

// Error callback from the extension
int32 CastManager::CastErrorCallback(void* systemData, void* userData)
{
    s3eDebugTracePrintf("**** CastErrorCallback received");

    s3eGoogleCastCallbackData* data = (s3eGoogleCastCallbackData*)systemData;
    CastManager* manager = static_cast<CastManager*>(userData);

    if (data != NULL)
    {
        s3eDebugTracePrintf("**** data->errorCode: %d", data->errorCode);

        switch ( data->errorCode )
        {
            // error connecting device application
            case S3E_GOOGLECAST_ERROR_CONNECT:
                s3eDebugTracePrintf("**** Error connecting with the device");
                manager->ConnectedDevice = NULL;
                break;

            // error launching application
            case S3E_GOOGLECAST_ERROR_LAUNCH_APPLICATION:
                s3eDebugTracePrintf("**** Error launching App");
                break;

            // error joining application
            case S3E_GOOGLECAST_ERROR_JOIN_APPLICATION:
                s3eDebugTracePrintf("**** Error joining App, trying to launch the app");
                break;

            // error leaving application
            case S3E_GOOGLECAST_ERROR_LEAVE_APPLICATION:
                s3eDebugTracePrintf("**** Error leaving App");
                break;

            // error leaving application
            case S3E_GOOGLECAST_ERROR_APPLICATION_NOT_RUNNING:
                s3eDebugTracePrintf("**** Error Application not running");
                break;

            // error leaving application
            case S3E_GOOGLECAST_ERROR_DEVICE_TIMEOUT:
                s3eDebugTracePrintf("**** Error device timeout");
                break;

            // undefined error
            default:
                s3eDebugTracePrintf("**** errorCode undefined");
        }
    }
    CAST_MANAGER_CALL_CALLBACK(manager, S3E_GOOGLECAST_CALLBACK_ERROR, systemData);

    return 1;
}

// Google play services update finished
int32 CastManager::GooglePlayServicesUpdateFinished(void* systemData, void* userData)
{
    CastManager* manager = static_cast<CastManager*>(userData);
    s3eDebugTracePrintf("**** GooglePlayServicesUpdateFinished");
    CAST_MANAGER_CALL_CALLBACK(manager, S3E_GOOGLECAST_CALLBACK_UPDATE_DIALOG_FINISHED, systemData);
    return 1;
}

// Google play services update was cancelled
int32 CastManager::GooglePlayServicesUpdateCancelled(void* systemData, void* userData)
{
    CastManager* manager = static_cast<CastManager*>(userData);
    manager->UpdateWasCancelled();
    s3eDebugTracePrintf("**** GooglePlayServicesUpdateCancelled");
    CAST_MANAGER_CALL_CALLBACK(manager, S3E_GOOGLECAST_CALLBACK_UPDATE_DIALOG_CANCELLED, systemData);
    return 1;
}

//-------------------------------------------------------------------------------------------------

void CastManager::_AddDevice(CastDevice* device)
{
    s3eDebugTracePrintf("**** _AddDevice, displayName: %s, id: %s", device->DisplayName.c_str(), device->DeviceId.c_str());

    CastDevice* dev = FindDevice(device->DeviceId.c_str());
    if (dev == NULL)
        CastDevices.push_back(device);
    else
    {
        s3eDebugTracePrintf("**** Updating device: %s, desc: %s", dev->DisplayName.c_str(), dev->DeviceDescription.c_str());

        dev->DeviceDescription = device->DeviceDescription;
        dev->DeviceId = device->DeviceId;
        dev->DeviceName = device->DeviceName;
        dev->DisplayName = device->DisplayName;
    }
}

//-------------------------------------------------------------------------------------------------

bool CastManager::_RemoveDevice(const char* device_id)
{
    for (_Iterator it = begin(); it != end(); ++it)
    {
        if ((*it)->DeviceId == device_id)
        {
            s3eDebugTracePrintf("**** _RemoveDevice, displayName: %s, id: %s", (*it)->DisplayName.c_str(), (*it)->DeviceId.c_str());

            delete *it;
            CastDevices.erase(it);
            return true;
        }
    }
    return false;
}

//-------------------------------------------------------------------------------------------------

CastDevice* CastManager::FindDevice(const char* device_id)
{
    for (_Iterator it = begin(); it != end(); ++it)
    {
        if ((*it)->DeviceId == device_id)
            return *it;
    }
    return NULL;
}

//-------------------------------------------------------------------------------------------------

void CastManager::setCallback(int index, s3eCallback callback, void* userData)
{
    UserCallbacks[index] = callback;
    UserCallbacksUserData[index] = userData;
}

void CastManager::UpdateWasCancelled()
{
    UpdateCancelled = true;
}


//-------------------------------------------------------------------------------------------------

int CastManager::Init()
{
    Available = s3eGoogleCastAvailable() == S3E_TRUE;
    if (!Available)
    {
        s3eDebugTracePrintf("**** Google Cast extension not available");
        return 0;
    }

    // Register callbacks
    s3eGoogleCastRegister( S3E_GOOGLECAST_CALLBACK_DEVICE_ONLINE, &CastDeviceOnlineCallback, this );
    s3eGoogleCastRegister( S3E_GOOGLECAST_CALLBACK_DEVICE_PROPERTIES_CHANGED, &CastDevicePropertiesChangedCallback, this );
    s3eGoogleCastRegister( S3E_GOOGLECAST_CALLBACK_DEVICE_OFFLINE, &CastDeviceOfflineCallback, this );
    s3eGoogleCastRegister( S3E_GOOGLECAST_CALLBACK_DEVICE_CONNECTED, &CastDeviceConnectedCallback, this );
    s3eGoogleCastRegister( S3E_GOOGLECAST_CALLBACK_DEVICE_DISCONNECTED, &CastDeviceDisconnectedCallback, this );
    s3eGoogleCastRegister( S3E_GOOGLECAST_CALLBACK_APPLICATION_LAUNCHED, &CastApplicationLaunchedCallback, this );
    s3eGoogleCastRegister( S3E_GOOGLECAST_CALLBACK_APPLICATION_LEFT, &CastApplicationLeftCallback, this );
    s3eGoogleCastRegister( S3E_GOOGLECAST_CALLBACK_CHANNEL_MESSAGES, &CastChannelMessageReceived, this );
    s3eGoogleCastRegister( S3E_GOOGLECAST_CALLBACK_CHANNEL_CONNECTED, &CastChannelConnected, this );
    s3eGoogleCastRegister( S3E_GOOGLECAST_CALLBACK_CHANNEL_DISCONNECTED, &CastChannelDisconnected, this );
    s3eGoogleCastRegister( S3E_GOOGLECAST_CALLBACK_ERROR, &CastErrorCallback, this );
    s3eGoogleCastRegister( S3E_GOOGLECAST_CALLBACK_UPDATE_DIALOG_FINISHED, &GooglePlayServicesUpdateFinished, this );
    s3eGoogleCastRegister( S3E_GOOGLECAST_CALLBACK_UPDATE_DIALOG_CANCELLED, &GooglePlayServicesUpdateCancelled, this );

    if (IsGooglePlayServicesAvailable())
    {
        if (s3eGoogleCastInitialise(AppId.c_str()) != S3E_RESULT_SUCCESS)
        {
            s3eDebugTracePrintf("**** Google Cast extension could not be initialised");
            return 0;
        }
    }
    else
        return -1;

    return 1;
}

//-------------------------------------------------------------------------------------------------

void CastManager::Release()
{
    if (Available)
    {
        LeaveApp();
        Disconnect();

        s3eGoogleCastUnRegister( S3E_GOOGLECAST_CALLBACK_DEVICE_ONLINE, &CastDeviceOnlineCallback );
        s3eGoogleCastUnRegister( S3E_GOOGLECAST_CALLBACK_DEVICE_PROPERTIES_CHANGED, &CastDevicePropertiesChangedCallback );
        s3eGoogleCastUnRegister( S3E_GOOGLECAST_CALLBACK_DEVICE_OFFLINE, &CastDeviceOfflineCallback );
        s3eGoogleCastUnRegister( S3E_GOOGLECAST_CALLBACK_DEVICE_CONNECTED, &CastDeviceConnectedCallback );
        s3eGoogleCastUnRegister( S3E_GOOGLECAST_CALLBACK_DEVICE_DISCONNECTED, &CastDeviceDisconnectedCallback );
        s3eGoogleCastUnRegister( S3E_GOOGLECAST_CALLBACK_APPLICATION_LAUNCHED, &CastApplicationLaunchedCallback );
        s3eGoogleCastUnRegister( S3E_GOOGLECAST_CALLBACK_APPLICATION_LEFT, &CastApplicationLeftCallback );
        s3eGoogleCastUnRegister( S3E_GOOGLECAST_CALLBACK_CHANNEL_MESSAGES, &CastChannelMessageReceived );
        s3eGoogleCastUnRegister( S3E_GOOGLECAST_CALLBACK_CHANNEL_DISCONNECTED, &CastChannelDisconnected );
        s3eGoogleCastUnRegister( S3E_GOOGLECAST_CALLBACK_ERROR, &CastErrorCallback );
        s3eGoogleCastUnRegister( S3E_GOOGLECAST_CALLBACK_UPDATE_DIALOG_FINISHED, &GooglePlayServicesUpdateFinished );
        s3eGoogleCastUnRegister( S3E_GOOGLECAST_CALLBACK_UPDATE_DIALOG_CANCELLED, &GooglePlayServicesUpdateCancelled );
    }
}

//-------------------------------------------------------------------------------------------------

bool CastManager::IsGooglePlayServicesAvailable() const
{
    // If Google Play Services not available or not correct version then install / update it
    if (s3eGoogleCastIsGooglePlayServicesAvailable() != S3E_TRUE)
    {
        s3eGoogleCastUpdateGooglePlayServices();
        return false;
    }
    return true;
}


//-------------------------------------------------------------------------------------------------

bool CastManager::Disconnect()
{
    if (!Available)
        return false;
    if (s3eGoogleCastDisconnectFromDevice() != S3E_RESULT_SUCCESS)
    {
        s3eDebugTracePrintf("**** Error disconnecting");
        return false;
    }

    return true;
}

//-------------------------------------------------------------------------------------------------

void CastManager::StartScan(bool clearList)
{
    if (Available)
    {
        s3eDebugTracePrintf("**** Starting scan");
        if (clearList)
        {
            _ClearDevices();
        }
        s3eGoogleCastStartScan();
    }
}

//-------------------------------------------------------------------------------------------------

void CastManager::StopScan()
{
    if (Available)
    {
        s3eDebugTracePrintf("**** Stopping scan");
        s3eGoogleCastStopScan();
    }
}

//-------------------------------------------------------------------------------------------------

void CastManager::_ClearDevices()
{
    s3eDebugTracePrintf("**** Clearing device list...");

    for (_Iterator it = begin(); it != end(); ++it)
    {
        s3eDebugTracePrintf("**** Clearing device: %s", (*it)->DeviceId.c_str());
        delete *it;
    }

    CastDevices.clear();
}

//-------------------------------------------------------------------------------------------------

bool CastManager::ConnectToDevice(const char* pDeviceID)
{
    s3eDebugTracePrintf("**** Connecting to device: %s", pDeviceID);

    if (Available)
    {
        // Saving device
        ConnectedDevice = FindDevice(pDeviceID);
        Connected = false;

        if (ConnectedDevice == NULL)
        {
            s3eDebugTracePrintf("**** WARNING - ConnectedDevice is NULL\n");
            return false;
        }
        else
        {
            return S3E_RESULT_SUCCESS == s3eGoogleCastConnectToDevice( pDeviceID ) ? true : false;
        }
    }

    return false;
}

//-------------------------------------------------------------------------------------------------

bool CastManager::LaunchApp(const char* pAppId, const char* pChannel)
{
    if (Available)
    {
        if (_CheckAppIdAndChannel(pAppId, pChannel))
        {
            s3eDebugTracePrintf("**** Launching APP: %s", AppId.c_str());

            return s3eGoogleCastLaunchApplication(AppId.c_str(), 0) == S3E_RESULT_SUCCESS ? true : false;
        }
    }

    return false;
}

//-------------------------------------------------------------------------------------------------

bool CastManager::JoinApp(const char* pAppId, const char* pChannel)
{
    if (Available)
    {
        if (_CheckAppIdAndChannel(pAppId, pChannel))
        {
            s3eDebugTracePrintf("**** Joining APP: %s", AppId.c_str());

            return s3eGoogleCastJoinApplication(AppId.c_str(), NULL) == S3E_RESULT_SUCCESS ? true : false;
        }
    }

    return false;
}

//-------------------------------------------------------------------------------------------------

bool CastManager::LeaveApp()
{
    if (Available)
    {
        return s3eGoogleCastLeaveApplication() == S3E_RESULT_SUCCESS ? true : false;
    }

    return false;
}

//-------------------------------------------------------------------------------------------------

bool CastManager::StopApp(const char* sessionId)
{
    if (Available)
    {
        return s3eGoogleCastStopApplication(sessionId) == S3E_RESULT_SUCCESS ? true : false;
    }

    return false;
}

//-------------------------------------------------------------------------------------------------

bool CastManager::SendMessage(const char* pMessage)
{
    s3eDebugTracePrintf("**** SEND TEXT MESSAGE\n");

    if (Available)
    {
        return s3eGoogleCastSendTextMessage(Channel.c_str(), pMessage) == S3E_RESULT_SUCCESS ? true : false;
    }

    return false;
}


//-------------------------------------------------------------------------------------------------

void CastManager::SetVolume(float volume)
{
    if (Available)
    {
        if (volume < 0.f)
            volume = 0.f;
        else if (volume > 1.f)
            volume = 1.f;

        if (volume != Volume)
        {
            s3eGoogleCastSetVolume(&volume);
            Volume = volume;
        }
    }
}

//-------------------------------------------------------------------------------------------------

float CastManager::GetVolume()
{
    float volume;

    if (!Available || s3eGoogleCastGetVolume(&volume) != S3E_RESULT_SUCCESS)
        volume = 0.f;
    else
        Volume = volume;

    return volume;
}

//-------------------------------------------------------------------------------------------------

bool CastManager::_CheckAppIdAndChannel(const char* pAppId, const char* pChannel)
{
    bool toReturn = true;

    // Check the AppId: If its different we will send an error
    if (pAppId != NULL)
    {
        if (AppId.size() == 0)
            AppId = pAppId;
        else if (stricmp(AppId.c_str(), pAppId ) != 0)
        {
            toReturn = false;
            s3eDebugTracePrintf("**** WARNING - CheckAppIdAndChannel trying to launch/join with a different appId\n");
        }
    }

    // Check the Channel: If its different we will print a warning
    if (pChannel != NULL)
    {
        if (Channel.size() == 0)
            Channel = pChannel;
        else if (stricmp(Channel.c_str(), pChannel) != 0)
        {
            Channel = pChannel;
            s3eDebugTracePrintf("**** WARNING - CheckAppIdAndChannel trying to launch/join with a different channel\n");
        }
    }

    return toReturn;
}
