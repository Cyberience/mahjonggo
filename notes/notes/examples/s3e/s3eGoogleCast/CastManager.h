/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
#if !defined(CAST_MANAGER_H)
#define CAST_MANAGER_H

#include <list>
#include <string>

// The CastDevice structure represents an available Google Cast device
struct CastDevice
{
    std::string DeviceId;
    std::string DeviceName;
    std::string DeviceDescription;
    std::string DisplayName;
};


// The CastManager class is a small helper class that is used to provide easier access to the s3eGoogleCast API
class CastManager
{
public:
    typedef std::list<CastDevice*>::iterator _Iterator;
    _Iterator       begin() { return CastDevices.begin(); }
    _Iterator       end() { return CastDevices.end(); }

    s3eCallback     UserCallbacks[S3E_GOOGLECAST_CALLBACK_MAX];         // User Callbacks
    void*           UserCallbacksUserData[S3E_GOOGLECAST_CALLBACK_MAX]; // User Callbacks user data

public:
    CastManager();

    bool HaveDevicesChanged() { if (DevicesChanged) { DevicesChanged = false; return true; } return false; }
    bool IsConnected() const { return Connected; }
    bool WasUpdateCancelled() const { return UpdateCancelled; }

    void            setAppId(const char* app_id) { AppId = app_id; }
    void            setCallback(int index, s3eCallback callback, void *userData);
    void            UpdateWasCancelled();
    int             Init();
    void            Release();
    bool            IsGooglePlayServicesAvailable() const;

    void            StartScan(bool clearList);
    void            StopScan();

    CastDevice*     FindDevice(const char* device_id);
    bool            HaveDeviceStatusesChanged() const { return DevicesChanged; }
    int             GetDeviceCount() const { return CastDevices.size(); }
    bool            HasAppLaunched() const { return AppLaunched; }

    bool            ConnectToDevice(const char* pDeviceID);
    bool            Disconnect();

    bool            LaunchApp(const char* pAppId, const char* pChannel);
    bool            JoinApp(const char* pAppId, const char* pChannel);
    bool            LeaveApp();
    bool            StopApp(const char* sessionId);

    bool            SendMessage(const char* pMessage);

    void            SetVolume(float volume);
    float           GetVolume();

private:
    std::list<CastDevice*> CastDevices;

    std::string     AppId;
    bool            Available;
    bool            UpdateCancelled;
    bool            DevicesChanged;
    bool            Connected;
    bool            AppLaunched;
    CastDevice*     ConnectedDevice;
    std::string     Channel;
    float           Volume;

    // Private methods
    void            _AddDevice(CastDevice* device);
    bool            _RemoveDevice(const char* device_id);
    bool            _ExistDevice(const char* deviceId);
    void            _ClearDevices();
    bool            _CheckAppIdAndChannel(const char* pAppId, const char* pChannel);

    // System Callbacks
    static int32    DeviceSuspend(void *systemData, void *userData);
    static int32    DeviceResume(void *systemData, void *userData);
    static int32    CastDeviceOnlineCallback(void* systemData, void* userData);
    static int32    CastDevicePropertiesChangedCallback(void* systemData, void* userData);
    static int32    CastDeviceOfflineCallback(void* systemData, void* userData);
    static int32    CastDeviceConnectedCallback(void* systemData, void* userData);
    static int32    CastDeviceDisconnectedCallback(void* systemData, void* userData);
    static int32    CastApplicationLaunchedCallback(void* systemData, void* userData);
    static int32    CastApplicationLeftCallback(void* systemData, void* userData);
    static int32    CastChannelMessageReceived(void* systemData, void* userData);
    static int32    CastChannelConnected(void* systemData, void* userData);
    static int32    CastChannelDisconnected(void* systemData, void* userData);
    static int32    CastErrorCallback(void* systemData, void* userData);
    static int32    GooglePlayServicesUpdateFinished(void* systemData, void* userData);
    static int32    GooglePlayServicesUpdateCancelled(void* systemData, void* userData);

};

#endif  // CAST_MANAGER_H
