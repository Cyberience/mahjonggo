/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EGoogleCast S3E Google Cast Example
 * The following example demonstrates how to use s3eGoogleCast to set up communication between a number of senders
 * and a custom Google cast receiver app written in JavaScript (see receiver for more details of custom receiver app).
 *
 * Features demonstrated in this example include:
 * - Connect to / disconnect from device
 * - Launch / join custom receiver app
 * - Send messages from connected senders to the receiver
 * - Send / broadcast messages from the receiver to connected senders
 *
 * For more information on the general topic of Google Cast see
 * https://developers.google.com/cast/docs/developers
 *
 * Also, see the Google Cast section of the Marmalade documentation for
 * more information http://docs.madewithmarmalade.com/display/MD/Google+Cast.
 *
 * The main functions demonstrated in this example include:
 * - s3eGoogleCastAvailable()
 * - s3eGoogleCastRegister()
 * - s3eGoogleCastUnRegister()
 * - s3eGoogleCastInitialise()
 * - s3eGoogleCastIsGooglePlayServicesAvailable()
 * - s3eGoogleCastUpdateGooglePlayServices()
 * - s3eGoogleCastDisconnectFromDevice()
 * - s3eGoogleCastStartScan()
 * - s3eGoogleCastStopScan()
 * - s3eGoogleCastConnectToDevice()
 * - s3eGoogleCastLaunchApplication()
 * - s3eGoogleCastJoinApplication()
 * - s3eGoogleCastLeaveApplication()
 * - s3eGoogleCastStopApplication()
 * - s3eGoogleCastSendTextMessage()
 * - s3eGoogleCastSetVolume()
 * - s3eGoogleCastGetVolume()
 * - s3eGoogleCastAddChannel()
 *
 * @include s3eGoogleCast.cpp
 */

#include "s3eGoogleCast.h"
#include "s3eOSReadString.h"
#include "IwDebug.h"

#include "ExamplesMain.h"

#include "CastManager.h"

// Message types that are sent to the receiver
enum eMessageSendType
{
    eMessageJoin = 0,
    eMessageLeave,
    eMessageSendText,
};

// Google Cast App ID
static const char* g_AppID = "your_cast_app_id";

// Channel namescape
static const char* g_Namescape = "urn:x-cast:com.marmalade.test";

// Google Cast manager
static CastManager* g_CastManager;

// Status string to print at bottom of screen
static char g_StatusStr[8192];
static char g_Name[256];
static int g_NumLines = 0;
static int g_MaxLines = 0;
static bool g_InChat = false;
static bool g_DisconnectRequested = false;

// Buttons
static Button* g_ConnectButton = NULL;
static Button* g_DisconnectButton = NULL;
static Button* g_JoinChatButton = NULL;
static Button* g_LeaveChatButton = NULL;
static Button* g_SendMessageButton = NULL;

// Sets a string to be displayed beneath the buttons
static void SetStatus(const char* statusStr, ...)
{
    va_list args;
    va_start(args, statusStr);
    strcpy(g_StatusStr, "`x000000");
    vsprintf(g_StatusStr+strlen(g_StatusStr), statusStr, args);
    va_end(args);
}

// Appends to the string to be displayed beneath the buttons
static void AddStatus(const char* statusStr, ...)
{
    va_list args;
    va_start(args, statusStr);
    int len = strlen(g_StatusStr);
    g_StatusStr[len] = '\n';
    vsprintf(g_StatusStr + len + 1, statusStr, args);
    va_end(args);
    g_NumLines++;
    if (g_NumLines > g_MaxLines)
    {
        // Remove first line
        len = strlen(g_StatusStr);
        for (int t = 0; t < len; t++)
        {
            if (g_StatusStr[t] == '\n')
            {
                std::string s = &g_StatusStr[t + 1];
                SetStatus(s.c_str());
                g_NumLines--;
                break;
            }
        }
    }
}

void SendMessage(eMessageSendType type, const char* data)
{
    std::string msg;
    switch (type)
    {
    case eMessageJoin:
        msg = "0";
        break;
    case eMessageLeave:
        msg = "1";
        break;
    case eMessageSendText:
        msg = "2";
        break;
    }
    if (data != NULL)
        msg += data;
    g_CastManager->SendMessage(msg.c_str());

}

// Callbacks
int32 DeviceOnlineCallback(void* systemData, void* userData)
{
    s3eGoogleCastCallbackData* data = (s3eGoogleCastCallbackData*)systemData;
    std::string msg = "Google Cast online device with ID ";
    msg += data->deviceId;
    s3eDebugTracePrintf(msg.c_str());

    g_ConnectButton->m_Enabled = true;

    return 0;
}

int32 DevicePropertiesChangedCallback(void* systemData, void* userData)
{
    return 0;
}

int32 DeviceOfflineCallback(void* systemData, void* userData)
{
    s3eGoogleCastCallbackData* data = (s3eGoogleCastCallbackData*)systemData;
    std::string msg = "Google Cast offline device with ID ";
    msg += data->deviceId;
    s3eDebugTracePrintf(msg.c_str());

    g_ConnectButton->m_Enabled = false;
    g_DisconnectButton->m_Enabled = false;
    g_JoinChatButton->m_Enabled = false;
    g_LeaveChatButton->m_Enabled = false;
    g_SendMessageButton->m_Enabled = false;
    g_InChat = false;

    AddStatus("Google Cast has gone offline");

    return 0;
}

int32 DeviceConnectedCallback(void* systemData, void* userData)
{
    s3eGoogleCastCallbackData* data = (s3eGoogleCastCallbackData*)systemData;
    std::string msg = "Connected to Google Cast device with ID ";
    msg += data->deviceId;
    s3eDebugTracePrintf(msg.c_str());

    // Launch chat app on receiver
    g_CastManager->LaunchApp(g_AppID, g_Namescape);
    g_DisconnectButton->m_Enabled = true;

    AddStatus("Connected to Google Cast");

    return 1;
}

int32 DeviceDisconnectedCallback(void* systemData, void* userData)
{
    s3eGoogleCastCallbackData* data = (s3eGoogleCastCallbackData*)systemData;
    std::string msg = "Disconnected from Google Cast device with ID ";
    msg += data->deviceId;
    s3eDebugTracePrintf(msg.c_str());

    g_ConnectButton->m_Enabled = true;
    g_DisconnectButton->m_Enabled = false;
    g_JoinChatButton->m_Enabled = false;
    g_LeaveChatButton->m_Enabled = false;
    g_SendMessageButton->m_Enabled = false;
    g_InChat = false;
    g_DisconnectRequested = false;

    AddStatus("You were dosconnected from the Google Cast");

    return 1;
}

int32 ApplicationLaunchedCallback(void* systemData, void* userData)
{
    std::string msg = "Application launched";
    s3eDebugTracePrintf(msg.c_str());

    g_JoinChatButton->m_Enabled = true;

    return 1;
}

int32 ApplicationLeftCallback(void* systemData, void* userData)
{
    std::string msg = "Left application";
    s3eDebugTracePrintf(msg.c_str());

    g_JoinChatButton->m_Enabled = true;
    g_LeaveChatButton->m_Enabled = false;
    g_SendMessageButton->m_Enabled = false;
    g_InChat = false;

    AddStatus("Chat was closed down");

    return 1;
}

int32 ChannelMessageReceived(void* systemData, void* userData)
{
    s3eGoogleCastChannelCallbackData* data = static_cast< s3eGoogleCastChannelCallbackData* >(systemData);
    std::string msg = "Message received '";

    if (strlen(data->textMessage) > 1)
    {
        // Process messages that are received from receiver
        char command = data->textMessage[0];
        switch (command)
        {
        case '0': // Player joined chat message from receiver
            {
                std::string s = &data->textMessage[1];
                if (s == g_Name)
                {
                    g_InChat = true;
                    g_JoinChatButton->m_Enabled = false;
                    g_LeaveChatButton->m_Enabled = true;
                    g_SendMessageButton->m_Enabled = true;
                    AddStatus("You joined the chat");
                }
                else
                {
                    s += " joined the chat";
                    AddStatus(s.c_str());
                }
            }
            break;
        case '1': // Player left chat from receiver
            {
                std::string s = &data->textMessage[1];
                if (s == g_Name)
                {
                    g_InChat = false;
                    g_JoinChatButton->m_Enabled = true;
                    g_LeaveChatButton->m_Enabled = false;
                    g_SendMessageButton->m_Enabled = false;
                    AddStatus("You left the chat");
                }
                else
                {
                    s += " left the chat";
                    AddStatus(s.c_str());
                }
            }
            break;
        case '2': // New message available from receiver
            {
                std::string s = &data->textMessage[1];
                if (s != g_Name)
                {
                    s += " sent new message";
                    AddStatus(s.c_str());
                }
            }
            break;
        case '3': // Player disconnected from receiver
            {
                std::string s = &data->textMessage[1];
                if (s != g_Name)
                {
                    s += " was disconnected from the chat";
                    AddStatus(s.c_str());
                }
            }
            break;
        }
    }

    msg += data->textMessage;
    msg += "'";
    s3eDebugTracePrintf(msg.c_str());

    return 1;
}

int32 ChannelDisconnected(void* systemData, void* userData)
{
    s3eGoogleCastChannelCallbackData* data = static_cast< s3eGoogleCastChannelCallbackData* >(systemData);
    std::string msg = "Channel connected with ID ";
    msg += data->channelId;
    s3eDebugTracePrintf(msg.c_str());

    return 1;
}

int32 ChannelConnected(void* systemData, void* userData)
{
    s3eGoogleCastChannelCallbackData* data = static_cast< s3eGoogleCastChannelCallbackData* >(systemData);
    std::string msg = "Channel disconnected with ID ";
    msg += data->channelId;
    s3eDebugTracePrintf(msg.c_str());

    if (g_InChat)
    {
        // Re-join chat
        SendMessage(eMessageJoin, g_Name);
    }

    return 1;
}

int32 ErrorCallback(void* systemData, void* userData)
{
    s3eGoogleCastCallbackData* data = (s3eGoogleCastCallbackData*)systemData;

    if (data != NULL)
    {
        switch (data->errorCode)
        {
            case S3E_GOOGLECAST_ERROR_CONNECT:
                AddStatus("Error: Could not connect to Google Cast");
                break;
            case S3E_GOOGLECAST_ERROR_DEVICE_OFFLINE:
                AddStatus("Error: Google Cast is offline");
                break;
            case S3E_GOOGLECAST_ERROR_DEVICE_TIMEOUT:
                AddStatus("Error: Google Cast timed out");
                break;
            case S3E_GOOGLECAST_ERROR_LAUNCH_APPLICATION:
                AddStatus("Error: Could not launch chat application");
                break;
            case S3E_GOOGLECAST_ERROR_JOIN_APPLICATION:
                AddStatus("Error: Could not join chat application");
                break;
            case S3E_GOOGLECAST_ERROR_LEAVE_APPLICATION:
                AddStatus("Error: Could not leave chat application");
                break;
            case S3E_GOOGLECAST_ERROR_APPLICATION_NOT_RUNNING:
                AddStatus("Error: Application not running");
                break;
            case S3E_GOOGLECAST_ERROR_ADD_CHANNEL:
                AddStatus("Error: Could not add channel");
                break;
            case S3E_GOOGLECAST_ERROR_REMOVE_CHANNEL:
                AddStatus("Error: Could not remove channel");
                break;
            case S3E_GOOGLECAST_ERROR_SEND_TEXT_MESSAGE:
                AddStatus("Error: Could not send text");
                break;
            default:
                break;
        }
    }

    return 1;
}

int32 GooglePlayServicesUpdateFinished(void* systemData, void* userData)
{
    if (!g_CastManager->WasUpdateCancelled())
    {
        AddStatus("Google Play Services updated");
        if (g_CastManager->Init() <= 0)
        {
            AddStatus("Error: Could not initialise Google Cast extension");
        }
        else
            g_CastManager->StartScan(true);
    }

    return 1;
}

int32 GooglePlayServicesUpdateCancelled(void* systemData, void* userData)
{
    AddStatus("Google Play Services update was cancelled");
    return 1;
}

void ExampleInit()
{
    SetStatus("s3eGoogleCast Test");
    g_NumLines = 1;
    g_Name[0] = 0;

    // Create buttons
    g_ConnectButton = NewButton("Connect to Cast");
    g_ConnectButton->m_Enabled = false;
    g_DisconnectButton = NewButton("Disconnect from Cast");
    g_DisconnectButton->m_Enabled = false;
    g_JoinChatButton = NewButton("Join Chat");
    g_JoinChatButton->m_Enabled = false;
    g_LeaveChatButton = NewButton("Leave Chat");
    g_LeaveChatButton->m_Enabled = false;
    g_SendMessageButton = NewButton("Send Message");
    g_SendMessageButton->m_Enabled = false;

    // Calculate max display lines
    g_MaxLines = (s3eSurfaceGetInt(S3E_SURFACE_HEIGHT) - GetYBelowButtons()) / s3eDebugGetInt(S3E_DEBUG_FONT_HEIGHT);

    // Initialise Google Cast extension
    g_CastManager = new CastManager();
    g_CastManager->setAppId(g_AppID);
    g_CastManager->setCallback(S3E_GOOGLECAST_CALLBACK_DEVICE_ONLINE, DeviceOnlineCallback, NULL);
    g_CastManager->setCallback(S3E_GOOGLECAST_CALLBACK_DEVICE_OFFLINE, DeviceOfflineCallback, NULL);
    g_CastManager->setCallback(S3E_GOOGLECAST_CALLBACK_DEVICE_CONNECTED, DeviceConnectedCallback, NULL);
    g_CastManager->setCallback(S3E_GOOGLECAST_CALLBACK_DEVICE_DISCONNECTED, DeviceDisconnectedCallback, NULL);
    g_CastManager->setCallback(S3E_GOOGLECAST_CALLBACK_APPLICATION_LAUNCHED, ApplicationLaunchedCallback, NULL);
    g_CastManager->setCallback(S3E_GOOGLECAST_CALLBACK_APPLICATION_LEFT, ApplicationLeftCallback, NULL);
    g_CastManager->setCallback(S3E_GOOGLECAST_CALLBACK_CHANNEL_CONNECTED, ChannelConnected, NULL);
    g_CastManager->setCallback(S3E_GOOGLECAST_CALLBACK_CHANNEL_DISCONNECTED, ChannelDisconnected, NULL);
    g_CastManager->setCallback(S3E_GOOGLECAST_CALLBACK_CHANNEL_MESSAGES, ChannelMessageReceived, NULL);
    g_CastManager->setCallback(S3E_GOOGLECAST_CALLBACK_ERROR, ErrorCallback, NULL);
    g_CastManager->setCallback(S3E_GOOGLECAST_CALLBACK_DEVICE_PROPERTIES_CHANGED, DevicePropertiesChangedCallback, NULL);
    g_CastManager->setCallback(S3E_GOOGLECAST_CALLBACK_UPDATE_DIALOG_FINISHED, GooglePlayServicesUpdateFinished, NULL);
    g_CastManager->setCallback(S3E_GOOGLECAST_CALLBACK_UPDATE_DIALOG_CANCELLED, GooglePlayServicesUpdateCancelled, NULL);

    // initialise then scan for devices
    int ret = g_CastManager->Init();
    if (ret <= 0)
    {
        if (ret == 0)
            AddStatus("Error: Could not initialise Google Cast extension");
    }
    else
        g_CastManager->StartScan(true);
}

void ExampleTerm()
{
    g_CastManager->Release();
}

bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    if (pressed)
    {
        if (g_ConnectButton == pressed)
        {
            // We make a big assumption here that only one Google Cast device was found
            // in release code you should allow the user to select a Google Cast device
            // from a list of found devices
            CastDevice* device = *g_CastManager->begin();
            g_CastManager->ConnectToDevice(device->DeviceId.c_str());
            g_ConnectButton->m_Enabled = false;
        }
        else if (g_DisconnectButton == pressed)
        {
            // Disconnect from the Google cast device
            g_DisconnectRequested = true;
            g_CastManager->Disconnect();
        }
        else if (g_JoinChatButton == pressed)
        {
            if (g_Name[0] == 0)
            {
                // Ask user for user name
                const char* name = s3eOSReadStringUTF8("Enter Name");
                if (name != NULL)
                    strcpy(g_Name, name);
            }
            if (strlen(g_Name) > 0)
            {
                // Send join chat message
                SendMessage(eMessageJoin, g_Name);
            }
        }
        else if (g_LeaveChatButton == pressed)
        {
            // Send leave chat message to receiver
            SendMessage(eMessageLeave, NULL);
        }
        else if (g_SendMessageButton == pressed)
        {
            // Ask user for message to send
            const char* text = s3eOSReadStringUTF8("Enter Message");
            if (text != NULL && strlen(text) > 0)
            {
                // Send entered message to receiver
                SendMessage(eMessageSendText, text);
                std::string s = "You said: ";
                s += text;
                AddStatus(s.c_str());
            }
        }
    }
    return true;
}
void ExampleRender()
{
    // Print status string just below the buttons
    s3eDebugPrint(0, GetYBelowButtons() + 20, g_StatusStr, S3E_TRUE);
}

