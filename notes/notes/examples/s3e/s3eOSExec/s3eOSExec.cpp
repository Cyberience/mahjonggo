/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EOSExec S3E OS Exec Example
 *
 * The following example calls the s3eOSExec API to allow the user
 * to launch a URL using the native OS handler on the device.
 *
 * The functions required to achieve this are:
 *  - s3eOSExecAvailable()
 *  - s3eOSExecExecute()
 *
 * @include s3eOSExec.cpp
 */

#include "ExamplesMain.h"
#include "s3eOSExec.h"

static const char* g_URL = "http://www.madewithmarmalade.com";
static const char* g_AppURL = "fb://app";

static Button* g_ButtonRun = 0;
static Button* g_ButtonRunWithFallback = 0;
static s3eResult g_Result = S3E_RESULT_SUCCESS;

void ExampleInit()
{
    // Create the UI.
    g_ButtonRun = NewButton("Run command to open an URL");
    if (s3eDeviceGetInt(S3E_DEVICE_OS) != S3E_OS_ID_WP8)
        g_ButtonRunWithFallback = NewButton("Run command to open an App or URL.");
}

void ExampleTerm()
{
}

bool ExampleUpdate()
{
    // Check for the availabilty of the OS Execute functionality.
    // If it is available then enable the button.
    if (s3eOSExecAvailable())
    {
        g_ButtonRun->m_Enabled = true;
    }
    else
        g_ButtonRun->m_Enabled = false;

    // Check for the interface button event
    Button* pressed = GetSelectedButton();

    // If the button has been selected, try to execute the OS command
    // and store the result.
    if (pressed == g_ButtonRun)
        g_Result = s3eOSExecExecute(g_URL, S3E_FALSE);
    else if (g_ButtonRunWithFallback && pressed == g_ButtonRunWithFallback)
        g_Result = s3eOSExecExecuteWithFallbackURL(g_AppURL, g_URL, S3E_FALSE);

    return true;
}

/*
 * The following function display the user's prompt and any error messages.
 * The s3eDebugPrint() function to print the strings.
 */
void ExampleRender()
{
    // Print results
    if (!s3eOSExecAvailable())
    {
        s3eDebugPrint(50, 200, "`xff0000Executing host OS commands API isn't available on this device.", 1);
    }

    if (g_Result == S3E_RESULT_ERROR)
    {
        s3eDebugPrint(50, 200, "`xff0000Failed to open the URL.", 1);
    }
}
