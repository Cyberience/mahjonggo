/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3ESamsungAdHub S3E Samsung AdHub Example
 *
 * The following example call the s3eSamsungAdHub API to allow the user
 * to use Samsung Advertisement SDK
 *
 * The functions required to achieve this are:
 * - s3eSamsungAdHubAvailable()
 * - s3eSamsungAdHubControlCreate()
 * - s3eDebugPrintf()
 *
 * All examples will follow this basic pattern; a brief description of what
 * the example does will be given followed by a list of all the important
 * functions and, perhaps, classes.
 *
 * Should the example be more complex, a more detailed explanation of what the
 * example does and how it does it will be added.
 *
 *
 * @include s3eSamsungAdHub.cpp
 */

#include "ExamplesMain.h"
#include "s3eSamsungAdHub.h"

s3eSamsungAdHubControl* adBlock = NULL;

static Button* g_ButtonCreate = 0;
static Button* g_ButtonShow = 0;
static Button* g_ButtonHide = 0;
static Button* g_ButtonDestroy = 0;
static int updatesCount = 0;

// Put Your inventory ID here.
const char* inventoryID = "xv0d00000001fq";

//This callback will be called for every new banner content update.
static int32 UpdatedCallback(void*, void*)
{
    updatesCount++;
    return 0;
}

void ExampleInit()
{
    g_ButtonCreate = NewButton("Crate banner");
    g_ButtonShow = NewButton("Show");
    g_ButtonHide = NewButton("Hide");
    g_ButtonDestroy = NewButton("Destroy");

    g_ButtonShow->m_Enabled = false;
    g_ButtonHide->m_Enabled = false;
    g_ButtonDestroy->m_Enabled = false;
}

void ExampleTerm()
{
    if (adBlock)
        s3eSamsungAdHubControlDestroy(adBlock);
}

bool ExampleUpdate()
{
    // Check for the availability of functionality.
    if (!s3eSamsungAdHubAvailable())
        return true;

    Button* pressed = GetSelectedButton();
    if (pressed == g_ButtonShow)
    {
        s3eSamsungAdHubControlSetShowState(adBlock, 1);
        g_ButtonShow->m_Enabled = false;
        g_ButtonHide->m_Enabled = true;
    }
    else if (pressed == g_ButtonHide)
    {
        s3eSamsungAdHubControlSetShowState(adBlock, 0);
        g_ButtonShow->m_Enabled = true;
        g_ButtonHide->m_Enabled = false;
    }
    else if (pressed == g_ButtonDestroy)
    {
        s3eSamsungAdHubControlDestroy(adBlock);
        g_ButtonShow->m_Enabled = false;
        g_ButtonHide->m_Enabled = false;
        g_ButtonDestroy->m_Enabled = false;
        g_ButtonCreate->m_Enabled = true;
        updatesCount = 0;
        s3eSamsungAdHubUnRegister(S3E_SAMSUNGADHUB_CALLBACK_UPDATED, UpdatedCallback);
        adBlock = NULL;
    }
    else if (pressed == g_ButtonCreate && !adBlock)
    {
        adBlock = s3eSamsungAdHubControlCreate(inventoryID, 0, 500);
        if (adBlock)
        {
            s3eSamsungAdHubControlSetStringProperty(adBlock, S3E_SAMSUNGADHUB_CONTROL_INTERESTS, "cars");
            g_ButtonCreate->m_Enabled = false;
            g_ButtonDestroy->m_Enabled = true;
            g_ButtonHide->m_Enabled = true;

            if (s3eSamsungAdHubRegister(S3E_SAMSUNGADHUB_CALLBACK_UPDATED, UpdatedCallback, 0) != S3E_RESULT_SUCCESS)
            {
                s3eDebugPrint(50, 250, "`x666666Error registering callback.", 1);
            }
        }
    }

    return true;
}

/*
 * The following function outputs the strings entered by the user.
 * The s3eDebugPrint() function to print the strings.
 */
void ExampleRender()
{
    // Print results
    if (!s3eSamsungAdHubAvailable())
    {
        s3eDebugPrint(50, 50, "`xff0000Samsung AdHub API isn't available on this device.", 1);
        g_ButtonShow->m_Display = false;
        g_ButtonHide->m_Display = false;
        g_ButtonDestroy->m_Display = false;
        g_ButtonCreate->m_Display = false;
    }

    if (updatesCount > 0)
    {
        s3eDebugPrintf(50, 350, 1, "`x0000ffReceived callbacks count is %d", updatesCount);
    }
}
