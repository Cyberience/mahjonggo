/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EIOSWatch S3E IOS Watch example
 *
 * The following example, demonstrates how to use iOS Watch extension in
 * a Marmalade application.
 *
 * It is designed to work together with iOS Watch Kit 1 App and iOS Watch Kit 1
 * iOS extension that are included in the same folder. These applications need to
 * be build separately in native Xcode toochain.
 *
 * There is a simple UI interface implemented in iOS Watch Kit 1 App with a UISwitch
 * control. Interacting with he control makes iOS Watch Kit 1 Extension to send a mesagge
 * to this app which listens on S3E_IOSWATCH_HANDLEREQUEST callbacks and changes the colour
 * and text on the screen.
 *
 * @include s3eIOSWatch.cpp
 */
#include "s3e.h"
#include "s3eIOSWatch.h"

bool cool;
const char* coolMarmaladeJson = "{ \"cool\" : \"yes\"}";
const char* nonCoolMarmaladeJson = "{ \"cool\" : \"no\"}";

// Callback function(s)
int32 watchRequest(void* systemData, void* userData)
{
    cool = !cool;
    (cool) ? s3eIOSWatchSendReply(coolMarmaladeJson) : s3eIOSWatchSendReply(nonCoolMarmaladeJson);
    return 1;
}

// Main entry point for the application
int main()
{
    s3eIOSWatchRegister(S3E_IOSWATCH_HANDLEREQUEST, watchRequest, NULL);

    // Wait for a quit request from the host OS
    while (!s3eDeviceCheckQuitRequest())
    {
        // Fill background blue
        s3eSurfaceClear(0, 0, 255);

        // Print a line of debug text to the screen at top left (0,0)
        // Starting the text with the ` (backtick) char followed by 'x' and a hex value
        // determines the colour of the text.
        if (cool) {
            s3eSurfaceClear(0, 0, 255);
            s3eDebugPrint(120, 150, "`xffffffMarmalade is cool", 0);
        } else {
            s3eSurfaceClear(255, 0, 0);
            s3eDebugPrint(120, 150, "`xffffffMarmalade is not cool", 0);
        }

        // Flip the surface buffer to screen
        s3eSurfaceShow();

        // Sleep for 0ms to allow the OS to process events etc.
        s3eDeviceYield(0);
    }
    return 0;
}
