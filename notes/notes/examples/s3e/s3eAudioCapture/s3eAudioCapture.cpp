/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EAudioCapture S3E Audio Capture Example
 *
 * The following example demonstrates S3E's Audio Capture functionality.
 *
 * The main functions used to achieve this are:
 * - s3eAudioCaptureToFile()
 *
 * The example allows the user to launch the native audio capture
 * application which returns the file path for the captured audio.
 *
 *
 * File paths are returned with the raw:// prefix,
 * followed by their full system path, rather than the normal local
 * path relative to the app's data folder. These raw paths can be passed to
 * S3E file APIs and will be resolved transparently.
 *
 * The example uses S3E Audio to play/stop recordings.
 *
 * @include s3eAudioCapture.cpp
 */

#include "ExamplesMain.h"
#include "s3eAudioCapture.h"
#include "s3eAudio.h"

static Button* g_ButtonCaptureAudioToFile = NULL;
static Button* g_ButtonPlayAudioFile = NULL;
static Button* g_ButtonStopAudioFile = NULL;

static bool g_PlayingFile = false;

static char g_Filepath[256] = {0};

int32 audioplayCB(void *systemData, void *userData)
{
    if (g_PlayingFile)
    {
        g_PlayingFile = false;
        g_ButtonPlayAudioFile->m_Enabled = true;
        g_ButtonStopAudioFile->m_Enabled = false;
    }

    return 0;
}

void ExampleInit()
{
    s3eAudioRegister(S3E_AUDIO_STOP, (s3eCallback) audioplayCB, NULL);

    g_ButtonCaptureAudioToFile = NewButton("Capture audio to file");
    g_ButtonPlayAudioFile = NewButton("Play last recorded file");
    g_ButtonStopAudioFile = NewButton("Stop playback");

    g_ButtonCaptureAudioToFile->m_Enabled = false;
    g_ButtonPlayAudioFile->m_Enabled = false;
    g_ButtonStopAudioFile->m_Enabled = false;

    InitMessages(10, 128);

    //Check whether the AudioCapture extension is available
    if (s3eAudioCaptureAvailable())
    {
        g_ButtonCaptureAudioToFile->m_Enabled = true;
    }
    else
    {
        AppendMessageColour(RED, "AudioCapture extension not available");
    }

}

void ExampleTerm()
{
}

int32 TouchToStop(void* sysData, void* userData)
{
     g_PlayingFile = false;

    return 0;
}


bool ExampleUpdate()
{

    Button* pressed = GetSelectedButton();
    if (!pressed)
        return true;

    if (pressed == g_ButtonCaptureAudioToFile)
    {
        memset(g_Filepath, 0, sizeof(g_Filepath));

        if (s3eAudioCaptureToFile(g_Filepath, 256) == S3E_RESULT_ERROR)
        {
            g_ButtonPlayAudioFile->m_Enabled = false;
            g_ButtonStopAudioFile->m_Enabled = false;

            g_Filepath[0] = 0;

            s3eAudioCaptureError err = s3eAudioCaptureGetError();
            if (err == S3E_AUDIOCAPTURE_ERR_CANCELLED)
                AppendMessageColour(RED, "Audio Capture was cancelled (%d)", err);
            else
                AppendMessageColour(RED, "Failed to capture audio to file with error code: %d", err);
        }
        else
        {
            AppendMessageColour(GREEN, "Captured audio to file: %s", g_Filepath);
            g_ButtonPlayAudioFile->m_Enabled = true;
            g_ButtonStopAudioFile->m_Enabled = false;
        }
    }
    else if (pressed == g_ButtonPlayAudioFile)
    {
        if (g_Filepath[0] != 0)
        {
            s3eAudioPlay(g_Filepath);
            g_PlayingFile = true;
            g_ButtonPlayAudioFile->m_Enabled = false;
            g_ButtonStopAudioFile->m_Enabled = true;
        }
        else
        {
            g_PlayingFile = false;
            g_ButtonPlayAudioFile->m_Enabled = false;
            g_ButtonStopAudioFile->m_Enabled = false;
        }
    }
    else if (pressed == g_ButtonStopAudioFile)
    {
        if (g_PlayingFile)
        {
            s3eAudioStop();
        }
    }

    return true;
}

/*
 * The ExampleRender function outputs a set of strings indicating the result of
 * an audio capture.
 */
void ExampleRender()
{
    int x = 20;
    int y = GetYBelowButtons();

    PrintMessages(x, y);
}
