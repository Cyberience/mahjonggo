/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EWindowsStoreBilling S3E Windows Store Billing Example
 *
 * This example demonstrates the use of the s3eWindowsStoreStoreBilling API to interface with
 * the Windows Store Store to offer in-app purchasing of products and services.
 *
 * The example can be submitted as a beta application for testing within a sandboxed environment
 * of the Windows Store Store.  To do so, the application needs to be submitted, alongside
 * a catalog of products, to the Dev Center.
 *
 * The example demonstrates the enumeration of licenses, retrieval of particular product license,
 * retrieval of app data, retrieval of product listing and associated data, reporting of fulfillment
 * and repurchase of consumables, as well as error trapping.
 *
 * @include s3eWindowsStoreBilling.cpp
 */
#include "ExamplesMain.h"
#include "s3eWindowsStoreBilling.h"
#include "s3eWindowsStoreBillingExample.h"

#include "IwDebug.h"


MyBillingExample g_Billing;

int32 onAppData(void* systemData, void* userData)
{
    s3eWindowsStoreBillingCurrentAppData* data = (s3eWindowsStoreBillingCurrentAppData*) systemData;
    AppendMessage("*** Current App Data ***");
    AppendMessage("AppId: %s", data->AppId);
    AppendMessage("URI: %s", data->LinkURI);
    AppendMessage("Trial: %s", data->IsTrial ? "true" : "false");
    AppendMessage("Active: %s", data->IsActive ? "true" : "false");
    AppendMessage("ExpirationDate: %s", data->ExpirationDate);
    return 0;
}

int32 onProdLicense(void* systemData, void* userData)
{
    s3eWindowsStoreBillingProductLicenseData* data = (s3eWindowsStoreBillingProductLicenseData*) systemData;
    if (data)
    {
        AppendMessage("*** Product License Data ***");
        AppendMessage("ProdId: %s", data->ProductId);
        AppendMessage("Active: %s", data->IsActive ? "true" : "false");
        AppendMessage("Type: %s", data->IsConsumable ? "consumable" : "durable");
    }
    return 0;
}

int32 onListing(void* systemData, void* userData)
{
    s3eWindowsStoreBillingListingInformation* data = (s3eWindowsStoreBillingListingInformation*) systemData;

    if (data)
    {
        AppendMessage("*** Listing ***");
        AppendMessage("Name: %s", data->Name);
        AppendMessage("Description: %s", data->Description);
        AppendMessage("Price: %s", data->FormattedPrice);
        AppendMessage("NumProducts: %d", data->NumberOfProducts);
        AppendMessage("AgeRating: %d", data->AgeRating);
        AppendMessage("CurrentMarket: %s", data->CurrentMarket);
        g_Billing.ClearList();

        for (int i=0; i<data->NumberOfProducts; i++)
        {
            AppendMessage("Product %d: %s (%s)", i, data->ProductListing[i]->Name, data->ProductListing[i]->ProductId);
            g_Billing.AddProduct(
                data->ProductListing[i]->Name,
                data->ProductListing[i]->ProductId,
                data->ProductListing[i]->FormattedPrice,
                data->ProductListing[i]->ProductType == S3E_WINDOWSSTOREBILLING_PRODUCTTYPE_CONSUMABLE);
        }

        g_Billing.UpdateUI();
    }

    return 0;
}

int32 onPurchase(void* systemData, void* userData)
{
    s3eWindowsStoreBillingProductReceiptDataFull* data = (s3eWindowsStoreBillingProductReceiptDataFull*) systemData;
    if (data)
    {
        const char* pid = data->ProductData.ProductId;
        AppendMessage("*** Receipt ***");
        AppendMessage("ReceiptData.CertificateId %s", data->ReceiptData.CertificateId);
        AppendMessage("ReceiptData.ReceiptDate %s", data->ReceiptData.ReceiptDate);
        AppendMessage("ReceiptData.ReceiptDeviceId %s", data->ReceiptData.ReceiptDeviceId);
        AppendMessage("ReceiptData.Version %s", data->ReceiptData.Version);

        AppendMessage("ProductData.AppId %s", data->ProductData.AppId);
        AppendMessage("ProductData.ExpirationDate %s", data->ProductData.ExpirationDate);
        AppendMessage("ProductData.Id %s", data->ProductData.Id);
        AppendMessage("ProductData.ProductId %s", data->ProductData.ProductId);
        AppendMessage("ProductData.ProductType: %s", data->ProductData.ProductType == s3eWindowsStoreBillingProductType::S3E_WINDOWSSTOREBILLING_PRODUCTTYPE_CONSUMABLE ? "consumable" : "durable");
        AppendMessage("ProductData.PurchaseDate %s", data->ProductData.PurchaseDate);

        AppendMessage("RawXML: %s", data->RawXML);

        AppendMessage("PurchasePrice %s", data->PurchasePrice);
        AppendMessage("PurchaseDate %s", data->PurchaseDate);
        AppendMessage("PublisherUserId %s", data->PublisherUserId);
        AppendMessage("PublisherDeviceId %s", data->PublisherDeviceId);
        AppendMessage("MicrosoftProductId %s", data->MicrosoftProductId);

        g_Billing.UpdateProductState(pid, TOFULFILL);
        g_Billing.UpdateUI();
    }
    return 0;
}

int32 onApplicationReceipt(void* systemData, void* userData)
{
    s3eWindowsStoreBillingApplicationReceiptDataFull* data = (s3eWindowsStoreBillingApplicationReceiptDataFull*) systemData;
    if (data)
    {
        AppendMessage("*** Receipt ***");
        AppendMessage("ReceiptData.CertificateId %s", data->ReceiptData.CertificateId);
        AppendMessage("ReceiptData.ReceiptDate %s", data->ReceiptData.ReceiptDate);
        AppendMessage("ReceiptData.ReceiptDeviceId %s", data->ReceiptData.ReceiptDeviceId);
        if (data->ReceiptData.PublisherDeviceId)
            AppendMessage("ReceiptData.PublisherDeviceId %s", data->ReceiptData.PublisherDeviceId);
        AppendMessage("ReceiptData.Version %s", data->ReceiptData.Version);

        AppendMessage("AppData.AppId %s", data->AppData.AppId);
        AppendMessage("AppData.Id %s", data->AppData.Id);
        AppendMessage("AppData.LicenseType %s", data->AppData.LicenseType);
        AppendMessage("AppData.PurchaseDate %s", data->AppData.PurchaseDate);

        for(unsigned int unProductIndex = 0; unProductIndex < data->unProductsCount; ++unProductIndex)
        {
            s3eWindowsStoreBillingProductReceiptData& prod = data->ProductsData[unProductIndex];
            AppendMessage("ProductData.ProductId %s", prod.ProductId);
            AppendMessage("ProductData.AppId %s", prod.AppId);
            AppendMessage("ProductData.ExpirationDate %s", prod.ExpirationDate);
            AppendMessage("ProductData.Id %s", prod.Id);
            AppendMessage("ProductData.ProductType: %s", prod.ProductType == s3eWindowsStoreBillingProductType::S3E_WINDOWSSTOREBILLING_PRODUCTTYPE_CONSUMABLE ? "consumable" : "durable");
            AppendMessage("ProductData.PurchaseDate %s", prod.PurchaseDate);
        }
        AppendMessage("RawXML: %s", data->RawXML);

        g_Billing.UpdateUI();
    }
    return 0;
}

int32 onReport(void* systemData, void* userData)
{
    s3eWindowsStoreBillingFulfillmentData* data = (s3eWindowsStoreBillingFulfillmentData*) systemData;
    if (data)
    {
        const char* pid = data->ProductId;
        AppendMessage("Reported fulfilled: %s", data->ProductId);
        g_Billing.UpdateProductState(pid, FULFILLED);
        g_Billing.UpdateProductState(pid, TOBUY);
        g_Billing.UpdateUI();
    }
    return 0;
}

int32 onLicense(void* systemData, void* userData)
{
    bool modified = false;
    s3eWindowsStoreBillingLicenseInformation* info = (s3eWindowsStoreBillingLicenseInformation*) systemData;
    AppendMessage("*** License ***");
    AppendMessage("Active: %s", info->IsActive ? "true" : "false");
    AppendMessage("Trial: %s", info->IsTrial ? "true" : "false");
    AppendMessage("Expiration date: %s", info->ExpirationDate);
    AppendMessage("NumLicenses: %d", info->NumberOfProductLicenses);
    for (int i = 0; i<info->NumberOfProductLicenses; i++)
    {
        // update state bases license data:
        if (info->ProductLicenses[i]->IsActive && info->ProductLicenses[i]->IsConsumable)
        {
            modified = true;
            g_Billing.UpdateProductState(info->ProductLicenses[i]->ProductId, TOFULFILL);
        }
        AppendMessage("License %d: (%s) %s (%s)",
            i,
            info->ProductLicenses[i]->IsConsumable ? "consumable" : "durable",
            info->ProductLicenses[i]->ProductId,
            info->ProductLicenses[i]->IsActive ? "active" : "not active");
    }
    if (modified)
        g_Billing.UpdateUI();
    return 0;
}

int32 onUnfulfilledConsumable(void* systemData, void* userData)
{
    s3eWindowsStoreBillingUnfulfilledConsumableList* data = (s3eWindowsStoreBillingUnfulfilledConsumableList*) systemData;
    AppendMessage("*** Current Unfulfilled Consumables ***");
    for(unsigned int unUnfulfilledConsumableIndex = 0
       ;unUnfulfilledConsumableIndex < data->NumberOfUnfulfilledConsumables
       ;++unUnfulfilledConsumableIndex
       )
    {
        AppendMessage("ProductId: %s", data->UnfulfilledConsumables[unUnfulfilledConsumableIndex]->ProductId);
        AppendMessage("OfferID: %s", data->UnfulfilledConsumables[unUnfulfilledConsumableIndex]->OfferId);
        AppendMessage("TransactionId: %s", data->UnfulfilledConsumables[unUnfulfilledConsumableIndex]->TransactionId);
    }
    return 0;
}

int32 onErrorBuyFull(void* systemData, void* userData)
{
    s3eWindowsStoreBillingError data = *(s3eWindowsStoreBillingError*) systemData;
    switch (data)
    {
    case s3eWindowsStoreBillingError::S3E_WINDOWSSTOREBILLING_ERR_PURCHASE_CANCELED:
        AppendMessage("BUY FULL PRODUCT: Purchase cancelled.");
        break;
    case s3eWindowsStoreBillingError::S3E_WINDOWSSTOREBILLING_ERR_PURCHASE:
        AppendMessage("BUY FULL PRODUCT: Error purchase.");
        break;
    case s3eWindowsStoreBillingError::S3E_WINDOWSSTOREBILLING_ERR_PURCHASE_PURCHASED:
        AppendMessage("BUY FULL PRODUCT: Product has already been purchased.");
        break;
    case s3eWindowsStoreBillingError::S3E_WINDOWSSTOREBILLING_ERR_NONE:
        AppendMessage("BUY FULL PRODUCT: Purchase successful.");
        break;
    default:
        AppendMessage("BUY FULL PRODUCT: Error UNKNOWN.");
        break;
    }
    return 0;
}

int32 onError(void* systemData, void* userData)
{
    s3eWindowsStoreBillingErrorData* data = (s3eWindowsStoreBillingErrorData*) systemData;
    if (data == NULL)
    {
        AppendMessage("Error with error.");
        return 1;
    }

    switch (data->code)
    {
    case s3eWindowsStoreBillingError::S3E_WINDOWSSTOREBILLING_ERR_FULFILLMENT:
        AppendMessage("Error FULFILLMENT:");
        break;
    case s3eWindowsStoreBillingError::S3E_WINDOWSSTOREBILLING_ERR_LICENSE:
        AppendMessage("Error LICENSE:");
        break;
    case s3eWindowsStoreBillingError::S3E_WINDOWSSTOREBILLING_ERR_LISTING:
        AppendMessage("Error LISTING:");
        break;
    case s3eWindowsStoreBillingError::S3E_WINDOWSSTOREBILLING_ERR_PURCHASE:
        AppendMessage("Error PURCHASE:");
        break;
    case s3eWindowsStoreBillingError::S3E_WINDOWSSTOREBILLING_ERR_UNFULFILLEDCONSUMABLES:
        AppendMessage("Error UNFULFILLEDCONSUMABLES:");
        break;
    case s3eWindowsStoreBillingError::S3E_WINDOWSSTOREBILLING_ERR_APPRECEIPT:
        AppendMessage("Error APPRECEIPT:");
        break;
    case s3eWindowsStoreBillingError::S3E_WINDOWSSTOREBILLING_ERR_NONE:
        AppendMessage("Error NONE:");
        break;
    default:
        AppendMessage("Error UNKNOWN.");
        break;
    }
    AppendMessage("%s %s", data->Message, data->ProductId == NULL ? "" : data->ProductId);

    return 0;
}

void ExampleInit()
{
    InitMessages(25, 256);

    // Check whether or not the extension is available and is ready to use
    if (!s3eWindowsStoreBillingAvailable())
    {
        AppendMessage("InAppBilling extension unavailable");
        return;
    }

    // Create Example Products
    g_Billing.InitProducts();
    g_Billing.InitUI();

    // todo rename to init
    s3eWindowsStoreBillingInit();

    // Register for in-app events:
    s3eWindowsStoreBillingRegister(S3E_WINDOWSSTOREBILLING_CURRENTAPP, onAppData, NULL);
    s3eWindowsStoreBillingRegister(S3E_WINDOWSSTOREBILLING_PRODUCTLICENSE, onProdLicense, NULL);
    s3eWindowsStoreBillingRegister(S3E_WINDOWSSTOREBILLING_LISTINGINFORMATION, onListing, NULL);
    s3eWindowsStoreBillingRegister(S3E_WINDOWSSTOREBILLING_LICENSEINFORMATION, onLicense, NULL);
    s3eWindowsStoreBillingRegister(S3E_WINDOWSSTOREBILLING_PRODUCTPURCHASE, onPurchase, NULL);
    s3eWindowsStoreBillingRegister(S3E_WINDOWSSTOREBILLING_FULFILLMENT, onReport, NULL);
    s3eWindowsStoreBillingRegister(S3E_WINDOWSSTOREBILLING_UNFULFILLEDCONSUMABLES, onUnfulfilledConsumable, NULL);
    s3eWindowsStoreBillingRegister(S3E_WINDOWSSTOREBILLING_APPRECEIPT, onApplicationReceipt, NULL);
    s3eWindowsStoreBillingRegister(S3E_WINDOWSSTOREBILLING_ERROR, onError, NULL);
    s3eWindowsStoreBillingRegister(S3E_WINDOWSSTOREBILLING_BUY, onErrorBuyFull, NULL);
}

void ExampleTerm()
{
}

bool ExampleUpdate()
{
    // Act on user action / button press
    g_Billing.HandleButtonPress();

    return true;
}

void ExampleRender()
{
    ButtonsRender();

    int y = GetYBelowButtons();
    y +=  20;
    PrintMessages(10, y);
}
