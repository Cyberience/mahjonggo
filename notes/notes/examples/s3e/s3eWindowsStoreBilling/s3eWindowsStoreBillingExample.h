/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
#include <sstream>
#include <string>
#include <map>

#include "ExamplesMain.h"
#include "s3eWindowsStoreBilling.h"

#include "IwDebug.h"
#include <malloc.h>

bool IsWS8()
{
    return s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WS8;
}

bool IsWP8()
{
    return s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WP8;
}

enum EnumProductState
{
    UNKNOWN = 0,
    TOBUY,     //
    TOFULFILL, // i.e. active
    FULFILLED
};

class MyProduct
{
    MyProduct(){}
public:
    MyProduct(const char* name)
    {
        m_State = UNKNOWN;
        m_Name = name;
    }

    virtual ~MyProduct()
    {
    }
    virtual std::string GetStatus() { return "---"; }

    virtual void SetState(EnumProductState state)
    {
        m_State = state;
    }

    void SetProductId(const char* pid)
    {
        m_Pid = pid != NULL ? pid : "";
    }

    void SetPrice(const char* price)
    {
        m_Price = price != NULL ? price : "";
    }


    std::string m_Pid;
    std::string m_Price;
    std::string m_Name;
    bool m_IsActive;
    EnumProductState m_State;
    std::string m_Status;
};

class MyDurable : public MyProduct
{
public:
    MyDurable(const char* name) : MyProduct(name)
    {
    }

    ~MyDurable()
    {
    }

    std::string GetStatus()
    {
        return "Durable: " + m_Name + (m_IsActive ? " (active)" : "");
    }
};
class MyConsumable : public MyProduct
{
public:
    MyConsumable(const char* name) : MyProduct(name)
    {
        m_Quantity = 0;
    }

    ~MyConsumable()
    {
    }

    virtual void SetState(EnumProductState state)
    {
        m_State = state;
        if (state == FULFILLED)
        {
            m_Quantity++;
            AppendMessage("Quantity increased to %d", m_Quantity);
        }
    }

    std::string GetStatus()
    {
        std::stringstream ss;
        ss << m_Quantity;

        return "Consumable: " + ss.str() + " x " + m_Name + (m_IsActive ? " (active)" : "");
    }


    int m_Quantity;
};

struct ButtonData
{
    ButtonData(Button* btn)
    {
        button = btn;
    }
    Button* button;
    std::string pstr;
};

class MyBillingExample
{
    typedef std::map<std::string, MyProduct*> MyMap;
    typedef std::map<std::string, ButtonData*> MyButtonMap;
public:
    MyBillingExample()
    {

    }
    ~MyBillingExample()
    {
        ClearButtons();
        ClearList();
    }

    void InitProducts()
    {
        if (m_Products.size() > 0)
            ClearList();
    }

    ButtonData* GetButtonData(Button* button)
    {
        MyButtonMap::iterator it = m_Buttons.begin();
        while (it != m_Buttons.end())
        {
            if ((*it).second->button == button)
                return (*it).second;
            it++;
        }
        return NULL;
    }

    const char* FindProductFromId(const char* pid)
    {
        MyMap::iterator it = m_Products.begin();
        while (it != m_Products.end())
        {
            if ((*it).second->m_Pid == pid)
                return (*it).second->m_Name.c_str();
            it++;
        }
        return NULL;
    }
    void InitUI()
    {
        UpdateUI();
    }

    void UpdateUI()
    {
        // create action buttons
        // 1. List Products
        if (m_Buttons["list"] == NULL)
        {
            m_Buttons["list"] = new ButtonData(NewButton("List all products"));
            m_Buttons["list"]->button->m_Enabled = true;
        }

        // 2. Refresh product list based on license scan
        if (m_Buttons["refresh"] == NULL)
        {
            m_Buttons["refresh"] = new ButtonData(NewButton("Refresh list via licenses"));
            m_Buttons["refresh"]->button->m_Enabled = true;
        }

        // 3. IsTrial button
        if (m_Buttons["istrial"] == NULL)
        {
            m_Buttons["istrial"] = new ButtonData(NewButton("Check trial"));
            m_Buttons["istrial"]->button->m_Enabled = true;
        }

        // 4. Buy button
        if (m_Buttons["buy"] == NULL)
        {
            m_Buttons["buy"] = new ButtonData(NewButton("Buy full app"));
            m_Buttons["buy"]->button->m_Enabled = true;
        }

        // 5. Request fullfillment list button
        if (IsWP8() == false && IsWS8() == false && m_Buttons["rfulfil"] == NULL)
        {
            m_Buttons["rfulfil"] = new ButtonData(NewButton("Request list of full fillment needs."));
            m_Buttons["rfulfil"]->button->m_Enabled = true;
        }

        // 6. Request full app receipt.
        if (IsWP8() == false && m_Buttons["appreceipt"] == NULL)
        {
            m_Buttons["appreceipt"] = new ButtonData(NewButton("Request full app receipt."));
            m_Buttons["appreceipt"]->button->m_Enabled = true;
        }

        // 5. BUY FULFILL Populate product button list
        MyMap::iterator it = m_Products.begin();
        while (it != m_Products.end())
        {
            char* action;
            switch ((*it).second->m_State)
            {
            case UNKNOWN:
                action = "ERR";
                IwTrace(MYAPP, ("Unknown product state."));
                break;
            case TOBUY:
                action = "BUY";
                break;
            case TOFULFILL:
                action = "FULFILL";
                break;
            case FULFILLED:
                action = "USE"; // bought and fulfilled!
                break;
            }
            std::stringstream ss;
            ss << action <<  " " << (*it).second->m_Name << "@" << (*it).second->m_Price;
            ButtonData* data = m_Buttons[(*it).second->m_Name];
            if (data != NULL)
            {
                free(data->button->m_Name);
                data->button->m_Name = strdup(ss.str().c_str());
            }
            else
            {
                data = m_Buttons[(*it).second->m_Name] = new ButtonData(NewButton(ss.str().c_str()));
                data->pstr = (*it).second->m_Name;
            }
            it++;
        }
    }

    s3eResult HandleBuyAction(Button* selectedButton)
    {
        bool buying = false;
        ButtonData* data = GetButtonData(selectedButton);
        if (data)
        {
            std::string &pid = m_Products[data->pstr]->m_Pid;
            const char* cpid = pid.c_str();
            if (cpid)
            {
                buying = true;
                s3eWindowsStoreBillingGetLicenseInformation(cpid);
                s3eWindowsStoreBillingRequestProductPurchase(cpid);
            }
        }

        return buying ? S3E_RESULT_SUCCESS : S3E_RESULT_ERROR;
    }

    s3eResult HandleFulfillmentAction(Button* selectedButton)
    {
        bool reporting = false;
        ButtonData* data = GetButtonData(selectedButton);
        if (data)
        {
            std::string &pid = m_Products[data->pstr]->m_Pid;
            const char* cpid = pid.c_str();
            if (cpid)
            {
                reporting = true;
                s3eWindowsStoreBillingReportProductFulfillment(cpid);
            }
        }

        return reporting ? S3E_RESULT_SUCCESS : S3E_RESULT_ERROR;
    }

    void HandleButtonPress()
    {
        Button* selectedButton = GetSelectedButton();
        if (selectedButton == NULL)
            return;

        if (m_Buttons["list"]->button == selectedButton)
        {
            s3eWindowsStoreBillingGetCurrentAppData();
            s3eWindowsStoreBillingLoadListingInformationEmpty();
        }
        else if (m_Buttons["istrial"]->button == selectedButton)
        {
            bool isTrial = false;
            s3eWindowsStoreBillingIsTrial(&isTrial);
            if (isTrial)
                AppendMessage("Trial license.");
            else
                AppendMessage("Not trial license.");
        }
        else if (IsWP8() == false && IsWS8() == false  && m_Buttons["rfulfil"]->button == selectedButton)
        {
            s3eWindowsStoreBillingGetUnfulfilledConsumable();
        }
        else if (m_Buttons["buy"]->button == selectedButton)
        {
            s3eWindowsStoreBillingBuyFull();
        }
        else if (m_Buttons["refresh"]->button == selectedButton)
        {
            if (m_Products.size() > 0)
            {
                s3eWindowsStoreBillingGetLicenseInformation(NULL);
            }
            else
            {
                AppendMessage("No product list to refresh.");
            }
        }
        else if (IsWP8() == false && m_Buttons["appreceipt"]->button == selectedButton)
        {
            s3eWindowsStoreBillingGetAppReceipt();
        }
        else if (selectedButton->m_Name && strlen(selectedButton->m_Name) >= 3)
        {
            char action[4];
            strncpy(action, selectedButton->m_Name, 3);

            if (strncmp(action, "BUY", 3) == 0)
            {
                if (HandleBuyAction(selectedButton) == S3E_RESULT_ERROR)
                {
                    AppendMessage("Error trying to purchase product.");
                }
            }
            else if (strncmp(action, "FUL", 3) == 0)
            {
                if (HandleFulfillmentAction(selectedButton) == S3E_RESULT_ERROR)
                {
                    AppendMessage("Error before reporting fulfillment.");
                }
            }
            else
            {
                AppendMessage("Unknown action selected: %s", action);
                return;
            }
        }
    }

    void UpdateProductState(const char* pid, EnumProductState state)
    {
        const char* name = FindProductFromId(pid);

        if (name && m_Products[name])
        {
            m_Products[name]->SetState(state);
            AppendMessage("Appending state %d for %s", (int) state, name);
        }
        else
        {
            AppendMessage("Error appending state: missing product.");
        }
    }

    void AddProduct
        (const char* name
        ,const char* pid
        ,const char* price
        ,bool isconsumable
        )
    {
        MyProduct* prod = NULL;
        if (isconsumable)
            prod = new MyConsumable(name);
        else
            prod = new MyDurable(name);

        prod->SetProductId(pid);
        prod->SetState(TOBUY);
        prod->SetPrice(price);

        m_Products[name] = prod;

        std::stringstream ss;
        ss << "Adding " << name;
        AppendMessage(ss.str().c_str());
    }

    void ClearList()
    {
        MyMap::iterator it = m_Products.begin();
        while (it != m_Products.end())
        {
            delete (*it).second;
            it++;
        }
        m_Products.clear();
    }

private:
    void ClearButtons()
    {
        m_Buttons.clear();
        DeleteButtons();
    }

    MyButtonMap m_Buttons;
    MyMap m_Products;
};
