/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3ESocket S3E Socket Example
 *
 * The following example uses S3E's socket functionality to send a string to
 * a test server via TCP/UDP protocols. It can also listen to a local socket
 * and acknowledge any incomming connection.
 *
 * The main functions used to achieve this are:
 *  - s3eInetLookup()
 *  - s3eSocketConnect()
 *  - s3eSocketSend()
 *  - s3eSocketSendTo()
 *  - s3eSocketRecv()
 *
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eSocketImage.png
 *
 * @include s3eSocket.cpp
 */
#include "ExamplesMain.h"
#include "s3eOSReadString.h"
#include <stdlib.h> // for atoi
#include <string.h> // for strlcpy

#define ECHO_HOST  "sdktest.madewithmarmalade.com"
#define ECHO_PORT  7
#define LISTEN_PORT  7777
#define SOCKET_TIMEOUT (30000)

static int g_nPort = LISTEN_PORT;
static char g_IPAddress[48] = {"/0"};
static char g_ErrorString[256];             //Contains operation results for display
static bool g_SocketIsConnected = false;    //Flag that keeps track of connect status
static bool g_Listening = false;
static bool g_IPv6 = false;
static bool g_TCP = true;
static s3eSocket* g_Sock = NULL;            //Socket for send/receive operation
static s3eSocket* g_AcceptedSocket = NULL;
static Button* g_ButtonConnect = NULL;
static Button* g_ButtonConnectEcho = NULL;
static Button* g_ButtonListen = NULL;
static Button* g_ButtonSend = NULL;
static Button* g_ButtonClose = NULL;
static Button* g_ButtonToggleIPProtocols = NULL;
static Button* g_ButtonToggleTCPUDP = NULL;

// Connect callback
static int32 ConnectCB(s3eSocket* g_Sock, void* sys, void* data)
{
    s3eResult res = *(s3eResult*)sys;

    if (res == S3E_RESULT_SUCCESS) {
        g_SocketIsConnected = true;
    }
    else
        g_SocketIsConnected = false;

    return 0;
}

static void IncommingConnection(s3eSocket* newSocket, s3eInetAddress* address)
{
    s3eDebugTracePrintf("IncommingConnection: %p %s", newSocket, s3eInetToString(address));
    g_AcceptedSocket = newSocket;
}

static int32 AcceptCallback(s3eSocket* sock, void* sysData, void* userData)
{
    s3eInetAddress addr;
    s3eSocket* newSocket = s3eSocketAccept(g_Sock, &addr, NULL, NULL);
    if (newSocket)
        IncommingConnection(newSocket, &addr);
    return 0;
}

/**
 * Callback function for the Listen button.
 */
static void Listen(Button* button)
{
    s3eSocketType socketType = g_TCP ? S3E_SOCKET_TCP : S3E_SOCKET_UDP;
    s3eSocketDomain socketDomain = g_IPv6 ? S3E_SOCKET_INET6 : S3E_SOCKET_INET;

    g_Sock = s3eSocketCreate(socketType, socketDomain);
    if (g_Sock == NULL)
    {
        strcpy(g_ErrorString,"`x666666Creation of socket failed");
        return;
    }

    s3eConfigGetInt("Example", "Port", &g_nPort);

    // look up address
    s3eInetAddress addr;
    memset(&addr, 0, sizeof(addr));
    addr.m_Type = g_IPv6 ? S3E_SOCKET_ADDR_IPV6 : S3E_SOCKET_ADDR_IPV4;
    addr.m_Port = s3eInetHtons(g_nPort);
    strcpy(g_IPAddress, s3eInetToString(&addr));

    if (s3eSocketBind(g_Sock, &addr, S3E_TRUE) != S3E_RESULT_SUCCESS)
    {
        strcpy(g_ErrorString,"`x666666Bind failed");
        s3eSocketClose(g_Sock);
        g_Sock = NULL;
        return;
    }

    if (s3eSocketListen(g_Sock, 10) != S3E_RESULT_SUCCESS)
    {
        strcpy(g_ErrorString,"`x666666Listen failed");
        return;
    }

    g_Listening = true;
    g_SocketIsConnected = true;
    s3eSocket* newSocket = s3eSocketAccept(g_Sock, &addr, AcceptCallback, NULL);
    if (newSocket)
        IncommingConnection(newSocket, &addr);
}

/*
 * The following function creates a socket and attempts to establish a connection.
 * Once the socket is created the s3eInetLookup() function is used to resolve the
 * IP address. The port set in ECHO_PORT are entered in a s3eInetAddress
 * structure. This structure, the created socket and a callback function are all
 * passed to the s3eSocketConnect() function. A wait is performed for a minute
 * while the connect is attempted. When connection succeeds the callback sets
 * the <code>g_SocketIsConnected</code> bool to true.
 */
static void Connect(const char* host, uint16 port)
{
    s3eSocketType socketType = g_TCP ? S3E_SOCKET_TCP : S3E_SOCKET_UDP;
    s3eSocketDomain socketDomain = g_IPv6 ? S3E_SOCKET_INET6 : S3E_SOCKET_INET;

    //Creating new socket
    g_Sock = s3eSocketCreate(socketType, socketDomain);
    if (g_Sock == NULL)
    {
        strcpy(g_ErrorString,"`x666666Creation of socket failed");
        return;
    }

    g_SocketIsConnected = false;

    // look up address
    s3eInetAddress addr;
    memset(&addr, 0, sizeof(addr));
    s3eSocketDomain hint = g_IPv6 ? S3E_SOCKET_INET6 : S3E_SOCKET_INET;
    if (s3eInetLookup(host, &addr, NULL, NULL, hint, 1) == S3E_RESULT_ERROR)
    {
        sprintf(g_ErrorString,"`x666666s3eInetLookup failed: %s", host);
        return;
    }

    addr.m_Port = s3eInetHtons(port);
    strcpy(g_IPAddress, s3eInetToString(&addr));

    int32 counter = 0;
    // Using the created socket, address structure and set callback function the Connect is called.
    // A wait is performed for a minute while the connect is attempted. When connection succeeds the
    // callback sets the g_SocketIsConnected bool to true causing the waiting to stop

    bool bNeedToWaitConnection = false;
    if (s3eSocketConnect(g_Sock, &addr, &ConnectCB, NULL) != S3E_RESULT_SUCCESS)
    {
        switch (s3eSocketGetError())
        {
            // These errors are 'OK', because they mean,
            // that a connect is in progress
            case S3E_SOCKET_ERR_INPROGRESS:
                bNeedToWaitConnection = true;
                break;
            case S3E_SOCKET_ERR_ALREADY:
            case S3E_SOCKET_ERR_WOULDBLOCK:
                break;
            default:
                // A 'real' error happened
                sprintf(g_ErrorString,"`x666666Connecting failed:\n%s", s3eSocketGetErrorString());
                s3eSocketClose(g_Sock);
                g_Sock = NULL;
                return;
        }
    }
    else
    {
        bNeedToWaitConnection = true;
    }

    if (bNeedToWaitConnection)
    {
        // Try to connect, but wait a maximum time of SOCKET_TIMEOUT
        uint64 testStartTime = s3eTimerGetMs();
        while (!ExampleCheckQuit() && s3eTimerGetMs()-testStartTime < SOCKET_TIMEOUT)
        {
            // Stop waiting since socket is now connected
            if (g_SocketIsConnected)
                break;

            // Press key 4 in order to cancel connect operation
            s3eKeyboardUpdate();
            if (s3eKeyboardGetState(s3eKey4) & S3E_KEY_STATE_PRESSED)
            {
                s3eSocketClose(g_Sock);
                g_Sock = NULL;
                strcpy(g_ErrorString,"");
                return;
            }

            //Update screen during connect operation
            s3eSurfaceClear(0xff,0xff,0xff);
            s3eDebugPrint(10, 120, g_ErrorString, 1);
            sprintf(g_ErrorString,"`x666666 Trying to connect");
            s3eDebugPrint(10, 80, "`x666666Press 4 to cancel connect", 0);

            switch (++counter%8)
            {
                case 0 :
                case 4 :
                    strcat(g_ErrorString, " |");break;
                case 1 :
                case 5:
                    strcat(g_ErrorString, " /");break;
                case 2 :
                case 6 :
                    strcat(g_ErrorString, " -");break;
                case 3 :
                case 7 :
                    strcat(g_ErrorString, " \\");break;
            }

            s3eSurfaceShow();
            s3eDeviceYield(30);
        }
    }

    if (g_SocketIsConnected == false)
    {
        strcpy(g_ErrorString,"`x666666Socket connecting timed out");
        s3eSocketClose(g_Sock);
        g_Sock = NULL;
    }
    else
    {
        strcpy(g_ErrorString,"");
    }
}



static void ConnectToEcho(Button* button)
{
    Connect(ECHO_HOST, ECHO_PORT);
}

 void SendToEchoUDP(const char* host, uint16 port)
{
    const char* message = "Hello World!";

    s3eSocketDomain socketDomain = g_IPv6 ? S3E_SOCKET_INET6 : S3E_SOCKET_INET;

    //Creating new socket
    g_Sock = s3eSocketCreate(S3E_SOCKET_UDP, socketDomain);
    if (g_Sock == NULL)
    {
        strcpy(g_ErrorString,"`x666666Creation of socket failed");
        return;
    }

    // look up address
    s3eInetAddress addr;
    memset(&addr, 0, sizeof(addr));

    if (s3eInetLookup(host, &addr, NULL, NULL, S3E_SOCKET_UNSPEC, 1) == S3E_RESULT_ERROR)
    {
        sprintf(g_ErrorString,"`x666666s3eInetLookup failed: %s", host);
        return;
    }

    addr.m_Port = s3eInetHtons(port);

    int bytes_sent = s3eSocketSendTo(g_Sock, message, strlen(message), 0, &addr);

    if (bytes_sent == -1)
    {
        // Any other error should be treated as "sending failed"
        sprintf(g_ErrorString, "`x666666udp send failed: %s", s3eSocketGetErrorString());
    } else {
        sprintf(g_ErrorString, "`x666666%d bytes sent", bytes_sent);
    }

    s3eSocketClose(g_Sock);
    g_Sock = NULL;
}

static void Connect(Button* button)
{
    char hostname[255];
    int port;
    s3eResult res = s3eConfigGetInt("Example", "Port", &port);
    if (res == S3E_RESULT_SUCCESS)
        res = s3eConfigGetString("Example", "Host", hostname);

    if (res != S3E_RESULT_SUCCESS)
    {
        if (!s3eOSReadStringAvailable())
        {
            s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "This plaform does not support the OSReadString extension\nPlease use the ICF settings to set the name of the server you want to connect to.");
            return;
        }
        else
        {
            const char* host = s3eOSReadStringUTF8("Enter hostname or IP");
            if (host == NULL)
                return;
            strlcpy(hostname, host, 255);
            const char* portStr = s3eOSReadStringUTF8("Enter port number", S3E_OSREADSTRING_FLAG_NUMBER);
            if (portStr == NULL)
                return;
            port = atoi(portStr);
        }
    }

    Connect(hostname, port);
}

/*
 * This function attempts to send and receive information over the established
 * connection. The s3eSocketSend() function is called till all bytes in the
 * string are sent. The s3eSocketRecv() function is then used to receive the
 * echo of the sent string.
 */
void SendAndReceive(Button* button)
{

    if (!g_TCP)
        return SendToEchoUDP(ECHO_HOST, ECHO_PORT);

    /*
     * Sending data
     */
    const char* messageToSend = "hello server!";
    int32       messageLength = strlen(messageToSend);
    int32       messageSent = 0;
    int32       retValue;

    // Loop until the full message has been send
    do
    {
        // returns the number of bytes sent, or -1 on failure.
        retValue = s3eSocketSend(g_Sock, messageToSend+messageSent, messageLength-messageSent, 0);

        // sending has been successfull, see how many bytes are left to send
        if (retValue > 0)
            messageSent += retValue;

        // an error occurred while sending
        if (retValue < 0)
        {
            // This error is OK, since S3E_SOCKET_ERR_AGAIN means, that
            // a function is in process right now
            if (s3eSocketGetError() == S3E_SOCKET_ERR_AGAIN)
            {
                s3eDeviceYield(50);
                continue;
            }

            // Any other error should be treated as "sending failed"
            sprintf(g_ErrorString, "`x666666send failed: %s", s3eSocketGetErrorString());
            return;
        }

    } while(messageSent < messageLength);


    /*
     * Receiving data
     */
    char messageReceived[0x200];            //Buffer for answer from server

    //Wait for answer for a maximum of SOCKET_TIMEOUT
    uint64 testStartTime = s3eTimerGetMs();
    while (s3eTimerGetMs()-testStartTime < SOCKET_TIMEOUT)
    {
        //return number of bytes read from socket or -1 on failure.
        retValue = s3eSocketRecv(g_Sock, messageReceived, 0x200, 0);

        //We received an amount of data
        if (retValue != -1)
            break;
        s3eDeviceYield(50);
    }

    // An error occurred
    if (retValue == -1)
    {
        sprintf(g_ErrorString, "`x666666Receive failed: %d", s3eSocketGetError());
        return;
    }

    messageReceived[messageLength] = '\0';

    // Compare sent and received data
    if (strcmp(messageReceived, messageToSend) != 0)
        sprintf(g_ErrorString, "`x666666Reply: '%s'\nis not correct", messageReceived);
    else
        sprintf(g_ErrorString, "`x666666Reply: '%s'\nis correct", messageReceived);

    return;
}

static void CloseSocket(Button* button)
{
    if (g_Sock)
    {
        g_SocketIsConnected = false;
        g_Listening = false;
        s3eSocketClose(g_Sock);
        g_Sock = NULL;
    }

    if (g_AcceptedSocket)
    {
        s3eSocketClose(g_AcceptedSocket);
        g_AcceptedSocket = NULL;
        g_IPAddress[0] = '\0';
    }

    g_ErrorString[0] = '\0';
}

static void ToggleIPProtocols(Button* button)
{
    g_IPv6 = !g_IPv6;
    CloseSocket(NULL);
}

static void ToggleTCPUDP(Button* button)
{
    g_TCP = !g_TCP;
    CloseSocket(NULL);
}

void ExampleInit()
{
    g_ButtonConnect = NewButton("Connect to server", Connect);
    g_ButtonConnectEcho = NewButton("Connect to echo server", ConnectToEcho);
    g_ButtonListen = NewButton("Listen for connections", Listen);
    g_ButtonSend = NewButton("Send & Receive Data", SendAndReceive);
    g_ButtonClose = NewButton("Close Socket", CloseSocket);
    g_ButtonToggleIPProtocols = NewButton("Toggle IPv4/IPv6", ToggleIPProtocols);
    g_ButtonToggleTCPUDP = NewButton("Toggle TCP/UDP", ToggleTCPUDP);
}

void ExampleTerm()
{
    CloseSocket(NULL);
}

/*
 * This function waits for keyboard input to start the echo. By pressing s3eKey1
 * the connect() function is called. By pressing s3eKey2
 * the sendAndReceive() function is called.
 */
bool ExampleUpdate()
{
    if (g_AcceptedSocket)
    {
        char buf[1024];
        int rtn = s3eSocketRecv(g_AcceptedSocket, buf, 1024, 0);
        if (rtn > 0)
        {
            buf[rtn] = '\0';
            s3eDebugTracePrintf("recieved %d bytes", rtn);
            snprintf(g_ErrorString, 256, "`x666666Got Data: '%s'", buf);
            s3eSocketSend(g_AcceptedSocket, buf, rtn, 0);
        }
    }

    return true;
}

void ExampleRender()
{
    const int textHeight = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);
    int ypos = GetYBelowButtons() + 2 * textHeight;

    g_ButtonConnect->m_Enabled = !g_SocketIsConnected && g_TCP;
    g_ButtonConnectEcho->m_Enabled = !g_SocketIsConnected && g_TCP;
    g_ButtonListen->m_Enabled = !g_SocketIsConnected && g_TCP;
    g_ButtonSend->m_Enabled = (g_SocketIsConnected || !g_TCP) && !g_Listening;
    g_ButtonClose->m_Enabled = (g_Sock || g_AcceptedSocket) ? true : false;

    ypos += textHeight;

    if (g_AcceptedSocket)
        s3eDebugPrint(10, ypos, "`x666666Incomming connection accepted", 1);
    else if (g_Listening)
        s3eDebugPrintf(10, ypos, 1, "`x666666Listening for connections on %s", g_IPAddress);
    else if (g_SocketIsConnected)
        s3eDebugPrintf(10, ypos, 1, "`x666666Socket connected to %s", g_IPAddress);
    else
        s3eDebugPrint(10, ypos, "`x666666Socket not connected", 1);

    ypos += 2 * textHeight;

    // Display error information
    s3eDebugPrint(10, ypos, g_ErrorString, 1);

    ypos += 8 * textHeight;
    s3eDebugPrintf(10, ypos, 1, "`x666666Hostname: %s", s3eSocketGetString(S3E_SOCKET_HOSTNAME));
    ypos += textHeight;
    s3eDebugPrintf(10, ypos, 1, "`x666666Network Type: %d, Connected: %s ", s3eSocketGetInt(S3E_SOCKET_NETWORK_TYPE), s3eSocketGetInt(S3E_SOCKET_NETWORK_AVAILABLE) ? "yes" : "no");
    ypos += textHeight;
    s3eDebugPrintf(10, ypos, 1, "`x666666Protocol: %s / %s", g_IPv6 ? "IPv6" : "IPv4", g_TCP ? "tcp" : "udp");
}
