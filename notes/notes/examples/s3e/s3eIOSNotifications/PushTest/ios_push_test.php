#!/usr/bin/env php
<?php

// Put your device token here (without spaces). This identifies the device.
$device_token = '123456701234567012345670123456701234567012345670123456701234567';

// Put your private key's passphrase here.
$password = 'mypass';

// Put the path to your SSL key/cert .pem file here. This identifies the app and authenticates the provider.
$pem = 'cert_and_key.pem'

// Put you notifications contents here:
$message = 'my message';
$badge_no = 666;
$sound_file = 'default';

$name = 'msg1';
$data = 'xxx123';

//----------------------------------------------------------------------------------

$ctx = stream_context_create();
stream_context_set_option($ctx, 'ssl', 'local_cert', $pem);
stream_context_set_option($ctx, 'ssl', 'passphrase', $password);

// Open a connection to the Appl Push Notifications Service server
$con = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

if (!$con)
	exit("Failed to connect: $err $errstr" . PHP_EOL);

echo 'Connected!' . PHP_EOL;

// Create payload body
// 'aps' is a required object/dictionary that contains default info
$body['aps'] = array('alert' => $message, 'sound' => $sound_file, 'badge' => $badge_no);
// For interactive push notifications enable category
//$body['aps'] += array('category' => $badge_no);
//
// Other values are ones the SDK defines
$body['name'] = $name;
$body['data'] = $data;
// The extension could be expanded to include other values...

// Encode the payload as JSON
$payload = json_encode($body);

// Build the notification
$msg = chr(0) . pack('n', 32) . pack('H*', $device_token) . pack('n', strlen($payload)) . $payload;
echo $msg;

// Send payload to APNs, which will then deliver to the device
$result = fwrite($con, $msg, strlen($msg));

if (!$result)
	echo 'Message delivery failed: ' . PHP_EOL;
else
	echo 'Message delivered success: ' . PHP_EOL;

fclose($con);
