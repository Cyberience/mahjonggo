/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EIOSNotifications S3E IOS Notifications Example
 *
 * The following example demonstrates the use of the s3eIOSNotifications API.
 * This example only runs on IOS.
 *
 * The example demonstrates:
 *    - Querying the applicaton start up process for any notification(s)
 *    - Getting a token to communicate with a server
 *    - Querying the number of notifications currently sheduled by the application
 *    - Scheduling a notification
 *    - Erasing a notification(s)
 *    - Getting data from local and remote notifications posted to the app
 *    - Changing the badge number associated with the application icon
 *
 * The functions required to achieve this are:
 *    - s3eIOSStartedWithNotification()
 *    - s3eIOSNotificationsGetRemoteNotificationToken()
 *    - s3eIOSNotificationsRegister()
 *    - s3eIOSNotificationsGetScheduleSize()
 *    - s3eIOSNotificationsCreate()
 *    - s3eIOSNotificationsErase()
 *    - s3eIOSNotificationsSetAppBadge()
 *
 *
 * The application waits for buttons to be pressed before using any
 * iOS Notification functionality (Not including setup functions like callback
 * registration and getting the token). Detection of button presses is handled
 * using the generic code in ExamplesMain.cpp
 *
 * Messges are output to screen using PrintMessages().
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eIOSNotificationsImage.png
 *
 * @include s3eIOSNotifications.cpp
 */

#include "ExamplesMain.h"
#include "s3eOSReadString.h"
#include "s3eIOSNotifications.h"

#include <stdlib.h> // for atoi()
#include <string.h>
#include <time.h> // for tm, time_t
#include <climits> // for INT_MAX


// Unique id for recieveing push notifications
const char* g_PushToken = NULL;

// Was the app started by a notification?
static s3eIOSNotificationsLaunchNotification g_LaunchNotification;

// Buttons
static Button* g_ButtonSetNotification = NULL;
static Button* g_ButtonSetNotificationByTimeStamp = NULL;
static Button* g_ButtonSetNotificationGenerated20Sec = NULL;
static Button* g_ButtonSetNotificationGeneratedNow = NULL;
static Button* g_ButtonLocalNotifScheduled = NULL;
static Button* g_ButtonSetBadgeNo = NULL;
static Button* g_ButtonNotificaltionClear = NULL;
static Button* g_ButtonNotificaltionClearAll = NULL;
static Button* g_ButtonUnRegisterFromRemoteNotifcations = NULL;

// Sample strings for generating quick messages
const char* QUICK_NAME = "msg";
const char* QUICK_MESSAGE = "Message Text";
const char* QUICK_DATA = "Event Data";
static int g_QuickMsgCounter = 0;

// Name Constants
#define MAX_NAME_SIZE 256
#define MAX_MESSAGE_SIZE 256

// Callback function(s)
int32 LocalPushNotificationRecieved(void* systemData, void* userData)
{
    s3eIOSNotificationsEventInfo* evntInfo = (s3eIOSNotificationsEventInfo*)systemData;
    if (evntInfo)
    {
        AppendMessageColour(GREEN, "--------------------------------------");
        // notifications may be fired "now" and have no fire date set
        if (evntInfo->m_FireDateS)
        {
            //Convert time in seconds to a readable string
            time_t time = (time_t)evntInfo->m_FireDateS;
            AppendMessageColour(GREEN, "Fired at: %s", ctime (&time));
        }
        else
            AppendMessageColour(GREEN, "Fired at: %s", "'NOW'");

        AppendMessageColour(GREEN, "Data: %s", evntInfo->m_EventData ? evntInfo->m_EventData : "NONE");
        AppendMessageColour(GREEN, "Message: %s", evntInfo->m_EventMessage ? evntInfo->m_EventMessage : "NONE");
        AppendMessageColour(GREEN, "Name: %s", evntInfo->m_EventName ? evntInfo->m_EventName : "NONE");
        AppendMessageColour(GREEN, "Local Push Notification Acknowledged");
    }

    return 1;
}

int32 RemotePushNotificationRecieved(void* systemData, void* userData)
{
    s3eIOSNotificationsEventInfo* toDoItem = (s3eIOSNotificationsEventInfo*)systemData;
    if (toDoItem)
    {
        if (toDoItem->m_ActionId)
        {
            // Remote notifications have no time value
            AppendMessageColour(BLUE, "--------------------------------------");
            AppendMessageColour(BLUE, "Category: %s", toDoItem->m_Category);
            AppendMessageColour(BLUE, "Identifier: %s", toDoItem->m_ActionId);
            AppendMessageColour(BLUE, "Message: %s", toDoItem->m_EventMessage ? toDoItem->m_EventMessage : "NONE");
            AppendMessageColour(BLUE, "Remote Interactive Push Notification Acknowledged");
        }
        else
        {
            // Remote notifications have no time value
            AppendMessageColour(GREEN, "--------------------------------------");
            AppendMessageColour(GREEN, "Data: %s", toDoItem->m_EventData ? toDoItem->m_EventData : "NONE");
            AppendMessageColour(GREEN, "Message: %s", toDoItem->m_EventMessage ? toDoItem->m_EventMessage : "NONE");
            AppendMessageColour(GREEN, "Name: %s", toDoItem->m_EventName ? toDoItem->m_EventName : "NONE");
            AppendMessageColour(GREEN, "Remote Push Notification Acknowledged");
        }
    }

    return 1;
}
int32 remoteRegistrationFailed(void* systemData, void* userData)
{
    AppendMessageColour(RED, "Remote registration failed. Check your setup");
    return 1;
}

int32 remoteRegistrationSucceeded(void* systemData, void* userData)
{
    g_PushToken = s3eIOSNotificationsGetRemoteNotificationToken();
    AppendMessageColour(RED, "push id token: %s", g_PushToken);
    return 1;
}

int32 registeredSettings(void* systemData, void* userData)
{
    char notifTypesString[80];
    strcpy(notifTypesString, "Registered settings: ");
    s3eIOSNotificationsTypes types = *(s3eIOSNotificationsTypes*)systemData;

    if (types == S3E_IOSNOTIFICATIONS_TYPE_NONE)
        strcat(notifTypesString, "none");
    else {
        if (types & S3E_IOSNOTIFICATIONS_TYPE_BADGE)
            strcat(notifTypesString, "badge ");
        if (types & S3E_IOSNOTIFICATIONS_TYPE_SOUND)
            strcat(notifTypesString, "sound ");
        if (types & S3E_IOSNOTIFICATIONS_TYPE_ALERT)
            strcat(notifTypesString, "alert ");
    }
    AppendMessageColour(RED, notifTypesString);
    return 1;
}


void ExampleInit()
{
    InitMessages(10,60);

    g_ButtonSetNotification = NewButton("Set Notification: with h/m d/m/y");
    g_ButtonSetNotificationByTimeStamp = NewButton("Set Notification: with Time Stamp");
    g_ButtonSetNotificationGenerated20Sec = NewButton("Set Notification: Generated, in 20 Secs");
    g_ButtonSetNotificationGeneratedNow = NewButton("Set Notification: Generated, Now");
    g_ButtonLocalNotifScheduled = NewButton("Get No of Notification Scheduled");
    g_ButtonSetBadgeNo = NewButton("Set Application Badge No");
    g_ButtonNotificaltionClear = NewButton("Clear SINGLE Notification");
    g_ButtonNotificaltionClearAll = NewButton("Clear ALL Notifications");
    g_ButtonUnRegisterFromRemoteNotifcations  = NewButton("Unregister from remote notification");



    if (!s3eIOSNotificationsAvailable())
    {
        AppendMessageColour(RED,"Extension Not Available");
        g_ButtonSetNotification->m_Enabled = false;
        g_ButtonSetNotificationByTimeStamp->m_Enabled = false;
        g_ButtonSetNotificationGenerated20Sec->m_Enabled = false;
        g_ButtonSetNotificationGeneratedNow->m_Enabled = false;
        g_ButtonLocalNotifScheduled->m_Enabled = false;
        g_ButtonSetBadgeNo->m_Enabled = false;
        g_ButtonNotificaltionClear->m_Enabled = false;
        g_ButtonNotificaltionClearAll->m_Enabled = false;
        g_ButtonUnRegisterFromRemoteNotifcations->m_Enabled = false;
        return;
    }

    // Register to be notified when recieving any notifications
    s3eIOSNotificationsRegister(S3E_IOSNOTIFICATIONS_LOCAL, LocalPushNotificationRecieved, NULL);
    s3eIOSNotificationsRegister(S3E_IOSNOTIFICATIONS_REMOTE, RemotePushNotificationRecieved, NULL);
    s3eIOSNotificationsRegister(S3E_IOSNOTIFICATIONS_REMOTE_REGISTRATION_FAILED, remoteRegistrationFailed, NULL);
    s3eIOSNotificationsRegister(S3E_IOSNOTIFICATIONS_REMOTE_REGISTRATION_SUCCEEDED, remoteRegistrationSucceeded, NULL);
    s3eIOSNotificationsRegister(S3E_IOSNOTIFICATIONS_REGISTERED_SETTINGS, registeredSettings, NULL);

    s3eIOSNotificationsTypes types = (s3eIOSNotificationsTypes) (S3E_IOSNOTIFICATIONS_TYPE_BADGE | S3E_IOSNOTIFICATIONS_TYPE_ALERT |  S3E_IOSNOTIFICATIONS_TYPE_SOUND);

    s3eIOSNotificationsRegisterAction("marmaladetest", "yes-action", "yes", true, false, false);
    s3eIOSNotificationsRegisterAction("marmaladetest", "no-action", "no", false, false, false);

    s3eIOSNotificationsRegisterApp(types);

    // Check if the app was launched via a notification. This will cause the
    // launching notification to be delivered in the next yield event. It must
    // be called AFTER setting-up the callbacks above, or else the
    // notification will be discarded.
    g_LaunchNotification = s3eIOSNotificationsGetLaunchNotification();
}

void ExampleTerm()
{
    s3eIOSNotificationsUnRegister(S3E_IOSNOTIFICATIONS_LOCAL, LocalPushNotificationRecieved);
    s3eIOSNotificationsUnRegister(S3E_IOSNOTIFICATIONS_REMOTE, RemotePushNotificationRecieved);
    s3eIOSNotificationsUnRegister(S3E_IOSNOTIFICATIONS_REMOTE_REGISTRATION_FAILED, remoteRegistrationFailed);
    s3eIOSNotificationsUnRegister(S3E_IOSNOTIFICATIONS_REMOTE_REGISTRATION_SUCCEEDED, remoteRegistrationSucceeded);
    s3eIOSNotificationsUnRegister(S3E_IOSNOTIFICATIONS_REGISTERED_SETTINGS, registeredSettings);
}

#include "IwDebug.h"

int GetIntFromOSReadString(const char* text, int min, int max)
{
    int result = 0;
    const char* str = s3eOSReadStringUTF8(text, S3E_OSREADSTRING_FLAG_NUMBER);
    if (str)
        result = atoi(str);
    else
        return 0;

    if ((min != INT_MAX && result < min) || (max != INT_MAX && result > max))
        return 0;

    return result;
}

bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();
    if (!pressed)
    {
        return true;
    }
    else if (pressed == g_ButtonUnRegisterFromRemoteNotifcations)
    {
        s3eIOSNotificationsUnRegisterApp();
        g_PushToken = NULL;
    }
    else if (pressed == g_ButtonSetNotification)
    {
        char eventName[MAX_NAME_SIZE] = "";
        char eventMessage[MAX_MESSAGE_SIZE] = "";
        tm lDate;
        lDate.tm_sec = 0;

        // s3eIOSNotificationsEventInfo created to house all items needed to set up a notificatioin
        s3eIOSNotificationsEventInfo toDoItem = {0};

        // Get notifications attributes from user
        // Event Name: Key used to recognise notification
        const char* eventNameReaded = s3eOSReadStringUTF8("Enter Event Name:", 0);
        if (eventNameReaded)
        {
            strcpy(eventName, eventNameReaded);
            toDoItem.m_EventName = eventName;
        }

        // Event Message: Message that will show up when notification is fired
        const char* eventMsgReaded = s3eOSReadStringUTF8("Enter Event Message:", 0);
        if (eventMsgReaded)
        {
            strcpy(eventMessage, eventMsgReaded);
            toDoItem.m_EventMessage = eventMessage;
        }

        // NOTE: TIME MUST BE ENTERED AS A UTC TIME VALUE NOT A LOCAL TIME VALUE

        //Hour: time for event to fire hour (0 - 23)
        lDate.tm_hour = GetIntFromOSReadString("Enter Event Time: Hour (UTC)", 0, 23);

        //Minute: time for event to fire minute (0 - 59)
        lDate.tm_min = GetIntFromOSReadString("Enter Event Time: Minute (UTC)", 0, 59);

        //Day: time for event to fire Day (1 - Dependent on Month)
        lDate.tm_mday = GetIntFromOSReadString("Enter Event Time: Day (UTC)", 1, 31);

        //Month: time for event to fire month (0 - 11)
        lDate.tm_mon = GetIntFromOSReadString("Enter Event Time: Month (UTC)", 0, 11);

        //Year: time for event to fire year (1970+)
        int year = GetIntFromOSReadString("Enter Event Time: Year (UTC)", 1970, INT_MAX);
        lDate.tm_year = (year > 1970 ? year - 1900 : 71);

        lDate.tm_isdst = -1;

        // Calculate date from Epoch
        time_t lTimeEpoch = mktime(&lDate);

        // Set Fire Date using newly calculated epoch
        toDoItem.m_FireDateS = (uint64)(lTimeEpoch);

        //Badge No: // Badge number to be displayed when the event is fired
                    // If 0 the badge is reset
        toDoItem.m_BadgeNo = (uint16)GetIntFromOSReadString("Enter A Badge Number to Display", 0, INT_MAX);

        s3eIOSNotificationsCreate(&toDoItem);
        AppendMessageColour(GREEN, "Local Notification \"%s\" set", toDoItem.m_EventName);
    }
    else if (pressed == g_ButtonSetNotificationByTimeStamp)
    {
        char eventName[MAX_NAME_SIZE] = "";
        char eventMessage[MAX_MESSAGE_SIZE] = "";
        const char* chEventItem = NULL;
        uint64 uiEventNo = 0;

        // s3eIOSNotificationsEventInfo created to house all items needed to set up a notificatioin
        s3eIOSNotificationsEventInfo toDoItem = {0};

        // Get notifications attributes from user
        // Event Name: Key used to recognise notification
        const char* eventNameReaded = s3eOSReadStringUTF8("Enter Event Name:", 0);
        if (eventNameReaded)
        {
            strcpy(eventName, eventNameReaded);
            toDoItem.m_EventName = eventName;
        }

        // Event Message: Message that will show up when notification is fired
        const char* eventMsgreaded = s3eOSReadStringUTF8("Enter Event Message:", 0);
        if (eventMsgreaded)
        {
            strcpy(eventMessage, eventMsgreaded);
            toDoItem.m_EventMessage = eventMessage;
        }

        //FireDate: time for event to fire (as TimeStamp from epoch 1970)
        chEventItem = s3eOSReadStringUTF8("Enter Event Time (seconds from Epoch 1970)", S3E_OSREADSTRING_FLAG_NUMBER);
        if (chEventItem)
        {
            uiEventNo = atoi(chEventItem);
            toDoItem.m_FireDateS = uiEventNo;
        }

        //Badge No: Badge number to be displayed when the event is fired (zero = no badge)
        toDoItem.m_BadgeNo = (uint16)GetIntFromOSReadString("Enter A Badge Number to Display", 0, INT_MAX);

        s3eIOSNotificationsCreate(&toDoItem);
        AppendMessageColour(GREEN, "Local Notification \"%s\" set", toDoItem.m_EventName);
    }
    else if (pressed == g_ButtonSetNotificationGenerated20Sec || pressed == g_ButtonSetNotificationGeneratedNow)
    {
        char eventName[MAX_NAME_SIZE] = "";
        char eventMessage[MAX_MESSAGE_SIZE] = "";
        char eventData[MAX_MESSAGE_SIZE] = "";

        // Pressing this button generates strings by incrementing a counter
        g_QuickMsgCounter++;

        s3eIOSNotificationsEventInfo toDoItem = {0};

        sprintf(eventName, "%s%d", QUICK_NAME, g_QuickMsgCounter);
        toDoItem.m_EventName = eventName;

        sprintf(eventMessage, "%s %d", QUICK_MESSAGE, g_QuickMsgCounter);
        toDoItem.m_EventMessage = eventMessage;

        sprintf(eventData, "%s %d", QUICK_DATA, g_QuickMsgCounter);
        toDoItem.m_EventData = eventData;

        toDoItem.m_BadgeNo = (uint16)g_QuickMsgCounter;

        if (pressed == g_ButtonSetNotificationGenerated20Sec)
            toDoItem.m_FireDateS = (uint64)s3eTimerGetUTC() / 1000 + 20; //miliseconds->seconds!
        else
            toDoItem.m_FireDateS = 0; // zero = "now".

        s3eIOSNotificationsCreate(&toDoItem);
        AppendMessageColour(GREEN, "Local Notification \"%s\" set", toDoItem.m_EventName);
    }
    else if (pressed == g_ButtonNotificaltionClear)
    {
        char eventName[MAX_NAME_SIZE] = "";
        const char* eventNameReaded = s3eOSReadStringUTF8("Enter Event Name:", 0);
        if (eventNameReaded)
            strcpy(eventName, eventNameReaded);

        if (S3E_RESULT_SUCCESS == s3eIOSNotificationsErase(eventName))
            AppendMessageColour(GREEN, "Local Notification \"%s\" cleared", eventName);
        else if (S3E_RESULT_ERROR == s3eIOSNotificationsErase(eventName))
            AppendMessageColour(RED, "Local Notification \"%s\" not found", eventName);
    }
    else if (pressed == g_ButtonNotificaltionClearAll)
    {
        if (S3E_RESULT_SUCCESS == s3eIOSNotificationsErase())
            AppendMessageColour(GREEN, "All Local Notifications cleared");
    }
    else if (pressed == g_ButtonLocalNotifScheduled)
    {
        int8 LocalNotifNo = s3eIOSNotificationsGetScheduleSize();
        AppendMessageColour(GREEN, "%i Local Notifications currently scheduled", LocalNotifNo);
    }
    else if (pressed == g_ButtonSetBadgeNo)
    {
        //Badge No: // Badge number to be displayed. If 0 the badge is reset
        s3eIOSNotificationsSetAppBadge((uint16)GetIntFromOSReadString("Enter a Badge Number", 0, INT_MAX));
    }
    return true;
}

void ExampleRender()
{
    int x = 20;
    int y = GetYBelowButtons();
    int lineHeight = s3eDebugGetInt(S3E_DEBUG_FONT_HEIGHT) + 2;

    y += lineHeight * 2;

    s3eDebugPrintf(x, y, 0, "`x666666App was%s started by a%s notification", g_LaunchNotification == S3E_IOSNOTIFICATIONS_LAUNCH_NONE ? " not" : "", g_LaunchNotification == S3E_IOSNOTIFICATIONS_LAUNCH_LOCAL ? " local" : (g_LaunchNotification == S3E_IOSNOTIFICATIONS_LAUNCH_REMOTE ? " remote" : ""));

    y += lineHeight*2;

    PrintMessages(x, y);
}
