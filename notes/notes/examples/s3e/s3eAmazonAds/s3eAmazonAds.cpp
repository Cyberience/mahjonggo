/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EAmazonAds S3E Amazon Ads Example
 *
 * The following demonstrates use of s3eAmazonAds API to display ads provided
 * by Amazon Mobile Ads API.   The example implements a simple state machine
 * for the requesting, loading, showing and destroying of ads:
 *
 * state      | API usage
 *            | s3eAmazonAdsRegister
 * AD_UNSET   | s3eAmazonAdsPrepareAd / s3eAmazonAdsPrepareLayout
 * AD_INIT    | s3eAmazonAdsLoadAd / s3eAmazonAdsLoadInterstitialAd
 * AD_LOADING | (see callback usage below)
 * AD_LOADED  | s3eAmazonAdsShowAd
 * AD_SHOWING | s3eAmazonAdsDestroyAd
 *            | s3eAmazonAdsTerminate
 *
 * The state transitions are shown for three types of ads:
 *   1. a small top banner ad that is displayed immedaitely after load.
 *   2. a auto-sized bottom banner ad that stretched the width of the display.
 *   3. a full screen interstitial ad with specific targeting options using
 *      s3eAmazonAdsGetAdTargetingOptions / s3eAmazonAdsSetAdTargetingOptions
 *
 * This example also demonstrates how to register for callback events:
 *   S3E_AMAZONADS_CALLBACK_AD_LOADED - Used to change state from to LOADED.
 *   S3E_AMAZONADS_CALLBACK_AD_ACTION - Used when dismissing interstitial ads
 *                                      to change state back to INIT.
 *   S3E_AMAZONADS_CALLBACK_AD_ERROR  - Used to change state back to INIT.
 *
 * @remark g_AppKey is purposely left empty.  Please enter a valid App Key to
 * test this example.
 *
 * @include s3eAmazonAds.cpp
 * @incldue s3eAmazonAdsUtil.h
 */
#include "s3eAmazonAdsUtil.h"

#include "ExamplesMain.h"
#include "IwDebug.h"

#include "s3eAmazonAds.h"

#include <string>


//
// Please add your app key here:
//
static const char* g_AppKey = "";

static CEventLog   s_Log;      // helper class for logging

class CAdManager
{
    class CAdState
    {
        friend CAdManager;

        enum AdStateEnum { AD_UNSET, AD_INIT, AD_LOADING, AD_LOADED, AD_SHOWING };
        static std::string m_Labels[5];

        CAdState() {}
    public:
        static const int MaxStates = 5;

        CAdState(std::string name)
        {
            m_Id = 0;
            m_Name = name;
            m_State = AD_UNSET;
            std::string label = m_Labels[AD_UNSET] + m_Name;
            m_Button = NewButton(label.c_str());
            m_Type = S3E_AMAZONADS_TYPE_UNKNOWN;
        }

        ~CAdState()
        {
            delete m_Button;
        }

        virtual bool do_prepare()
        {
            s_Log.Log(std::string("Preparing request for ") + m_Name);

            s3eResult res = s3eAmazonAdsPrepareAd(&m_Id);
            return (res == S3E_RESULT_SUCCESS);
        }

        virtual bool do_load(bool show)
        {
            s_Log.Log(std::string("Loading ") + m_Name);

            s3eBool toshow = show ? S3E_TRUE : S3E_FALSE;
            s3eResult res = s3eAmazonAdsLoadAd(m_Id, toshow);
            return (res == S3E_RESULT_SUCCESS);
        }

        virtual bool do_show()
        {
            s_Log.Log(std::string("Showing ") + m_Name);

            s3eResult res = s3eAmazonAdsShowAd(m_Id);
            return (res == S3E_RESULT_SUCCESS);
        }

        virtual bool do_destroy()
        {
            s_Log.Log(std::string("Destroying ") + m_Name);

            s3eResult res = s3eAmazonAdsDestroyAd(m_Id);
            m_Id = -1;
            return (res == S3E_RESULT_SUCCESS);
        }

        virtual void changeState(AdStateEnum state)
        {
            m_State = state;
            std::string str;
            str = m_Labels[state] + m_Name;
            SetButtonName(m_Button, str.c_str());
        }

        virtual bool onClick(Button* button)
        {
            if (m_Button == button)
            {
                switch (m_State)
                {
                case AD_UNSET:      // "prepare request"
                    if (do_prepare())
                        changeState(AD_INIT);
                    break;
                case AD_INIT:       // "load ad"
                    if (do_load(true))
                        changeState(AD_LOADING);
                    break;
                case AD_LOADING:    // "loading..." (disabled)
                    // state change from callback   --> AD_LOADED or AD_SHOWING
                    return false;
                case AD_LOADED:     // "show ad"
                    if (do_show())
                        changeState(AD_SHOWING);
                    break;
                case AD_SHOWING:    // "destroy ad"
                    if (do_destroy())
                        changeState(AD_UNSET);
                    break;
                default:
                    return false;
                }
            }
            return true;
        }

        virtual void onRefresh()
        {
        }

        virtual void onLoad() = 0;

        virtual void onAction()
        {
            s_Log.Log(std::string("onAction for ") + m_Name);
        }

        virtual void onError(int error)
        {
            char temp[64];
            sprintf(temp, "%d", error);
            s_Log.Log(std::string("onError for ") + m_Name + "=" + temp);
            changeState(AD_INIT);
        }

    protected:
        s3eAmazonAdsAdType m_Type;
        s3eAmazonAdsId m_Id;
        std::string m_Name;
        AdStateEnum m_State;
        Button* m_Button;
    };

    class CTopBannerAd : public CAdState
    {
    public:
        CTopBannerAd(std::string name) : CAdState(name) {}

        bool do_prepare()
        {
            if (CAdState::do_prepare())
            {
                s3eAmazonAdsPrepareAdLayout(m_Id, S3E_AMAZONADS_POSITION_TOP, S3E_AMAZONADS_SIZE_320x50);
                return true;
            }
            return false;
        }

        void onRefresh()
        {
            if (m_State == AD_SHOWING)
            {
                s_Log.Log(std::string("orientation change: ") + m_Name);
                if (do_load(true))
                    changeState(AD_LOADING);
            }
        }

        void onLoad()
        {
            s_Log.Log(std::string("onLoad for ") + m_Name);
            changeState(AD_SHOWING);
        }
    };

    class CBottomBannerAd : public CAdState
    {
        bool m_skip_show_button;
    public:
        CBottomBannerAd(std::string name) :
            CAdState(name),
            m_skip_show_button(false)
        {
        }

        void onRefresh()
        {
            if (m_State == AD_SHOWING)
            {
                s_Log.Log(std::string("orientation change: ") + m_Name);
                m_skip_show_button = true;
                if (do_load_show())
                    changeState(AD_LOADING);
            }
        }

        bool do_destroy()
        {
            m_skip_show_button = false;
            return CAdState::do_destroy();
        }

        void onLoad()
        {
            s_Log.Log(std::string("onLoad for ") + m_Name);
            if (!m_skip_show_button)
                changeState(AD_LOADED);
            else
                changeState(AD_SHOWING);
        }

        bool do_load_show()
        {
            return CAdState::do_load(true);
        }

        bool do_load(bool show)
        {
            IwTrace(AMAZONADS_EXAMPLE, ("do_load override"));
            return CAdState::do_load(false);
        }

        bool do_prepare()
        {
            if (CAdState::do_prepare())
            {
                s3eAmazonAdsPrepareAdLayout(m_Id, S3E_AMAZONADS_POSITION_BOTTOM, S3E_AMAZONADS_SIZE_AUTO);
                return true;
            }
            return false;
        }
    };

    class CInterstitialAd : public CAdState
    {
    public:
        CInterstitialAd(std::string name) : CAdState(name)
        {
            this->m_Type = S3E_AMAZONADS_TYPE_INTERSTITIAL;
        }

        bool do_prepare()
        {
            bool res = CAdState::do_prepare();

            // example of getting and setting targeting options
            // uncomment below to test:
            /*
            if (!res)
                return false;

            s3eAmazonAdsTargetingOptions* options = s3eAmazonAdsGetAdTargetingOptions(m_Id);
            if (options == NULL)
                s_Log.Log("ERROR: Failed to get targeting options.");

            // DO NOT overwrite and reuse the object return from s3eAmazonAdsGetAdTargetingOptions
            // create a new object to pass to s3eAmazonAdsSetAdTargetingOptions.
            s3eAmazonAdsTargetingOptions* newoptions = new s3eAmazonAdsTargetingOptions();
            newoptions->m_OptionsMask = S3E_AMAZONADS_OPTIONS_FLOORPRICE | S3E_AMAZONADS_OPTIONS_GEOLOCATION | S3E_AMAZONADS_OPTIONS_ADVANCED;
            newoptions->m_FloorPrice = 9991;
            newoptions->m_EnableGeoLocation = true;
            newoptions->m_AdvancedOptions = (char*) "hello\nworld\nadd";
            s3eResult result = s3eAmazonAdsSetAdTargetingOptions(m_Id, newoptions);

            if (result != S3E_RESULT_SUCCESS)
                s_Log.Log("ERROR: Failed to set targeting options.");

            //
            // check that the options have been set by Amazon Ads API
            options = s3eAmazonAdsGetAdTargetingOptions(m_Id);
            if (options == NULL)
                s_Log.Log("ERROR: Failed to get targeting options.");
            else
            {
                IwTrace(AMAZONADS, ("get options: %d %s", options->m_FloorPrice, options->m_AdvancedOptions));
            }
            res = result == S3E_RESULT_SUCCESS;
            */
            return res;
        }

        bool do_load(bool show)
        {
            // example of s3eAmazonAdsInspectAd:
            s3eAmazonAdsAdInfo info;
            s3eAmazonAdsInspectAd(m_Id, &info);
            if (info.m_IsLoading)
                s_Log.Log(std::string("Warning s3eAmazonAdsInspectAd::isLoading is true."));
            s_Log.Log(std::string("Loading ") + m_Name);
            s3eResult res = s3eAmazonAdsLoadInterstitialAd(m_Id);
            return res == S3E_RESULT_SUCCESS;
        }

        virtual void onLoad()
        {
            s_Log.Log(std::string("onLoad for ") + m_Name);
            changeState(AD_LOADED);
        }

        void onAction() // dismissed
        {
            CAdState::onAction();
            changeState(AD_INIT);
        }
    };

public:
    CAdManager()
    {
        // Create three ad state objects to handle button / ad state
        m_Ad[0] = new CTopBannerAd("Top Small Banner Ad");
        m_Ad[1] = new CBottomBannerAd("Bottom Auto Banner Ad");
        m_Ad[2] = new CInterstitialAd("Interstitial Ad");

        // REGISTER for all s3eAmazonAds callbacks:
        s3eAmazonAdsRegister(S3E_AMAZONADS_CALLBACK_AD_LOADED, onAdLoad,  this);
        s3eAmazonAdsRegister(S3E_AMAZONADS_CALLBACK_AD_ACTION, onAdAction, this);
        s3eAmazonAdsRegister(S3E_AMAZONADS_CALLBACK_AD_ERROR,  onAdError,  this);

        s3eSurfaceRegister(S3E_SURFACE_SCREENSIZE, ScreenSizeChangeCallback, this);
    }

    void Update()
    {
        Button* pressed = GetSelectedButton();
        for (int i=0; i<numAds; i++)
            m_Ad[i]->onClick(pressed);
    }

private:
    static const int numAds = 3;
    CAdState* m_Ad[numAds];

public:
    //
    // Callbacks
    //
    static int32 onAdLoad(void* systemData, void* userData)
    {
        s3eAmazonAdsCallbackLoadedData* data = static_cast<s3eAmazonAdsCallbackLoadedData*>(systemData);
        CAdManager* mgr = static_cast<CAdManager*>(userData);

        if (data == NULL || data->m_Properties == NULL|| mgr == NULL)
        {
            s_Log.Log(std::string("onAdLoad error: callback data is NULL."));
            return 1;
        }

        // example of how to access s3eAmazonAdsProperties from s3eAmazonAdsCallbackLoadedData
        char buffer[60];
        sprintf(buffer, "onAdLoad->AdType: %d", data->m_Properties->m_AdType);
        s_Log.Log(std::string(buffer));

        for (int i=0; i<numAds; i++)
            if (mgr->m_Ad[i]->m_Id == data->m_Id)
                mgr->m_Ad[i]->onLoad();

        return 0;
    }

    static int32 onAdAction(void* systemData, void* userData)
    {
        s3eAmazonAdsCallbackActionData* data = static_cast<s3eAmazonAdsCallbackActionData*>(systemData);
        CAdManager* mgr = static_cast<CAdManager*>(userData);

        if (data == NULL || mgr == NULL)
        {
            s_Log.Log(std::string("onAdAction error: callback data is NULL."));
            return 1;
        }

        for (int i=0; i<numAds; i++)
            if (mgr->m_Ad[i]->m_Id == data->m_Id)
                mgr->m_Ad[i]->onAction();

        return 0;
    }

    static int32 onAdError(void* systemData, void* userData)
    {
        s3eAmazonAdsCallbackErrorData* data = static_cast<s3eAmazonAdsCallbackErrorData*>(systemData);
        CAdManager* mgr = static_cast<CAdManager*>(userData);

        if (data == NULL || mgr == NULL)
        {
            s_Log.Log(std::string("onAdError error: callback data is NULL."));
            return 1;
        }

        for (int i=0; i<numAds; i++)
            if (mgr->m_Ad[i]->m_Id == data->m_Id)
                mgr->m_Ad[i]->onError(data->m_Error);

        return 0;
    }

    static int32 ScreenSizeChangeCallback(void* systemData, void* userData)
    {
        s_Log.Log(std::string("screensize"));

        CAdManager* mgr = static_cast<CAdManager*>(userData);

        if (mgr == NULL)
        {
            s_Log.Log(std::string("callback data is NULL."));
            return 1;
        }

        for (int i=0; i<numAds; i++)
        {
            mgr->m_Ad[i]->onRefresh();
        }
        return 0;
    }

};

std::string CAdManager::CAdState::m_Labels[5] = { "Prepare ", "Load ", "Loading ", "Show ", "Destroy " };
    // note these labels are the strings to show on the button during the associated AdStateEnum,
    // but are not the state names themselves.

static CAdManager* s_Manager;  // helper class for managing button / ad state

void ExampleInit()
{
    // Check if the the s3eAmazonAds extension is available
    if (s3eAmazonAdsAvailable())
    {
        // INITIALIZE the s3eAmazonAds extension using the App Key, testing enabled, logging enabled
        if (s3eAmazonAdsInit(g_AppKey, S3E_TRUE, S3E_TRUE) == S3E_RESULT_SUCCESS)
        {
            s_Log.Log("Successfully initialized extension.");

            // Add two empty buttons to move real buttons down to prevent them from being obstructed by top banner.
            (void) NewButton("");
            (void) NewButton("");

            s_Manager = new CAdManager();
        }
        else
            s_Log.Log(s3eAmazonAdsGetErrorString());
    }
    else
        s_Log.Log("Amazon Mobile Ads API is not available.");
}

void ExampleTerm()
{
    if (s_Manager != NULL)
    {
        s3eResult res = s3eAmazonAdsTerminate();
        if (res == S3E_RESULT_SUCCESS)
        {
            s_Log.Log("termination failed.");
        }
        s_Log.Log("terminating...");
        delete s_Manager;
    }
}

bool ExampleUpdate()
{
    if (s_Manager != NULL)
        s_Manager->Update();

    return true;
}

void ExampleRender()
{
    // Display event log
    int y = GetYBelowButtons();
    s_Log.Display(y);
}
