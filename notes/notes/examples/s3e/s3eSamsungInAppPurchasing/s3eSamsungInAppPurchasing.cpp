/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

//-----------------------------------------------------------------------------
// SamsungInAppPurchasingExample
//-----------------------------------------------------------------------------

/**
 * @page ExampleS3ESamsungInAppPurchasing S3E Samsung In App Purchasing Example
 * The following example demonstrates a basic Samsung in-app purchasing application.
 *
 * This Application demonstrates usage of the Samsung In App Purchasing Marmalade
 * extension on the Android platform. The application is intended to be functional
 * and demonstrate how to use the extension code without the distraction of complex UI.
 * This example illustrates the use of the In App Billing functionality on the
 * Android platform, allowing items, products or services to be bought via
 * Samsung. It connects to requests information for a product,allows the user to
 * purchase the product and then displays the receipt for the transaction.
 *
 * For more information on the general topic of Samsung In App Billing see
 * http://developer.samsung.com/in-app-purchase
 *
 * Note that the API only handles purchasing of the product, not delivery of the
 * actual service/data that was paid for. Typically, an application will
 * require its own external server to perform these extra tasks.
 * Providing data via a server is beyond the scope of this example.
 *
 * See S3E Samsung In App Purchasing overview in the Marmalade API Reference for
 * more information.
 *
 * The main functions demonstrated in this example are:
 * - s3eSamsungInAppPurchasingAvailable()
 * - s3eSamsungInAppPurchasingRegister()
 * - s3eSamsungInAppPurchasingInit()
 * - s3eSamsungInAppPurchasingSetDeveloperState()
 * - s3eSamsungInAppPurchasingPurchaseItem()
 * - s3eSamsungInAppPurchasingGetItemData
 * - s3eSamsungInAppPurchasingEnumerateEntitlements()
 *
 * Note that the extension is explicitly initialized and terminated unlike
 * some other extensions.
 *
 * @include SamsungInAppPurchasingExample.cpp
 *
 */

#include "IwNUI.h"
#include "s3eSamsungInAppPurchasing.h"
#include <time.h>

using namespace IwNUI;


///////////////////////////////////////////////////////////////////////////////

int32 ItemDataCallback(void *systemData, void *userData);
int32 PurchaseInitializedResponseCallback(void *systemData, void *userData);
int32 PurchaseFinishedResponseCallback(void *systemData, void *userData);
int32 RestoreEntitlementCallback(void *systemData, void *userData);

///////////////////////////////////////////////////////////////////////////////

bool OnSkuClick(void* data, CButton* button);
bool OnItemInfoClick(void* data, CButton* button);
bool OnRestoreItemsClick(void* data, CButton* button);


///////////////////////////////////////////////////////////////////////////////

static int control_yPos = 4;
CViewPtr view;

///////////////////////////////////////////////////////////////////////////////

struct CatalogEntry
{
    enum State
    {
        UNKNOWN,
        AVAILABLE,
        PURCHASED,
        RESTORED
    };

    const char*     m_sku;
    const char*     m_buttonCaption;
    const char*     m_text;
    bool (*m_ClickHandler)(void*, CButton*);
    State               m_state;
    CButtonPtr      m_pButton;
    CLabelPtr       m_pText;
};

///////////////////////////////////////////////////////////////////////////////

void CreateCatalogControl(CView* pView, CatalogEntry *pSku, int& yPos)
{
    CAttributes buttonAttrs = CAttributes()
        .Set("x1",              "2%")
        .Set("width",           "40%")
        .Set("height",      "8%");

    CAttributes textFieldAttrs = CAttributes()
        .Set("name",        "sourceTextField")
        .Set("x1",          "45%")
        .Set("width",       "50%")
        .Set("height",      "8%")
        .Set("font",        "serif")
        .Set("fontSize",    8)
        .Set("fontColour",  CColour(0xFF000000))
        .Set("backgroundColour", CColour(0xFFFFFFFF))
        .Set("fontAlignH",  "left");

    char szYpos[8];

    sprintf(szYpos, "%d%%", yPos);

    pSku->m_pButton = CreateButton(buttonAttrs);
    pSku->m_pButton->SetAttribute("name",      pSku->m_buttonCaption);
    pSku->m_pButton->SetAttribute("caption", pSku->m_buttonCaption);
    pSku->m_pButton->SetAttribute("y1",        szYpos);
    //pSku->m_pButton->SetAttribute("enabled", 0);

    if (pSku->m_ClickHandler != NULL)
    {
        pSku->m_pButton->SetEventHandler("click", (void*)NULL, pSku->m_ClickHandler);
    }

    pSku->m_pText = CreateLabel(textFieldAttrs);
    pSku->m_pText->SetAttribute("caption",  pSku->m_text);
    pSku->m_pText->SetAttribute("y1",      szYpos);

    pSku->m_state = CatalogEntry::AVAILABLE;

    pView->AddChild(pSku->m_pButton);
    pView->AddChild(pSku->m_pText);

    yPos += 12;
}

///////////////////////////////////////////////////////////////////////////////


class Catalog
{
public:
    Catalog(CView* pView);
    ~Catalog();

    CatalogEntry* Find(const char *szSku);
    CatalogEntry* Find(CButton* pButton);
    void Add(CatalogEntry* pEntry);
    CatalogEntry* Add(s3eSamsungInAppPurchasingItemData* pItemData);

protected:

    CView* m_pView;
    std::vector<CatalogEntry*> m_catalog;

private:

    Catalog();
};

///////////////////////////////////////////////////////////////////////////////

Catalog::Catalog(CView* pView)
{
    m_pView = pView;
}

Catalog::~Catalog()
{
    // TODO delete the catalog entries
}

CatalogEntry* Catalog::Find(const char *szSku)
{
    std::vector<CatalogEntry*>::iterator it = m_catalog.begin();
    std::vector<CatalogEntry*>::iterator end = m_catalog.end();

    CatalogEntry* pEntry  = NULL;

    while(it != end)
    {
        pEntry = *it;

        if(strcmp(pEntry->m_sku, szSku) == 0)
        {
            return pEntry;
        }

        ++ it;
    }

    return NULL;
}

CatalogEntry* Catalog::Find(CButton* pButton)
{
    std::vector<CatalogEntry*>::iterator it = m_catalog.begin();
    std::vector<CatalogEntry*>::iterator end = m_catalog.end();

    CatalogEntry* pEntry  = NULL;

    while(it != end)
    {
        pEntry = *it;

        if(pEntry->m_pButton.get() == pButton)
        {
            return pEntry;
        }

        ++ it;
    }

    return NULL;
}

void Catalog::Add(CatalogEntry* pEntry)
{
    if (Find(pEntry->m_sku) == NULL)
    {
        m_catalog.push_back(pEntry);

        CreateCatalogControl(m_pView, pEntry, control_yPos);
    }
}

CatalogEntry* Catalog::Add(s3eSamsungInAppPurchasingItemData* pItemData)
{
    CatalogEntry *pEntry = Find(pItemData->m_itemId);


    if (pEntry == NULL)
    {
        pEntry = new CatalogEntry();

        if (pEntry != NULL)
        {
            pEntry->m_sku               = strdup(pItemData->m_itemId);
            pEntry->m_buttonCaption = strdup(pItemData->m_itemName);
            pEntry->m_text              = strdup(pItemData->m_itemDescription);
            pEntry->m_ClickHandler  = &OnSkuClick;
            pEntry->m_state         = CatalogEntry::AVAILABLE;

            m_catalog.push_back(pEntry);

            CreateCatalogControl(m_pView, pEntry, control_yPos);
        }
    }

    return pEntry;
}


///////////////////////////////////////////////////////////////////////////////

CatalogEntry catalogButtons[] =
{
    { "Update Catalog",         "Update Catalog",           "Query the Samsung server for item SKU info", &OnItemInfoClick, CatalogEntry::AVAILABLE },
    { "Restore Entitlements",   "Restore Entitlements", "Query the Samsung server for previously purchased items", &OnRestoreItemsClick, CatalogEntry::AVAILABLE },
    { NULL },
};

///////////////////////////////////////////////////////////////////////////////

Catalog* g_pCatalog = NULL;

///////////////////////////////////////////////////////////////////////////////

int main()
{
    if (s3eSamsungInAppPurchasingAvailable())
    {
        s3eSamsungInAppPurchasingRegister(S3E_SAMSUNGINAPPPURCHASING_ITEM_DATA_RESPONSE,            &ItemDataCallback,                          NULL);
        s3eSamsungInAppPurchasingRegister(S3E_SAMSUNGINAPPPURCHASING_PURCHASE_FINISHED_RESPONSE,    &PurchaseFinishedResponseCallback,          NULL);
        s3eSamsungInAppPurchasingRegister(S3E_SAMSUNGINAPPPURCHASING_ENTITLEMENT_RESPONSE,          &RestoreEntitlementCallback,                NULL);

        // Use the Samsung sample application item group and set developer state
        s3eSamsungInAppPurchasingInit("100000102757", S3E_FALSE);
        s3eSamsungInAppPurchasingSetDeveloperState(S3E_SAMSUNGINAPPPURCHASING_DEVELOPER_STATE_ACTIVE);
    }


    CAppPtr app = CreateApp();
    CWindowPtr window = CreateWindow();

    app->AddWindow(window);

     //CViewPtr view = CreateView("canvas");
    view = CreateView("canvas");

    g_pCatalog = new Catalog(view.get());

    control_yPos = 4;

    CreateCatalogControl(view.get(), &catalogButtons[0], control_yPos);
    CreateCatalogControl(view.get(), &catalogButtons[1], control_yPos);

    //xx // Disable controls till the purchasing service is available
    //catalog[0].m_pButton->SetAttribute("enabled", 0);
    //catalog[1].m_pButton->SetAttribute("enabled", 0);

    control_yPos += 8;

    window->SetChild(view);

    app->ShowWindow(window);

    if (s3eSamsungInAppPurchasingAvailable())
    {
        // Enable the catalog button
        catalogButtons[0].m_pButton->SetAttribute("enabled", 1);

        // Enable the restore entitlements button
        catalogButtons[1].m_pButton->SetAttribute("enabled", 1);
    }


    //app->Run();

    while (app->Update())
    {
        // Sleep for 0ms to allow the OS to process events etc.
        s3eDeviceYield(5);
    }

    return 0;
}

///////////////////////////////////////////////////////////////////////////////


bool OnSkuClick(void* data, CButton* button)
{
    if (s3eSamsungInAppPurchasingAvailable())
    {
        // Find the catalog entry associated with this button
        CatalogEntry* pEntry = g_pCatalog->Find(button);

        if (pEntry != NULL)
        {
            s3eSamsungInAppPurchasingPurchaseItem(pEntry->m_sku);
        }
    }

    return true;
}

bool OnItemInfoClick(void* data, CButton* button)
{
    if (s3eSamsungInAppPurchasingAvailable())
    {
        // TODO All items
        s3eSamsungInAppPurchasingGetItemData(1, 15);
    }
    return true;
}


bool OnRestoreItemsClick(void* data, CButton* button)
{
    if (s3eSamsungInAppPurchasingAvailable())
    {
        // TODO All items
        s3eSamsungInAppPurchasingEnumerateEntitlements(1, 15);
    }

    return true;
}


///////////////////////////////////////////////////////////////////////////////

int32 ItemDataCallback(void *systemData, void *userData)
{
    s3eDebugOutputString("SamsungInAppPurchasing: ItemDataCallback");

    if (systemData != NULL)
    {
        s3eSamsungInAppPurchasingItemData* pItemData = (s3eSamsungInAppPurchasingItemData*) systemData;

        if (pItemData  && (pItemData->m_status == S3E_SAMSUNGINAPPPURCHASING_STATUS_SUCCESSFUL))
        {
            if (g_pCatalog)
            {
                g_pCatalog->Add(pItemData);
            }
        }
    }
    else
    {
        // End of the transaction item data list
    }

    return S3E_RESULT_SUCCESS;
}

///////////////////////////////////////////////////////////////////////////////

int32 PurchaseInitializedResponseCallback(void *systemData, void *userData)
{
    s3eDebugOutputString("SamsungInAppPurchasing: PurchaseResponseCallback");

    return S3E_RESULT_SUCCESS;
}


int32 PurchaseFinishedResponseCallback(void *systemData, void *userData)
{
    s3eDebugOutputString("SamsungInAppPurchasing: PurchaseResponseCallback");

    s3eSamsungInAppPurchasingItemData* pItemData = (s3eSamsungInAppPurchasingItemData*) systemData;

    if (pItemData  && (pItemData->m_status == S3E_SAMSUNGINAPPPURCHASING_STATUS_SUCCESSFUL))
    {
        if (g_pCatalog)
        {
            char szBuff[128];

            // Find the catalog entry associated with the item
            CatalogEntry* pEntry = g_pCatalog->Find(pItemData->m_itemId);

            if (pEntry != NULL)
            {

                // De-activate the button
                pEntry->m_pButton->SetAttribute("enabled", 0);

                sprintf(szBuff, "Purchased on: %s    PurchaseId: %s", pItemData->m_purchaseDate, pItemData->m_paymentId);

                // Reset the caption with useful info
                pEntry->m_pText->SetAttribute("caption", szBuff);

            }
        }
    }

    return S3E_RESULT_SUCCESS;
}


///////////////////////////////////////////////////////////////////////////////

int32 RestoreEntitlementCallback(void *systemData, void *userData)
{
    s3eDebugOutputString("SamsungInAppPurchasing: RestoreEntitlementCallback");

    if (systemData != NULL)
    {
        s3eSamsungInAppPurchasingItemData* pItemData = (s3eSamsungInAppPurchasingItemData*) systemData;

        if (pItemData && (pItemData->m_status == S3E_SAMSUNGINAPPPURCHASING_STATUS_SUCCESSFUL))
        {
            if (g_pCatalog)
            {
                char szBuff[256];

                // Find the catalog entry associated with the item
                CatalogEntry* pEntry = g_pCatalog->Find(pItemData->m_itemId);

                if (pEntry == NULL)
                {
                    // Not in catalog so add it as a new entry
                    pEntry = g_pCatalog->Add(pItemData);
                }

                if (pEntry != NULL)
                {
                    // De-activate the button
                    pEntry->m_pButton->SetAttribute("enabled", 0);

                    sprintf(szBuff, "Entitlement purchased  on: %s    PurchaseId: %s", pItemData->m_purchaseDate, pItemData->m_paymentId);

                    // Reset the caption with useful info
                    pEntry->m_pText->SetAttribute("caption", szBuff);
                }
            }
        }
    }
    else
    {
        // End of the transaction entitlement list
    }

    return S3E_RESULT_SUCCESS;
}

///////////////////////////////////////////////////////////////////////////////
