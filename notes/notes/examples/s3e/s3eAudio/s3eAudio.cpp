/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EAudio S3E Audio Example
 *
 * The following example plays a music file using S3E's audio functionality.
 *
 * The main functions used to achieve this are:
 * - s3eDebugPrint()
 * - s3eKeyboardGetState()
 * - s3eAudioPlay()
 * - s3eAudioPause()
 * - s3eAudioResume()
 * - s3eAudioStop()
 * - s3eAudioPlayFromBuffer()
 * - s3eFileOpen()
 * - s3eFileRead()
 * - s3eFileGetSize()
 *
 * The application waits for buttons to be pressed before using any of the
 * Audio functionality. Detection of button presses are handled using the
 * generic code in ExamplesMain.cpp
 *
 * The state of playback is output to screen using the s3eDebugPrint()
 * function.
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eAudioImage.png
 *
 * MKB File:
 * @include s3eAudio.mkb
 *
 * ICF File:
 * @include data/app.icf
 *
 * Source:
 * @include s3eAudio.cpp
 */

#include "ExamplesMain.h"

enum PlayerState
{
    STATE_PLAYING,
    STATE_PAUSED,
    STATE_STOPPED
};

static PlayerState g_Status = STATE_STOPPED;        //player status information
static void*   g_AudioData;
static int     g_AudioSize;
static Button* g_ButtonPlay = 0;
static Button* g_ButtonPlayBuf = 0;
static Button* g_ButtonPause = 0;
static Button* g_ButtonVolUp = 0;
static Button* g_ButtonVolDown = 0;
static Button* g_ButtonResume = 0;
static Button* g_ButtonStop = 0;
static s3eBool g_MP3Supported = S3E_FALSE;

int32 AudioStopCallback(void* systemData, void* userData)
{
    g_Status = STATE_STOPPED;
    return 0;
}

void ExampleInit()
{
    // Allocating buffer for audio data
    s3eFile * FileHandle = s3eFileOpen("test.mp3", "rb");
    g_AudioSize = s3eFileGetSize(FileHandle);
    g_AudioData = s3eMallocBase(g_AudioSize);

    // Reading in audio data
    s3eFileRead(g_AudioData, g_AudioSize, 1, FileHandle);

    s3eFileClose(FileHandle);

    g_ButtonPlay    = NewButton("Play");
    g_ButtonPlayBuf = NewButton("Play from buffer");
    g_ButtonPause   = NewButton("Pause");
    g_ButtonResume  = NewButton("Resume");
    g_ButtonStop    = NewButton("Stop");
    g_ButtonVolUp   = NewButton("Volume Up");
    g_ButtonVolDown = NewButton("Volume Down");

    g_MP3Supported = s3eAudioIsCodecSupported(S3E_AUDIO_CODEC_MP3);
    s3eAudioRegister(S3E_AUDIO_STOP, AudioStopCallback, NULL);
}

/*
 * The following function frees memory that was alocated during the example.
 */
void ExampleTerm()
{
    s3eAudioStop();
    s3eFree(g_AudioData);
}

/*
 * If a button has been pressed then start/pause/resume/stop playback.
 */
bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    if (!g_MP3Supported)
        return true;

    if (pressed == g_ButtonPlay && (g_Status == STATE_STOPPED))
    {
        if (s3eAudioPlay("test.mp3") == S3E_RESULT_SUCCESS)
            g_Status = STATE_PLAYING;
    }

    if (pressed == g_ButtonPlayBuf && g_Status == STATE_STOPPED)
    {
        if (s3eAudioPlayFromBuffer(g_AudioData, g_AudioSize, 0) == S3E_RESULT_SUCCESS)
            g_Status = STATE_PLAYING;
    }

    if (pressed == g_ButtonPause && (g_Status == STATE_PLAYING))
    {
        if (s3eAudioPause() == S3E_RESULT_SUCCESS)
            g_Status = STATE_PAUSED;
    }

    if (pressed == g_ButtonVolUp)
    {
        int vol = s3eAudioGetInt(S3E_AUDIO_VOLUME);
        s3eAudioSetInt(S3E_AUDIO_VOLUME, vol+10);
    }

    if (pressed == g_ButtonVolDown)
    {
        int vol = s3eAudioGetInt(S3E_AUDIO_VOLUME);
        s3eAudioSetInt(S3E_AUDIO_VOLUME, vol-10);
    }

    if (pressed == g_ButtonResume && (g_Status == STATE_PAUSED))
    {
        if (s3eAudioResume() == S3E_RESULT_SUCCESS)
            g_Status = STATE_PLAYING;
    }

    if (pressed == g_ButtonStop)
    {
        s3eAudioStop();
        g_Status = STATE_STOPPED;
    }

    return true;
}

/*
 * The following function outputs a set of strings. A switch is used to
 * output a string indicating the status. Each string is output using the
 * s3eDebugPrint() function.
 */
void ExampleRender()
{
    const int textHeight = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);

    g_ButtonPlay->m_Enabled = (g_Status == STATE_STOPPED);
    g_ButtonPlayBuf->m_Enabled = (g_Status == STATE_STOPPED);
    g_ButtonPause->m_Enabled = (g_Status == STATE_PLAYING);
    g_ButtonResume->m_Enabled = (g_Status == STATE_PAUSED);
    g_ButtonStop->m_Enabled = (g_Status == STATE_PLAYING);

    int ypos = GetYBelowButtons() + textHeight * 2;

    if (!g_MP3Supported)
    {
        g_ButtonPlay->m_Enabled = false;
        s3eDebugPrint(10, ypos, "`x666666Device does not support mp3 playback", 0);
        return;
    }

    // Display status
    switch (g_Status)
    {
        case STATE_PLAYING:
            s3eDebugPrint(10, ypos, "`x666666Music Playing", 0);
            break;
        case STATE_PAUSED:
            s3eDebugPrint(10, ypos, "`x666666Music Paused", 0);
            break;
        case STATE_STOPPED:
            s3eDebugPrint(10, ypos, "`x666666Music Stopped", 0);
            break;
    }

    ypos += 2 * textHeight;
    int32 pos = s3eAudioGetInt(S3E_AUDIO_POSITION);
    s3eDebugPrintf(10, ypos, 0, "`x666666Position: %d.%d", pos / 1000, pos % 1000);
    ypos += 2 * textHeight;
    s3eDebugPrintf(10, ypos, 0, "`x666666Volume: %d", s3eAudioGetInt(S3E_AUDIO_VOLUME));
}
