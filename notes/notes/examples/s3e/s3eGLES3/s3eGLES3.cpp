/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EGLES3 S3E OpenGL ES 3.0 Example
 * The following example demonstrates OpenGL ES 3.0.
 *
 * The core of this example is based on an example from the maemo
 * project and was updated by the guys at webos-internals:
 * http://www.webos-internals.org/wiki/OpenGL_Triangle_of_Doom
 *
 * @include s3eGLES3.cpp
 */

#include <stdlib.h>
#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <GLES3/gl3.h>
#include <EGL/egl.h>

#include "s3e.h"

static EGLSurface g_EGLSurface = NULL;
static EGLDisplay g_EGLDisplay = NULL;
static EGLContext g_EGLContext = NULL;

#define MAX_CONFIG 256

#define BINARY_FORMAT 0x93B0

bool g_glIsInitialized = false;
static char* g_vertexSrc = NULL;
static char* g_fragmentSrc = NULL;

static int eglInit()
{
    EGLint major;
    EGLint minor;
    EGLint numFound = 0;
    EGLConfig configList[MAX_CONFIG];

    g_EGLDisplay = eglGetDisplay(EGL_DEFAULT_DISPLAY);
    if (!g_EGLDisplay)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "eglGetDisplay failed");
        return 1;
    }

    EGLBoolean res = eglInitialize(g_EGLDisplay, &major, &minor);
    if (!res)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "eglInitialize failed");
        return 1;
    }

    eglGetConfigs(g_EGLDisplay, configList, MAX_CONFIG, &numFound);
    if (!numFound)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "eglGetConfigs failed to find any configs");
        return 1;
    }

    int requiredRedDepth = 8;
    bool requiredFound = s3eConfigGetInt("GL", "EGL_RED_SIZE", &requiredRedDepth) == S3E_RESULT_SUCCESS;

    int config = -1;
    int actualRedDepth = 0;
    printf("found %d configs\n", numFound);
    for (int i = 0; i < numFound; i++)
    {
        EGLint renderable = 0;
        EGLint surfacetype = 0;
        eglGetConfigAttrib(g_EGLDisplay, configList[i], EGL_RENDERABLE_TYPE, &renderable);
        eglGetConfigAttrib(g_EGLDisplay, configList[i], EGL_RED_SIZE, &actualRedDepth);
        eglGetConfigAttrib(g_EGLDisplay, configList[i], EGL_SURFACE_TYPE, &surfacetype);

        if ((!requiredFound || (actualRedDepth == requiredRedDepth)) && (renderable & EGL_OPENGL_ES2_BIT) && (surfacetype & EGL_WINDOW_BIT))
        {
            config = i;
            break;
        }
    }

    if (config == -1)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "No GLES3 configs reported.  Trying random config");
        config = 0;
    }

    int version = s3eGLGetInt(S3E_GL_VERSION)>>8;
    printf("requesting GL version: %d\n", version);

    int configid;
    eglGetConfigAttrib(g_EGLDisplay, configList[config], EGL_CONFIG_ID, &configid);
    printf("choosing config with EGL ID: %d\n", configid);

    EGLint attribs[] = { EGL_CONTEXT_CLIENT_VERSION, version, EGL_NONE, };
    g_EGLContext = eglCreateContext(g_EGLDisplay, configList[config], NULL, attribs);
    if (!g_EGLContext)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "eglCreateContext failed");
        return 1;
    }

    printf("S3E_GL_VERSION: %d\n", s3eGLGetInt(S3E_GL_VERSION)>>8);

    version = s3eGLGetInt(S3E_GL_VERSION)>>8;
    if (version != 3)
    {
        printf("reported GL version: %d", version);
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "This example requires GLES v3.x");
        return 1;
    }

    void* nativeWindow = s3eGLGetNativeWindow();
    g_EGLSurface = eglCreateWindowSurface(g_EGLDisplay, configList[config], nativeWindow, NULL);
    eglMakeCurrent(g_EGLDisplay, g_EGLSurface, g_EGLSurface, g_EGLContext);
    g_glIsInitialized = true;
    return 0;
}

static void eglTerm()
{
    if (g_EGLDisplay)
    {
        eglMakeCurrent(g_EGLDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
        eglDestroySurface(g_EGLDisplay, g_EGLSurface);
        eglDestroyContext(g_EGLDisplay, g_EGLContext);
    }
    eglTerminate(g_EGLDisplay);
    g_EGLDisplay = 0;
    g_glIsInitialized = false;
}

void printShaderInfoLog(GLuint shader)
{
    GLint length;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
    if (length > 1)
    {
        char* buffer = (char*)s3eMalloc( sizeof(char) * length ) ;
        glGetShaderInfoLog(shader, length, NULL, buffer);
        printf("shader error: %s", buffer);
        s3eFree(buffer);
        GLint success;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
        if (success != GL_TRUE) {
            exit(1);
        }
    }
}

size_t readFile(const char* filename, char** buffer, bool binary)
{
    FILE* fp = fopen(filename, binary ? "rb" : "r");
    if (fp == NULL) return 0;

    fseek(fp, 0, SEEK_END);
    size_t len = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    (*buffer) = new char[len + 1];

    len = fread(*buffer, 1, len, fp);

    fclose(fp);

    (*buffer)[len] = 0;
    return len;
}

GLuint createShader(GLenum type, const char* source)
{
    char* buffer = NULL;
    int len = 0;
    char** tmp;
    if (type == GL_VERTEX_SHADER)
        tmp = &g_vertexSrc;
    else
        tmp = &g_fragmentSrc;

    if (*tmp == NULL)
    {
        len = (int) readFile(source, tmp, false);
    }

    buffer = *tmp;
    len = (int) strlen(buffer);

    if (len == 0)
    {
        s3eDebugErrorPrintf("failed to load shader: %s", source);
        return 1;
    }

    GLuint shader = glCreateShader(type);

    glShaderSource(shader, 1, (const GLchar**)&buffer, &len);
    glCompileShader(shader);
    printShaderInfoLog(shader);

    return shader;
}

int phaseLocation;
GLuint valueBlockIndex = -1;
GLuint valueBlockHandle[] = {0, 0};
int currBlock = 0;
int timer = 40;

int compileShaders()
{
    GLuint shaderProgram = glCreateProgram();
    GLuint vertexShader = createShader(GL_VERTEX_SHADER, "shaders/doom.vshader");
    GLuint fragmentShader = createShader(GL_FRAGMENT_SHADER, "shaders/doom.pshader");

    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);

    glLinkProgram(shaderProgram);

    int linked = 0;
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &linked);

    if (!linked)
    {
        GLint infoLen = 0;
        glGetProgramiv(shaderProgram, GL_INFO_LOG_LENGTH, &infoLen);

        char* infoLog = NULL;
        if (infoLen > 1)
        {
            infoLog = (char*) s3eMalloc(sizeof(char) * infoLen);
            glGetProgramInfoLog(shaderProgram, infoLen, NULL, infoLog );
        }

        s3eDebugErrorPrintf("Failed to link program: %s", infoLog);

        s3eFree(infoLog);

        glDeleteProgram(shaderProgram);
        return 1;
    }

    glUseProgram(shaderProgram);

    phaseLocation = glGetUniformLocation(shaderProgram, "phase");

    if (phaseLocation < 0)
    {
        s3eDebugErrorPrintf("Unable to get uniform location");
        glDeleteProgram(shaderProgram);
        return 1;
    }

    GLint valueBlockSize = 0;
    valueBlockIndex = glGetUniformBlockIndex(shaderProgram, "ValueBlock");
    glGetActiveUniformBlockiv(shaderProgram, valueBlockIndex, GL_UNIFORM_BLOCK_DATA_SIZE, &valueBlockSize);

    GLubyte* valueBlock = (GLubyte*)malloc(valueBlockSize*2);
    memset(valueBlock, 0, valueBlockSize*2);

    const GLchar* names[] = {"shift"};
    GLuint valueBlockIndices[] = {0xffffffff};

    glGetUniformIndices(shaderProgram, 1, names, valueBlockIndices);

    if (valueBlockIndices[0] == 0xffffffff)
    {
        s3eDebugErrorPrintf("Unable to get uniform location");
        glDeleteProgram(shaderProgram);
        return 1;
    }

    glGenBuffers(2, valueBlockHandle);

    float* f1 = (float*)valueBlock;
    float* f2 = (float*)(valueBlock + valueBlockSize);

    f1[0] = 1.0f;
    f2[0] = 1.5f;

    glBindBuffer(GL_UNIFORM_BUFFER, valueBlockHandle[0]);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(valueBlockSize), valueBlock, GL_STATIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, valueBlockHandle[1]);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(valueBlockSize), valueBlock + sizeof(valueBlockSize), GL_STATIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

    delete valueBlock;

    glBindBufferBase(GL_UNIFORM_BUFFER, valueBlockIndex, valueBlockHandle[0]);

    return 0;
}

const float vertexArray[] = {
    0, -1, 0, 1,
    1, 1, 0, 1,
    -1, 1, 0, 1
};

void render()
{
    static float offset = 0;
    int w = s3eSurfaceGetInt(S3E_SURFACE_WIDTH);
    int h = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT);
    glViewport(0, 0, w, h) ;
    glClearColor(0, 1, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT);

    glUniform1f(phaseLocation, offset);
    if (timer <= 0)
    {
        timer = 100;
        currBlock++;
        if (currBlock >= 2)
            currBlock = 0;

        glBindBufferBase(GL_UNIFORM_BUFFER, valueBlockIndex, valueBlockHandle[currBlock]);
    }

    glVertexAttribPointer(0, 4, GL_FLOAT, false, 0, vertexArray);
    glEnableVertexAttribArray(0);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 3);

    eglSwapBuffers(g_EGLDisplay, g_EGLSurface);

    offset = fmodf(offset + 0.2f, 2*3.141f);
    timer--;
}

int main()
{
    if (eglInit())
       return 1;

    printf("Screen BPP: %d\n", s3eSurfaceGetInt(S3E_SURFACE_PIXEL_TYPE) & S3E_SURFACE_PIXEL_SIZE_MASK);
    printf("\n");
    printf("Vendor     : %s\n", (const char*)glGetString( GL_VENDOR ) );
    printf("Renderer   : %s\n", (const char*)glGetString( GL_RENDERER ) );
    printf("Version    : %s\n", (const char*)glGetString( GL_VERSION ) );
    printf("Extensions : %s\n", (const char*)glGetString( GL_EXTENSIONS ) );
    printf("\n");

    if (compileShaders())
        return 1;

    // some platforms require the egl context to be destroyed and restored arbitrarily
    if (s3eGLGetInt(S3E_GL_MUST_SUSPEND))
    {
        // when a suspend is requested, terminate EGL
        s3eGLRegister(S3E_GL_SUSPEND, (s3eCallback)eglTerm, NULL);

        // when a resume is requested, re-initialise EGL...
        s3eGLRegister(S3E_GL_RESUME, (s3eCallback)eglInit, NULL);
        // ...and restore the shaders
        s3eGLRegister(S3E_GL_RESUME, (s3eCallback)compileShaders, NULL);
    }

    bool quit = false;

    int numFrames = 0;

    while (!quit) {
                s3eKeyboardUpdate();
                s3eDeviceYield(0);
                if (s3eDeviceCheckQuitRequest())
                    quit = 1;
                if (s3eKeyboardGetState(s3eKeyEsc) & S3E_KEY_STATE_PRESSED)
                    quit = 1;
                // Only try and render if gl is initialized.  It may get
                // terminated due to a S3E_GL_SUSPEND callback.
                if (g_glIsInitialized)
                    render();
                numFrames++;
    }
    glDeleteBuffers(2, valueBlockHandle);

    eglTerm();

    delete[] g_fragmentSrc;
    delete[] g_vertexSrc;

    return 0;
}
