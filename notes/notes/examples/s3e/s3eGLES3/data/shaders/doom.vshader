#version 300 es

in highp vec4 position;
out mediump vec2 pos;

void main()
{
    gl_Position = position;
    pos = position.xy;
}