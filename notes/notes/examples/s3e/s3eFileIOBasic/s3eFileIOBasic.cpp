/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleS3EFileIOBasic S3E File IO Basic Example
 *
 * This application opens an existing file and reads it in.
 *
 * The main functions used to achieve this are:
 *  - s3eFileOpen()
 *  - s3eFileRead()
 *  - s3eFileWrite()
 *  - s3eMallocBase()
 *
 * This application demonstrates how to use the S3E file management
 * functionality. The application lets you open the original file by pressing
 * 1 and read the file by pressing 2. You can close the opened file by
 * pressing 3.
 *
 * The following graphic illustrates the example output.
 *
 * @image html s3eFileIOBasicImage.png
 *
 * @include s3eFileIOBasic.cpp
 */

#include "ExamplesMain.h"

enum FileIO
{
    kOpen,
    kRead,
    kReset,
    kError
};

#define FILENAME "old.txt"                  // name of file which is to be operated on

static FileIO      g_Status = kReset;              // Statemachine for file operation
static s3eFile*    g_FileHandle;                   // Handle to file which is to be operated on
static char        g_ErrorString[256];             // Contains info about errors for displaying purposes
static char*       g_DataToBeRead;                 // Data which is being kRead from the file
static int         g_ColourSpecifier;
static Button*     g_ButtonOpen = NULL;
static Button*     g_ButtonRead = NULL;
static Button*     g_ButtonClose = NULL;

void ExampleInit()
{
    g_ButtonOpen = NewButton("Open");
    g_ButtonRead = NewButton("Read");
    g_ButtonClose = NewButton("Close");
}

void ExampleTerm()
{
    s3eFreeBase((void*)g_DataToBeRead);
}

/*
 * The following function checks the keyboard state and performs a number of
 * operations based on it. The options are:
 *  - If s3eKey1 is pressed, the specified file is opened for reading.
 *  - If s3eKey2 is pressed, data is read from the opened file.
 *  - If s3eKey3 is pressed, the file is closed.
 */
bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    if ((pressed == g_ButtonOpen) && (g_Status == kReset))
    {
        //Open file for reading in binary to prevent the crlf translation
        g_FileHandle = s3eFileOpen(FILENAME, "rb");

        if (g_FileHandle != NULL)
        {
            //No error has occurred
            g_Status = kOpen;
        }
        else
        {
            // Something went wrong during opening of the file
            // retrieve error for display
            g_Status = kError;
            s3eFileGetError();
            strcpy(g_ErrorString, s3eFileGetErrorString());
        }
    }

    if ((pressed == g_ButtonRead) && (g_Status == kOpen))
    {
        // Clear Buffer to make sure space is not allocated more than once
        s3eFreeBase((void*)g_DataToBeRead);

        g_ColourSpecifier = strlen("`x666666");

        // Allocate buffer to be filled with the files contents
        int32 fileSize = s3eFileGetSize(g_FileHandle);
        g_DataToBeRead = (char*)s3eMallocBase(fileSize+g_ColourSpecifier+1);
        memset(g_DataToBeRead, 0, fileSize+g_ColourSpecifier);
        strcpy(g_DataToBeRead, "`x666666");

        // Read data from file
        if (s3eFileRead(&g_DataToBeRead[g_ColourSpecifier], fileSize, 1, g_FileHandle) != 1)
        {
            // An kError has occurred, retrieve information for display
            g_Status = kError;
            s3eFileGetError();
            strcpy(g_ErrorString, s3eFileGetErrorString());
        }
        else
        {
            // Data reading has been successful
            g_DataToBeRead[fileSize+g_ColourSpecifier] = '\0';
            g_Status = kRead;
        }
    }

    if ((pressed == g_ButtonClose) && ((g_Status == kOpen) || (g_Status == kRead)))
    {
        // Make sure file has been opened before trying to close it
        if (g_FileHandle)
            s3eFileClose(g_FileHandle);
        g_Status = kReset;
    }

    return true;
}

/*
 * The following function outputs any errors that have occurred and the
 * state of the File System.
 */
void ExampleRender()
{
    const int textHeight = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);
    int ypos = GetYBelowButtons() + 2 * textHeight;

    switch (g_Status)
    {
        case kReset:
            g_ButtonOpen->m_Enabled = true;
            g_ButtonRead->m_Enabled = false;
            g_ButtonClose->m_Enabled = false;
            break;
        case kOpen:
            g_ButtonOpen->m_Enabled = false;
            g_ButtonRead->m_Enabled = true;
            g_ButtonClose->m_Enabled = true;
            s3eDebugPrint(10, ypos, "`x666666File Opened: " FILENAME, 0);
            break;
        case kRead:
            g_ButtonOpen->m_Enabled = false;
            g_ButtonRead->m_Enabled = true;
            g_ButtonClose->m_Enabled = true;
            s3eDebugPrint(10, ypos, "`x666666File Read, Contents are:", 0);
            ypos = ypos + textHeight * 2;
            s3eDebugPrint(10, ypos, g_DataToBeRead, 1);
            break;
        case kError:
            g_ButtonOpen->m_Enabled = false;
            g_ButtonRead->m_Enabled = false;
            g_ButtonClose->m_Enabled = false;
            s3eDebugPrint(10, ypos, "`x666666Error", 0);
            ypos = ypos + textHeight * 2;
            s3eDebugPrint(10, ypos, g_ErrorString, 1);
            break;
    }
}
