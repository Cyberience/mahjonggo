/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwSSL IwSSL Example
 *
 * This is an example of the use of the IwSSL secure sockets component.
 * Demonstrates how to connect to a server socket in a secure manner.
 *
 * @include IwSSLExample.cpp
 */

//-----------------------------------------------------------------------------

#include "IwSSLExample.h"
#include <string>
#include "s3eMemory.h"
#include "ExamplesMain.h"
#include "IwGx.h"
#include "IwGxPrint.h"

#include <s3eSocket.h>

#include <unistd.h>
#include <assert.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <openssl/ssl.h>

#define TIMEOUT 20 // 20 seconds to comply
#define SSL_SERVER "www.google.com"

typedef enum SocketState
{
    IDLE,
    CONNECTING,
    HANDSHAKING,
    SECURE_CONNECTION,
    SENT_REQUEST,
    REPLY_RECEIVED
} SocketState;

SocketState g_socketState = IDLE;

const char *g_statusMessage = "Initialising..";

// The SSL connection block.
// One per connection required.
SSL *g_SSL;

// The context
// One per connection required
SSL_CTX *g_SSL_CTX;

// The socket we're going to connect via
int g_socket = -1;

// Flag which network events we're interested in
bool g_bWantRead = false, g_bWantWrite = false;

// Scratch inet address.
s3eInetAddress g_inetAddr;

// Record the request time for timeout
time_t g_requestTime = 0;

// Forward declarations
void StartSSLHandshake();
void ContinueSSLHandshake();
void DestroySSL();

/* DestroySSL - Destroy all SSL objects related to the connect */
void DestroySSL()
{
    if (g_SSL)
    {
        SSL_shutdown(g_SSL);
        SSL_free(g_SSL);
        g_SSL = NULL;
    }

    if (g_SSL_CTX)
    {
        SSL_CTX_free(g_SSL_CTX);
        g_SSL_CTX = NULL;
    }
}

/* StartSSLHandshake - The first half of the connection process */
void StartSSLHandshake()
{
    if (g_SSL)
        DestroySSL();

    // Choose TLSv1..
    SSL_METHOD *method  = TLSv1_client_method();
    assert(method);

    g_SSL_CTX = SSL_CTX_new(method);
    assert(g_SSL_CTX);

    // Turn off verification of certs.. this is currently
    // unsupported.
    SSL_CTX_set_verify(g_SSL_CTX, SSL_VERIFY_NONE, NULL);

    g_SSL = SSL_new(g_SSL_CTX);

    // Set socket to use..
    SSL_set_fd(g_SSL, g_socket);

    ContinueSSLHandshake();
}

/* ContinueSSLHandshake - The second half of the connection process */
void ContinueSSLHandshake()
{
    IwTrace(SSL, ("ContinueSSLHandshake"));

    int ret;
    if ((ret = SSL_connect(g_SSL)) != SSL_SUCCESS)
    {
        int err = SSL_get_error(g_SSL, 0);

        if (err == SSL_ERROR_WANT_READ)
            g_bWantRead = true;
        else if (err == SSL_ERROR_WANT_WRITE)
            g_bWantWrite = true;
        else
        {
            IwTrace(SSL, ("SSL Error"));
            g_statusMessage = "SSL Handshake failed. Check connection";
        }
    }
    else
    {
        IwTrace(SSL, ("SSL handshake complete"));

        g_statusMessage = "Secure connection established";

        // Connection is now secure
        g_bWantWrite = true;
        g_socketState = SECURE_CONNECTION;
    }
}

// Event handlers..

int onDNSComplete(void *sysData, void *)
{
    IwTrace(SSL, ("DNS Lookup complete"));
    g_statusMessage = "DNS Lookup complete";

    if (sysData)
    {
        // DNS succeeded.

        // Translate address to berkely style

        struct sockaddr_in addr;
        memset(&addr, 0, sizeof(addr));

        s3eInetAddress *s3eAddr = (s3eInetAddress *)sysData;

        addr.sin_family = AF_INET;
        addr.sin_addr.s_addr = s3eAddr->m_IPAddress;
        addr.sin_port = htons(443); // The HTTPS port


        // Start up the socket

        g_socket = socket(PF_INET, SOCK_STREAM, 0);
        assert(g_socket != -1);

        // We first connect the socket to the server before initiating
        // the SSL handshake
        connect(g_socket, (struct sockaddr *)&addr, sizeof(addr));

        // Wait for socket to connect..
        g_socketState = CONNECTING;
        g_bWantWrite = true;
    }
    else
        g_statusMessage = "DNS lookup failed. Check connection.";

    return 0;
}

void SendRequest()
{
    const char *request = "GET / HTTP/1.0\n\n";

    // Make a simple HTTP request to the server using
    // the SSL_write function

    if (SSL_write(g_SSL, request, strlen(request)) <= 0)
    {
        IwTrace(SSL, ("SSL Error"));
        g_statusMessage = "SSL write failed.";
    }
    else
    {
        g_socketState = SENT_REQUEST;
        g_requestTime = time(NULL);
        g_bWantRead = true;
    }
}

char *g_response = NULL;

void ReceiveReply()
{
    if (g_response == NULL)
    {
        g_response = new char[1024];
        memset(g_response, 0, 1024);
    }

    // Receive data from the SSL layer.
    // SSL buffers the data so it's important to exhaust this buffer
    // before expecting the socket to select for read again

    static bool bDone = false;
    while (!bDone && SSL_read(g_SSL, &g_response[strlen(g_response)], 128))
    {
        // Look for the end of the headers.
        // This is enough to prove we've correctly perfomed the request
        // over the secure connection
        char *pos;
        if ((pos = strstr(g_response, "\r\n\r\n")) != NULL)
        {
            bDone = true;

            pos = strstr(g_response, "\n") - 1;
            *pos = 0;

            g_requestTime = 0;
            g_statusMessage = g_response;
        }
    }
}

//-----------------------------------------------------------------------------
void ExampleInit()
{
    IwGxInit();

    g_statusMessage = "Performing DNS lookup...";

    // Start by resolving the server address.
    s3eInetLookup(SSL_SERVER, &g_inetAddr, onDNSComplete, NULL);
}

//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // Close the socket before destroying SSL
    close(g_socket);
    DestroySSL();

    delete g_response;
    IwGxTerminate();
}

// From this point on, the code is general housekeeping functions
// common to other examples.
bool ExampleUpdate()
{
    // Check for request timeout
    if (g_requestTime && time(NULL) - g_requestTime > TIMEOUT * 1000)
    {
        // Request has timed out
        g_statusMessage = "Request timed out.";
        return false;
    }

    if (g_bWantRead || g_bWantWrite)
    {
        assert(g_socket != -1);

        fd_set r, w, e;
        struct timeval timeout;

        memset(&timeout, 0, sizeof(timeout));
        FD_ZERO(&r); FD_ZERO(&w); FD_ZERO(&e);

        if (g_bWantRead)
            FD_SET(g_socket, &r);

        if (g_bWantWrite)
            FD_SET(g_socket, &w);

        // Good practice to always check for errors..
        FD_SET(g_socket, &e);

        // NB: Passing NULL as timeval * to select is unsupported and will
        // cause a segfault.
        int nfds = select(g_socket + 1, &r, &w, &e, &timeout);

        if (nfds)
        {
            IwTrace(SSL, ("Socket has been selected"));

            // Reset the read/write flags..

            if (g_bWantRead && FD_ISSET(g_socket, &r))
                g_bWantRead = false;

            if (g_bWantWrite && FD_ISSET(g_socket, &w))
                g_bWantWrite = false;

            switch (g_socketState)
            {
                // We're now connected, start up SSL.
                case CONNECTING:
                    StartSSLHandshake();
                    break;

                // We're still handshaking, keep pumping the
                // SSL layer whilst protocol negotiation
                // continues.
                case HANDSHAKING:
                    ContinueSSLHandshake();
                    break;

                case SECURE_CONNECTION:
                    SendRequest();
                    break;

                case SENT_REQUEST:
                    ReceiveReply();
                    break;

                default: // Nothing
                    break;
            }
        }
    }

    s3eDeviceYield(1000); // Sleep 1 second for reply
    return true;
}

//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear screen
    IwGxClear( IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F );

    if (g_statusMessage)
    {
        IwGxPrintString(10, 50, g_statusMessage, 1);
    }

    // Swap buffers
    IwGxFlush();
    IwGxSwapBuffers();
}
