/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwTwitter IwTwitter Example
 * The following example demonstrates IwTwitter functionality.
 *
 * @include IwTwitter.cpp
 */

#include "ExamplesMain.h"
#include <math.h>
#include <sstream>

#include "json/json.h"
#include "iwtwitter.h"

#include "s3eOSReadString.h" // For text entry through native inputs

// Replace these with your own strings from your twitter account and application
// (For a real app, you'd not want to hardwire the user and password but it makes the example easier having them here.
// Also the API Secret should not be human readable - so take care.)
const char* TWITTER_USER = NULL;          // This is your Twitter username or email (without the @ for username)
const char* TWITTER_PASSWD = NULL;        // The account's password
const char* TWITTER_API_KEY = NULL;       // The API Key for your app, obtained via https://apps.twitter.com
const char* TWITTER_API_SECRET = NULL;    // The API Secret for your app, obtained via https://apps.twitter.com

class IwTwitterExample
{
    IwTwitter m_Twitter;
    std::string m_Message;
    std::string m_ReplyMsg;

    std::string m_screenName;

    Json::Reader m_Reader;
    void GetAccountCred();
    void GetErrorMessage();
public:
    IwTwitterExample(const std::string& usr, const std::string& pwd,
                const std::string& key, const std::string& secret);
    std::string Message() const {return m_Message;}
    bool Connect();
    void GetLastTwit();
    void GetFollower();
    void UpdateStatus();
};

std::auto_ptr<IwTwitterExample> g_IwTwitterExample;

static bool g_Connected = false;
static Button* g_ButtonConnect = NULL;
static Button* g_ButtonGetLastTwit = NULL;
static Button* g_ButtonGetFollower = NULL;
static Button* g_ButtonUpdateStatus = NULL;

static void SetInitialButtonsState()
{
    g_Connected = false;
    g_ButtonConnect->m_Enabled = true;
    g_ButtonGetLastTwit->m_Enabled = false;
    g_ButtonGetFollower->m_Enabled = false;
    g_ButtonUpdateStatus->m_Enabled = false;
}

static void SetConnectedButtonsState()
{
    g_Connected = true;
    g_ButtonConnect->m_Enabled = false;
    g_ButtonGetLastTwit->m_Enabled = true;
    g_ButtonGetFollower->m_Enabled = true;
    g_ButtonUpdateStatus->m_Enabled = true;
}

void ExampleInit()
{
    if (!TWITTER_USER || !TWITTER_PASSWD)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "Please supply a valid Twitter "
            "user account for TWITTER_USER and associated password for TWITTER_PASSWD "
            "in s3eTwitter.cpp!");
        s3eDeviceExit(1);
    }

    if (!TWITTER_API_KEY || !TWITTER_USER)
    {
        s3eDebugErrorShow(S3E_MESSAGE_CONTINUE, "Please supply a valid app API Key "
            "for TWITTER_API_KEY and associated app API Secret for TWITTER_API_SECRET "
            "in s3eTwitter.cpp!");
        s3eDeviceExit(1);
    }

    g_IwTwitterExample.reset(new IwTwitterExample(TWITTER_USER, TWITTER_PASSWD, TWITTER_API_KEY, TWITTER_API_SECRET));

    g_ButtonConnect = NewButton("Connect Twitter");
    g_ButtonGetLastTwit = NewButton("Get last Twit");
    g_ButtonGetFollower = NewButton("Get Follower");
    g_ButtonUpdateStatus = NewButton("Update Status");

    SetInitialButtonsState();
}

void ExampleTerm()
{
}

bool ExampleUpdate()
{
    Button* pressed = GetSelectedButton();

    if (pressed == g_ButtonConnect)
    {
        if (g_IwTwitterExample->Connect())
            SetConnectedButtonsState();
    }

    if (pressed == g_ButtonGetLastTwit)
    {
        g_IwTwitterExample->GetLastTwit();
    }

    if (pressed == g_ButtonGetFollower)
    {
        g_IwTwitterExample->GetFollower();
    }

    if (pressed == g_ButtonUpdateStatus)
    {
        g_IwTwitterExample->UpdateStatus();
    }
    return true;
}

/*
 * The ExampleRender function outputs a set of strings indicating the state of
 * playback. The additional output that can occur outputs whether the callback
 * was registered successfully or not.
 */
void ExampleRender()
{
    const int textHeight = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);
    int ypos = GetYBelowButtons() + textHeight * 2;

    s3eDebugPrintf(10, ypos, 1, g_Connected?"`x666666Is connected":"`x666666Is disconnected");
    s3eDebugPrintf(10, ypos + 30, 1, "`x666666%s", g_IwTwitterExample->Message().c_str());
}

// IwTwitterExample implementation
IwTwitterExample::IwTwitterExample(const std::string& usr, const std::string& pwd,
                         const std::string& key, const std::string& secret)
    : m_Twitter(usr, pwd, key, secret)
{
}

void IwTwitterExample::GetErrorMessage()
{
    m_Twitter.getLastCurlError(m_Message);

    if (m_Message.empty())
    {
        std::string response;

        m_Twitter.getLastWebResponse(response);

        const std::string ERROR_TAG("<error>");
        const std::string ERROR_TAG_CLOSE("</error>");

        std::string::size_type error_pos = response.find(ERROR_TAG);
        if (error_pos != std::string::npos)
        {
            error_pos += ERROR_TAG.length();

            response.erase(0, error_pos);

            std::string::size_type error_end = response.find(ERROR_TAG_CLOSE);

            if (error_end != std::string::npos)
            {
                m_Message.assign(response.substr(0, error_end));
            }
        }

        if (m_Message.empty())
        {
            m_Message.assign("Unknown error");
        }
    }
}

bool IwTwitterExample::Connect()
{
    if (!m_Twitter.connect())
    {
        GetErrorMessage();
        return false;
    }

    GetAccountCred();
    return true;
}

void IwTwitterExample::GetAccountCred()
{
    if (!m_Twitter.accountVerifyCredGet())
    {
        GetErrorMessage();
        return;
    }
    m_Twitter.getLastWebResponse(m_ReplyMsg);

    Json::Value values;
    m_Reader.parse(m_ReplyMsg, values);

    std::string name = values.get("name", "<noname>").asString();
    m_screenName = values.get("screen_name", "<noname>").asString();

    m_Message = "account/verify_credentials :\nname : " + name + "\nscreen_name : " + m_screenName;
}

void IwTwitterExample::GetLastTwit()
{
    if (!m_Twitter.timelineUserGet(false, false, 1, m_screenName))
    {
        GetErrorMessage();
        m_Message.insert(0, "GetLastTwit error : ");
        return;
    }
    m_Twitter.getLastWebResponse(m_ReplyMsg);

    Json::Value values;
    m_Reader.parse(m_ReplyMsg, values);

    m_Message = "statuses/user_timeline :\n";
    if (0 != values.size())
        m_Message += ("text : " +  values[0].get("text", "<no text>").asString());
    else
        m_Message += "No statuses found." ;
}

void IwTwitterExample::GetFollower()
{
    std::string nextCursor("");
    std::string searchUser(m_screenName);
    std::stringstream stream;
    do
    {
        if (!m_Twitter.followersIdsGet(nextCursor, searchUser))
        {
            GetErrorMessage();
            return;
        }
        m_Twitter.getLastWebResponse(m_ReplyMsg);

        Json::Value values;
        m_Reader.parse(m_ReplyMsg, values);

        Json::Value ids = values.get("ids", "<noname>");

        stream << "followers/ids :\nsize : " << ids.size();

        if (ids.size())
        {
            Json::Value id = ids[0];
            int firstId = id.asInt();
            stream << "\nid[0] : " << firstId;
        }

        nextCursor = values.get("next_cursor_str", "0").asString();
    }
    while (nextCursor != "0"  && !nextCursor.empty());
    m_Message = stream.str();
}

void IwTwitterExample::UpdateStatus()
{
    std::stringstream stream;
    std::string tmpStr = "Sample status message";

    //Use OSReadString to get text from the user
    if (s3eOSReadStringAvailable())
    {
        const char *statusText = s3eOSReadStringUTF8("Type your twit here:");
        if (NULL == statusText || 0 == *statusText)
        {
            m_Message = "Empty status message";
            return;
        }
        tmpStr = statusText;
    }

    if (!m_Twitter.statusUpdate(tmpStr) )
    {
        GetErrorMessage();
        return;
    }
    m_Twitter.getLastWebResponse(m_ReplyMsg);

    Json::Value values;
    m_Reader.parse(m_ReplyMsg, values);
    if (!values["errors"].empty())
    {
        Json::Value errors = values["errors"];
        stream << "Failed to post message";
        stream << "`xff0000\nerror code: " << errors[0].get("code","<no code>").asInt();
        stream << "\nerror message: " << errors[0].get("message","<no message>").asString();
    }
    else if (!values["text"].empty())
    {
        stream << "Status was updated with : \ntext : " << values["text"].asString();
    }
    else
    {
        stream << "Raw server replay\n" << m_ReplyMsg;
    }
    m_Message = (stream.str());
}
