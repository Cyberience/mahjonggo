/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
//-----------------------------------------------------------------------------
// ExampleIwGraphicsModelBuilder
//-----------------------------------------------------------------------------

/**
 * @page ExampleLegacyIwGraphicsModelBuilder IwGraphics Model Builder Example (Legacy)
 *
 * This example demonstrates the conditioning of models for SW and HW,
 * as well as "chunking" of large models.
 *
 * @note For more information about resource templates and the conditioning of
 * models, see the @ref ResManager "Resource Manager" documentation.
 *
 * The main classes used to achieve this are:
 *  - CIwModel
 *  - CIwResGroup
 *  - CIwModelBuilder
 *  - CIwMenuManager
 *  - CIwFace
 *
 * The main functions used to achieve this are:
 *  - IwGraphicsInit()
 *  - CIwResManager::LoadGroup()
 *  - CIwResManager::GetGroupNamed()
 *  - CIwTextParserITX::ParseFile()
 *  - CIwModel::Render()
 *
 * By default, the model builder will condition models for SW rasterisation, unless IwGx has been initialised
 * to support HW rasterisation, in which case the model builder will condition models for HW rasterisation.
 * Models conditioned for SW rasterisation will not be renderable if the pipeline is switched
 * to HW rasterisation. However the converse is not true; i.e., models conditioned for
 * HW can in fact be rendered using SW rasterisation (although performance may not be optimal).
 *
 * To enable this example to dynamically switch between SW and HW rasterisation, you must amend the "Rasterisation" line
 * in the app.icf file to "Rasterisation=SW/HW". This will initialise IwGx to support both forms of rasterisation, and
 * start in SW mode. "Rasterisation=HW/SW" would do the same but start in HW mode.
 *
 * To toggle between SW and HW rasterisation:
 *  -# Build and run the example.
 *  -# Press <b>F6</b> to display the debug menu.
 *  -# Use the arrow keys to navigate into the IwGx settings.
 *  -# Select the rasterisation option and use the <b>+</b> and <b>-</b>
 *     keys to the toggle rasterisation between HW and SW.
 *
 * The display changes (for example, any texture filtering disappears for
 * SW rasterisation), but you can see that the model displays equivalently in
 * SW and HW.
 *
 * Now try editing the app.icf file so that the application is not built to support HW rasterisation (use the line "Rasterisation=SW").
 * When running the example, the model will now be conditioned optimally for SW rasterisation (any attempt to switch to HW
 * rasterisation will result in an assertion).
 *
 * The second point of this example is to illustrate the concept of model "chunking". This is a model conditioning option which
 * binary-partitions the model into spatially discrete sections. A hierarchy of bounding spheres is constructed to provide rapid
 * frustrum culling of non-visible sections.
 *
 * The following code is contained in the loaded ITX file to condition the loaded model.
 *
 * @code
 * CIwResTemplateGEO
 * {
 *  name        "chunked"
 *  chunked     true
 *  maxPrimsPerChunk 100
 * }
 * @endcode
 *
 * @note For more information on conditioning of resources, see the
 * @ref resourcetemplates "Resource Templates" section
 * of the <i>IwResManager API Documentation</i>
 *
 * The following code is contained in the loaded group file, ModBuild.group:
 *
 * @code
 * CIwResGroup
 * {
 *  name    "Example"
 *
 *  // Plain model
 *  "lava/models/lava.geo"
 *
 *  // Chunked model
 *  useTemplate "geo" "chunked"
 *  "lava/models/lava.geo" { resName "lavaChunked" }
 * }
 * @endcode
 *
 * The GROUP file causes the same GEO file to be loaded twice to create
 * two different binary model resources. The second version is chunked.
 *
 * To toggle between chunked and not chunked:
 *  -# Press <b>2</b> to switch to rendering the second version.
 *  -# Press F6 to bring up the debug menu.
 *  -# Navigate to IwGraphics settings, and toggle the COLOUR_CHUNKS setting.
 *
 * This displays the model chunks in contrasting colours, showing the spatial partitioning.
 *
 * @note For more information on the loading of meshes from GEO files, see @ref loadingofmodels "Loading of Models".
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwGraphicsModBuildImage.png
 *
 * @include IwGraphicsModBuild.cpp
 */

#include "IwGraphics.h"
#include "IwGx.h"
#include "IwGxPrint.h"
#include "IwModel.h"
#include "IwModelBuilder.h"
#include "IwResManager.h"
#include "IwTextParserITX.h"
#include "s3eKeyboard.h"
#include "ExamplesMain.h"

// Angles
CIwSVec3    s_Angles;

// Local matrix
CIwMat      s_ModelMatrix;

// Model resource
CIwModel*   s_Models[2];

// Timer
uint32      s_Timer;

// Model ID
uint32      m_ModelID = 0;

void CreateButtonsUI(int w, int h)
{
    // Create the UI layout
    AddButton("Zoom in: 1", 0, 70, 70, 30, s3eKey1);
    AddButton("Zoom out: 2", 0, 110, 70, 30, s3eKey2);
    AddButton("Model 1: 3", w - 70, 70, 70, 30, s3eKey3);
    AddButton("Model 2: 4", w - 70, 110, 70, 30, s3eKey4);
}

void SurfaceChangedCallback()
{
    DeleteButtons();
    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}

//--------------------------------------------------------------------------------
CIwMenu* DebugCreateMainMenu()
{
    IW_CALLSTACK("DebugCreateMainMenu")

    CIwMenu* pMenu = new CIwMenu;

#ifdef IW_DEBUG
    pMenu->AddItem(new CIwMenuItemResManager);
    pMenu->AddItem(new CIwMenuItemGraphics);
    pMenu->AddItem(new CIwMenuItemGx);
#endif
    return pMenu;
}
//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    IwGxRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
    IwGxInit();
    IwGraphicsInit();

    // Set field of view
    IwGxSetPerspMul(0xa0);

    // Set near and far planes
    IwGxSetFarZNearZ(0x2000, 0x10);

    // Initialise angles
    s_Angles = CIwSVec3(0x200, 0x800, 0);

    // If configured to support HW, then build model for HW
#ifdef IW_BUILD_RESOURCES
    // Load the resource templates
    IwGetTextParserITX()->ParseFile("ModBuild_templates.itx");
#endif

    // Parse the GROUP file, which will load the model, its materials and textures
    IwGetResManager()->LoadGroup("ModBuild.group");

    // Get a pointer to the named group
    CIwResGroup* pGroup = IwGetResManager()->GetGroupNamed("Example");

    // Get and store pointers to the model resources
    s_Models[0] = (CIwModel*)pGroup->GetResNamed("lava", IW_GRAPHICS_RESTYPE_MODEL);
    s_Models[1] = (CIwModel*)pGroup->GetResNamed("lavaChunked", IW_GRAPHICS_RESTYPE_MODEL);

    // Set up the view matrix
    CIwMat view;
    view.SetRotZ(0x800);
    view.t.y =  0x80;
    view.t.z = -0x800;
    IwGxSetViewMatrix(&view);

    //-------------------------------------------------------------------------
    // Set up scene lighting
    //-------------------------------------------------------------------------
    // Set single ambient light
    IwGxSetLightType(0, IW_GX_LIGHT_AMBIENT);
    CIwColour colA = {0x40, 0x40, 0x40, 0xff};
    IwGxSetLightCol(0, &colA);

    // Set single diffuse light
    IwGxSetLightType(1, IW_GX_LIGHT_DIFFUSE);
    CIwColour colD = {0xc0, 0xc0, 0xc0, 0xff};
    IwGxSetLightCol(1, &colD);
    CIwSVec3 dd(0x93c, 0x93c, 0x93c);
    IwGxSetLightDirn(1, &dd);

    // Set up the menu manager
    new CIwMenuManager;
    IwGetMenuManager()->SetTextCallback(IwGxPrintMenuCallback);
    IwGetMenuManager()->SetMainMenuFn(DebugCreateMainMenu);

    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    delete IwGetMenuManager();

    // Terminate
    IwGraphicsTerminate();
    IwGxTerminate();
    IwGxUnRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // Reset metrics
#ifdef IW_DEBUG
    IwGraphicsMetricsReset();
    IwGxMetricsReset();
#endif
    // Update angles from the arrow keys
    if (CheckCursorState() == EXCURSOR_LEFT )
        s_Angles.y -= 0x40;
    if (CheckCursorState() == EXCURSOR_RIGHT )
        s_Angles.y += 0x40;
    if (CheckCursorState() == EXCURSOR_UP )
        s_Angles.x -= 0x40;
    if (CheckCursorState() == EXCURSOR_DOWN )
        s_Angles.x += 0x40;

    // Move camera in/out
    if (CheckButton("Zoom out: 2") & S3E_KEY_STATE_DOWN )
        s_ModelMatrix.t.z += 0x40;
    if (CheckButton("Zoom in: 1") & S3E_KEY_STATE_DOWN )
        s_ModelMatrix.t.z -= 0x40;

    // Increase/decrease model ID
    if (CheckButton("Model 1: 3") & S3E_KEY_STATE_PRESSED )
        m_ModelID = (m_ModelID + 1) % 2;
    if (CheckButton("Model 2: 4") & S3E_KEY_STATE_PRESSED )
        m_ModelID = (m_ModelID + (2-1)) % 2;

#ifdef IW_DEBUG
    // Toggle flag to display chunks in debug colours
    if (s3eKeyboardGetState(s3eKeyD) & S3E_KEY_STATE_PRESSED)
    {
        if (IwGraphicsGetDebugFlags() & IW_GRAPHICS_DEBUG_MODEL_COLOUR_CHUNKS_F)
            IwGraphicsClearDebugFlags(IW_GRAPHICS_DEBUG_MODEL_COLOUR_CHUNKS_F);
        else
            IwGraphicsSetDebugFlags(IW_GRAPHICS_DEBUG_MODEL_COLOUR_CHUNKS_F);
    }
#endif

    // Build model matrix rotation from angles
    CIwMat rotX, rotY;
    rotX.SetRotX(s_Angles.x);
    rotY.SetRotY(s_Angles.y);
    s_ModelMatrix.CopyRot(rotY * rotX);

    // Update timer
    s_Timer++;

    // Update menu manager
    IwGetMenuManager()->Update();

    // Update IwGx state time stamp
    IwGxTickUpdate();
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Start with full lighting
    IwGxLightingOn();

    //-------------------------------------------------------------------------
    // Render a background poly
    //-------------------------------------------------------------------------
    CIwSVec2* pVec = AllocClientScreenRectangle();
    CIwMaterial* pMat = IW_GX_ALLOC_MATERIAL();
    pMat->Init();
    pMat->SetColAmbient(0, 0, 255, 255);
    IwGxSetMaterial(pMat);
    IwGxSetVertStreamScreenSpace(pVec, 4);
    IwGxSetScreenSpaceSlot(-1);
    IwGxDrawPrims(IW_GX_QUAD_STRIP, NULL, 4);
    IwGxSetScreenSpaceSlot(0);

    //-------------------------------------------------------------------------
    // Render the main model
    //-------------------------------------------------------------------------
    IwGxSetModelMatrix(&s_ModelMatrix);
    s_Models[m_ModelID]->Render();

    // Display name of model
    IwGxPrintString( 2, IwGxGetScreenHeight() - 70, s_Models[m_ModelID]->DebugGetName());

    // Paint the cursor keys buttons
    IwGxPrintString(2, IwGxGetScreenHeight() - 60, "Rotate Model: Arrow Keys");
    RenderCursorskeys();

    // Render menu manager
    IwGetMenuManager()->Render();

    // Flush and swap
    IwGxFlush();
    IwGxSwapBuffers();
}
