/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
//-----------------------------------------------------------------------------
// ExampleIwGraphicsResourceGroups
//-----------------------------------------------------------------------------

/**
 * @page ExampleLegacyIwGraphicsResourceGroups IwGraphics Resource Groups Example (Legacy)
 * This example demonstrates using resource groups and resource templates to condition models.
 *
 * The main classes used to achieve this are:
 *  - @ref CIwModel
 *  - @ref CIwMenuManager
 *  - @ref CIwMenuItemGraphics
 *
 * The main functions used to achieve this are:
 *  - IwGraphicsInit()
 *  - CIwTextParserITX::ParseFile()
 *  - IwGetResManager()
 *  - CIwResManager::GetGroupNamed()
 *  - CIwResManager::GetResNamed()
 *
 * In this example the same GEO file is loaded 5 times, and built into three different binary models. This is done as follows:
 *
 * Firstly, an ITX file is parsed which creates a named GEO resource template:
 * @code
 * CIwResTemplateGEO
 * {
 *  name "revNorms"
 *  reverseNorms true
 * }
 * @endcode
 *
 * Next, a GROUP file is loaded. The first relevant line is:
 * @code
 * "./Test01.geo" { resName "Test" }
 * @endcode
 *
 * This tells the resource manager to load the file "./Test01.geo". Additionally, the created binary model resource will be
 * named "Test".
 *
 * The next relevant line is:
 * @code
 * "./Test01.geo" { resName "Test1" buildNorms false }
 * @endcode
 *
 * This tells the resource manager to load the file "./Test01.geo", named the created binary model resource as "Test1", and
 * additionally build the model without vertex normals.
 *
 * The next relevant line is:
 * @code
 * useTemplate "geo" "revNorms"
 * @endcode
 *
 * This tells the resource manager to use the GEO resource template named
 * "revNorms" to build all future models (until another named template is
 * specified). So the following line:
 * @code
 * "./Test01.geo" { resName "Test2" }
 * @endcode
 * ...results in the same GEO file being used to build a third binary model
 * resource, this time named "Test2", and using the settings of the current GEO
 * resource template, which in this case builds the model with reversed
 * normals.
 *
 * Another two version of the resource are loaded, this time with different
 * scales. Notice how, on these versions, the "revNorms true" setting persists,
 * because it is still set on the current template.
 *
 * Press the 1 and 2 buttons to cycle the model being displayed. Notice
 * how the scene diffuse lighting changes as we move between
 * the model with default normals, no normals, and reversed normals.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwGraphicsResourceGroupsImage.png
 *
 * @note For more information on the loading of meshes from GEO files, see @ref
 * loadingofmodels "Loading of Models".
 *
 * @include IwGraphicsResourceGroups.cpp
 */

#include "IwGraphics.h"
#include "IwGxPrint.h"
#include "IwModel.h"
#include "IwModelBuilder.h"
#include "IwResManager.h"
#include "IwTextParserITX.h"
#include "s3eKeyboard.h"
#include "ExamplesMain.h"

// Angles
CIwSVec3    s_Angles;

// Local matrix
CIwMat      s_ModelMatrix;

// Model resource
CIwModel*   s_Models[5];

// Timer
uint32      s_Timer;

// Model ID
uint32      m_ModelID = 0;

//--------------------------------------------------------------------------------
CIwMenu* DebugCreateMainMenu()
{
    IW_CALLSTACK("DebugCreateMainMenu")

    CIwMenu* pMenu = new CIwMenu;

#ifdef IW_DEBUG
    pMenu->SetXY(0, 50);
    pMenu->AddItem(new CIwMenuItemResManager);
    pMenu->AddItem(new CIwMenuItemGraphics);
#endif
    return pMenu;
}


void CreateButtonsUI(int w, int h)
{
    // Create the UI
    AddButton("Model1: 1", 0, 70, 70, 30, s3eKey1);
    AddButton("Model2: 2", 0, 110, 70, 30, s3eKey2);
}

void SurfaceChangedCallback()
{
    DeleteButtons();
    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    IwGxRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
    IwGxInit();
    IwGraphicsInit();

    // Set screen clear colour
    IwGxSetColClear(0x00, 0x00, 0xff, 0x00);

    // Set field of view
    IwGxSetPerspMul(0xa0);

    // Set near and far planes
    IwGxSetFarZNearZ(0x400, 0x10);

    // Initialise angles
    s_Angles = CIwSVec3(0x200, 0x200, 0);

#ifdef IW_BUILD_RESOURCES
    // Load the resource templates
    IwGetTextParserITX()->ParseFile("ResourceGroups_templates.itx");
#endif

    // Parse the GROUP file, which will load the model, its materials and textures
    IwGetResManager()->LoadGroup("ResourceGroups.group");

    // Get a pointer to the named group
    CIwResGroup* pGroup = IwGetResManager()->GetGroupNamed("Example");

    // Get and store pointer to the model resource
    s_Models[0] = (CIwModel*)pGroup->GetResNamed("Test", IW_GRAPHICS_RESTYPE_MODEL);
    s_Models[1] = (CIwModel*)pGroup->GetResNamed("Test1", IW_GRAPHICS_RESTYPE_MODEL);
    s_Models[2] = (CIwModel*)pGroup->GetResNamed("Test2", IW_GRAPHICS_RESTYPE_MODEL);
    s_Models[3] = (CIwModel*)pGroup->GetResNamed("Test3", IW_GRAPHICS_RESTYPE_MODEL);
    s_Models[4] = (CIwModel*)pGroup->GetResNamed("Test4", IW_GRAPHICS_RESTYPE_MODEL);

    // Set up the view matrix
    CIwMat view;
    view.SetRotZ(0x800);
    view.t.y =  0x0;
    view.t.z = -0x180;
    IwGxSetViewMatrix(&view);

    //-------------------------------------------------------------------------
    // Set up scene lighting
    //-------------------------------------------------------------------------
    // Set single ambient light
    IwGxSetLightType(0, IW_GX_LIGHT_AMBIENT);
    CIwColour colA = {0x20, 0x20, 0x20, 0xff};
    IwGxSetLightCol(0, &colA);

    // Set single diffuse light
    IwGxSetLightType(1, IW_GX_LIGHT_DIFFUSE);
    CIwColour colD = {0xc0, 0xc0, 0xc0, 0xff};
    IwGxSetLightCol(1, &colD);
    CIwSVec3 dd(0x1000, 0, 0);
    IwGxSetLightDirn(1, &dd);

    // Set up the menu manager
    new CIwMenuManager;
    IwGetMenuManager()->SetTextCallback(IwGxPrintMenuCallback);
    IwGetMenuManager()->SetMainMenuFn(DebugCreateMainMenu);

    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    delete IwGetMenuManager();

    // Terminate
    IwGraphicsTerminate();
    IwGxTerminate();
    IwGxUnRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // Reset metrics
#ifdef IW_DEBUG
    IwGraphicsMetricsReset();
    IwGxMetricsReset();
#endif

    // Update angles from the arrow keys
    if (CheckCursorState() == EXCURSOR_LEFT )
        s_Angles.y -= 0x40;
    if (CheckCursorState() == EXCURSOR_RIGHT )
        s_Angles.y += 0x40;
    if (CheckCursorState() == EXCURSOR_UP )
        s_Angles.x -= 0x40;
    if (CheckCursorState() == EXCURSOR_DOWN )
        s_Angles.x += 0x40;

    // Increase/decrease model ID
    if (CheckButton("Model1: 1") & S3E_KEY_STATE_PRESSED)
        m_ModelID = (m_ModelID + 1) % 5;
    if (CheckButton("Model2: 2") & S3E_KEY_STATE_PRESSED)
        m_ModelID = (m_ModelID + (5-1)) % 5;

    // Build model matrix rotation from angles
    CIwMat rotX, rotY;
    rotX.SetRotX(s_Angles.x);
    rotY.SetRotY(s_Angles.y);
    s_ModelMatrix.CopyRot(rotY * rotX);

    // Update timer
    s_Timer++;

    // Update menu manager
    IwGetMenuManager()->Update();

    // Update IwGx state time stamp
    IwGxTickUpdate();
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Start with full lighting
    IwGxLightingOn();

    //-------------------------------------------------------------------------
    // Render the main model
    //-------------------------------------------------------------------------
    IwGxSetModelMatrix(&s_ModelMatrix);
    s_Models[m_ModelID]->Render();

    // Paint the cursor keys buttons and prompt text
    IwGxPrintString(2, IwGxGetScreenHeight() - 60, "Rotate Model: Arrow Keys");
    RenderCursorskeys();

    // Display name of model
    IwGxPrintString( 2, IwGxGetScreenHeight() - 70, s_Models[m_ModelID]->DebugGetName());

    // Render menu manager
    IwGetMenuManager()->Render();

    // Flush and swap
    IwGxFlush();
    IwGxSwapBuffers();
}
