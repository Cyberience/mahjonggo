/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
//-----------------------------------------------------------------------------
// ExampleIwGraphicsModel
//-----------------------------------------------------------------------------

/**
 * @page ExampleLegacyIwGraphicsModel IwGraphics Model Example (Legacy)
 *
 * This example demonstrates the loading of a model resource, and rendering of
 * the model.
 *
 * The main classes used to achieve this are:
 *  - CIwModel
 *  - CIwResGroup
 *
 * The main functions used to achieve this are:
 *  - IwGraphicsInit()
 *  - CIwResManager::LoadGroup()
 *  - CIwResManager::GetGroupNamed()
 *  - IwGraphicsTerminate();
 *  - CIwModel::Render();
 *
 * After IwGraphicsInit() is called the GROUP file containing details of the
 * target model is loaded.  The model resource is then identified using the
 * GetGroupNamed() function and then rendered by calling the Render() function.
 *
 * Upon the closing of the example the IwGraphicsTerminate() function is
 * called.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwGraphicsModelImage.png
 *
 * @note For more information on the loading of meshes from GEO files, see @ref
 * loadingofmodels "Loading of Models".
 *
 * @include IwGraphicsModel.cpp
 */

#include "IwGraphics.h"
#include "IwModel.h"
#include "IwModelBuilder.h"
#include "IwResManager.h"
#include "IwGxPrint.h"
#include "s3eKeyboard.h"
#include "ExamplesMain.h"

// Angles
CIwSVec3    s_Angles;

// Local matrix
CIwMat      s_ModelMatrix;

// Model resource
CIwModel*   s_Model;

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    IwGxInit();
    IwGraphicsInit();

    // Set screen clear colour
    IwGxSetColClear(0x40, 0x40, 0x40, 0x00);

    // Set field of view
    IwGxSetPerspMul(0xa0);

    // Set near and far planes
    IwGxSetFarZNearZ(0x400, 0x10);

    // Initialise angles
    s_Angles = CIwSVec3(0, 0x800, 0);

    // Parse the GROUP file, which will load the model, its materials and textures
    IwGetResManager()->LoadGroup("Model.group");

    // Get a pointer to the named group
    CIwResGroup* pGroup = IwGetResManager()->GetGroupNamed("Example");

    // Get and store pointer to the model resource
    s_Model = (CIwModel*)pGroup->GetResNamed("FunkyVic", IW_GRAPHICS_RESTYPE_MODEL);

    // Set up the view matrix
    CIwMat view;
    view.SetRotZ(0x800);
    view.t.y =  0x80;
    view.t.z = -0x180;
    IwGxSetViewMatrix(&view);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // Terminate
    IwGraphicsTerminate();
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // Update angles from the arrow keys
    if (CheckCursorState() == EXCURSOR_LEFT )
        s_Angles.y -= 0x40;
    if (CheckCursorState() == EXCURSOR_RIGHT )
        s_Angles.y += 0x40;
    if (CheckCursorState() == EXCURSOR_UP )
        s_Angles.x -= 0x40;
    if (CheckCursorState() == EXCURSOR_DOWN )
        s_Angles.x += 0x40;

    // Build view matrix rotation from angles
    CIwMat rotX, rotY;
    rotX.SetRotX(s_Angles.x);
    rotY.SetRotY(s_Angles.y);
    s_ModelMatrix.CopyRot(rotY * rotX);

    // Update IwGx state time stamp
    IwGxTickUpdate();
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Start with no lighting
    IwGxLightingOff();

    //-------------------------------------------------------------------------
    // Render the model
    //-------------------------------------------------------------------------
    IwGxSetModelMatrix(&s_ModelMatrix);
    s_Model->Render();

    // Paint the cursor keys buttons and prompt text
    IwGxPrintString(2, IwGxGetScreenHeight() - 60, "Rotate Model: Arrow Keys");
    RenderCursorskeys();

    // End drawing
    IwGxFlush();

    // Swap buffers
    IwGxSwapBuffers();
}
