/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
//-----------------------------------------------------------------------------
// ExampleIwGraphicsModel
//-----------------------------------------------------------------------------

/**
 * @page ExampleLegacyIwGraphicsGLES2 IwGraphics GLES2 Example (Legacy)
 *
 * This example demonstrates the loading of a model resource which includes a
 * custom GLES 2.0 shader, changing shader parameters in realtime and rendering
 * of the model.
 *
 * This example builds on the IwGraphicsModel example.
 *
 * The main classes used to achieve this are:
 *  - CIwGxShaderTechnique
 *  - CIwGxShaderUniform
 *
 * After the resources have been loaded, the shader technique is retrieved from
 * ResManager.  The shader technique resource is a standard CIwResource.
 *
 * The shader in this example exposes one of its uniforms as a parameter. A
 * pointer to this parameter (as a CIwGxShaderUniform) can be retrived using
 * the GetParam method of the CIwGxShaderTechnique.
 *
 * CIwGxShaderUniform::Set methods can be used to change the value of the
 * parameter in realtime.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwGraphicsGLES2Image.png
 *
 * @note If the example is run with SW rendering or the GLES 1.x API (the
 * default) the model will appear without the shader effect, and will render
 * normally. The second texture will be ignored.  In this way, data sets can be
 * build that scale from SW to GLES 2.0
 *
 * @include IwGraphicsGLES2.cpp
 */

#include "IwGraphics.h"
#include "IwModel.h"
#include "IwModelBuilder.h"
#include "IwResManager.h"
#include "IwGx.h"
#include "s3eKeyboard.h"
#include "ExamplesMain.h"

// Angles
CIwSVec3    s_Angles;

// Local matrix
CIwMat      s_ModelMatrix;

// Model resource
CIwModel*   s_Model;
CIwTexture* s_Texture;
CIwGxShaderUniform* s_Param;

float s_UniformValue = 0.5f;

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    IwGxInit();
    IwGraphicsInit();

    // Set screen clear colour
    IwGxSetColClear(0x40, 0x40, 0x40, 0x00);

    // Set field of view
    IwGxSetPerspMul(0x4f0 * IwGxGetScreenHeight() / 320);

    // Set near and far planes
    IwGxSetFarZNearZ(0x400, 0x10);

    // Initialise angles
    s_Angles = CIwSVec3(0, 0x800, 0);

    // Parse the GROUP file, which will load the model, its materials and textures
    IwGetResManager()->LoadGroup("GLES2.group");

    // Get a pointer to the named group
    CIwResGroup* pGroup = IwGetResManager()->GetGroupNamed("Example");

    // Get and store pointer to the model resource
    s_Model = (CIwModel*)pGroup->GetResNamed("FunkyVicGLES2", IW_GRAPHICS_RESTYPE_MODEL);

    // Get a pointer to the background texture
    s_Texture = (CIwTexture*)pGroup->GetResNamed("background", "CIwTexture");

    // Get a pointer to the shader
    CIwGxShaderTechnique* technique =
        (CIwGxShaderTechnique*)pGroup->GetResNamed("FunkyShader", "CIwGxShaderTechnique");

    // Get the parameter we're interested in
    s_Param = technique->GetParam("refractiveIndex");

    // Set up the view matrix
    CIwMat view;
    view.SetRotZ(0x800);
    view.t.y =  0x100;
    view.t.z = -0x180;
    IwGxSetViewMatrix(&view);

    if (IwGxGetHWType() == IW_GX_HWTYPE_GL2 )
    {
        //Add buttons
        AddButton("Toggle shaders", 0, 60, 100, 30, s3eKey1);
        AddButton("Increase refractiveIndex", 0, 95, 150, 30, s3eKey2);
        AddButton("Decrease refractiveIndex", 0, 135, 150, 30, s3eKey3);
    }
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // Terminate
    IwGraphicsTerminate();
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // Update angles from the arrow keys
    if (CheckCursorState() == EXCURSOR_LEFT )
        s_Angles.y -= 0x40;
    if (CheckCursorState() == EXCURSOR_RIGHT )
        s_Angles.y += 0x40;
    if (CheckCursorState() == EXCURSOR_UP )
        s_Angles.x -= 0x40;
    if (CheckCursorState() == EXCURSOR_DOWN )
        s_Angles.x += 0x40;

    if (CheckButton("Toggle shaders") & S3E_KEY_STATE_PRESSED )
    {
        // The IW_GX_DISABLE_CUSTOM_SHADERS_F flag can be used to disable custom shading
        // In this mode, gx will use its default shaders to replicate gles 1.x mode.
        if (IwGxGetFlags() & IW_GX_DISABLE_CUSTOM_SHADERS_F )
        {
            IwGxClearFlags(IW_GX_DISABLE_CUSTOM_SHADERS_F);
        }
        else
        {
            IwGxSetFlags(IW_GX_DISABLE_CUSTOM_SHADERS_F);
        }
    }

    if (CheckButton("Increase refractiveIndex") & S3E_KEY_STATE_DOWN )
    {
        s_UniformValue += 0.1f;
    }
    if (CheckButton("Decrease refractiveIndex") & S3E_KEY_STATE_DOWN )
    {
        s_UniformValue -= 0.1f;
    }

    // Set the shader uniform parameter. Various overloads are provided for common Studio types.
    if (s_Param)
    {
        s_Param->Set(s_UniformValue);
    }
    // Build view matrix rotation from angles
    CIwMat rotX, rotY;
    rotX.SetRotX(s_Angles.x);
    rotY.SetRotY(s_Angles.y);
    s_ModelMatrix.CopyRot(rotY * rotX);

    // Update IwGx state time stamp
    IwGxTickUpdate();
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Start with no lighting
    IwGxLightingOff();

    //-------------------------------------------------------------------------
    // Render the Backdrop
    //-------------------------------------------------------------------------
    IwGxSetScreenSpaceSlot(-1);
    CIwMaterial* pMat = IW_GX_ALLOC_MATERIAL();
    pMat->SetTexture(s_Texture);
    pMat->SetColAmbient(0xffffffff);
    IwGxSetMaterial(pMat);
    CIwSVec2 wh((int16)IwGxGetScreenWidth(), (int16)IwGxGetScreenHeight());
    IwGxDrawRectScreenSpace(&CIwSVec2::g_Zero, &wh);
    IwGxSetScreenSpaceSlot(0);

    //-------------------------------------------------------------------------
    // Render the model
    //-------------------------------------------------------------------------
    IwGxSetModelMatrix(&s_ModelMatrix);
    s_Model->Render();

    // Paint the cursor keys buttons and prompt text
    IwGxPrintString(2, IwGxGetScreenHeight() - 60, "Rotate Model: Arrow Keys");
    RenderCursorskeys();

    if (IwGxGetHWType() != IW_GX_HWTYPE_GL2 )
    {
        IwGxPrintString(2, IwGxGetScreenHeight() - 90, "Select Configuration->GL and set the GLES API to gles 2.0 to see the shaders added to this model.", true);
    }

    // End drawing
    IwGxFlush();

    // Swap buffers
    IwGxSwapBuffers();
}
