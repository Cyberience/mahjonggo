/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
//-----------------------------------------------------------------------------
// ExampleIwGraphicsModel2
//-----------------------------------------------------------------------------

/**
 * @page ExampleLegacyIwGraphicsModelTwo IwGraphics Model Two Example (Legacy)
 *
 * This example demonstrates the loading of multiple model resources,
 * and rendering of multiple models, with lighting options.
 *
 * The main classes used to achieve this are:
 *  - CIwModel
 *  - CIwResGroup
 *
 * The main functions used to achieve this are:
 *  - IwGraphicsInit()
 *  - CIwResManager::LoadGroup()
 *  - CIwResManager::GetGroupNamed()
 *  - CIwResManager::GetResNamed()
 *  - CIwModel::Render()
 *  - IwGraphicsTerminate()
 *
 * The lighting is configured differently prior to the rendering of each cube:
 *  - Cube 0:\n
 *         Emissive = true\n
 *         Ambient = false\n
 *         Diffuse = false\n
 *
 *     Therefore cube 0 is applied with the material emissive colour (green).
 *
 *  - Cube 1:\n
 *         Emissive = false\n
 *         Ambient = false\n
 *         Diffuse = false\n
 *
 *     Therefore cube 1 is applied with the material ambient response (grey) as colour, unmodified by scene ambient lighting.
 *
 *  - Cube 2:\n
 *         Emissive = false\n
 *         Ambient = true\n
 *         Diffuse = false\n
 *
 *     Therefore cube 2 is applied with the material ambient response, modified by scene ambient colour (red).
 *
 *  - Cube 3:\n
 *          Emissive = false\n
 *          Ambient = true\n
 *          Diffuse = true\n
 *
 *          Therefore cube 3 is the same as Cube 2, but applied with additional white diffuse lighting.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwGraphicsModel2Image.png
 *
 * @note For more information on the loading of meshes from GEO files, see @ref
 * loadingofmodels "Loading of Models".
 *
 * @include IwGraphicsModel2.cpp
 */

#include "IwGraphics.h"
#include "IwGxPrint.h"
#include "IwModel.h"
#include "IwModelBuilder.h"
#include "IwResManager.h"
#include "s3eKeyboard.h"
#include "ExamplesMain.h"

// Angles
CIwSVec3    s_Angles;

// Local matrix
CIwMat      s_ModelMatrix;

// Model resources
CIwModel*   s_Models[2];

// Timer
uint32      s_Timer;

//--------------------------------------------------------------------------------
CIwMenu* DebugCreateMainMenu()
{
    IW_CALLSTACK("DebugCreateMainMenu")

    CIwMenu* pMenu = new CIwMenu;

#ifdef IW_DEBUG
    pMenu->AddItem(new CIwMenuItemResManager);
    pMenu->AddItem(new CIwMenuItemGraphics);
#endif
    return pMenu;
}
//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    IwGxInit();
    IwGraphicsInit();

    // Set screen clear colour
    IwGxSetColClear(0x40, 0x40, 0x40, 0x00);

    // Set field of view
    IwGxSetPerspMul(0xa0);

    // Set near and far planes
    IwGxSetFarZNearZ(0x400, 0x10);

    // Initialise angles
    s_Angles = CIwSVec3(0, 0x800, 0);

    // Parse the GROUP file, which will load the model, its materials and textures
    IwGetResManager()->LoadGroup("Model2.group");

    // Get a pointer to the named group
    CIwResGroup* pGroup = IwGetResManager()->GetGroupNamed("Example");

    // Get and store pointer to the model resources
    s_Models[0] = (CIwModel*)pGroup->GetResNamed("FunkyVic", IW_GRAPHICS_RESTYPE_MODEL);
    s_Models[1] = (CIwModel*)pGroup->GetResNamed("Test01", IW_GRAPHICS_RESTYPE_MODEL);

    // Set up the view matrix
    CIwMat view;
    view.SetRotZ(0x800);
    view.t.y =  0x80;
    view.t.z = -0x180;
    IwGxSetViewMatrix(&view);

    //-------------------------------------------------------------------------
    // Set up scene lighting
    //-------------------------------------------------------------------------
    // Set single ambient light
    IwGxSetLightType(0, IW_GX_LIGHT_AMBIENT);
    CIwColour colA = {0xff, 0x00, 0x00, 0xff};
    IwGxSetLightCol(0, &colA);

    // Set single diffuse light
    IwGxSetLightType(1, IW_GX_LIGHT_DIFFUSE);
    CIwColour colD = {0xff, 0xff, 0xff, 0xff};
    IwGxSetLightCol(1, &colD);
    CIwSVec3 dd(0x93c, 0x93c, 0x93c);
    IwGxSetLightDirn(1, &dd);

    // Set up the menu manager
    new CIwMenuManager;
    IwGetMenuManager()->SetTextCallback(IwGxPrintMenuCallback);
    IwGetMenuManager()->SetMainMenuFn(DebugCreateMainMenu);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    delete IwGetMenuManager();

    // Terminate
    IwGraphicsTerminate();
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // Reset metrics
#ifdef IW_DEBUG
    IwGraphicsMetricsReset();
    IwGxMetricsReset();
#endif

    // Update angles from the arrow keys
    if (CheckCursorState() == EXCURSOR_LEFT )
        s_Angles.y -= 0x40;
    if (CheckCursorState() == EXCURSOR_RIGHT )
        s_Angles.y += 0x40;
    if (CheckCursorState() == EXCURSOR_UP )
        s_Angles.x -= 0x40;
    if (CheckCursorState() == EXCURSOR_DOWN )
        s_Angles.x += 0x40;

    // Build model matrix rotation from angles
    CIwMat rotX, rotY;
    rotX.SetRotX(s_Angles.x);
    rotY.SetRotY(s_Angles.y);
    s_ModelMatrix.CopyRot(rotY * rotX);

    // Update timer
    s_Timer++;

    // Update menu manager
    IwGetMenuManager()->Update();

    // Update IwGx state time stamp
    IwGxTickUpdate();
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Start with no lighting
    IwGxLightingOff();

    //-------------------------------------------------------------------------
    // Render the main model
    //-------------------------------------------------------------------------
    IwGxSetModelMatrix(&s_ModelMatrix);
    s_Models[0]->Render();

    //-------------------------------------------------------------------------
    // Render the smaller models
    //-------------------------------------------------------------------------
    CIwSVec3 angles;
    angles.x = (int16)s_Timer * 0x20;
    angles.y = (int16)s_Timer * 0x28;
    angles.z = (int16)s_Timer * 0x30;
    CIwMat rotX, rotY, rotZ, m0;
    rotX.SetRotX(angles.x);
    rotY.SetRotY(angles.y);
    rotZ.SetRotZ(angles.z);
    m0.CopyRot(rotZ * rotY * rotX);

    const int32 RADIUS = 0xc0;
    for (uint32 m = 0; m < 4; m++)
    {
        m0.t.y = 0;
        m0.t.x = IW_FIXED_MUL(RADIUS, IW_GEOM_COS((m * 0x400) + (s_Timer << 5)));
        m0.t.z = IW_FIXED_MUL(RADIUS, IW_GEOM_SIN((m * 0x400) + (s_Timer << 5)));

        IwGxSetModelMatrix(&m0);

        /* Configure lighting differently for each cube:
        Cube 0:
            Emissive = true
            Ambient = false
            Diffuse = false
            ...so cube gets material emissive colour (green)

        Cube 1:
            Emissive = false
            Ambient = false
            Diffuse = false
            ...so cube gets material ambient response (grey) as colour, unmodified by scene ambient

        Cube 2:
            Emissive = false
            Ambient = true
            Diffuse = false
            ...so cube gets material ambient response, modified by scene ambient colour (red)

        Cube 3:
            Emissive = false
            Ambient = true
            Diffuse = true
            ...so cube is same as Cube 2, but with additional white diffuse
        */

        IwGxLightingEmissive(m == 0);
        IwGxLightingAmbient(m > 1);
        IwGxLightingDiffuse(m > 2);
        s_Models[1]->Render();
    }

    // Paint the cursor keys buttons
    IwGxPrintString(2, IwGxGetScreenHeight() - 60, "Rotate Model: Arrow Keys");
    RenderCursorskeys();

    // End drawing
    IwGxFlush();

    // Render menu manager
    IwGetMenuManager()->Render();
    IwGxFlush();

    // Swap buffers
    IwGxSwapBuffers();
}
