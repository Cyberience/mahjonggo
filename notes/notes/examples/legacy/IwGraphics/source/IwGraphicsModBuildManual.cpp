/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
//-----------------------------------------------------------------------------
// ExampleIwGraphicsModelBuilderManual
//-----------------------------------------------------------------------------

/**
 * @page ExampleLegacyIwGraphicsManualModelBuilder IwGraphics Manual Model Builder Example (Legacy)
 *
 * This example demonstrates manual creation of a model within the code. Also
 * manual creation and serialisation of a resource group containing the model
 * and all its required resources.
 *
 * The main classes used to achieve this are:
 *  - CIwModel
 *  - CIwModelBuilder
 *  - CIwFace
 *
 * The main functions used to achieve this are:
 *  - IwGraphicsInit()
 *  - CIwModelBuilder::Reset()
 *  - CIwModelBuilder::AddVert()
 *  - CIwModelBuilder::AddNorm()
 *  - CIwModelBuilder::AddUV()
 *  - CIwModelBuilder::AddCol()
 *  - CIwModelBuilder::SetMaterial()
 *  - CIwModelBuilder::AddFace()
 *  - CIwModelBuilder::CreateModel()
 *
 * A CIwModel resource is usually created by parsing a GEO file. However, it is
 * also possible to create models "manually" from within the code. The user
 * adds vertex, normal, colour and UV coords to the CIwModelBuilder singleton.
 * They then add face (triangle or quadrilateral) and material information.
 * Once the CIwModelBuilder has a complete description of the mesh, the user
 * calls CreateModel() to return a fully-formed CIwModel instance.
 *
 * The CIwModelBuilder singleton is only available in Debug configurations.
 * This is because it contains a fairly large amount of code for manipulating
 * floating point data types, processing lists, etc. Creating the CIwModel
 * instance is also a time-consuming process, due to the amount of "n-squared"
 * type list processing involved.
 *
 * For this reason, if you wish to manually create models within the code, they
 * must be serialised to a file which is then loaded in Release configurations.
 * This example also illustrates one way of doing that.
 *
 * In this example, we create a simple cube. Three sides have a single texture
 * mapped continuously. Two sides have another texture face-mapped. The final
 * side is untextured but uses a colour stream to create gouraud shading
 * between four colours.
 *
 * In order for our example to create the model the vertices, normals and UVs
 * are first declared.  After this they are added to the model using the
 * AddVert(), AddNorm() and AddUV() functions, respectively.  The AddCol()
 * function is then used to add the colours to the model.  Six CIwFace objects
 * are declared to hold the face information for each face of the cube. For
 * each face, four Point objects are declared, and these are attributed the ID
 * of the relevant Vert, Normal and UV. The CreateModel() function is then
 * called to build the model.
 *
 * @note Having very few faces per material is quite inefficient for rendering
 * purposes; we are doing it for illustration only.
 *
 * Use the up/down/left/right keys to rotate the cube.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwGraphicsModBuildManualImage.png
 *
 * @note For more information on the loading of meshes, see @ref
 * loadingofmodels "Loading of Models".
 *
 * @include IwGraphicsModBuildManual.cpp
 */

#include "IwGraphics.h"
#include "IwGx.h"
#include "IwGxPrint.h"
#include "IwModel.h"
#include "IwModelBuilder.h"
#include "IwResManager.h"
#include "IwTextParserITX.h"
#include "s3eKeyboard.h"
#include "ExamplesMain.h"

// Angles
CIwSVec3    s_Angles;

// Local matrix
CIwMat      s_ModelMatrix;

// Model resource
CIwModel*   s_Model;

// Vertices to add
const float s = 200.0f;
CIwFVec3    s_Verts[8] =
{
    CIwFVec3(-s, -s,  s),   // this will become "vertex 0" of the model builder
    CIwFVec3( s, -s,  s),   // this will become "vertex 1" of the model builder, etc.
    CIwFVec3( s,  s,  s),
    CIwFVec3(-s,  s,  s),
    CIwFVec3(-s, -s, -s),
    CIwFVec3( s, -s, -s),
    CIwFVec3( s,  s, -s),
    CIwFVec3(-s,  s, -s),
};

// Normals to add
CIwFVec3    s_Norms[6] =
{
    CIwFVec3(-1.0, 0, 0),   // this will become "normal 0" of the model builder, etc.
    CIwFVec3( 1.0, 0, 0),
    CIwFVec3(0, -1.0, 0),
    CIwFVec3(0,  1.0, 0),
    CIwFVec3(0, 0,  1.0),
    CIwFVec3(0, 0, -1.0),
};

// UVs to add
CIwFVec2    s_UVs[8] =
{
    CIwFVec2(0, 0),         // this will become "UV 0" of the model builder, etc.
    CIwFVec2(0.33f, 0),
    CIwFVec2(0.66f, 0),
    CIwFVec2(1, 0),
    CIwFVec2(0, 1),
    CIwFVec2(0.33f, 1),
    CIwFVec2(0.66f, 1),
    CIwFVec2(1, 1),
};

// Textures
CIwTexture* s_Tex0;
CIwTexture* s_Tex1;

// Materials
CIwMaterial* s_MatTex0;
CIwMaterial* s_MatTex1;
CIwMaterial* s_Mat2;

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    IwGxInit();
    IwGraphicsInit();

    // Set screen clear colour
    IwGxSetColClear(0x00, 0x00, 0xff, 0x00);

    // Set field of view
    IwGxSetPerspMul(0xa0);

    // Set near and far planes
    IwGxSetFarZNearZ(0x2000, 0x100);

    // Initialise angles
    s_Angles = CIwSVec3(0x200, 0x800, 0);

    // Set up the view matrix
    CIwMat view;
    view.SetRotZ(0x800);
    view.t.z = -600;
    IwGxSetViewMatrix(&view);

    //-------------------------------------------------------------------------
    // Set up scene lighting
    //-------------------------------------------------------------------------
    // Set single ambient light
    IwGxSetLightType(0, IW_GX_LIGHT_AMBIENT);
    CIwColour colA = {0x40, 0x40, 0x40, 0xff};
    IwGxSetLightCol(0, &colA);

    // Set single diffuse light
    IwGxSetLightType(1, IW_GX_LIGHT_DIFFUSE);
    CIwColour colD = {0xc0, 0xc0, 0xc0, 0xff};
    IwGxSetLightCol(1, &colD);
    CIwSVec3 dd(0x93c, 0x93c, 0x93c);
    IwGxSetLightDirn(1, &dd);

    //-------------------------------------------------------------------------
    // Construct the model
    //-------------------------------------------------------------------------
#ifndef IW_BUILD_RESOURCES
    // In this case, we simply load the existing group.bin, which contains the model, materials and textures
    IwGetResManager()->SetMode(CIwResManager::MODE_LOAD);
    IwGetResManager()->LoadGroup("ModBuildManual.group");
    s_Model = (CIwModel*)IwGetResManager()->GetCurrentGroup()->GetResNamed("cube", IW_GRAPHICS_RESTYPE_MODEL);
#else
    uint32 n;

    // First, reset the builder
    IwGetModelBuilder()->Reset();

    // Add vertices
    for (n = 0; n < 8; n++)
        IwGetModelBuilder()->AddVert(s_Verts[n]);

    // Add normals
    for (n = 0; n < 6; n++)
        IwGetModelBuilder()->AddNorm(s_Norms[n]);

    // Add UVs
    for (n = 0; n < 8; n++)
        IwGetModelBuilder()->AddUV(s_UVs[n]);

    // Add colours
    IwGetModelBuilder()->AddCol(IwGxGetColFixed(IW_GX_COLOUR_RED));
    IwGetModelBuilder()->AddCol(IwGxGetColFixed(IW_GX_COLOUR_GREEN));
    IwGetModelBuilder()->AddCol(IwGxGetColFixed(IW_GX_COLOUR_BLUE));
    IwGetModelBuilder()->AddCol(IwGxGetColFixed(IW_GX_COLOUR_WHITE));

    // Load the textures
    s_Tex0 = new CIwTexture;
    s_Tex0->LoadFromFile("textures/testTexture_4bit.bmp");
    s_Tex0->SetName("Tex0");
    s_Tex1 = new CIwTexture;
    s_Tex1->LoadFromFile("textures/testTexture_8bit.bmp");
    s_Tex1->SetName("Tex1");

    // Set up the materials
    s_MatTex0 = new CIwMaterial;
    s_MatTex0->SetName("MatTex0");
    s_MatTex0->SetTexture(s_Tex0);

    s_MatTex1 = new CIwMaterial;
    s_MatTex1->SetName("MatTex1");
    s_MatTex1->SetTexture(s_Tex1);

    s_Mat2 = new CIwMaterial;
    s_Mat2->SetName("Mat2");

    // Set up the faces. These index into the vertex, normal, etc. streams. Each face uses a single material.
    CIwFace f0, f1, f2, f3, f4, f5;

    IwGetModelBuilder()->SetMaterial(s_MatTex0);
    f0.m_NumPoints = 4;
    f0.m_Points[0].m_VertID     = 0;
    f0.m_Points[0].m_NormID     = 0;
    f0.m_Points[0].m_UVID[0]    = 0;
    f0.m_Points[1].m_VertID     = 3;
    f0.m_Points[1].m_NormID     = 0;
    f0.m_Points[1].m_UVID[0]    = 4;
    f0.m_Points[2].m_VertID     = 7;
    f0.m_Points[2].m_NormID     = 0;
    f0.m_Points[2].m_UVID[0]    = 5;
    f0.m_Points[3].m_VertID     = 4;
    f0.m_Points[3].m_NormID     = 0;
    f0.m_Points[3].m_UVID[0]    = 1;
    IwGetModelBuilder()->AddFace(&f0);

    f1.m_NumPoints = 4;
    f1.m_Points[0].m_VertID     = 4;
    f1.m_Points[0].m_NormID     = 5;
    f1.m_Points[0].m_UVID[0]    = 1;
    f1.m_Points[1].m_VertID     = 7;
    f1.m_Points[1].m_NormID     = 5;
    f1.m_Points[1].m_UVID[0]    = 5;
    f1.m_Points[2].m_VertID     = 6;
    f1.m_Points[2].m_NormID     = 5;
    f1.m_Points[2].m_UVID[0]    = 6;
    f1.m_Points[3].m_VertID     = 5;
    f1.m_Points[3].m_NormID     = 5;
    f1.m_Points[3].m_UVID[0]    = 2;
    IwGetModelBuilder()->AddFace(&f1);

    f2.m_NumPoints = 4;
    f2.m_Points[0].m_VertID     = 5;
    f2.m_Points[0].m_NormID     = 1;
    f2.m_Points[0].m_UVID[0]    = 2;
    f2.m_Points[1].m_VertID     = 6;
    f2.m_Points[1].m_NormID     = 1;
    f2.m_Points[1].m_UVID[0]    = 6;
    f2.m_Points[2].m_VertID     = 2;
    f2.m_Points[2].m_NormID     = 1;
    f2.m_Points[2].m_UVID[0]    = 7;
    f2.m_Points[3].m_VertID     = 1;
    f2.m_Points[3].m_NormID     = 1;
    f2.m_Points[3].m_UVID[0]    = 3;
    IwGetModelBuilder()->AddFace(&f2);

    IwGetModelBuilder()->SetMaterial(s_MatTex1);
    f3.m_NumPoints = 4;
    f3.m_Points[0].m_VertID     = 0;
    f3.m_Points[0].m_NormID     = 2;
    f3.m_Points[0].m_UVID[0]    = 0;
    f3.m_Points[1].m_VertID     = 4;
    f3.m_Points[1].m_NormID     = 2;
    f3.m_Points[1].m_UVID[0]    = 4;
    f3.m_Points[2].m_VertID     = 5;
    f3.m_Points[2].m_NormID     = 2;
    f3.m_Points[2].m_UVID[0]    = 7;
    f3.m_Points[3].m_VertID     = 1;
    f3.m_Points[3].m_NormID     = 2;
    f3.m_Points[3].m_UVID[0]    = 3;
    IwGetModelBuilder()->AddFace(&f3);

    f4.m_NumPoints = 4;
    f4.m_Points[0].m_VertID     = 7;
    f4.m_Points[0].m_NormID     = 3;
    f4.m_Points[0].m_UVID[0]    = 0;
    f4.m_Points[1].m_VertID     = 3;
    f4.m_Points[1].m_NormID     = 3;
    f4.m_Points[1].m_UVID[0]    = 4;
    f4.m_Points[2].m_VertID     = 2;
    f4.m_Points[2].m_NormID     = 3;
    f4.m_Points[2].m_UVID[0]    = 7;
    f4.m_Points[3].m_VertID     = 6;
    f4.m_Points[3].m_NormID     = 3;
    f4.m_Points[3].m_UVID[0]    = 3;
    IwGetModelBuilder()->AddFace(&f4);

    IwGetModelBuilder()->SetMaterial(s_Mat2);
    f5.m_NumPoints = 4;
    f5.m_Points[0].m_VertID     = 1;
    f5.m_Points[0].m_NormID     = 4;
    f5.m_Points[0].m_ColID[0]   = 0;
    f5.m_Points[0].m_UVID[0]    = 0;
    f5.m_Points[1].m_VertID     = 2;
    f5.m_Points[1].m_NormID     = 4;
    f5.m_Points[1].m_ColID[0]       = 3;
    f5.m_Points[1].m_UVID[0]    = 6;
    f5.m_Points[2].m_VertID     = 3;
    f5.m_Points[2].m_NormID     = 4;
    f5.m_Points[2].m_ColID[0]       = 2;
    f5.m_Points[2].m_UVID[0]    = 7;
    f5.m_Points[3].m_VertID     = 0;
    f5.m_Points[3].m_NormID     = 4;
    f5.m_Points[3].m_ColID[0]       = 1;
    f5.m_Points[3].m_UVID[0]    = 1;
    IwGetModelBuilder()->AddFace(&f5);

    // Create the model
    s_Model = IwGetModelBuilder()->CreateModel("cube");

    // Create a resource group and serialise it to disk
    CIwResGroup* pGroup = new CIwResGroup;
    pGroup->SetName("CubeAndStuff");

    // Note that resources must be added in dependency order; i.e. model depends on material depends on texture
    pGroup->AddRes(IW_GX_RESTYPE_TEXTURE, s_Tex0);
    pGroup->AddRes(IW_GX_RESTYPE_TEXTURE, s_Tex1);
    pGroup->AddRes(IW_GX_RESTYPE_MATERIAL, s_MatTex0);
    pGroup->AddRes(IW_GX_RESTYPE_MATERIAL, s_MatTex1);
    pGroup->AddRes(IW_GX_RESTYPE_MATERIAL, s_Mat2);
    pGroup->AddRes(IW_GRAPHICS_RESTYPE_MODEL, s_Model);

    IwSerialiseOpen("ModBuildManual.group.bin", false);
    pGroup->Write();
    IwSerialiseClose();
    IwGetResManager()->AddGroup(pGroup);

    // Note how textures must be uploaded AFTER serialising out to disk
    s_Tex0->Upload();
    s_Tex1->Upload();
#endif
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // We don't explicitly have to free any resources (CIwTexture, CIwMaterial, CIwModel) as they have all been
    // added to the resource manager, which is automatically destroyed by IwGraphicsTerminate, and will destroy the contents
    // of any resource groups it knows about.

    // Terminate
    IwGraphicsTerminate();
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // Reset metrics
#ifdef IW_DEBUG
    IwGraphicsMetricsReset();
    IwGxMetricsReset();
#endif
    // Update angles from the arrow keys
    if (CheckCursorState() == EXCURSOR_LEFT )
        s_Angles.y -= 0x40;
    if (CheckCursorState() == EXCURSOR_RIGHT )
        s_Angles.y += 0x40;
    if (CheckCursorState() == EXCURSOR_UP )
        s_Angles.x -= 0x40;
    if (CheckCursorState() == EXCURSOR_DOWN )
        s_Angles.x += 0x40;

    // Build model matrix rotation from angles
    CIwMat rotX, rotY;
    rotX.SetRotX(s_Angles.x);
    rotY.SetRotY(s_Angles.y);
    s_ModelMatrix.CopyRot(rotY * rotX);

    // Update IwGx state time stamp
    IwGxTickUpdate();
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Start with full lighting
    IwGxLightingOn();

    //-------------------------------------------------------------------------
    // Render the main model
    //-------------------------------------------------------------------------
    IwGxSetModelMatrix(&s_ModelMatrix);
    s_Model->Render();

    // Paint the cursor keys buttons
    IwGxPrintString(2, IwGxGetScreenHeight() - 60, "Rotate Model: Arrow Keys");
    RenderCursorskeys();

    // End drawing
    IwGxFlush();

    // Swap buffers
    IwGxSwapBuffers();
}
