/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleLegacyIwAnimAnimationPlayback IwAnim Animation Playback Example (Legacy)
 *
 * The following example demonstrates how to play animations
 * at any speed, forward and backwards, looping or one-shot. Also shows
 * pause/restart.
 *
 * The main classes used to achieve this are:
 *  - CIwAnimPlayer
 *
 * The main functions used to achieve this are:
 *  - CIwAnimPlayer::PlayAnim()
 *
 * This example builds on Animation Rendering Example by adding further
 * controls to set the speed of animation playback. The speed of the
 * animation playback is set using the second parameter of the PlayAnim()
 * function, and it can be thrown into reverse by negating the value
 * passed in.
 *
 * @note For more information on adjusting the playing of an animation,
 * see the Animation Player section of the IwAnim API Documentation.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwAnimPlaybackImage.png
 *
 * @include IwAnimPlayback.cpp
 */
#include "IwAnim.h"
#include "IwAnimPlayer.h"
#include "IwAnimSkel.h"
#include "IwAnimSkin.h"
#include "IwGraphics.h"
#include "IwGxPrint.h"
#include "IwModelBuilder.h"
#include "IwResManager.h"
#include "IwTextParserITX.h"
#include "s3eKeyboard.h"
#include "ExamplesMain.h"

// Angles
CIwSVec3    s_Angles;

// Local matrix
CIwMat      s_ModelMatrix;

// Resources
CIwModel*       s_Model;
CIwAnim*        s_Anim;
CIwAnimSkel*    s_Skel;
CIwAnimSkin*    s_Skin;

// Animation player
CIwAnimPlayer*  s_Player;

int32   s_Speed     = IW_GEOM_ONE;
bool    s_Reverse   = false;
bool    s_Looping   = true;

void CreateButtonsUI(int w, int h)
{
    // Create the UI layout
    AddButton("Zoom in: 1", 0, 70, 70, 30, s3eKey1);
    AddButton("Zoom out: 2", 0, 110, 70, 30, s3eKey2);

    AddButton("Speed -: 4", w - 70, 70, 70, 30, s3eKey4);
    AddButton("Speed +: 5", w - 70, 110, 70, 30, s3eKey5);

    AddButton("Reverse: 3", 0, 150, 70, 30, s3eKey3);

    AddButton("Looping: 6",  w - 70, 150, 70, 30, s3eKey6);
}

void SurfaceChangedCallback()
{
    DeleteButtons();
    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    IwGxRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
    IwGxInit();
    IwGraphicsInit();
    IwAnimInit();

    // Set screen clear colour
    IwGxSetColClear(0x40, 0x40, 0x40, 0x00);

    // Set field of view
    IwGxSetPerspMul(IwGxGetScreenWidth() / 2);

    // Set near and far planes
    IwGxSetFarZNearZ(0x4010, 0x10);

    // Initialise angles
    s_Angles = CIwSVec3(0, 0xc00, 0x800);

    // Initialise model position
    s_ModelMatrix.t = CIwVec3(0, 0x200, 0x300);

#ifdef IW_BUILD_RESOURCES
    //Initialise resource templates - This scales Eva up so animation can interpolate smoothly
    if (IwGetResManager()->m_Flags & CIwResManager::BUILDRES_F )
        IwGetTextParserITX()->ParseFile("resource_template.itx");
#endif

    // Parse the GROUP file, which will load the resources
    IwGetResManager()->LoadGroup("IwAnimPlayback.group");

    // Get a pointer to the named group
    CIwResGroup* pGroup = IwGetResManager()->GetGroupNamed("ExampleIwAnimPlayback");

    // Get and store pointer to the resources
    s_Model = (CIwModel*)pGroup->GetResNamed("combatEva", IW_GRAPHICS_RESTYPE_MODEL);
    s_Skin  = (CIwAnimSkin*)pGroup->GetResNamed("combatEva", IW_ANIM_RESTYPE_SKIN);
    s_Skel  = (CIwAnimSkel*)pGroup->GetResNamed("combatEva_rig", IW_ANIM_RESTYPE_SKELETON);
    s_Anim  = (CIwAnim*)pGroup->GetResNamed("LfStance", IW_ANIM_RESTYPE_ANIMATION);

    // Create animation player
    s_Player = new CIwAnimPlayer;
    s_Player->SetSkel(s_Skel);
    s_Player->PlayAnim(s_Anim, IW_GEOM_ONE, CIwAnimBlendSource::LOOPING_F);

    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // Clear up resources
    delete s_Player;

    // Terminate IwAnim
    IwAnimTerminate();
    IwGraphicsTerminate();
    IwGxTerminate();
    IwGxUnRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // Update angles from the arrow keys
    if (CheckCursorState() == EXCURSOR_LEFT )
        s_Angles.y -= 0x40;
    if (CheckCursorState() == EXCURSOR_RIGHT )
        s_Angles.y += 0x40;
    if (CheckCursorState() == EXCURSOR_UP )
        s_Angles.x -= 0x40;
    if (CheckCursorState() == EXCURSOR_DOWN )
        s_Angles.x += 0x40;

    // Build view matrix rotation from angles
    CIwMat rotX, rotY, rotZ;
    rotX.SetRotX(s_Angles.x);
    rotY.SetRotY(s_Angles.y);
    rotZ.SetRotZ(0x800);
    s_ModelMatrix.CopyRot(rotZ * rotX * rotY);

    // Forward/back
    CIwVec3 ofs(0, 0, 0x10);
    if (CheckButton("Zoom out: 2") & S3E_KEY_STATE_DOWN )
        s_ModelMatrix.t += ofs;
    if (CheckButton("Zoom in: 1") & S3E_KEY_STATE_DOWN )
        s_ModelMatrix.t -= ofs;

    // Update speed
    bool replay = false;

    if (CheckButton("Speed -: 4") & S3E_KEY_STATE_DOWN )
    {
        s_Speed = MAX(0x100, s_Speed >> 1);
        replay = true;
    }
    else
    if (CheckButton("Speed +: 5") & S3E_KEY_STATE_DOWN )
    {
        s_Speed = MIN(0x4000, s_Speed << 1);
        replay = true;
    }

    // Toggle backwards status
    if (CheckButton("Reverse: 3") & S3E_KEY_STATE_PRESSED )
    {
        s_Reverse = !s_Reverse;
        replay = true;
    }

    // Toggle looping status
    if (CheckButton("Looping: 6") & S3E_KEY_STATE_PRESSED )
    {
        s_Looping = !s_Looping;
        replay = true;
    }

    if (replay)
        s_Player->PlayAnim(s_Anim, s_Speed * ((s_Reverse == false) ? 1 : -1), (s_Looping == true ? CIwAnimBlendSource::LOOPING_F : 0) | CIwAnimBlendSource::RESET_IF_SAME_F);

    // Update animation player
    s_Player->Update(0x1000 / 30);

    // Update IwGx state time stamp
    IwGxTickUpdate();
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Start with no lighting
    IwGxLightingOff();

    //-------------------------------------------------------------------------
    // Render the model
    //-------------------------------------------------------------------------
    IwGxSetModelMatrix(&s_ModelMatrix);

    IwAnimSetSkelContext(s_Player->GetSkel());
    IwAnimSetSkinContext(s_Skin);
    s_Model->Render();

    // Tidier to reset these
    IwAnimSetSkelContext(NULL);
    IwAnimSetSkinContext(NULL);

    // Debug display
    char line[80];
    sprintf(line, "Speed: %d.%02d", s_Speed / IW_GEOM_ONE, (s_Speed % IW_GEOM_ONE) * 100 / IW_GEOM_ONE);
    IwGxPrintString(2, 32, line);
    sprintf(line, "Reverse: %s", s_Reverse ? "true" : "false");
    IwGxPrintString(2, 40, line);
    sprintf(line, "Looping: %s", s_Looping ? "true" : "false");
    IwGxPrintString(2, 48, line);

    IwGxPrintString(2, IwGxGetScreenHeight() - 60, "Rotate Model: Arrow Keys");
    RenderCursorskeys();

    // Flush and swap
    IwGxFlush();
    IwGxSwapBuffers();
}
