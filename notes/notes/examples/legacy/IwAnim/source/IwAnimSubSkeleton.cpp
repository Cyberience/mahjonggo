/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleLegacyIwAnimSubSkeleton IwAnim SubSkeleton Example (Legacy)
 *
 * The following example demonstrates overlaying of sub-skeleton
 * animations.
 *
 * The main classes used to achieve this are:
 *  - CIwAnim
 *  - CIwAnimBlendSource
 *
 * The main functions used to achieve this are:
 *  - CIwResManager::GetGroupNamed()
 *  - CIwResGroup::GetResNamed()
 *  - CIwAnimPlayer::PlayAnim()
 *  - CIwAnimPlayer::PlaySubAnim()
 *  - CIwAnimPlayer::ClearSubAnim()
 *
 * This example builds on Animation Skeleton Example by adding further
 * controls to allow you to add a sub-skeleton animation to an animation
 * that is currently running.
 *
 * The example continuously blends between two animations and
 * allows you to add either of two sub-skeletons to those animations.
 * The controls are as follows:
 *  - Press "1" to add sub-skeleton animation anim0 to the
 *    current animation.
 *  - Press "2" to add sub-skeleton animation anim1 to the current
 *    animation.
 *  - Press "0" to remove any of the applied sub-skeleton animations
 *    from the current animation.
 *
 * Pressing either of "1" or "2" calls the PlaySubAnim() function,
 * which is similar to the PlayAnim function, but takes a sub-skeleton
 * animation. Pressing "0" calls the ClearSubAnim(), which halts the
 * sub-skeleton animation.
 *
 * @note For more information on overlaying sub-skeleton animations,
 * see the Smooth Transitions section of the <i>IwAnim API Documentation</i>.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwAnimSubSkeletonImage.png
 *
 * @include IwAnimSubSkeleton.cpp
 */

#include "IwAnim.h"
#include "IwAnimPlayer.h"
#include "IwAnimSkel.h"
#include "IwAnimSkin.h"
#include "IwGraphics.h"
#include "IwGxPrint.h"
#include "IwModelBuilder.h"
#include "IwResManager.h"
#include "IwTextParserITX.h"
#include "s3eKeyboard.h"
#include "ExamplesMain.h"

// Angles
CIwSVec3    s_Angles;

// Local matrix
CIwMat      s_ModelMatrix;

// Resources
CIwModel*       s_Model;
CIwAnim*        s_FullSkelAnims[2];
CIwAnim*        s_SubSkelAnims[2];
CIwAnimSkel*    s_Skel;
CIwAnimSkin*    s_Skin;

// Animation player
CIwAnimPlayer*  s_Player;

// Timer
uint32  s_Timer = 0;

void CreateButtonsUI(int w, int h)
{
    // Create the UI
    AddButton("Zoom in: 3", 0, 70, 70, 30, s3eKey3);
    AddButton("Zoom out: 6", 0, 110, 70, 30, s3eKey6);

    AddButton("Add Anim 1: 1", w - 90, 70, 90, 30, s3eKey1);
    AddButton("Add Anim 2: 2", w - 90, 110, 90, 30, s3eKey2);
    AddButton("Remove Anim: 0", w - 90, 150, 90, 30, s3eKey0);
}

void SurfaceChangedCallback()
{
    DeleteButtons();
    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}


//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    IwGxRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
    IwGxInit();
    IwGraphicsInit();
    IwAnimInit();

    // Set screen clear colour
    IwGxSetColClear(0x40, 0x40, 0x40, 0x00);

    // Set field of view
    IwGxSetPerspMul(IwGxGetScreenWidth() / 2);

    // Set near and far planes
    IwGxSetFarZNearZ(0x4010, 0x10);

    // Initialise angles
    s_Angles = CIwSVec3(0, -0x400, 0x800);

    // Initialise model position
    s_ModelMatrix.t = CIwVec3(0, 0x200, 0x300);

#ifdef IW_BUILD_RESOURCES
    //Initialise resource templates - This scales Eva up so animation can interpolate smoothly
    if (IwGetResManager()->m_Flags & CIwResManager::BUILDRES_F )
        IwGetTextParserITX()->ParseFile("resource_template.itx");
#endif

    // Parse the GROUP file, which will load the resources
    IwGetResManager()->LoadGroup("IwAnimSubSkeleton.group");

    // Get a pointer to the named group
    CIwResGroup* pGroup = IwGetResManager()->GetGroupNamed("ExampleIwAnimSubSkeleton");

    // Get and store pointer to the resources
    s_Model = (CIwModel*)pGroup->GetResNamed("combatEva", IW_GRAPHICS_RESTYPE_MODEL);
    s_Skin  = (CIwAnimSkin*)pGroup->GetResNamed("combatEva", IW_ANIM_RESTYPE_SKIN);
    s_Skel  = (CIwAnimSkel*)pGroup->GetResNamed("combatEva_rig", IW_ANIM_RESTYPE_SKELETON);

    s_FullSkelAnims[0]  = (CIwAnim*)pGroup->GetResNamed("LfStance", IW_ANIM_RESTYPE_ANIMATION);
    s_FullSkelAnims[1]  = (CIwAnim*)pGroup->GetResNamed("RtStance", IW_ANIM_RESTYPE_ANIMATION);
    s_SubSkelAnims[0]   = (CIwAnim*)pGroup->GetResNamed("SubAnimA", IW_ANIM_RESTYPE_ANIMATION);
    s_SubSkelAnims[1]   = (CIwAnim*)pGroup->GetResNamed("SubAnimB", IW_ANIM_RESTYPE_ANIMATION);

    // Create animation player
    s_Player = new CIwAnimPlayer;
    s_Player->SetSkel(s_Skel);

    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // Clear up resources
    delete s_Player;

    // Terminate IwAnim
    IwAnimTerminate();
    IwGraphicsTerminate();
    IwGxTerminate();
    IwGxUnRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // Update angles from the arrow keys
    if (CheckCursorState() == EXCURSOR_LEFT )
        s_Angles.y -= 0x40;
    if (CheckCursorState() == EXCURSOR_RIGHT )
        s_Angles.y += 0x40;
    if (CheckCursorState() == EXCURSOR_UP )
        s_Angles.x -= 0x40;
    if (CheckCursorState() == EXCURSOR_DOWN )
        s_Angles.x += 0x40;

    // Build view matrix rotation from angles
    CIwMat rotX, rotY, rotZ;
    rotX.SetRotX(s_Angles.x);
    rotY.SetRotY(s_Angles.y);
    rotZ.SetRotZ(0x800);
    s_ModelMatrix.CopyRot(rotZ * rotX * rotY);

    // Forward/back
    CIwVec3 ofs(0, 0, 0x10);
    if (CheckButton("Zoom out: 6") & S3E_KEY_STATE_DOWN)
        s_ModelMatrix.t += ofs;
    if (CheckButton("Zoom in: 3") & S3E_KEY_STATE_DOWN)
        s_ModelMatrix.t -= ofs;

    // Alternate between two full-skeleton anims, with a short blend duration
    if (!(s_Timer & 0x1f))
        s_Player->PlayAnim(s_FullSkelAnims[(s_Timer >> 5) & 0x1], IW_GEOM_ONE, CIwAnimBlendSource::LOOPING_F, 0x400);

    if (CheckButton("Add Anim 1: 1") & S3E_KEY_STATE_PRESSED)
    {
        // Pressed 1: blend in sub-skeleton anim 0, on sub-skeleton anim slot 0
        s_Player->PlaySubAnim(0, s_SubSkelAnims[0], IW_GEOM_ONE, 0, 0x800);
    }
    else
    if (CheckButton("Add Anim 2: 2") & S3E_KEY_STATE_PRESSED)
    {
        // Pressed 2: blend in sub-skeleton anim 1, on sub-skeleton anim slot 0
        s_Player->PlaySubAnim(0, s_SubSkelAnims[1], IW_GEOM_ONE, 0, 0x800);
    }
    else
    if (CheckButton("Remove Anim: 0") & S3E_KEY_STATE_PRESSED)
    {
        // Pressed 0: blend out any anim of sub-skeleton slot 0
        s_Player->ClearSubAnim(0, 0x800);
    }

    s_Timer++;

    // Update animation player
    s_Player->Update(0x1000 / 30);

    // Update IwGx state time stamp
    IwGxTickUpdate();
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Start with no lighting
    IwGxLightingOff();

    //-------------------------------------------------------------------------
    // Render the model
    //-------------------------------------------------------------------------
    IwGxSetModelMatrix(&s_ModelMatrix);

    IwAnimSetSkelContext(s_Player->GetSkel());
    IwAnimSetSkinContext(s_Skin);
    s_Model->Render();

    // Tidier to reset these
    IwAnimSetSkelContext(NULL);
    IwAnimSetSkinContext(NULL);

    // Debug display
    char line[80];
    sprintf(line, "Full-skel anim: %s", s_Player->GetSourceCurr()->GetAnim()->DebugGetName());
    IwGxPrintString(2, 42, line);
    sprintf(line, "Sub-skel anim:  %s", s_Player->GetSourceSub(0)->GetAnim() ? s_Player->GetSourceSub(0)->GetAnim()->DebugGetName() : "NONE");
    IwGxPrintString(2, 50, line);

    IwGxPrintString(2, IwGxGetScreenHeight() - 60, "Rotate Model: Arrow Keys");
    RenderCursorskeys();

    // Flush and swap
    IwGxFlush();
    IwGxSwapBuffers();
}
