/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
//-----------------------------------------------------------------------------
// IwGxCoordinateSpaces
//-----------------------------------------------------------------------------

/**
 * @page ExampleLegacyIwGxCoordinateSpaces IwGx Coordinate Spaces Example (Legacy)
 *
 * This example demonstrates how to draw in all available coordinate spaces.
 *
 * The functions required to achieve this are:
 *  - IwGxSetVertStreamScreenSpace()
 *  - IwGxSetVertStreamViewSpace()
 *  - IwGxSetModelMatrix()
 *  - IwGxSetVertStream()
 *  - IwGxSetVertStreamWorldSpace()
 *  - IwGxDrawPrims()
 *
 * You can call IwGxDrawPrims() multiple times, changing only transform
 * (model matrix), materials, or some streams in between. However, if you
 * change the transform, you must call IwGxSetVertStream() again, even if the
 * stream has not changed.
 *
 * The example application demonstrates how to submit vertex streams in
 * screenspace, viewspace, modelspace and worldspace, and how each coordinate
 * space is affected by the view and model matrices.
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwGxCoordinateSpacesImage.png
 *
 * @note For more information on the various coordinate spaces, see the
 * @ref transform "Transform" section of <i>IwGx API Documentation</i>.
 *
 * @include IwGxCoordinateSpaces.cpp
 */

#include "IwGx.h"
#include "IwGxPrint.h"
#include "IwMaterial.h"
#include "IwMenu.h"

// Vertex data
const int16 s = 0x80;
CIwSVec3    s_Verts[8] =
{
    CIwSVec3(-s, -s, -s),
    CIwSVec3( s, -s, -s),
    CIwSVec3( s,  s, -s),
    CIwSVec3(-s,  s, -s),
    CIwSVec3(-s, -s,  s),
    CIwSVec3( s, -s,  s),
    CIwSVec3( s,  s,  s),
    CIwSVec3(-s,  s,  s),
};

// Normal data
const int16 n = 0x93c;
CIwSVec3    s_Norms[8] =
{
    CIwSVec3(-n, -n, -n),
    CIwSVec3( n, -n, -n),
    CIwSVec3( n,  n, -n),
    CIwSVec3(-n,  n, -n),
    CIwSVec3(-n, -n,  n),
    CIwSVec3( n, -n,  n),
    CIwSVec3( n,  n,  n),
    CIwSVec3(-n,  n,  n),
};

// Vertex colours
CIwColour s_Cols[8] =
{
    {0xff, 0x00, 0x00, 0x00},
    {0xff, 0xff, 0x00, 0x00},
    {0x00, 0xff, 0xff, 0x00},
    {0x00, 0x00, 0xff, 0x00},
    {0xff, 0x00, 0xff, 0x00},
    {0xff, 0x00, 0x00, 0x00},
    {0xff, 0xff, 0x00, 0x00},
    {0x00, 0xff, 0xff, 0x00},
};

// Index stream
uint16      s_TriStrip[22] =
{
    1, 2, 5, 6, 4, 7, 0, 3, 1, 2,
    2, 4, // degenerates
    4, 0, 5, 1,
    1, 3, // degenerates
    3, 7, 2, 6,
};

// Angles
CIwSVec3     s_Angles;

// Matrices
CIwMat       s_ModelMatrix;
CIwMat       s_ViewMatrix;

// Materials
CIwMaterial *s_Material;

// Update timer
uint32       s_Timer = 0;

//--------------------------------------------------------------------------------
CIwMenu* DebugCreateMainMenu()
{
    IW_CALLSTACK("DebugCreateMainMenu")

    CIwMenu* pMenu = new CIwMenu;

#ifdef IW_DEBUG
    pMenu->AddItem(new CIwMenuItemGx);
#endif
    return pMenu;
}
//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    IwGxInit();

    // Set field of view
    IwGxSetPerspMul(0xa0);

    // Set near and far planes
    IwGxSetFarZNearZ(0x400, 0x10);

    // Initialise angles
    s_Angles = CIwSVec3::g_Zero;

    // Initialise view matrix - identity rotation, and slightly offset along the X and Y axes.
    s_ViewMatrix.SetIdentity();
    s_ViewMatrix.t.x = -0xc0;
    s_ViewMatrix.t.y = -0xc0;

    // Initialise model matrix - slightly offset along the X axis.
    s_ModelMatrix.SetIdentity();
    s_ModelMatrix.t.x = -0x180;
    s_ModelMatrix.t.y =  0x0;

    // Initialise material
    s_Material = new CIwMaterial;

    // Set up the menu manager
    new CIwMenuManager;
    IwGetMenuManager()->SetTextCallback(IwGxPrintMenuCallback);
    IwGetMenuManager()->SetMainMenuFn(DebugCreateMainMenu);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    delete s_Material;
    delete IwGetMenuManager();

    // Terminate
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // Update angles
    s_Angles.x += 0x10;
    s_Angles.y += 0x24;
    s_Angles.z += 0x38;

    // Build model matrix rotation from angles
    CIwMat rotX, rotY, rotZ;
    rotX.SetRotX(s_Angles.x);
    rotY.SetRotY(s_Angles.y);
    rotZ.SetRotZ(s_Angles.z);
    s_ModelMatrix.CopyRot(rotX * rotY * rotZ);

    // Set view matrix to zoom in and out along world Z axis
    s_ViewMatrix.t.z = -(IW_GEOM_COS(s_Timer << 6) + 0x3000) >> 4;
    IwGxSetViewMatrix(&s_ViewMatrix);

    // Update timer
    s_Timer++;

    // Update menu manager
    IwGetMenuManager()->Update();
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Set material to use for all drawing
    IwGxSetMaterial(s_Material);

    //-------------------------------------------------------------------------
    // Render screenspace quad, top-left
    //-------------------------------------------------------------------------
    // Demonstrate use of the data cache - allocate and write 4 screenspace verts
    CIwSVec2* pCoord = IW_GX_ALLOC(CIwSVec2, 4);
    pCoord[0].x = 0x10; pCoord[0].y = 0x30;
    pCoord[1].x = 0x10; pCoord[1].y = 0x60;
    pCoord[2].x = 0x40; pCoord[2].y = 0x30;
    pCoord[3].x = 0x40; pCoord[3].y = 0x60;

    // Set screenspace origin and Z position
    IwGxSetScreenSpaceOrg(&CIwSVec2::g_Zero);
    IwGxSetScreenSpaceSlot(-1);

    // Set screenspace vertex stream
    // Note how the view and model matrices have no effect
    IwGxSetVertStreamScreenSpace(pCoord, 4);

    // Set colour stream
    IwGxSetColStream(s_Cols, 8);

    // Draw screenspace quad. Note that passing in a NULL index stream is equiavalent to passing in an index
    // stream of {0, 1, 2, 3}.
    IwGxDrawPrims(IW_GX_QUAD_STRIP, NULL, 4);

    //-------------------------------------------------------------------------
    // Render viewspace cube, top-right
    //-------------------------------------------------------------------------
    // Set the viewspace origin
    CIwSVec3 vOrg(0xd0, -0x80, 0x300);
    IwGxSetViewSpaceOrg(&vOrg);

    // Set viewspace vertex stream
    // Note how the view and model matrices have no effect
    IwGxSetVertStreamViewSpace(s_Verts, 8);

    // Set colour stream
    IwGxSetColStream(s_Cols, 8);

    // Draw the cube.
    IwGxDrawPrims(IW_GX_TRI_STRIP, s_TriStrip, 22);

    //-------------------------------------------------------------------------
    // Render modelspace cube, bottom-left
    //-------------------------------------------------------------------------
    // Set the model matrix
    IwGxSetModelMatrix(&s_ModelMatrix);

    // Set modelspace vertex stream
    // Note how the view and model matrices both have an effect
    IwGxSetVertStream(s_Verts, 8);

    // Set colour stream
    IwGxSetColStream(s_Cols, 8);

    // Draw the cube.
    IwGxDrawPrims(IW_GX_TRI_STRIP, s_TriStrip, 22);

    //-------------------------------------------------------------------------
    // Render worldspace cube
    //-------------------------------------------------------------------------
    // Set worldspace vertex stream
    // Note how the model matrix has no effect
    IwGxSetVertStreamWorldSpace(s_Verts, 8);

    // Set colour stream
    IwGxSetColStream(s_Cols, 8);

    // Draw the cube.
    IwGxDrawPrims(IW_GX_TRI_STRIP, s_TriStrip, 22);

    // End drawing
    IwGxFlush();

    // Render menu manager
    IwGetMenuManager()->Render();
    IwGxFlush();

    // Swap buffers
    IwGxSwapBuffers();
}
