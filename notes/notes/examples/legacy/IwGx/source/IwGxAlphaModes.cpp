/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
// ExampleIwGxAlphaModes
//-----------------------------------------------------------------------------

/**
 * @page ExampleLegacyIwGxAlphaModes IwGx Alpha Modes Example (Legacy)
 * The following example Demonstrates multiple spinning cubes, showing various
 * alpha modes.
 *
 * The SetAlphaMode() function is used to set the level of transparency
 * applied to each texture.
 *
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwGxAlphaModesImage.png
 *
 * @include IwGxAlphaModes.cpp
 */

#include "IwGx.h"
#include "IwMaterial.h"
#include "IwTexture.h"

#include "ExamplesMain.h"

// Texture object
CIwTexture* s_Texture = NULL;

// Vertex data
const int16 s = 0x80;
CIwSVec3    s_Verts[16] =
{
    CIwSVec3(-s, -s, -s),
    CIwSVec3( s, -s, -s),
    CIwSVec3( s,  s, -s),
    CIwSVec3(-s,  s, -s),

    CIwSVec3( s, -s, -s),
    CIwSVec3( s, -s,  s),
    CIwSVec3( s,  s,  s),
    CIwSVec3( s,  s, -s),

    CIwSVec3( s, -s,  s),
    CIwSVec3(-s, -s,  s),
    CIwSVec3(-s,  s,  s),
    CIwSVec3( s,  s,  s),

    CIwSVec3(-s, -s,  s),
    CIwSVec3(-s, -s, -s),
    CIwSVec3(-s,  s, -s),
    CIwSVec3(-s,  s,  s),
};

// UV data
CIwSVec2        s_UVs[16] =
{
    CIwSVec2(0,             0),
    CIwSVec2(IW_GEOM_ONE,   0),
    CIwSVec2(IW_GEOM_ONE,   IW_GEOM_ONE),
    CIwSVec2(0,             IW_GEOM_ONE),

    CIwSVec2(0,             0),
    CIwSVec2(IW_GEOM_ONE,   0),
    CIwSVec2(IW_GEOM_ONE,   IW_GEOM_ONE),
    CIwSVec2(0,             IW_GEOM_ONE),

    CIwSVec2(0,             0),
    CIwSVec2(IW_GEOM_ONE,   0),
    CIwSVec2(IW_GEOM_ONE,   IW_GEOM_ONE),
    CIwSVec2(0,             IW_GEOM_ONE),

    CIwSVec2(0,             0),
    CIwSVec2(IW_GEOM_ONE,   0),
    CIwSVec2(IW_GEOM_ONE,   IW_GEOM_ONE),
    CIwSVec2(0,             IW_GEOM_ONE),
};

// Index stream for textured material
uint16      s_QuadList[16] =
{
    0, 3, 2, 1,
    4, 7, 6, 5,
    8, 11, 10, 9,
    12, 15, 14, 13,
};

// Angles
CIwSVec3     s_Angles;

// Local matrix
CIwMat       s_ModelMatrix;

// Materials
CIwMaterial *s_MaterialBG;
CIwMaterial *s_MaterialCubes[4];

//-----------------------------------------------------------------------------
// The following function loads a texture from file and applies it to each of
// the rendered cubes. The important difference between each cube is the alpha
// mode. The SetAlphaMode() function is used to set the level of transparency
// applied to each texture.
//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    IwGxInit();

    // Set screen clear colour
    IwGxSetColClear(0x40, 0x40, 0x40, 0x00);

    // Enable emissive lighting only
    IwGxLightingEmissive(true);
    IwGxLightingAmbient(false);
    IwGxLightingDiffuse(false);

    // Set field of view
    IwGxSetPerspMul(0xa0);

    // Set near and far planes
    IwGxSetFarZNearZ(0x1000, 0x10);

    // Create empty texture object
    s_Texture = new CIwTexture;

    // Load image data from disk into texture
    s_Texture->LoadFromFile("./textures/testTexture_8bit.bmp");

    // "Upload" texture to VRAM
    s_Texture->Upload();

    // Initialise angles
    s_Angles = CIwSVec3::g_Zero;

    // Set the view matrix along the -ve z axis
    CIwMat view = CIwMat::g_Identity;
    view.t.z = -0x400;
    IwGxSetViewMatrix(&view);

    s_MaterialBG = new CIwMaterial;

    // Initialise materials
    s_MaterialCubes[0] = new CIwMaterial;
    s_MaterialCubes[0]->SetTexture(s_Texture);
    s_MaterialCubes[0]->SetAlphaMode(CIwMaterial::NONE);
    s_MaterialCubes[0]->SetColEmissive(0x808080);

    s_MaterialCubes[1] = new CIwMaterial;
    s_MaterialCubes[1]->SetTexture(s_Texture);
    s_MaterialCubes[1]->SetAlphaMode(CIwMaterial::ADD);
    s_MaterialCubes[1]->SetColEmissive(0x808080);

    s_MaterialCubes[2] = new CIwMaterial;
    s_MaterialCubes[2]->SetTexture(s_Texture);
    s_MaterialCubes[2]->SetAlphaMode(CIwMaterial::SUB);
    s_MaterialCubes[2]->SetColEmissive(0x808080);

    s_MaterialCubes[3] = new CIwMaterial;
    s_MaterialCubes[3]->SetTexture(s_Texture);
    s_MaterialCubes[3]->SetAlphaMode(CIwMaterial::HALF);
    s_MaterialCubes[3]->SetColEmissive(0x808080);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // Destroy texture
    delete s_Texture;

    // Destroy materials
    delete s_MaterialCubes[0];
    delete s_MaterialCubes[1];
    delete s_MaterialCubes[2];
    delete s_MaterialCubes[3];
    delete s_MaterialBG;

    // Terminate
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // Update angles
    s_Angles.y += 0x40;

    // Build model matrix from angles
    s_ModelMatrix.SetRotY(s_Angles.y);
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    //-------------------------------------------------------------------------
    // Render background
    //-------------------------------------------------------------------------
    // Set active material
    IwGxSetMaterial(s_MaterialBG);

    // Set colour stream
    static CIwColour bgCols[4] =
    {
        {0xff, 0x00, 0x00, 0xff},
        {0x00, 0xff, 0x00, 0xff},
        {0x00, 0x00, 0xff, 0xff},
        {0x00, 0xff, 0xff, 0xff},
    };
    IwGxSetColStream(bgCols, 4);

    // Set the (screenspace) vertex stream
    CIwSVec2* pCoords = AllocClientScreenRectangle();
    IwGxSetScreenSpaceOrg(&CIwSVec2::g_Zero);
    IwGxSetScreenSpaceSlot(-3);
    IwGxSetVertStreamScreenSpace(pCoords, 4);

    // Draw the textured primitives
    IwGxDrawPrims(IW_GX_QUAD_STRIP, NULL, 4);

    //-------------------------------------------------------------------------
    // Render cubes
    //-------------------------------------------------------------------------
    // Set UV stream
    IwGxSetUVStream(s_UVs);

    // Clear colour stream
    IwGxSetColStream(NULL);

    // Display 4 cubes
    for (uint32 y = 0; y < 2; y++)
    {
        for (uint32 x = 0; x < 2; x++)
        {
            s_ModelMatrix.t.x = -0x100 + (int32)(x * 0x200);
            s_ModelMatrix.t.y = -0x0c0 + (int32)(y * 0x200);

            // Set the model matrix
            IwGxSetModelMatrix(&s_ModelMatrix);

            // Set the (modelspace) vertex stream
            IwGxSetVertStreamModelSpace(s_Verts, 16);

            // Set active material
            IwGxSetMaterial(s_MaterialCubes[y * 2 + x]);

            // Draw the cube quad list
            IwGxDrawPrims(IW_GX_QUAD_LIST, s_QuadList, 16);
        }
    }

    // End drawing
    IwGxFlush();

    // Swap buffers
    IwGxSwapBuffers();
}
