/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
// ExampleIwGxTexture
//-----------------------------------------------------------------------------

/**
 * @page ExampleLegacyIwGxTexture IwGx Texture Example (Legacy)
 *
 * Textures can be set on a material. To display a texture on a material,
 * you must:
 *  -# Load the texture.
 *  -# Set the texture on the material.
 *  -# Map the texture on the geometry using UV coordinates.
 *
 * In this example we add a texture to a material using the following code:
 * @code
 * // Create empty texture object
 * s_Texture = new CIwTexture;
 *
 * // Load image data from disk into texture
 * s_Texture->LoadFromFile("./textures/testTexture_8bit.bmp");
 *
 * // "Upload" texture to VRAM
 * s_Texture->Upload();
 * @endcode
 *
 * This code loads the file testTexture_8bit.bmp from the data/textures
 * folder in your application directory.
 * UV coordinates must be set for the texture in order to orientate it
 * correctly on the geometry. To do this the UV coordinates are entered as
 * 2D vectors in the ExampleUpdate section of the example code. The UV
 * coordinates use the same indexing as the applied drawing mode; therefore,
 * they must be arranged in the same order to ensure the texture displays
 * correctly.
 *
 * @note 1 << 12, 1 bit shifted by 12, indicates a UV coordinate of 1.
 *
 * The texture must then be set on the material, this is done in the
 * ExampleRender section of our example using the following code:
 *
 * @code
 * mpMatQuad->SetTexture(s_Texture);
 * @endcode
 *
 * @note For more information on how to implement materials and how to use
 * textures in your application, see the @ref texture "Texture" section of
 * the <i>IwGx API Documentation</i>.
 *
 * The example application demonstrates how to create a textured spinning
 * cube. Two primitives are drawn - an untextured, vertex coloured tri
 * list (5 faces of the cube); and a textured, uncoloured quad list
 * (1 face of the cube).
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwGxTextureImage.png
 *
 * @include IwGxTexture.cpp
 */

#include "IwGx.h"
#include "IwMaterial.h"
#include "IwTexture.h"

// Texture object
CIwTexture* s_Texture = NULL;

// Vertex data
const int16 s = 0x80;
CIwSVec3    s_Verts[8] =
{
    CIwSVec3(-s, -s, -s),
    CIwSVec3( s, -s, -s),
    CIwSVec3( s,  s, -s),
    CIwSVec3(-s,  s, -s),
    CIwSVec3(-s, -s,  s),
    CIwSVec3( s, -s,  s),
    CIwSVec3( s,  s,  s),
    CIwSVec3(-s,  s,  s),
};

// Colour data
CIwColour   s_Cols[8] =
{
    {0x00, 0x00, 0x00},
    {0x00, 0x00, 0xff},
    {0x00, 0xff, 0x00},
    {0x00, 0xff, 0xff},
    {0xff, 0x00, 0x00},
    {0xff, 0x00, 0xff},
    {0xff, 0xff, 0x00},
    {0xff, 0xff, 0xff},
};

// UV data
CIwSVec2    s_UVs[4] =
{
    CIwSVec2(0 << 12, 0 << 12),
    CIwSVec2(1 << 12, 0 << 12),
    CIwSVec2(1 << 12, 1 << 12),
    CIwSVec2(0 << 12, 1 << 12),
};

// Index stream for textured material
uint16      s_QuadStrip[4] =
{
    0, 3, 1, 2,
};

// Index stream for untextured material
uint16      s_TriStrip[20] =
{
    1, 2, 5, 6, 4, 7, 0, 3,
    3, 4, // degenerates
    4, 0, 5, 1,
    1, 3, // degenerates
    3, 7, 2, 6,
};

// Angles
CIwSVec3    s_Angles;

// Local matrix
CIwMat      s_ModelMatrix;

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    IwGxInit();

    // Set screen clear colour
    IwGxSetColClear(0x40, 0x40, 0x40, 0x00);

    // Turn all lighting off
    IwGxLightingOff();

    // Set field of view
    IwGxSetPerspMul(0xa0);

    // Set near and far planes
    IwGxSetFarZNearZ(0x1000, 0x10);

    // Create empty texture object
    s_Texture = new CIwTexture;

    // Load image data from disk into texture
    s_Texture->LoadFromFile("./textures/testTexture_8bit.bmp");

    // "Upload" texture to VRAM
    s_Texture->Upload();

    // Initialise angles
    s_Angles = CIwSVec3::g_Zero;

    // Set the view matrix along the -ve z axis
    CIwMat view = CIwMat::g_Identity;
    view.t.z = -0x200;
    IwGxSetViewMatrix(&view);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // Destroy texture
    delete s_Texture;

    // Terminate
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // Update angles
    s_Angles.x += 0x10;
    s_Angles.y += 0x20;
    s_Angles.z += 0x30;

    // Build model matrix from angles
    CIwMat  rotX, rotY, rotZ;
    rotX.SetRotX(s_Angles.x);
    rotY.SetRotY(s_Angles.y);
    rotZ.SetRotZ(s_Angles.z);
    s_ModelMatrix = rotX * rotY * rotZ;

    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    CIwMaterial* pMat;

    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Allocate and initialise material from the IwGx global cache
    pMat = IW_GX_ALLOC_MATERIAL();

    // Set this as the active material
    IwGxSetMaterial(pMat);

    // Set the model matrix
    IwGxSetModelMatrix(&s_ModelMatrix);

    // Set the (modelspace) vertex stream
    IwGxSetVertStreamModelSpace(s_Verts, 8);

    // Set the vertex colour stream
    IwGxSetColStream(s_Cols, 8);

    // Draw the untextured primitives
    IwGxDrawPrims(IW_GX_TRI_STRIP, s_TriStrip, 20);

    // Allocate and initialise another material
    pMat = IW_GX_ALLOC_MATERIAL();

    // Set the diffuse map
    pMat->SetTexture(s_Texture);

    // Set this as the active material
    IwGxSetMaterial(pMat);

    // Set the vertex UV stream
    IwGxSetUVStream(s_UVs);

    // Clear the vertex colour stream
    IwGxSetColStream(NULL);

    // Draw the textured primitives
    IwGxDrawPrims(IW_GX_QUAD_STRIP, s_QuadStrip, 4);

    // End drawing
    IwGxFlush();

    // Swap buffers
    IwGxSwapBuffers();
}
