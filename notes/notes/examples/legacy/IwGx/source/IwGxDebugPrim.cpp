/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleLegacyIwGxDebugPrim IwGx DebugPrim Example (Legacy)
 *
 * The following example draws cross, sphere, box, axes, and circle debug
 * primitives.
 *
 * The main functions used to achieve this are:
 *  - IwGxDebugPrimAxes()
 *  - IwGxDebugPrimSBBox()
 *  - IwGxDebugPrimCircle()
 *  - IwGxDebugPrimCross()
 *  - IwGxDebugPrimSphere()
 *
 * The key controls are as follows:
 *  - <b>s3eKeyUp</b>: Spins the view anti-clockwise on the X-Axis
 *  - <b>s3eKeyDown</b>: Spins the view clockwise on the X-Axis
 *  - <b>s3eKeyRight</b>: Spins the view anti-clockwise on the Y-Axis
 *  - <b>s3eKeyLeft</b>: Spins the view clockwise on the Y-Axis
 *
 * The following debug primitives are drawn in this example:
 *  - The first primitive is a bounding box which is created by passingq
 *    the IwGxDebugPrimSBBox() function a colour value and a CIwSBBox object
 *  - The second and third primitives are crosses. A cross is created by passing
 *    a single point and colour value to the IwGxDebugPrimCross() function.
 *    A string is printed next to each cross to indicate the points at which
 *    they are placed. The IwGxWorldToScreenXY() function is used to calculate
 *    that position and the IwGxPrintString() string is used to print the
 *    string at a slight offset from that point.
 *  - A sphere, which is created by passing the IwGxDebugPrimSphere()
 *    function a CIwSphere object. The Sphere object is defined by a point
 *    representing its origin and a radius length.
 *  - The fifth is a set of local axes, in our example this takes the identity
 *    matrix as its transform to indicate its origin and a value of 0x20
 *    to indicate the axes indicators' length.
 *  - The sixth is a circle, which is placed on one face of the bounding box.
 *    It is defined by a local transform, radius, local axis normal and colour.
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwGxDebugPrimImage.png
 *
 * @include IwGxDebugPrim.cpp
 */

#include "s3e.h"
#include "IwGx.h"
#include "IwGxPrint.h"
#include "IwMaterial.h"
#include "IwDebugPrim.h"
#include "IwGxTransformSW.h"
#include "ExamplesMain.h"

/**
 * direction vectors, camera position
 */
CIwVec3 s_left, s_up, s_cameraPos;

/**
 * view matrix
 */
CIwMat s_viewMatrix;

//-----------------------------------------------------------------------------
// The following function sets the required up, left and camera position
// vectors.
//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    IwGxInit();

    // Set screen clear colour
    IwGxSetColClear(0x40, 0x40, 0x40, 0x00);

    // Turn all lighting off
    IwGxLightingOff();

    // Initialise vectors
    s_left      =  CIwVec3::g_AxisX;
    s_up        = -CIwVec3::g_AxisY;
    s_cameraPos =  CIwVec3(0, 0, -0x130);

    // Initialise view matrix
    s_viewMatrix = CIwMat::g_Identity;

    s_viewMatrix.SetTrans(s_cameraPos);
    s_viewMatrix.LookAt(
        s_viewMatrix.GetTrans(),
         CIwVec3::g_Zero,
        -CIwVec3::g_AxisY);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // Terminate
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
// The following function observes the keyboard state and changes the values
// governing the view accordingly.
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    CIwMat   rotation;
    CIwVec3  lookFrom(0,0,0);
    bool     bUpdateLookAt = false;

    // Up, Down, Left, Right keys
    //

    if (CheckCursorState() == EXCURSOR_UP )
    {
        rotation.SetAxisAngle(
            s_viewMatrix.RotateVec(s_left),
            IW_ANGLE_FROM_DEGREES(-5));
        lookFrom = (s_viewMatrix * rotation).RotateVec(
            s_cameraPos);
        bUpdateLookAt = true;
    }

    if (CheckCursorState() == EXCURSOR_DOWN )
    {
        rotation.SetAxisAngle(
            s_viewMatrix.RotateVec(s_left),
            IW_ANGLE_FROM_DEGREES(5));
        lookFrom = (s_viewMatrix * rotation).RotateVec(
            s_cameraPos);
        bUpdateLookAt = true;
    }

    if (CheckCursorState() == EXCURSOR_RIGHT )
    {
        rotation.SetAxisAngle(
            s_viewMatrix.RotateVec(s_up),
            IW_ANGLE_FROM_DEGREES(5));
        lookFrom = (s_viewMatrix * rotation).RotateVec(
            s_cameraPos);
        bUpdateLookAt = true;
    }

    if (CheckCursorState() == EXCURSOR_LEFT )
    {
        rotation.SetAxisAngle(
            s_viewMatrix.RotateVec(s_up),
            IW_ANGLE_FROM_DEGREES(-5));
        lookFrom = (s_viewMatrix * rotation).RotateVec(
            s_cameraPos);
        bUpdateLookAt = true;
    }

    // Do this so that we do not
    // lose accuracy when looking at
    // the target
    if (bUpdateLookAt){
        s_viewMatrix.SetTrans(lookFrom);
        s_viewMatrix.LookAt(
            s_viewMatrix.GetTrans(),
             CIwVec3::g_Zero,
            s_viewMatrix.RotateVec(-s_up));
    }

    IwGxSetViewMatrix(&s_viewMatrix);

    return true;
}
//-----------------------------------------------------------------------------
// The following function renders each of the debug primitives. The debug
// primitives drawn are:
//  - bounding box
//  - crosses
//  - sphere
//  - set of axes
//  - circle
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Allocate a material from the IwGx global cache
    CIwMaterial* pMat = IW_GX_ALLOC_MATERIAL();

    // Set this as the active material
    IwGxSetMaterial(pMat);

    // BOX
    //
    // Draw the box around the origin
    IwGxDebugPrimSBBox(CIwSBBox(CIwSVec3(-0x40,-0x40,-0x40),
                                CIwSVec3(0x40,0x40,0x40)),
                       IwGxGetColFixed(IW_GX_COLOUR_WHITE));

    // CROSSES
    //
    int16 sx, sy;

    // green cross of length 0x4 at (-0x40, -0x40, -0x40)
    IwGxDebugPrimCross(CIwVec3(-0x40,-0x40,-0x40),
        0x4,
        IwGxGetColFixed(IW_GX_COLOUR_GREEN));
    // Label
    IwGxWorldToScreenXY(sx, sy, CIwVec3(-0x40,-0x40,-0x40));
    IwGxPrintString(sx + 1, sy + 1, "`xc0c0c0(-0x40,-0x40,-0x40)", false);

    // green cross of length 0x4 at (0x40, 0x40, 0x40)
    IwGxDebugPrimCross(CIwVec3(0x40,0x40,0x40),
        0x4,
        IwGxGetColFixed(IW_GX_COLOUR_GREEN));
    // Label
    IwGxWorldToScreenXY(sx, sy, CIwVec3(0x40,0x40,0x40));
    IwGxPrintString(sx + 1, sy + 1, "`xc0c0c0(0x40,0x40,0x40)", false);

    // SPHERE
    //
    IwGxDebugPrimSphere(
        CIwSphere(CIwVec3::g_Zero, 0x6E), // sphere around the box
        IwGxGetColFixed(IW_GX_COLOUR_BLUE),
        false);                           // do not draw radial lines


    // AXES
    //

    // Mark the origin using debug axes
    IwGxDebugPrimAxes(CIwMat::g_Identity, // axes at the origin
                      0x20);              // length 0x20
    // Label the origin
    IwGxWorldToScreenXY(sx, sy, CIwVec3::g_Zero);
    IwGxPrintString(sx + 1, sy + 1, "`x5050d0Origin", false);

    // CIRCLE
    //

    CIwMat circleTransform = CIwMat::g_Identity;

    circleTransform.SetTrans(CIwVec3(0, 0, 0x40));
    IwGxDebugPrimCircle(
        circleTransform, // circle's local transformation
        0x20,            // circle's radius
        2,               // z axis is normal to the circle
        IwGxGetColFixed(IW_GX_COLOUR_YELLOW),
        true);           // draw radial lines

    // Paint the cursor keys buttons
    RenderCursorskeys();

    // Flush and swap
    IwGxFlush();
    IwGxSwapBuffers();
}
