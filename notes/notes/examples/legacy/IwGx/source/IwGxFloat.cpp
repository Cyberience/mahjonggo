/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
// ExampleIwGxTexture
//-----------------------------------------------------------------------------

/**
 * @page ExampleLegacyIwGxFloat IwGx Floating Point Matrix Example (Legacy)
 *
 * This example demonstrates the use of the floating point matrix stack
 * in IwGx, and the advantage of using floats.
 *
 * NB: This code is only relevant for the legacy fixed-point version of IwGx.
 * From SDK 6.1, the supported standard version of IwGx supports floating-point
 * only and not fixed-point.
 *
 * IwGx maintains two entirely seperate matrix stacks for fixed point and
 * floating point transformations.
 *
 * When an IwGxSetVertStream* call is made, the vertices will be transformed
 * using the active stack. By default, this is the last stack to be touched.
 *
 * The IwGxSetFloatTransform and IwGxSetFixedTransform functions can also be
 * used to select the current stack.
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwGxFloatImage.png
 *
 * @include IwGxFloat.cpp
 */


#include "IwGx.h"
#include "IwMaterial.h"
#include "IwTexture.h"

// Texture object
CIwTexture* s_Texture = NULL;

// Vertex data
const int16 s = 0x10;
CIwSVec3    s_Verts[8] =
{
    CIwSVec3(-s, -s, -s),
    CIwSVec3( s, -s, -s),
    CIwSVec3( s,  s, -s),
    CIwSVec3(-s,  s, -s),
    CIwSVec3(-s, -s,  s),
    CIwSVec3( s, -s,  s),
    CIwSVec3( s,  s,  s),
    CIwSVec3(-s,  s,  s),
};

// Colour data
CIwColour   s_Cols[8] =
{
    {0x00, 0x00, 0x00},
    {0x00, 0x00, 0xff},
    {0x00, 0xff, 0x00},
    {0x00, 0xff, 0xff},
    {0xff, 0x00, 0x00},
    {0xff, 0x00, 0xff},
    {0xff, 0xff, 0x00},
    {0xff, 0xff, 0xff},
};

// UV data
CIwSVec2    s_UVs[4] =
{
    CIwSVec2(0 << 12, 0 << 12),
    CIwSVec2(1 << 12, 0 << 12),
    CIwSVec2(1 << 12, 1 << 12),
    CIwSVec2(0 << 12, 1 << 12),
};

// Index stream for textured material
uint16      s_QuadStrip[4] =
{
    0, 3, 1, 2,
};

// Index stream for untextured material
uint16      s_TriStrip[20] =
{
    1, 2, 5, 6, 4, 7, 0, 3,
    3, 4, // degenerates
    4, 0, 5, 1,
    1, 3, // degenerates
    3, 7, 2, 6,
};

// Angles
CIwFVec3    s_Angles;

// Local matrix
CIwFMat      s_ModelMatrix;

//-----------------------------------------------------------------------------
// Function to convert a floating point matrix to the equivalent fixed point matrix
CIwMat FMatToMat(const CIwFMat& fmat)
{
    CIwMat fixedMat;
    for (int y = 0; y < 4; y++)
    {
        for (int x = 0; x < 3; x++)
        {
            if (y < 3)
            {
                // rotational components are based on fixed point
                fixedMat.m[x][y] = (int)(fmat.m[x][y] * IW_GEOM_ONE);
            }
            else
            {
                // translation components are based on whole part of float translation
                fixedMat.t[x] = (int)fmat.t[x];
            }
        }
    }
    return fixedMat;
}
//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    IwGxInit();

    // Set screen clear colour
    IwGxSetColClear(0x40, 0x40, 0x40, 0x00);

    // Turn all lighting off
    IwGxLightingOff();

    // Set field of view
    IwGxSetPerspMul(0xa0);

    // Set near and far planes
    IwGxSetFarZNearZ(0x1000, 0x10);

    // Create empty texture object
    s_Texture = new CIwTexture;

    // Load image data from disk into texture
    s_Texture->LoadFromFile("./textures/testTexture_8bit.bmp");

    // "Upload" texture to VRAM
    s_Texture->Upload();

    // Initialise angles
    s_Angles = CIwSVec3::g_Zero;

    // Set the view matrix along the -ve z axis
    CIwMat view = CIwMat::g_Identity;
    view.t.z = -0x40;
    IwGxSetViewMatrix(&view);

    // Set equivalent floating point view matrix
    CIwFMat fview = IwGxMatToFMat(view);
    IwGxSetViewMatrix(&fview);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // Destroy texture
    delete s_Texture;

    // Terminate
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // Update angles
    s_Angles.x += 0.001f;
    s_Angles.y += 0.002f;
    s_Angles.z += 0.004f;

    // Build model matrix from angles
    CIwFMat  rotX, rotY, rotZ;
    rotX.SetRotX(s_Angles.x);
    rotY.SetRotY(s_Angles.y);
    rotZ.SetRotZ(s_Angles.z);
    s_ModelMatrix = rotX * rotY * rotZ;
    s_ModelMatrix.t = s_ModelMatrix.RotateVec(CIwFVec3(0,0,30.0f));

    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    CIwMaterial* pMat;

    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    for (int i = 0; i < 2; i++)
    {
        // Print a label for the transform pipes
        IwGxPrintString(IwGxGetScreenWidth()/2 + 75*(i?-1:1) - 20,
            IwGxGetScreenHeight()/4, i ? "Fixed" : "Float");

        if (i == 0)
        {
            // Set the model matrix (40 units to the right)
            s_ModelMatrix.t.x += 40.0f;
            IwGxSetModelMatrix(&s_ModelMatrix);
        }
        else
        {
            // Set the equivalent fixed point matrix (80 units left)
            s_ModelMatrix.t.x -= 80.0f;
            CIwMat fixedModelMatrix = FMatToMat(s_ModelMatrix);
            IwGxSetModelMatrix(&fixedModelMatrix);
        }

        // Allocate and initialise material from the IwGx global cache
        pMat = IW_GX_ALLOC_MATERIAL();

        // Set this as the active material
        IwGxSetMaterial(pMat);

        // Set the (modelspace) vertex stream
        IwGxSetVertStreamModelSpace(s_Verts, 8);

        // Set the vertex colour stream
        IwGxSetColStream(s_Cols, 8);

        // Draw the untextured primitives
        IwGxDrawPrims(IW_GX_TRI_STRIP, s_TriStrip, 20);

        // Allocate and initialise another material
        pMat = IW_GX_ALLOC_MATERIAL();

        // Set the diffuse map
        pMat->SetTexture(s_Texture);

        // Set this as the active material
        IwGxSetMaterial(pMat);

        // Set the vertex UV stream
        IwGxSetUVStream(s_UVs);

        // Clear the vertex colour stream
        IwGxSetColStream(NULL);

        // Draw the textured primitives
        IwGxDrawPrims(IW_GX_QUAD_STRIP, s_QuadStrip, 4);
    }

    // End drawing
    IwGxFlush();

    // Swap buffers
    IwGxSwapBuffers();
}
