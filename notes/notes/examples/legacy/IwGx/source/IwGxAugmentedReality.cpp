/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
// ExampleIwGxAugmentedReality
//-----------------------------------------------------------------------------

/**
 * @page ExampleLegacyIwGxAugmentedReality IwGx AugmentedReality Example (Legacy)
 *
 * This example demonstrates the use of the s3eCamera API with IwGx, feeding
 * the output of the camera directly onto a quad as a texture. This method is
 * described fully in the IwGxDynamicTexture example. The camera output format
 * used is chosen to be RGB565_CONVERTED. This format is available on all
 * platforms, although it may be slower than using a native format on a given
 * platform. IwGx can accept textures of this format so no further processing
 * is required.
 *
 * The example also uses the s3eCompass and s3eLocation extensions to derive
 * directional information. This enables a compass graphic to be overlaid on
 * the screen and rotated to the current device heading. A set of directional
 * markers show the position of major cities in relation to the device's
 * current position.
 *
 * @image html IwGxAugmentedReality.png
 *
 * @include IwGxAugmentedReality.cpp
 *
 */

#include "s3e.h"
#include "s3eCamera.h"
#include "s3eCompass.h"
#include "s3eLocation.h"

#include "IwGx.h"
#include "IwGxPrint.h"
#include "IwMaterial.h"
#include "IwTexture.h"
#include "ExamplesMain.h"

#include "IwMath.h"
#include <sys/param.h>

static CIwTexture* g_CompassTexture = NULL;
static CIwTexture* g_ImageTexture = NULL;
static CIwTexture* g_MarkerTexture = NULL;

static double g_CurrentHeading = 0;
static s3eCameraFrameRotation g_FrameRotation = S3E_CAMERA_FRAME_ROT90;
static bool g_CompassError, g_LocationError;

static CIwMat viewMatrix;

#define NUM_LOCATIONS 10

// Array of locations to display in the view.
struct SLocation
{
    // Display name for the location
    const char* m_Name;
    // Degrees and minutes of latitude and longitude for the location
    int   m_Latitude_deg, m_Latitude_min, m_Longitude_deg, m_Longitude_min;
    // Compass bearing of the location from current location
    double      m_Bearing;
    // Distance in Km to the location from current location
    double      m_Distance;
};

struct SLocation g_Locations[NUM_LOCATIONS] = {
    { "London",        51, 32,  0,  -5, 0.0, 0.0 },
    { "Madrid",        40, 26, -3, -42, 0.0, 0.0 },
    { "New York",      40, 47, -73,-58, 0.0, 0.0 },
    { "Dubai",         25, 16,  55, 16, 0.0, 0.0 },
    { "Sydney",       -34,  0, 151,  0, 0.0, 0.0 },
    { "Tokyo",         35, 40, 139, 45, 0.0, 0.0 },
    { "Seoul",         37,  0, 127, 30, 0.0, 0.0 },
    { "Beijing",       39, 55, 116, 25, 0.0, 0.0 },
    { "Bangkok",       13, 45, 100, 30, 0.0, 0.0 },
    { "Johannesburg", -26, -12, 28,  4, 0.0, 0.0 },
};

double inline rad(double d)
{
    return d / 180.0f * PI;
}

double inline deg(double d)
{
    return d / PI * 180.0f;
}

//-----------------------------------------------------------------------------
// Camera callback
//
// Copy the capture frame buffer into a texture ready for rendering.
// This function will be called during s3eDeviceYield() so cannot conflict with
// Render calls.
//-----------------------------------------------------------------------------
static int32 frameReceived(void* systemData, void* userData)
{
    s3eCameraFrameData *data = (s3eCameraFrameData*)systemData;

    // If there is no texture, create one.
    // This is a slow operation compared to memcpy so we don't want to do it every frame.
    if (g_ImageTexture == NULL)
    {
        g_ImageTexture = new CIwTexture();
        g_ImageTexture->SetMipMapping(false);
        g_ImageTexture->SetModifiable(true);

        g_ImageTexture->CopyFromBuffer(data->m_Width, data->m_Height, CIwImage::RGB_565, data->m_Pitch, (uint8*)data->m_Data, NULL);
        g_ImageTexture->Upload();
    }

    // Copy the camera image data into the texture. Note that it does not get copied to VRAM at this point.
    memcpy(g_ImageTexture->GetTexels(), data->m_Data, data->m_Height * data->m_Pitch);
    g_FrameRotation = data->m_Rotation;

    return 0;
}

//-----------------------------------------------------------------------------
// The following function sets the screen colour and turns off the lighting.
//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    IwGxInit();

    // Turn all lighting off
    IwGxLightingOff();

    // Set field of view
    IwGxSetPerspMul(0xa0);
    IwGxSetFarZNearZ(0x2000,0x10);

    // Create new texture objects
    g_CompassTexture = new CIwTexture;
    g_MarkerTexture = new CIwTexture;

    // Load texture from files
    g_CompassTexture->LoadFromFile("textures/compass.tga");
    g_MarkerTexture->LoadFromFile ("textures/marker.bmp");

    // Upload the textures to VRAM ready for use
    g_CompassTexture->Upload();
    g_MarkerTexture->Upload();

    // Set up camera capture
    if (s3eCameraAvailable())
    {
        // Register callback to receive camera frames
        s3eCameraRegister(S3E_CAMERA_UPDATE_STREAMING, frameReceived, NULL);

        // Request medium sized image (for decent frame rate) with a platform independent format
        s3eCameraStart(S3E_CAMERA_STREAMING_SIZE_HINT_MEDIUM, S3E_CAMERA_PIXEL_TYPE_RGB565_CONVERTED);
    }
    else
    {
        s3eDebugTraceLine("Camera Extension not available!");
    }

    // Initialize compass if it is available
    if (s3eCompassAvailable())
    {
        s3eCompassStart();
    }

    // Initialize location service if it is available
    if (s3eLocationAvailable())
    {
        s3eLocationStart();
    }
}

//-----------------------------------------------------------------------------
// The following function deletes textures if they exist and terminates
// IwGx and s3e API functionality.
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // Release all textures
    if (g_CompassTexture)
        delete g_CompassTexture;

    if (g_ImageTexture)
        delete g_ImageTexture;

    if (g_MarkerTexture)
        delete g_MarkerTexture;

    // stop API services
    if (s3eLocationAvailable())
        s3eLocationStop();
    if (s3eCompassAvailable())
        s3eCompassStop();

    if (s3eCameraAvailable())
    {
        s3eCameraStop();
        s3eCameraUnRegister(S3E_CAMERA_UPDATE_STREAMING, frameReceived);
    }

    // Terminate
    IwGxTerminate();
}

//-----------------------------------------------------------------------------

bool ExampleUpdate()
{
    // Only update location information every 30 seconds
    static int64 last_update = -30000;
    if ((int64)s3eTimerGetMs() - last_update > 30000)
    {
        last_update = s3eTimerGetMs();

        // Get the current location from location service
        s3eLocation location;
        g_LocationError = S3E_RESULT_SUCCESS != s3eLocationGet(&location);

        // Calculate bearing and distance to list of locations
        // For more information see http://www.movable-type.co.uk/scripts/latlong.html
        for (int i=0; i<NUM_LOCATIONS; i++)
        {
            const double dest_longitude = (g_Locations[i].m_Longitude_min / 60.0f + g_Locations[i].m_Longitude_deg);
            const double dest_latitude =  (g_Locations[i].m_Latitude_min  / 60.0f + g_Locations[i].m_Latitude_deg);

            // distance calculation
            const double R = 6371; // km
            const double dLat = rad(dest_latitude - location.m_Latitude);
            const double dLon = rad(dest_longitude - location.m_Longitude) ;
            const double a = sin(dLat/2) * sin(dLat/2) + cos(rad(location.m_Latitude))
                           * cos(rad(dest_latitude)) * sin(dLon/2) * sin(dLon/2);
            const double c = 2 * atan2(sqrt(a), sqrt(1-a));
            g_Locations[i].m_Distance = R * c;

            // bearing calculation
            const double y = sin(dLon) * cos(location.m_Latitude);
            const double x = cos(location.m_Latitude) * sin(dest_latitude) -
                             sin(location.m_Latitude) * cos(dest_latitude)* cos(dLon);
            g_Locations[i].m_Bearing = deg(atan2(y, x));

            s3eDebugTracePrintf("Calculated location: %s, Bearing %f, Distance %fkm", g_Locations[i].m_Name, g_Locations[i].m_Bearing, g_Locations[i].m_Distance);
        }
    }

    // Get current compass heading and update
    s3eCompassHeading h;
    g_CompassError = S3E_RESULT_SUCCESS != s3eCompassGetHeading(&h);

    g_CurrentHeading = h.m_Heading;

    return true;
}

//-----------------------------------------------------------------------------
// The following function displays the augmented reality scene
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Refresh dynamic texture
    if (g_ImageTexture != NULL)
        g_ImageTexture->ChangeTexels(g_ImageTexture->GetTexels(), CIwImage::RGB_565);

    // Allocate a material from the IwGx global cache
    CIwMaterial* pMat = IW_GX_ALLOC_MATERIAL();
    pMat->SetModulateMode(CIwMaterial::MODULATE_NONE);

    // Use Texture on Material
    pMat->SetTexture(g_ImageTexture);

    // Set this as the active material
    IwGxSetMaterial(pMat);

    // Draw behind the view space
    IwGxSetScreenSpaceSlot(-1);

    // Set up screenspace vertex coords
    // (We are drawing camera image over whole screen)
    const int16 x1 = 0;
    const int16 x2 = (int16)IwGxGetScreenWidth();
    const int16 y1 = 0;
    const int16 y2 = (int16)IwGxGetScreenHeight();

    static CIwSVec2 xy3[4];
    xy3[0].x = x1, xy3[0].y = y1;
    xy3[1].x = x1, xy3[1].y = y2;
    xy3[2].x = x2, xy3[2].y = y2;
    xy3[3].x = x2, xy3[3].y = y1;

    // Camera image must not rotate with surface
    static CIwSVec2 normal_uvs[4] =
    {
        CIwSVec2(0 << 12, 0 << 12),
        CIwSVec2(0 << 12, 1 << 12),
        CIwSVec2(1 << 12, 1 << 12),
        CIwSVec2(1 << 12, 0 << 12),
    };

    // rotate UV coordinates based on rotation
    // (image must rotate against rotation of surface,
    // assuming that the camera image is NOT auto rotated)
    static CIwSVec2 uvs[4];
    for (int j=0; j<4; j++)
    {
        uvs[j] = normal_uvs[(4 - (int)g_FrameRotation + j ) % 4];
    }

    IwGxSetUVStream(uvs);
    IwGxSetVertStreamScreenSpace(xy3, 4);

    // Draw single quad in screen space for camera image
    IwGxDrawPrims(IW_GX_QUAD_LIST, NULL, 4);

    // Draw compass
    // Allocate a material from the IwGx global cache
    pMat = IW_GX_ALLOC_MATERIAL();
    pMat->SetModulateMode(CIwMaterial::MODULATE_NONE);
    // Use Texture on Material
    pMat->SetTexture(g_CompassTexture);
    pMat->SetAlphaMode(CIwMaterial::ALPHA_BLEND);
    // Set this as the active material
    IwGxSetMaterial(pMat);

    // Compass quad is in world space on the surface of the ZX plane
    const int s = 500;
    static CIwSVec3 compass_plane[4] =
    {
        CIwSVec3(  -s,  0, -s),
        CIwSVec3(  -s,  0,  s),
        CIwSVec3(   s,  0,  s),
        CIwSVec3(   s,  0, -s),
    };
    // Set up screenspace vertex coords
    IwGxSetVertStreamWorldSpace(compass_plane, 4);

    // Map compass texture coordinates
    static CIwSVec2 compass_uvs[4] =
    {
        CIwSVec2(1 << 12, 1 << 12),
        CIwSVec2(1 << 12, 0 << 12),
        CIwSVec2(0 << 12, 0 << 12),
        CIwSVec2(0 << 12, 1 << 12),
    };
    IwGxSetUVStream(compass_uvs);

    // Set viewing position (above ZX plane, looking along Z axis
    // (slightly downward) with the Y axis as upright.
    viewMatrix.SetIdentity();
    viewMatrix.LookAt(CIwVec3(0,100,0), CIwVec3(0,0,200), CIwVec3(0,-1,0));
    viewMatrix.SetTrans(CIwSVec3(0, 200, 0));

    // Rotate according to compass heading
    viewMatrix.PostRotateY(IW_ANGLE_FROM_DEGREES(g_CurrentHeading));
    IwGxSetViewMatrix(&viewMatrix);

    // Draw single quad for compass
    IwGxDrawPrims(IW_GX_QUAD_LIST, NULL, 4);

    // Quad for markers
    static CIwSVec3 marker_plane[4] =
    {
        CIwSVec3(  -20,  0, 0),
        CIwSVec3(  -20,  100, 0),
        CIwSVec3(   20,  100, 0),
        CIwSVec3(   20,  0, 0),
    };

    // Texture coordinates for marker
    static CIwSVec2 marker_uvs[4] =
    {
        CIwSVec2(1 << 12, 1 << 12),
        CIwSVec2(1 << 12, 0 << 12),
        CIwSVec2(0 << 12, 0 << 12),
        CIwSVec2(0 << 12, 1 << 12),
    };

    char buffer[256];
    CIwVec3 markerPos(0,0, s);
    // draw markers at various points around compass edge using model matrix
    for (unsigned int i=0; i< NUM_LOCATIONS; ++i)
    {
        static CIwMat modelMatrix;

        // Place the markers on the edge of the compass radius
        // rotated to their correct bearing to current location
        modelMatrix.SetRotY(IW_ANGLE_FROM_DEGREES(g_Locations[i].m_Bearing));
        modelMatrix.SetTrans(modelMatrix.RotateVec(markerPos));
        IwGxSetModelMatrix(&modelMatrix);

        // Allocate a material from the IwGx global cache
        pMat = IW_GX_ALLOC_MATERIAL();
        pMat->SetModulateMode(CIwMaterial::MODULATE_NONE);
        // Use Texture on Material
        pMat->SetTexture(g_MarkerTexture);
        // Set this as the active material
        IwGxSetMaterial(pMat);

        // Set vertex stream into model space
        IwGxSetVertStreamModelSpace(marker_plane, 4);
        IwGxSetUVStream(marker_uvs);

        // Draw quad for marker
        IwGxDrawPrims(IW_GX_QUAD_LIST, NULL, 4);

        //IwGxDebugPrimAxes(modelMatrix, 100);

        // Draw in front of 3D projection
        IwGxSetScreenSpaceSlot(1);
        sprintf(buffer, "%s\n%fkm", g_Locations[i].m_Name, g_Locations[i].m_Distance);

        // render captions in front of clipping plane
        CIwVec3 viewPos = IwGxModelToView(CIwVec3::g_Zero);
        if (viewPos.z > 10)
        {
            int x, y;
            IwGxWorldToScreenXY(x,y, modelMatrix.t);//ViewToScreenXY(x, y, viewPos); //TransformViewspaceToScreenspace(viewPos);
            IwGxPrintString(x, y, buffer, 0);
        }
    }

    if (g_CompassError)
        IwGxPrintString(2, 50, "No compass data", 0);
    if (g_LocationError)
        IwGxPrintString(2, 65, "No location data", 0);

    // Flush and swap
    IwGxFlush();
    IwGxSwapBuffers();
}
