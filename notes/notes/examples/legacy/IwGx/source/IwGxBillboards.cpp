/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
// ExampleIwGxBillboards
//-----------------------------------------------------------------------------

/**
 * @page ExampleLegacyIwGxBillboards IwGx Billboards Example (Legacy)
 *
 * The following example draws a cube surrounded on all six sides by viewspace
 * billboards.
 *
 * The main functions used to achieve this are:
 *  - CIwMat::SetTrans()
 *  - CIwMat::LookAt()
 *  - CIwMat::GetTrans()
 *  - CIwMat::RotateVec()
 *  - CIwMat::TransformVec()
 *  - IwGxSetModelMatrix()
 *  - IwGxSetNormStream();
 *  - IwGxSetColStream();
 *  - IwGxSetVertStream();
 *  - IwGxDrawPrims();
 *  - IwGxGetWorldViewMatrix()
 *  - IwGxSetViewSpaceOrg();
 *  - IwGxSetVertStreamViewSpace()
 *
 * The cube coordinates are entered and then drawn in model space.
 *
 * Each billboard position is gained by transforming the relevant static vector set
 * in the array @a locations[] using the worldview transform. The billboards are
 * drawn in a for loop, by first setting the origin to be the billboard position
 * using the IwGxSetViewSpaceOrg() function, and then feeding the coordinates
 * into viewspace using the IwGxSetVertStreamViewSpace().
 *
 * The example uses the following keyboard controls:
 *
 * Translation Controls
 *  - <b>s3eKey8</b>: Translates objects along the Z-Axis in a positive direction.
 *  - <b>s3eKey2</b>: Translates objects along the Z-Axis in a negative direction.
 *  - <b>s3eKey4</b>: Translates objects along the X-Axis in a positive direction.
 *  - <b>s3eKey6</b>: Translates objects along the X-Axis in a negative direction.
 * Rotation Controls
 *  - <b>s3eKeyLeft</b>: Rotates objects clockwise around the Y-Axis.
 *  - <b>s3eKeyRight</b>: Rotates objects anti-clockwise around the Y-Axis.
 *  - <b>s3eKeyUp</b>: Rotates objects clockwise around the X-Axis.
 *  - <b>s3eKeyDown</b>: Rotates objects anti-clockwise around the X-Axis.
 *
 * Selecting one of the translation options results in the modification of the
 * translation part of the view matrix. This modification is produced by transforming the
 * relevant axis vector by the rotation part of the view matrix using the RotateVec()
 * function. The resultant value is then multiplied by five and added back into the original
 * translation part of the view matrix. The result is then applied to the view matrix using
 * the SetTrans() function.
 *
 * Selecting one of the rotation options results in the combination of the matrix
 * rotation and the view matrix using the PreMult() function. Before they are combined,
 * the matrix @a rotation is first rotated by a 5 degree angle around the relvant axis
 * using the SetAxisAngle() function.
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwGxBillboardsImage.png
 *
 * @include IwGxBillboards.cpp
 */

#include "s3e.h"
#include "IwGx.h"
#include "IwMaterial.h"
#include "IwDebugPrim.h"
#include "IwGxPrint.h"
#include "ExamplesMain.h"

// direction vectors used to navigate in this example.

CIwVec3 up, down, left, right, forward, backward;

// matrices

CIwMat viewMatrix;

// billboard locations

CIwVec3 locations[] = {
    CIwVec3(    0,     0, 0x80),
    CIwVec3(    0,     0,-0x80),
    CIwVec3(-0x80,     0, 0   ),
    CIwVec3( 0x80,     0, 0   ),
    CIwVec3(    0, -0x80, 0   ),
    CIwVec3(    0,  0x80, 0   )
};
int     num_locations = 6;

// material

CIwMaterial* defaultMaterial = NULL;

void CreateButtonsUI(int w, int h)
{
    // Create the UI
    AddButton("In: 8", 0, 70, 70, 30, s3eKey8);
    AddButton("Left: 4", 0, 110, 70, 30, s3eKey4);
    AddButton("Out: 2", w - 70, 70, 70, 30, s3eKey2);
    AddButton("Right: 6", w - 70, 110, 70, 30, s3eKey6);
}

void SurfaceChangedCallback()
{
    DeleteButtons();
    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}


//-----------------------------------------------------------------------------
// The following function initialises the vectors that will be used in
// translation and rotation of objects.
//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    IwGxRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
    IwGxInit();

    // Turn all lighting off
    IwGxLightingOff();

    // initialise vectors
    down     =  CIwVec3::g_AxisY;
    up       = -CIwVec3::g_AxisY;
    right    =  CIwVec3::g_AxisX;
    left     = -CIwVec3::g_AxisX;
    forward  =  CIwVec3::g_AxisZ;
    backward = -CIwVec3::g_AxisZ;

    // set view matrix
    viewMatrix.SetTrans(CIwVec3(0, 0, -0x1a0));
    viewMatrix.LookAt(
        viewMatrix.GetTrans(),
        CIwVec3(0, 0, 0),
        up);

    // default material
    defaultMaterial = new CIwMaterial;

    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    delete defaultMaterial;

    // Terminate
    IwGxTerminate();
    IwGxUnRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
}
//-----------------------------------------------------------------------------
// The following function observes keyboard state and sets the viewMatrix
// based on user input.
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // control camera movement
    //

    // TRANSLATION
    if (CheckButton("In: 8") == S3E_KEY_STATE_DOWN){
        viewMatrix.SetTrans(
            viewMatrix.GetTrans() +
            viewMatrix.RotateVec(forward) * 0x5);
    }

    if (CheckButton("Out: 2") == S3E_KEY_STATE_DOWN){
        viewMatrix.SetTrans(viewMatrix.GetTrans() + viewMatrix.RotateVec(backward) * 0x5);
    }

    if (CheckButton("Left: 4") == S3E_KEY_STATE_DOWN){
        viewMatrix.SetTrans(
            viewMatrix.GetTrans() +
            viewMatrix.RotateVec(left) * 0x5);
    }

    if (CheckButton("Right: 6") == S3E_KEY_STATE_DOWN){
        viewMatrix.SetTrans(
            viewMatrix.GetTrans() -
            viewMatrix.RotateVec(left) * 0x5);
    }

    // ROTATION
    CIwMat rotation;
    if (CheckCursorState() == EXCURSOR_LEFT){
        rotation.SetAxisAngle(up, IW_ANGLE_FROM_DEGREES(5));
        viewMatrix.PreMult(rotation);
    }

    if (CheckCursorState() == EXCURSOR_RIGHT){
        rotation.SetAxisAngle(down, IW_ANGLE_FROM_DEGREES(5));
        viewMatrix.PreMult(rotation);
    }

    if (CheckCursorState() == EXCURSOR_UP){
        rotation.SetAxisAngle(right, IW_ANGLE_FROM_DEGREES(5));
        viewMatrix.PreMult(rotation);
    }

    if (CheckCursorState() == EXCURSOR_DOWN){
        rotation.SetAxisAngle(left, IW_ANGLE_FROM_DEGREES(5));
        viewMatrix.PreMult(rotation);
    }

    // update the view matrix
    IwGxSetViewMatrix(&viewMatrix);

    return true;
}
//-----------------------------------------------------------------------------
// The following function renders the cube and the billboards that surround it.
// The billboards are rendered using a for loop, which changes the view space
// origin to be the location of each billboard before they are rendered.
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Set default material
    IwGxSetMaterial(defaultMaterial);

    // RENDER THE CUBE
    //
    static CIwSVec3 cubeVertices[] = {
        CIwSVec3(-0x40,-0x40,-0x40),
        CIwSVec3( 0x40,-0x40,-0x40),
        CIwSVec3(-0x40, 0x40,-0x40),
        CIwSVec3( 0x40, 0x40,-0x40),
        CIwSVec3(-0x40,-0x40, 0x40),
        CIwSVec3( 0x40,-0x40, 0x40),
        CIwSVec3(-0x40, 0x40, 0x40),
        CIwSVec3( 0x40, 0x40, 0x40)
    };

    static CIwColour cubeColours[] = {
        {0x00, 0x00, 0xff, 0xff},
        {0xff, 0xff, 0xff, 0xff},
        {0x00, 0xff, 0x00, 0xff},
        {0xff, 0x00, 0x00, 0xff},
        {0xff, 0x00, 0x00, 0xff},
        {0xff, 0xff, 0xff, 0xff},
        {0x00, 0xff, 0x00, 0xff},
        {0x00, 0x00, 0xff, 0xff}
    };

    static uint16 cubeIndices[] = {
        2, 3, 1, 0,
        1, 3, 7, 5,
        7, 6, 4, 5,
        6, 2, 0, 4,
        2, 6, 7, 3,
        1, 5, 4, 0
    };

    // Render the cube in model space.
    IwGxSetModelMatrix(&CIwMat::g_Identity);
    IwGxSetNormStream(NULL, 0);
    IwGxSetColStream(cubeColours, 8);
    IwGxSetVertStream(cubeVertices, 8);
    IwGxDrawPrims(IW_GX_QUAD_LIST, cubeIndices, 24);


    // RENDER THE BILLBOARDS
    //
    static CIwSVec3 billboardVertices[] = {
        CIwSVec3(-0x20, 0x10, 0),
        CIwSVec3( 0x20, 0x10, 0),
        CIwSVec3( 0x20,-0x10, 0),
        CIwSVec3(-0x20,-0x10, 0)
    };

    static CIwSVec3 billboardNormals[] = {
        CIwSVec3(0, 0, -IW_GEOM_ONE),
        CIwSVec3(0, 0, -IW_GEOM_ONE),
        CIwSVec3(0, 0, -IW_GEOM_ONE),
        CIwSVec3(0, 0, -IW_GEOM_ONE)
    };

    static CIwColour billboardColours[] = {
        {0xff, 0xff, 0xff, 0xff},
        {0xff, 0xff, 0xff, 0xff},
        {0xff, 0xff, 0xff, 0xff},
        {0xff, 0x00, 0x00, 0xff}
    };

    static uint16 billboardIndices[] = { 0, 1, 2, 3 };

    for (int i = 0; i < num_locations; i++){
        // get the billboard location in current viewspace
        CIwSVec3 viewSpaceLocation =
            (CIwSVec3)IwGxGetWorldViewMatrix().TransformVec(locations[i]);

        // set the viewspace origin to the billboard's
        // location in viewspace
        IwGxSetViewSpaceOrg(&viewSpaceLocation);

        // render the billboard in viewspace
        IwGxSetVertStreamViewSpace(billboardVertices, 4);
        IwGxSetColStream(billboardColours, 4);
        IwGxSetNormStream(billboardNormals, 4);
        IwGxDrawPrims(IW_GX_QUAD_LIST, billboardIndices, 4);
    }

    // Print all the text to the screen
    IwGxPrintString(2, 60, "Camera translate controls:");
    IwGxPrintString(2, IwGxGetScreenHeight() - 70, "Rotate Camera: Arrow Keys");
    RenderCursorskeys();
    IwGxPrintString(3, IwGxGetScreenHeight () - 80 - 2  * 0xa ,
        "cube in model space,", false);
    IwGxPrintString(4, IwGxGetScreenHeight () - 80 - 0xa, "billboards in viewspace");

    // Flush and swap
    IwGxFlush();
    IwGxSwapBuffers();
}
