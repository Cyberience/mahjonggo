/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
// ExampleIwGxSurfacePostProcess
//-----------------------------------------------------------------------------

/**
 * @page ExampleLegacyIwGxSurfacePostProcess IwGxSurface PostProcess Example (Legacy)
 *
 * The IwGxSurface simplies post-process type effect work. Surfaces hold a "client window"
 * rectangle that is automatically set as the active view rectangle when the surface is selected.
 * This circumvents various limitations of the underlying surface implementations
 * (like being restricted to non-power-2 textures).
 *
 * In this example we use two surfaces to apply a motion blur to a flying cube. The frame is
 * rendered to s_FrameSurface, then s_FrameSurface is blended with the contents of s_AccumulatingSurface.
 * Finally s_AccumulatingSurface is copied to the display surface.
 *
 * Extra flags are passed to surface creation:
 * @code
 *  if(!s_AccumulatingSurface->CreateSurface(NULL,
 *      IwGxGetScreenWidth(), IwGxGetScreenHeight(),
 *      CIwGxSurface::EXACT_MATCH_F | CIwGxSurface::PERSISTENT_F) )
 *  {
 *      IwError(("Surface creation failed"));
 *  }
 * @endcode
 *
 * This code creates an offscreen surface the same size as the screen (i.e. the display surface).
 *
 * The EXACT_MATCH_F flag requests a surface that is large enough to give an exact match to the dimensions
 * requested (the actual surface will probably be larger). If a large enough surface cannot be provided the
 * largest possible size will be used.
 *
 * The PERSISTENT_F flag requires the surface to be persistent - that is, when the surface is
 * switched to, it has the same contents it had last time it was selected. The accumulating buffer
 * must be persistent, so the previous frame is still present.
 *
 * @note Persistent surfaces can be considerably slower than non-persistent surfaces, so it's usually
 * best to only request this if it's strictly necessary (as in this example).
 *
 * The client window of the surface is selected when the surface is made current.
 * IwGxGetDisplayWidth/IwGxGetDisplayHeight will contain the client window dimensions.
 * When rendering from the surface's texture GetClientUVExtent() can be used to render the client
 * portion of the texture.
 *
 * @code
 *  CIwSVec2 wh((int16)IwGxGetDisplayWidth(), (int16)IwGxGetDisplayHeight());
 *  IwGxDrawRectScreenSpace(&CIwSVec2::g_Zero, &wh, &CIwSVec2::g_Zero, &pSurface->GetClientUVExtent());
 * @endcode
 *
 * This example does not handle the surface(s) not being created. A real application should be prepared for
 * this eventuality and fall back gracefully. The default fallback provides a reasonable compromise
 * for this example.
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwGxSurfacePostProcessImage.png
 *
 * @include IwGxSurfacePostProcess.cpp
 */


#include "IwGx.h"
#include "IwMaterial.h"
#include "IwTexture.h"
#include "s3eKeyboard.h"
#include "ExamplesMain.h"

// Texture object
CIwTexture* s_Texture = NULL;

// Surface for frame
CIwGxSurface* s_FrameSurface;

// Surface for accumulation
CIwGxSurface* s_AccumulatingSurface;

// Vertex data
const int16 s = 0x80;
CIwSVec3    s_Verts[8] =
{
    CIwSVec3(-s, -s, -s),
    CIwSVec3( s, -s, -s),
    CIwSVec3( s,  s, -s),
    CIwSVec3(-s,  s, -s),
    CIwSVec3(-s, -s,  s),
    CIwSVec3( s, -s,  s),
    CIwSVec3( s,  s,  s),
    CIwSVec3(-s,  s,  s),
};

// Colour data
CIwColour   s_Cols[8] =
{
    {0x00, 0x00, 0x00, 0xff},
    {0x00, 0x00, 0xff, 0xff},
    {0x00, 0xff, 0x00, 0xff},
    {0x00, 0xff, 0xff, 0xff},
    {0xff, 0x00, 0x00, 0xff},
    {0xff, 0x00, 0xff, 0xff},
    {0xff, 0xff, 0x00, 0xff},
    {0xff, 0xff, 0xff, 0xff},
};

// UV data
CIwSVec2    s_UVs[4] =
{
    CIwSVec2(0 << 12, 0 << 12),
    CIwSVec2(1 << 12, 0 << 12),
    CIwSVec2(1 << 12, 1 << 12),
    CIwSVec2(0 << 12, 1 << 12),
};

// Index stream for textured material
uint16      s_QuadStrip[4] =
{
    0, 3, 1, 2,
};

// Index stream for untextured material
uint16      s_TriStrip[20] =
{
    1, 2, 5, 6, 4, 7, 0, 3,
    3, 4, // degenerates
    4, 0, 5, 1,
    1, 3, // degenerates
    3, 7, 2, 6,
};

// Angles
CIwSVec3    s_Angles;

// Local matrix
CIwMat      s_ModelMatrix;

//-----------------------------------------------------------------------------
void RecreateSurfaces()
{
    s_FrameSurface->RecreateSurface(IwGxGetScreenWidth(), IwGxGetScreenHeight());
    s_AccumulatingSurface->RecreateSurface(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}

void ExampleInit()
{
    // Initialise
    IwGxInit();

    // Set screen clear colour
    IwGxSetColClear(0x40, 0x40, 0x40, 0x00);

    // Turn all lighting off
    IwGxLightingOff();

    // Set field of view
    IwGxSetPerspMul(0xa0);

    // Set near and far planes
    IwGxSetFarZNearZ(0x1000, 0x10);

    // Create empty texture object
    s_Texture = new CIwTexture;

    // Load image data from disk into texture
    s_Texture->LoadFromFile("./textures/testTexture_8bit.bmp");

    // "Upload" texture to VRAM
    s_Texture->Upload();

    // Create two surfaces

    // One surface to render the current frame to
    s_FrameSurface = new CIwGxSurface();
    if (!s_FrameSurface->CreateSurface(NULL,
        IwGxGetScreenWidth(), IwGxGetScreenHeight(), CIwGxSurface::EXACT_MATCH_F) )
    {
        IwError(("Surface creation failed"));
    }

    // One surface to accumulate frames into
    s_AccumulatingSurface = new CIwGxSurface();
    if (!s_AccumulatingSurface->CreateSurface(NULL,
        IwGxGetScreenWidth(), IwGxGetScreenHeight(),
        CIwGxSurface::EXACT_MATCH_F | CIwGxSurface::PERSISTENT_F ) )
    {
        IwError(("Surface creation failed"));
    }

    // If the surface dimensions have changed, recreate the surfaces at the new size
    IwGxRegister(IW_GX_SCREENSIZE, RecreateSurfaces);

    // Initialise angles
    s_Angles = CIwSVec3::g_Zero;

    // Set the view matrix along the -ve z axis
    CIwMat view = CIwMat::g_Identity;
    view.t.z = -0x200;
    IwGxSetViewMatrix(&view);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // Destroy texture
    delete s_Texture;

    // Destroy surfaces
    delete s_FrameSurface;
    delete s_AccumulatingSurface;

    // Terminate
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // Update angles
    s_Angles.x += 0x10;
    s_Angles.y += 0x20;
    s_Angles.z += 0x30;

    // Build model matrix from angles
    CIwMat  rotX, rotY, rotZ;
    rotX.SetRotX(s_Angles.x);
    rotY.SetRotY(s_Angles.y);
    rotZ.SetRotZ(s_Angles.z);
    s_ModelMatrix = rotX * rotY * rotZ;
    s_ModelMatrix.t += s_ModelMatrix.RowZ() * IW_FIXED(0.075);

    return true;
}
//-----------------------------------------------------------------------------
void RenderCube(CIwTexture* pTex)
{
    // Allocate and initialise material from the IwGx global cache
    CIwMaterial* pMat = IW_GX_ALLOC_MATERIAL();

    // Set this as the active material
    IwGxSetMaterial(pMat);

    // Set the model matrix
    IwGxSetModelMatrix(&s_ModelMatrix);

    // Set the (modelspace) vertex stream
    IwGxSetVertStreamModelSpace(s_Verts, 8);

    // Set the vertex colour stream
    IwGxSetColStream(s_Cols, 8);

    // Draw the untextured primitives
    IwGxDrawPrims(IW_GX_TRI_STRIP, s_TriStrip, 20);

    // Allocate and initialise another material
    pMat = IW_GX_ALLOC_MATERIAL();

    // Set the diffuse map
    pMat->SetTexture(pTex);

    // Set this as the active material
    IwGxSetMaterial(pMat);

    // Set the vertex UV stream
    IwGxSetUVStream(s_UVs);

    // Clear the vertex colour stream
    IwGxSetColStream(NULL);

    // Draw the textured primitives
    IwGxDrawPrims(IW_GX_QUAD_STRIP, s_QuadStrip, 4);

    // End drawing
    IwGxFlush();
}


void RenderSurfaceTexture(CIwGxSurface* pSurface, bool blended)
{
    // Create a material in the data cache
    CIwMaterial* pMat = IW_GX_ALLOC_MATERIAL();

    // Set the surface's texture
    pMat->SetTexture(pSurface->GetTexture());

    // If we are rendering to the accumulating surface we blend 50:50 with the current contents
    if (blended)
    {
        pMat->SetAlphaMode(CIwMaterial::ALPHA_HALF);
    }

    // Set the material to be full brightness
    pMat->SetColAmbient(0xffffffff);

    // Select this as the active material
    IwGxSetMaterial(pMat);

    // Fill the current view rectangle with the surface texture
    CIwSVec2 wh((int16)IwGxGetDisplayWidth(), (int16)IwGxGetDisplayHeight());
    IwGxDrawRectScreenSpace(&CIwSVec2::g_Zero, &wh, &CIwSVec2::g_Zero, &pSurface->GetClientUVExtent());
}


void ExampleRender()
{
    // Switch to the frame surface
    s_FrameSurface->MakeCurrent();

    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Render a flying cube
    RenderCube(s_Texture);

    // Switch to the accumulating surface
    s_AccumulatingSurface->MakeCurrent();

    // Clear the depth buffer only
    IwGxClear(IW_GX_DEPTH_BUFFER_F);

    // Blend the frame texture with the accumulated surface
    RenderSurfaceTexture(s_FrameSurface, true);

    // Switch back to the main surface
    CIwGxSurface::MakeDisplayCurrent();

    // Clear the depth buffer only
    IwGxClear(IW_GX_DEPTH_BUFFER_F);

    // Render the accumulated surface
    RenderSurfaceTexture(s_AccumulatingSurface, false);

    // Have to force a redraw of the example's interface
    RenderButtons();
    RenderSoftkeys();

    // Flush the draw commands
    IwGxFlush();

    // Swap buffers
    IwGxSwapBuffers();
}
