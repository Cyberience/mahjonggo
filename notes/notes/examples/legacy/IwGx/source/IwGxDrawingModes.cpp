/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
//-----------------------------------------------------------------------------
// ExampleIwGxDrawingModes
//-----------------------------------------------------------------------------

/**
 * @page ExampleLegacyIwGxDrawingModes IwGx Drawing Modes Example (Legacy)
 *
 * The example application draws a set of polygons to screen using a specific
 * drawing mode.
 *
 * The main functions used to achieve this are:
 *  - IwGxSetVertStreamScreenSpace()
 *  - IwGxDrawPrims()
 *
 * The example demonstrates the following primitive types:
 *  - Tri list
 *  - Tri strip
 *  - Tri fan
 *  - Quad list
 *  - Quad strip
 *  - Line list
 *  - Line strip
 *
 * The application allows you to toggle between drawing modes
 * using the up and down arrow keys. Each type
 * uses the index stream differently, so the fixed array of screenspace coords
 * are effectively stitched together differently.
 *
 * The IwGxSetVertStreamScreenSpace() function is used to feed in the coordinates
 * as well as specifying the number of coordinates. And the IwGxDrawPrims() function
 * lets you specify the primitive type, the indices and the number of indices being
 * passed.
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwGxDrawingModesImage.png
 *
 * @include IwGxDrawingModes.cpp
 */

#include "IwGx.h"
#include "IwGxPrint.h"
#include "IwMaterial.h"
#include "s3eKeyboard.h"
#include "s3ePointer.h"
#include "ExamplesMain.h"

// Screenspace vertex coords
CIwSVec2* g_Coords = NULL;

// Vertex colours
CIwColour g_Cols[10] =
{
    {0xff, 0x00, 0x00, 0x00},
    {0xff, 0xff, 0x00, 0x00},
    {0x00, 0xff, 0xff, 0x00},
    {0x00, 0x00, 0xff, 0x00},
    {0xff, 0x00, 0xff, 0x00},
    {0xff, 0x00, 0x00, 0x00},
    {0xff, 0xff, 0x00, 0x00},
    {0x00, 0xff, 0xff, 0x00},
    {0x00, 0x00, 0xff, 0x00},
    {0xff, 0x00, 0xff, 0x00},
};

// Primitive types to demonstrate
enum PrimType
{
    TRI_LIST,
    TRI_STRIP,
    TRI_FAN,
    QUAD_LIST,
    QUAD_STRIP,
    LINE_LIST,
    LINE_STRIP,
    MAX,
};
PrimType g_PrimType = TRI_LIST;

// Tri-list example: display every other triangle
uint16  g_IndsTriList[12] = {0, 5, 1, 1, 6, 2, 2, 7, 3, 3, 8, 4};

// Tri-strip example: display all triangles
uint16  g_IndsTriStrip[10] = {0, 5, 1, 6, 2, 7, 3, 8, 4, 9};

// Tri-fan example: 4 large triangles
uint16  g_IndsTriFan[6] = {7, 9, 4, 2, 0, 5};

// Quad-list example: display first and last quads
uint16  g_IndsQuadList[8] = {0, 5, 6, 1, 3, 8, 9, 4};

// Note.. for QUAD_STRIP, we can re-use the tri-strip inds!

// Line-list example: display all lines
uint16  g_IndsLineList[26] = {0, 1, 1, 2, 2, 3, 3, 4, 5, 6, 6, 7, 7, 8, 8, 9, 0, 5, 1, 6, 2, 7, 3, 8, 4, 9};

// Line-strip example: display continuous U shapes
uint16  g_IndsLineStrip[10] = {0, 5, 6, 1, 2, 7, 8, 3, 4, 9};

// Primitive type names to display
const char* g_PrimNames[] =
{
    "TRI_LIST",
    "TRI_STRIP",
    "TRI_FAN",
    "QUAD_LIST",
    "QUAD_STRIP",
    "LINE_LIST",
    "LINE_STRIP",
};

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    IwGxInit();

    // Set screen clear colour
    IwGxSetColClear(0x40, 0x40, 0x40, 0x00);

    // Turn all lighting off
    IwGxLightingOff();

/*  Allocate and set up screenspace vertex coords in the following way:
    0--1--2--3--4
    |  |  |  |  |
    5--6--7--8--9
*/
    g_Coords = new CIwSVec2[10];
    int16 x0 = 0x8;
    int16 x1 = (int16)IwGxGetScreenWidth() - 0x8;
    int16 dx = (x1 - x0) / 4;
    int16 y0 = (int16)IwGxGetScreenHeight() / 2 - 0x10;
    CIwSVec2* pCoord = g_Coords;
    for (int16 y = 0; y < 2; y++)
    {
        for (int16 x = 0; x < 5; x++)
        {
            pCoord->x = x0 + dx * x;
            pCoord->y = y0 + y * 0x20;
            pCoord++;
        }
    }
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    delete[] g_Coords;

    // Terminate
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // Press up/down to change displayed primitive type
    if (CheckCursorState() == EXCURSOR_UP )
        g_PrimType = (PrimType)((g_PrimType == MAX - 1) ? 0 : (g_PrimType + 1));
    else
    if (CheckCursorState() == EXCURSOR_DOWN )
        g_PrimType = (PrimType)((g_PrimType == 0) ? (MAX - 1) : (g_PrimType - 1));

    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Allocate a material from the IwGx global cache
    CIwMaterial* pMat = IW_GX_ALLOC_MATERIAL();

    // Set this as the active material
    IwGxSetMaterial(pMat);

    // Set screenspace vertex coords
    IwGxSetVertStreamScreenSpace(g_Coords, 10);

    // Set vertex colours
    IwGxSetColStream(g_Cols, 10);

    // Draw primitives
    switch(g_PrimType)
    {
    case TRI_LIST:
        IwGxDrawPrims(IW_GX_TRI_LIST, g_IndsTriList, 12);
        break;

    case TRI_STRIP:
        IwGxDrawPrims(IW_GX_TRI_STRIP, g_IndsTriStrip, 10);
        break;

    case TRI_FAN:
        IwGxDrawPrims(IW_GX_TRI_FAN, g_IndsTriFan, 6);
        break;

    case QUAD_LIST:
        IwGxDrawPrims(IW_GX_QUAD_LIST, g_IndsQuadList, 8);
        break;

    case QUAD_STRIP:
        // Note: we can re-use the tri-strip inds!
        IwGxDrawPrims(IW_GX_QUAD_STRIP, g_IndsTriStrip, 10);
        break;

    case LINE_LIST:
        IwGxDrawPrims(IW_GX_LINE_LIST, g_IndsLineList, 26);
        break;

    case LINE_STRIP:
        IwGxDrawPrims(IW_GX_LINE_STRIP, g_IndsLineStrip, 10);
        break;

    default:
        break;
    }

    // Display primitive type used
    IwGxPrintString(0x2, IwGxGetScreenHeight() - 80, g_PrimNames[g_PrimType]);
    IwGxPrintString(0x2, IwGxGetScreenHeight() - 70, "Use Up/Down to change mode");
    RenderCursorskeys();

    // Flush and swap
    IwGxFlush();
    IwGxSwapBuffers();
}
