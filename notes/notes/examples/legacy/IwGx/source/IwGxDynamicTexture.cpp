/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
// ExampleIwGxDynamicTexture
//-----------------------------------------------------------------------------

/**
 * @page ExampleLegacyIwGxDynamicTexture IwGx Dynamic Texture Example (Legacy)
 *
 * This example demonstrates dynamically changing texture contents from client memory.
 *
 * Generic texture uploading can be far too slow to use for realtime updating.
 *
 * The usual steps involved are:
 * - (1) Convert the incoming format to a renderer-compatible format.
 * - (2) Resize the texture to meet renderer-requirements (often power of 2)
 * - (3) Generate a set of mipmaps
 * - (4) Copy to renderer storage (VRAM)
 * - (5) Convert to internal renderer format.
 *
 * Steps 1-3 take place within IwGx and this example demonstates skipping them all for
 * optimal upload speed.
 *
 * Step 4 is obligatory, and step 5 varies between renderers. For example, on MBX based devices
 * (like the iPhone) a final swizzling conversion is always performed by the GPU on the first render.
 * The only way to avoid this cost on iPhone is to upload in a compressed format - generating
 * dynamic contents in this format is too difficult for most applications.
 *
 * In this example, we avoid step (1) by choosing a renderer native format. The final format passed to
 * the HW/SW can be queried and set using Set/GetFormatHW/SW() functions. Note that setting an incompatible
 * format will generate a runtime error. Unfortunately, apart from the RGB_565 format used in this example,
 * the Marmalade SW renderer's native formats are reversed compared to GL, meaning for optimal performance
 * a seperate SW loop maybe required for alpha formats etc.
 *
 * Note that GL will only accept tightly packed images, so to avoid a copy the incoming texels must have a
 * pitch equal to the row byte length i.e. no padding.
 *
 * We avoid step (2) by using a power of 2 size.
 *
 * We avoid step (3) by disabling mipmapping.
 *
 * Dynamic texturing is suitable for transfering data from client memory. If the application wants to use
 * IwGx to render to a texture it should use IwGxSurface instead.
 *
 * This example uses rand() to scribble on a 512x512 texture to simulate a corrosion effect. On most
 * Marmalade target devices, it will achieve the basic 20fps required for the example framework. The expense
 * of uploading the texture is typically proportional to the amount of the texels in the texture being uploaded,
 * so splitting a large texture into smaller subtextures will generally perform better if fewer texels end up
 * being uploaded. For example, rather than using a single 512x512 texture to generate a 320x480 region, it
 * generally be faster to use four tiles, two 256x256, and two 64x256.
 *
 * Currently IwGx does not support uploading a sub rectangle: this is not much faster on current target HW.
 *
 * @include IwGxDynamicTexture.cpp
 */


#include "IwGx.h"
#include "IwMaterial.h"
#include "IwTexture.h"
#include "s3eKeyboard.h"
#include "ExamplesMain.h"

// Texture object
CIwTexture* s_Texture = NULL;
CIwSVec2 s_SnakePos;
uint16 s_SnakeMask = 0xffff;
#define TEXTURE_FORMAT CIwImage::RGB_565

// Vertex data
const int16 s = 0x80;
CIwSVec3    s_Verts[8] =
{
    CIwSVec3(-s, -s, -s),
    CIwSVec3( s, -s, -s),
    CIwSVec3( s,  s, -s),
    CIwSVec3(-s,  s, -s),
    CIwSVec3(-s, -s,  s),
    CIwSVec3( s, -s,  s),
    CIwSVec3( s,  s,  s),
    CIwSVec3(-s,  s,  s),
};

// Colour data
CIwColour   s_Cols[8] =
{
    {0x00, 0x00, 0x00},
    {0x00, 0x00, 0xff},
    {0x00, 0xff, 0x00},
    {0x00, 0xff, 0xff},
    {0xff, 0x00, 0x00},
    {0xff, 0x00, 0xff},
    {0xff, 0xff, 0x00},
    {0xff, 0xff, 0xff},
};

// UV data
CIwSVec2    s_UVs[4] =
{
    CIwSVec2(0 << 12, 0 << 12),
    CIwSVec2(1 << 12, 0 << 12),
    CIwSVec2(1 << 12, 1 << 12),
    CIwSVec2(0 << 12, 1 << 12),
};

// Index stream for textured material
uint16      s_QuadStrip[4] =
{
    0, 3, 1, 2,
};

// Index stream for untextured material
uint16      s_TriStrip[20] =
{
    1, 2, 5, 6, 4, 7, 0, 3,
    3, 4, // degenerates
    4, 0, 5, 1,
    1, 3, // degenerates
    3, 7, 2, 6,
};

// Angles
CIwSVec3    s_Angles;

// Local matrix
CIwMat      s_ModelMatrix;

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    IwGxInit();

    // Set screen clear colour
    IwGxSetColClear(0x40, 0x40, 0x40, 0x00);

    // Turn all lighting off
    IwGxLightingOff();

    // Set field of view
    IwGxSetPerspMul(0xa0);

    // Set near and far planes
    IwGxSetFarZNearZ(0x1000, 0x10);

    // Create empty texture object
    s_Texture = new CIwTexture;

    {
        //Load image in original format
        CIwImage img;
        img.LoadFromFile("Image.bmp");

        //Convert to working format
        s_Texture->GetImage().SetFormat(TEXTURE_FORMAT);
        img.ConvertToImage(&s_Texture->GetImage());
    }

    // Set IwGx to upload in the working format
    s_Texture->SetFormatSW(TEXTURE_FORMAT);
    s_Texture->SetFormatHW(TEXTURE_FORMAT);

    // Instruct IwGx to keep a copy in client memory
    s_Texture->SetModifiable(true);

    // Disable mipmapping for upload efficiency
    s_Texture->SetMipMapping(false);

    // "Upload" texture to VRAM
    s_Texture->Upload();

    // Initialise angles
    s_Angles = CIwSVec3::g_Zero;

    // Set the view matrix along the -ve z axis
    CIwMat view = CIwMat::g_Identity;
    view.t.z = -0x200;
    IwGxSetViewMatrix(&view);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // Destroy texture
    delete s_Texture;

    // Terminate
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // Update angles
    s_Angles.z += 0x30;

    // Build model matrix from angles
    s_ModelMatrix.SetRotX(IW_ANGLE_PI/8+10);
    s_ModelMatrix.PostRotateZ(s_Angles.z);
    s_ModelMatrix.t.z -= 0x100;

    //Lift constants out of the loop
    int width = (int)s_Texture->GetWidth();
    int height = (int)s_Texture->GetHeight();
    uint16* texels = ((uint16*)s_Texture->GetTexels());

    // Move 'Snake' randomly to scribble on the texture
    for (uint32 i = 0; i < 200; i++)
    {
        switch(rand() % 4)
        {
        case 0:
            s_SnakePos.x++;
            if (s_SnakePos.x >= (int)width )
                s_SnakePos.x = 0;
            s_SnakeMask--;
            break;
        case 1:
            s_SnakePos.x--;
            if (s_SnakePos.x < 0)
                s_SnakePos.x = (int)width-1;
            s_SnakeMask -= 0xf0;
            break;
        case 2:
            s_SnakePos.y++;
            if (s_SnakePos.y >= (int)height )
                s_SnakePos.y = 0;
            s_SnakeMask -= 0xf00;
            break;
        case 3:
            s_SnakePos.y--;
            if (s_SnakePos.y < 0)
                s_SnakePos.y = (int)height-1;
            s_SnakeMask -= 0xf000;
            break;
        }

        //Modify the snake texture
        texels[s_SnakePos.y * width + s_SnakePos.x] -= 0x8f;
    }

    return true;
}
//-----------------------------------------------------------------------------
void RenderCube(CIwTexture* pTex)
{
    // Allocate and initialise material from the IwGx global cache
    CIwMaterial* pMat = IW_GX_ALLOC_MATERIAL();

    // Set this as the active material
    IwGxSetMaterial(pMat);

    // Set the model matrix
    IwGxSetModelMatrix(&s_ModelMatrix);

    // Set the (modelspace) vertex stream
    IwGxSetVertStreamModelSpace(s_Verts, 8);

    // Set the vertex colour stream
    IwGxSetColStream(s_Cols, 8);

    // Draw the untextured primitives
    IwGxDrawPrims(IW_GX_TRI_STRIP, s_TriStrip, 20);

    // Allocate and initialise another material
    pMat = IW_GX_ALLOC_MATERIAL();

    // Modulate by light grey
    pMat->SetColAmbient(0xfff0f0f0);

    // Set the diffuse map
    pMat->SetTexture(pTex);

    // Set this as the active material
    IwGxSetMaterial(pMat);

    // Set the vertex UV stream
    IwGxSetUVStream(s_UVs);

    // Clear the vertex colour stream
    IwGxSetColStream(NULL);

    // Draw the textured primitives
    IwGxDrawPrims(IW_GX_QUAD_STRIP, s_QuadStrip, 4);

    // End drawing
    IwGxFlush();
}

void ExampleRender()
{
    // Render a cube textured with s_Texture
    RenderCube(s_Texture);

    // Swap buffers
    IwGxSwapBuffers();

    // Upload the texels
    s_Texture->ChangeTexels(s_Texture->GetTexels(), TEXTURE_FORMAT);
}
