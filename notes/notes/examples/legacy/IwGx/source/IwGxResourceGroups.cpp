/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
//-----------------------------------------------------------------------------
// ExampleIwGxResourceGroups
//-----------------------------------------------------------------------------

/**
 * @page ExampleLegacyIwGxResourceGroups IwGx Resource Groups Example (Legacy)
 *
 * The following example demonstrates resource loading via GROUP files.
 * The example is identical in appearance to IwGxUVAnimation - the
 * difference being that the texture and material resources are loaded through
 * a GROUP file.
 *
 * The important functions required to achieve this are:
 *  - CIwResManager::LoadGroup()
 *  - CIwResManager::GetGroupNamed()
 *  - CIwResManager::GetResNamed()
 *
 * The F6 debug menu now contains a submenu for the resource manager
 * singleton - explore the named group and named resources.
 *
 * @include IwGxResourceGroups.cpp
 */

#include "IwGx.h"
#include "IwGxPrint.h"
#include "IwMaterial.h"
#include "IwMenu.h"
#include "IwResHandlerImage.h"
#include "IwResHandlerMTL.h"
#include "IwResManager.h"
#include "IwTextParserITX.h"

// Vertex stream
const int16 s = 0x80;
const int16 p = 0xc0;
CIwSVec3    s_Verts[6] =
{
    CIwSVec3(0, 0, -p),
    CIwSVec3(-s, -s, 0),
    CIwSVec3( s, -s, 0),
    CIwSVec3( s,  s, 0),
    CIwSVec3(-s,  s, 0),
    CIwSVec3(0, 0,  p),
};

// UV stream
CIwSVec2        s_UVs[6] =
{
    CIwSVec2(0 << 12, 0 << 12),
    CIwSVec2(1 << 12, 0 << 12),
    CIwSVec2(1 << 12, 0 << 12),
    CIwSVec2(1 << 12, 0 << 12),
    CIwSVec2(1 << 12, 0 << 12),
    CIwSVec2(0 << 12, 0 << 12),
};

// Index streams
uint16      s_TriFan0[6] =
{
    0, 4, 3, 2, 1, 4,
};
uint16      s_TriFan1[6] =
{
    5, 1, 2, 3, 4, 1,
};

// Angles
CIwSVec3    s_Angles;

// Local matrix
CIwMat      s_ModelMatrix;

// Materials
CIwMaterial* s_MaterialCel;
CIwMaterial* s_MaterialScroll;

//--------------------------------------------------------------------------------
CIwMenu* DebugCreateMainMenu()
{
    IW_CALLSTACK("DebugCreateMainMenu")

    CIwMenu* pMenu = new CIwMenu;

#ifdef IW_DEBUG
    pMenu->AddItem(new CIwMenuItemGx);
    pMenu->AddItem(new CIwMenuItemResManager);
#endif
    return pMenu;
}
//-----------------------------------------------------------------------------
// The following function sets the basic display settings and loads the
// required resource groups.
//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    IwGxInit();

    // Set field of view
    IwGxSetPerspMul(0xa0);

    // Set near and far planes
    IwGxSetFarZNearZ(0x400, 0x10);

    // Initialise angles
    s_Angles = CIwSVec3::g_Zero;

    // Create the resource manager singleton
    IwResManagerInit();

    // Parse the GROUP file, which will load the textures and materials
    IwGetResManager()->LoadGroup("IwGxResourceGroups.group");

    // Get a pointer to the named group
    CIwResGroup* pGroup = IwGetResManager()->GetGroupNamed("Example");

    // Get and store pointers to the material resources
    s_MaterialCel = (CIwMaterial*)pGroup->GetResNamed("cel", IW_GX_RESTYPE_MATERIAL);
    s_MaterialScroll = (CIwMaterial*)pGroup->GetResNamed("scroll", IW_GX_RESTYPE_MATERIAL);

    // Set the view matrix along the -ve z axis
    CIwMat view = CIwMat::g_Identity;
    view.t.z = -0x200;
    IwGxSetViewMatrix(&view);

    // Set up the menu manager
    new CIwMenuManager;
    IwGetMenuManager()->SetTextCallback(IwGxPrintMenuCallback);
    IwGetMenuManager()->SetMainMenuFn(DebugCreateMainMenu);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    delete IwGetMenuManager();

    // Destroy the resource manager singleton. This destroys all groups.
    IwResManagerTerminate();

    // Terminate
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
// The following function updates the model matrix.
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // Update angles
    s_Angles.x += 0x10;
    s_Angles.y += 0x24;
    s_Angles.z += 0x38;

    // Build model matrix rotation from angles
    CIwMat rotX, rotY, rotZ;
    rotX.SetRotX(s_Angles.x);
    rotY.SetRotY(s_Angles.y);
    rotZ.SetRotZ(s_Angles.z);
    s_ModelMatrix.CopyRot(rotX * rotY * rotZ);

    // Update menu manager
    IwGetMenuManager()->Update();

    // Update IwGx state time stamp
    IwGxTickUpdate();
    return true;
}
//-----------------------------------------------------------------------------
// The following function renders the screenspace cel animation and the scroll
// animation in modelspace.
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Start with no lighting
    IwGxLightingOff();

    //-------------------------------------------------------------------------
    // Render screenspace quad with cel animation
    //-------------------------------------------------------------------------
    IwGxSetMaterial(s_MaterialCel);

    static CIwSVec2 xy(0x08, 0x28);
    static CIwSVec2 wh(0x40, 0x40);
    static CIwSVec2 uv(0 << 12, 0 << 12);
    static CIwSVec2 duv(1 << 11, 1 << 11);
    IwGxSetScreenSpaceSlot(-1);
    IwGxDrawRectScreenSpace(&xy, &wh, &uv, &duv);

    //-------------------------------------------------------------------------
    // Render diamond shape with scroll animation
    //-------------------------------------------------------------------------
    IwGxSetMaterial(s_MaterialScroll);

    IwGxSetModelMatrix(&s_ModelMatrix);
    IwGxSetVertStream(s_Verts, 6);
    IwGxSetUVStream(s_UVs);
    IwGxDrawPrims(IW_GX_TRI_FAN, s_TriFan0, 6);
    IwGxDrawPrims(IW_GX_TRI_FAN, s_TriFan1, 6);

    // End drawing
    IwGxFlush();

    // Render menu manager
    IwGetMenuManager()->Render();
    IwGxFlush();

    // Swap buffers
    IwGxSwapBuffers();
}
