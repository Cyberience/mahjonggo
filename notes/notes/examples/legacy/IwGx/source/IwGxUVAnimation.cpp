/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleLegacyIwGxUVAnimation IwGx UV Animation Example (Legacy)
 *
 * The following example demonstrates how to create UV animation.
 * The example illustrates two different types of animation, scroll and cel.
 *
 * The main classes used to achieve this are:
 *  - CIwSVec2
 *  - CIwTexture
 *  - CIwMaterial
 *
 * The main functions used to achieve this are:
 *  - CIwMaterial::SetName()
 *  - CIwTexture::LoadFromFile()
 *  - CIwTexture::Upload()
 *  - CIwMaterial::SetTexture()
 *  - CIwMaterial::CreateAnim()
 *  - CIwMaterial::SetAnimCelW()
 *  - CIwMaterial::SetAnimCelH()
 *  - CIwMaterial::SetAnimCelPeriod()
 *  - IwGxSetMaterial()
 *  - IwGxSetUVStream()
 *  - IwGxSetColStream()
 *  - IwGxDrawPrims()
 *
 * This example first sets the vertices for the screenspace quad in which
 * the animations will be played. It then sets the UVs, indicating the
 * point at which the animation should start from. The cel UVs set the
 * the starting point of the animation to be the first 1/16
 * (the 1/4 of the width and 1/4 of the height) of the texture at
 * any one time. The scroll UVs will allow the animation to display 1/256 of
 * the animation at any one time.
 *
 * A material and texture is object is instantiated for each animation. A
 * texture is then uploaded and set on the material using the LoadFromFile(),
 * Upload() and SetTexture() functions.
 *
 * Based on the Keyboard state only one animation is played, pressing 1 starts
 * the scroll animation and pressing 2 returns to the cel animation.
 *
 * The cel animation uses the CreateAnim() to create the animation. The
 * SetAnimCelW() function sets the width of the cels, SetAnimCelH() sets
 * the height of the cels and SetAnimCelPeriod() sets the amount of updates
 * that must occur before the next cel is displayed.
 *
 * The IwGxSetMaterial() function is used to set the appropriate material
 * based on the keyboard input. The UVs for each animation are then set using
 * the IwGxSetUVStream() function.
 *
 * @note For more information on creating UV animation,
 * see the @ref materialuvanimation "Material UV Animation" section in
 * the <i>IwGx API Documentation</i>.
 *
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwGxUVAnimationImage.png
 *
 * @include IwGxUVAnimation.cpp
 */

#include "s3e.h"
#include "IwGx.h"
#include "IwGxPrint.h"
#include "IwMaterial.h"
#include "IwMenu.h"
#include "IwTexture.h"
#include "ExamplesMain.h"

bool animSwitch;


// Texture objects
CIwTexture* s_Textures[2];

//Vertex data
CIwSVec2 anim_xy[4];

// UV data
CIwSVec2    cel_UVs[4];
CIwSVec2    scroll_UVs[4];

// Materials
CIwMaterial* s_MaterialCel;
CIwMaterial* s_MaterialScroll;

void CreateButtonsUI(int w, int h)
{
    // Create the UI layout
    // Print the program instruction.
    AddButton("Select scroll animation: 1", 2, h - 60, 170, 20, s3eKey1);
    AddButton("Select cell animation: 2",  2, h - 30, 160, 20, s3eKey2);
}

void SurfaceChangedCallback()
{
    DeleteButtons();
    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    IwGxRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
    IwGxInit();

    animSwitch = false;

    int16 offset = 32;

    int16 midx = (int16)IwGxGetScreenWidth()/2;
    int16 midy = (int16)IwGxGetScreenHeight()/2;

// Vertex stream
// Set up screenspace vertex coords

    //cel animation screen coordinates
    int16 animXLeft = midx - offset;
    int16 animXRight = midx + offset;
    int16 animYTop = midy - offset;
    int16 animYBottom = midy + offset;

    anim_xy[0] = CIwSVec2(animXLeft, animYTop);
    anim_xy[2] = CIwSVec2(animXRight, animYTop);
    anim_xy[3] = CIwSVec2(animXRight, animYBottom);
    anim_xy[1] = CIwSVec2(animXLeft, animYBottom);

    //Set up the UVs for cel animation
    cel_UVs[0] = CIwSVec2(0 << 12, 0 << 12);
    cel_UVs[2] = CIwSVec2(1 << 10, 0 << 12);
    cel_UVs[3] = CIwSVec2(1 << 10, 1 << 10);
    cel_UVs[1] = CIwSVec2(0 << 12, 1 << 10);

    //Set up the UVs for scroll animation
    scroll_UVs[0] = CIwSVec2(0 << 12, 0 << 12);
    scroll_UVs[2] = CIwSVec2(1 << 4, 0 << 12);
    scroll_UVs[3] = CIwSVec2(1 << 4, 1 << 12);
    scroll_UVs[1] = CIwSVec2(0 << 12, 1 << 12);

    // Initialise material to use UV animation for "cel" animation
    s_MaterialCel = new CIwMaterial;
    s_MaterialCel->SetName("Cel");
    s_Textures[0] = new CIwTexture;
    s_Textures[0]->LoadFromFile("./textures/numbers.bmp");
    s_Textures[0]->Upload();
    s_MaterialCel->SetTexture(s_Textures[0]);
    s_MaterialCel->CreateAnim();
    s_MaterialCel->SetAnimCelW(0x20);
    s_MaterialCel->SetAnimCelH(0x20);
    s_MaterialCel->SetAnimCelPeriod(10);

    // Initialise material to use UV animation for "scrolling"
    s_MaterialScroll = new CIwMaterial;
    s_MaterialScroll->SetName("Scroll");
    s_Textures[1] = new CIwTexture;
    s_Textures[1]->LoadFromFile("./textures/glow.bmp");
    s_Textures[1]->Upload();
    s_MaterialScroll->SetTexture(s_Textures[1]);
    s_MaterialScroll->CreateAnim();

    // Note that this is equivalent to SetAnimCelW(0x1) - but the concept of a U or V offset makes more
    // sense when defining a scrolling animation
    s_MaterialScroll->SetAnimOfsU(0x1);

    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    delete s_Textures[0];
    delete s_Textures[1];
    delete s_MaterialCel;
    delete s_MaterialScroll;

    // Terminate
    IwGxTerminate();
    IwGxUnRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    //Press 1 to play the scroll animation and 2 to reinstate the cel animation.
    if (CheckButton("Select scroll animation: 1") & S3E_KEY_STATE_PRESSED)
    {
            animSwitch = true;
    }
    else if (CheckButton("Select cell animation: 2") & S3E_KEY_STATE_PRESSED)
    {
            animSwitch = false;
    }


    // Update IwGx state time stamp
    IwGxTickUpdate();
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Start with no lighting
    IwGxLightingOff();

    //-------------------------------------------------------------------------
    // Render screenspace quad with cel animation
    //-------------------------------------------------------------------------
    if (animSwitch == false)
    {
        IwGxSetMaterial(s_MaterialCel);

        // Set the vertex UV stream
        IwGxSetUVStream(cel_UVs);
    }


    //-------------------------------------------------------------------------
    // Render screenspace quad with scroll animation
    //-------------------------------------------------------------------------
    if (animSwitch == true)
    {
        IwGxSetMaterial(s_MaterialScroll);

        // Set the vertex UV stream
        IwGxSetUVStream(scroll_UVs);
    }

    // Clear the vertex colour stream
    IwGxSetColStream(NULL);

    // Set Vertex Stream
    IwGxSetVertStreamScreenSpace(anim_xy, 4);

    // Draw the textured primitives
    IwGxDrawPrims(IW_GX_QUAD_STRIP, NULL, 4);

    // End drawing
    IwGxFlush();

    // Swap buffers
    IwGxSwapBuffers();
}
