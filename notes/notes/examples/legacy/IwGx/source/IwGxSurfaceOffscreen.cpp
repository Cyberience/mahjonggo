/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
//-----------------------------------------------------------------------------
// ExampleIwGxSurfaceOffscreen
//-----------------------------------------------------------------------------

/**
 * @page ExampleLegacyIwGxSurfaceOffscreen IwGxSurface Offscreen Example (Legacy)
 *
 * Surfaces can be used to perform offscreen rendering. To use a surface you
 * must:
 *  -# Create the destination surface.
 *  -# Select the destination surface as the current surface.
 *  -# Perform rendering as usual.
 *  -# Select the display as the current surface.
 *  -# Perform rendering using the surface's attached texture.
 *
 * In this example we render a cube and then render the cube again using the
 * previously rendered cube as one of the faces.
 *
 * The code to create the surface is:
 * @code
 *  // Create a 256x256 surface. A NULL texture pointer is passed
 *  // to indicate that the surface should create a texture.
 *  if(!s_Surface->CreateSurface(NULL, 256, 256, 0) )
 *      IwError(("Surface creation failed"));
 * @endcode
 *
 * This code creates an offscreen surface that can be rendered to.
 *
 * The desired size of the surface is specified and default surface behaviour
 * is requested.
 *
 * This example does not handle the surface not being created. A real
 * application should be prepared for this eventuality and fall back
 * gracefully. The default fallback provides a reasonable compromise for this
 * demo.
 *
 * @note The resulting surface may not exactly match the requested
 * size/capabilities depending on the requirements of the current renderer.
 * Generally surfaces will be power-of-2 sizes up to some maximum.
 *
 * In order to render to the offscreen surface it must be selected as the
 * current surface.
 *
 * @code
 *  s_Surface->MakeCurrent();
 * @endcode
 *
 * All rendering will now go to s_Surface.
 *
 * Once all render commands for the offscreen surface have been issued the
 * display should be reselected.
 *
 * @code
 *  CIwGxSurface::MakeDisplayCurrent();
 * @endcode
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwGxSurfaceOffscreenImage.png
 *
 * @include IwGxSurfaceOffscreen.cpp
 */

#include "IwGx.h"
#include "IwMaterial.h"
#include "IwTexture.h"
#include "s3eKeyboard.h"
#include "ExamplesMain.h"

// Texture object
CIwTexture* s_Texture = NULL;

// Texture object to receive surface
CIwGxSurface* s_Surface;

// Vertex data
const int16 s = 0x80;
CIwSVec3    s_Verts[8] =
{
    CIwSVec3(-s, -s, -s),
    CIwSVec3( s, -s, -s),
    CIwSVec3( s,  s, -s),
    CIwSVec3(-s,  s, -s),
    CIwSVec3(-s, -s,  s),
    CIwSVec3( s, -s,  s),
    CIwSVec3( s,  s,  s),
    CIwSVec3(-s,  s,  s),
};

// Colour data
CIwColour   s_Cols[8] =
{
    {0x00, 0x00, 0x00},
    {0x00, 0x00, 0xff},
    {0x00, 0xff, 0x00},
    {0x00, 0xff, 0xff},
    {0xff, 0x00, 0x00},
    {0xff, 0x00, 0xff},
    {0xff, 0xff, 0x00},
    {0xff, 0xff, 0xff},
};

// UV data
CIwSVec2    s_UVs[4] =
{
    CIwSVec2(0 << 12, 0 << 12),
    CIwSVec2(1 << 12, 0 << 12),
    CIwSVec2(1 << 12, 1 << 12),
    CIwSVec2(0 << 12, 1 << 12),
};

// Index stream for textured material
uint16      s_QuadStrip[4] =
{
    0, 3, 1, 2,
};

// Index stream for untextured material
uint16      s_TriStrip[20] =
{
    1, 2, 5, 6, 4, 7, 0, 3,
    3, 4, // degenerates
    4, 0, 5, 1,
    1, 3, // degenerates
    3, 7, 2, 6,
};

// Angles
CIwSVec3    s_Angles;

// Local matrix
CIwMat      s_ModelMatrix;

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    IwGxInit();

    // Set screen clear colour
    IwGxSetColClear(0x40, 0x40, 0x40, 0x00);

    // Turn all lighting off
    IwGxLightingOff();

    // Set field of view
    IwGxSetPerspMul(0xa0);

    // Set near and far planes
    IwGxSetFarZNearZ(0x1000, 0x10);

    // Create empty texture object
    s_Texture = new CIwTexture;

    // Load image data from disk into texture
    s_Texture->LoadFromFile("./textures/testTexture_8bit.bmp");

    // "Upload" texture to VRAM
    s_Texture->Upload();

    // Create the offscreen surface
    s_Surface = new CIwGxSurface();

    // Create a 256x256 surface. A NULL texture pointer is passed
    // to indicate that the surface should create (and destroy) a texture.
    if (!s_Surface->CreateSurface(NULL, 256, 256, 0) )
        IwError(("Surface creation failed"));

    // Initialise angles
    s_Angles = CIwSVec3::g_Zero;

    // Set the view matrix along the -ve z axis
    CIwMat view = CIwMat::g_Identity;
    view.t.z = -0x200;
    IwGxSetViewMatrix(&view);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // Destroy texture
    delete s_Texture;

    // Destroy surface
    delete s_Surface;

    // Terminate
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // Update angles
    s_Angles.x += 0x10;
    s_Angles.y += 0x20;
    s_Angles.z += 0x30;

    // Build model matrix from angles
    CIwMat  rotX, rotY, rotZ;
    rotX.SetRotX(s_Angles.x);
    rotY.SetRotY(s_Angles.y);
    rotZ.SetRotZ(s_Angles.z);
    s_ModelMatrix = rotX * rotY * rotZ;

    return true;
}
//-----------------------------------------------------------------------------
void RenderCube(CIwTexture* pTex)
{
    // Allocate and initialise material from the IwGx global cache
    CIwMaterial* pMat = IW_GX_ALLOC_MATERIAL();

    // Set this as the active material
    IwGxSetMaterial(pMat);

    // Set the model matrix
    IwGxSetModelMatrix(&s_ModelMatrix);

    // Set the (modelspace) vertex stream
    IwGxSetVertStreamModelSpace(s_Verts, 8);

    // Set the vertex colour stream
    IwGxSetColStream(s_Cols, 8);

    // Draw the untextured primitives
    IwGxDrawPrims(IW_GX_TRI_STRIP, s_TriStrip, 20);

    // Allocate and initialise another material
    pMat = IW_GX_ALLOC_MATERIAL();

    // Set the diffuse map
    pMat->SetTexture(pTex);

    // Set this as the active material
    IwGxSetMaterial(pMat);

    // Set the vertex UV stream
    IwGxSetUVStream(s_UVs);

    // Clear the vertex colour stream
    IwGxSetColStream(NULL);

    // Draw the textured primitives
    IwGxDrawPrims(IW_GX_QUAD_STRIP, s_QuadStrip, 4);

    // End drawing
    IwGxFlush();
}


void ExampleRender()
{
    // Select the offscreen surface
    s_Surface->MakeCurrent();

    // Clear the surface
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Render a textured cube
    RenderCube(s_Texture);

    // Select the display
    CIwGxSurface::MakeDisplayCurrent();

    // Clear the main display
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Render the standard example information (this has already been rendered into
    // the main display by the example framework, but rendering the fullscreen quad will cover it).
    RenderSoftkeys();

    // Render a cube textured with s_Surface
    RenderCube(s_Surface->GetTexture());

    // Swap buffers
    IwGxSwapBuffers();
}
