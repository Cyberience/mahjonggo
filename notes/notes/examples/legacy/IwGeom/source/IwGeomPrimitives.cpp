/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleLegacyIwGeomPrimitives Primitives Example (Using Legacy IwGx Code)
 *
 * The following example demonstrates how to use the IwGeom bounding
 * box functionality. The example renders a sphere, a triangle and
 * a bounding box which bounds the entire triangle and the centre of
 * the sphere.
 *
 * The main classes used to achieve this are:
 *  - CIwVec3
 *  - CIwMat
 *  - CIwSphere
 *  - CIwBBox
 *
 * The main functions used to achieve this are:
 *  - CIwSphere::SetRadius
 *  - CIwBBox::BoundVec
 *  - CIwMat::SetIdentity
 *  - IwGxDebugPrimSphere
 *  - IwGxDebugPrimTri
 *  - IwGxDebugPrimBBox
 *  - IwGxSetViewMatrix
 *
 * The example creates a sphere primitive and a triangle primitive,
 * and then bounds these items using the CIwBBox::BoundVec to set the
 * bounding box values. To display these items the example sets the
 * view matrix and renders the sphere, triangle and bounding box using
 * the IwGxDebugPrimSphere(), IwGxDebugPrimTri() and IwGxDebugPrimBBox() functions.
 *
 * @note For more information on using bounding boxes,
 * see the @ref boundingbox "Bounding Box" section in the <i>IwGeom API Documentation</i>.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwGeomPrimitivesImage.png
 *
 * @include IwGeomPrimitives.cpp
 */
//-----------------------------------------------------------------------------

#include "IwGeom.h"

// for primitive rendering
#include "IwGx.h"
#include "IwDebugPrim.h"

//-----------------------------------------------------------------------------
struct Triangle
{
    // vertices
    CIwVec3 m_V0, m_V1, m_V2;
};
//-----------------------------------------------------------------------------

// primitives:

// 32-bit fixed point sphere
CIwSphere   s_Sphere;

// 32-bit fixed point triangle
Triangle    s_Triangle;

// 32-bit fixed point bounding box
// initialise the bounding box with the zero vector
CIwBBox     s_BoundingBox(CIwVec3::g_Zero, CIwVec3::g_Zero);

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // initialise GX
    IwGxInit();

    // initialise sphere:

    // as below, the IW_FIXED macro is used throughout this example to convert
    // floating point values to fixed point
    s_Sphere.SetRadius(IW_FIXED(0.2));
    s_Sphere.t = CIwVec3(IW_FIXED(0.1), IW_FIXED(0.1), IW_FIXED(0.3));

    // initialise triangle
    s_Triangle.m_V0 = CIwVec3(0, 0, 0);
    s_Triangle.m_V1 = CIwVec3(0, IW_FIXED(-0.3), 0);
    s_Triangle.m_V2 = CIwVec3(IW_FIXED(-0.3), 0, 0);

    // setup bounding box:

    // bound the centre of the sphere
    s_BoundingBox.BoundVec(&s_Sphere.GetTrans());

    // bound the triangle
    s_BoundingBox.BoundVec(&s_Triangle.m_V0);
    s_BoundingBox.BoundVec(&s_Triangle.m_V1);
    s_BoundingBox.BoundVec(&s_Triangle.m_V2);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // terminate GX
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // start with no lighting
    IwGxLightingOff();

    // the IwGxDebugPrim* functions are used to render primitives in wireframe
    // (only available in debug builds)

    // when rendering we translate the view so that the primitives are visible:

    CIwMat view;

    // set to the identity rotation matrix with zero translation component
    view.SetIdentity();

    // set the translation vector of the matrix such that the primitives are visible
    view.t.z = IW_FIXED(-0.7);

    // set the view matrix
    IwGxSetViewMatrix(&view);

    // render the sphere
    IwGxDebugPrimSphere(s_Sphere, IwGxGetColFixed(IW_GX_COLOUR_BLUE));

    // render the triangle
    IwGxDebugPrimTri(s_Triangle.m_V0, s_Triangle.m_V1, s_Triangle.m_V2, IwGxGetColFixed(IW_GX_COLOUR_RED));

    // render the bounding box
    IwGxDebugPrimBBox(s_BoundingBox, IwGxGetColFixed(IW_GX_COLOUR_YELLOW));

    IwGxFlush();

    // swap buffers
    IwGxSwapBuffers();
}
//-----------------------------------------------------------------------------
