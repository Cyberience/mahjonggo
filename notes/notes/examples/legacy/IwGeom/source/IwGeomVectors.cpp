/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */


/**
 * @page ExampleLegacyIwGeomVectors Vectors Example (Using Legacy IwGx Code)
 *
 * The following example demonstrates how to use the IwGeom Types
 * functionality. The example renders a rotating triangle and its normal.
 * The colour of the triangle depends on the angle its normal makes
 * with the eye.
 *
 * The main classes used to achieve this are:
 *  - CIwVec3
 *  - CIwMat
 *  - CIwColour
 *
 * The main functions used to achieve this are:
 *  - CIwVec3::Normalise()
 *  - CIwVec3::GetNormalised()
 *  - CIwVec3::operator +
 *  - CIwVec3::operator /(iwfixed)    [scale]
 *  - CIwVec3::operator *(iwfixed)    [scale]
 *  - CIwVec3::operator *(CIwVec3)    [dot product]
 *  - CIwVec3::operator ^         [cross product]
 *  - CIwColour::Set()
 *  - CIwMat::SetIdentity()
 *  - CIwMat::SetAxisAngle()
 *  - CIwMat::RotateVec()
 *  - IwGxDebugPrimTri()
 *  - IwGxSetViewMatrix()
 *
 * The example creates a triangle structure containing a number
 * of CIwVec3 objects to hold the three points of the triangle as well
 * as the triangle normal and the centre point. The structure is used
 * to create the triangle and for every render loop the triangle is
 * rotated and has its normal calculated.
 *
 * @note For more information on the various types,
 * see the @ref types "Types" section in the <i>IwGeom API Documentation</i>.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwGeomVectorsImage.png
 *
 * @include IwGeomVectors.cpp
 */

//-----------------------------------------------------------------------------

#include "IwGeom.h"

// for primitive rendering
#include "IwGx.h"
#include "IwDebugPrim.h"

//-----------------------------------------------------------------------------
struct Triangle
{
    // vertices
    CIwVec3     m_V0, m_V1, m_V2;

    // normal of the triangle
    CIwSVec3    m_Normal;

    // centre of the triangle
    CIwVec3     m_Centre;

    CIwColour   m_Colour;
};
//-----------------------------------------------------------------------------

Triangle s_Triangle;

// the angular speed of the triangle
#define TRIANGLE_ROT    0x40

//-----------------------------------------------------------------------------
// calculate and set the normal, centre and colour of the triangle
void UpdateTriangle()
{
    // the normal of the triangle is the normal of the result of the cross
    // product of two of its edges (the order of the cross product determines
    // the orientation of the triangle)
    CIwVec3 edge1 = s_Triangle.m_V1 - s_Triangle.m_V0;
    CIwVec3 edge2 = s_Triangle.m_V2 - s_Triangle.m_V0;
    s_Triangle.m_Normal = (edge1 ^ edge2).GetNormalised();

    // the centre of the triangle is the average of the sum of its vertices
    s_Triangle.m_Centre = (s_Triangle.m_V0 + s_Triangle.m_V1 + s_Triangle.m_V2) / IW_FIXED(3);

    // viewVector is a normal vector pointing along the positive z axis
    CIwVec3 viewVector(0, 0, IW_GEOM_ONE);

    // calculate the dot product of the triangle normal and viewVector;
    // since both vectors are normal, the result will be the cosine of the
    // angle between them; the absolute value of this will be in the
    // interval [0,0x1000] and can be used to scale the red component of the triangle colour
    iwfixed dot = s_Triangle.m_Normal * viewVector;

    // set the triangle colour; note that the absolute value of the product of the dot product
    // and 255 must be shifted by IW_GEOM_POINT in order to give a result in [0,255]
    s_Triangle.m_Colour.Set(255 - (uint8)((abs(dot) * 255) >> IW_GEOM_POINT), 255, 255);
}
//-----------------------------------------------------------------------------
void ExampleInit()
{
    // initialise GX
    IwGxInit();

    // initialise triangle
    s_Triangle.m_V0 = CIwVec3(0, 0, 0);
    s_Triangle.m_V1 = CIwVec3(0, IW_FIXED(-0.3), 0);
    s_Triangle.m_V2 = CIwVec3(IW_FIXED(-0.3), 0, 0);

    UpdateTriangle();
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // terminate GX
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // (arbitrarily) rotate the triangle vertices:

    // define the rotation axis
    CIwVec3 rotationAxis(IW_FIXED(1), IW_FIXED(-1), 0);
    // ensure the axis is normal
    rotationAxis.Normalise();

    // create a rotation matrix, defined by rotationAxis and TRIANGLE_ROT:
    CIwMat rot;
    rot.SetAxisAngle(rotationAxis, TRIANGLE_ROT);

    s_Triangle.m_V0 = rot.RotateVec(s_Triangle.m_V0);
    s_Triangle.m_V1 = rot.RotateVec(s_Triangle.m_V1);
    s_Triangle.m_V2 = rot.RotateVec(s_Triangle.m_V2);

    UpdateTriangle();

    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // start with no lighting
    IwGxLightingOff();

    // the IwGxDebugPrim* functions are used to render primitives in wireframe
    // (only available in debug builds)

    // when rendering we translate the view so that the primitives are visible:

    CIwMat view;

    // set to the identity rotation matrix with zero translation component
    view.SetIdentity();

    // set the translation vector of the matrix such that the primitives are visible
    view.t.z = IW_FIXED(-0.7);

    // set the view matrix
    IwGxSetViewMatrix(&view);

    // render the triangle
    IwGxDebugPrimTri(s_Triangle.m_V0, s_Triangle.m_V1, s_Triangle.m_V2, s_Triangle.m_Colour);

    // render the triangle normal
    IwGxDebugPrimLine(s_Triangle.m_Centre, s_Triangle.m_Centre + (s_Triangle.m_Normal * IW_FIXED(0.1)));

    IwGxFlush();

    // swap buffers
    IwGxSwapBuffers();
}
//-----------------------------------------------------------------------------
