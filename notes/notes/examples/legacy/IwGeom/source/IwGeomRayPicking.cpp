/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleLegacyIwGeomRayPicking Ray Picking Example (Using Legacy IwGx Code)
 *
 * The following example demonstrates how to use the IwGeom primitives
 * to intersect with a ray from the camera into the frustrum passing
 * though a given pixel position.
 *
 * The example renders a number of 3D spheres which will change colour
 * when the pointer moves over them.
 *
 * The main classes used to achieve this are:
 *  - CIwVec3
 *  - CIwSphere
 *  - CIwMat
 *
 * The main functions used to achieve this are:
 *  - CIwVec3::Normalise()
 *  - CIwVec3::operator +
 *  - CIwVec3::operator *(iwfixed)    [scale]
 *  - CIwVec3::operator *(CIwVec3)    [dot product]
 *  - CIwVec3::operator ^            [cross product]
 *  - CIwMat::SetIdentity()
 *  - CIwMat::RotateVec()
 *  - IwIntersectLineSphere()
 *  - IwGxSetViewMatrix()
 *
 * The example creates a number of randomly placed spheres and allows
 * the user to change their colour by moving the cursor over them.
 *
 * The main principle being demonstrated here is the creation of a
 * world-space ray which passes down the pixel currently under the cursor.
 *
 * The origin of this ray is the camera position.
 *
 * The direction of the ray is towards a point at the current x/y cursor
 * position offset relative to the centre of the screen and on the view plane.
 *
 * The view plane (at perspmul z) is the plane where the cross-section of
 * the frustrum has the same dimensions as the screen (in pixels).
 *
 * The direction is then rotated by the camera's transformation matrix.
 *
 * The generated ray can be used with any of IwGeom's intersection functions.
 *
 * @note For more information on the various types,
 * see the @ref types "Types" section in the <i>IwGeom API Documentation</i>.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwGeomRayPickingImage.png
 *
 * @include IwGeomRayPicking.cpp
 */

//-----------------------------------------------------------------------------

#include "IwGeom.h"

// for primitive rendering
#include "IwGx.h"
#include "IwDebugPrim.h"
#include "s3ePointer.h"

//-----------------------------------------------------------------------------

// Spheres to use
#define NUMBER_SPHERES (10)
CIwSphere s_Spheres[NUMBER_SPHERES];

// The index of the currently selected sphere
int32 s_SelectedSphere = -1;

// The view matrix
CIwMat s_ViewMatrix;

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise GX
    IwGxInit();

    // Generate spheres
    for (uint32 i = 0; i < NUMBER_SPHERES; i++)
    {
        CIwVec3 pos;
        pos.x = IwRand() % 1000;
        pos.y = IwRand() % 1000;
        pos.z = IwRand() % 1000;

        iwfixed radius = IwRand() % 100;

        s_Spheres[i] = CIwSphere(pos, radius);
    }

    // Set to the identity rotation matrix with zero translation component
    s_ViewMatrix.SetIdentity();

    // Set the translation vector of the matrix such that the spheres are visible
    s_ViewMatrix.t.x = 500;
    s_ViewMatrix.t.y = 700;
    s_ViewMatrix.t.z = -500;
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // Terminate GX
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // Select "no sphere"
    s_SelectedSphere = -1;

    // Generate a ray pointing to the view plane from the camera
    CIwVec3 dir(s3ePointerGetX() - IwGxGetScreenWidth()/2,
        s3ePointerGetY() - IwGxGetScreenHeight()/2,
        IwGxGetPerspMul());

    // Rotate into camera space
    dir = s_ViewMatrix.RotateVec(dir);

    // Normalise (NormaliseSlow is a slow but accurate version of normalise)
    dir.NormaliseSlow();

    for (uint32 i = 0; i < NUMBER_SPHERES; i++)
    {
        // Test each sphere for intersection
        if (IwIntersectLineSphere(s_ViewMatrix.t, dir, s_Spheres[i]) )
        {
            //If this sphere intersects, set it as selected.
            s_SelectedSphere = i;
        }
    }

    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Start with no lighting
    IwGxLightingOff();

    // Set the view matrix
    IwGxSetViewMatrix(&s_ViewMatrix);

    for (int32 i = 0; i < NUMBER_SPHERES; i++)
    {
        // The IwGxDebugPrim* functions are used to render primitives in wireframe
        if (i == s_SelectedSphere)
            IwGxDebugPrimSphere(s_Spheres[i], g_IwGxColours[IW_GX_COLOUR_RED]);
        else
            IwGxDebugPrimSphere(s_Spheres[i], g_IwGxColours[IW_GX_COLOUR_BLUE]);
    }

    IwGxFlush();

    // Swap buffers
    IwGxSwapBuffers();
}
//-----------------------------------------------------------------------------
