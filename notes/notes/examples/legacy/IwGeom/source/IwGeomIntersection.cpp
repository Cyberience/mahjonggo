/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleLegacyIwGeomIntersection Intersection Example (Using Legacy IwGx Code)
 *
 * Illustrates some of the geometry intersection functions by rotating a ray such that it
 * intersects with a sphere and triangle
 *
 * Procedure:
 * - initialise triangle, sphere and ray
 * - rotate the ray direction every frame, checking for intersections with the ray
 *   and the sphere and triangle
 * - set the triangle and sphere colours to red if there's an intersection and black if not
 *
 * Main classes:
 * - CIwVec3
 * - CIwMat
 * - CIwSphere
 *
 * Main functions:
 * - IwIntersectLineTriNorm
 * - IwIntersectLineSphere
 * - CIwVec3::GetNormalised
 * - CIwVec3 operators
 * - CIwMat::SetIdentity
 * - CIwMat::SetRot*
 * - CIwMat::RotateVec
 * - IwGxDebugPrimTri
 * - IwGxDebugPrimSphere
 * - IwGxDebugPrimLine
 * - IwGxSetViewMatrix
 */
//-----------------------------------------------------------------------------

#include "IwGeom.h"

// for primitive rendering
#include "IwGx.h"
#include "IwDebugPrim.h"

//-----------------------------------------------------------------------------
struct Triangle
{
    // vertices
    CIwVec3     m_V0, m_V1, m_V2;

    // normal of the triangle
    CIwSVec3    m_Normal;

    // colour of the triangle
    CIwColour   m_Colour;
};
//-----------------------------------------------------------------------------
struct Ray
{
    // origin of the ray
    CIwVec3     m_Origin;

    // direction of the ray
    CIwVec3     m_Direction;
};
//-----------------------------------------------------------------------------
struct ColourSphere
{
    CIwSphere   m_Sphere;
    CIwColour   m_Colour;
};
//-----------------------------------------------------------------------------

// primitives:

ColourSphere    s_CSphere;
Triangle        s_Triangle;
Ray             s_Ray;

// the angular speed of the ray
#define RAY_ROT 0x20

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // initialise GX
    IwGxInit();

    // initialise triangle:

    s_Triangle.m_V0 = CIwSVec3(IW_FIXED(-0.1), IW_FIXED(0.1), IW_FIXED(0.1));
    s_Triangle.m_V1 = CIwSVec3(IW_FIXED(-0.1), IW_FIXED(-0.1), IW_FIXED(0.1));
    s_Triangle.m_V2 = CIwSVec3(IW_FIXED(-0.3), IW_FIXED(0.1), IW_FIXED(-0.1));

    // set the triangle normal:

    // the normal of the triangle is the normal of the result of the cross
    // product of two of its edges (the order of the cross product determines
    // the orientation of the triangle)
    CIwVec3 edge1 = s_Triangle.m_V1 - s_Triangle.m_V0;
    CIwVec3 edge2 = s_Triangle.m_V2 - s_Triangle.m_V0;
    s_Triangle.m_Normal = (edge1 ^ edge2).GetNormalised();

    // initialise ray:
    s_Ray.m_Origin = CIwVec3::g_Zero;
    s_Ray.m_Direction = CIwVec3(0, 0, -IW_FIXED(1));

    // initialise sphere:
    s_CSphere.m_Sphere.SetRadius(IW_FIXED(0.3));
    s_CSphere.m_Sphere.t = CIwVec3(IW_FIXED(0.3), 0, IW_FIXED(0.3));
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // terminate GX
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // create a rotation matrix, defined by a rotation of RAY_ROT about the y axis
    CIwMat rot;
    rot.SetRotY(RAY_ROT);

    // rotate the ray direction
    s_Ray.m_Direction = rot.RotateVec(s_Ray.m_Direction).GetNormalised();

    // set the colour of the triangle based on whether it intersects the ray
    // (intersected => red, otherwise white):
    int32 f;
    if (IwIntersectLineTriNorm( s_Ray.m_Origin,
                                s_Ray.m_Direction,
                                s_Triangle.m_V0,
                                s_Triangle.m_V1,
                                s_Triangle.m_V2,
                                s_Triangle.m_Normal,
                                f   )   )
    {
        s_Triangle.m_Colour = IwGxGetColFixed(IW_GX_COLOUR_RED);
    }
    else
    {
        s_Triangle.m_Colour = IwGxGetColFixed(IW_GX_COLOUR_BLACK);
    }

    // set the colour of the sphere based on whether it intersects the ray:
    // (intersected => red, otherwise white):
    if (IwIntersectLineSphere(  s_Ray.m_Origin,
                                s_Ray.m_Direction,
                                s_CSphere.m_Sphere  )   )
    {
        s_CSphere.m_Colour = IwGxGetColFixed(IW_GX_COLOUR_RED);
    }
    else
    {
        s_CSphere.m_Colour = IwGxGetColFixed(IW_GX_COLOUR_BLACK);
    }

    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // no lighting
    IwGxLightingOff();

    // the IwGxDebugPrim* functions are used to render primitives in wireframe
    // (only available in debug builds)

    // when rendering we translate and orientate the view such that the primitives are visible:

    CIwMat view;

    // set the matrix to represent a rotation about the x axis
    view.SetRotX(0x120);

    // set the translation vector of the matrix
    view.t.z = IW_FIXED(-0.7);
    view.t.y = IW_FIXED(-0.4);
    view.t.x = IW_FIXED(0.1);

    // set the view matrix
    IwGxSetViewMatrix(&view);

    // render the triangle
    IwGxDebugPrimTri(s_Triangle.m_V0, s_Triangle.m_V1, s_Triangle.m_V2, s_Triangle.m_Colour);

    // render the sphere
    IwGxDebugPrimSphere(s_CSphere.m_Sphere, s_CSphere.m_Colour);

    // render the ray
    IwGxDebugPrimLine(s_Ray.m_Origin, s_Ray.m_Origin + (s_Ray.m_Direction * IW_FIXED(0.4)));

    IwGxFlush();

    // swap buffers
    IwGxSwapBuffers();
}
//-----------------------------------------------------------------------------
