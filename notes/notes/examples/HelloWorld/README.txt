Hello World for Marmalade
=========================

This folder contains two "Hello World" projects that demonstrate the two main
ways of drawing in Marmalade.

IwGxHelloWorld demonstrates using Marmalade Studio's IwGx API to draw the
"Hello World" string. The IwGx API will render using OpenGL ES on all devices
that support it.

s3eHelloWorld demonstrates using the s3eSurface API to plot directly into a
surface ("software rendering"). This API exposes a (more or less) direct
pointer to the surface buffer so applications can push pixel values directly.
Note that devices with GPUs tend to expect applications to use hardware
rendering (OpenGL ES) and the s3eSurface API tends to be much less direct and
much slower (this is especially true on iOS). Therefore, the s3eSurface API is
generally useful only for debugging purposes.

Marmalade applications can also use OpenGL ES directly - we have not provided
a "Hello World" example using pure OpenGL ES calls. The normal GL equivalent is
a spinning cube. You can find that example in examples/s3e/s3eGLES1 or
examples/IwGL/IwGLES1 for a version running through Marmalade's OpenGL ES
helper module, IwGL.
