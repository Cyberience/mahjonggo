/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
#include "s3eDebug.h"
#include "s3eDevice.h"
#include "s3eSurface.h"

void library_func()
{
    while (!s3eDeviceCheckQuitRequest())
    {
        s3eDebugPrint(10, 10, "Hello from a library!", 1);
        s3eSurfaceShow();
        s3eDeviceYieldUntilEvent();
    }
}
