/*
 *	RollerCoaster2005
 *	Copyright (C) 2005 Plusplus (plusplus AT free.fr)
 *
 *	This program is free software; you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation; either version 2 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program; if not, write to the Free Software
 *	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "RollerCoaster.h"

#include "s3e.h"
#include "GLES/egl.h"

#define TRUE	1
#define FALSE	0

// The display where the graphics are drawn
EGLDisplay  iEglDisplay;
// The rendering context
EGLContext  iEglContext;
// The window where the graphics are drawn
EGLSurface  iEglSurface;

void InitEGL()
{    
	EGLConfig Config;

    // Get the display for drawing graphics
    iEglDisplay = eglGetDisplay( EGL_DEFAULT_DISPLAY );
    if( iEglDisplay == NULL )
    {
		s3eDebugErrorShow(S3E_MESSAGE_CONTINUE ,"eglGetDisplay failed");
		return;
    }

    // Initialize display
    if( eglInitialize( iEglDisplay, NULL, NULL ) == EGL_FALSE )
    {
		s3eDebugErrorShow(S3E_MESSAGE_CONTINUE ,"eglInitialize failed");
		return;
    }

    EGLConfig *configList = NULL;
    EGLint numOfConfigs = 0;
    EGLint configSize = 0;

    // Get the number of possible EGLConfigs
    if( eglGetConfigs(iEglDisplay, configList, configSize, &numOfConfigs) == EGL_FALSE )
    {
		s3eDebugErrorShow(S3E_MESSAGE_CONTINUE ,"eglGetConfigs failed");
		return;
    }

    configSize = numOfConfigs;

    // Allocate memory for the configList
    configList = (EGLConfig*) s3eMalloc( sizeof(EGLConfig)*configSize );
    if( configList == NULL )
    {
		s3eDebugErrorShow(S3E_MESSAGE_CONTINUE ,"config alloc failed");
		return;
    }

    // Define properties for the wanted EGLSurface
    const EGLint attrib_list[] = {EGL_BUFFER_SIZE, 16, EGL_DEPTH_SIZE, 16, EGL_NONE};

    // Choose an EGLConfig that best matches to the properties in attrib_list
    if( eglChooseConfig(iEglDisplay, attrib_list, configList, configSize, &numOfConfigs) == EGL_FALSE )
    {
		s3eDebugErrorShow(S3E_MESSAGE_CONTINUE ,"eglChooseConfig failed");
		return;
    }

    Config = configList[0];
    s3eFree( configList );

    // Create a window where the graphics are blitted
    iEglSurface = eglCreateWindowSurface( iEglDisplay, Config, s3eGLGetNativeWindow(), NULL );
    if( iEglSurface == NULL )
    {
		s3eDebugErrorShow(S3E_MESSAGE_CONTINUE ,"eglCreateWindowSurface failed");
		return;
    }

    // Create a rendering context
    iEglContext = eglCreateContext( iEglDisplay, Config, NULL, NULL );
    if( iEglContext == NULL )
    {
		s3eDebugErrorShow(S3E_MESSAGE_CONTINUE ,"eglCreateContext failed");
		return;
    }

    // Make the context current. Binds context to the current rendering thread and surface.
    if( eglMakeCurrent(iEglDisplay, iEglSurface, iEglSurface, iEglContext) == EGL_FALSE )
    {
		s3eDebugErrorShow(S3E_MESSAGE_CONTINUE ,"eglMakeCurrent failed");
		return;
    }
}

void TerminateEGL()
{
    eglMakeCurrent(iEglDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
    eglDestroySurface(iEglDisplay, iEglSurface);
    eglDestroyContext(iEglDisplay, iEglContext);
    eglTerminate(iEglDisplay);
}

int main()
{
	InitEGL();
	
	// Default screen size
	int ScreenWidth = 320;
	int ScreenHeight = 240;

	CRollerCoaster* rc = new CRollerCoaster(ScreenWidth, ScreenHeight);

	rc->AppInit();

	while (!s3eDeviceCheckQuitRequest() && !s3eKeyboardAnyKey())
	{
		rc->AppCycle(0);
	    eglSwapBuffers( iEglDisplay, iEglSurface );
		s3eDeviceYield(0);
		s3eKeyboardUpdate();
	}

	rc->AppExit();
	delete rc;

	TerminateEGL();
	return 0;
}

CRollerCoaster::CRollerCoaster(int aWidth, int aHeight)
: iScreenWidth(aWidth), iScreenHeight(aHeight)
{
}

void CRollerCoaster::AppInit()
{
	static const point array[] = {
		{4.0f, -2.0f, 0.2f}, {0.0f, 0.0f, 0.0f},
		{3.0f, 0.0f, 0.21f}, {-0.5f, 1.0f, 0.0f},
		{2.0f, 2.0f, 3.5f}, {0.0f, 0.0f, 0.0f},
		{0.0f, 3.0f, 3.4f}, {0.0f, 0.0f, 0.0f},
		{-3.0f, 0.0f, 0.2f}, {0.0f, 0.0f, 0.0f},
		{-3.0f, -2.0f, 2.5f}, {0.0f, 0.0f, 0.0f},
		{0.0f, -3.0f, 0.2f}, {1.0f, 0.0f, 0.0f},
		{1.0f, -3.1f, 1.2f}, {0.0f, 0.0f, 0.0f},
		{0.0f, -3.2f, 2.2f}, {0.0f, 0.0f, 0.0f},
		{-1.0f, -3.3f, 1.2f}, {0.0f, 0.0f, 0.0f},
		{0.0f, -3.4f, 0.2f}, {1.0f, 0.0f, 0.0f},
		{0.5f, -3.4f, 0.2f}, {1.0f, 0.0f, 0.0f},
		{1.5f, -3.5f, 1.2f}, {0.0f, 0.0f, 0.0f},
		{0.5f, -3.6f, 2.2f}, {0.0f, 0.0f, 0.0f},
		{-0.5f, -3.7f, 1.2f}, {0.0f, 0.0f, 0.0f},
		{0.5f, -3.8f, 0.2f}, {1.0f, 0.0f, 0.0f},
		{4.0f, -4.0f, 2.5f}, {0.0f, 0.0f, 0.0f},
		{4.0f, -1.0f, 3.0f}, {0.0f, 0.0f, 0.0f},
		{2.0f, 2.0f, 1.0f}, {0.0f, 0.0f, 0.0f},
		{-3.0f, -2.0f, 1.0f}, {0.0f, 0.0f, 0.0f},
		{-3.0f, 3.0f, 3.3f}, {0.0f, 0.0f, 0.0f},
		{0.0f, 3.3f, 0.7f}, {0.0f, 0.0f, 0.0f},
		{2.0f, 3.5f, 0.2f}, {0.0f, 0.0f, 0.0f},
		{3.5f, 3.4f, 1.7f}, {0.0f, 0.0f, 0.0f},
		{2.0f, 3.3f, 3.2f}, {0.0f, 0.0f, 0.0f},
		{0.5f, 3.2f, 1.7f}, {0.0f, 0.0f, 0.0f},
		{2.0f, 3.1f, 0.3f}, {1.0f, 0.0f, 0.0f},
		{4.0f, 2.0f, 0.3f}, {0.0f, 0.0f, 0.0f},
		{3.0f, 0.0f, 2.0f}, {0.0f, 0.0f, 0.0f},
		{0.0f, -1.0f, 3.0f}, {0.0f, 0.0f, 0.0f},
		{-3.0f, -3.0f, 1.0f}, {0.0f, 0.0f, 0.0f},
		{0.0f, -5.0f, 0.2f}, {4.0f, -0.5f, 0.0f},
		{5.0f, -4.0f, 0.2f}, {-0.5f, 1.0f, 0.0f}
	};
	iRollerCoaster.trkd.nbControlPoint = 33;
	iRollerCoaster.trkd.control = (point*)array;
	iRollerCoaster.trkd.startSegment = 2;
	iRollerCoaster.trkd.brakeSegment = -12;
	iRollerCoaster.trkd.twistFactor = 1.5f;
	iRollerCoaster.trkd.averageSegmentLength = 0.35f;

	iStartTime = s3eTimerGetMs();
	iRollerCoaster.eng.doIntro = 1;
	iRollerCoaster.eng.wndWidth = iScreenWidth;
	iRollerCoaster.eng.wndHeight = iScreenHeight;

	static const point coord[] = {
		{0.98f, -3.05f, 1.0f}, {1.0f, 0.0f, 0.0f},
		{0.0f, -3.0f, 1.0f}, {1.0f, 0.0f, 0.0f},
		{0.25f, -3.4f, 1.0f}, {1.0f, 0.0f, 0.0f},
		{0.5f, -3.8f, 1.0f}, {1.0f, 0.0f, 0.0f},
		{4.44f, -2.88f, 0.0f}, {-0.5f, 1.0f, -0.18f},
		{2.0f, 3.5f, 1.0f}, {1.0f, 0.0f, 0.0f},
		{3.4f, 3.4f, 0.0f}, {1.0f, 0.0f, 0.0f},
		{1.0f, 3.05f, 2.0f}, {1.0f, 0.0f, 0.0f},
		{2.5f, 3.05f, 1.0f}, {1.0f, 0.0f, 0.0f}
	};
	iRollerCoaster.supd.pillar_coord = (point*)coord;
	iRollerCoaster.supd.nbPillarCoord = 9;
	static const int absc[] = {
		9,
		15,
		22,
		30,
		40,
		50,
		60,
		115, //after double loop
		120,
		135,
		140,
		150,
		155,
		165,
		180,
		252,
		258,
		265,
		275,
		280,
		285,
		290,
		295
	};
	iRollerCoaster.supd.pillar_absc = (int*)absc;
	iRollerCoaster.supd.nbPillarAbsc = 23;

	InitializeRoller(&iRollerCoaster, "");
}


void CRollerCoaster::AppExit()
{
	ExitRoller(&iRollerCoaster);
}


void CRollerCoaster::AppCycle(int /*aFrame*/)
{
	uint64 curTime;
	curTime = s3eTimerGetMs();
	unsigned long timeTick = (unsigned long)(curTime - iStartTime);
	//unsigned long timeTick = (curTime.MicroSecondsFrom(iStartTime).Int64() / 1000).Low();
	DrawRoller(timeTick, &iRollerCoaster);
}


void CRollerCoaster::FlatShading()
{
	SetRollerShadeMode(&iRollerCoaster, ROLLER_SHADE_FLAT);
}


void CRollerCoaster::SmoothShading()
{
	SetRollerShadeMode(&iRollerCoaster, ROLLER_SHADE_SMOOTH);
}


void CRollerCoaster::SetPerspectiveCorrection(bool aPerspective)
{
	SetRollerPerspectiveCorrection(&iRollerCoaster, aPerspective);
}


bool CRollerCoaster::GetPerspectiveCorrection()
{
	return iRollerCoaster.eng.perspective_correction ? TRUE : FALSE;
}


void CRollerCoaster::SetMipmapping(bool aMipmapping)
{
	SetRollerMipmapping(&iRollerCoaster, aMipmapping);
}


bool CRollerCoaster::GetMipmapping()
{
	return iRollerCoaster.eng.mipmapping ? TRUE : FALSE;
}
