RollerCoaster2005 is an attempt for the coolest self-running demo.
It has been tested on a Nokia 6600 phone with Hybrid's OpenGLES
implementation. The sdk used to compile it is the version s60 v2.0
from Nokia. Although it has not been tested, it should also run on
other series 60 phones.

When launched, the program first dynamically generates all the
vertices of the track for a few second. Then the animation starts
with a first turn around the coaster, a fade in/fade out, and the
ride starts...

This project is a port of an existing roller coaster simulator to
OpenGLES. The original project is hosted on the following webpage:
http://plusplus.free.fr/rollercoaster/

The port involved removal of all global variables, usage of fixed
point type for all coordinates, translation from glBegin/glEnd to
glDrawElements/glDrawArrays, a lot of optimizations, and a big code
cleanup. In addition, this port was the occasion to add a skydome
texture to have a more realistic sky.
