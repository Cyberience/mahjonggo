#ifndef _COLOR_H_
#define _COLOR_H_

#ifdef __ARMCC_VERSION 
	#pragma anon_unions
#endif

class Color4
{
public:
	union
	{
		struct
		{
			unsigned char r,g,b,a;
		};
		unsigned char v[4];
	};
};

#endif
