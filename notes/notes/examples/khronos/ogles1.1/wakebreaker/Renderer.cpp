#include "Game.h"
#include "s3e.h"

//---------------------------------
bool Renderer::Create()
{
	m_display	= NULL;
	m_config	= NULL;
	m_surface	= NULL;
	m_context	= NULL;

    EGLint s_configAttribs[] =
    {

		EGL_RED_SIZE,       5,
		EGL_GREEN_SIZE,     5,
	    EGL_BLUE_SIZE,      5,
        EGL_ALPHA_SIZE,     EGL_DONT_CARE,
        EGL_DEPTH_SIZE,     EGL_DONT_CARE,
        EGL_STENCIL_SIZE,   EGL_DONT_CARE,
        EGL_SURFACE_TYPE,   EGL_WINDOW_BIT,
        EGL_NONE
    };


    EGLint numConfigs, 
		   majorVersion,
		   minorVersion;

    m_display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
    eglGetConfigs(m_display, NULL, 0, &numConfigs);
    eglInitialize(m_display, &majorVersion, &minorVersion);
    eglChooseConfig(m_display, s_configAttribs, &m_config, 1, &numConfigs);
    m_context = eglCreateContext(m_display, m_config, NULL, NULL);

	m_surface = eglCreateWindowSurface(m_display, m_config, s3eGLGetNativeWindow(), NULL);

    eglMakeCurrent(m_display, m_surface, m_surface, m_context);

	return 1;
}
//---------------------------------
void Renderer::Initialize(uint16 width,uint16 height)
{
	m_currTexture = -1;
	
	// Enable the zbuffer
	glEnable(GL_DEPTH_TEST);

	// Set the view port size to the window size
	glViewport(0, 0, width, height);

	// Diable lighting and alpha blending
	glDisable(GL_LIGHTING);
	glDisable(GL_BLEND);
	
	glCullFace(GL_BACK);
	glEnable(GL_CULL_FACE);
	
	glEnable(GL_TEXTURE_2D);

	glDisable(GL_DITHER);

    glClearColor(0.745f, 0.95f, 1.0f, 0.f);

	// Set a precomputed view matrix
	int matrix[16] = {	 FTOX( 2.41421 / ((float)width/(float)height) ),  FTOX(0.000000), FTOX(0.000000), FTOX(0.000000),
						 FTOX(0.000000), FTOX(2.41421),  FTOX(0.000000), FTOX(0.000000),
						 FTOX(0.000000), FTOX(0.000000), FTOX(-1.00020), FTOX(-1.00000),
						 FTOX(0.000000), FTOX(0.000000), FTOX(-2.00020), FTOX(0.000000) };


	glMatrixMode(GL_PROJECTION);
	glLoadMatrixx(matrix);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	// Set up fog
	
	glFogx(GL_FOG_DENSITY,FTOX(0.01));
	glFogx(GL_FOG_MODE,GL_EXP);

	glHint(GL_FOG_HINT, GL_DONT_CARE);

	int color[] = { FTOX(0.745f), FTOX(0.95f), FTOX(1.0f), FTOX(0.f) };
	glFogxv(GL_FOG_COLOR,color);




	// Set the model view to identity
	glMatrixMode(GL_MODELVIEW );
	glLoadIdentity();

	// Enable the arrays we want used when we glDrawElements()
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
}
//---------------------------------
//Renders a renderInstance
void Renderer::Render(RenderInstance* data)
{
	if(data->renderData() != m_currData)
	{
		if(data->renderData()->m_texCoords)
			glTexCoordPointer(2,GL_FIXED,0,&data->renderData()->m_texCoords[0]);

		glVertexPointer(3, GL_FIXED, 0, data->renderData()->m_vertices[0].v);
		
		if(data->renderData()->m_colorData)
			glColorPointer(4, GL_UNSIGNED_BYTE, 0, data->renderData()->m_colorData[0].v);	// Set the color data source

		m_currData = data->renderData();
	}

	if(data->renderData()->m_texture && m_currTexture != data->renderData()->m_texture->id())
	{
		data->renderData()->m_texture->Bind();
		m_currTexture = data->renderData()->m_texture->id();
	}

	glPushMatrix();

	glTranslatex(data->position().x,data->position().y,data->position().z);
	glScalex(data->scale().x,data->scale().y,data->scale().z);
	glRotatex(data->rotation().y,0,1,0);
	glRotatex(data->rotation().x,1,0,0);
	glRotatex(data->rotation().z,0,0,1);
		
	glDrawElements(GL_TRIANGLES, data->renderData()->m_numIndices, data->renderData()->m_indexDataType, data->renderData()->m_indices);// Draw the triangle

	glPopMatrix();
}

//---------------------------------
//Draws a screen size quad with tex on it
void Renderer::Draw2DQuad(Texture* tex)
{

	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	glPushMatrix();
	glLoadIdentity();

	glTranslatex(0,  FTOX(-0.25), ITOX(-5));


  	  int FaceData[18] =
	  {
		-FTOX(3), -ITOX(2), ITOX(0),    // First vertex position
		 FTOX(3), -ITOX(2), ITOX(0),    // Second vertex position
		-FTOX(3),  ITOX(2), ITOX(0),    // Third vertex position

		 FTOX(3),  ITOX(2), ITOX(0),    // First vertex position
		 FTOX(3), -ITOX(2), ITOX(0),    // Second vertex position
		-FTOX(3),  ITOX(2), ITOX(0)    // Third vertex position
		 
	  };
	  int TexCoordData[12] = 
	  { 
		  FTOX(0.0),FTOX(0.0),
		  FTOX(1.0),FTOX(0.0),
		  FTOX(0.0),FTOX(1.0),

		  FTOX(1.0),FTOX(1.0),
		  FTOX(1.0),FTOX(0.0),
		  FTOX(0.0),FTOX(1.0),

	  };

	  
	  tex->Bind();

	  uint8 IndexData[6] = {0, 1, 2, 5, 4, 3};

	  glVertexPointer(3, GL_FIXED, 0, FaceData);  // Set the vertex (position) data source
	  glTexCoordPointer(2, GL_FIXED, 0, TexCoordData);  // Set the vertex (position) data source
	
	  glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, IndexData);  // Draw the triangle

	  glPopMatrix();	

}

//---------------------------------
void Renderer::Destroy()
{
	eglMakeCurrent(EGL_NO_DISPLAY, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);

	if(m_context)
	{
		eglDestroyContext(m_display, m_context);
		m_context = NULL;
	}
	if(m_surface)
	{
		eglDestroySurface(m_display, m_surface);
		m_surface = NULL;
	}
	if(m_display)
	{
		eglTerminate(m_display);
		m_display = NULL;
	}
}
//---------------------------------
