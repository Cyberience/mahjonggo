#ifndef _BASE_H_
#define _BASE_H_

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include "GLES/gl.h"
#include "GLES/egl.h"

#if defined (_MSC_VER)
	#pragma warning (disable : 4244 4100)
#endif

#include "s3eTypes.h"
//typedef  __int64 int64;

#define SAFE_DELETE(p) { if(p) { delete p; p = NULL; } }
#define SAFE_ARRAY_DELETE(p) { if(p) { delete [] p; p = NULL; } }
#define PIX 205887
#define TWOPIX 411775

#endif
