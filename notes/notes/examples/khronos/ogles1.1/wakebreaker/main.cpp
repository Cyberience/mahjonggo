#if defined (_MSC_VER)
    #pragma warning( disable : 4244 )
#endif

#include "Base.h"
#include "Game.h"

#include "s3e.h"

Game g_game;

#define MAP_S3E_KEY_TO_GAME(A, B) \
        if (s3eKeyboardGetState(A) & S3E_KEY_STATE_PRESSED) \
            g_game.KeyDown(B); \
        if (s3eKeyboardGetState(A) & S3E_KEY_STATE_RELEASED) \
            g_game.KeyUp(B); \

int main()
{  
    // Default width and height
    int ScreenWidth = 320;
    int ScreenHeight = 240;
    
    g_game.Create(ScreenWidth,ScreenHeight);

    while(!s3eDeviceCheckQuitRequest())
    {
        s3eKeyboardUpdate();

        if (s3eKeyboardGetState(s3eKeyEsc) & S3E_KEY_STATE_DOWN)
        {
            static bool disableExit =
                s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WS8 ||
                s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WS81 ||
                s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WIN10;

            if (!disableExit)
            {
                s3eDeviceRequestQuit();
            }
        }

        MAP_S3E_KEY_TO_GAME(s3eKeyLeft, G_LEFT)
        MAP_S3E_KEY_TO_GAME(s3eKeyUp, G_UP)
        MAP_S3E_KEY_TO_GAME(s3eKeyRight, G_RIGHT)
        MAP_S3E_KEY_TO_GAME(s3eKeyDown, G_DOWN)
        MAP_S3E_KEY_TO_GAME(s3eKeyOk, G_OK)
        MAP_S3E_KEY_TO_GAME(s3eKey1, G_DEVICE1)
        MAP_S3E_KEY_TO_GAME(s3eKey2, G_DEVICE2)

        //calculate how long it takes to draw one frame
        uint64 frameTime = s3eTimerGetMs();
        g_game.Menu();
        frameTime = s3eTimerGetMs() - frameTime;
        
        //this tick essentially has to happen 30 times a second
        //so that means it has 33 milliseconds to fill
        //therefore, we must wait
        uint64 waitTime = 33 - frameTime;
        if(waitTime < 0 || waitTime > 33)
            waitTime = 0;

        s3eDeviceYield(waitTime);
    }

    g_game.Destroy();
    return 0;
}

