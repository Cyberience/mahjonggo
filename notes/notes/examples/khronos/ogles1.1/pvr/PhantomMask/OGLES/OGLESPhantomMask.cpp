/******************************************************************************

 @File         OGLESPhantomMask.cpp

 @Title        PhantomMask

 @Copyright    Copyright (C) 2005 - 2006 by Imagination Technologies Limited.

 @Platform     Independant

 @Description  Shows Spherical Harmonics Lighting using an IMG Vertex Program
               Requires the PVRShell.

******************************************************************************/
#include <string.h>
#include "PVRShell.h"
#include "OGLESTools.h"

/* Textures */

#include "MaskMain.h"
#include "RoomStill.h"

/* Vertex Programs */

#include "DIF_VGPARMVP.h"
#include "SHL_VGPARMVP.h"
#include "DIF_VGP.h"
#include "SHL_VGP.h"
#include "DIF_VGPLITE.h"
#include "SHL_VGPLITE.h"

/****************************************************************************
 ** DEFINES                                                                **
 ****************************************************************************/

/* Build option to select Spherical Harmonics or Vertex Lighting */
#if defined BUILD_NONSH
	#define BUILD_OPTION 0	// Vertex Lighting Case
#else
	#define BUILD_OPTION 1	// Default Spherical Harmonics Lighting Case
#endif

/* Geometry Data File Name */
#define FILENAME_SCENE ("MaskWithAnimation.POD")

/* Number of Vertex Programs */
#define NUM_OF_PROGRAMS		 2

// Assuming a 4:3 aspect ratio:
#define CAM_ASPECT	f2vt(1.333333333f)
#define CAM_NEAR	f2vt(50.0f)
#define CAM_FAR		f2vt(5000.0f)

/* Influences the animation speed - tweak this for HW/SW usage - too slow/fast */
#define ANIMATION_FACTOR		f2vt(0.686f)

/****************************************************************************
** Structures
****************************************************************************/

/* Per Vertex Data Storage */
struct SVtx
{
	VERTTYPE	x, y, z;	// Position
	VERTTYPE	u, v;		// TexCoord
	VERTTYPE	nx, ny, nz;	// Normal
};

/* Per Object Data Storage */
struct SOb
{
	int		nCount;			// Number of Indices to draw
	int		nFirstIndex;	// First Index to draw
	unsigned short	*Ib;	// Pointer to Indices
	SVtx	*Vb;			// Pointer to Vertex Structure
};

/****************************************************************************
** Class: OGLESPhantomMask
****************************************************************************/
class OGLESPhantomMask : public PVRShell
{
    /* Animation Related */
	VERTTYPE gfAnimationFrame;

	/* Texture IDs */
	GLuint guiTextureMask;
	GLuint guiTextureBackground;

	/* Vertex Program IDs */
	GLuint guiProgs[NUM_OF_PROGRAMS];

	/* Print3D, Extension and POD Class Objects */
	CPVRTPrint3D 		gsMyPrint3D;
	CPVRTPODScene		g_sScene;
	CPVRTglesExt		g_PVRTglesExt;

	/* Created resources */
	SOb				*g_pOb;

	/* View and Projection Matrices */
	PVRTMATRIX	g_mView, g_mProj;

	/* Render Options */
	int		g_nNumberOfRenderCalls;
	bool	g_bEnableSH;

public:
	OGLESPhantomMask()
	{
		/* Init values to defaults */
		gfAnimationFrame = 0;

		g_nNumberOfRenderCalls = 0;

		g_bEnableSH = BUILD_OPTION;
	}

	/* PVRShell functions */
	virtual bool InitApplication();
	virtual bool InitView();
	virtual bool ReleaseView();
	virtual bool QuitApplication();
	virtual bool RenderScene();

	/****************************************************************************
	** Function Definitions
	****************************************************************************/
	void	ComputeAndSetSHIrradEnvMapConstants( float* pSHCoeffsRed, float* pSHCoeffsGreen, float* pSHCoeffsBlue );
	void	SetupVGPConstants(bool light0, bool light1, bool envlight);
	bool InitIMG_vertex_program();
	bool LoadTextures();
	bool LoadObjects();
	bool		LoadVertexPrograms(const void *pSHL, size_t nSzSHL, const void *pDIF, size_t nSzDIF);
	void	CameraGetMatrix();
};

/*******************************************************************************
 * Function Name  : InitApplication
 * Inputs		  : argc, *argv[], uWidth, uHeight
 * Returns        : true if no error occured
 * Description    : Code in InitApplication() will be called by the Shell ONCE per
 *					run, early on in the execution of the program.
 *					Used to initialize variables that are not dependant on the
 *					rendering context (e.g. external modules, loading meshes, etc.)
 *******************************************************************************/
bool OGLESPhantomMask::InitApplication()
{
	/* Gets the Data Path */
	const char	*dataPath;
	char		*filename = new char[2048];

	dataPath = (char*)PVRShellGet(prefDataPath);
	if (!dataPath)
	{
		PVRShellSet(prefExitMessage, "**ERROR** Failed to provide a buffer large enough for OGLESGetDataPath.\n");
		return false;
	}

	/* Load Geometry Data */
	sprintf(filename, "%s%s", dataPath, FILENAME_SCENE);
	if(!g_sScene.ReadFromFile(filename))
	{
		PVRShellSet(prefExitMessage, "**ERROR** FAILED TO LOAD POD SCENE !!!\n\n");
		return false;
	}
	delete [] filename;

	/* Allocate basic data structures*/
	g_pOb = (SOb *)calloc(g_sScene.nNumMesh, sizeof(*g_pOb));

	return true;
}


/*******************************************************************************
 * Function Name  : QuitApplication
 * Returns        : true if no error occured
 * Description    : Code in QuitApplication() will be called by the Shell ONCE per
 *					run, just before exiting the program.
 *******************************************************************************/
bool OGLESPhantomMask::QuitApplication()
{
	/* Release basic data structures */
	FREE(g_pOb);
	g_sScene.Destroy();

    return true;
}

/*******************************************************************************
 * Function Name  : InitView
 * Inputs		  : uWidth, uHeight
 * Returns        : true if no error occured
 * Description    : Code in InitView() will be called by the Shell upon a change
 *					in the rendering context.
 *					Used to initialize variables that are dependant on the rendering
 *					context (e.g. textures, vertex buffers, etc.)
 *******************************************************************************/
bool OGLESPhantomMask::InitView()
{
	/******************************
	** IMG VERTEX PROGRAM        **
	*******************************/
	if(!InitIMG_vertex_program())
		return false;

	/******************************
	** Create Textures           **
	*******************************/
	if(!LoadTextures())
		return false;

	/*********************************************
	** Create Objects and render info           **
	**********************************************/
	if(!LoadObjects())
		return false;

	/******************************
	** Projection Matrix         **
	*******************************/

	/* Get Camera info from POD file */
	CameraGetMatrix();

	/******************************
	** GENERIC RENDER STATES     **
	*******************************/

	// The Type Of Depth Test To Do
	glDepthFunc(GL_LEQUAL);

	// Enables Depth Testing
	glEnable(GL_DEPTH_TEST);

	// Enables Smooth Color Shading
	glShadeModel(GL_SMOOTH);

	// Enables texturing
	glEnable(GL_TEXTURE_2D);

	return true;
}

/*******************************************************************************
 * Function Name  : ReleaseView
 * Returns        : Nothing
 * Description    : Code in ReleaseView() will be called by the Shell before
 *					changing to a new rendering context.
 *******************************************************************************/
bool OGLESPhantomMask::ReleaseView()
{
	int i;

	/* Release all Textures */

	PVRTReleaseTexture(guiTextureMask);
	PVRTReleaseTexture(guiTextureBackground);

	/* Release Vertex Programs */

	g_PVRTglesExt.glDeleteProgramsARB(NUM_OF_PROGRAMS, guiProgs);

	/* Release the Print3D textures and windows */

	gsMyPrint3D.DeleteAllWindows();
	gsMyPrint3D.ReleaseTextures();

	/* Geometry Data */

	for(i = 0; i < g_nNumberOfRenderCalls; ++i)
	{
		FREE(g_pOb[i].Ib);
		FREE(g_pOb[i].Vb);
	}
	return true;
}


/*******************************************************************************
 * Function Name  : RenderScene
 * Returns		  : 0 if no error occured
 * Description    : Main rendering loop function of the program. The shell will
 *					call this function every frame.
 *******************************************************************************/
bool OGLESPhantomMask::RenderScene()
{
	PVRTMATRIX	mTrans, mWorld;
	SPODNode	*pNode;
	SOb			*pOb;
	int			i;

	/* Default render states - mainly recovery from the changes that Print3D does at the end of the frame */

	myglClearColor(f2vt(0.5f), f2vt(0.5f), f2vt(0.5f), f2vt(0));

	myglColor4(f2vt(1.0f), f2vt(1.0f), f2vt(1.0f), f2vt(1.0f));

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/* Set Z compare properties */
	glEnable(GL_DEPTH_TEST);

	/* Disable Blending and Lighting*/
	glDisable(GL_BLEND);
	glDisable(GL_LIGHTING);

	// Set Constants and Matrices based on current mode.

	glMatrixMode ( GL_MATRIX0_ARB );
	glLoadIdentity();

	/* Create model view matrix */
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	/* Bind the VGP progtam for Vertex Lighting or for DOT3 Lighting */
	if (g_bEnableSH)
	{
		g_PVRTglesExt.glBindProgramARB(GL_VERTEX_PROGRAM_ARB, guiProgs[0]); // SH Program
	}
	else
	{
        g_PVRTglesExt.glBindProgramARB(GL_VERTEX_PROGRAM_ARB, guiProgs[1]); // Vertex Lighting Program
	}

	/* Render the geometry with the correct texture */

	for(i = 0; i < (int)g_sScene.nNumMeshNode; i++)
	{
		pNode = &g_sScene.pNode[i];	// POD node pointer (e.g. position of object)
		pOb = &g_pOb[pNode->nIdx];	// Additional app per-mesh-data (e.g. index & vertex buffers)

		/* Texture Setup */
		/* Layer 0 - default layer */

		/* Bind correct Texture */
		if (pNode->nIdx==0)
		{
			glBindTexture(GL_TEXTURE_2D, guiTextureMask);
		}
		else
		{
			glBindTexture(GL_TEXTURE_2D, guiTextureBackground);
		}

		/* Animation Setup */
		g_sScene.SetFrame(gfAnimationFrame); /* Set current frame*/

		/* Get pointers for this object	*/
		g_sScene.GetWorldMatrix(mWorld, *pNode);
		PVRTMatrixMultiply(mTrans, mWorld, g_mView);
		glMatrixMode(GL_MODELVIEW);
		myglLoadMatrix(mTrans.f);


/*		PVRTMatrixMultiply(&mTrans, &mWorld, &g_mView);
		glMatrixMode(GL_MODELVIEW);
		myglLoadMatrix(mTrans.f);*/

		glMatrixMode ( GL_MATRIX0_ARB );

		/* Enable Lighting only for the Mask */
		if (pNode->nIdx==0)
		{
            if (g_bEnableSH)
			{
				myglLoadMatrix(mWorld.f);
				myglRotate(f2vt(60.0f), f2vt(0), f2vt(1), f2vt(0));
			}
			else
			{
				glLoadIdentity();
				myglLoadMatrix(mWorld.f);
			}

			// Enable Vertex Program
			glEnable(GL_VERTEX_PROGRAM_ARB);

			// Projection matrix
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			if(PVRShellGet(prefIsRotated) && PVRShellGet(prefFullScreen))
			{
#if defined(UNDER_CE) && !defined(WIN32_PLATFORM_WFSP)
				{
					PVRTMATRIX m = {
						0, 1, 0, 0,
						1, 0, 0, 0,
						0, 0, 1, 0,
						0, 0, 0, 1
					};

					myglMultMatrix(m.f);
				}
#endif
				myglRotate(f2vt(90), f2vt(0), f2vt(0), f2vt(1));
			}

			myglMultMatrix(g_mProj.f);
		}
		else
		{
			glLoadIdentity();

			// Disable Vertex Program
			glDisable(GL_VERTEX_PROGRAM_ARB);
			glDisableClientState(GL_COLOR_ARRAY);
			myglColor4(f2vt(1.0f), f2vt(1.0f), f2vt(1.0f), f2vt(1.0f));

			// Projection matrix
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			if(PVRShellGet(prefIsRotated) && PVRShellGet(prefFullScreen))
				myglRotate(f2vt(90), f2vt(0), f2vt(0), f2vt(1));

			myglMultMatrix(g_mProj.f);
		}

		/*	Render object	*/
		/*  Enable States */
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_NORMAL_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);

		/* Set Data Pointers */
		glVertexPointer(3, VERTTYPEENUM, sizeof(SVtx), &pOb->Vb[0].x);
		glNormalPointer(VERTTYPEENUM, sizeof(SVtx), &pOb->Vb[0].nx);
		glTexCoordPointer(2,VERTTYPEENUM, sizeof(SVtx),&pOb->Vb[0].u);

		/* Draw */
		glDrawElements(GL_TRIANGLES, pOb->nCount, GL_UNSIGNED_SHORT, &pOb->Ib[pOb->nFirstIndex]);
		if(glGetError())
			PVRShellOutputDebug("Loop : %i\n",i);
	}

	/* Disable Vertex Program */
	glDisable(GL_VERTEX_PROGRAM_ARB);

	/* Clean-up */

	myglColor4(f2vt(1.0f), f2vt(1.0f), f2vt(1.0f), f2vt(1.0f));

	/* Disable the Various Vertex Attribs */

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);

	/* Check for Problems */
	if(glGetError())
		PVRShellOutputDebug("**GL_ERROR** Detected prior to Print3D call.\n");

	/* Print text on screen */
	if (g_bEnableSH)
	{
		/* Shadow */
		gsMyPrint3D.Print3D(0.0f, 1.0f, 1.205f,  0xFF000000, "PhantomMask");
		gsMyPrint3D.Print3D(0.0f, 8.0f, 0.905f,  0xFF000000, "VGP Spherical Harmonics Lighting");

		/* Base*/
        gsMyPrint3D.DisplayDefaultTitle("PhantomMask", "VGP Spherical Harmonics Lighting", PVR_LOGO);
	}
	else
	{
		/* Shadow */
		gsMyPrint3D.Print3D(0.0f, 1.0f, 1.205f,  0xFF000000, "PhantomMask");
		gsMyPrint3D.Print3D(0.0f, 8.0f, 0.905f,  0xFF000000, "VGP Vertex Lighting");

		/* Base */
		gsMyPrint3D.DisplayDefaultTitle("PhantomMask", "VGP Vertex Lighting", PVR_LOGO);
	}

	gsMyPrint3D.Flush();

	/* Increment the framecounter to make sure our animation works */
	gfAnimationFrame = gfAnimationFrame + ANIMATION_FACTOR;

	/* Loop animation */
	if (gfAnimationFrame >= f2vt(g_sScene.nNumFrame-1))
		gfAnimationFrame = gfAnimationFrame - f2vt(g_sScene.nNumFrame-1);

	return true;
}

bool OGLESPhantomMask::InitIMG_vertex_program()
{
	char			*extensions;
	bool			bErr;

	/******************************
	** IMG VERTEX PROGRAM        **
	*******************************/

	// Check Extension Available

	extensions = (char*)glGetString(GL_EXTENSIONS);

	if(!strstr(extensions, "GL_IMG_vertex_program"))
	{
		/* Extension not available so quit the app and write a debug message */
		PVRShellSet(prefExitMessage, "**ERROR** GL_IMG_vertex_program is NOT supported by this platform\n\n");
		return false;
	}

	// Extension is available so :
	// 1) Init the function calls using our tools function

	g_PVRTglesExt.Init();

	// 2) Load our program(s) for usage

	const char *pszRendererString;

	/* Retrieve pointer to renderer string */
	pszRendererString = (const char*) glGetString(GL_RENDERER);

	bErr = true; // No error yet

	/* PowerVR hardware present? */
	if (strstr(pszRendererString, "PowerVR"))
	{
		/* VGP present? */
		if (strstr(pszRendererString, "VGPLite"))
		{
			/* VGPLite present */
			bErr = LoadVertexPrograms(vgp_SHL_VGPLITE,sizeof(vgp_SHL_VGPLITE),vgp_DIF_VGPLITE,sizeof(vgp_DIF_VGPLITE));
		}
		else if (strstr(pszRendererString, "VGP"))
		{
			/* VGP present */
			bErr = LoadVertexPrograms(vgp_SHL_VGP,sizeof(vgp_SHL_VGP),vgp_DIF_VGP,sizeof(vgp_DIF_VGP));
			if (!bErr)
			{
				/* Try loading ARM VP Vertex Program instead */
				bErr = LoadVertexPrograms(vgp_SHL_VGPARMVP,sizeof(vgp_SHL_VGPARMVP),vgp_DIF_VGPARMVP,sizeof(vgp_DIF_VGPARMVP));
			}
		}
    }

	if (bErr==false)
	{
		PVRShellSet(prefExitMessage, "**ERROR** FAILED TO LOAD VGP BINARY PROGRAMS !!!\n\n");
		return false;
	}

	// 3) Setup some base constants

	SetupVGPConstants(false, false, true);

	return true;
}

bool OGLESPhantomMask::LoadTextures()
{
	bool			bErr;
	SPVRTContext	TempContext;

	/* Init Print3D to display text on screen */
	bErr = gsMyPrint3D.SetTextures(&TempContext,PVRShellGet(prefWidth),PVRShellGet(prefHeight));
	if (bErr == false)
	{
		PVRShellSet(prefExitMessage, "**ERROR** Cannot initialise Print3D\n");
		return false;
	}

	/******************************
	** Create Textures           **
	*******************************/
	if(!PVRTLoadTextureFromPointer((void*)RoomStill, &guiTextureBackground))
	{
		PVRShellSet(prefExitMessage, "**ERROR** Failed to load texture for Background.\n");
		return false;
	}
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if(!PVRTLoadTextureFromPointer((void*)MaskMain, &guiTextureMask))
	{
		PVRShellSet(prefExitMessage, "**ERROR** Failed to load texture for Mask.\n");
		return false;
	}
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	return true;
}

bool OGLESPhantomMask::LoadObjects()
{
	SVtx			*pvData;
	unsigned short	*pwData;
	int				i;
	unsigned int	j;
	SPODMesh		*pMesh;

	/*********************************************
	** Create Objects and render info           **
	**********************************************/

	for(i = 0; i < (int)g_sScene.nNumMesh; ++i)
	{
		/*	Create: Vertex buffer	*/

		g_pOb[g_nNumberOfRenderCalls].Vb = (SVtx*)malloc(g_sScene.pMesh[i].nNumVertex * sizeof(SVtx));

		pvData = g_pOb[g_nNumberOfRenderCalls].Vb;

		pMesh = &g_sScene.pMesh[i];

		for(j = 0; j < g_sScene.pMesh[i].nNumVertex; ++j)
		{
			/* Copy Data for Vertex Position, UVs and Normals */
			pvData[j].x = f2vt(((float*)pMesh->sVertex.pData)[3 * j + 0]);
			pvData[j].y = f2vt(((float*)pMesh->sVertex.pData)[3 * j + 1]);
			pvData[j].z = f2vt(((float*)pMesh->sVertex.pData)[3 * j + 2]);

			if (pMesh->psUVW)
			{
               pvData[j].u = f2vt(((float*)pMesh->psUVW[0].pData)[3 * j + 0]);
               pvData[j].v = f2vt(((float*)pMesh->psUVW[0].pData)[3 * j + 1]);
			}
			else
			{
				pvData[j].u = f2vt(0.0f);
                pvData[j].v = f2vt(0.0f);
			}

			pvData[j].nx = f2vt(((float*)pMesh->sNormals.pData)[3 * j + 0]);
			pvData[j].ny = f2vt(((float*)pMesh->sNormals.pData)[3 * j + 1]);
			pvData[j].nz = f2vt(((float*)pMesh->sNormals.pData)[3 * j + 2]);
		}

		/* Create: Index buffer	*/

		g_pOb[g_nNumberOfRenderCalls].Ib = (unsigned short*)malloc(g_sScene.pMesh[i].nNumFaces * 3 * sizeof(unsigned short));

		pwData = g_pOb[g_nNumberOfRenderCalls].Ib;

		/* Copy Data */
		_ASSERT(pMesh->sFaces.eType == EPODDataUnsignedShort);
		memcpy(pwData, pMesh->sFaces.pData, pMesh->nNumFaces * 3 * sizeof(unsigned short));

		/* Store render info such as start index and number of indices */
		g_pOb[g_nNumberOfRenderCalls].nCount = g_sScene.pMesh[i].nNumFaces * 3;
		g_pOb[g_nNumberOfRenderCalls].nFirstIndex=0;

		g_nNumberOfRenderCalls++;
	}

	return true;
}

/*******************************************************************************
 * Function Name  : LoadVertexPrograms
 * Returns        : true if no error occured
 * Global Used    :
 * Description    : Function to load Vertex Programs used by this App
 *
 *******************************************************************************/
bool OGLESPhantomMask::LoadVertexPrograms(const void *pSHL, size_t nSzSHL, const void *pDIF, size_t nSzDIF)
{
	/* Generate Vertex Program IDs */
	g_PVRTglesExt.glGenProgramsARB(2, guiProgs);

	/* Bind and Load Program */
	g_PVRTglesExt.glBindProgramARB(GL_VERTEX_PROGRAM_ARB, guiProgs[0]);
	g_PVRTglesExt.glProgramStringARB(GL_VERTEX_PROGRAM_ARB, GL_PROGRAM_FORMAT_BINARY_IMG, nSzSHL, pSHL);

	/* Check for problems */
	if (glGetError()!=GL_NO_ERROR)
	{
		return false;
	}

	/* Bind and Load Program */
	g_PVRTglesExt.glBindProgramARB(GL_VERTEX_PROGRAM_ARB, guiProgs[1]);
	g_PVRTglesExt.glProgramStringARB(GL_VERTEX_PROGRAM_ARB, GL_PROGRAM_FORMAT_BINARY_IMG, nSzDIF, pDIF);

	/* Check for problems */
	if (glGetError()!=GL_NO_ERROR)
	{
		return false;
	}

	return true;
}

/*******************************************************************************
 * Function Name  : CameraGetMatrix
 * Global Used    :
 * Description    : Function to setup camera position
 *
 *******************************************************************************/
void OGLESPhantomMask::CameraGetMatrix()
{
	PVRTVECTOR3	vFrom, vTo, vUp;
	VERTTYPE	fFOV = 0;

	vUp.x = f2vt(0.0f);	vUp.y = f2vt(1.0f);	vUp.z = f2vt(0.0f);

	if(g_sScene.nNumCamera)
	{
		/* Get Camera data from POD Geometry File */
		fFOV = g_sScene.GetCameraPos(vFrom, vTo, 0);
		fFOV = VERTTYPEMUL(fFOV, f2vt(0.75f));		// Convert from horizontal FOV to vertical FOV (0.75 assumes a 4:3 aspect ratio)
	}

	// View
	PVRTMatrixLookAtRH(g_mView, vFrom, vTo, vUp);

	// Projection
	PVRTMatrixPerspectiveFovRH(g_mProj, fFOV, CAM_ASPECT, CAM_NEAR, CAM_FAR);
}

/*******************************************************************************
 * Function Name  : ComputeAndSetSHIrradEnvMapConstants
 * Global Used    :
 * Description    : Function to pre-calculate and setup the Spherical Harmonics Constants
 *
 *******************************************************************************/
void OGLESPhantomMask::ComputeAndSetSHIrradEnvMapConstants( float* pSHCoeffsRed, float* pSHCoeffsGreen, float* pSHCoeffsBlue )
{
    float* fLight[3] = { pSHCoeffsRed, pSHCoeffsGreen, pSHCoeffsBlue };

    // Lighting environment coefficients
    VERTTYPE vCoefficients[3][4];
	int iChannel;

    // These constants are described in the article by Peter-Pike Sloan titled
    // "Efficient Evaluation of Irradiance Environment Maps" in the book
    // "ShaderX 2 - Shader Programming Tips and Tricks" by Wolfgang F. Engel.

    static const float s_fSqrtPI = 1.772453850905516027298167483341f;
    const float fC0 = 1.0f/(2.0f*s_fSqrtPI);
    const float fC1 = (float)1.7320508075688772935274463415059f/(3.0f*s_fSqrtPI);
    const float fC2 = (float)3.8729833462074168851792653997824f/(8.0f*s_fSqrtPI);
    const float fC3 = (float)2.2360679774997896964091736687313f/(16.0f*s_fSqrtPI);
    const float fC4 = 0.5f*fC2;

    for( iChannel=0; iChannel<3; iChannel++ )
    {
        vCoefficients[iChannel][0] = f2vt(-fC1*fLight[iChannel][3]);
        vCoefficients[iChannel][1] = f2vt(-fC1*fLight[iChannel][1]);
        vCoefficients[iChannel][2] = f2vt( fC1*fLight[iChannel][2]);
        vCoefficients[iChannel][3] = f2vt( fC0*fLight[iChannel][0] - fC3*fLight[iChannel][6]);
    }

	g_PVRTglesExt.myglProgramEnvParameter4v(GL_VERTEX_PROGRAM_ARB,0,vCoefficients[0]);
	g_PVRTglesExt.myglProgramEnvParameter4v(GL_VERTEX_PROGRAM_ARB,1,vCoefficients[1]);
	g_PVRTglesExt.myglProgramEnvParameter4v(GL_VERTEX_PROGRAM_ARB,2,vCoefficients[2]);

	for( iChannel=0; iChannel<3; iChannel++ )
    {
        vCoefficients[iChannel][0] = f2vt(     fC2*fLight[iChannel][4]);
        vCoefficients[iChannel][1] = f2vt(    -fC2*fLight[iChannel][5]);
        vCoefficients[iChannel][2] = f2vt(3.0f*fC3*fLight[iChannel][6]);
        vCoefficients[iChannel][3] = f2vt(    -fC2*fLight[iChannel][7]);
    }

	g_PVRTglesExt.myglProgramEnvParameter4v(GL_VERTEX_PROGRAM_ARB,3,vCoefficients[0]);
	g_PVRTglesExt.myglProgramEnvParameter4v(GL_VERTEX_PROGRAM_ARB,4,vCoefficients[1]);
	g_PVRTglesExt.myglProgramEnvParameter4v(GL_VERTEX_PROGRAM_ARB,5,vCoefficients[2]);

    vCoefficients[0][0] = f2vt(fC4*fLight[0][8]);
    vCoefficients[0][1] = f2vt(fC4*fLight[1][8]);
    vCoefficients[0][2] = f2vt(fC4*fLight[2][8]);
    vCoefficients[0][3] = f2vt(1.0f);

	g_PVRTglesExt.myglProgramEnvParameter4v(GL_VERTEX_PROGRAM_ARB,6,vCoefficients[0]);
}

/*******************************************************************************
 * Function Name  : SetupVGPConstants
 * Input		  : Enabled Light Sources
 * Description    : Function to setup the VGP constants used for Diffuse and SH Lighting
 *
 *******************************************************************************/
void OGLESPhantomMask::SetupVGPConstants(bool light0, bool light1, bool envlight)
{
	/* SH Data Sets */
	/* Not all are used */

	float	SHCoeffsLight1Red[9] = {0.83409595f, -1.4446964f, 0.00000000f, 0.00000000f, 0.00000000f, 0.00000000f, -0.93254757f, 0.00000000f, -1.6152197f};
	float	SHCoeffsLight1Green[9] = {0.83409595f, -1.4446964f, 0.00000000f, 0.00000000f, 0.00000000f, 0.00000000f, -0.93254757f, 0.00000000f, -1.6152197f};
	float	SHCoeffsLight1Blue[9] = {0.83409595f, -1.4446964f, 0.00000000f, 0.00000000f, 0.00000000f, 0.00000000f, -0.93254757f, 0.00000000f, -1.6152197f};

	float	SHCoeffsLight2Red[9] = {0.83409595f, -1.2120811f, -0.24892779f, -0.74568230f, -1.3989232f, -0.46699628f, -0.84948879f, 0.28729999f, -0.70663643f};
	float	SHCoeffsLight2Green[9] = {0.83409595f, -1.2120811f, -0.24892779f, -0.74568230f, -1.3989232f, -0.46699628f, -0.84948879f, 0.28729999f, -0.70663643f};
	float	SHCoeffsLight2Blue[9] = {0.83409595f, -1.2120811f, -0.24892779f, -0.74568230f, -1.3989232f, -0.46699628f, -0.84948879f, 0.28729999f, -0.70663643f};

	float	SHCoeffsLightEnvRed[9] = {1.2961891f, -0.42659417f, -0.10065936f, -8.4035477e-005f, -0.00021227333f, 0.10019236f, 0.011847760f, 0.00016783635f, -0.10584830f};
	float	SHCoeffsLightEnvGreen[9] = {1.2506844f, -0.12775756f, 0.33325988f, -8.7283181e-005f, -0.00015105936f, -0.025249202f, -0.048718069f, 0.00026852929f, -0.28519103f};
	float	SHCoeffsLightEnvBlue[9] = {1.6430428f, 0.098693930f, 0.071262904f, 0.00044371662f, 0.00027166531f, 0.056100018f, -0.23762819f, -0.00015725456f, -0.49318397f};

	float	SHCoeffsLightSideRed[9] = {	0.83409595f, 0.00000000f, 0.00000000f, -1.4446964f, 0.00000000f, 0.00000000f, -0.93254757f, 0.00000000f, 1.6152197f};
	float	SHCoeffsLightSideGreen[9] = {	0.83409595f, 0.00000000f, 0.00000000f, -1.4446964f, 0.00000000f, 0.00000000f, -0.93254757f, 0.00000000f, 1.6152197f};
	float	SHCoeffsLightSideBlue[9] = {	0.83409595f, 0.00000000f, 0.00000000f, -1.4446964f, 0.00000000f, 0.00000000f, -0.93254757f, 0.00000000f, 1.6152197f};

	float	SHCoeffsLightEnvGraceCrossRed[9] = {10.153550f, -5.0607910f, -4.3494077f, 3.7619650f, -1.4272760f, 3.3470039f, -2.0500889f, -7.1480651f, 2.7244451f};
	float	SHCoeffsLightEnvGraceCrossGreen[9] = {5.6218147f, -4.4867749f, -2.3315217f, 0.71724868f, -0.65607071f, 2.8644383f, -1.2423282f, -2.7321301f, -0.70176142f};
	float	SHCoeffsLightEnvGraceCrossBlue[9] = {6.9620109f, -7.7706318f, -3.4473803f, -0.12024292f, -1.5760463f, 6.0764866f, -1.9274533f, -1.7631743f, -3.9185245f};

	float	SHCoeffsLightSummedRed[9];
	float	SHCoeffsLightSummedGreen[9];
	float	SHCoeffsLightSummedBlue[9];

	VERTTYPE	fValue[4];
	float		LIGHT1WEIGHT,LIGHT2WEIGHT,LIGHTENVWEIGHT,LIGHTSIDEWEIGHT,LIGHTGRACECROSSWEIGHT;

	int i;

	/* SH Weights */

	LIGHT1WEIGHT=0.0f;
	LIGHT2WEIGHT=0.0f;
	LIGHTENVWEIGHT=0.0f;
	LIGHTSIDEWEIGHT=0.0f;
	LIGHTGRACECROSSWEIGHT=0.0f;

	/* Set weights based on scene info */

	if (light0 && light1 && envlight)
	{
		LIGHT1WEIGHT=0.3f;
		LIGHT2WEIGHT=0.3f;
		LIGHTENVWEIGHT=1.0f;
	}
	else if(!light0 && !light1 && envlight)
	{
		LIGHTENVWEIGHT=1.0f;
	}

	/* Calculate the final SH coefs using the different lights and weights */

	for (i=0; i<9;i++)
	{
		SHCoeffsLightSummedRed[i]=LIGHT1WEIGHT*SHCoeffsLight1Red[i]+LIGHT2WEIGHT*SHCoeffsLight2Red[i]+LIGHTENVWEIGHT*SHCoeffsLightEnvRed[i]+LIGHTSIDEWEIGHT*SHCoeffsLightSideRed[i]+LIGHTGRACECROSSWEIGHT*SHCoeffsLightEnvGraceCrossRed[i];
		SHCoeffsLightSummedGreen[i]=LIGHT1WEIGHT*SHCoeffsLight1Green[i]+LIGHT2WEIGHT*SHCoeffsLight2Green[i]+LIGHTENVWEIGHT*SHCoeffsLightEnvGreen[i]+LIGHTSIDEWEIGHT*SHCoeffsLightSideGreen[i]+LIGHTGRACECROSSWEIGHT*SHCoeffsLightEnvGraceCrossGreen[i];
		SHCoeffsLightSummedBlue[i]=LIGHT1WEIGHT*SHCoeffsLight1Blue[i]+LIGHT2WEIGHT*SHCoeffsLight2Blue[i]+LIGHTENVWEIGHT*SHCoeffsLightEnvBlue[i]+LIGHTSIDEWEIGHT*SHCoeffsLightSideBlue[i]+LIGHTGRACECROSSWEIGHT*SHCoeffsLightEnvGraceCrossBlue[i];
	}

	ComputeAndSetSHIrradEnvMapConstants( SHCoeffsLightSummedRed, SHCoeffsLightSummedGreen, SHCoeffsLightSummedBlue  );

	/************************************************************************/
	/*            VERTEX LIGHTING CONSTANTS                                 */
	/************************************************************************/

	/* Light Position 1 : TOP */

	fValue[0]=f2vt(0.0f);
    fValue[1]=f2vt(0.5f);
	fValue[2]=f2vt(0.0f);
	fValue[3]=f2vt(0.0f);
	g_PVRTglesExt.myglProgramEnvParameter4v(GL_VERTEX_PROGRAM_ARB,7,fValue);

	/* Light Position 2 : BOTTOM */

    fValue[0]=f2vt(0.0f);
	fValue[1]=f2vt(-0.5f);
	fValue[2]=f2vt(0.0f);
	fValue[3]=f2vt(0.0f);
	g_PVRTglesExt.myglProgramEnvParameter4v(GL_VERTEX_PROGRAM_ARB,8,fValue);

	/* Light Position 3 : LEFT*/

    fValue[0]=f2vt(-0.5f);
	fValue[1]=f2vt(0.0f);
	fValue[2]=f2vt(0.0f);
	fValue[3]=f2vt(0.0f);
	g_PVRTglesExt.myglProgramEnvParameter4v(GL_VERTEX_PROGRAM_ARB,9,fValue);

	/* Light Position 4 : RIGHT*/

    fValue[0]=f2vt(0.5f);
	fValue[1]=f2vt(0.0f);
	fValue[2]=f2vt(0.0f);
	fValue[3]=f2vt(0.0f);
	g_PVRTglesExt.myglProgramEnvParameter4v(GL_VERTEX_PROGRAM_ARB,10,fValue);

	/* Ambient Light */

	fValue[0]=f2vt(0.05f);
    fValue[1]=f2vt(0.05f);
	fValue[2]=f2vt(0.05f);
	fValue[3]=f2vt(0.05f);
	g_PVRTglesExt.myglProgramEnvParameter4v(GL_VERTEX_PROGRAM_ARB,11,fValue);

}

/*******************************************************************************
 * Function Name  : NewDemo
 * Description    : Called by the Shell to initialize a new instance to the
 *					demo class.
 *******************************************************************************/
PVRShell* NewDemo(){
	return new OGLESPhantomMask();
}

/*****************************************************************************
 End of file (OGLESPhantomMask.cpp)
*****************************************************************************/
