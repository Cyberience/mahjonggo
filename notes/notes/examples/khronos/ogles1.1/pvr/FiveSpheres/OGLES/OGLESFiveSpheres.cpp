/******************************************************************************

 @File         OGLESFiveSpheres.cpp

 @Title        OGLESFiveSpheres

 @Copyright    Copyright (C) 2000 - 2006 by Imagination Technologies Limited.

 @Platform     Independant

 @Description  Shows different primitive types applied to a model. This is more a
               test than a demonstration. Programmers new to OpenGL ES are
               invited to start from a simpler and more featured demo like e.g.
               OGLESVase. The blending modes have been removed to keep the code
               simple and relevant.

******************************************************************************/
#include <math.h>
#include <string.h>

#include "PVRShell.h"
#include "OGLESTools.h"

#include "GRASSFAR.h"

/******************************************************************************
 Defines
******************************************************************************/

#define NUM_RIMS		9		// Number of rims on the sphere. Including the top and bottom rims made of one vertex. Must be odd.
#define RIM_SIZE		16		// Number of vertices in each rim (except the top and bottom rims). Must be even.
#define SPHERE_SCALE	1.2f	// Sphere scale factor.

/******************************************************************************
 Structure definitions
******************************************************************************/

// Enum to specify the sphere we are drawing
enum ESphereType {Points, Triangles, LineStrip, Fan, Strip};

/*!****************************************************************************
 Class implementing the PVRShell functions.
******************************************************************************/
class OGLESFiveSpheres : public PVRShell
{
	// Print3D class used to display text
	CPVRTPrint3D 	m_Print3D;

	// Projection and Model View matrices
	PVRTMATRIX		m_mProjection, m_mView;

	// Texture handle
	GLuint	m_uiTexture;

	// Rotation variables
	VERTTYPE fXAng, fYAng;

	// Vertices, normals and texture coordinates of the sphere
	VERTTYPE		*m_pfVertices, *m_pfNormals, *m_pfUVs;

	// Indices to draw the triangle list
	unsigned short*	m_puiTriListIndices;

	// Indices to draw the triangle fan
	unsigned short* m_puiTriFanIndices;

	// Variables to hold the triangle strips
	unsigned short*	m_puiStrips;
	unsigned int*	m_piStripLength;
	unsigned int	m_iNumStrip;

public:
	OGLESFiveSpheres()
	{
		// Initializes the rotation angles
		fXAng = 0;
		fYAng = 0;
	}

	virtual bool InitApplication();
	virtual bool InitView();
	virtual bool ReleaseView();
	virtual bool QuitApplication();
	virtual bool RenderScene();

	void CreateGeometry();
	int GetVertexIndex(int iRim, int iPosition);
	void SetModelViewMatrix(ESphereType eSphere);
};

/*!****************************************************************************
 @Function		InitApplication
 @Return		bool		true if no error occured
 @Description	Code in InitApplication() will be called by PVRShell once per
				run, before the rendering context is created.
				Used to initialize variables that are not dependant on it
				(e.g. external modules, loading meshes, etc.)
				If the rendering context is lost, InitApplication() will
				not be called again.
******************************************************************************/
bool OGLESFiveSpheres::InitApplication()
{
	// Creates all the geometry needed
	CreateGeometry();

	// Calculates the view matrix
	PVRTMatrixTranslation(m_mView, 0, 0, f2vt(-8.0));

	return true;
}

/*!****************************************************************************
 @Function		QuitApplication
 @Return		bool		true if no error occured
 @Description	Code in QuitApplication() will be called by PVRShell once per
				run, just before exiting the program.
				If the rendering context is lost, QuitApplication() will
				not be called.
******************************************************************************/
bool OGLESFiveSpheres::QuitApplication()
{
	// Deletes the geometry
	delete [] m_puiTriFanIndices;
	delete [] m_puiStrips;
	delete [] m_piStripLength;
	delete [] m_puiTriListIndices;
	delete [] m_pfNormals;
	delete [] m_pfUVs;
	delete [] m_pfVertices;

	return true;
}

/*!****************************************************************************
 @Function		InitView
 @Return		bool		true if no error occured
 @Description	Code in InitView() will be called by PVRShell upon
				initialization or after a change in the rendering context.
				Used to initialize variables that are dependant on the rendering
				context (e.g. textures, vertex buffers, etc.)
******************************************************************************/
bool OGLESFiveSpheres::InitView()
{
	// Initialize Print3D
	if(!m_Print3D.SetTextures(0,PVRShellGet(prefWidth),PVRShellGet(prefHeight)))
	{
		PVRShellSet(prefExitMessage, "ERROR: Cannot initialise Print3D\n");
		return false;
	}

	// Loads the texture
	if (!PVRTLoadTextureFromPointer((void*)GRASSFAR, &m_uiTexture))
	{
		PVRShellSet(prefExitMessage, "ERROR: Cannot load the texture\n");
		return false;
	}
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// Calculates the projection matrix
	bool bRotate = PVRShellGet(prefIsRotated) && PVRShellGet(prefFullScreen);
	PVRTMatrixPerspectiveFovRH(m_mProjection, f2vt(0.6f), f2vt(1.33f), f2vt(0.01f), f2vt(100.0f), bRotate);

	// Set point size
	myglPointSize(f2vt(2.0f));

	// Set front face direction
	glFrontFace(GL_CW);

	// Loads the projection matrix
	glMatrixMode(GL_PROJECTION);
	myglLoadMatrix(m_mProjection.f);

	// Set the clear color
	myglClearColor(f2vt(0.4f), f2vt(0.4f), f2vt(0.4f), f2vt(0.0f));

	// Set material properties
	VERTTYPE fObjectMatAmb[]	= { f2vt(0.1f), f2vt(0.1f), f2vt(0.1f), f2vt(1.0f)};
	VERTTYPE fObjectMatDiff[]	= { f2vt(0.5f), f2vt(0.5f), f2vt(0.5f), f2vt(1.0f)};
	VERTTYPE fObjectMatSpec[]	= { f2vt(1.0f), f2vt(1.0f), f2vt(1.0f), f2vt(1.0f)};
	myglMaterialv(GL_FRONT_AND_BACK, GL_DIFFUSE, fObjectMatDiff);
	myglMaterialv(GL_FRONT_AND_BACK, GL_AMBIENT, fObjectMatAmb);
	myglMaterialv(GL_FRONT_AND_BACK, GL_SPECULAR, fObjectMatSpec);
	myglMaterial(GL_FRONT_AND_BACK, GL_SHININESS, f2vt(5));

	// Set lighting properties (light position set in RenderScene())
	VERTTYPE fLightAmb[4]  = { f2vt(0.1f), f2vt(0.1f), f2vt(0.1f), f2vt(1.0f) };
	VERTTYPE fLightDif[4]  = { f2vt(1.0f), f2vt(1.0f), f2vt(1.0f), f2vt(1.0f) };
	VERTTYPE fLightSpec[4]	= { f2vt(1.0f), f2vt(1.0f), f2vt(1.0f), f2vt(1.0f) };
	VERTTYPE fAmbient[4]	= { f2vt(1.0f), f2vt(1.0f), f2vt(1.0f), f2vt(1.0f) };
	myglLightv(GL_LIGHT0, GL_AMBIENT, fLightAmb);
	myglLightv(GL_LIGHT0, GL_DIFFUSE, fLightDif);
	myglLightv(GL_LIGHT0, GL_SPECULAR, fLightSpec);
	myglLight(GL_LIGHT0, GL_SPOT_EXPONENT, f2vt(5.0f));
	myglLightModelv(GL_LIGHT_MODEL_AMBIENT, fAmbient);

	// Set the light direction
	glMatrixMode(GL_MODELVIEW);
	myglLoadMatrix(m_mView.f);
	VERTTYPE fLightPos[4]  = { f2vt(0.0f), f2vt(0.0f), f2vt(+1.0f), 0 };
	myglLightv(GL_LIGHT0, GL_POSITION, fLightPos);

	return true;
}

/*!****************************************************************************
 @Function		ReleaseView
 @Return		bool		true if no error occured
 @Description	Code in ReleaseView() will be called by PVRShell when the
				application quits or before a change in the rendering context.
******************************************************************************/
bool OGLESFiveSpheres::ReleaseView()
{
	// Release the textures
	PVRTReleaseTexture(m_uiTexture);

	// Release Print3D textures
	m_Print3D.ReleaseTextures();
	return true;
}

/*!****************************************************************************
 @Function		RenderScene
 @Return		bool		true if no error occured
 @Description	Main rendering loop function of the program. The shell will
				call this function every frame.
				eglSwapBuffers() will be performed by PVRShell automatically.
				PVRShell will also manage important OS events.
				Will also manage relevent OS events. The user has access to
				these events through an abstraction layer provided by PVRShell.
******************************************************************************/
bool OGLESFiveSpheres::RenderScene()
{
	int ii, jj;

	// Clears the buffers
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/*
		Setup the OpenGL ES states needed.
	*/

	// Enables texturing
	glEnable(GL_TEXTURE_2D);

	// Setup back-face culling
	glFrontFace(GL_CW);
	glCullFace(GL_FRONT);
	glEnable(GL_CULL_FACE);

	// Use the texture we loaded
	glBindTexture(GL_TEXTURE_2D, m_uiTexture);

	// Enable lighting - this needs to be re-enabled every frame because Print3D will disable it
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	// Gives the vertex, normals and texture coordinates to OpenGL ES
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glVertexPointer(3,VERTTYPEENUM,0,m_pfVertices);
	glNormalPointer(VERTTYPEENUM, 0, m_pfNormals);
	glTexCoordPointer(2, VERTTYPEENUM, 0, m_pfUVs);

	/*
		Draw the points.
	*/
	SetModelViewMatrix(Points);
	glDrawArrays(GL_POINTS, 0, (NUM_RIMS-2)*RIM_SIZE+2);

	/*
		Draw the triangle list.
	*/
	SetModelViewMatrix(Triangles);
	glDrawElements(GL_TRIANGLES, (RIM_SIZE + (NUM_RIMS-3)*RIM_SIZE*2 + RIM_SIZE) * 3, GL_UNSIGNED_SHORT, m_puiTriListIndices);

	/*
		Draw the triangle fan.
	*/
	SetModelViewMatrix(Fan);
	// First draw the two caps
	glDrawElements(GL_TRIANGLE_FAN, RIM_SIZE+2, GL_UNSIGNED_SHORT, m_puiTriFanIndices);
	glDrawElements(GL_TRIANGLE_FAN, RIM_SIZE+2, GL_UNSIGNED_SHORT, m_puiTriFanIndices+RIM_SIZE+2);
	// Then draw all the other fans organized in an array over the sphere
	for (ii=2; ii<=(NUM_RIMS-3); ii+=2)
		for (jj=0; jj<RIM_SIZE; jj+=2)
			glDrawElements(GL_TRIANGLE_FAN, 10, GL_UNSIGNED_SHORT, m_puiTriFanIndices+(RIM_SIZE+2)*2+ ((ii/2)-1)*10*RIM_SIZE/2 + (jj/2)*10);

	/*
		Draw the line strip.
	*/
	SetModelViewMatrix(LineStrip);
	glDrawElements(GL_LINE_STRIP, (RIM_SIZE + (NUM_RIMS-3)*RIM_SIZE*2 + RIM_SIZE) * 3, GL_UNSIGNED_SHORT, m_puiTriListIndices);

	/*
		Draw the triangle strip.
	*/
	SetModelViewMatrix(Strip);
	int iDrawn = 0;
	for (ii=0; ii<(int)m_iNumStrip; ii++)
	{
		glDrawElements(GL_TRIANGLE_STRIP, m_piStripLength[ii]+2, GL_UNSIGNED_SHORT, &((unsigned short*)m_puiStrips)[iDrawn]);
		iDrawn += m_piStripLength[ii]+2;
	}

	// Increase rotation angles
	fXAng += f2vt(1.0f / 100.0f);
	fYAng += f2vt(1.0f / 100.0f);

	// Display info text
	m_Print3D.DisplayDefaultTitle("FiveSpheres", "Primitives test.", PVR_LOGO);
	m_Print3D.Flush();

	return true;
}

/*!****************************************************************************
 @Function		CreateGeometry
 @Description	Creates all the geometry used in this demo.
******************************************************************************/
void OGLESFiveSpheres::CreateGeometry()
{
	int i, j;

	/*
		Creates the sphere vertices and texture coordinates
	*/
	float fSphereScale = 1.2f;
	m_pfVertices = new VERTTYPE[((NUM_RIMS-2)*RIM_SIZE+2)*3];
	m_pfUVs = new VERTTYPE[((NUM_RIMS-2)*RIM_SIZE+2)*2];

	// Bottom vertex
	m_pfVertices[0] = 0;
	m_pfVertices[1] = f2vt(-0.5f * SPHERE_SCALE);
	m_pfVertices[2] = 0;
	m_pfUVs[0] = f2vt(0.5f);
	m_pfUVs[1] = f2vt(0.5f);

	// 7 rims of 16 vertices each
	float fYAngleInc = PVRTPIf / (float)(NUM_RIMS-1);
	float fYAngle = fYAngleInc;
	int iIndex = 1;
	for (i=0; i<(NUM_RIMS-2); i++)
	{
		float fPosY = -(float)cos(fYAngle) / 2.0f;
		fYAngle += fYAngleInc;
		for (int j=0; j<RIM_SIZE; j++)
		{
			float fAngle = (float)j * 2.0f * PVRTPIf / (float)RIM_SIZE;
			float fSize = (float)cos(asin(fPosY*2)) / 2.0f;
			m_pfVertices[GetVertexIndex(i+1,j)*3+0] = VERTTYPEMUL( f2vt((float)PVRTFCOS(fAngle)), f2vt(fSize*SPHERE_SCALE) );
			m_pfVertices[GetVertexIndex(i+1,j)*3+1] = VERTTYPEMUL( f2vt(fPosY), f2vt(fSphereScale));
			m_pfVertices[GetVertexIndex(i+1,j)*3+2] = VERTTYPEMUL( f2vt((float)PVRTFSIN(fAngle)), f2vt(fSize*SPHERE_SCALE) );
			m_pfUVs[GetVertexIndex(i+1,j)*2+0] = m_pfVertices[GetVertexIndex(i+1,j)*3+0] + f2vt(0.5f);
			m_pfUVs[GetVertexIndex(i+1,j)*2+1] = m_pfVertices[GetVertexIndex(i+1,j)*3+2] + f2vt(0.5f);
			iIndex++;
		}
	}

	// Top vertex
	m_pfVertices[iIndex*3+0] = 0;
	m_pfVertices[iIndex*3+1] = f2vt(+0.5f * fSphereScale);
	m_pfVertices[iIndex*3+2] = 0;
	m_pfUVs[iIndex*2+0] = f2vt(0.5f);
	m_pfUVs[iIndex*2+1] = f2vt(0.5f);

	/*
		Creates the sphere normals.
	*/
	m_pfNormals = new VERTTYPE[((NUM_RIMS-2)*RIM_SIZE+2)*3];
	for (i=0; i<(NUM_RIMS-2)*RIM_SIZE+2; i++)
	{
		PVRTVECTOR3 vNormal;
		vNormal.x = m_pfVertices[i*3+0];
		vNormal.y = m_pfVertices[i*3+1];
		vNormal.z = m_pfVertices[i*3+2];
		PVRTMatrixVec3Normalize(vNormal, vNormal);
		m_pfNormals[i*3+0] = vNormal.x;
		m_pfNormals[i*3+1] = vNormal.y;
		m_pfNormals[i*3+2] = vNormal.z;
	}

	/*
		Creates the indices for the triangle list.
	*/
	m_puiTriListIndices = new unsigned short[(RIM_SIZE + (NUM_RIMS-3)*RIM_SIZE*2 + RIM_SIZE) * 3];
	// From bottom vertex to lowest rim and from top vertex to highest rim
	int iRimTopIndex = (RIM_SIZE + (NUM_RIMS-3)*RIM_SIZE*2) * 3;
	for (i=0; i<RIM_SIZE; i++)
	{
		m_puiTriListIndices[i*3+2] = GetVertexIndex(0,0);
		m_puiTriListIndices[i*3+1] = GetVertexIndex(1, (i+1)%RIM_SIZE);
		m_puiTriListIndices[i*3+0] = GetVertexIndex(1, i);

		m_puiTriListIndices[iRimTopIndex+i*3+2] = GetVertexIndex((NUM_RIMS-1), 0);
		m_puiTriListIndices[iRimTopIndex+i*3+1] = GetVertexIndex((NUM_RIMS-2), i);
		m_puiTriListIndices[iRimTopIndex+i*3+0] = GetVertexIndex((NUM_RIMS-2), (i+1)%RIM_SIZE);
	}
	// From rim to rim
	for (i=1; i<NUM_RIMS-2; i++)
		for (j=0; j<RIM_SIZE; j++)
		{
			m_puiTriListIndices[RIM_SIZE*3 + (i-1)*RIM_SIZE*2*3 + j*2*3 + 5] = GetVertexIndex(i,j);
			m_puiTriListIndices[RIM_SIZE*3 + (i-1)*RIM_SIZE*2*3 + j*2*3 + 4] = GetVertexIndex(i,(j+1)%RIM_SIZE);
			m_puiTriListIndices[RIM_SIZE*3 + (i-1)*RIM_SIZE*2*3 + j*2*3 + 3] = GetVertexIndex(i+1,j);
			m_puiTriListIndices[RIM_SIZE*3 + (i-1)*RIM_SIZE*2*3 + j*2*3 + 2] = GetVertexIndex(i+1,j);
			m_puiTriListIndices[RIM_SIZE*3 + (i-1)*RIM_SIZE*2*3 + j*2*3 + 1] = GetVertexIndex(i,(j+1)%RIM_SIZE);
			m_puiTriListIndices[RIM_SIZE*3 + (i-1)*RIM_SIZE*2*3 + j*2*3 + 0] = GetVertexIndex(i+1,(j+1)%RIM_SIZE);
		}

	/*
		Generates the triangle strips using our tools.
	*/
	PVRTTriStrip(&m_puiStrips, &m_piStripLength, &m_iNumStrip, m_puiTriListIndices, RIM_SIZE + (NUM_RIMS-3)*RIM_SIZE*2 + RIM_SIZE);

	/*
		Creates the indices for the triangle fan.
	*/
	m_puiTriFanIndices = new unsigned short[1+(RIM_SIZE+1) + 1+(RIM_SIZE+1) + (NUM_RIMS-3)*(RIM_SIZE/2)*10];
	// Does the caps at the bottom and top
	m_puiTriFanIndices[0] = GetVertexIndex(0,0);
	m_puiTriFanIndices[RIM_SIZE+2] = GetVertexIndex((NUM_RIMS-1),0);
	for (i=0; i<RIM_SIZE+1; i++)
	{
		m_puiTriFanIndices[i+1] = GetVertexIndex(1,i%RIM_SIZE);
		m_puiTriFanIndices[(RIM_SIZE+2)+i+1] = GetVertexIndex((NUM_RIMS-2),(RIM_SIZE-i)%RIM_SIZE);
	}
	// Fans the rest of the sphere
	iIndex = (RIM_SIZE+2)*2;
	for (i=2; i<=(NUM_RIMS-3); i+=2)
		for (j=1; j<RIM_SIZE; j+=2)
		{
			m_puiTriFanIndices[iIndex++] = GetVertexIndex(i,	j);
			m_puiTriFanIndices[iIndex++] = GetVertexIndex(i,	(j+1)%RIM_SIZE);
			m_puiTriFanIndices[iIndex++] = GetVertexIndex(i-1,	(j+1)%RIM_SIZE);
			m_puiTriFanIndices[iIndex++] = GetVertexIndex(i-1,	j);
			m_puiTriFanIndices[iIndex++] = GetVertexIndex(i-1,	(j-1)%RIM_SIZE);
			m_puiTriFanIndices[iIndex++] = GetVertexIndex(i,	(j-1)%RIM_SIZE);
			m_puiTriFanIndices[iIndex++] = GetVertexIndex(i+1,	(j-1)%RIM_SIZE);
			m_puiTriFanIndices[iIndex++] = GetVertexIndex(i+1,	j);
			m_puiTriFanIndices[iIndex++] = GetVertexIndex(i+1,	(j+1)%RIM_SIZE);
			m_puiTriFanIndices[iIndex++] = GetVertexIndex(i,	(j+1)%RIM_SIZE);
		}
}

/*!****************************************************************************
 @Function		GetVertexIndex
 @Input			iRim		rim where the vertex is (between 0 and NUM_RIMS-1)
 @Input			iPosition	position on the rim where the vertex is (between 0 and RIM_SIZE-1)
 @Return		int			Index of the vertex in the list
 @Description	Returns the index of a vertex in the list by its position.
******************************************************************************/
int OGLESFiveSpheres::GetVertexIndex(int iRim, int iPosition)
{
	if (iRim==0)
		return 0;
	if (iRim==(NUM_RIMS-1))
		return 1 + RIM_SIZE*(NUM_RIMS-2);
	return 1 + RIM_SIZE*(iRim-1) + iPosition;
}

/*!****************************************************************************
 @Function		SetModelViewMatrix
 @Input			eSphere		Sphere to set the matrix for
 @Description	Sets the model view matrix for a specific sphere.
******************************************************************************/
void OGLESFiveSpheres::SetModelViewMatrix(ESphereType eSphere)
{
	// Each sphere specific translations
	VERTTYPE aTranslations[] = {
		f2vt(-0.7f), f2vt(0.6f), f2vt(0.0f),
		f2vt(+0.7f), f2vt(0.6f), f2vt(0.0f),
		f2vt(-1.5f), f2vt(-0.6f), f2vt(0.0f),
		f2vt(0.0f), f2vt(-0.6f), f2vt(0.0f),
		f2vt(+1.5f), f2vt(-0.6f), f2vt(0.0f)
	};

	// Compose the sphere specific translation with two rotations to get the Model matrix
	PVRTMATRIX mModel, mRotationX, mRotationY, mTranslation, mTemp;
	PVRTMatrixTranslation(mTranslation, aTranslations[eSphere*3+0], aTranslations[eSphere*3+1], aTranslations[eSphere*3+2]);
	PVRTMatrixRotationX(mRotationX, fXAng);
	PVRTMatrixRotationY(mRotationY, fYAng);
	PVRTMatrixMultiply(mTemp, mTranslation, mRotationY);
	PVRTMatrixMultiply(mModel, mTemp, mRotationX);

	// Multiply the Model matrix with the View matrix to get the Model-View matrix
	PVRTMATRIX mModelView;
	PVRTMatrixMultiply(mModelView, mModel, m_mView);

	// Loads the Model-View matrix into OpenGL ES
	glMatrixMode(GL_MODELVIEW);
	myglLoadMatrix(mModelView.f);
}

/*!****************************************************************************
 @Function		NewDemo
 @Return		PVRShell*		The demo supplied by the user
 @Description	This function must be implemented by the user of the shell.
				The user should return its PVRShell object defining the
				behaviour of the application.
******************************************************************************/
PVRShell* NewDemo()
{
	return new OGLESFiveSpheres();
}

/*****************************************************************************
 End of file (OGLESFiveSpheres.cpp)
*****************************************************************************/
