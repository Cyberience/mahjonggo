/******************************************************************************

 @File         OGLESChameleonMan.cpp

 @Title        ChameleonMan

 @Copyright    Copyright (C) 2005 - 2006 by Imagination Technologies Limited.

 @Platform     Independant

 @Description  Shows Skinning and DOT3 Lighting using an IMG Vertex Program.
               Requires the PVRShell. Requires VGP support.

******************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "PVRShell.h"
#include "OGLESTools.h"

/* Textures */
/* Base Textures */
#include "FinalChameleonManHeadBody.h"
#include "FinalChameleonManLegs.h"
#include "ChameleonBelt.h"
#include "skyline.h"
#include "Wall_diffuse_baked.h"
#include "lamp.h"

/* Tangent Space BumpMap Textures */
#include "Tang_space_BodyMap.h"
#include "Tang_space_LegsMap.h"
#include "Tang_space_BeltMap.h"

/* Vertex Programs */

#include "DOT3_Skinning_VGPARMVP.h"
#include "VertexLit_Skinning_VGPARMVP.h"
#include "DOT3_Skinning_VGP.h"
#include "VertexLit_Skinning_VGP.h"
#include "DOT3_Skinning_VGPLITE.h"
#include "VertexLit_Skinning_VGPLITE.h"


/****************************************************************************
 ** DEFINES                                                                **
 ****************************************************************************/

/* Build option to allow build with or without bumpmapping */
#if defined BUILD_NONDOT3
	#define BUILD_OPTION 0		// Render with Vertex Lighting
#else
	#define BUILD_OPTION 1		// Render with Per Pixel BumpMapping
#endif

/* Geometry Data File Name */
#ifdef PVRTFIXEDPOINTENABLE
#define FILENAME_SCENE ("ChameleonScene_Fixed.pod")
#else
#define FILENAME_SCENE ("ChameleonScene.pod")
#endif

/* Number of Vertex Programs */
#define NUM_OF_PROGRAMS		 2

/* Maximum Number of Meshes supported - note : remember splitting due to number of bone matrices supported */
#define MAX_NMBR_OF_MESHES	50

/* Max number of bones per Mesh supported */
#define MAX_BONES			 8

/* Camera related defines */
#define CAM_ASPECT	f2vt(1.333333333f)			// Assuming a 4:3 aspect ratio:
#define CAM_NEAR	f2vt(10.0f)
#define CAM_FAR		f2vt(30000.0f)

/* Influences the animation speed - tweak this for HW/SW usage - too slow/fast */
#define ANIMATION_FACTOR		f2vt(0.486f)

/****************************************************************************
** Structures
****************************************************************************/

/* Per Vertex Data Structure */
struct SVtx
{
	VERTTYPE		x, y, z;			// Position
	VERTTYPE		u, v;				// TexCoord
	VERTTYPE		nx, ny, nz;			// Normal
	unsigned char	nMatrixIndices[4];	// Matrix Indices
	unsigned char	nBlendWeights[4];	// Blend Weight
	VERTTYPE		bx, by, bz;			// BiNormal
	VERTTYPE		tx, ty, tz;			// Tangent
};

/* Per Object Data Structure */
struct SOb
{
	int				nCount;					// Number of Indices to draw
	int				nFirstIndex;			// First Index to draw
	int				nMatrixList[MAX_BONES];	// Matrix Indices to set when drawing
	int				nNumberOfMatrices;		// Actual Number of Matrices that need to be set
	int				nObjectID;				// Original Object ID (in case of splitting so the correct texture can be set
	int				nCopy;					// Is this a copy or real data - to avoid double free-ing of data
	unsigned short	*Ib;					// Pointer to Indices
	SVtx			*Vb;					// Pointer to Vertex Structure
};

/****************************************************************************
** Class: OGLESChameleonMan
****************************************************************************/

class OGLESChameleonMan : public PVRShell
{
    /* Animation related variables */
	VERTTYPE	gfAnimationFrame, gfLightPos, gfWallPos, gfBgPos;

	/* Texture IDs */
	GLuint guiTextureHeadBody;
	GLuint guiTextureLegs;
	GLuint guiTextureBeltNormalMap;
	GLuint guiTextureHeadNormalMap;
	GLuint guiTextureLegsNormalMap;
	GLuint guiTextureSkyLine;
	GLuint guiTextureWall;
	GLuint guiTextureLamp;
	GLuint guiTextureBelt;

	/* Vertex Program Related Vars */
	GLuint guiProgs[NUM_OF_PROGRAMS];

	/* Print3D Class Object */
	CPVRTPrint3D	gsMyPrint3D;

	/* POD Class Object */
	CPVRTPODScene	g_sScene;

	/* Extension Class Object */
	CPVRTglesExt	g_glesExt;

	/* Created resources */
	SOb	*g_pOb;

	/* View and Projection Matrices */
	PVRTMATRIX	g_mView, g_mProj;

	/* Bone Batching Related Vars */
	int			g_nNumberOfRenderCalls;

	/* Status Vars */
	bool	g_bEnableDOT3;
	int		gnCombinersPresent;
	int		gnIMGTextureFFExtPresent;

public:
	OGLESChameleonMan()
	{
		g_bEnableDOT3 = BUILD_OPTION;
	}

	/****************************************************************************
	** PROTOTYPES                                                             **
	****************************************************************************/

	/* PVRShell functions */
	virtual bool InitApplication();
	virtual bool InitView();
	virtual bool ReleaseView();
	virtual bool QuitApplication();
	virtual bool RenderScene();

	/* function definitions */
	bool InitIMG_vertex_program();
	bool LoadTextures();
	bool LoadObjects();
	bool LoadVertexPrograms(const void *pSkinningProg,size_t nSzSkinningProg,const void *pVertLitProg,size_t nSzVertLitProg);
	void CameraGetMatrix();
};

/*******************************************************************************
 * Function Name  : InitApplication
 * Inputs		  : argc, *argv[], uWidth, uHeight
 * Returns        : true if no error occured
 * Description    : Code in InitApplication() will be called by the Shell ONCE per
 *					run, early on in the execution of the program.
 *					Used to initialize variables that are not dependant on the
 *					rendering context (e.g. external modules, loading meshes, etc.)
 *******************************************************************************/
bool OGLESChameleonMan::InitApplication()
{
	/* Gets the Data Path */
	const char	*dataPath;
	char		*filename = new char[2048];

	PVRShellSet(prefAppName, "ChameleonMan");

	dataPath = (char*)PVRShellGet(prefDataPath);
	if (!dataPath)
	{
		PVRShellSet(prefExitMessage, "**ERROR** Failed to provide a buffer large enough for OGLESGetDataPath");
		return false;
	}

	/* Load POD File Data */
	sprintf(filename, "%s%s", dataPath, FILENAME_SCENE);
	if(!g_sScene.ReadFromFile(filename))
	{
		PVRShellSet(prefExitMessage, "**ERROR** Failed to load POD scene!");
		return false;
	}
	delete [] filename;

	/* Allocate Base Data Structures */
	g_pOb = (SOb *)calloc(MAX_NMBR_OF_MESHES, sizeof(*g_pOb));

	return true;
}


/*******************************************************************************
 * Function Name  : QuitApplication
 * Returns        : true if no error occured
 * Description    : Code in QuitApplication() will be called by the Shell ONCE per
 *					run, just before exiting the program.
 *******************************************************************************/
bool OGLESChameleonMan::QuitApplication()
{
	/* Release Base Data Structure Data */
	FREE(g_pOb);
	g_sScene.Destroy();

	return true;
}

/*******************************************************************************
 * Function Name  : InitView
 * Inputs		  : uWidth, uHeight
 * Returns        : true if no error occured
 * Description    : Code in InitView() will be called by the Shell upon a change
 *					in the rendering context.
 *					Used to initialize variables that are dependant on the rendering
 *					context (e.g. textures, vertex buffers, etc.)
 *******************************************************************************/
bool OGLESChameleonMan::InitView()
{
	/* Initialise variables */
	gfAnimationFrame	= 0;
	gfLightPos			= 0;
	gfWallPos			= 0;
	gfBgPos				= 0;

	g_nNumberOfRenderCalls = 0;

	/*
		IMG_vertex_program
	*/
	if(!InitIMG_vertex_program())
		return false;

	/*
		Load Textures
	*/
	if(!LoadTextures())
		return false;

	/*
		Create Objects and render info
	*/
	if(!LoadObjects())
		return false;

	/******************************
	** Projection Matrix         **
	*******************************/

	/* Get Camera Data */
	CameraGetMatrix();

	/******************************
	** GENERIC RENDER STATES     **
	*******************************/

	// The Type Of Depth Test To Do
	glDepthFunc(GL_LEQUAL);

	// Enables Depth Testing
	glEnable(GL_DEPTH_TEST);

	// Enables Smooth Color Shading
	glShadeModel(GL_SMOOTH);

	/* Enable texturing */
	glEnable(GL_TEXTURE_2D);

	return true;
}

/*******************************************************************************
 * Function Name  : ReleaseView
 * Returns        : Nothing
 * Description    : Code in ReleaseView() will be called by the Shell before
 *					changing to a new rendering context.
 *******************************************************************************/
bool OGLESChameleonMan::ReleaseView()
{
	int i;

	/* Release all Textures */
	PVRTReleaseTexture(guiTextureLegs);
	PVRTReleaseTexture(guiTextureBeltNormalMap);
	PVRTReleaseTexture(guiTextureHeadNormalMap);
	PVRTReleaseTexture(guiTextureLegsNormalMap);
	PVRTReleaseTexture(guiTextureSkyLine);
	PVRTReleaseTexture(guiTextureWall);
	PVRTReleaseTexture(guiTextureLamp);
	PVRTReleaseTexture(guiTextureBelt);

	/* Release Vertex Programs */
	g_glesExt.glDeleteProgramsARB(NUM_OF_PROGRAMS, guiProgs);

	/* Release the Print3D textures and windows */
	gsMyPrint3D.DeleteAllWindows();
	gsMyPrint3D.ReleaseTextures();

	/* Release Geometry Data */
	for(i = 0; i < g_nNumberOfRenderCalls; ++i)
	{
		/* Release Index and Vertex Buffer Data */
		if(g_pOb[i].nCopy!=1)
		{
			/* But only when it is not a copy - copies are made for rendering the data in multiple calls*/
            FREE(g_pOb[i].Ib);
			FREE(g_pOb[i].Vb);
		}
	}
	return true;
}


/*******************************************************************************
 * Function Name  : RenderScene
 * Returns		  : true if no error occured
 * Description    : Main rendering loop function of the program. The shell will
 *					call this function every frame.
 *******************************************************************************/
bool OGLESChameleonMan::RenderScene()
{
	PVRTMATRIX	mTrans, mWorld, mSetRot, mWorldWall;
	SPODNode	*pNode;
	SOb			*pOb;
	int			i,j;
	VERTTYPE	LightPosition[4];

	bool bShowDiffuse;
	bool bSecondLayer;

	PVRTMATRIX	mBoneWorld;

	/* Set default render states - mainly recovery from the changes that Print3D does at the end of the frame */
	/* Buffer clears */
	myglClearColor(f2vt(0.5f), f2vt(0.5f), f2vt(0.5f), f2vt(0));
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/* Set Z compare properties */
	glEnable(GL_DEPTH_TEST);

	/* Disable Blending and Lighting*/
	glDisable(GL_BLEND);
	glDisable(GL_LIGHTING);

	/* Update Light Position and related VGP Program constant */
	LightPosition[0] = f2vt(200.0f);
	LightPosition[1] = f2vt(350.0f);
	LightPosition[2] = VERTTYPEMUL(f2vt(200.0f), PVRTABS(PVRTSIN(VERTTYPEDIV(PVRTPIx, f2vt(4.0f)) + gfLightPos)));
	LightPosition[3] = f2vt(1.0f);

	g_glesExt.myglProgramEnvParameter4v(GL_VERTEX_PROGRAM_ARB,0,LightPosition);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (PVRShellGet(prefIsRotated) && PVRShellGet(prefFullScreen))
	{
#if defined(UNDER_CE) && !defined(WIN32_PLATFORM_WFSP)
		{
			PVRTMATRIX m = {
				0, 1, 0, 0,
				1, 0, 0, 0,
				0, 0, 1, 0,
				0, 0, 0, 1
			};

			myglMultMatrix(m.f);
		}
#endif
		myglRotate(f2vt(90), f2vt(0), f2vt(0), f2vt(1));
	}

	myglMultMatrix(g_mProj.f);	// Set Camera Based Data */

	/* Set Constants and Matrices based on current mode */
	/* Set model view matrix */
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	/* Bind the VGP progtam for Vertex Lighting or for DOT3 Lighting */
	if (g_bEnableDOT3)
	{
		g_glesExt.glBindProgramARB(GL_VERTEX_PROGRAM_ARB, guiProgs[0]); // DOT3 Program
	}
	else
	{
        g_glesExt.glBindProgramARB(GL_VERTEX_PROGRAM_ARB, guiProgs[1]); // Vertex Lighting Program
	}

	/* Enable Vertex Program */
	glEnable(GL_VERTEX_PROGRAM_ARB);

	/* Render the geometry with the correct texture - Vertex Lighting does not use the DOT3 Maps */
	if (g_bEnableDOT3)
	{
        bShowDiffuse = false;
		bSecondLayer = true;
	}
	else
	{
		bShowDiffuse = true;
		bSecondLayer = false;
	}

	for(i = 0; i < 4; i++)
	{
		/* Texture Setup */
		/* Layer 0 */
		glActiveTexture(GL_TEXTURE0);

		/* Bind correct Texture */
		if (g_pOb[i].nObjectID==0)
		{
			glBindTexture(GL_TEXTURE_2D, bShowDiffuse ? guiTextureHeadBody : guiTextureHeadNormalMap);
		}
		else if (g_pOb[i].nObjectID==1)
		{
			glBindTexture(GL_TEXTURE_2D, bShowDiffuse ? guiTextureLegs : guiTextureLegsNormalMap);
		}
		else
		{
			glBindTexture(GL_TEXTURE_2D, bShowDiffuse ? guiTextureBelt : guiTextureBeltNormalMap);
		}

		/* TexEnv for Layer 0 */
		if (!bShowDiffuse)
		{
			if (gnCombinersPresent)
			{
				myglTexEnv(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE);
				myglTexEnv(GL_TEXTURE_ENV, GL_COMBINE_RGB, GL_DOT3_RGBA);
				myglTexEnv(GL_TEXTURE_ENV, GL_SOURCE0_RGB, GL_TEXTURE);
				myglTexEnv(GL_TEXTURE_ENV, GL_SOURCE1_RGB, GL_PREVIOUS);
			}
			else if (gnIMGTextureFFExtPresent)
			{
				myglTexEnv(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_DOT3_RGBA);
			}
			else
			{
				PVRShellSet(prefExitMessage, "**ERROR** neither combiners nor GL_IMG_texture_env_enhanced_fixed_function present\n");
				return false;
			}
		}
		else
		{
			if (gnCombinersPresent)
			{
				myglTexEnv(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, (VERTTYPE)(g_bEnableDOT3 ? GL_REPLACE : GL_MODULATE));
				myglTexEnv(GL_TEXTURE_ENV, GL_SOURCE0_RGB, (VERTTYPE)(g_bEnableDOT3 ? GL_PREVIOUS : GL_TEXTURE));
				myglTexEnv(GL_TEXTURE_ENV, GL_SOURCE1_RGB, GL_PREVIOUS);
			}
			else if (gnIMGTextureFFExtPresent)
			{
				myglTexEnv(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, (VERTTYPE)(g_bEnableDOT3 ? GL_REPLACE : GL_MODULATE));
			}
			else
			{
				PVRShellSet(prefExitMessage, "**ERROR** neither combiners nor GL_IMG_texture_env_enhanced_fixed_function present\n");
				return false;
			}
		}

		/* Layer 1 */
		glActiveTexture(GL_TEXTURE1);

		/* Bind Correct Texture */
		if (g_pOb[i].nObjectID==0)
		{
			glBindTexture(GL_TEXTURE_2D, guiTextureHeadBody);
		}
		else if (g_pOb[i].nObjectID==1)
		{
			glBindTexture(GL_TEXTURE_2D, guiTextureLegs);
		}
		else
		{
			glBindTexture(GL_TEXTURE_2D, guiTextureBelt);
		}

		/* Texture Options */
		if (bSecondLayer)
		{
			glEnable(GL_TEXTURE_2D);
			myglTexParameter	(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			myglTexParameter	(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

			/* TexEnv for Layer 1 */
			if (gnCombinersPresent)
			{
				myglTexEnv(GL_TEXTURE_ENV, GL_COMBINE_RGB, GL_MODULATE);
				myglTexEnv(GL_TEXTURE_ENV, GL_SOURCE0_RGB, GL_TEXTURE);
				myglTexEnv(GL_TEXTURE_ENV, GL_SOURCE1_RGB, GL_PREVIOUS);
			}
			else if (gnIMGTextureFFExtPresent)
			{
				myglTexEnv(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
			}
			else
			{
				PVRShellSet(prefExitMessage, "**ERROR** neither combiners nor GL_IMG_texture_env_enhanced_fixed_function present\n");
				return false;
			}
		}
		else
		{
			/* Layer Not Active */
			glDisable(GL_TEXTURE_2D);
		}

		/* Bone Data Setup */
		/* Set current frame */
		g_sScene.SetFrame(gfAnimationFrame);

		for(j = 0; j <g_pOb[i].nNumberOfMatrices; j++)
		{
			g_sScene.GetBoneWorldMatrix(mBoneWorld, g_sScene.pNode[g_pOb[i].nObjectID], g_sScene.pNode[g_pOb[i].nMatrixList[j]]);
			glMatrixMode(GL_MATRIX0_ARB+j);
			myglLoadMatrix(mBoneWorld.f);
		}

		/* Get pointers for this object	*/
		pOb = &g_pOb[i];						// Additional app per-mesh-data (e.g. index & vertex buffers)

		// When skinning only the View Matrix is required, no need to use a World Matrix for the Obj
		glMatrixMode(GL_MODELVIEW);
		myglLoadMatrix(g_mView.f);

		/*	Render object	*/
		/* Enable and Set Default Client States and Pointers: Vertex, Normal and UVs */
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_NORMAL_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glVertexPointer(3, VERTTYPEENUM, sizeof(SVtx), &pOb->Vb[0].x);
		glNormalPointer(VERTTYPEENUM, sizeof(SVtx), &pOb->Vb[0].nx);
		glTexCoordPointer(2,VERTTYPEENUM, sizeof(SVtx),&pOb->Vb[0].u);

		/* Define Generic Vertex Attribs to supply Blend Weight and Matrix Index */
		g_glesExt.glVertexAttribPointerARB(1,4,GL_UNSIGNED_BYTE_NORM_IMG,GL_FALSE,sizeof(SVtx),&pOb->Vb[0].nBlendWeights);
		g_glesExt.glVertexAttribPointerARB(2,4,GL_BYTE,GL_FALSE,sizeof(SVtx),&pOb->Vb[0].nMatrixIndices);

		/* Define Generic Vertex Attribs to supply BiNormal and Tangent */
		g_glesExt.glVertexAttribPointerARB(3,3,VERTTYPEENUM,GL_FALSE,sizeof(SVtx),&pOb->Vb[0].bx);
		g_glesExt.glVertexAttribPointerARB(4,3,VERTTYPEENUM,GL_FALSE,sizeof(SVtx),&pOb->Vb[0].tx);

		/* Enable those Generic Vertex Attribs */
		g_glesExt.glEnableVertexAttribArrayARB(1);
		g_glesExt.glEnableVertexAttribArrayARB(2);

		g_glesExt.glEnableVertexAttribArrayARB(3);
		g_glesExt.glEnableVertexAttribArrayARB(4);

        /* Render Geometry */
		glDrawElements(GL_TRIANGLES, pOb->nCount, GL_UNSIGNED_SHORT, &pOb->Ib[pOb->nFirstIndex]);

		/* Check for Problems */
		if(glGetError())
			PVRShellOutputDebug("glGetError() reported and error in loop iteration: %i\n",i);
	}

	/* Disable Vertex Program */
	glDisable(GL_VERTEX_PROGRAM_ARB);

	/* Clean-up */

	/* Disable the Various Vertex Attribs */

	g_glesExt.glDisableVertexAttribArrayARB(1);
	g_glesExt.glDisableVertexAttribArrayARB(2);

	g_glesExt.glDisableVertexAttribArrayARB(3);
	g_glesExt.glDisableVertexAttribArrayARB(4);

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);

	glClientActiveTexture(GL_TEXTURE0);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);

	glClientActiveTexture(GL_TEXTURE1);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (PVRShellGet(prefIsRotated) && PVRShellGet(prefFullScreen))
		myglRotate(f2vt(90), f2vt(0), f2vt(0), f2vt(1));

	myglMultMatrix(g_mProj.f);	// Set Camera Based Data */

	/* Decide on the rotation used for the wall and lights */

	PVRTMatrixRotationY(mSetRot, gfWallPos);

	/* Render Wall */

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, guiTextureWall);

	myglTexParameter	(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	myglTexParameter	(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	myglTexEnv(GL_TEXTURE_ENV, GL_COMBINE_RGB, GL_REPLACE);
	myglTexEnv(GL_TEXTURE_ENV, GL_SOURCE0_RGB, GL_TEXTURE);
	myglTexEnv(GL_TEXTURE_ENV, GL_SOURCE1_RGB, GL_PREVIOUS);

	glActiveTexture(GL_TEXTURE1);
	glDisable(GL_TEXTURE_2D);

	myglTexEnv(GL_TEXTURE_ENV, GL_COMBINE_RGB, GL_REPLACE);
	myglTexEnv(GL_TEXTURE_ENV, GL_SOURCE0_RGB, GL_PREVIOUS);
	myglTexEnv(GL_TEXTURE_ENV, GL_SOURCE1_RGB, GL_PREVIOUS);

	pOb = &g_pOb[4];			// Wall "Mesh" pointer (e.g. Index Buffer and Vertex Buffer)
	pNode = &g_sScene.pNode[3];	// Wall "Node" pointer (e.g. position of object in World Space)

	g_sScene.GetWorldMatrix(mWorldWall, *pNode);
	PVRTMatrixMultiply(mTrans, mSetRot, mWorldWall);
	PVRTMatrixMultiply(mTrans, mTrans, g_mView);

	glMatrixMode(GL_MODELVIEW);
	myglLoadMatrix(mTrans.f);

	/*	Render object with Position and UV only	*/
	glEnableClientState(GL_VERTEX_ARRAY);

	glClientActiveTexture(GL_TEXTURE0);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	glVertexPointer(3, VERTTYPEENUM, sizeof(SVtx), &pOb->Vb[0].x);
	glTexCoordPointer(2,VERTTYPEENUM, sizeof(SVtx),&pOb->Vb[0].u);

	glDrawElements(GL_TRIANGLES, pOb->nCount, GL_UNSIGNED_SHORT, &pOb->Ib[pOb->nFirstIndex]);

	/* Render Background */

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, guiTextureSkyLine);

	myglTexParameter	(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	myglTexParameter	(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	myglTexEnv(GL_TEXTURE_ENV, GL_COMBINE_RGB, GL_REPLACE);
	myglTexEnv(GL_TEXTURE_ENV, GL_SOURCE0_RGB, GL_TEXTURE);

	pOb = &g_pOb[5];			// Background "Mesh" Pointer (e.g. Vertex and Index Buffers
	pNode = &g_sScene.pNode[4];	// Background "Node" pointer (e.g. Position of object in World Space)

	g_sScene.GetWorldMatrix(mWorld, *pNode);
	PVRTMatrixMultiply(mTrans, mWorld, g_mView);
	glMatrixMode(GL_MODELVIEW);
	myglLoadMatrix(mTrans.f);

	/* Background has texture animation - simple sideways translation */
	glMatrixMode(GL_TEXTURE);
	glLoadIdentity();
	myglTranslate(gfBgPos, f2vt(0.0f), f2vt(0.0f));

	/*	Render object	*/
	glEnableClientState(GL_VERTEX_ARRAY);

	glClientActiveTexture(GL_TEXTURE0);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	glVertexPointer(3, VERTTYPEENUM, sizeof(SVtx), &pOb->Vb[0].x);
	glTexCoordPointer(2,VERTTYPEENUM, sizeof(SVtx),&pOb->Vb[0].u);

	glDrawElements(GL_TRIANGLES, pOb->nCount, GL_UNSIGNED_SHORT, &pOb->Ib[pOb->nFirstIndex]);

	/* Reset Texture Matrix Translation */

	glMatrixMode(GL_TEXTURE);
	glLoadIdentity();

	/* Render Lights */

	/* Set texture and texture environment */
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, guiTextureLamp);
	myglTexEnv(GL_TEXTURE_ENV, GL_COMBINE_RGB, GL_REPLACE);
	myglTexEnv(GL_TEXTURE_ENV, GL_SOURCE0_RGB, GL_TEXTURE);

	pOb = &g_pOb[6];
	pNode = &g_sScene.pNode[5];

	g_sScene.GetWorldMatrix(mWorld, *pNode);
	PVRTMatrixInverse(mTrans, mWorldWall);
	PVRTMatrixMultiply(mTrans, mWorld, mTrans);
	PVRTMatrixMultiply(mTrans, mTrans, mSetRot);
	PVRTMatrixMultiply(mTrans, mTrans, mWorldWall);
	PVRTMatrixMultiply(mTrans, mTrans, g_mView);

	glMatrixMode(GL_MODELVIEW);
	myglLoadMatrix(mTrans.f);

	/*	Render object	*/
	glEnableClientState(GL_VERTEX_ARRAY);

	glClientActiveTexture(GL_TEXTURE0);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	glVertexPointer(3, VERTTYPEENUM, sizeof(SVtx), &pOb->Vb[0].x);
	glTexCoordPointer(2,VERTTYPEENUM, sizeof(SVtx),&pOb->Vb[0].u);

	glDrawElements(GL_TRIANGLES, pOb->nCount, GL_UNSIGNED_SHORT, &pOb->Ib[pOb->nFirstIndex]);

	/* Print text on screen */

	if (glGetError())
		PVRShellOutputDebug("**GL_ERROR** Detected Prior to Print3D calls\n");

	if (g_bEnableDOT3)
	{
        gsMyPrint3D.DisplayDefaultTitle("Chameleon Man", "VGP Skinning with DOT3 Per Pixel Lighting", PVR_LOGO);
	}
	else
	{
		gsMyPrint3D.DisplayDefaultTitle("Chameleon Man", "VGP Skinning with Vertex Lighting", PVR_LOGO);
	}

	gsMyPrint3D.Flush();

	/* Increment the gnFrameCounter to make sure our animation works */
	gfLightPos			+= f2vt(0.073339703f);
	gfWallPos			+= f2vt(0.005f);
	gfBgPos				+= f2vt(-0.0005f);
	gfAnimationFrame	+= ANIMATION_FACTOR;

	/* Wrap the Animation of the Character back to the Start */
	/* Frame 16 is equal to Frame 0 in the animation data*/
	if(gfLightPos >= PVRTTWOPI)
		gfLightPos -= PVRTTWOPI;
	if(gfWallPos >= PVRTTWOPI)
		gfWallPos -= PVRTTWOPI;
	if(gfBgPos <= 0)
		gfBgPos += f2vt(1.0f);
	if(gfAnimationFrame >= f2vt(16.0f))
		gfAnimationFrame -= f2vt(16.0f);

	return true;
}

/*******************************************************************************
 * Function Name  : InitIMG_vertex_program
 * Returns        : true if no error occured
 * Description    : Function to intialise and load data for GL extension IMG_vertex_program
  *******************************************************************************/
bool OGLESChameleonMan::InitIMG_vertex_program()
{
	char *pszExtensions;
	bool err;
	VERTTYPE	fValue[4];

	/******************************
	** IMG VERTEX PROGRAM        **
	*******************************/

	// Check Extension is Available

	pszExtensions = (char*)glGetString(GL_EXTENSIONS);

	if(!strstr(pszExtensions, "GL_IMG_vertex_program"))
	{
		/* Extension not available so quit the app and write a debug message */
		PVRShellSet(prefExitMessage, "**ERROR** GL_IMG_vertex_program is NOT supported by this platform\n\n");
		return false;
	}

	// Extension is available so :
	// 1) Init the function calls using our tools function

	g_glesExt.Init();

	// 2) Load our program(s) for usage


	const char *pszRendererString;

	/* Retrieve pointer to renderer string */
	pszRendererString = (const char*) glGetString(GL_RENDERER);

	err = false; // No VGP

	/* PowerVR hardware present? */
	if (strstr(pszRendererString, "PowerVR"))
	{
		/* VGP present? */
		if (strstr(pszRendererString, "VGPLite"))
		{
			/* VGPLite present */
			err = LoadVertexPrograms(vgp_DOT3_Skinning_VGPLITE,sizeof(vgp_DOT3_Skinning_VGPLITE),vgp_VertexLit_Skinning_VGPLITE,sizeof(vgp_VertexLit_Skinning_VGPLITE));
		}
		else if (strstr(pszRendererString, "VGP"))
		{
			/* VGP present */
			err = LoadVertexPrograms(vgp_DOT3_Skinning_VGP,sizeof(vgp_DOT3_Skinning_VGP),vgp_VertexLit_Skinning_VGP,sizeof(vgp_VertexLit_Skinning_VGP));
			if (!err)
			{
				/* Try loading ARM VP Vertex Program instead */
				err = LoadVertexPrograms(vgp_DOT3_Skinning_VGPARMVP,sizeof(vgp_DOT3_Skinning_VGPARMVP),vgp_VertexLit_Skinning_VGPARMVP,sizeof(vgp_VertexLit_Skinning_VGPARMVP));
			}
		}
    }

	if (!err)
	{
		PVRShellSet(prefExitMessage, "**ERROR** FAILED TO LOAD VGP BINARY PROGRAMS !!!\n\n");
		return false;
	}

	// 3) Setup some base constants

	fValue[0] = f2vt(1.0f);
    fValue[1] = f2vt(1.0f);
	fValue[2] = f2vt(1.0f);
	fValue[3] = f2vt(1.0f);

	g_glesExt.myglProgramEnvParameter4v(GL_VERTEX_PROGRAM_ARB, 0, fValue);

	/* Check for DOT3 support */
	/* Retrieve OpenGL ES driver version */
	int		nOGLESVersionMajor=1, nOGLESVersionMinor=0;
	char	*pszVersionNumber;
	pszVersionNumber = (char *)strchr((const char *)glGetString(GL_VERSION), '.');
	if (pszVersionNumber)
	{
		nOGLESVersionMajor = pszVersionNumber[-1] - '0';
		nOGLESVersionMinor = pszVersionNumber[+1] - '0';
	}

	/* Check extensions */
	if (nOGLESVersionMajor>1 || (nOGLESVersionMajor==1 && nOGLESVersionMinor>0))
	{
		gnCombinersPresent = true;
	}
	else
	{
        gnCombinersPresent = CPVRTglesExt::IsGLExtensionSupported("GL_ARB_texture_env_combine");
	}

	gnIMGTextureFFExtPresent = CPVRTglesExt::IsGLExtensionSupported("GL_IMG_texture_env_enhanced_fixed_function");

	if(!gnCombinersPresent && !gnIMGTextureFFExtPresent )
	{
		PVRShellSet(prefExitMessage, "**ERROR** Can't run this demo without support for GL_ARB_texture_env_combine or GL_IMG_texture_env_enhanced_fixed_function\n");
		return false; // Can't run this demo
	}
	return true;
}

/*******************************************************************************
 * Function Name  : LoadObjects
 * Returns        : true if no error occured
 * Description    : Function to load the objects used by this App
  *******************************************************************************/
bool OGLESChameleonMan::LoadObjects()
{
	SVtx	*pvData;
	unsigned short	*pwData;
	int	i, j, k, l;
	SPODMesh	*pMesh;
	int total;

	/*********************************************
	** Create Objects and render info           **
	**********************************************/

	for(i = 0; i < (int)g_sScene.nNumMesh; ++i)
	{
		/*	Create: Vertex buffer	*/

		g_pOb[g_nNumberOfRenderCalls].Vb = (SVtx*)malloc(g_sScene.pMesh[i].nNumVertex * sizeof(SVtx));

		pvData = g_pOb[g_nNumberOfRenderCalls].Vb;

		pMesh = &g_sScene.pMesh[i];

#ifdef PVRTFIXEDPOINTENABLE
		_ASSERT(pMesh->sVertex.eType == EPODDataFixed16_16);
		_ASSERT(pMesh->psUVW[0].eType == EPODDataFixed16_16);
		_ASSERT(pMesh->sNormals.eType == EPODDataFixed16_16);
		_ASSERT(pMesh->sTangents.eType == EPODDataFixed16_16);
		_ASSERT(pMesh->sBinormals.eType == EPODDataFixed16_16);
#else
		_ASSERT(pMesh->sVertex.eType == EPODDataFloat);
		_ASSERT(pMesh->psUVW[0].eType == EPODDataFloat);
		_ASSERT(pMesh->sNormals.eType == EPODDataFloat);
		_ASSERT(pMesh->sTangents.eType == EPODDataFloat);
		_ASSERT(pMesh->sBinormals.eType == EPODDataFloat);
#endif

		for(j = 0; j < ((int)pMesh->nNumVertex); ++j)
		{
			/* Store Position Data */
			pvData[j].x = ((VERTTYPE*)pMesh->sVertex.pData)[3 * j + 0];
			pvData[j].y = ((VERTTYPE*)pMesh->sVertex.pData)[3 * j + 1];
			pvData[j].z = ((VERTTYPE*)pMesh->sVertex.pData)[3 * j + 2];

			if (pMesh->psUVW)
			{
				/* Store UV data if exists */
                pvData[j].u = ((VERTTYPE*)pMesh->psUVW[0].pData)[2 * j + 0];
                pvData[j].v = ((VERTTYPE*)pMesh->psUVW[0].pData)[2 * j + 1];
			}
			else
			{
				/* Store defaults if UV data is non existant */
				pvData[j].u = f2vt(0.0f);
                pvData[j].v = f2vt(0.0f);
			}

			/* Store Normal Data */
			pvData[j].nx = ((VERTTYPE*)pMesh->sNormals.pData)[3 * j + 0];
			pvData[j].ny = ((VERTTYPE*)pMesh->sNormals.pData)[3 * j + 1];
			pvData[j].nz = ((VERTTYPE*)pMesh->sNormals.pData)[3 * j + 2];

			/* Init some fields to default values which will be filled in later */
			pvData[j].nMatrixIndices[0] = 0;
			pvData[j].nMatrixIndices[1] = 0;
			pvData[j].nMatrixIndices[2] = 0;
			pvData[j].nMatrixIndices[3] = 0;

			pvData[j].nBlendWeights[0] = 1;
			pvData[j].nBlendWeights[1] = 0;
			pvData[j].nBlendWeights[2] = 0;
			pvData[j].nBlendWeights[3] = 0;

			/* Store Tangent Data */
			pvData[j].tx = ((VERTTYPE*)pMesh->sTangents.pData)[3 * j + 0];
			pvData[j].ty = ((VERTTYPE*)pMesh->sTangents.pData)[3 * j + 1];
			pvData[j].tz = ((VERTTYPE*)pMesh->sTangents.pData)[3 * j + 2];

			/* Store Binormal Data */
			pvData[j].bx = ((VERTTYPE*)pMesh->sBinormals.pData)[3 * j + 0];
			pvData[j].by = ((VERTTYPE*)pMesh->sBinormals.pData)[3 * j + 1];
			pvData[j].bz = ((VERTTYPE*)pMesh->sBinormals.pData)[3 * j + 2];

			/* Generate Matrix Indices and Blend Weights */

			if(pMesh->sBoneIdx.n != 0)
			{
				_ASSERT(pMesh->sBoneIdx.n == pMesh->sBoneWeight.n);
				_ASSERT(pMesh->sBoneIdx.eType == EPODDataInt);
#ifdef PVRTFIXEDPOINTENABLE
				_ASSERT(pMesh->sBoneWeight.eType == EPODDataFixed16_16);
#else
				_ASSERT(pMesh->sBoneWeight.eType == EPODDataFloat);
#endif

				if(!g_sScene.CreateSkinIdxWeight(
					(char*)&pvData[j].nMatrixIndices,
					(char*)&pvData[j].nBlendWeights,
					pMesh->sBoneIdx.n,
					&((int*)pMesh->sBoneIdx.pData)[pMesh->sBoneIdx.n * j],
					&((VERTTYPE*)pMesh->sBoneWeight.pData)[pMesh->sBoneIdx.n * j]))
				{
					PVRShellSet(prefExitMessage, "**ERROR** Failed to generate Bone Weights, Idx and Matrices.\n");
					return false;
				}
			}
		}

		/* Create: Index buffer	*/

		g_pOb[g_nNumberOfRenderCalls].Ib = (unsigned short*)malloc(pMesh->nNumFaces * 3 * sizeof(unsigned short));

		pwData = g_pOb[g_nNumberOfRenderCalls].Ib;

		_ASSERT(pMesh->sFaces.eType == EPODDataUnsignedShort);
		memcpy(pwData, pMesh->sFaces.pData, pMesh->nNumFaces * 3 * sizeof(unsigned short));

		// Get a pointer to the vertex data
		pvData = g_pOb[g_nNumberOfRenderCalls].Vb;

		/**************************************************/

		if (pMesh->sBoneBatches.nBatchCnt)
		{
			/* Object has more Bones than directly supported so generate extra render calls */

			/******************************
			** Bone Batching             **
			*******************************/

			// Fix the per vertex IDX to ASM mode (ASM indexes each row, HighLevel languages index per matrix)
			total = 0;

			for (k=0;k<(int)pMesh->nNumVertex;k++)
			{
				pvData[k].nMatrixIndices[0] *= 4;
				pvData[k].nMatrixIndices[1] *= 4;
				pvData[k].nMatrixIndices[2] *= 4;
				pvData[k].nMatrixIndices[3] *= 4;
			}

			for (k=0;k<pMesh->sBoneBatches.nBatchCnt; k++)
			{
				/* For each created Batch */

				g_pOb[g_nNumberOfRenderCalls+k].Vb=pvData;
				g_pOb[g_nNumberOfRenderCalls+k].Ib=g_pOb[g_nNumberOfRenderCalls].Ib;

				if (k==0)
				{
					g_pOb[g_nNumberOfRenderCalls+k].nCopy=0;
				}
				else
				{
					/* Keep track of the fact that this is not a base object */
					g_pOb[g_nNumberOfRenderCalls+k].nCopy=1;
				}

				/* Store data required to render the vertex data */

				if ((k+1)<pMesh->sBoneBatches.nBatchCnt)
				{
                    g_pOb[g_nNumberOfRenderCalls+k].nCount=(((pMesh->sBoneBatches.pnBatchOffset[k+1]))*3) - (((pMesh->sBoneBatches.pnBatchOffset[k]))*3);
				}
				else
				{
					g_pOb[g_nNumberOfRenderCalls+k].nCount=((pMesh->nNumFaces * 3)-(pMesh->sBoneBatches.pnBatchOffset[k])*3);
				}

				g_pOb[g_nNumberOfRenderCalls+k].nFirstIndex=(pMesh->sBoneBatches.pnBatchOffset[k]*3);

				g_pOb[g_nNumberOfRenderCalls+k].nNumberOfMatrices=pMesh->sBoneBatches.pnBatchBoneCnt[k];

				for (l=0; l<pMesh->sBoneBatches.pnBatchBoneCnt[k];l++)
				{
					g_pOb[g_nNumberOfRenderCalls+k].nMatrixList[l]=pMesh->sBoneBatches.pnBatches[l+total];
				}

				g_pOb[g_nNumberOfRenderCalls+k].nObjectID = i;

				total = total + pMesh->sBoneBatches.pnBatchBoneCnt[k];
			}

			g_nNumberOfRenderCalls=g_nNumberOfRenderCalls+k-1;
		}
		else
		{
			/* Non split object store default render values */
			for (k=0;k<((int)pMesh->nNumVertex);k++)
			{
				pvData[k].nMatrixIndices[0]=pvData[k].nMatrixIndices[0]*4;
				pvData[k].nMatrixIndices[1]=pvData[k].nMatrixIndices[1]*4;
				pvData[k].nMatrixIndices[2]=pvData[k].nMatrixIndices[2]*4;
				pvData[k].nMatrixIndices[3]=pvData[k].nMatrixIndices[3]*4;
			}

			g_pOb[g_nNumberOfRenderCalls].nNumberOfMatrices = 0;

			g_pOb[g_nNumberOfRenderCalls].nCount = pMesh->nNumFaces * 3;
			g_pOb[g_nNumberOfRenderCalls].nFirstIndex=0;

			g_pOb[g_nNumberOfRenderCalls].nObjectID = i;
		}

		g_nNumberOfRenderCalls++;
	}
	return true;
}

/*******************************************************************************
 * Function Name  : LoadTextures
 * Returns        : true if no error occured
 * Description    : Function to load the textures used by this App
  *******************************************************************************/
bool OGLESChameleonMan::LoadTextures()
{
	SPVRTContext	sTempContext;
	bool			bRet;

	/* Init Print3D to display text on screen */
	bRet = gsMyPrint3D.SetTextures(&sTempContext,PVRShellGet(prefWidth),PVRShellGet(prefHeight));
	if (!bRet)
	{
		PVRShellSet(prefExitMessage, "**ERROR**: Cannot initialise Print3D\n");
		return false;
	}

	/* Load Textures */
	if(!PVRTLoadTextureFromPointer((void*)FinalChameleonManHeadBody, &guiTextureHeadBody))
	{
		PVRShellSet(prefExitMessage, "**ERROR** Failed to load texture for Background.\n");
		return false;
	}
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);


	if(!PVRTLoadTextureFromPointer((void*)FinalChameleonManHeadBody, &guiTextureHeadBody))
	{
		PVRShellSet(prefExitMessage, "**ERROR** Failed to load texture for Upper Body.\n");
		return false;
	}
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if(!PVRTLoadTextureFromPointer((void*)FinalChameleonManLegs, &guiTextureLegs))
	{
		PVRShellSet(prefExitMessage, "**ERROR** Failed to load texture for Legs.\n");
		return false;
	}
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if(!PVRTLoadTextureFromPointer((void*)Tang_space_BodyMap, &guiTextureHeadNormalMap))
	{
		PVRShellSet(prefExitMessage, "**ERROR** Failed to load normalmap texture for Upper Body.\n");
		return false;
	}
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if(!PVRTLoadTextureFromPointer((void*)Tang_space_LegsMap, &guiTextureLegsNormalMap))
	{
		PVRShellSet(prefExitMessage, "**ERROR** Failed to load normalmap texture for Legs.\n");
		return false;
	}
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if(!PVRTLoadTextureFromPointer((void*)Tang_space_BeltMap, &guiTextureBeltNormalMap))
	{
		PVRShellSet(prefExitMessage, "**ERROR** Failed to load normalmap texture for Belt.\n");
		return false;
	}
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if(!PVRTLoadTextureFromPointer((void*)skyline, &guiTextureSkyLine))
	{
		PVRShellSet(prefExitMessage, "**ERROR** Failed to load texture for SkyLine.\n");
		return false;
	}
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if(!PVRTLoadTextureFromPointer((void*)Wall_diffuse_baked, &guiTextureWall))
	{
		PVRShellSet(prefExitMessage, "**ERROR** Failed to load texture for Wall.\n");
		return false;
	}
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if(!PVRTLoadTextureFromPointer((void*)lamp, &guiTextureLamp))
	{
		PVRShellSet(prefExitMessage, "**ERROR** Failed to load texture for Lamps.\n");
		return false;
	}
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if(!PVRTLoadTextureFromPointer((void*)ChameleonBelt, &guiTextureBelt))
	{
		PVRShellSet(prefExitMessage, "**ERROR** Failed to load texture for Belt.\n");
		return false;
	}
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	return true;
}

/*******************************************************************************
 * Function Name  : LoadVertexPrograms
 * Returns        : true if no error occured
 * Description    : Function to load the Vertex Programs used by this App
  *******************************************************************************/
bool OGLESChameleonMan::LoadVertexPrograms(const void *pSkinningProg,size_t nSzSkinningProg,const void *pVertLitProg,size_t nSzVertLitProg)
{
	/* Generate IDs for 2 Vertex Programs */
	g_glesExt.glGenProgramsARB(2, guiProgs);

	/* Bind and Load first program */
	g_glesExt.glBindProgramARB(GL_VERTEX_PROGRAM_ARB, guiProgs[0]);
	g_glesExt.glProgramStringARB(GL_VERTEX_PROGRAM_ARB, GL_PROGRAM_FORMAT_BINARY_IMG, nSzSkinningProg, pSkinningProg);

	/* Check for problems */
	if (glGetError()!=GL_NO_ERROR)
	{
		return false;
	}

	/* Bind and Load second program */
	g_glesExt.glBindProgramARB(GL_VERTEX_PROGRAM_ARB, guiProgs[1]);
	g_glesExt.glProgramStringARB(GL_VERTEX_PROGRAM_ARB, GL_PROGRAM_FORMAT_BINARY_IMG, nSzVertLitProg, pVertLitProg);

	/* Check for problems */
	if (glGetError()!=GL_NO_ERROR)
	{
		return false;
	}

	return true;
}

/*******************************************************************************
 * Function Name  : CameraGetMatrix
 * Returns        :
 * Description    : Function to Setup the Camera Matrices
  *******************************************************************************/
void OGLESChameleonMan::CameraGetMatrix()
{
	PVRTVECTOR3	vFrom, vTo, vUp;
	VERTTYPE	fFOV = f2vt(3.1415f / 6);

	vUp.x = f2vt(0.0f);
	vUp.y = f2vt(1.0f);
	vUp.z = f2vt(0.0f);

	/* Grab Camera Data from POD file */
	if(g_sScene.nNumCamera)
	{
		fFOV = g_sScene.GetCameraPos(vFrom, vTo, 0);
		fFOV = VERTTYPEMUL(fFOV, f2vt(0.65f));		// Convert from horizontal FOV to vertical FOV (0.75 assumes a 4:3 aspect ratio)
		vTo.y = vTo.y - f2vt(30.0f);
	}

	// Generate View
	PVRTMatrixLookAtRH(g_mView, vFrom, vTo, vUp);

	// Generate Projection
	PVRTMatrixPerspectiveFovRH(g_mProj, fFOV, CAM_ASPECT, CAM_NEAR, CAM_FAR);
}

/*******************************************************************************
 * Function Name  : NewDemo
 * Description    : Called by the Shell to initialize a new instance to the
 *					demo class.
 *******************************************************************************/
PVRShell* NewDemo()
{
	return new OGLESChameleonMan();
}
