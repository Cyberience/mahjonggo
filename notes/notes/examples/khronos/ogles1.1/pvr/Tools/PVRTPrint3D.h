/******************************************************************************

 @File         PVRTPrint3D.h

 @Title        

 @Copyright    Copyright (C) 2000 - 2006 by Imagination Technologies Limited.

 @Platform     ANSI compatible

 @Description  Code to print text through the 3D interface.

******************************************************************************/
#ifndef _PVRTPRINT3D_H_
#define _PVRTPRINT3D_H_

#include "PVRTContext.h"


/****************************************************************************
** Enums
****************************************************************************/
/*!***************************************************************************
 dwFlags for PVRTPrint3DSetWindowFlags
*****************************************************************************/
typedef enum {
	PRINT3D_ACTIVATE_WIN		=	0x01,
	PRINT3D_DEACTIVATE_WIN		=	0x02,
	PRINT3D_ACTIVATE_TITLE		=	0x04,
	PRINT3D_DEACTIVATE_TITLE	=	0x08,
	PRINT3D_FULL_OPAQUE			=	0x10,
	PRINT3D_FULL_TRANS			=	0x20,
	PRINT3D_ADJUST_SIZE_ALWAYS	=	0x40,
	PRINT3D_NO_BORDER			=	0x80
} PRINT3D_FLAGS;

/*!***************************************************************************
 Logo flags for DisplayDefaultTitle
*****************************************************************************/
typedef enum {
	NO_LOGO  = 0x00,
	PVR_LOGO = 0x02,
	IMG_LOGO = 0x04
} PRINT3D_LOGO;

struct PVRTPrint3DGlob;	// Internal implementation data

/*!***************************************************************************
 Class: CPVRTPrint3D
*****************************************************************************/
class CPVRTPrint3D
{
public:
	// constructor/destructor
	/*************************************************************************
	 @Function			CPVRTPrint3D
	 @Description		Init some values.
	*************************************************************************/
	CPVRTPrint3D( void );
	/*************************************************************************
	 @Function			~CPVRTPrint3D
	 @Description		De-allocate the memory in case the developer didn't.
	*************************************************************************/
	~CPVRTPrint3D( void );

	/****************************************************************************
	** Functions
	****************************************************************************/

	/*!***********************************************************************
	 @Function			PVRTPrint3DSetTextures
	 @Input				pContext		Context
	 @Input				dwScreenX		Screen resolution along X
	 @Input				dwScreenY		Screen resolution along Y
	 @Return			true or false
	 @Description		Initialization and texture upload. Should be called only once
						for a given context.
	*************************************************************************/
	bool SetTextures(
		const SPVRTContext	* const pContext,
		const unsigned int	dwScreenX,
		const unsigned int	dwScreenY);

	/*!***********************************************************************
	 @Function			PVRTPrint3DReleaseTextures
	 @Description		Deallocate the memory allocated in PVRTPrint3DSetTextures(...)
	*************************************************************************/
	void ReleaseTextures();

	/*!***********************************************************************
	 @Function			PVRTPrint3D
	 @Input				fPosX		Position of the text along X
	 @Input				fPosY		Position of the text along Y
	 @Input				fScale		Scale of the text
	 @Input				Colour		Colour of the text
	 @Input				Format		Format string for the text
	 @Description		Display 3D text on screen.
						No window needs to be allocated to use this function.
						However, PVRTPrint3DSetTextures(...) must have been called
						beforehand.
						This function accepts formatting in the printf way.
	*************************************************************************/
	void Print3D(float fPosX, float fPosY, float fScale, int Colour, const char *Format, ...);

	/*!***********************************************************************
	 @Function			DisplayDefaultTitle
	 @Input				sTitle				Title to display
	 @Input				sDescription		Description to display
	 @Input				uDisplayLogo		1 = Display the logo
	 @Description		Creates a default title with predefined position and colours.
						It displays as well company logos when requested:
						0 = No logo
						1 = PowerVR logo
						2 = Img Tech logo
	*************************************************************************/
	 void DisplayDefaultTitle(char *sTitle, char *sDescription, unsigned int uDisplayLogo);

	/*!***********************************************************************
	 @Function			CreateDefaultWindow
	 @Input				fPosX					Position X for the new window
	 @Input				fPosY					Position Y for the new window
	 @Input				nXSize_LettersPerLine
	 @Input				sTitle					Title of the window
	 @Input				sBody					Body text of the window
	 @Return			Window handle
	 @Description		Creates a default window.
						If Title is NULL the main body will have just one line
						(for InfoWin).
	*************************************************************************/
	unsigned int CreateDefaultWindow(float fPosX, float fPosY, int nXSize_LettersPerLine, char *sTitle, char *sBody);

	/*!***********************************************************************
	 @Function			InitWindow
	 @Input				dwBufferSizeX		Buffer width
	 @Input				dwBufferSizeY		Buffer height
	 @Return			Window handle
	 @Description		Allocate a buffer for a newly-created window and return its
						handle.
	*************************************************************************/
	unsigned int InitWindow(unsigned int dwBufferSizeX, unsigned int dwBufferSizeY);

	/*!***********************************************************************
	 @Function			DeleteWindow
	 @Input				dwWin		Window handle
	 @Description		Delete the window referenced by dwWin.
	*************************************************************************/
	void DeleteWindow(unsigned int dwWin);

	/*!***********************************************************************
	 @Function			DeleteAllWindows
	 @Description		Delete all windows.
	*************************************************************************/
	void DeleteAllWindows();

	/*!***********************************************************************
	 @Function			DisplayWindow
	 @Input				dwWin
	 @Description		Display window.
						This function MUST be called between a BeginScene/EndScene
						pair as it uses D3D render primitive calls.
						PVRTPrint3DSetTextures(...) must have been called beforehand.
	*************************************************************************/
	void DisplayWindow(unsigned int dwWin);

	/*!***********************************************************************
	 @Function			SetText
	 @Input				dwWin		Window handle
	 @Input				Format		Format string
	 @Description		Feed the text buffer of window referenced by dwWin.
						This function accepts formatting in the printf way.
	*************************************************************************/
	void SetText(unsigned int dwWin, const char *Format, ...);

	/*!***********************************************************************
	 @Function			PVRTPrint3DSetWindow
	 @Input				dwWin			Window handle
	 @Input				dwWinColor		Window colour
	 @Input				dwFontColor		Font colour
	 @Input				fFontSize		Font size
	 @Input				fPosX			Window position X
	 @Input				fPosY			Window position Y
	 @Input				fSizeX			Window size X
	 @Input				fSizeY			Window size Y
	 @Description		Set attributes of window.
						Windows position and size are referred to a virtual screen
						of 100x100. (0,0) is the top-left corner and (100,100) the
						bottom-right corner.
						These values are the same for all resolutions.
	*************************************************************************/
	void SetWindow(unsigned int dwWin, unsigned int dwWinColor, unsigned int dwFontColor, float fFontSize,
							  float fPosX, float fPosY, float fSizeX, float fSizeY);

	/*!***********************************************************************
	 @Function			SetTitle
	 @Input				dwWin				Window handle
	 @Input				dwBackgroundColor	Background color
	 @Input				fFontSize			Font size
	 @Input				dwFontColorLeft
	 @Input				sTitleLeft
	 @Input				dwFontColorRight
	 @Input				sTitleRight
	 @Description		Set window title.
	*************************************************************************/
	void SetTitle(unsigned int dwWin, unsigned int dwBackgroundColor, float fFontSize,
							 unsigned int dwFontColorLeft, char *sTitleLeft,
							 unsigned int dwFontColorRight, char *sTitleRight);

	/*!***********************************************************************
	 @Function			PVRTPrint3DSetWindowFlags
	 @Input				dwWin				Window handle
	 @Input				dwFlags				Flags
	 @Description		Set flags for window referenced by dwWin.
						A list of flag can be found at the top of this header.
	*************************************************************************/
	void SetWindowFlags(unsigned int dwWin, unsigned int dwFlags);

	/*!***********************************************************************
	 @Function			AdjustWindowSize
	 @Input				dwWin				Window handle
	 @Input				dwMode				dwMode 0 = Both, dwMode 1 = X only,  dwMode 2 = Y only
	 @Description		Calculates window size so that all text fits in the window.
	*************************************************************************/
	void AdjustWindowSize(unsigned int dwWin, unsigned int dwMode);

	/*!***********************************************************************
	 @Function			GetSize
	 @Output			pfWidth				Width of the string in pixels
	 @Output			pfHeight			Height of the string in pixels
	 @Input				fFontSize			Font size
	 @Input				sString				String to take the size of
	 @Description		Returns the size of a string in pixels.
	*************************************************************************/
	void GetSize(
		float		* const pfWidth,
		float		* const pfHeight,
		const float	fFontSize,
		const char	* sString);

	/*!***********************************************************************
	 @Function			GetAspectRatio
	 @Output			dwScreenX		Screen resolution X
	 @Output			dwScreenY		Screen resolution Y
	 @Description		Returns the current resolution used by Print3D
	*************************************************************************/
	void GetAspectRatio(unsigned int *dwScreenX, unsigned int *dwScreenY);

	/*!***********************************************************************
	 @Function			Flush
	 @Description		Flushes all the print text commands
	*************************************************************************/
	int Flush();

private:
	PVRTPrint3DGlob	*m_pReserved;	/*!< Internal implementation data */
};


#endif /* _PVRTPRINT3D_H_ */

/*****************************************************************************
 End of file (PVRTPrint3D.h)
*****************************************************************************/
