/******************************************************************************

 @File         PVRTMiscBackground.h

 @Title        

 @Copyright    Copyright (C) 1999 - 2006 by Imagination Technologies Limited.

 @Platform     ANSI compatible

 @Description  Function to draw a background texture.

******************************************************************************/
#ifndef _PVRTMISCBACKGROUND_H_
#define _PVRTMISCBACKGROUND_H_

/****************************************************************************
** Functions
****************************************************************************/

/*!***************************************************************************
 @Function		PVRTMiscDrawBackground
 @Input			uiTexture	Texture to use
 @Input			bRotate		true to rotate texture 90 degrees.
 @Description	Draws a texture on a quad recovering the whole screen.
*****************************************************************************/
void PVRTMiscDrawBackground(GLuint uiTexture, bool bRotate);


#endif /* _PVRTMISC_H_ */

/*****************************************************************************
 End of file (PVRTMiscBackground.h)
*****************************************************************************/
