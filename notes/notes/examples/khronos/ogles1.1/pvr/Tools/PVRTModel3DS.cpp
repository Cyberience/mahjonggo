/******************************************************************************

 @File         PVRTModel3DS.cpp

 @Title        

 @Copyright    Copyright (C) 2000 - 2006 by Imagination Technologies Limited.

 @Platform     ANSI compatible

 @Description  Functions to load 3DS files.

******************************************************************************/
#include <stdio.h>
#include <math.h>
#include <malloc.h>

#include "PVRTContext.h"
#include "PVRTMatrix.h"
#include "PVRTModel3DS.h"



/***********
** Macros **
***********/
#define PI		3.14159f


/************
** Defines **
************/
#define DIAGNOSTICS				false
#define MAX_MESHES				4096
//#define MAX_MESHES				300
#define MAX_NODES				300

#define M3DMAGIC				0x4D4D
#define M3D_VERSION				0x0002
#define MDATA					0x3D3D
#define MASTER_SCALE			0x0100
#define KFDATA					0xB000
#define NAMED_OBJECT			0x4000
#define N_TRI_OBJECT			0x4100
#define	N_DIRECT_LIGHT			0x4600
#define N_CAMERA				0x4700
#define POINT_ARRAY				0x4110
#define FACE_ARRAY				0x4120
#define TEX_VERTS				0x4140
#define MESH_MATRIX				0x4160

#define MSH_MAT_GROUP			0x4130
#define MAT_ENTRY				0xAFFF
#define MAT_NAME				0xA000
#define MAT_AMBIENT				0xA010
#define MAT_DIFFUSE				0xA020
#define MAT_SPECULAR			0xA030
#define MAT_SHININESS			0xA040
#define MAT_TRANSPARENCY		0xA050
#define MAT_SHADING				0xA100
#define MAT_TEXMAP				0xA200
#define MAT_OPACMAP				0xA210

#define KFHDR					0xB00A
#define KFSEG					0xB008
#define KFCURTIME				0xB009
#define OBJECT_NODE_TAG			0xB002
#define CAMERA_NODE_TAG			0xB003
#define TARGET_NODE_TAG			0xB004
#define LIGHT_NODE_TAG			0xB005
#define SPOTLIGHT_NODE_TAG		0xB007
#define L_TARGET_NODE_TAG		0xB006
#define NODE_HDR				0xB010
#define PIVOT					0xB013
#define INSTANCE_NAME			0xB011
#define MORPH_SMOOTH			0xB015
#define BOUNDBOX				0xB014
#define POS_TRACK_TAG			0xB020
#define COL_TRACK_TAG			0xB025
#define ROT_TRACK_TAG			0xB021
#define SCL_TRACK_TAG			0xB022
#define MORPH_TRACK_TAG			0xB026
#define FOV_TRACK_TAG			0xB023
#define ROLL_TRACK_TAG			0xB024
#define HOT_TRACK_TAG			0xB027
#define FALL_TRACK_TAG			0xB028
#define NODE_ID					0xB030

#define INT_PERCENTAGE			0x0030
#define FLOAT_PERCENTAGE		0x0031
#define COLOR_24				0x0011
#define COLOR_F					0x0010


/*******************************
** Local function definitions **
*******************************/
static PVRTMATRIX GetHierarchyMatrix(ObjectType *pObject, short Node, float fFrameNumber, PVRTMATRIX CurrentMatrix);
static void GetAbsoluteRotation(PVRTMATRIX * const pmRot, float fFrameNumber, NodeType *pNode);
static void Normal(float *pV1, float *pV2, float *pV3, float NormalVect[3]);
static void CalculateNormals(int nNumVertex, float *pVertex, int nNumFaces, unsigned short *pFaces, float *pNormals);
//static VECTOR GetInterpolatedPosition(int nFrameNumber, NodeType *pNode, int Key1, int Key2);



/**************
** Functions **
**************/


/* For some weird reason global optimations have to be turned OFF for that function */
#pragma optimize( "g", off )
/*!***************************************************************************
 @Function			PVRTModel3DSRead
 @Output			pObject				Object containing the loaded 3DS data
 @Input				Name3DSFile			Name of the 3DS file to load
 @Return			true or false
 @Description		Read a 3DS file and create an object structure from it.
					After use, the object structure must be destroyed with a
					call to PVRTModel3DSDestroy().
					The 3DS file can either be a resource or file.
					If stored in a resource, the resource identifier must be "TDS".
*****************************************************************************/
bool PVRTModel3DSRead(ObjectType *pObject, char *Name3DSFile)
{
#define BREAD(Destination, Pointer, Size)	{ memcpy(Destination, Pointer, Size); Pointer+=Size; }
	FILE			*Fich;
	unsigned char	*p3DSFile=NULL, *pByte;
	PVRTMATRIX		g_Matrix[MAX_MESHES], InversionMatrix;
	PVRTVECTOR3		*pSource, D;
	int				MeshPos[MAX_MESHES];
	int				MatPos[MAX_MESHES];
	int				LightPos[MAX_MESHES];
	int				CameraPos[MAX_MESHES];
	char			MeshName[MAX_MESHES][20];
	char			CameraName[MAX_MESHES][20];
	bool			bLoadFromResource=false;
	int				nMeshNumber, nMatNumber, nLightNumber, nCameraNumber;
	unsigned int	FilePos, FileSize, ChunkSize;
	int				MDATAFileOffset=-1, KFDATAFileOffset=-1;
	int				i, j;
	unsigned short	Chunk=0, TempChunk=0;
	unsigned int	Size, TempSize, SecCont=0, TmpInt;
	short			TmpShort =0, nMatFaces = 0;
	float			fMasterScale=1.0f;
	float			fX, fY, fZ;
	unsigned char	TempByte[3];
	char			pszTmp[512];
	bool			bNodeTarget = 0;

	/***************************************************
	** Copy 3DS file or resource into a memory buffer **
	***************************************************/

	/* Debug message */
	sprintf(pszTmp, "PVRTModel3DSRead : Reading model %s\n", Name3DSFile);
	OutputDebugString(pszTmp);

	/* First initialise object structure to 0 */
	ZeroMemory(pObject, sizeof(ObjectType));

	{
		/* Disk case */

		/* Open 3DS file for reading in binary mode */
		if ((Fich = fopen(Name3DSFile, "rb")) == NULL)
		{
			sprintf(pszTmp, "PVRTModel3DSRead : Can't find %s\n", Name3DSFile);
			OutputDebugString(pszTmp);
			return false;
		}

		/* Read file ID and size */
		fread(&Chunk,	 2, 1, Fich);
		fread(&FileSize, 4, 1, Fich);

		/* Allocate memory to store the entire file in memory */
		p3DSFile=(unsigned char *)malloc(FileSize*sizeof(unsigned char));
		if (!p3DSFile)
		{
			OutputDebugString("PVRTModel3DSRead : Not enough memory to store 3DS model\n");
			fclose(Fich);
			return false;
		}

		/* Reset file pointer to beginning */
		fseek(Fich, 0, SEEK_SET);

		/* Copy entire file into memory buffer */
		if (fread(p3DSFile, FileSize*sizeof(unsigned char), 1, Fich)!=1)
		{
			sprintf(pszTmp, "PVRTModel3DSRead : Error when loading file %s\n");
			OutputDebugString(pszTmp);
			FREE(p3DSFile);
			fclose(Fich);
			return false;
		}

		/* Close file */
		fclose(Fich);
	}


	/***********************************************************
	** At this point the entire 3DS file is located in memory **
	***********************************************************/

	/* Set pointer to 3DS buffer */
	pByte = p3DSFile;

	/* Read file ID and size */
	BREAD(&Chunk, pByte, sizeof(unsigned short));
	BREAD(&FileSize, pByte, sizeof(unsigned int));

	/* Check that file is a valid 3DS file */
	if (Chunk != M3DMAGIC)
	{
		OutputDebugString("PVRTModel3DSRead : This file is not a valid 3D Studio File.\n");
		if (!bLoadFromResource) FREE(p3DSFile);
		return false;
	}


	/* File offset is 6 (just after 3DS file format ID chunk : M3DMAGIC) */
	FilePos=6;

	/* Looking for the three main chunks (M3D_VERSION, MDATA and KFDATA) */
    while(FilePos<FileSize && ++SecCont<200)
	{
		/* Go to next chunk */
		pByte = p3DSFile + FilePos;

		/* Read chunk and size */
		BREAD(&Chunk, pByte, sizeof(unsigned short));
		BREAD(&Size,  pByte, sizeof(unsigned int));

		/* Get offsets of MDATA and KFDATA */
		switch(Chunk)
		{
			case MDATA :		MDATAFileOffset = FilePos; break;
			case KFDATA :		KFDATAFileOffset = FilePos; break;
			case M3D_VERSION :	BREAD(&TmpInt, pByte, sizeof(unsigned int));
								if (DIAGNOSTICS)
								{
									sprintf(pszTmp, "M3D_VERSION = %X\n", TmpInt);
									OutputDebugString(pszTmp);
								}
								break;
			default :			if (DIAGNOSTICS) OutputDebugString("PVRTModel3DSRead : Unknown M3DMAGIC chunk encountered.\n");
		}

		/* Have we found what we're looking for ? */
		if (MDATAFileOffset != -1 && KFDATAFileOffset != -1) break;

		/* Compute next chunk position */
        FilePos += Size;
    }

	/* Check that MDATA chunk was found */
	if (MDATAFileOffset==-1)
	{
		OutputDebugString("PVRTModel3DSRead : ERROR! MDATA chunk not found.\n");
		if (!bLoadFromResource) FREE(p3DSFile);
		return false;
	}

	/* Did we find KFDATA ? */
	if (KFDATAFileOffset==-1)
	{
		OutputDebugString("PVRTModel3DSRead : Model does not contain animation.\n");
	}


	/***************
	****************
	** Read MDATA **
	****************
	***************/

	/* Set pointer to start of data */
	SecCont = 0;
	FilePos = MDATAFileOffset + 6;
	pByte = p3DSFile + FilePos;

	/******************************************************************
	** Initialise chunks counters (mesh, material, light and camera) **
	******************************************************************/
	nMeshNumber =	0;
	nMatNumber =	0;
	nLightNumber =	0;
	nCameraNumber =	0;

	/*  Looking for different Meshes or Material definitions */
    while (SecCont++ < MAX_MESHES)
	{
		/* Read chunk and size */
		BREAD(&Chunk, pByte, sizeof(unsigned short));
		BREAD(&Size,  pByte, sizeof(unsigned int));

		/* Is chunk a valid one? */
		switch(Chunk)
		{
		case MASTER_SCALE:		/* Read master scale (not used) */
								BREAD(&fMasterScale, pByte, sizeof(float));
								if (DIAGNOSTICS)
								{
									sprintf(pszTmp, "Master scale is %.2f\n", fMasterScale);
									OutputDebugString(pszTmp);
								}
								break;

		case NAMED_OBJECT:		/* Check whether we've reached the maximum number of meshes */
								if (nMeshNumber >= MAX_MESHES)
								{
									OutputDebugString("MAX_MESHES reached\n.");
									break;
								}

								/* Read mesh name */
								for (j=0; j<20; j++)
								{
									BREAD(&MeshName[nMeshNumber][j], pByte, sizeof(char));
									if (MeshName[nMeshNumber][j]==0) break;
								//	if (MeshName[nMeshNumber][j]<'0' || MeshName[nMeshNumber][j]>'z') MeshName[nMeshNumber][j] = '_';
								}
							//	if (MeshName[nMeshNumber][0]>='0' && MeshName[nMeshNumber][0]<='9') MeshName[nMeshNumber][0] = 'N';

								/* Read chunk */
								BREAD(&Chunk, pByte, sizeof(unsigned short));

								switch(Chunk)
								{
								case N_TRI_OBJECT:		/* Object geometry */
														MeshPos[nMeshNumber] = FilePos + 7 + j;
														nMeshNumber++;
														break;

								case N_DIRECT_LIGHT:	/* Object light */
														LightPos[nLightNumber] = FilePos + 7 + j;
														nLightNumber++;
														break;

								case N_CAMERA:			/* Object camera */
														// copy the mesh name into the especific field in the camera struct
														strcpy(CameraName[nCameraNumber], MeshName[nMeshNumber]);

														CameraPos[nCameraNumber] = FilePos + 7 + j;
														nCameraNumber++;
														break;

								default:				/* Unknown chunk */
														if (DIAGNOSTICS)
														{
															OutputDebugString("Unknown NAMED_OBJECT chunk type.\n");
														}
								}
								break; /* NAMED_OBJECT */

		case MAT_ENTRY:			/* If chunk is a material entry, then save its file position */
								MatPos[nMatNumber] = FilePos;
								nMatNumber++;
								break; /* MAT_ENTRY */

		default:				/* Unknown chunk */
								if (DIAGNOSTICS)
								{
									//OutputDebugString("Unknown MDATA chunk type.\n");
								}
		}

		/* Have we finished looking for chunks ? */
		FilePos += Size;
        if (FilePos > FileSize) break;

        /* Set file pointer to next chunk */
		pByte = p3DSFile + FilePos;
    }

	/* Write number of meshes, materials, lights and cameras used in object structure */
	pObject->nNumMeshes		= nMeshNumber;
	pObject->nNumMaterials	= nMatNumber;
	pObject->nNumLights		= nLightNumber;
	pObject->nNumCameras	= nCameraNumber;

	/*********************
	** GEOMETRIC MESHES **
	*********************/

	/* Allocate memory for each mesh structure in the object */
	pObject->pMesh = (MeshType *)calloc(pObject->nNumMeshes, sizeof(MeshType));
	if (!pObject->pMesh)
	{
		OutputDebugString("Read3DSData : Not enough memory to allocate mesh structures\n");
		if (!bLoadFromResource) FREE(p3DSFile);
		return false;
	}

	/* Reading data for each geometric Mesh */
	for (i=0; i<pObject->nNumMeshes; i++)
	{
		/* Go to next mesh file offset */
		FilePos = MeshPos[i];
		pByte = p3DSFile + FilePos;

		/* Read chunk and size */
		BREAD(&Chunk, pByte, sizeof(unsigned short));
		BREAD(&Size,  pByte, sizeof(unsigned int));
		ChunkSize = FilePos + Size;

		/* Increase file pointer by 6 (chunk+size) */
		FilePos += 6;

		/* Loop through each chunk until finished */
		while (1)
		{
			/* Read chunk and size */
			BREAD(&Chunk, pByte, sizeof(unsigned short));
			BREAD(&Size,  pByte, sizeof(unsigned int));

			/* Which type of chunk is it ? */
			switch (Chunk)
			{
			case POINT_ARRAY:	/* Read Number of vertices in mesh */
								BREAD(&pObject->pMesh[i].nNumVertex, pByte, sizeof(unsigned short));

								/* Allocate mesh memory for vertex geometry */
								pObject->pMesh[i].pVertex  = (float *)malloc(pObject->pMesh[i].nNumVertex * 3 * sizeof(float));
								pObject->pMesh[i].pNormals = (float *)malloc(pObject->pMesh[i].nNumVertex * 3 * sizeof(float));

								/* Check that memory was correctly allocated */
								if (!pObject->pMesh[i].pVertex || !pObject->pMesh[i].pNormals)
								{
									OutputDebugString("PVRTModel3DSRead : Not enough memory for object geometry\n");
									PVRTModel3DSDestroy(pObject);
									if (!bLoadFromResource) FREE(p3DSFile);
									return false;
								}

								/* Read mesh vertices */
								BREAD(pObject->pMesh[i].pVertex, pByte, pObject->pMesh[i].nNumVertex*3*sizeof(float));
								break; /* POINT_ARRAY */

            case FACE_ARRAY:	/* Read number of faces in mesh */
								BREAD(&pObject->pMesh[i].nNumFaces, pByte, sizeof(unsigned short));

								/* Allocate mesh memory for face list data */
								pObject->pMesh[i].pFaces = (unsigned short *)malloc(pObject->pMesh[i].nNumFaces * 3 * sizeof(unsigned short));

								/* Check that memory was correctly allocated */
								if (!pObject->pMesh[i].pFaces)
								{
									OutputDebugString("Not enough memory for face list\n");
									PVRTModel3DSDestroy(pObject);
									if (!bLoadFromResource) FREE(p3DSFile);
									return false;
								}

								/* Read face list information (we only take the first three parameters, as the fourth one is not needed) */
								for (j=0; j<pObject->pMesh[i].nNumFaces; j++)
								{
									/* Read indices in opposite order */
									BREAD(&pObject->pMesh[i].pFaces[j*3+2], pByte, sizeof(unsigned short));
									BREAD(&pObject->pMesh[i].pFaces[j*3+1], pByte, sizeof(unsigned short));
									BREAD(&pObject->pMesh[i].pFaces[j*3+0], pByte, sizeof(unsigned short));
									BREAD(&TmpShort, pByte, sizeof(short));
								}

								/* Read chunk and size */
								BREAD(&Chunk,     pByte, sizeof(unsigned short));
								BREAD(&TempSize,  pByte, sizeof(unsigned int));

								/* Assign material for this mesh to the first material found */
								if (Chunk==MSH_MAT_GROUP)
								{
									/* Read material name */
									for (j=0; j<16; j++)
									{
										BREAD(&pObject->pMesh[i].pszMaterial[j], pByte, sizeof(char));
										if (pObject->pMesh[i].pszMaterial[j]==0) break;
										if (pObject->pMesh[i].pszMaterial[j]<'0' || pObject->pMesh[i].pszMaterial[j]>'z')
										{
										//	pObject->pMesh[i].pszMaterial[j] = '_';
										}
									}
								}
								break; /* FACE_ARRAY */

			case TEX_VERTS:		/* Read number of vertices in mesh */
								BREAD(&pObject->pMesh[i].nNumVertex, pByte, sizeof(unsigned short));

								/* Allocate mesh memory for UVs */
								pObject->pMesh[i].pUV = (float *)malloc(pObject->pMesh[i].nNumVertex * 2 * sizeof(float));

								/* Check that memory was correctly allocated */
								if (!pObject->pMesh[i].pUV)
								{
									OutputDebugString("Not enough memory for face list\n");
									PVRTModel3DSDestroy(pObject);
									if (!bLoadFromResource) FREE(p3DSFile);
									return false;
								}

								/* Read UV coordinates for mesh */
								BREAD(pObject->pMesh[i].pUV, pByte, pObject->pMesh[i].nNumVertex * 2 * sizeof(float));
								break; /* TEX_VERTS */

			case MESH_MATRIX:	/* Read matrix information for object (not exported) */
								PVRTMatrixIdentity(g_Matrix[i]);
#if 0
								BREAD(&g_Matrix[i]._11, pByte, sizeof(float));
								BREAD(&g_Matrix[i]._12, pByte, sizeof(float));
								BREAD(&g_Matrix[i]._13, pByte, sizeof(float));
								BREAD(&g_Matrix[i]._21, pByte, sizeof(float));
								BREAD(&g_Matrix[i]._22, pByte, sizeof(float));
								BREAD(&g_Matrix[i]._23, pByte, sizeof(float));
								BREAD(&g_Matrix[i]._31, pByte, sizeof(float));
								BREAD(&g_Matrix[i]._32, pByte, sizeof(float));
								BREAD(&g_Matrix[i]._33, pByte, sizeof(float));
								BREAD(&g_Matrix[i]._41, pByte, sizeof(float));
								BREAD(&g_Matrix[i]._42, pByte, sizeof(float));
								BREAD(&g_Matrix[i]._43, pByte, sizeof(float));
#else
								BREAD(&g_Matrix[i].f[ 0], pByte, sizeof(float));
								BREAD(&g_Matrix[i].f[ 4], pByte, sizeof(float));
								BREAD(&g_Matrix[i].f[ 8], pByte, sizeof(float));
								BREAD(&g_Matrix[i].f[ 1], pByte, sizeof(float));
								BREAD(&g_Matrix[i].f[ 5], pByte, sizeof(float));
								BREAD(&g_Matrix[i].f[ 9], pByte, sizeof(float));
								BREAD(&g_Matrix[i].f[ 2], pByte, sizeof(float));
								BREAD(&g_Matrix[i].f[ 6], pByte, sizeof(float));
								BREAD(&g_Matrix[i].f[10], pByte, sizeof(float));
								BREAD(&g_Matrix[i].f[ 3], pByte, sizeof(float));
								BREAD(&g_Matrix[i].f[ 7], pByte, sizeof(float));
								BREAD(&g_Matrix[i].f[11], pByte, sizeof(float));
#endif
								break; /* MESH_MATRIX */

			default:			if (DIAGNOSTICS)
								{
									OutputDebugString("Unknown N_TRI_OBJECT mesh chunk.\n");
								}
            }

			/* Have we finished looking for chunks in N_TRI_OBJECT ? */
			FilePos += Size;
			if (FilePos > ChunkSize) break;

			/* Set file to next chunk */
			pByte = p3DSFile + FilePos;
		}
	}

	/**************
	** MATERIALS **
	**************/

	/* Allocate memory for material structures in object */
	pObject->pMaterial = (MaterialType *)calloc(pObject->nNumMaterials, sizeof(MaterialType));

	/* Check that memory was correctly allocated */
	if (!pObject->pMaterial)
	{
		OutputDebugString("Not enough memory for face list\n");
		PVRTModel3DSDestroy(pObject);
		if (!bLoadFromResource) FREE(p3DSFile);
		return false;
	}

	/* Read materials */
	for (i=0; i<pObject->nNumMaterials; i++)
	{
		/* Go to next material file offset */
		FilePos = MatPos[i];
		pByte = p3DSFile + FilePos;

		/* Read chunk and size */
		BREAD(&Chunk, pByte, sizeof(unsigned short));
		BREAD(&Size, pByte, sizeof(unsigned int));
		ChunkSize = FilePos + Size;

		/* Increase file pointer by 6 (chunk+size) */
		FilePos += 6;

		while (1)
		{
			/* Read chunk and size */
			BREAD(&Chunk, pByte, sizeof(unsigned short));
			BREAD(&Size, pByte, sizeof(unsigned int));

            switch (Chunk)
			{
			case MAT_NAME:		/* Read material name (16 characters plus NULL) */
								for (j=0; j<16; j++)
								{
									BREAD(&pObject->pMaterial[i].pszMatName[j], pByte, sizeof(char));
									if (pObject->pMaterial[i].pszMatName[j]==0) break;
								//	if (pObject->pMaterial[i].pszMatName[j]<'0' || pObject->pMaterial[i].pszMatName[j]>'z')
								//		pObject->pMaterial[i].pszMatName[j] = '_';
								}
								break;

            case MAT_TEXMAP:	/* Read texture map strength (percentage) */
								BREAD(&Chunk, pByte, sizeof(unsigned short));
								if (Chunk==INT_PERCENTAGE) pByte+=12;
									else pByte+=20;

								/* Read texture map name */
								for (j=0; j<20; j++)
								{
									BREAD(&pObject->pMaterial[i].pszMatFile[j], pByte, 1);
									if(pObject->pMaterial[i].pszMatFile[j]==0) break;
								}
								break;

			case MAT_OPACMAP:	/* Read opacity map strength (percentage) */
								pObject->pMaterial[i].nMatOpacity = true;
								BREAD(&Chunk, pByte, sizeof(unsigned short));
								if (Chunk==INT_PERCENTAGE) pByte+=12;
									else pByte+=20;

								/* Read opacity map name */
								for (j=0; j<20; j++)
								{
									BREAD(&pObject->pMaterial[i].pszMatOpaq[j], pByte, sizeof(char));
									if (pObject->pMaterial[i].pszMatOpaq[j]==0) break;
								}
								break;

            case MAT_AMBIENT:	/* Read ambient material color */
								BREAD(&Chunk, pByte, sizeof(unsigned short));
								BREAD(&TempSize, pByte, sizeof(unsigned int));
								if (Chunk == COLOR_24)
								{
									BREAD(&TempByte[0], pByte, 3*sizeof(char));
									pObject->pMaterial[i].fMatAmbient[0] = (float)(TempByte[0])/255.0f;
									pObject->pMaterial[i].fMatAmbient[1] = (float)(TempByte[1])/255.0f;
									pObject->pMaterial[i].fMatAmbient[2] = (float)(TempByte[2])/255.0f;
								}
								else if (Chunk == COLOR_F)
								{
									BREAD(&pObject->pMaterial[i].fMatAmbient[0], pByte, 3*sizeof(float));
								}
								break;

            case MAT_DIFFUSE:	/* Read diffuse material color */
								BREAD(&Chunk, pByte, sizeof(unsigned short));
								BREAD(&TempSize, pByte, sizeof(unsigned int));
								if (Chunk == COLOR_24)
								{
									BREAD(&TempByte[0], pByte, 3*sizeof(char));
									pObject->pMaterial[i].fMatDiffuse[0] = (float)(TempByte[0]) * (1.0f/255.0f);
									pObject->pMaterial[i].fMatDiffuse[1] = (float)(TempByte[1]) * (1.0f/255.0f);
									pObject->pMaterial[i].fMatDiffuse[2] = (float)(TempByte[2]) * (1.0f/255.0f);
								}
								else if (Chunk == COLOR_F)
								{
									BREAD(&pObject->pMaterial[i].fMatDiffuse[0], pByte, 3*sizeof(float));
								}
								break;

            case MAT_SPECULAR:	/* Read specular material color */
								BREAD(&Chunk, pByte, sizeof(unsigned short));
								BREAD(&TempSize, pByte, sizeof(unsigned int));
								if (Chunk == COLOR_24)
								{
									BREAD(&TempByte[0], pByte, 3*sizeof(char));
									pObject->pMaterial[i].fMatSpecular[0] = (float)(TempByte[0]) * (1.0f/255.0f);
									pObject->pMaterial[i].fMatSpecular[1] = (float)(TempByte[1]) * (1.0f/255.0f);
									pObject->pMaterial[i].fMatSpecular[2] = (float)(TempByte[2]) * (1.0f/255.0f);
								}
								else if (Chunk == COLOR_F)
								{
									BREAD(&pObject->pMaterial[i].fMatSpecular[0], pByte, 3*sizeof(float));
								}
								break;

            case MAT_SHININESS:	/* Read shininess ratio (percentage) */
								BREAD(&Chunk, pByte, sizeof(unsigned short));
								BREAD(&TempSize, pByte, sizeof(unsigned int));
								if (Chunk == INT_PERCENTAGE)
								{
									BREAD(&TmpShort, pByte, sizeof(unsigned short));
									pObject->pMaterial[i].fMatShininess = (float)TmpShort/100.0f;
								}
								else if (Chunk == FLOAT_PERCENTAGE)
								{
									BREAD(&pObject->pMaterial[i].fMatShininess, pByte, sizeof(float));
								}
								break;

            case MAT_TRANSPARENCY:	/* Read material transparency */
									BREAD(&Chunk, pByte, sizeof(unsigned short));
									BREAD(&TempSize, pByte, sizeof(unsigned int));
									if (Chunk == INT_PERCENTAGE)
									{
										BREAD(&TmpShort, pByte, sizeof(unsigned short));
										pObject->pMaterial[i].fMatTransparency = (float)TmpShort/100.0f;
									}
									else if (Chunk == FLOAT_PERCENTAGE)
									{
										BREAD(&pObject->pMaterial[i].fMatTransparency, pByte, sizeof(float));
									}
									break;

            case MAT_SHADING:		/* Material shading method */
									BREAD(&pObject->pMaterial[i].sMatShading, pByte, sizeof(short));
									break;

			default:				if (DIAGNOSTICS)
									{
										OutputDebugString("Unknown MAT_ENTRY chunk.\n");
									}
			}

			/* Have we finished looking for chunks in MAT_ENTRY ? */
			FilePos += Size;
			if (FilePos > ChunkSize) break;
			pByte = p3DSFile + FilePos;
		}
	}

	/***********
	** LIGHTS **
	***********/
	if (pObject->nNumLights)
	{
		/* Allocate memory for all lights defined in object */
		pObject->pLight=(LightType *)calloc(pObject->nNumLights, sizeof(LightType));
		if (!pObject->pLight)
		{
			OutputDebugString("PVRTModel3DSRead : Not enough memory to allocate light structures\n");
			PVRTModel3DSDestroy(pObject);
			if (!bLoadFromResource) FREE(p3DSFile);
			return false;
		}

		/* Read data for each light */
		for (i=0; i<pObject->nNumLights; i++)
		{
			/* Go to next light file offset */
			FilePos = LightPos[i];
			pByte = p3DSFile + FilePos;

			/* Read chunk and size */
			BREAD(&Chunk, pByte, sizeof(unsigned short));
			BREAD(&Size, pByte, sizeof(unsigned int));

			/* Read light position (inverting y and z) */
			BREAD(&pObject->pLight[i].fPosition[0], pByte, sizeof(float));
			BREAD(&pObject->pLight[i].fPosition[2], pByte, sizeof(float));
			BREAD(&pObject->pLight[i].fPosition[1], pByte, sizeof(float));

			/* Read light color */
			BREAD(&Chunk, pByte, sizeof(unsigned short));
			BREAD(&TempSize, pByte, sizeof(unsigned int));
			if (Chunk == COLOR_24)
			{
				BREAD(&TempByte[0], pByte, 3*sizeof(char));
				pObject->pLight[i].fColour[0] = (float)(TempByte[0]) * (1.0f/255.0f);
				pObject->pLight[i].fColour[1] = (float)(TempByte[1]) * (1.0f/255.0f);
				pObject->pLight[i].fColour[2] = (float)(TempByte[2]) * (1.0f/255.0f);
			}
			else if (Chunk == COLOR_F)
			{
				BREAD(&pObject->pLight[i].fColour[0], pByte, 3*sizeof(float));
			}
		}
	}

	/************
	** CAMERAS **
	************/
	if (pObject->nNumCameras)
	{
		/* Allocate memory for all cameras defined in object */
		pObject->pCamera=(CameraType *)calloc(pObject->nNumCameras, sizeof(CameraType));
		if (!pObject->pCamera)
		{
			OutputDebugString("PVRTModel3DSRead : Not enough memory to allocate camera structures\n");
			PVRTModel3DSDestroy(pObject);
			if (!bLoadFromResource) FREE(p3DSFile);
			return false;
		}

		/* Read data for each camera */
		for (i=0; i<pObject->nNumCameras; i++)
		{
			/* Go to next camera file offset */
			FilePos = CameraPos[i];
			pByte = p3DSFile + FilePos;

			/* Read chunk and size */
			BREAD(&Chunk, pByte, sizeof(unsigned short));
			BREAD(&Size, pByte, sizeof(unsigned int));

			/* Read camera position (inverting y and z) */
			BREAD(&pObject->pCamera[i].fPosition[0], pByte, sizeof(float));
			BREAD(&pObject->pCamera[i].fPosition[2], pByte, sizeof(float));
			BREAD(&pObject->pCamera[i].fPosition[1], pByte, sizeof(float));

			/* Read camera looking point (inverting y and z) */
			BREAD(&pObject->pCamera[i].fTarget[0], pByte, sizeof(float));
			BREAD(&pObject->pCamera[i].fTarget[2], pByte, sizeof(float));
			BREAD(&pObject->pCamera[i].fTarget[1], pByte, sizeof(float));

			/* Read camera roll value */
			BREAD(&pObject->pCamera[i].fRoll, pByte, sizeof(float));

			/* Read camera focal length value */
			BREAD(&pObject->pCamera[i].fFocalLength, pByte, sizeof(float));

			/* Calculate FOV from focal length (FOV  = arctan(1/(2*f)))*/
			if(pObject->pCamera[i].fFocalLength==0.0f)
			{
				pObject->pCamera[i].fFOV = 0.0f;
			}
			else
			{
				pObject->pCamera[i].fFOV = 70.0f * (float)atan( 1.0f / (2.0f*pObject->pCamera[i].fFocalLength));
			}

			/* Copy camera name */
			strcpy(pObject->pCamera[i].pszName, CameraName[i]);
		}
	}


	/*******************************
	** Feeding the Info Structure **
	*******************************/
	for (i=0; i<pObject->nNumMeshes;i++)
	{
		/* Copy mesh and material names */
		strcpy(pObject->pMesh[i].pszName, MeshName[i]);

		/* Find the material number corresponding to mesh */
		if (pObject->pMesh[i].pszMaterial)
		{
			/* Loop through all materials */
			for (j=0; j<pObject->nNumMaterials; j++)
			{
				if (strcmp(pObject->pMesh[i].pszMaterial, pObject->pMaterial[j].pszMatName)==0)
				{
					pObject->pMesh[i].nMaterialNumber=j;
				}
			}
		}
	}

	/****************
	*****************
	** Read KFDATA **
	** Animation.  **
	*****************
	****************/

	/* Only read animation if it exists */
	if (KFDATAFileOffset!=-1)
	{
		char			pszName[20];
		unsigned int	TempFilePos;
		unsigned int	TmpInt;
		short			Revision;
		short			NodeId=0;
		short			SplineTerms;
		float			fDummy;

		/* First find out how many nodes are contained in the file */

		/* Reset file position */
		SecCont = 0;
		FilePos = KFDATAFileOffset + 6;

		/* Reset number of nodes */
		pObject->nNumNodes=0;

		/* Look for chunks */
		while (FilePos<FileSize && ++SecCont<MAX_MESHES)
		{
			/* Set file pointer to next chunk */
			pByte = p3DSFile + FilePos;

			/* Read chunk and size */
			BREAD(&Chunk, pByte, sizeof(unsigned short));
			BREAD(&Size, pByte, sizeof(unsigned int));

			/* Find object chunk */
			switch (Chunk)
			{
				case OBJECT_NODE_TAG:
				case CAMERA_NODE_TAG:
				case TARGET_NODE_TAG:
				case LIGHT_NODE_TAG:
				case SPOTLIGHT_NODE_TAG:
				case L_TARGET_NODE_TAG:		pObject->nNumNodes++;
			}

			/* Have we finished looking for chunks ? */
			FilePos += Size;
		}

		/* Debug info */
		if (DIAGNOSTICS)
		{
			sprintf(pszTmp, "Number of nodes = %d\n", pObject->nNumNodes);
			OutputDebugString(pszTmp);
		}

		/* Allocate memory for nodes */
		pObject->pNode = (NodeType *)calloc(pObject->nNumNodes, sizeof(NodeType));
		if (!pObject->pNode)
		{
			PVRTModel3DSDestroy(pObject);
			if (!bLoadFromResource) FREE(p3DSFile);
			return false;
		}

		/* Initialise nodes */
		for (i=0; i<pObject->nNumNodes; i++)
		{
			pObject->pNode[i].Mesh=-1;
			pObject->pNode[i].ParentIndex=-1;
			pObject->pNode[i].Camera=-1;
			pObject->pNode[i].Target=-1;
		}


		/* Set pointer to start of data */
		SecCont = 0;

		FilePos = KFDATAFileOffset + 6;

		/*  Looking for sub-chunks */
		while(FilePos<FileSize && ++SecCont<MAX_MESHES)
		{
			/* Set file pointer to next chunk */
			pByte = p3DSFile + FilePos;

			/* Read chunk and size */
			BREAD(&Chunk, pByte, sizeof(unsigned short));
			BREAD(&Size, pByte, sizeof(unsigned int));

			switch(Chunk)
			{
			case KFHDR:	/* Keyframe header */

						/* Read revision number */
						BREAD(&Revision, pByte, sizeof(short));

						/* Read 3DS file name */
						for (j=0; j<20; j++)
						{
							BREAD(&pszName[j], pByte, sizeof(char));
							if(pszName[j] == 0) break;
							//if(pszName[j]<'0' || pszName[j]>'z') pszName[j] = '_';
							//if(pszName[0]>='0' && pszName[0]<='9') pszName[0] = 'N';
						}

						/* Read number of frames */
						BREAD(&pObject->nNumFrames, pByte, sizeof(unsigned int));

						/* Seems like frame 0 is not taken into account */
						pObject->nNumFrames++;

						/* Debug info */
						if (DIAGNOSTICS)
						{
							sprintf(pszTmp, "Animation header :\nRevision=%d\nName=%s\nNb of frames=%d\n", Revision, pszName, pObject->nNumFrames);
							OutputDebugString(pszTmp);
						}
						break;

			case KFSEG: /* Active frame segment */
						if (DIAGNOSTICS)
						{
							/* Keyframe active segment of frames to render */
							int nFirstFrame, nLastFrame;

							/* Read first and last frame */
							BREAD(&nFirstFrame, pByte, sizeof(unsigned int));
							BREAD(&nLastFrame, pByte, sizeof(unsigned int));

							/* Debug info */
							sprintf(pszTmp, "Frames to render %d -> %d\n", nFirstFrame, nLastFrame);
							OutputDebugString(pszTmp);
						}
						break;

			case KFCURTIME: /* Current frame number */
							if (DIAGNOSTICS)
							{
								/* Current active frame number */
								int nFrameNumber;

								/* Read frame number */
								BREAD(&nFrameNumber, pByte, sizeof(unsigned int));

								/* Debug info */
								sprintf(pszTmp, "Active frame number : %d\n", nFrameNumber);
								OutputDebugString(pszTmp);
							}
							break;

			case OBJECT_NODE_TAG:	/* Object node */

									/* Set file position after chunk and size */
									TempFilePos = FilePos + 6;

									/* Read all subchunks of OBJECT_NODE_TAG */
									while (TempFilePos < FilePos+Size)
									{
										/* Go to next subchunk */
										pByte = p3DSFile + TempFilePos;

										/* Read chunk and size */
										BREAD(&TempChunk, pByte, sizeof(unsigned short));
										BREAD(&TempSize, pByte, sizeof(unsigned int));

										/* Process chunk */
										switch(TempChunk)
										{
										case NODE_ID:	/* NODE_ID subchunk */
														BREAD(&NodeId, pByte, sizeof(short));
														break;

										case NODE_HDR:	/* NODE_HDR subchunk */

														/* Read mesh name */
														for (j=0; j<20; j++)
														{
															BREAD(&pszName[j], pByte, sizeof(char));
															if(pszName[j] == 0) break;
														//	if(pszName[j]<'0' || pszName[j]>'z') pszName[j] = '_';
														//	if(pszName[0]>='0' && pszName[0]<='9') pszName[0] = 'N';
														}

														/* Find mesh index corresponding to that node */
														for (i=0; i<pObject->nNumMeshes; i++)
														{
															if (strcmp(pObject->pMesh[i].pszName, pszName)==0)
															{
																pObject->pNode[NodeId].Mesh=i;
																break;
															}
														}

														/* Read flags (2 of them) (not used) */
														BREAD(&TmpShort, pByte, sizeof(short));
														BREAD(&TmpShort, pByte, sizeof(short));

														/* Parent index */
														BREAD(&pObject->pNode[NodeId].ParentIndex, pByte, sizeof(short));

														/* Debug info */
														if (DIAGNOSTICS)
														{
															sprintf(pszTmp, "------------\nNode ID = %d\n------------\nMesh name = %s\nParent index = %d\n", NodeId, pszName, pObject->pNode[NodeId].ParentIndex);
															OutputDebugString(pszTmp);
														}
														break;

										case PIVOT:		/* PIVOT subchunk */

														/* Read pivot point */
														BREAD(&pObject->pNode[NodeId].Pivot[0], pByte, sizeof(float));
														BREAD(&pObject->pNode[NodeId].Pivot[1], pByte, sizeof(float));
														BREAD(&pObject->pNode[NodeId].Pivot[2], pByte, sizeof(float));

														/* Debug info */
														if (DIAGNOSTICS)
														{
															sprintf(pszTmp, "Pivot point : %.2f %.2f %.2f\n", pObject->pNode[NodeId].Pivot[0], pObject->pNode[NodeId].Pivot[1], pObject->pNode[NodeId].Pivot[2]);
															OutputDebugString(pszTmp);
														}
														break;

										case BOUNDBOX:	/* BOUNDBOX subchunk */
														if (DIAGNOSTICS)
														{
															float MinX, MinY, MinZ;
															float MaxX, MaxY, MaxZ;

															/* Read Minimum bounding point */
															BREAD(&MinX, pByte, sizeof(float));
															BREAD(&MinY, pByte, sizeof(float));
															BREAD(&MinZ, pByte, sizeof(float));

															/* Read Maximum bounding point */
															BREAD(&MaxX, pByte, sizeof(float));
															BREAD(&MaxY, pByte, sizeof(float));
															BREAD(&MaxZ, pByte, sizeof(float));

															/* Debug info */
															sprintf(pszTmp, "Bounding Box : Min=(%.2f, %.2f, %.2f)  Max=(%.2f, %.2f, %.2f)\n", MinX, MinY, MinZ, MaxX, MaxY, MaxZ);
															OutputDebugString(pszTmp);
														}
														break;

										case POS_TRACK_TAG: /* POS_TRACK_TAG subchunk */

															/* Read internal flags and 2 unused unsigned int */
															BREAD(&TmpShort, pByte, sizeof(short));
															BREAD(&TmpInt, pByte, sizeof(unsigned int));
															BREAD(&TmpInt, pByte, sizeof(unsigned int));

															/* Read number of keys in track */
															BREAD(&pObject->pNode[NodeId].PositionKeys, pByte, sizeof(unsigned int));

															/* Debug info */
															if (DIAGNOSTICS)
															{
																sprintf(pszTmp, "Position keys in track = %d\n", pObject->pNode[NodeId].PositionKeys);
																OutputDebugString(pszTmp);
															}

															/* Allocate memory for these keys */
															if (pObject->pNode[NodeId].PositionKeys)
															{
																pObject->pNode[NodeId].pPosition = (POSITIONKEY *)calloc(pObject->pNode[NodeId].PositionKeys, sizeof(POSITIONKEY));
															}

															/* Read all keys */
															for (i=0; i<pObject->pNode[NodeId].PositionKeys; i++)
															{
																/* Read frame number */
																BREAD(&pObject->pNode[NodeId].pPosition[i].FrameNumber, pByte, sizeof(unsigned int));

																/* Read spline terms */
																BREAD(&SplineTerms, pByte, sizeof(short));
																if (SplineTerms & 0x01) BREAD(&pObject->pNode[NodeId].pPosition[i].fTension, pByte, sizeof(float));
																if (SplineTerms & 0x02) BREAD(&pObject->pNode[NodeId].pPosition[i].fContinuity, pByte, sizeof(float));
																if (SplineTerms & 0x04) BREAD(&pObject->pNode[NodeId].pPosition[i].fBias, pByte, sizeof(float));
																if (SplineTerms & 0x08) BREAD(&fDummy, pByte, sizeof(float));
																if (SplineTerms & 0x10) BREAD(&fDummy, pByte, sizeof(float));

																/* Read position */
																BREAD(&pObject->pNode[NodeId].pPosition[i].p.x, pByte, sizeof(float));
																BREAD(&pObject->pNode[NodeId].pPosition[i].p.y, pByte, sizeof(float));
																BREAD(&pObject->pNode[NodeId].pPosition[i].p.z, pByte, sizeof(float));

																/* Debug info */
																if (DIAGNOSTICS)
																{
																	sprintf(pszTmp, "Frame %d : Translation of (%.2f, %.2f, %.2f)\n", pObject->pNode[NodeId].pPosition[i].FrameNumber, pObject->pNode[NodeId].pPosition[i].p.x, pObject->pNode[NodeId].pPosition[i].p.y, pObject->pNode[NodeId].pPosition[i].p.z);
																	OutputDebugString(pszTmp);
																	sprintf(pszTmp, "Spline terms : (%.2f, %.2f, %.2f)\n", pObject->pNode[NodeId].pPosition[i].fTension, pObject->pNode[NodeId].pPosition[i].fContinuity, pObject->pNode[NodeId].pPosition[i].fBias);
																	OutputDebugString(pszTmp);
																}
															}
															break;

										case ROT_TRACK_TAG: /* ROT_TRACK_TAG subchunk */

															/* Read internal flags and 2 unused unsigned int */
															BREAD(&TmpShort, pByte, sizeof(short));
															BREAD(&TmpInt, pByte, sizeof(unsigned int));
															BREAD(&TmpInt, pByte, sizeof(unsigned int));

															/* Read number of keys in track */
															BREAD(&pObject->pNode[NodeId].RotationKeys, pByte, sizeof(unsigned int));

															/* Debug info */
															if (DIAGNOSTICS)
															{
																sprintf(pszTmp, "Rotation keys in track = %d\n", pObject->pNode[NodeId].RotationKeys);
																OutputDebugString(pszTmp);
															}

															/* Allocate memory for these keys */
															if (pObject->pNode[NodeId].RotationKeys)
															{
																pObject->pNode[NodeId].pRotation= (ROTATIONKEY *)calloc(pObject->pNode[NodeId].RotationKeys, sizeof(ROTATIONKEY));
															}

															/* Read all keys */
															for (i=0; i<pObject->pNode[NodeId].RotationKeys; i++)
															{
																/* Read frame number */
																BREAD(&pObject->pNode[NodeId].pRotation[i].FrameNumber, pByte, sizeof(int));

																/* Read spline terms */
																BREAD(&SplineTerms, pByte, sizeof(short));
																if (SplineTerms & 0x01) BREAD(&pObject->pNode[NodeId].pRotation[i].fTension, pByte, sizeof(float));
																if (SplineTerms & 0x02) BREAD(&pObject->pNode[NodeId].pRotation[i].fContinuity, pByte, sizeof(float));
																if (SplineTerms & 0x04) BREAD(&pObject->pNode[NodeId].pRotation[i].fBias, pByte, sizeof(float));
																if (SplineTerms & 0x08) BREAD(&fDummy, pByte, sizeof(float));
																if (SplineTerms & 0x10) BREAD(&fDummy, pByte, sizeof(float));

																/* Read rotation values (first one is angle) */
																BREAD(&pObject->pNode[NodeId].pRotation[i].Angle, pByte, sizeof(float));
																BREAD(&pObject->pNode[NodeId].pRotation[i].r.x, pByte, sizeof(float));
																BREAD(&pObject->pNode[NodeId].pRotation[i].r.y, pByte, sizeof(float));
																BREAD(&pObject->pNode[NodeId].pRotation[i].r.z, pByte, sizeof(float));

																/* Debug info */
																if (DIAGNOSTICS)
																{
																	sprintf(pszTmp, "Frame %d : Rotation of %.2f radians around the (%.2f, %.2f, %.2f) axis\n", pObject->pNode[NodeId].pRotation[i].FrameNumber, pObject->pNode[NodeId].pRotation[i].Angle, pObject->pNode[NodeId].pRotation[i].r.x, pObject->pNode[NodeId].pRotation[i].r.y, pObject->pNode[NodeId].pRotation[i].r.z);
																	OutputDebugString(pszTmp);
																	sprintf(pszTmp, "Spline terms : (%.2f, %.2f, %.2f)\n", pObject->pNode[NodeId].pRotation[i].fTension, pObject->pNode[NodeId].pRotation[i].fContinuity, pObject->pNode[NodeId].pRotation[i].fBias);
																	OutputDebugString(pszTmp);
																}
															}
															break;

										case SCL_TRACK_TAG: /* SCL_TRACK_TAG subchunk */

															/* Read internal flags and 2 unused unsigned int */
															BREAD(&TmpShort, pByte, sizeof(short));
															BREAD(&TmpInt, pByte, sizeof(unsigned int));
															BREAD(&TmpInt, pByte, sizeof(unsigned int));

															/* Read number of keys in track */
															BREAD(&pObject->pNode[NodeId].ScalingKeys, pByte, sizeof(unsigned int));

															/* Debug info */
															if (DIAGNOSTICS)
															{
																sprintf(pszTmp, "Scaling keys in track = %d\n", pObject->pNode[NodeId].ScalingKeys);
																OutputDebugString(pszTmp);
															}

															/* Allocate memory for these keys */
															if (pObject->pNode[NodeId].ScalingKeys)
															{
																pObject->pNode[NodeId].pScaling= (SCALINGKEY *)calloc(pObject->pNode[NodeId].ScalingKeys, sizeof(SCALINGKEY));
															}

															/* Read all keys */
															for (i=0; i<pObject->pNode[NodeId].ScalingKeys; i++)
															{
																/* Read frame number */
																BREAD(&pObject->pNode[NodeId].pScaling[i].FrameNumber, pByte, sizeof(int));

																/* Read spline terms */
																BREAD(&SplineTerms, pByte, sizeof(short));
																if (SplineTerms & 0x01) BREAD(&pObject->pNode[NodeId].pScaling[i].fTension, pByte, sizeof(float));
																if (SplineTerms & 0x02) BREAD(&pObject->pNode[NodeId].pScaling[i].fContinuity, pByte, sizeof(float));
																if (SplineTerms & 0x04) BREAD(&pObject->pNode[NodeId].pScaling[i].fBias, pByte, sizeof(float));
																if (SplineTerms & 0x08) BREAD(&fDummy, pByte, sizeof(float));
																if (SplineTerms & 0x10) BREAD(&fDummy, pByte, sizeof(float));

																/* Read scaling values */
																BREAD(&pObject->pNode[NodeId].pScaling[i].s.x, pByte, sizeof(float));
																BREAD(&pObject->pNode[NodeId].pScaling[i].s.y, pByte, sizeof(float));
																BREAD(&pObject->pNode[NodeId].pScaling[i].s.z, pByte, sizeof(float));

																/* Debug info */
																if (DIAGNOSTICS)
																{
																	sprintf(pszTmp, "Frame %d : Scaling of (%.2f, %.2f, %.2f)\n", pObject->pNode[NodeId].pScaling[i].FrameNumber, pObject->pNode[NodeId].pScaling[i].s.x, pObject->pNode[NodeId].pScaling[i].s.y, pObject->pNode[NodeId].pScaling[i].s.z);
																	OutputDebugString(pszTmp);
																	sprintf(pszTmp, "Spline terms : (%.2f, %.2f, %.2f)\n", pObject->pNode[NodeId].pScaling[i].fTension, pObject->pNode[NodeId].pScaling[i].fContinuity, pObject->pNode[NodeId].pScaling[i].fBias);
																	OutputDebugString(pszTmp);
																}
															}
															break;

										case MORPH_TRACK_TAG:	/* Morph object keys (not exported) */
																if (DIAGNOSTICS)
																{
																	OutputDebugString("MORPH_TRACK_TAG chunk found\n");
																}
																break;

										case MORPH_SMOOTH:		/* Smoothing angle for morphing objects */
																if (DIAGNOSTICS)
																{
																	OutputDebugString("MORPH_SMOOTH chunk found\n");
																}
																break;

										case INSTANCE_NAME:		/* Mesh instance name */
																if (DIAGNOSTICS)
																{
																	char pszInstance[12];

																	OutputDebugString("INSTANCE_NAME chunk found\n");
																	BREAD(pszInstance, pByte, 11*sizeof(char));
																	sprintf(pszTmp, "Instance name = %s\n", pszInstance);
																	OutputDebugString(pszTmp);
																}
																break;

										default:				/* Unknown chunks */
																if (DIAGNOSTICS)
																{
																	OutputDebugString("Unknown OBJECT_NODE_TAG chunk\n");
																	sprintf(pszTmp ,"Unknown chunk is 0x%X\n", TempChunk);
																	OutputDebugString(pszTmp);
																}
										}

										/* Next subchunk offset */
										TempFilePos+=TempSize;
									}

									/* Increase current node index */
									NodeId++;
									break;

	case TARGET_NODE_TAG :	bNodeTarget = 1; // mark node as target

	case CAMERA_NODE_TAG:	/* Object node */

									/* Set file position after chunk and size */
									TempFilePos = FilePos + 6;

									/* Read all subchunks of OBJECT_NODE_TAG */
									while (TempFilePos < FilePos+Size)
									{
										/* Go to next subchunk */
										pByte = p3DSFile + TempFilePos;

										/* Read chunk and size */
										BREAD(&TempChunk, pByte, sizeof(unsigned short));
										BREAD(&TempSize, pByte, sizeof(unsigned int));

										/* Process chunk */
										switch(TempChunk)
										{
										case NODE_ID:	/* NODE_ID subchunk */
														BREAD(&NodeId, pByte, sizeof(short));
														break;


										case NODE_HDR:	/* NODE_HDR subchunk */

														/* Read mesh name */
														for (j=0; j<20; j++)
														{
															BREAD(&pszName[j], pByte, sizeof(char));
															if(pszName[j] == 0) break;
														//	if(pszName[j]<'0' || pszName[j]>'z') pszName[j] = '_';
														//	if(pszName[0]>='0' && pszName[0]<='9') pszName[0] = 'N';
														}

														/* Find camera index corresponding to that node */
														for (i=0; i<pObject->nNumCameras; i++)
														{

															if (strcmp(pObject->pCamera[i].pszName, pszName)==0)
															{
																if(bNodeTarget==0)
																{
																	pObject->pNode[NodeId].Camera=i;
																	bNodeTarget = 0;
																}
																else
																{
																	pObject->pNode[NodeId].Target=i;
																	bNodeTarget = 0;
																}
																break;
															}
														}

														/* Read flags (2 of them) (not used) */
														BREAD(&TmpShort, pByte, sizeof(short));
														BREAD(&TmpShort, pByte, sizeof(short));

														/* Parent index */
														BREAD(&pObject->pNode[NodeId].ParentIndex, pByte, sizeof(short));

														/* Debug info */
														if (1)
														{
															sprintf(pszTmp, "------------\nCamera ID = %d %d %d\n------------\nCamera name = %s\nParent index = %d\n", NodeId, pObject->pNode[NodeId].Camera,pObject->pNode[NodeId].Target, pszName, pObject->pNode[NodeId].ParentIndex);
															OutputDebugString(pszTmp);
														}
														break;

										case POS_TRACK_TAG: /* P	Material[i].dwVertexShaderList	CXX0017: Error: symbol "Material" not found
OS_TRACK_TAG subchunk */
															OutputDebugString("here POS_TRACK_TAG chunk\n");

															/* Read internal flags and 2 unused unsigned int */
															BREAD(&TmpShort, pByte, sizeof(short));
															BREAD(&TmpInt, pByte, sizeof(unsigned int));
															BREAD(&TmpInt, pByte, sizeof(unsigned int));

															/* Read number of keys in track */
															BREAD(&pObject->pNode[NodeId].PositionKeys, pByte, sizeof(unsigned int));

															/* Debug info */
															if (1)
															{
																sprintf(pszTmp, "Position keys in track = %d\n", pObject->pNode[NodeId].PositionKeys);
																OutputDebugString(pszTmp);
															}

															/* Allocate memory for these keys */
															if (pObject->pNode[NodeId].PositionKeys)
															{
																pObject->pNode[NodeId].pPosition = (POSITIONKEY *)calloc(pObject->pNode[NodeId].PositionKeys, sizeof(POSITIONKEY));
															}

															/* Read all keys */
															for (i=0; i<pObject->pNode[NodeId].PositionKeys; i++)
															{
																/* Read frame number */
																BREAD(&pObject->pNode[NodeId].pPosition[i].FrameNumber, pByte, sizeof(unsigned int));

																/* Read spline terms */
																BREAD(&SplineTerms, pByte, sizeof(short));
																if (SplineTerms & 0x01) BREAD(&pObject->pNode[NodeId].pPosition[i].fTension, pByte, sizeof(float));
																if (SplineTerms & 0x02) BREAD(&pObject->pNode[NodeId].pPosition[i].fContinuity, pByte, sizeof(float));
																if (SplineTerms & 0x04) BREAD(&pObject->pNode[NodeId].pPosition[i].fBias, pByte, sizeof(float));
																if (SplineTerms & 0x08) BREAD(&fDummy, pByte, sizeof(float));
																if (SplineTerms & 0x10) BREAD(&fDummy, pByte, sizeof(float));

																/* Read position */
																BREAD(&pObject->pNode[NodeId].pPosition[i].p.x, pByte, sizeof(float));
																BREAD(&pObject->pNode[NodeId].pPosition[i].p.z, pByte, sizeof(float));
																BREAD(&pObject->pNode[NodeId].pPosition[i].p.y, pByte, sizeof(float));

																/* Debug info */
																if (1)
																{
																	sprintf(pszTmp, "Frame %d : Translation of (%.2f, %.2f, %.2f)\n", pObject->pNode[NodeId].pPosition[i].FrameNumber, pObject->pNode[NodeId].pPosition[i].p.x, pObject->pNode[NodeId].pPosition[i].p.y, pObject->pNode[NodeId].pPosition[i].p.z);
																	OutputDebugString(pszTmp);
																	sprintf(pszTmp, "Spline terms : (%.2f, %.2f, %.2f)\n", pObject->pNode[NodeId].pPosition[i].fTension, pObject->pNode[NodeId].pPosition[i].fContinuity, pObject->pNode[NodeId].pPosition[i].fBias);
																	OutputDebugString(pszTmp);
																}
															}
															break;


										default:				/* Unknown chunks */
																if (1)
																{
																	OutputDebugString("Unknown OBJECT_NODE_TAG chunk\n");
																	sprintf(pszTmp ,"Unknown chunk is 0x%X\n", TempChunk);
																	OutputDebugString(pszTmp);
																}
										}

										/* Next subchunk offset */
										TempFilePos+=TempSize;
									}

									/* Increase current node index */
									NodeId++;
									break;
		//	case CAMERA_NODE_TAG :		NodeId++; if (DIAGNOSTICS) OutputDebugString("CAMERA_NODE_TAG found\n"); break;
		//	case TARGET_NODE_TAG :		NodeId++; if (DIAGNOSTICS) OutputDebugString("TARGET_NODE_TAG found\n"); break;
			case LIGHT_NODE_TAG :		NodeId++; if (DIAGNOSTICS) OutputDebugString("LIGHT_NODE_TAG found\n"); break;
			case SPOTLIGHT_NODE_TAG :	NodeId++; if (DIAGNOSTICS) OutputDebugString("SPOTLIGHT_NODE_TAG found\n"); break;
			case L_TARGET_NODE_TAG :	NodeId++; if (DIAGNOSTICS) OutputDebugString("L_TARGET_NODE_TAG found\n"); break;
			default:					if (DIAGNOSTICS) OutputDebugString("Unknown KFDATA chunk found.\n"); break;
			}

			/* Have we finished looking for chunks ? */
			FilePos += Size;
		}
	}

	/* Done. We don't need the .3DS model anymore. */
	if (!bLoadFromResource) FREE(p3DSFile);


	/********************************
	*********************************
	** PROCESS EACH MESH IN OBJECT **
	*********************************
	********************************/
	/* Meshes only need to be processed if animation data was found */
	if (KFDATAFileOffset!=-1)
	{
		/* Loop through each mesh */
		for (i=0; i<pObject->nNumMeshes; i++)
		{
			PVRTMATRIX	FinalMatrix, TransMatrix, FlipX, ObjectOffsetMatrix;
			PVRTVECTOR3	V1, V2, V3, CrossProduct;
			unsigned short		Tmp;
			bool		bFlipOrder=false;
			short		nNode=-1;

			/******************************
			** Get World-to-Local Matrix **
			******************************/
			/* Get MESH_MATRIX for this mesh. This matrix is the transformation matrix
			   that transform a mesh from its local space to world space */
			TransMatrix = g_Matrix[i];

			/* Inverse it. This matrix can be used to transform the vertices from world space
			   to their local space (required for keyframer operations) */
			PVRTMatrixInverse(FinalMatrix, TransMatrix);


			/*****************
			** Parity Check **
			*****************/
			/* Check for objects that have been flipped: their 3D "parity" will be off */
			/* Get vectors from transformation matrix */
#if 0
			V1.x = TransMatrix._11; V1.y = TransMatrix._21; V1.z = TransMatrix._31;
			V2.x = TransMatrix._12;	V2.y = TransMatrix._22; V2.z = TransMatrix._32;
			V3.x = TransMatrix._13; V3.y = TransMatrix._23; V3.z = TransMatrix._33;
#else
			V1.x = TransMatrix.f[ 0];	V1.y = TransMatrix.f[ 1];	V1.z = TransMatrix.f[ 2];
			V2.x = TransMatrix.f[ 4];	V2.y = TransMatrix.f[ 5];	V2.z = TransMatrix.f[ 6];
			V3.x = TransMatrix.f[ 8];	V3.y = TransMatrix.f[ 9];	V3.z = TransMatrix.f[10];
#endif

			/* Compute cross product vector between V1 and V2 */
			PVRTMatrixVec3CrossProduct(CrossProduct, V1, V2);

			/* If dot product is negative the object has been flipped */
			if (PVRTMatrixVec3DotProduct(CrossProduct, V3) < 0.0f)
			{
				/* Debug output */
				if (DIAGNOSTICS)
				{
					OutputDebugString("Object has been flipped\n");
				}

				/* Flip the X coordinate by appending the FlipX matrix to our final matrix */
				PVRTMatrixIdentity(FlipX);
				FlipX.f[0] = -1.0f;
				PVRTMatrixMultiply(FinalMatrix, FinalMatrix, FlipX);

				/* Flip triangle ordering since we just flipped X */
				for (j=0; j<3*pObject->pMesh[i].nNumFaces; j+=3)
				{
					Tmp = pObject->pMesh[i].pFaces[j + 0];
					pObject->pMesh[i].pFaces[j + 0] = pObject->pMesh[i].pFaces[j + 2];
					pObject->pMesh[i].pFaces[j + 2] = Tmp;
				}
			}

			/****************************
			** Find Node For That Mesh **
			****************************/
			/* Find node corresponding to current mesh */
			for (j=0; j<pObject->nNumNodes; j++)
			{
				if (pObject->pNode[j].Mesh==(short)i)
				{
					nNode=j;
					break;
				}
			}


			/**************************************************************
			** Compute Local-to-World Matrix Using Keyframer Information **
			**************************************************************/
			if (nNode!=-1)
			{
				/* Reset matrix */
				PVRTMatrixIdentity(ObjectOffsetMatrix);

				/* Subtract pivot point from the matrix */
#if 0
				ObjectOffsetMatrix._41 -= pObject->pNode[nNode].Pivot[0];
				ObjectOffsetMatrix._42 -= pObject->pNode[nNode].Pivot[1];
				ObjectOffsetMatrix._43 -= pObject->pNode[nNode].Pivot[2];
#else
				ObjectOffsetMatrix.f[ 3] -= pObject->pNode[nNode].Pivot[0];
				ObjectOffsetMatrix.f[ 7] -= pObject->pNode[nNode].Pivot[1];
				ObjectOffsetMatrix.f[11] -= pObject->pNode[nNode].Pivot[2];
#endif

				/* Get local-to-world matrix for this mesh at frame 0 */
				ObjectOffsetMatrix = GetHierarchyMatrix(pObject, nNode, 0, ObjectOffsetMatrix);

				/* Concatenate matrices together */
				PVRTMatrixMultiply(FinalMatrix, FinalMatrix, ObjectOffsetMatrix);
			}

			/************************************************
			** Append Inversion Matrix (Inverting Y and Z) **
			************************************************/
			ZeroMemory(&InversionMatrix, sizeof(PVRTMATRIX));
#if 0
			InversionMatrix._11=1.0f;
			InversionMatrix._32=1.0f;
			InversionMatrix._23=1.0f;
			InversionMatrix._44=1.0f;
#else
			InversionMatrix.f[ 0]=1.0f;
			InversionMatrix.f[ 6]=1.0f;	// TMP
			InversionMatrix.f[ 9]=1.0f;
			InversionMatrix.f[15]=1.0f;
#endif

			/* Concatenate matrices together */
			PVRTMatrixMultiply(FinalMatrix, FinalMatrix, InversionMatrix);


			/*****************************************
			** Transform Vertices With Final Matrix **
			*****************************************/
			/* Get source pointer */
			pSource = (PVRTVECTOR3 *)pObject->pMesh[i].pVertex;

			/* Transform all vertices with FinalMatrix */
			for (j=0; j<pObject->pMesh[i].nNumVertex; j++)
			{
				/* Compute transformed vertex */
#if 0
				D.x =	pSource->x*FinalMatrix._11 + pSource->y*FinalMatrix._21 + pSource->z*FinalMatrix._31 + FinalMatrix._41;
				D.y =	pSource->x*FinalMatrix._12 + pSource->y*FinalMatrix._22 + pSource->z*FinalMatrix._32 + FinalMatrix._42;
				D.z =	pSource->x*FinalMatrix._13 + pSource->y*FinalMatrix._23 + pSource->z*FinalMatrix._33 + FinalMatrix._43;
#else
				D.x =	pSource->x*FinalMatrix.f[ 0] + pSource->y*FinalMatrix.f[ 4] + pSource->z*FinalMatrix.f[ 8] + FinalMatrix.f[12];
				D.y =	pSource->x*FinalMatrix.f[ 1] + pSource->y*FinalMatrix.f[ 5] + pSource->z*FinalMatrix.f[ 9] + FinalMatrix.f[13];
				D.z =	pSource->x*FinalMatrix.f[ 2] + pSource->y*FinalMatrix.f[ 6] + pSource->z*FinalMatrix.f[10] + FinalMatrix.f[14];
#endif

				/* Replace vertex with transformed vertex */
				*pSource++ = D;
			}
		}
	}
	else
	{
		/***************************************
		** NO ANIMATION EXPORTED (OLD MODELS) **
		***************************************/

		/* Make sure we set the number of nodes and frames to 0 */
		pObject->nNumNodes =	0;
		pObject->nNumFrames =	0;
		pObject->pNode =		NULL;

		/* Create inversion matrix */
		ZeroMemory(&InversionMatrix, sizeof(PVRTMATRIX));
#if 0
		InversionMatrix._11=1.0f;
		InversionMatrix._32=1.0f;
		InversionMatrix._23=1.0f;
		InversionMatrix._44=1.0f;
#else
		InversionMatrix.f[ 0]=1.0f;
		InversionMatrix.f[ 6]=1.0f;
		InversionMatrix.f[ 9]=1.0f;
		InversionMatrix.f[15]=1.0f;
#endif

		/* Loop through each mesh */
		for (i=0; i<pObject->nNumMeshes; i++)
		{
			/*********************************************
			** Transform Vertices with Inversion Matrix **
			*********************************************/
			/* Get source pointer */
			pSource = (PVRTVECTOR3 *)pObject->pMesh[i].pVertex;

			/* Transform all vertices with FinalMatrix */
			for (j=0; j<pObject->pMesh[i].nNumVertex; j++)
			{
				/* Compute transformed vertex */
#if 0
				D.x =	pSource->x*InversionMatrix._11 + pSource->y*InversionMatrix._21 + pSource->z*InversionMatrix._31 + InversionMatrix._41;
				D.y =	pSource->x*InversionMatrix._12 + pSource->y*InversionMatrix._22 + pSource->z*InversionMatrix._32 + InversionMatrix._42;
				D.z =	pSource->x*InversionMatrix._13 + pSource->y*InversionMatrix._23 + pSource->z*InversionMatrix._33 + InversionMatrix._43;
#else
				D.x =	pSource->x*InversionMatrix.f[ 0] + pSource->y*InversionMatrix.f[ 4] + pSource->z*InversionMatrix.f[ 8] + InversionMatrix.f[12];
				D.y =	pSource->x*InversionMatrix.f[ 1] + pSource->y*InversionMatrix.f[ 5] + pSource->z*InversionMatrix.f[ 9] + InversionMatrix.f[13];
				D.z =	pSource->x*InversionMatrix.f[ 2] + pSource->y*InversionMatrix.f[ 6] + pSource->z*InversionMatrix.f[10] + InversionMatrix.f[14];
#endif

				/* Replace vertex with transformed vertex */
				*pSource++ = D;
			}
		}
	}


	/********************************
	** Compute normals and centres **
	********************************/
	/* Loop through each mesh */
	for (i=0; i<pObject->nNumMeshes; i++)
	{
		/********************
		** Compute Normals **
		********************/
		CalculateNormals(pObject->pMesh[i].nNumVertex, pObject->pMesh[i].pVertex,
						 pObject->pMesh[i].nNumFaces, pObject->pMesh[i].pFaces,
						 pObject->pMesh[i].pNormals);

		/************************
		** Compute mesh centre **
		************************/
 		/* Initialise boundary box values */
		pObject->pMesh[i].fMinimum[0] = pObject->pMesh[i].fMaximum[0] = pObject->pMesh[i].pVertex[0];
		pObject->pMesh[i].fMinimum[1] = pObject->pMesh[i].fMaximum[1] = pObject->pMesh[i].pVertex[1];
		pObject->pMesh[i].fMinimum[2] = pObject->pMesh[i].fMaximum[2] = pObject->pMesh[i].pVertex[2];

		/* For each vertex of each mesh */
		for (j=0; j<3*pObject->pMesh[i].nNumVertex; j+=3)
		{
			/* Get current vertex */
			fX = pObject->pMesh[i].pVertex[j + 0];
			fY = pObject->pMesh[i].pVertex[j + 1];
			fZ = pObject->pMesh[i].pVertex[j + 2];

			/* Mesh minimum */
			if (fX < pObject->pMesh[i].fMinimum[0])	pObject->pMesh[i].fMinimum[0] = fX;
			if (fY < pObject->pMesh[i].fMinimum[1])	pObject->pMesh[i].fMinimum[1] = fY;
			if (fZ < pObject->pMesh[i].fMinimum[2])	pObject->pMesh[i].fMinimum[2] = fZ;

			/* Mesh maximum */
			if (fX > pObject->pMesh[i].fMaximum[0])	pObject->pMesh[i].fMaximum[0] = fX;
			if (fY > pObject->pMesh[i].fMaximum[1])	pObject->pMesh[i].fMaximum[1] = fY;
			if (fZ > pObject->pMesh[i].fMaximum[2])	pObject->pMesh[i].fMaximum[2] = fZ;
		}

		/* Write mesh centre */
		pObject->pMesh[i].fCentre[0] = (pObject->pMesh[i].fMinimum[0] + pObject->pMesh[i].fMaximum[0]) * 0.5f;
		pObject->pMesh[i].fCentre[1] = (pObject->pMesh[i].fMinimum[1] + pObject->pMesh[i].fMaximum[1]) * 0.5f;
		pObject->pMesh[i].fCentre[2] = (pObject->pMesh[i].fMinimum[2] + pObject->pMesh[i].fMaximum[2]) * 0.5f;
	}


	/******************************************
	** Calculating the total number of polys **
	******************************************/
	pObject->nTotalVertices = 0;
	pObject->nTotalFaces = 0;
	for (i=0; i<pObject->nNumMeshes;i++)
	{
		pObject->nTotalVertices += pObject->pMesh[i].nNumVertex;
		pObject->nTotalFaces += pObject->pMesh[i].nNumFaces;
	}


	/*********************************
	** Compute object global centre **
	*********************************/
	/* Initialise bounding box values */
	pObject->fGroupMinimum[0] = pObject->fGroupMaximum[0] = pObject->pMesh[0].fMinimum[0];
	pObject->fGroupMinimum[1] = pObject->fGroupMaximum[1] = pObject->pMesh[0].fMinimum[1];
	pObject->fGroupMinimum[2] = pObject->fGroupMaximum[2] = pObject->pMesh[0].fMinimum[2];

	/* Look through all meshes */
	for (i=0; i<pObject->nNumMeshes; i++)
	{
		if (pObject->pMesh[i].fMinimum[0] < pObject->fGroupMinimum[0]) pObject->fGroupMinimum[0] = pObject->pMesh[i].fMinimum[0];
		if (pObject->pMesh[i].fMinimum[1] < pObject->fGroupMinimum[1]) pObject->fGroupMinimum[1] = pObject->pMesh[i].fMinimum[1];
		if (pObject->pMesh[i].fMinimum[2] < pObject->fGroupMinimum[2]) pObject->fGroupMinimum[2] = pObject->pMesh[i].fMinimum[2];

		if (pObject->pMesh[i].fMaximum[0] > pObject->fGroupMaximum[0]) pObject->fGroupMaximum[0] = pObject->pMesh[i].fMaximum[0];
		if (pObject->pMesh[i].fMaximum[1] > pObject->fGroupMaximum[1]) pObject->fGroupMaximum[1] = pObject->pMesh[i].fMaximum[1];
		if (pObject->pMesh[i].fMaximum[2] > pObject->fGroupMaximum[2]) pObject->fGroupMaximum[2] = pObject->pMesh[i].fMaximum[2];
	}

	/* Finally compute object centre from bounding box */
	pObject->fGroupCentre[0] = (pObject->fGroupMinimum[0] + pObject->fGroupMaximum[0]) * 0.5f;
	pObject->fGroupCentre[1] = (pObject->fGroupMinimum[1] + pObject->fGroupMaximum[1]) * 0.5f;
	pObject->fGroupCentre[2] = (pObject->fGroupMinimum[2] + pObject->fGroupMaximum[2]) * 0.5f;

	/* No problem occured */
	return true;
}
#pragma optimize( "", on )


/*!***************************************************************************
 @Function			PVRTModel3DSDestroy
 @Modified			pObject				Object containing the loaded 3DS data
 @Return			true or false
 @Description		Destroy an object allocated with PVRTModel3DSRead()
*****************************************************************************/
void PVRTModel3DSDestroy(ObjectType *pObject)
{
	int i;

	/* Only release sub-pointers if main pointer was allocated! */
	if (pObject->pNode)
	{
		/* Loop through all frames */
		for (i=0; i<pObject->nNumNodes; i++)
		{
			/* Release memory taken up by each mesh animation structure in each frame */
			FREE(pObject->pNode[i].pScaling);
			FREE(pObject->pNode[i].pRotation);
			FREE(pObject->pNode[i].pPosition);
		}
	}

	/* Release memory taken up by each node */
	FREE(pObject->pNode);

    /* Only release sub-pointers if main pointer was allocated! */
	if (pObject->pMesh)
	{
		/* Loop through all meshes */
		for (i=0; i<pObject->nNumMeshes; i++)
		{
			/* Free all mesh data */
			FREE(pObject->pMesh[i].pVertex);
			FREE(pObject->pMesh[i].pFaces);
			FREE(pObject->pMesh[i].pUV);
			FREE(pObject->pMesh[i].pNormals);

			/* Reset number of vertices and faces */
			pObject->pMesh[i].nNumVertex= 0;
			pObject->pMesh[i].nNumFaces	= 0;
		}
	}

	/* Release memory taken up by meshes structures */
	FREE(pObject->pMesh);

	/* Release memory taken up by cameras structures */
	FREE(pObject->pCamera);

	/* Release memory taken up by lights structures */
	FREE(pObject->pLight);

	/* Release memory taken up by materials structures */
	FREE(pObject->pMaterial);

	/* Reset counters */
	pObject->nNumMeshes		=  0;
	pObject->nNumMaterials	=  0;
	pObject->nNumCameras	=  0;
	pObject->nNumLights		=  0;
	pObject->nNumFrames		=  0;
}

/*!***************************************************************************
 @Function			PVRTModel3DSGetCameraAnimation
 @Output			pPosition			Camera position at this frame
 @Output			pTarget				Camera target at this frame
 @Input				pObject				Object containing the loaded 3DS data
 @Input				nCamera				Number of the camera
 @Input				fFrameNumber		Frame number
 @Return			true or false
 @Description		Return position and target required to animate this camera at the
					specified frame. Frame number is a floating point number as (linear)
					interpolation will be used to compute the required matrix at
					this frame.
*****************************************************************************/
bool PVRTModel3DSGetCameraAnimation(ObjectType *pObject, short nCamera, float fFrameNumber,
									PVRTVECTOR3 *pPosition, PVRTVECTOR3 *pTarget)
{
	int			i, j;
	int			nKey1, nKey2;
	float		t;

	/* Parameter checking */
	if (!pObject || nCamera<0 || nCamera>pObject->nNumCameras || fFrameNumber<0.0f || (int)fFrameNumber>pObject->nNumFrames)
	{
		OutputDebugString("PVRTModel3DSGetCameraAnimation : Bad parameters\n");
		return false;
	}

	/* Loop through all nodes until we find our mesh */
	for (i=0; i<pObject->nNumNodes; i++)
	{
		//sprintf(pszTmp, "Node : %d\n", i); OutputDebugString(pszTmp);

		/* Does this node correspond to our camera or target? */
		if (pObject->pNode[i].Camera==nCamera || pObject->pNode[i].Target==nCamera)
		{
			/* When there is no animation we just copy the first key */
			if(pObject->pNode[i].PositionKeys<=1)
			{
				if( pObject->pNode[i].Camera==nCamera) // for camera position
				{
						*pPosition = pObject->pNode[i].pPosition[0].p;
				}
				else
				{
						*pTarget = pObject->pNode[i].pPosition[0].p;
				}
			}

			else /* Otherwise, go through all keys until we find the ones that contain fFrameNumber */
			{
				for (j=0; j<pObject->pNode[i].PositionKeys-1; j++)
				{
					nKey1 = pObject->pNode[i].pPosition[j].FrameNumber;
					nKey2 = pObject->pNode[i].pPosition[j+1].FrameNumber;

					if(nKey1<=(int)fFrameNumber && nKey2>=(int)fFrameNumber)
					{
							/* We are into, so interpolate the two positions */

						/* Compute t, the time corresponding to the frame we want to interpolate */
						t = (float)(fFrameNumber - nKey1) / (float)(nKey2 - nKey1);


						/* Get interpolated position */
						if( pObject->pNode[i].Camera==nCamera) // for camera position
						{
							PVRTMatrixVec3Lerp(*pPosition, pObject->pNode[i].pPosition[j].p, pObject->pNode[i].pPosition[j+1].p, t);
						}
						else  // otherwise for camera target
						{
							PVRTMatrixVec3Lerp(*pTarget, pObject->pNode[i].pPosition[j].p, pObject->pNode[i].pPosition[j+1].p, t);
						}
					}
				} // j loop
			}
		}
	} // i loop

	/* Return error */
	return true;
}

/*!***************************************************************************
 @Function			PVRTModel3DSGetAnimationMatrix
 @Input				pObject				Object containing the loaded 3DS data
 @Input				nMesh				Mesh number
 @Input				fFrameNumber		Frame number
 @Output			pAnimationMatrix	Animation matrix at this frame
 @Return			true or false
 @Description		Return animation matrix required to animate this mesh at the
					specified frame.
					Frame number is a floating point number as (linear)
					interpolation will be used to compute the required matrix at
					this frame.
*****************************************************************************/
bool PVRTModel3DSGetAnimationMatrix(ObjectType *pObject, short nMesh, float fFrameNumber,
									PVRTMATRIX *pAnimationMatrix)
{
	PVRTMATRIX	ObjectOffsetMatrix, WorldToLocalMatrix, TmpMatrix;
	int			i;

	/* Parameter checking */
	if (!pObject || nMesh<0 || nMesh>pObject->nNumMeshes || fFrameNumber<0.0f || (int)fFrameNumber>pObject->nNumFrames)
	{
		OutputDebugString("PVRTModel3DSGetAnimationMatrix : Bad parameters\n");
		PVRTMatrixIdentity(*pAnimationMatrix);
		return false;
	}

	/* Loop through all nodes until we find our mesh */
	for (i=0; i<pObject->nNumNodes; i++)
	{
		/* Does this node correspond to our mesh ? */
		if (pObject->pNode[i].Mesh==nMesh)
		{
			/* Only process if mesh is valid */
			if (nMesh!=-1)
			{
				/* Reset matrix to identity */
				PVRTMatrixIdentity(TmpMatrix);

				/* Subtract pivot point from the matrix */
#if 0
				TmpMatrix._41 -= pObject->pNode[i].Pivot[0];
				TmpMatrix._42 -= pObject->pNode[i].Pivot[1];
				TmpMatrix._43 -= pObject->pNode[i].Pivot[2];
#else
				TmpMatrix.f[12] -= pObject->pNode[i].Pivot[0];
				TmpMatrix.f[13] -= pObject->pNode[i].Pivot[1];
				TmpMatrix.f[14] -= pObject->pNode[i].Pivot[2];
#endif

				/* Get final local-to-world matrix for this mesh at this frame */
				ObjectOffsetMatrix = GetHierarchyMatrix(pObject, (short)i, fFrameNumber, TmpMatrix);

				/* Get final local-to-world matrix for this mesh at frame 0 */
				WorldToLocalMatrix = GetHierarchyMatrix(pObject, (short)i, 0.0f, TmpMatrix);

				/* Invert y and z to get back to 3DS coordinates */
				TmpMatrix = WorldToLocalMatrix;
#if 0
				WorldToLocalMatrix._12 = TmpMatrix._13;
				WorldToLocalMatrix._22 = TmpMatrix._23;
				WorldToLocalMatrix._32 = TmpMatrix._33;
				WorldToLocalMatrix._42 = TmpMatrix._43;
				WorldToLocalMatrix._13 = TmpMatrix._12;
				WorldToLocalMatrix._23 = TmpMatrix._22;
				WorldToLocalMatrix._33 = TmpMatrix._32;
				WorldToLocalMatrix._43 = TmpMatrix._42;
#else
				WorldToLocalMatrix.f[ 1] = TmpMatrix.f[ 2];
				WorldToLocalMatrix.f[ 5] = TmpMatrix.f[ 6];
				WorldToLocalMatrix.f[ 9] = TmpMatrix.f[10];
				WorldToLocalMatrix.f[13] = TmpMatrix.f[14];
				WorldToLocalMatrix.f[ 2] = TmpMatrix.f[ 1];
				WorldToLocalMatrix.f[ 6] = TmpMatrix.f[ 5];
				WorldToLocalMatrix.f[10] = TmpMatrix.f[ 9];
				WorldToLocalMatrix.f[14] = TmpMatrix.f[13];
#endif

				/* Inverse matrix so that we can bring the model back to local coordinate */
				PVRTMatrixInverse(WorldToLocalMatrix, WorldToLocalMatrix);

				/* Concatenate this matrix with the local mesh matrix */
				PVRTMatrixMultiply(ObjectOffsetMatrix, WorldToLocalMatrix, ObjectOffsetMatrix);

				/* Re-invert y and z */
				*pAnimationMatrix  = ObjectOffsetMatrix;
#if 0
				pAnimationMatrix->_12 = ObjectOffsetMatrix._13;
				pAnimationMatrix->_22 = ObjectOffsetMatrix._23;
				pAnimationMatrix->_32 = ObjectOffsetMatrix._33;
				pAnimationMatrix->_42 = ObjectOffsetMatrix._43;
				pAnimationMatrix->_13 = ObjectOffsetMatrix._12;
				pAnimationMatrix->_23 = ObjectOffsetMatrix._22;
				pAnimationMatrix->_33 = ObjectOffsetMatrix._32;
				pAnimationMatrix->_43 = ObjectOffsetMatrix._42;
#else
				pAnimationMatrix->f[ 1] = ObjectOffsetMatrix.f[ 2];
				pAnimationMatrix->f[ 5] = ObjectOffsetMatrix.f[ 6];
				pAnimationMatrix->f[ 9] = ObjectOffsetMatrix.f[10];
				pAnimationMatrix->f[13] = ObjectOffsetMatrix.f[14];
				pAnimationMatrix->f[ 2] = ObjectOffsetMatrix.f[ 1];
				pAnimationMatrix->f[ 6] = ObjectOffsetMatrix.f[ 5];
				pAnimationMatrix->f[10] = ObjectOffsetMatrix.f[ 9];
				pAnimationMatrix->f[14] = ObjectOffsetMatrix.f[13];
#endif

				/* No problem occured */
				return true;
			}
		}
	}

	/* No nodes contained our mesh */
	OutputDebugString("PVRTModel3DSGetAnimationMatrix : Mesh not found\n");

	/* Return Identity matrix to avoid problems */
	PVRTMatrixIdentity(*pAnimationMatrix);
	return false;
}


/*!***************************************************************************
 @Function			PVRTModel3DSDisplayInfo
 @Modified			pObject				Object containing the loaded 3DS data
 @Description		Display model data into debug output
*****************************************************************************/
void PVRTModel3DSDisplayInfo(ObjectType *pObject)
{
	int		i;
	char	pszTmp[512];

	/* Display total number of vertices and faces */
	sprintf(pszTmp, "Meshes : %d\nFaces : %d\n", pObject->nTotalVertices, pObject->nTotalFaces);
	OutputDebugString(pszTmp);

	/* Display centre and extremas */
	sprintf(pszTmp, "Meshes : %d\nGroup centre : (%f, %f, %f)\nMinimum : (%f, %f, %f)\nMaximum : (%f, %f, %f)\n",
		pObject->nNumMeshes, pObject->fGroupCentre[0], pObject->fGroupCentre[1], pObject->fGroupCentre[2],
		pObject->fGroupMinimum[0], pObject->fGroupMinimum[1], pObject->fGroupMinimum[2],
		pObject->fGroupMaximum[0], pObject->fGroupMaximum[1], pObject->fGroupMaximum[2]);
	OutputDebugString(pszTmp);

	/* Display total number of meshes, nodes and frames */
	sprintf(pszTmp, "Meshes : %d\nNodes : %d\nFrames : %d\n", pObject->nNumMeshes, pObject->nNumNodes, pObject->nNumFrames);
	OutputDebugString(pszTmp);

	/* Display light information */
	if (pObject->nNumLights)
	{
		for (i=0; i<pObject->nNumLights; i++)
		{
			sprintf(pszTmp, "Light %d : Position = (%.3f, %.3f, %.3f)\n        Color = %.3f %.3f %.3f\n", i,
					pObject->pLight[i].fPosition[0], pObject->pLight[i].fPosition[1], pObject->pLight[i].fPosition[2],
					pObject->pLight[i].fColour[0], pObject->pLight[i].fColour[1], pObject->pLight[i].fColour[2]);
			OutputDebugString(pszTmp);
		}
	}
	else
	{
		OutputDebugString("No light defined\n");
	}

	/* Display camera information */
	if (pObject->nNumCameras)
	{
		for (i=0; i<pObject->nNumCameras; i++)
		{
			sprintf(pszTmp, "Camera %d : Position = (%.3f, %.3f, %.3f)\n         Target = (%.3f %.3f %.3f)\n         Roll = %.3f  Focal length = %.3f\n", i,
					pObject->pCamera[i].fPosition[0], pObject->pCamera[i].fPosition[1], pObject->pCamera[i].fPosition[2],
					pObject->pCamera[i].fTarget[0], pObject->pCamera[i].fTarget[1], pObject->pCamera[i].fTarget[2],
					pObject->pCamera[i].fRoll, pObject->pCamera[i].fFocalLength);
			OutputDebugString(pszTmp);
		}
	}
	else
	{
		OutputDebugString("No camera defined\n");
	}

	/* Display each mesh */
	if (pObject->nNumMeshes)
	{
		for (i=0; i<pObject->nNumMeshes; i++)
		{
			sprintf(pszTmp, "* Mesh %d: %s\n", i, pObject->pMesh[i].pszName);
			OutputDebugString(pszTmp);
			sprintf(pszTmp, "Material : %s (Number %d)\n", pObject->pMesh[i].pszMaterial, pObject->pMesh[i].nMaterialNumber);
			OutputDebugString(pszTmp);
			sprintf(pszTmp, "Mesh minimum : (%f, %f, %f)\n", pObject->pMesh[i].fMinimum[0], pObject->pMesh[i].fMinimum[1], pObject->pMesh[i].fMinimum[2]);
			OutputDebugString(pszTmp);
			sprintf(pszTmp, "Mesh maximum : (%f, %f, %f)\n", pObject->pMesh[i].fMaximum[0], pObject->pMesh[i].fMaximum[1], pObject->pMesh[i].fMaximum[2]);
			OutputDebugString(pszTmp);
			sprintf(pszTmp, "Mesh centre : (%f, %f, %f)\n", pObject->pMesh[i].fCentre[0], pObject->pMesh[i].fCentre[1], pObject->pMesh[i].fCentre[2]);
			OutputDebugString(pszTmp);
			sprintf(pszTmp, "Vertices : %d  Faces : %d\n", pObject->pMesh[i].nNumVertex, pObject->pMesh[i].nNumFaces);
			OutputDebugString(pszTmp);
			sprintf(pszTmp, "Pointers : Vertex = 0x%X   Faces = 0x%X   Normals = 0x%X   UV = 0x%X\n",
				pObject->pMesh[i].pVertex, pObject->pMesh[i].pFaces, pObject->pMesh[i].pNormals, pObject->pMesh[i].pUV);
			OutputDebugString(pszTmp);
		}
	}
	else
	{
		OutputDebugString("No meshes defined\n");
	}

	/* Display each material */
	if (pObject->nNumMaterials)
	{
		for (i=0; i<pObject->nNumMaterials; i++)
		{
			sprintf(pszTmp, "Material %d:\n", i);
			OutputDebugString(pszTmp);

			sprintf(pszTmp, "Name : %s\nFile : %s\nOpaq : %s\n",
				pObject->pMaterial[i].pszMatName, pObject->pMaterial[i].pszMatFile, pObject->pMaterial[i].pszMatOpaq);
			OutputDebugString(pszTmp);

			sprintf(pszTmp, "Opacity : %d\n", pObject->pMaterial[i].nMatOpacity);
			OutputDebugString(pszTmp);

			sprintf(pszTmp, "Ambient : %f %f %f\nDiffuse : %f %f %f\nSpecular : %f %f %f\n",
				pObject->pMaterial[i].fMatAmbient[0], pObject->pMaterial[i].fMatAmbient[1], pObject->pMaterial[i].fMatAmbient[2],
				pObject->pMaterial[i].fMatDiffuse[0], pObject->pMaterial[i].fMatDiffuse[1], pObject->pMaterial[i].fMatDiffuse[2],
				pObject->pMaterial[i].fMatSpecular[0], pObject->pMaterial[i].fMatSpecular[1], pObject->pMaterial[i].fMatSpecular[2]);
			OutputDebugString(pszTmp);

			sprintf(pszTmp, "Shininess : %f  Transparency : %f\n",
				pObject->pMaterial[i].fMatShininess, pObject->pMaterial[i].fMatTransparency);
			OutputDebugString(pszTmp);

			sprintf(pszTmp, "Shading : %d\n", pObject->pMaterial[i].sMatShading);
			OutputDebugString(pszTmp);
		}
	}
	else
	{
		OutputDebugString("No materials defined\n");
	}
}


/*!***************************************************************************
 @Function			PVRTModel3DSScale
 @Modified			pObject				Object containing the loaded 3DS data
 @Input				Scale				Scale to apply
 @Description		Scale a model's vertices with a uniform scaling value
*****************************************************************************/
void PVRTModel3DSScale(ObjectType *pObject, const float Scale)
{
	int i, j;

	/* Loop through all meshes */
	for (i=0; i<pObject->nNumMeshes; i++)
	{
		/* Loop through all vertices in mesh */
		for (j=0; j<pObject->pMesh[i].nNumVertex*3;j++)
		{
			/* Scale vertex */
			*(pObject->pMesh[i].pVertex + j) *= Scale;
		}
	}
}



/******************************************************************************
*******************************************************************************
**************************** LOCAL FUNCTIONS **********************************
*******************************************************************************
******************************************************************************/



/*!***************************************************************************
 @Function			GetHierarchyMatrix
 @Modified			nNode
 @Modified			fFrameNumber
 @Modified			CurrentMatrix
 @Return			Concatenated matrix
 @Description		Return total local-to-world matrix for this mesh
*****************************************************************************/
static PVRTMATRIX GetHierarchyMatrix(ObjectType *pObject, short nNode, float fFrameNumber, PVRTMATRIX CurrentMatrix)
{
	PVRTMATRIX		Matrix, PositionMatrix, RotationMatrix, ScalingMatrix;
	PVRTVECTOR3		Position, Scaling;
	int				i;
	int				nPreviousKey, nNextKey;
	float			t;

	/* Reset matrices */
	PVRTMatrixIdentity(PositionMatrix);
	PVRTMatrixIdentity(RotationMatrix);
	PVRTMatrixIdentity(ScalingMatrix);
	PVRTMatrixIdentity(Matrix);


	/*********************
	** Process position **
	*********************/
	/* Find previous key containing position information */
	nPreviousKey=-1;
	for (i=pObject->pNode[nNode].PositionKeys-1; i>=0; i--)
	{
		/* Is the first frame number below the one required in this key ? */
		if (pObject->pNode[nNode].pPosition[i].FrameNumber<=(int)fFrameNumber)
		{
			/* Yes, store it and exit */
			nPreviousKey = i;
			break;
		}
	}

	/* Find next key containing position information */
	nNextKey=-1;
	for (i=0; i<pObject->pNode[nNode].PositionKeys; i++)
	{
		/* Is the first frame number above the one required in this key ? */
		if (pObject->pNode[nNode].pPosition[i].FrameNumber>=(int)fFrameNumber+1)
		{
			/* Yes, store it and exit */
			nNextKey = i;
			break;
		}
	}

	/* If no previous key was found set position to the first valid key position (i.e next key) */
	if (nPreviousKey==-1)
	{
		Position = pObject->pNode[nNode].pPosition[nNextKey].p;
	}
	else
	{
		/* If no next key was found, set position to previous valid key position (i.e previous key) */
		if (nNextKey==-1)
		{
			Position = pObject->pNode[nNode].pPosition[nPreviousKey].p;
		}
		else
		{
			/* Interpolate between these two frames */

			/* Compute t, the time corresponding to the frame we want to interpolate */
			t = (float)(fFrameNumber - pObject->pNode[nNode].pPosition[nPreviousKey].FrameNumber) /
				(pObject->pNode[nNode].pPosition[nNextKey].FrameNumber - pObject->pNode[nNode].pPosition[nPreviousKey].FrameNumber);

			/* Get interpolated position */
			PVRTMatrixVec3Lerp(Position, pObject->pNode[nNode].pPosition[nPreviousKey].p, pObject->pNode[nNode].pPosition[nNextKey].p, t);

			// The following uses tangents, continuity, bias and tension values */
			//Position = GetInterpolatedPosition(nFrameNumber, &pObject->pNode[nNode], nPreviousKey, nNextKey);
		}
	}

	/* Perform translation */
	PVRTMatrixTranslation(PositionMatrix, Position.x, Position.y, Position.z);

	/* Concatenate matrices */
	PVRTMatrixMultiply(Matrix, PositionMatrix, Matrix);


	/*********************
	** Process rotation **
	*********************/
	/* Get absolute rotation at this frame */
	GetAbsoluteRotation(&RotationMatrix, fFrameNumber, &pObject->pNode[nNode]);

	/* Concatenate matrices */
	PVRTMatrixMultiply(Matrix, RotationMatrix, Matrix);


	/********************
	** Process scaling **
	********************/
	/* Find previous key containing scaling information */
	nPreviousKey=-1;
	for (i=pObject->pNode[nNode].ScalingKeys-1; i>=0; i--)
	{
		/* Is the first frame number below the one required in this key ? */
		if (pObject->pNode[nNode].pScaling[i].FrameNumber<=(int)fFrameNumber)
		{
			/* Yes, store it and exit */
			nPreviousKey = i;
			break;
		}
	}

	/* Find next key containing scaling information */
	nNextKey=-1;
	for (i=0; i<pObject->pNode[nNode].ScalingKeys; i++)
	{
		/* Is the first frame number above the one required in this key ? */
		if (pObject->pNode[nNode].pScaling[i].FrameNumber>=(int)fFrameNumber+1)
		{
			/* Yes, store it and exit */
			nNextKey = i;
			break;
		}
	}

	/* If no previous key was found set scaling to the first valid key scaling (i.e next key) */
	if (nPreviousKey==-1)
	{
		Scaling = pObject->pNode[nNode].pScaling[nNextKey].s;
	}
	else
	{
		/* If no next key was found, set scaling to previous valid key scaling (i.e previous key) */
		if (nNextKey==-1)
		{
			Scaling = pObject->pNode[nNode].pScaling[nPreviousKey].s;
		}
		else
		{
			/* Interpolate between these two frames */

			/* Compute t, the time corresponding to the frame we want to interpolate */
			t = (float)(fFrameNumber - pObject->pNode[nNode].pScaling[nPreviousKey].FrameNumber) /
				(pObject->pNode[nNode].pScaling[nNextKey].FrameNumber - pObject->pNode[nNode].pScaling[nPreviousKey].FrameNumber);

			/* Get interpolated scaling */
			PVRTMatrixVec3Lerp(Scaling, pObject->pNode[nNode].pScaling[nPreviousKey].s, pObject->pNode[nNode].pScaling[nNextKey].s, t);
		}
	}

	/* Perform scaling */
#if 0
	ScalingMatrix._11 = Scaling.x;
	ScalingMatrix._22 = Scaling.y;
	ScalingMatrix._33 = Scaling.z;
#else
	ScalingMatrix.f[0] = Scaling.x;
	ScalingMatrix.f[5] = Scaling.y;
	ScalingMatrix.f[10] = Scaling.z;
#endif
	/* Concatenate matrices */
	PVRTMatrixMultiply(Matrix, ScalingMatrix, Matrix);


	/***********************************
	** Concatenate with parent matrix **
	***********************************/
	PVRTMatrixMultiply(CurrentMatrix, CurrentMatrix, Matrix);


	/*************************
	** Recurse in hierarchy **
	*************************/
	if (pObject->pNode[nNode].ParentIndex!=-1 && pObject->pNode[nNode].ParentIndex!=nNode)
	{
		CurrentMatrix = GetHierarchyMatrix(pObject, pObject->pNode[nNode].ParentIndex, fFrameNumber, CurrentMatrix);
	}

	/* Return matrix */
	return CurrentMatrix;
}


/*!***************************************************************************
 @Function			GetAbsoluteRotation
 @Input				nFrameNumber
 @Input				pNode
 @Return			Quaternion corresponding to rotation at specified frame
 @Description		Return quaternion corresponding to rotation at specified
					frame. Interpolation might be used if no key contains the
					specified frame number.
*****************************************************************************/
static void GetAbsoluteRotation(PVRTMATRIX * const pmRot, float fFrameNumber, NodeType *pNode)
{
	PVRTQUATERNION	TmpQuaternion, RotationQuaternion, Q1, Q2;
	int			i, nPreviousKey, nNextKey;
	float		t;


	/* Find previous key containing rotation information */
	nPreviousKey=-1;
	for (i=pNode->RotationKeys-1; i>=0; i--)
	{
		/* Is the first frame number below the one required in this key ? */
		if (pNode->pRotation[i].FrameNumber<=(int)fFrameNumber)
		{
			/* Yes, store it and exit */
			nPreviousKey = i;
			break;
		}
	}

	/* Find next key containing rotation information */
	nNextKey=-1;
	for (i=0; i<pNode->RotationKeys; i++)
	{
		/* Is the first frame number above the one required in this key ? */
		if (pNode->pRotation[i].FrameNumber>=(int)fFrameNumber+1)
		{
			/* Yes, store it and exit */
			nNextKey = i;
			break;
		}
	}

	/* If no previous key was found set rotation to the first valid key rotation (i.e next key) */
	if (nPreviousKey==-1)
	{
		/* Create quaternion from vector and angle */
		PVRTMatrixQuaternionRotationAxis(RotationQuaternion, pNode->pRotation[nNextKey].r, pNode->pRotation[nNextKey].Angle);
		PVRTMatrixRotationQuaternion(*pmRot, RotationQuaternion);
		PVRTMatrixTranspose(*pmRot, *pmRot);
		return;
	}
	else
	{
		/* Compute absolute quaternion for previous key */

		/* Initialise quaternion */
		PVRTMatrixQuaternionIdentity(Q1);
		for (i=0; i<=nPreviousKey; i++)
		{
			/* Create quaternion from vector and angle */
			PVRTMatrixQuaternionRotationAxis(TmpQuaternion, pNode->pRotation[i].r, pNode->pRotation[i].Angle);

			/* Multiply quaternion with previous one */
			PVRTMatrixQuaternionMultiply(Q1, Q1, TmpQuaternion);
		}

		/* If no next key was found, set rotation to previous valid key rotation (i.e previous key) */
		if (nNextKey==-1)
		{
			/* Convert quaternion to matrix & return result */
			PVRTMatrixRotationQuaternion(*pmRot, Q1);
			PVRTMatrixTranspose(*pmRot, *pmRot);
			return;
		}
	}

	/* Compute absolute quaternion for next key */

	/* Create quaternion from vector and angle */
	PVRTMatrixQuaternionRotationAxis(TmpQuaternion, pNode->pRotation[nNextKey].r, pNode->pRotation[nNextKey].Angle);

	/* Multiply quaternion with previous one */
	PVRTMatrixQuaternionMultiply(Q2, Q1, TmpQuaternion);


	/* We now have our two quaternions Q1 and Q2, let's interpolate */

	/* Compute t, the time corresponding to the frame we want to interpolate */
	t = (fFrameNumber - pNode->pRotation[nPreviousKey].FrameNumber)/(pNode->pRotation[nNextKey].FrameNumber - pNode->pRotation[nPreviousKey].FrameNumber);

	/* Using linear interpolation, find quaternion at time t between Q1 and Q2 */
	PVRTMatrixQuaternionSlerp(RotationQuaternion, Q1, Q2, t);

	/* Convert quaternion to matrix & return result */
	PVRTMatrixRotationQuaternion(*pmRot, RotationQuaternion);
	PVRTMatrixTranspose(*pmRot, *pmRot);
}

/*!***************************************************************************
 @Function			Normal
 @Input				pV1
 @Input				pV2
 @Input				pV3
 @Output			NormalVect
 @Description		Compute the normal to the triangle defined by the vertices V1,
					V2 and V3.
*****************************************************************************/
static void Normal(float *pV1, float *pV2, float *pV3, float NormalVect[3])
{
	float Vect1[3], Vect2[3], PMod;

    /* Compute triangle vectors */
	Vect1[0] = pV1[0]-pV2[0];   Vect1[1] = pV1[1]-pV2[1];   Vect1[2] = pV1[2]-pV2[2];
    Vect2[0] = pV1[0]-pV3[0];   Vect2[1] = pV1[1]-pV3[1];   Vect2[2] = pV1[2]-pV3[2];

	/* Find cross-product vector of these two vectors */
	NormalVect[0] = (Vect1[1] * Vect2[2]) - (Vect1[2] * Vect2[1]);
	NormalVect[1] = (Vect1[2] * Vect2[0]) - (Vect1[0] * Vect2[2]);
	NormalVect[2] = (Vect1[0] * Vect2[1]) - (Vect1[1] * Vect2[0]);

	/* Compute length of the resulting vector */
    PMod = (float)sqrt(NormalVect[0]*NormalVect[0]+NormalVect[1]*NormalVect[1]+NormalVect[2]*NormalVect[2]);

	/* This is to avoid a division by zero */
    if (PMod < 1e-10f) PMod = 1e-10f;
	PMod = 1.0f / PMod;

	/* Normalize normal vector */
    NormalVect[0] *= PMod;
	NormalVect[1] *= PMod;
	NormalVect[2] *= PMod;
}


/*!***************************************************************************
 @Function			CalculateNormals
 @Modified			pObject
 @Description		Compute vertex normals of submitted vertices array.
*****************************************************************************/
static void CalculateNormals(int nNumVertex, float *pVertex,
							 int nNumFaces, unsigned short *pFaces,
							 float *pNormals)
{
	unsigned short	P1, P2, P3;
	float			pfN[3], fMod, *pfVN;
	int				nIdx;
	int				j, k;

	/* Parameter checking */
	if (!pVertex || !pFaces || !pNormals)
	{
		OutputDebugString("CalculateNormals : Bad parameters\n");
		return;
	}

	/* Use the actual output array for summing face-normal contributions */
	pfVN = pNormals;

	/* Zero normals array */
	ZeroMemory(pfVN, nNumVertex * 3 * sizeof(float));

	/* Sum the components of each face's normal to a vector normal */
	for (j=0; j<3*nNumFaces; j+=3)
	{
		/* Get three points defining a triangle */
		P1 = pFaces[j + 0];
		P2 = pFaces[j + 1];
		P3 = pFaces[j + 2];

		/* Calculate face normal in pfN */
		Normal(&pVertex[3*P1], &pVertex[3*P2], &pVertex[3*P3], pfN);

		/* Add the normal of this triangle to each vertex */
		for (k=0; k<3; k++)
		{
			nIdx = pFaces[j + k];
			pfVN[nIdx*3 + 0]	+= pfN[0];
			pfVN[nIdx*3 + 1]	+= pfN[1];
			pfVN[nIdx*3 + 2]	+= pfN[2];
		}
	}

	/* Normalise each vector normal and set in mesh */
	for (j=0; j<3*nNumVertex; j+=3)
	{
		fMod = (float)sqrt(pfVN[j + 0] * pfVN[j + 0] + pfVN[j + 1] * pfVN[j + 1] + pfVN[j + 2] * pfVN[j + 2]);

		/* Zero length normal? Either point down an axis or leave... */
		if (fMod==0.0f) continue;
		fMod = 1.0f / fMod;

		pNormals[j + 0] *= fMod;
		pNormals[j + 1] *= fMod;
		pNormals[j + 2] *= fMod;
	}
}


/* The following is not used as linear interpolation is used instead */
#if 0
/*!***************************************************************************
 @Function			GetInterpolatedPosition
 @Input				P1
 @Input				P2
 @Input				nFrameNumber
 @Return			Interpolated position
 @Description		Return interpolated position between position key P1 and P2
*****************************************************************************/
static PVRTVECTOR3 GetInterpolatedPosition(int nFrameNumber, NodeType *pNode, int Key1, int Key2)
{
	PVRTVECTOR3	P, P1, P2, R1, R2;
	float		T, C, B, D1, D2, fAdjust;
	float		t;

	/* Parameter checking */
	if (nFrameNumber<pNode->pPosition[Key1].FrameNumber || nFrameNumber>pNode->pPosition[Key2].FrameNumber || Key2!=(Key1+1))
	{
		OutputDebugString("Frame number is not contained in segment\n");
		P.x = 0.0f; P.y = 0.0f; P.z = 0.0f;
		return P;
	}

	/* Compute t, the time corresponding to the frame we want to interpolate */
	t = (float)(nFrameNumber - pNode->pPosition[Key1].FrameNumber)/(pNode->pPosition[Key2].FrameNumber - pNode->pPosition[Key1].FrameNumber);

	/* Get positions for each of the two keys */
	P1 = pNode->pPosition[Key1].p;
	P2 = pNode->pPosition[Key2].p;

	/* Compute P1, P2, R1 and R2 depending on key positions */
	if (Key1==0 && Key2==pNode->PositionKeys-1)
	{
		/******************************************
		** Special case : only two keys in array **
		******************************************/

		/* Compute R1, tangent of P1 */
		T = pNode->pPosition[Key1].fTension;

		R1.x = (P2.x - P1.x)*(1.0f - T);
		R1.y = (P2.y - P1.y)*(1.0f - T);
		R1.z = (P2.z - P1.z)*(1.0f - T);

		/* Compute R2, tangent of P2 */
		T = pNode->pPosition[Key2].fTension;

		R2.x = (P2.x - P1.x)*(1.0f - T);
		R2.y = (P2.y - P1.y)*(1.0f - T);
		R2.z = (P2.z - P1.z)*(1.0f - T);
	}
	else if (Key1==0)
	{
		/***********************************
		** Special case : P1 is first key **
		***********************************/

		/* Compute R2, tangent of P2 */
		T =	pNode->pPosition[Key2].fTension;
		C = pNode->pPosition[Key2].fContinuity;
		B = pNode->pPosition[Key2].fBias;
		D1 = (float)pNode->pPosition[Key2].FrameNumber - pNode->pPosition[Key2-1].FrameNumber;
		D2 = (float)pNode->pPosition[Key2+1].FrameNumber - pNode->pPosition[Key2].FrameNumber;
		fAdjust = D2/(D1+D2);
		fAdjust = 0.5f + (1.0f-(float)fabs(C))*(fAdjust-0.5f);

		R2.x = ( (P2.x - pNode->pPosition[Key2-1].p.x)*(1.0f+B)*(1.0f+C) + (pNode->pPosition[Key2+1].p.x - P2.x)*(1.0f-B)*(1.0f-C) ) * (1.0f-T) * fAdjust;
		R2.y = ( (P2.y - pNode->pPosition[Key2-1].p.y)*(1.0f+B)*(1.0f+C) + (pNode->pPosition[Key2+1].p.y - P2.y)*(1.0f-B)*(1.0f-C) ) * (1.0f-T) * fAdjust;
		R2.z = ( (P2.z - pNode->pPosition[Key2-1].p.z)*(1.0f+B)*(1.0f+C) + (pNode->pPosition[Key2+1].p.z - P2.z)*(1.0f-B)*(1.0f-C) ) * (1.0f-T) * fAdjust;

		/* Compute R1, tangent of P1 */
		T =	pNode->pPosition[Key1].fTension;
		R1.x = ( (P2.x - P1.x)*1.5f - R2.x*0.5f)*(1-T);
		R1.y = ( (P2.y - P1.y)*1.5f - R2.y*0.5f)*(1-T);
		R1.z = ( (P2.z - P1.z)*1.5f - R2.z*0.5f)*(1-T);
	}
	else if (Key2==pNode->PositionKeys-1)
	{
		/**********************************
		** Special case : P2 is last key **
		**********************************/

		/* Compute R1, tangent of P1 */
		T =	pNode->pPosition[Key1].fTension;
		C = pNode->pPosition[Key1].fContinuity;
		B = pNode->pPosition[Key1].fBias;
		D1 = (float)pNode->pPosition[Key1].FrameNumber - pNode->pPosition[Key1-1].FrameNumber;
		D2 = (float)pNode->pPosition[Key1+1].FrameNumber - pNode->pPosition[Key1].FrameNumber;
		fAdjust = D1/(D1+D2);
		fAdjust = 0.5f + (1.0f-(float)fabs(C))*(fAdjust-0.5f);

		R1.x = ( (P1.x - pNode->pPosition[Key1-1].p.x)*(1.0f+B)*(1.0f-C) + (pNode->pPosition[Key1+1].p.x - P1.x)*(1.0f-B)*(1.0f+C) ) * (1.0f-T) * fAdjust;
		R1.y = ( (P1.y - pNode->pPosition[Key1-1].p.y)*(1.0f+B)*(1.0f-C) + (pNode->pPosition[Key1+1].p.y - P1.y)*(1.0f-B)*(1.0f+C) ) * (1.0f-T) * fAdjust;
		R1.z = ( (P1.z - pNode->pPosition[Key1-1].p.z)*(1.0f+B)*(1.0f-C) + (pNode->pPosition[Key1+1].p.z - P1.z)*(1.0f-B)*(1.0f+C) ) * (1.0f-T) * fAdjust;

		/* Compute R2, tangent of P2 */
		T =	pNode->pPosition[Key2].fTension;

		R2.x = ( (P2.x - P1.x)*1.5f - R1.x*0.5f)*(1-T);
		R2.y = ( (P2.y - P1.y)*1.5f - R1.y*0.5f)*(1-T);
		R2.z = ( (P2.z - P1.z)*1.5f - R1.z*0.5f)*(1-T);
	}
	else
	{
		/****************
		** Normal case **
		****************/

		/* Compute R1, tangent of P1 */
		T =	pNode->pPosition[Key1].fTension;
		C = pNode->pPosition[Key1].fContinuity;
		B = pNode->pPosition[Key1].fBias;
		D1 = (float)pNode->pPosition[Key1].FrameNumber - pNode->pPosition[Key1-1].FrameNumber;
		D2 = (float)pNode->pPosition[Key1+1].FrameNumber - pNode->pPosition[Key1].FrameNumber;
		fAdjust = D1/(D1+D2);
		fAdjust = 0.5f + (1.0f-(float)fabs(C))*(fAdjust-0.5f);

		R1.x = ( (P1.x - pNode->pPosition[Key1-1].p.x)*(1.0f+B)*(1.0f-C) + (pNode->pPosition[Key1+1].p.x - P1.x)*(1.0f-B)*(1.0f+C) ) * (1.0f-T) * fAdjust;
		R1.y = ( (P1.y - pNode->pPosition[Key1-1].p.y)*(1.0f+B)*(1.0f-C) + (pNode->pPosition[Key1+1].p.y - P1.y)*(1.0f-B)*(1.0f+C) ) * (1.0f-T) * fAdjust;
		R1.z = ( (P1.z - pNode->pPosition[Key1-1].p.z)*(1.0f+B)*(1.0f-C) + (pNode->pPosition[Key1+1].p.z - P1.z)*(1.0f-B)*(1.0f+C) ) * (1.0f-T) * fAdjust;

		/* Compute R2, tangent of P2 */
		T =	pNode->pPosition[Key2].fTension;
		C = pNode->pPosition[Key2].fContinuity;
		B = pNode->pPosition[Key2].fBias;
		D1 = (float)pNode->pPosition[Key2].FrameNumber - pNode->pPosition[Key2-1].FrameNumber;
		D2 = (float)pNode->pPosition[Key2+1].FrameNumber - pNode->pPosition[Key2].FrameNumber;
		fAdjust = D2/(D1+D2);
		fAdjust = 0.5f + (1.0f-(float)fabs(C))*(fAdjust-0.5f);

		R2.x = ( (P2.x - pNode->pPosition[Key2-1].p.x)*(1.0f+B)*(1.0f+C) + (pNode->pPosition[Key2+1].p.x - P2.x)*(1.0f-B)*(1.0f-C) ) * (1.0f-T) * fAdjust;
		R2.y = ( (P2.y - pNode->pPosition[Key2-1].p.y)*(1.0f+B)*(1.0f+C) + (pNode->pPosition[Key2+1].p.y - P2.y)*(1.0f-B)*(1.0f-C) ) * (1.0f-T) * fAdjust;
		R2.z = ( (P2.z - pNode->pPosition[Key2-1].p.z)*(1.0f+B)*(1.0f+C) + (pNode->pPosition[Key2+1].p.z - P2.z)*(1.0f-B)*(1.0f-C) ) * (1.0f-T) * fAdjust;
	}

	/* Final computation */
	P.x = P1.x*(2.0f*t*t*t-3.0f*t*t+1.0f) + R1.x*(t*t*t-2.0f*t*t+t) + P2.x*(-2.0f*t*t*t+3.0f*t*t) + R2.x*(t*t*t-t*t);
	P.y = P1.y*(2.0f*t*t*t-3.0f*t*t+1.0f) + R1.y*(t*t*t-2.0f*t*t+t) + P2.y*(-2.0f*t*t*t+3.0f*t*t) + R2.y*(t*t*t-t*t);
	P.z = P1.z*(2.0f*t*t*t-3.0f*t*t+1.0f) + R1.z*(t*t*t-2.0f*t*t+t) + P2.z*(-2.0f*t*t*t+3.0f*t*t) + R2.z*(t*t*t-t*t);

	/* Return interpolated position */
	return P;
}
#endif

/*****************************************************************************
 End of file (PVRTModel3DS.cpp)
*****************************************************************************/
