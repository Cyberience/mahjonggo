/******************************************************************************

 @File         PVRTShadowVol.h

 @Title        

 @Copyright    Copyright (C) 2001 - 2006 by Imagination Technologies Limited.

 @Platform     ANSI compatible

 @Description  Declarations of functions relating to shadow volume generation.

******************************************************************************/
#ifndef _PVRTSHADOWVOL_H_
#define _PVRTSHADOWVOL_H_

#include "PVRTContext.h"


/****************************************************************************
** Defines
****************************************************************************/
#define PVRTSHADOWVOLUME_VISIBLE		0x00000001
#define PVRTSHADOWVOLUME_NEED_CAP_FRONT	0x00000002
#define PVRTSHADOWVOLUME_NEED_CAP_BACK	0x00000004
#define PVRTSHADOWVOLUME_NEED_ZFAIL		0x00000008

/****************************************************************************
** Structures
****************************************************************************/
struct PVRTSvMEdge {
	WORD		wV0, wV1;		/*!< Indices of the vertices of the edge */
	int			nVis;			/*!< Bit0 = Visible, Bit1 = Hidden, Bit2 = Reverse Winding */
};

struct PVRTSvMTriangle {
	WORD		w[3];				/*!< Source indices of the triangle */
	PVRTSvMEdge	*pE0, *pE1, *pE2;	/*!< Edges of the triangle */
	PVRTVECTOR3	vNormal;			/*!< Triangle normal */
	int			nWinding;			/*!< BitN = Correct winding for edge N */
};

struct PVRTSvShadowMesh {
	PVRTVECTOR3		*pV;	/*!< Unique vertices in object space */
	PVRTSvMEdge		*pE;
	PVRTSvMTriangle	*pT;
	unsigned int	nV;		/*!< Vertex count */
	unsigned int	nE;		/*!< Edge count */
	unsigned int	nT;		/*!< Triangle count */

#ifdef BUILD_OGLES
	void			*pivb;		/*!< Two copies of the vertices */
#endif
};

/*
	Renderable shadow-volume information:
*/
struct PVRTSvShadowVol {
#ifdef BUILD_OGLES
	WORD					*piib;		/*!< Indices to render the volume */
#endif
	unsigned int			nIdxCnt;	/*!< Number of indices in piib */

#ifdef _DEBUG
	unsigned int			nIdxCntMax;	/*!< Number of indices which can fit in piib */
#endif
};

/****************************************************************************
** Declarations
****************************************************************************/
void PVRTSvMeshCreateMesh(
	PVRTSvShadowMesh		* const psMesh,
	const float				* const pVertex,
	const unsigned int		nNumVertex,
	const unsigned short	* const pFaces,
	const unsigned int		nNumFaces);

BOOL PVRTSvMeshInitMesh(
	PVRTSvShadowMesh		* const psMesh,
	const SPVRTContext		* const pContext);

BOOL PVRTSvMeshInitVol(
	PVRTSvShadowVol			* const psVol,
	const PVRTSvShadowMesh	* const psMesh,
	const SPVRTContext		* const pContext);

void PVRTSvMeshDestroyMesh(
	PVRTSvShadowMesh		* const psMesh);

void PVRTSvMeshReleaseMesh(
	PVRTSvShadowMesh		* const psMesh);

void PVRTSvMeshReleaseVol(
	PVRTSvShadowVol			* const psVol);

void PVRTSvSilhouetteProjectedBuild(
	PVRTSvShadowVol			* const psVol,
	const DWORD				dwVisFlags,
	const PVRTSvShadowMesh	* const psMesh,
	const PVRTVECTOR3		* const pvLightModel,
	const BOOL				bPointLight);

void PVRTSvBoundingBoxExtrude(
	PVRTVECTOR3				* const pvExtrudedCube,
	const PVRTBOUNDINGBOX	* const pBoundingBox,
	const PVRTVECTOR3		* const pvLightMdl,
	const BOOL				bPointLight,
	const float				fVolLength);

void PVRTSvBoundingBoxIsVisible(
	DWORD					* const pdwVisFlags,
	const BOOL				bObVisible,				// Is the object visible?
	const BOOL				bNeedsZClipping,		// Does the object require Z clipping?
	const PVRTBOUNDINGBOX	* const pBoundingBox,
	const PVRTMATRIX		* const pmTrans,
	const PVRTVECTOR3		* const pvLightMdl,
	const BOOL				bPointLight,
	const float				fCamZProj,
	const float				fVolLength);

int PVRTSvSilhouetteProjectedRender(
	const PVRTSvShadowMesh	* const psMesh,
	const PVRTSvShadowVol	* const psVol,
	const SPVRTContext		* const pContext);


#endif /* _PVRTSHADOWVOL_H_ */

/*****************************************************************************
 End of file (PVRTShadowVol.h)
*****************************************************************************/
