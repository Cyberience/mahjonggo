/******************************************************************************

 @File         PVRTTexture.cpp

 @Title        

 @Copyright    Copyright (C) 2004 - 2006 by Imagination Technologies Limited.

 @Platform     ANSI compatible

 @Description  Texture loading.

******************************************************************************/
#include <string.h>
#include <stdlib.h>

#include "PVRTContext.h"
#include "PVRTglesExt.h"
#include "PVRTTexture.h"
#include "PVRTDecompress.h"
#include "PVRTFixedPoint.h"
#include "PVRTMatrix.h"
#include "PVRTMisc.h"

/*****************************************************************************
** Functions
*****************************************************************************/

static void PVRTTextureLoadTiled(
	unsigned char		* const pDst,
	const unsigned int	nWidthDst,
	const unsigned int	nHeightDst,
	const unsigned char	* const pSrc,
	const unsigned int	nWidthSrc,
	const unsigned int	nHeightSrc,
	const unsigned int	nElementSize,		// Bytes per pixel
	const bool			bTwiddled)
{
	unsigned int nXs, nYs;
	unsigned int nXd, nYd;
	unsigned int nIdxSrc, nIdxDst;

	for(nIdxDst = 0; nIdxDst < nWidthDst*nHeightDst; ++nIdxDst)
	{
		if(bTwiddled)
		{
			PVRTMiscDeTwiddle(nXd, nYd, nIdxDst);
		}
		else
		{
			nXd = nIdxDst % nWidthDst;
			nYd = nIdxDst / nWidthDst;
		}

		nXs = nXd % nWidthSrc;
		nYs = nYd % nHeightSrc;

		if(bTwiddled)
		{
			PVRTMiscTwiddle(nIdxSrc, nXs, nYs);
		}
		else
		{
			nIdxSrc = nYs * nWidthSrc + nXs;
		}

		memcpy(pDst + nIdxDst*nElementSize, pSrc + nIdxSrc*nElementSize, nElementSize);
	}
}

/*!***************************************************************************
@Function		PVRTTextureCreate
@Input			w			Size of the texture
@Input			h			Size of the texture
@Input			wMin		Minimum size of a texture level
@Input			hMin		Minimum size of a texture level
@Input			nBPP		Bits per pixel of the format
@Input			bMIPMap		Create memory for MIP-map levels also?
@Return			Allocated texture memory (must be free()d)
@Description	Creates a PVR_Texture_Header structure, including room for
				the specified texture, in memory.
*****************************************************************************/
PVR_Texture_Header *PVRTTextureCreate(
	unsigned int		w,
	unsigned int		h,
	const unsigned int	wMin,
	const unsigned int	hMin,
	const unsigned int	nBPP,
	const bool			bMIPMap)
{
	size_t			len;
	unsigned char	*p;

	len = 0;
	do
	{
		len += max(w, wMin) * max(h, hMin);
		w >>= 1;
		h >>= 1;
	}
	while(bMIPMap && (w || h));

	len = (len * nBPP) / 8;
	len += sizeof(PVR_Texture_Header);

	p = (unsigned char*)malloc(len);
	_ASSERT(p);
	return (PVR_Texture_Header*)p;
}

/*!***************************************************************************
@Function		PVRTTextureTile
@Modified		pOut		The tiled texture in system memory
@Input			pIn			The source texture
@Input			nRepeatCnt	Number of times to repeat the source texture
@Description	Allocates and fills, in system memory, a texture large enough
				to repeat the source texture specified number of times.
*****************************************************************************/
void PVRTTextureTile(
	PVR_Texture_Header			**pOut,
	const PVR_Texture_Header	* const pIn,
	const int					nRepeatCnt)
{
	unsigned int		nFormat = 0, nType = 0, nBPP, nSize, nElW = 0, nElH = 0;
	unsigned char		*pMmSrc, *pMmDst;
	unsigned int		nLevel;
	PVR_Texture_Header	*psTexHeaderNew;

	_ASSERT(pIn->dwWidth);
	_ASSERT(pIn->dwWidth == pIn->dwHeight);
	_ASSERT(nRepeatCnt > 1);

	switch(pIn->dwpfFlags & 0xFF)
	{
	case 17:
		nFormat		= GL_UNSIGNED_SHORT_5_5_5_1;
		nType		= GL_RGBA;
		nElW		= 1;
		nElH		= 1;
		break;
	case 18:
		nFormat		= GL_UNSIGNED_BYTE;
		nType		= GL_RGBA;
		nElW		= 1;
		nElH		= 1;
		break;
	case 24:
		nFormat		= GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG;
		nType		= 0;
		nElW		= 8;
		nElH		= 4;
		break;
	case 25:
		nFormat		= GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG;
		nType		= 0;
		nElW		= 4;
		nElH		= 4;
		break;
	}

	nBPP = PVRTTextureFormatBPP(nFormat, nType);
	nSize = pIn->dwWidth * nRepeatCnt;

	psTexHeaderNew	= PVRTTextureCreate(nSize, nSize, nElW, nElH, nBPP, true);
	*psTexHeaderNew	= *pIn;
	pMmDst	= (unsigned char*)psTexHeaderNew + sizeof(*psTexHeaderNew);
	pMmSrc	= (unsigned char*)pIn + sizeof(*pIn);

	for(nLevel = 0; ((unsigned int)1 << nLevel) < nSize; ++nLevel)
	{
		int nBlocksDstW = max(1, (nSize >> nLevel) / nElW);
		int nBlocksDstH = max(1, (nSize >> nLevel) / nElH);
		int nBlocksSrcW = max(1, (pIn->dwWidth >> nLevel) / nElW);
		int nBlocksSrcH = max(1, (pIn->dwHeight >> nLevel) / nElH);
		int nBlocksS	= nBPP * nElW * nElH / 8;

		PVRTTextureLoadTiled(
			pMmDst,
			nBlocksDstW,
			nBlocksDstH,
			pMmSrc,
			nBlocksSrcW,
			nBlocksSrcH,
			nBlocksS,
			(pIn->dwpfFlags & 0x00000200) ? true : false);

		pMmDst += nBlocksDstW * nBlocksDstH * nBlocksS;
		pMmSrc += nBlocksSrcW * nBlocksSrcH * nBlocksS;
	}

	psTexHeaderNew->dwWidth = nSize;
	psTexHeaderNew->dwHeight = nSize;
	psTexHeaderNew->dwMipMapCount = nLevel;
	*pOut = psTexHeaderNew;
}

/*!***************************************************************************
 @Function		PVRTLoadDecompressedTextureFromPointer
 @Modified		texName			the OpenGL ES texture name as returned by glBindTexture
 @Input			pointer			Pointer to the header-prefixed texture from
 @Return		true on success
 @Description	Allows textures to be stored in header files and loaded in. Loads the whole texture
				Release texture by calling PVRTReleaseTexture.  Decompresses to RGBA8888 internally.
*****************************************************************************/
bool PVRTLoadDecompressedTextureFromPointer(void* pointer, GLuint *texName)
{
	return PVRTLoadDecompressedPartialTextureFromPointer(pointer, 0, texName);
}

/*!***************************************************************************
 @Function		PVRTLoadDecompressedPartialTextureFromPointer
 @Input			pointer			Pointer to the header-prefixed texture from
 @Input			nLoadFromLevel	Which mipmap level to start loading from (0=all)
 @Modified		texName			the OpenGL ES texture name as returned by glBindTexture
 @Return		true on success
 @Description	Allows textures to be stored in header files and loaded in. Loads the whole texture
				Release texture by calling PVRTReleaseTexture.  Decompresses to RGBA8888 internally.
*****************************************************************************/
bool PVRTLoadDecompressedPartialTextureFromPointer(void *pointer, unsigned int nLoadFromLevel, GLuint *texName)
{
	//Malloc off a duplicate pointer for the header _only_
	PVR_Texture_Header *oldHeader = (PVR_Texture_Header*)pointer;

	if(((oldHeader->dwpfFlags&0xFF) != 24) && ((oldHeader->dwpfFlags&0xFF) != 25))
	{
		return PVRTLoadPartialTextureFromPointer(pointer, 0, 0, texName);
	}

	PVR_Texture_Header *newHeader = (PVR_Texture_Header*)malloc(sizeof(PVR_Texture_Header));
	memcpy(newHeader,pointer,sizeof(PVR_Texture_Header));

	//Change the decompressed texture header's format to be RGBA8888, drop top mip level
	newHeader->dwpfFlags = 18;//RGBA8888
	newHeader->dwpfFlags |= (oldHeader->dwpfFlags & 0x00000100);
	newHeader->dwMipMapCount--;
	newHeader->dwBitCount = 32;
	newHeader->dwWidth /= 2;
	newHeader->dwHeight /= 2;
	int newSize = 0;

	//Malloc width*height*miplevels*4 bytes
	int szx = newHeader->dwWidth;
	int szy = newHeader->dwHeight;
	int nMips = 1;
	while(szx && szy){
		newSize += 4*szx*szy;
		szx>>=1;
		szy>>=1;
		nMips++;
	}

	unsigned char *newTexture = (unsigned char*)malloc(newSize);

	//Decompress each texture layer into the new memory
	szx = newHeader->dwWidth;
	szy = newHeader->dwHeight;
	unsigned char *thisMipLevelSrc = (unsigned char*)pointer+sizeof(PVR_Texture_Header)+
		(oldHeader->dwWidth*oldHeader->dwHeight*oldHeader->dwBitCount + 7)/8;
	unsigned char *thisMipLevelDest = newTexture;
	while(szx && szy){
		PVRTCDecompress(thisMipLevelSrc,
				((oldHeader->dwpfFlags&0xFF) == 24) ? 1 : 0,
				szx,
				szy,
				thisMipLevelDest);
		thisMipLevelSrc += (szx * szy * oldHeader->dwBitCount + 7)/8;
		thisMipLevelDest += (szx * szy * 4);
		szx>>=1;
		szy>>=1;
	}

	//Load the textures
	bool result = PVRTLoadPartialTextureFromPointer(newHeader,newTexture, 0, texName);

	//Bin the temporary header and memory
	free(newHeader);
	free(newTexture);

	return result;
}

/*!***************************************************************************
 @Function		PVRTLoadPartialTextureFromPointer
 @Input			pointer			Pointer to header-texture's structure
 @Input			texPtr			If null, texture follows header, else texture is here.
 @Input			nLoadFromLevel	Which mipmap level to start loading from (0=all)
 @Modified		texName			the OpenGL ES texture name as returned by glBindTexture
 @Return		true on success
 @Description	Allows textures to be stored in C header files and loaded in.  Can load parts of a
				mipmaped texture (ie skipping the highest detailed levels).  Release texture by calling
				PVRTReleaseTexture.
*****************************************************************************/
bool PVRTLoadPartialTextureFromPointer(void* pointer, void *texPtr, unsigned int nLoadFromLevel, GLuint *texName)
{
	PVR_Texture_Header* header = (PVR_Texture_Header*)pointer;

	GLuint textureName;
	GLenum textureFormat = 0;
	GLenum textureType = GL_RGB;
	unsigned char alphaBitsToSwap = 0;
	int err = 0;

	bool IsPVRTCSupported = CPVRTglesExt::IsGLExtensionSupported("GL_IMG_texture_compression_pvrtc");

	*texName = 0;	// install warning value

	/* Only accept untwiddled data UNLESS texture format is PVRTC */
	if ( ((header->dwpfFlags & 0x200) == 0x200) && ((header->dwpfFlags & 0xFF)!=0x18) && ((header->dwpfFlags & 0xFF)!=0x19) )
	{
		// We need to load untwiddled textures -- hw will twiddle for us.
		err = ShellTextureLoadErr_TwiddledTexture;
		goto bail;
	}

	switch(header->dwpfFlags & 0xFF)					// FORMAT		NEEDS SWAP
	{													// ------		----------
	case 16:
		textureFormat = GL_UNSIGNED_SHORT_4_4_4_4;		// RGBA4444
		textureType = GL_RGBA;
		break;

	case 17:
		textureFormat = GL_UNSIGNED_SHORT_5_5_5_1;		// RGBA5551
		textureType = GL_RGBA;
		break;

	case 18:
		textureFormat = GL_UNSIGNED_BYTE;				// RGBA8888
		textureType = GL_RGBA;
		break;

	/* New OGL Specific Formats Added */

	case 19:
		textureFormat = GL_UNSIGNED_SHORT_5_6_5;		// RGB565
		textureType = GL_RGB;
		break;

	case 20:
		err=1; // Not supported format 555
		break;

	case 21:
		textureFormat = GL_UNSIGNED_BYTE;				// RGB888
		textureType = GL_RGB;
		break;

	case 22:
		textureFormat = GL_UNSIGNED_BYTE;				// I8
		textureType = GL_LUMINANCE;
		break;

	case 23:
		textureFormat = GL_UNSIGNED_BYTE;
		textureType = GL_LUMINANCE_ALPHA;
		break;

	case 24:
		if(IsPVRTCSupported)
			textureFormat = header->dwAlphaBitMask==0 ? GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG : GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG ;	// PVRTC2
		else
			err = 1; // Deal with exceptional case
		break;

	case 25:
		if(IsPVRTCSupported)
			textureFormat = header->dwAlphaBitMask==0 ? GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG : GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG ;	// PVRTC4
		else
			err = 1; // Deal with exceptional case
		break;

	default:											// NOT SUPPORTED
		err = 1;
		break;
	}

	if(err)
	{
		if (((header->dwpfFlags & 0xFF)<=8) || (header->dwpfFlags & 0xFF)==12 || (header->dwpfFlags & 0xFF)==13)
		{
//			PVRShellOutputDebug("PVRShellLoadPartialTextureFromHeader() Non-OGL Specific texture format - please re-export using OGL version of texture format\n");
		}
		else
		{
//            PVRShellOutputDebug("PVRShellLoadPartialTextureFromHeader() called for unsupported texture format\n");
		}
		goto bail;
	}

	// load the texture up

	glPixelStorei(GL_UNPACK_ALIGNMENT,1);				// Never have row-aligned in headers

	glGenTextures(1, &textureName);
	glBindTexture(GL_TEXTURE_2D, textureName);

	err = glGetError();
	if(err) goto bail;

	err = glGetError();
	if(err) goto bail;

	{			// all in a block to avoid errors from vc6
		char *theTexturePtr = texPtr? (char*)texPtr :  (char*)header + header->dwHeaderSize;
		char *theTextureToLoad = 0;
		int		nMIPMapLevel;
		int		nTextureLevelsNeeded = (header->dwpfFlags & 0x00000100)? header->dwMipMapCount : 0;
		unsigned int		nSizeX= header->dwWidth, nSizeY = header->dwHeight;
		unsigned int		CompressedImageSize = 0;
		for(nMIPMapLevel = 0; nMIPMapLevel <= nTextureLevelsNeeded; nSizeX=max(nSizeX/2, 1), nSizeY=max(nSizeY/2, 1), nMIPMapLevel++)
		{
			// Do Alpha-swap if needed

			theTextureToLoad = theTexturePtr;
			if(alphaBitsToSwap)
			{
				char *theTextureConverted = 0;

				switch(alphaBitsToSwap)
				{
				case 1:
					{
						unsigned short * dstPixPtr = (unsigned short*)
							malloc( (((header->dwBitCount/8) + (header->dwBitCount%8))) * nSizeX * nSizeY);
						unsigned short * srcPixPtr = (unsigned short*) theTexturePtr;
						theTextureConverted = (char*) dstPixPtr;

						if(!dstPixPtr)
						{
//							PVRShellOutputDebug("PVRShellLoadPartialTextureFromHeader() could not allocate enough memory\n");
							err = 1;
							goto bail;
						}

						for(unsigned int y = 0; y < nSizeY; y++)
						{
							for(unsigned int x = 0; x < nSizeX; x++)
							{
								unsigned short pix = *srcPixPtr++;
								*dstPixPtr++ = ((pix << 1) | (pix >> 15));
							}
						}
					}
					break;

				case 4:
					{
						unsigned short * dstPixPtr = (unsigned short*)
							malloc( (((header->dwBitCount)/8) + (header->dwBitCount%8)) * nSizeX * nSizeY);
						unsigned short * srcPixPtr = (unsigned short*) theTexturePtr;
						theTextureConverted = (char*) dstPixPtr;

						if(!dstPixPtr)
						{
//							PVRShellOutputDebug("PVRShellLoadPartialTextureFromHeader() could not allocate enough memory\n");
							err = 1;
							goto bail;
						}

						for(unsigned int y = 0; y < nSizeY; y++)
						{
							for(unsigned int x = 0; x < nSizeX; x++)
							{
								unsigned short pix = *srcPixPtr++;
								*dstPixPtr++ = ((pix << 4) | (pix >> 12));
							}
						}
					}
					break;

				case 8:
					{
						unsigned long * dstPixPtr = (unsigned long*)
							malloc( (((header->dwBitCount)/8) + (header->dwBitCount%8)) * nSizeX * nSizeY);
						unsigned long * srcPixPtr = (unsigned long*) theTexturePtr;
						theTextureConverted = (char*) dstPixPtr;

						if(!dstPixPtr)
						{
//							PVRShellOutputDebug("PVRShellLoadPartialTextureFromHeader() could not allocate enough memory\n");
							err = 1;
							goto bail;
						}

						for(unsigned int y = 0; y < nSizeY; y++)
						{
							for(unsigned int x = 0; x < nSizeX; x++)
							{
								unsigned long pix = *srcPixPtr++;
								*dstPixPtr++ = (pix&0xFF00FF00) | ((pix&0x00FF0000) >> 16) | ((pix&0x000000FF) << 16);
							}
						}
					}
					break;
				}

				theTextureToLoad = theTextureConverted;
			}

			// Load the Texture

			/* If the texture is PVRTC then use GLCompressedTexImage2D */
			if ( (((header->dwpfFlags & 0xFF)==0x18) || ((header->dwpfFlags & 0xFF)==0x19))  && IsPVRTCSupported )
			{
				/* Calculate how many bytes this MIP level occupies */
				if ((header->dwpfFlags & 0xFF)==0x18)
				{
					/* PVRTC2 case */
                    CompressedImageSize = ( max(nSizeX, 16) * max(nSizeY, 8) * header->dwBitCount + 7) / 8;
				}
				else
				{
					/* PVRTC4 case */
                    CompressedImageSize = ( max(nSizeX, 8) * max(nSizeY, 8) * header->dwBitCount + 7) / 8;
				}

				if(((signed int)nMIPMapLevel - (signed int)nLoadFromLevel) >= 0)
				{
					/* Load compressed texture data at selected MIP level */
					glCompressedTexImage2D(GL_TEXTURE_2D, nMIPMapLevel-nLoadFromLevel, textureFormat, nSizeX, nSizeY, 0,
										CompressedImageSize, theTextureToLoad);
				}
			}
			else
			{
				if(((signed int)nMIPMapLevel - (signed int)nLoadFromLevel) >= 0)
				{
					/* Load uncompressed texture data at selected MIP level */
					glTexImage2D(GL_TEXTURE_2D,nMIPMapLevel-nLoadFromLevel,textureType,nSizeX,nSizeY,0, textureType,textureFormat,theTextureToLoad);
				}
			}

			if(alphaBitsToSwap)
			{
				free(theTextureToLoad);
			}

			err = glGetError();
			if(err) goto bail;

			// offset the texture pointer by one mip-map level

			/* PVRTC case */
			if ( ((header->dwpfFlags & 0xFF)==0x18) || ((header->dwpfFlags & 0xFF)==0x19) )
			{
				theTexturePtr += CompressedImageSize;
			}
			else
			{
				/* Uncompressed case */
				//theTexturePtr += ((((header->dwBitCount)/8) + (header->dwBitCount%8)) * nSizeX * nSizeY);

				/* New formula that takes into account bit counts inferior to 8 (e.g. 1 bpp) */
				theTexturePtr += (nSizeX * nSizeY * header->dwBitCount + 7) / 8;
			}
		}
	}

	*texName = textureName;

bail:
	if(err)
	{
//		PVRShellOutputDebug("PVRTLoadPartialTextureFromPointer() failed\n");
	}
	return err?false:true;
}

/*!***************************************************************************
 @Function		PVRTLoadTextureFromPointer
 @Input			pointer			Pointer to header-texture's structure
 @Modified		texName			the OpenGL ES texture name as returned by glBindTexture
 @Return		true on success
 @Description	Allows textures to be stored in C header files and loaded in.  Loads the whole texture.
				Release texture by calling PVRTReleaseTexture.
*****************************************************************************/
bool PVRTLoadTextureFromPointer(void* pointer, GLuint *texName)
{
	return PVRTLoadPartialTextureFromPointer(pointer,0 , 0, texName);
}


/*!***************************************************************************
 @Function		PVRTLoadPartialTextureFromPVR
 @Input			filename		Filename of the .PVR file to load the texture from
 @Input			altHeader		If null, texture follows header, else texture is here.
 @Input			nLoadFromLevel	Which mipmap level to start loading from (0=all)
 @Modified		texName			the OpenGL ES texture name as returned by glBindTexture
 @Return		true on success
 @Description	Allows textures to be stored in binary PVR files and loaded in. Can load parts of a
				mipmaped texture (ie skipping the highest detailed levels). Release texture by
				calling PVRTReleaseTexture.
*****************************************************************************/
bool PVRTLoadPartialTextureFromPVR(char* filename, char *altHeader, unsigned int nLoadFromLevel, GLuint *texName)
{
	FILE* file;
	PVR_Texture_Header* header;
	*texName = 0;	// install warning value

	file = (FILE *)fopen(filename, "rb");
	if (!file) return false;

	/* Read the header */
	header = new PVR_Texture_Header;
	fread(header, sizeof(PVR_Texture_Header), 1, file);

	/* Copy the header in a bigger buffer */
	unsigned long* allData = new unsigned long[ (sizeof(PVR_Texture_Header) + header->dwTextureDataSize) / 4 ];
	memcpy(allData, header, sizeof(PVR_Texture_Header));

	/* Read the data from the file in the big buffer after the header */
	fread(allData+(sizeof(PVR_Texture_Header)/4), 4, header->dwTextureDataSize/4, file);

	/* Call PVRTLoadPartialTextureFromPointer on the big buffer */
	bool res = PVRTLoadPartialTextureFromPointer(allData,altHeader?altHeader:0, nLoadFromLevel, texName);

	/* Cleans everything up */
	delete [] allData;
	delete header;
	fclose(file);
	return res;
}

/*!***************************************************************************
 @Function		PVRTLoadTextureFromPVR
 @Input			filename		Filename of the .PVR file to load the texture from
 @Modified		texName			the OpenGL ES texture name as returned by glBindTexture
 @Return		true on success
 @Description	Allows textures to be stored in binary PVR files and loaded in. Loads the whole texture
				Release texture by calling PVRTReleaseTexture.
*****************************************************************************/
bool PVRTLoadTextureFromPVR(char* filename, GLuint *texName)
{
	return PVRTLoadPartialTextureFromPVR(filename, 0, 0, texName);
}

/*!***************************************************************************
 @Function		PVRTLoadDecompressedPartialTextureFromPVR
 @Input			filename		Filename of the .PVR file to load the texture from
 @Input			nLoadFromLevel	Which mipmap level to start loading from (0=all)
 @Modified		texName			the OpenGL ES texture name as returned by glBindTexture
 @Return		true on success
 @Description	Allows textures to be stored in binary PVR files and loaded in. Can load parts of a
				mipmaped texture (ie skipping the highest detailed levels). Release texture by
				calling PVRTReleaseTexture.  This variant decompresses to RGBA8888.
*****************************************************************************/
bool PVRTLoadDecompressedPartialTextureFromPVR(char* filename, unsigned int nLoadFromLevel, GLuint *texName)
{
	FILE* file;
	PVR_Texture_Header* header;
	*texName = 0;	// install warning value

	file = (FILE *)fopen(filename, "rb");
	if (!file) return false;

	/* Read the header */
	header = new PVR_Texture_Header;
	fread(header, sizeof(PVR_Texture_Header), 1, file);

	/* Copy the header in a bigger buffer */
	unsigned long* allData = new unsigned long[ (sizeof(PVR_Texture_Header) + header->dwTextureDataSize) / 4 ];
	memcpy(allData, header, sizeof(PVR_Texture_Header));

	/* Read the data from the file in the big buffer after the header */
	fread(allData+(sizeof(PVR_Texture_Header)/4), 4, header->dwTextureDataSize/4, file);

	/* Call PVRTLoadPartialTextureFromPointer on the big buffer */
	bool res = PVRTLoadDecompressedPartialTextureFromPointer(allData, nLoadFromLevel, texName);

	/* Cleans everything up */
	delete [] allData;
	delete header;
	fclose(file);
	return res;
}

/*!***************************************************************************
 @Function		PVRTLoadDecompressedTextureFromPVR
 @Input			filename		Filename of the .PVR file to load the texture from
 @Modified		texName			the OpenGL ES texture name as returned by glBindTexture
 @Return		true on success
 @Description	Allows textures to be stored in binary PVR files and loaded in. Loads the whole texture
				Release texture by calling PVRTReleaseTexture.  This variant decompresses to RGBA8888.
*****************************************************************************/
bool PVRTLoadDecompressedTextureFromPVR(char* filename, GLuint *texName)
{
	return PVRTLoadDecompressedPartialTextureFromPVR(filename, 0, texName);
}

/*!***************************************************************************
 @Function		PVRTReleaseTexture
 @Input			texName			the name returned by PVRTLoadTextureFromPointer or
								PVRTLoadTextureFromPVR
 @Description	Releases the resources used by a texture.
*****************************************************************************/
void PVRTReleaseTexture(GLuint texName)
{
	glDeleteTextures(1,&texName);
}

/*!***************************************************************************
 @Function			PVRTTextureFormatBPP
 @Input				nFormat
 @Input				nType
 @Description		Returns the bits per pixel (BPP) of the format.
*****************************************************************************/
unsigned int PVRTTextureFormatBPP(const GLuint nFormat, const GLuint nType)
{
	switch(nFormat)
	{
	case GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG:
	case GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG:
		return 2;
	case GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG:
	case GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG:
		return 4;
	case GL_UNSIGNED_BYTE:
		switch(nType)
		{
		case GL_RGBA:
			return 32;
		}
	case GL_UNSIGNED_SHORT_5_5_5_1:
		switch(nType)
		{
		case GL_RGBA:
			return 16;
		}
	}

	return 0xFFFFFFFF;
}

/*****************************************************************************
 End of file (PVRTTexture.cpp)
*****************************************************************************/
