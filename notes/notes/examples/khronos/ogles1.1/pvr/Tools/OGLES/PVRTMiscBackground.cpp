/******************************************************************************

 @File         PVRTMiscBackground.cpp

 @Title        

 @Copyright    Copyright (C) 1999 - 2006 by Imagination Technologies Limited.

 @Platform     ANSI compatible

 @Description  Function to draw a background texture.

******************************************************************************/
#include <string.h>
#include "PVRTContext.h"

#include "PVRTFixedPoint.h"
#include "PVRTMatrix.h"
#include "PVRTMiscBackground.h"

/*!***************************************************************************
 @Function		PVRTMiscDrawBackground
 @Input			uiTexture	Texture to use
 @Input			bRotate		true to rotate texture 90 degrees.
 @Description	Draws a texture on a quad recovering the whole screen.
*****************************************************************************/
void PVRTMiscDrawBackground(GLuint uiTexture, bool bRotate)
{
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, uiTexture);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);

	// Store matrices and set them to Identity
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	if (bRotate)
		myglRotate(f2vt(90), f2vt(0), f2vt(0), f2vt(1));

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	VERTTYPE verts[] = { f2vt(-1.0f),f2vt(-1.0f),f2vt(1.0f),  f2vt(1.0f),f2vt(-1.0f),f2vt(1.0f),  f2vt(-1.0f),f2vt(1.0f),f2vt(1.0f),  f2vt(1.0f),f2vt(1.0f),f2vt(1.0f) };
	VERTTYPE texcoords[] =	{ f2vt(0.0f),f2vt(0.0f),  f2vt(1.0f),f2vt(0.0f),  f2vt(0.0f),f2vt(1.0f),  f2vt(1.0f),f2vt(1.0f) };

	glDisableClientState(GL_COLOR_ARRAY);

	/* Set vertex data */
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3,VERTTYPEENUM,0,verts);

	/* Set texture coordinates */
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2,VERTTYPEENUM,0,texcoords);

	/* Render geometry */
	glDrawArrays(GL_TRIANGLE_STRIP,0,4);

	/* Disable client states */
	glDisableClientState(GL_COLOR_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);

	/* Recover matrices */
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}

/*****************************************************************************
 End of file (PVRTMiscBackground.cpp)
*****************************************************************************/
