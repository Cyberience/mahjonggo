/******************************************************************************

 @File         PVRTTexture.h

 @Title        

 @Copyright    Copyright (C) 2004 - 2006 by Imagination Technologies Limited.

 @Platform     ANSI compatible

 @Description  GL texture loading.

******************************************************************************/
#ifndef _PVRTTEXTURE_H_
#define _PVRTTEXTURE_H_


/*!***************************************************************************
 Describes the header of a PVR header-texture
*****************************************************************************/
typedef struct PVR_Header_Texture_TAG
{
	unsigned long dwHeaderSize;			/*!< size of the structure */
	unsigned long dwHeight;				/*!< height of surface to be created */
	unsigned long dwWidth;				/*!< width of input surface */
	unsigned long dwMipMapCount;		/*!< number of mip-map levels requested */
	unsigned long dwpfFlags;			/*!< pixel format flags */
	unsigned long dwTextureDataSize;	/*!< Total size in bytes */
	unsigned long dwBitCount;			/*!< number of bits per pixel  */
	unsigned long dwRBitMask;			/*!< mask for red bit */
	unsigned long dwGBitMask;			/*!< mask for green bits */
	unsigned long dwBBitMask;			/*!< mask for blue bits */
	unsigned long dwAlphaBitMask;		/*!< mask for alpha channel */
} PVR_Texture_Header;

enum{
	ShellTextureLoadErr_UnsupportedTexture = 1,
	ShellTextureLoadErr_TwiddledTexture
};


/*****************************************************************************
** Functions
*****************************************************************************/

/*!***************************************************************************
@Function		PVRTTextureCreate
@Input			w			Size of the texture
@Input			h			Size of the texture
@Input			wMin		Minimum size of a texture level
@Input			hMin		Minimum size of a texture level
@Input			nBPP		Bits per pixel of the format
@Input			bMIPMap		Create memory for MIP-map levels also?
@Return			Allocated texture memory (must be free()d)
@Description	Creates a PVR_Texture_Header structure, including room for
				the specified texture, in memory.
*****************************************************************************/
PVR_Texture_Header *PVRTTextureCreate(
	unsigned int		w,
	unsigned int		h,
	const unsigned int	wMin,
	const unsigned int	hMin,
	const unsigned int	nBPP,
	const bool			bMIPMap);

/*!***************************************************************************
@Function		PVRTTextureTile
@Modified		pOut		The tiled texture in system memory
@Input			pIn			The source texture
@Input			nRepeatCnt	Number of times to repeat the source texture
@Description	Allocates and fills, in system memory, a texture large enough
				to repeat the source texture specified number of times.
*****************************************************************************/
void PVRTTextureTile(
	PVR_Texture_Header			**pOut,
	const PVR_Texture_Header	* const pIn,
	const int					nRepeatCnt);

/*!***************************************************************************
 @Function		PVRTLoadDecompressedTextureFromPointer
 @Modified		texName			the OpenGL ES texture name as returned by glBindTexture
 @Input			pointer			Pointer to the header-prefixed texture from
 @Return		true on success
 @Description	Allows textures to be stored in header files and loaded in. Loads the whole texture
				Release texture by calling PVRTReleaseTexture.  Decompresses to RGBA8888 internally.
*****************************************************************************/
bool PVRTLoadDecompressedTextureFromPointer(void* pointer, GLuint *texName);

/*!***************************************************************************
 @Function		PVRTLoadDecompressedPartialTextureFromPointer
 @Input			pointer			Pointer to the header-prefixed texture from
 @Input			nLoadFromLevel	Which mipmap level to start loading from (0=all)
 @Modified		texName			the OpenGL ES texture name as returned by glBindTexture
 @Return		true on success
 @Description	Allows textures to be stored in header files and loaded in. Loads the whole texture
				Release texture by calling PVRTReleaseTexture.  Decompresses to RGBA8888 internally.
*****************************************************************************/
bool PVRTLoadDecompressedPartialTextureFromPointer(void *pointer, unsigned int nLoadFromLevel, GLuint *texName);

/*!***************************************************************************
 @Function		PVRTLoadPartialTextureFromPointer
 @Input			pointer			Pointer to header-texture's structure
 @Input			texPtr			If null, texture follows header, else texture is here.
 @Input			nLoadFromLevel	Which mipmap level to start loading from (0=all)
 @Modified		texName			the OpenGL ES texture name as returned by glBindTexture
 @Return		true on success
 @Description	Allows textures to be stored in C header files and loaded in.  Can load parts of a
				mipmaped texture (ie skipping the highest detailed levels).  Release texture by calling
				PVRTReleaseTexture.
*****************************************************************************/
bool PVRTLoadPartialTextureFromPointer(void* pointer, void *texPtr, unsigned int nLoadFromLevel, GLuint *texName);

/*!***************************************************************************
 @Function		OGLESShellLoadTextureFromPointer
 @Input			pointer			Pointer to header-texture's structure
 @Modified		texName			the OpenGL ES texture name as returned by glBindTexture
 @Return		true on success
 @Description	Allows textures to be stored in C header files and loaded in.  Loads the whole texture.
				Release texture by calling PVRTReleaseTexture.
*****************************************************************************/
bool PVRTLoadTextureFromPointer(void* pointer, GLuint *texName);


/*!***************************************************************************
 @Function		PVRTLoadPartialTextureFromPVR
 @Input			filename		Filename of the .PVR file to load the texture from
 @Input			altHeader		If null, texture follows header, else texture is here.
 @Input			nLoadFromLevel	Which mipmap level to start loading from (0=all)
 @Modified		texName			the OpenGL ES texture name as returned by glBindTexture
 @Return		true on success
 @Description	Allows textures to be stored in binary PVR files and loaded in. Can load parts of a
				mipmaped texture (ie skipping the highest detailed levels). Release texture by
				calling PVRTReleaseTexture.
*****************************************************************************/
bool PVRTLoadPartialTextureFromPVR(char* filename, char *altHeader, unsigned int nLoadFromLevel, GLuint *texName);

/*!***************************************************************************
 @Function		PVRTLoadTextureFromPVR
 @Input			filename		Filename of the .PVR file to load the texture from
 @Modified		texName			the OpenGL ES texture name as returned by glBindTexture
 @Return		true on success
 @Description	Allows textures to be stored in binary PVR files and loaded in. Loads the whole texture
				Release texture by calling PVRTReleaseTexture.
*****************************************************************************/
bool PVRTLoadTextureFromPVR(char* filename, GLuint *texName);

/*!***************************************************************************
 @Function		PVRTLoadDecompressedPartialTextureFromPVR
 @Input			filename		Filename of the .PVR file to load the texture from
 @Input			nLoadFromLevel	Which mipmap level to start loading from (0=all)
 @Modified		texName			the OpenGL ES texture name as returned by glBindTexture
 @Return		true on success
 @Description	Allows textures to be stored in binary PVR files and loaded in. Can load parts of a
				mipmaped texture (ie skipping the highest detailed levels). Release texture by
				calling PVRTReleaseTexture.  This variant decompresses to RGBA8888.
*****************************************************************************/
bool PVRTLoadDecompressedPartialTextureFromPVR(char* filename, unsigned int nLoadFromLevel, GLuint *texName);

/*!***************************************************************************
 @Function		PVRTLoadDecompressedTextureFromPVR
 @Input			filename		Filename of the .PVR file to load the texture from
 @Modified		texName			the OpenGL ES texture name as returned by glBindTexture
 @Return		true on success
 @Description	Allows textures to be stored in binary PVR files and loaded in. Loads the whole texture
				Release texture by calling PVRTReleaseTexture.  This variant decompresses to RGBA8888.
*****************************************************************************/
bool PVRTLoadDecompressedTextureFromPVR(char* filename, GLuint *texName);

/*!***************************************************************************
 @Function		PVRTReleaseTexture
 @Input			texName			the name returned by PVRTLoadTextureFromPointer or
								PVRTLoadTextureFromPVR
 @Description	Releases the resources used by a texture.
*****************************************************************************/
void PVRTReleaseTexture(GLuint texName);

/*!***************************************************************************
 @Function			PVRTTextureFormatBPP
 @Input				nFormat
 @Input				nType
 @Description		Returns the bits per pixel (BPP) of the format.
*****************************************************************************/
unsigned int PVRTTextureFormatBPP(const GLuint nFormat, const GLuint nType);


#endif /* _PVRTTEXTURE_H_ */

/*****************************************************************************
 End of file (PVRTTexture.h)
*****************************************************************************/
