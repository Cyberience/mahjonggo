/******************************************************************************

 @File         PVRTContext.h

 @Title        

 @Copyright    Copyright (C) 2004 - 2006 by Imagination Technologies Limited.

 @Platform     ANSI compatible

 @Description  Context specific stuff - i.e. 3D API-related.

******************************************************************************/
#ifndef _PVRTCONTEXT_H_
#define _PVRTCONTEXT_H_

#ifndef IW_USE_SYSTEM_STDLIB
	#define IW_USE_SYSTEM_STDLIB
#endif

#include <new>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <GLES/egl.h>
#include <GLES/gl.h>

#ifndef min
 #define min(x,y) ( ((x) < (y)) ? (x) : (y) )
#endif

#ifndef max
 #define max(x,y) ( ((x) > (y)) ? (x) : (y) )
#endif

/****************************************************************************
** Macros
****************************************************************************/
#define FREE(X)		if(X) { free(X); (X) = 0; }

/****************************************************************************
** Defines
****************************************************************************/

#ifdef IW_DEBUG
#include <assert.h>
#define _CRT_WARN 0
#undef _RPT0
#undef _RPT1
#undef _RPT2
#undef _RPT3
#undef _RPT4
#undef _ASSERTE
#define _RPT0(a,b) printf(b)
#define _RPT1(a,b,c) printf(b, c)
#define _RPT2(a,b,c,d) printf(b,c,d)
#define _RPT3(a,b,c,d,e) printf(b,c,d,e)
#define _RPT4(a,b,c,d,e,f) printf(b,c,d,f)
#ifndef _ASSERT
#define _ASSERT(X) assert(X)
#endif
#define _ASSERTE(X) _ASSERT(X)
#else
#define _CRT_WARN 0
#define _RPT0(a,b)
#define _RPT1(a,b,c)
#define _RPT2(a,b,c,d)
#define _RPT3(a,b,c,d,e)
#define _RPT4(a,b,c,d,e,f)
#ifdef _ASSERT
#undef _ASSERT
#endif
#define _ASSERT(X) /* */
#define _ASSERTE(X) /* */
#endif

/****************************************************************************
** Enumerations
****************************************************************************/

/****************************************************************************
** Structures
****************************************************************************/
struct SPVRTContext
{
	int reserved;	// No context info for OGL.
};

/****************************************************************************
** Functions
****************************************************************************/


#endif /* _PVRTCONTEXT_H_ */

/*****************************************************************************
 End of file (PVRTContext.h)
*****************************************************************************/
