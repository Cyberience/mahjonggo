/******************************************************************************

 @File         PVRTPrint3D.cpp

 @Title        

 @Copyright    Copyright (C) 2000 - 2006 by Imagination Technologies Limited.

 @Platform     ANSI compatible

 @Description  Displays a text string using 3D polygons. Can be done in two ways:
               using a window defined by the user or writing straight on the
               screen.

******************************************************************************/
#include <stdarg.h>

/****************************************************************************
** Includes
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "PVRTContext.h"
#include "PVRTFixedPoint.h"
#include "PVRTMatrix.h"
#include "PVRTPrint3D.h"
#include "PVRTPrint3Ddat.h"		/* Print3D texture data */
#include "PVRTglesExt.h"

/****************************************************************************
** Defines
****************************************************************************/
#define MAX_LETTERS				(5120)
#define MIN_CACHED_VTX			(0x1000)
#define MAX_CACHED_VTX			(0x00100000)
#define MAX_RENDERABLE_LETTERS	(0xFFFF >> 2)
#define MAX_WINDOWS				(512)
#define LINES_SPACING			(29.0f)

#define Print3D_WIN_EXIST	1
#define Print3D_WIN_ACTIVE	2
#define Print3D_WIN_TITLE	4
#define Print3D_WIN_STATIC	8
#define Print3D_FULL_OPAQUE	16
#define Print3D_FULL_TRANS	32
#define Print3D_ADJUST_SIZE	64
#define Print3D_NO_BORDER	128

#define OPENGL_RGBA(r, g, b, a)   ((GLuint) (((a) << 24) | ((b) << 16) | ((g) << 8) | (r)))

/****************************************************************************
** Structures
****************************************************************************/
// A structure for our vertex type
struct CUSTOMVERTEX
{
	VERTTYPE	sx, sy, sz;
	GLuint	color;
	VERTTYPE	tu, tv;
};

struct PVRTPrint3DWIN
{
	unsigned int	dwFlags;

	bool			bNeedUpdated;

	/* Text Buffer */
	char			*pTextBuffer;
	unsigned int	dwBufferSizeX;
	unsigned int	dwBufferSizeY;

	/* Title */
	float			fTitleFontSize;
	float			fTextRMinPos;
	unsigned int	dwTitleFontColorL;
	unsigned int	dwTitleFontColorR;
	unsigned int	dwTitleBaseColor;
	char			*bTitleTextL;
	char			*bTitleTextR;
	unsigned int	nTitleVerticesL;
	unsigned int	nTitleVerticesR;
	CUSTOMVERTEX	*pTitleVtxL;
	CUSTOMVERTEX	*pTitleVtxR;

	/* Window */
	float			fWinFontSize;
	unsigned int	dwWinFontColor;
	unsigned int	dwWinBaseColor;
	float			fWinPos[2];
	float			fWinSize[2];
	float			fZPos;
	unsigned int	dwSort;
	unsigned int	nLineVertices[255]; // every line of text is allocated and drawn apart.
	CUSTOMVERTEX	*pLineVtx[256];
	CUSTOMVERTEX	*pWindowVtxTitle;
	CUSTOMVERTEX	*pWindowVtxText;
};

struct PVRTPrint3DGlob{
	GLuint						uTexture[5];
	GLuint						uTexturePVRLogo;
	GLuint						uTextureIMGLogo;
	GLuint						uLogoToDisplay;
	unsigned short				*pwFacesFont;
	CUSTOMVERTEX				*pPrint3dVtx;
	unsigned int				dwCreatedWins;
	float						fScreenScale[2];
	bool						bInitOK;
	bool						bTexturesSet;
	CUSTOMVERTEX				*pVtxCache;
	int							nVtxCache;
	int							nVtxCacheMax;
	PVRTPrint3DWIN				pWin[MAX_WINDOWS];
	bool						bTexturesReleased;
};

/****************************************************************************
** Declarations
****************************************************************************/
static void PVRTPrint3DDrawLogo(PVRTPrint3DGlob &Glob, GLuint tex, int nPos);

static bool			UpdateBackgroundWindow		(PVRTPrint3DGlob &Glob, unsigned int dwWin, unsigned int Color, float fZPos, float fPosX, float fPosY, float fSizeX, float fSizeY, CUSTOMVERTEX **ppVtx);
static bool			UpLoad4444					(PVRTPrint3DGlob &Glob, unsigned int TexID, unsigned char *pSource, unsigned int nSize, unsigned int nMode);
static unsigned int	UpdateLine					(PVRTPrint3DGlob &Glob, unsigned int dwWin, float fZPos, float XPos, float YPos, float fScale, int Colour, const char *Text, CUSTOMVERTEX *pVertices);
static void			DrawBackgroundWindowUP		(PVRTPrint3DGlob &Glob, unsigned int dwWin, CUSTOMVERTEX *pVtx);
static void			DrawLineUP					(PVRTPrint3DGlob &Glob, CUSTOMVERTEX *pVtx, unsigned int nVertices);
static void			UpdateTitleVertexBuffer		(PVRTPrint3DGlob &Glob, unsigned int dwWin);
static void			UpdateMainTextVertexBuffer	(PVRTPrint3DGlob &Glob, unsigned int dwWin);
static float		GetLength					(float fFontSize, char *sString);
static void			RenderStates				(PVRTPrint3DGlob &Glob, int nAction);

/****************************************************************************
** Class: CPVRTPrint3D
****************************************************************************/

/*****************************************************************************
 @Function			CPVRTPrint3D
 @Description		Init some values.
*****************************************************************************/
CPVRTPrint3D::CPVRTPrint3D()
{
#if !defined(DISABLE_PRINT3D)

	// Allocate memory for the globals
	m_pReserved = new PVRTPrint3DGlob;
	memset(m_pReserved, 0, sizeof(*m_pReserved));

#endif
}

/*****************************************************************************
 @Function			~CPVRTPrint3D
 @Description		De-allocate the working memory
*****************************************************************************/
CPVRTPrint3D::~CPVRTPrint3D()
{
#if !defined (DISABLE_PRINT3D)

	// Free global memory
	delete m_pReserved;

#endif
}

/*!***************************************************************************
 @Function			PVRTPrint3DSetTextures
 @Input				pContext		Context
 @Input				dwScreenX		Screen resolution along X
 @Input				dwScreenY		Screen resolution along Y
 @Return			true or false
 @Description		Initialization and texture upload. Should be called only once
					for a given context.
*****************************************************************************/
bool CPVRTPrint3D::SetTextures(
	const SPVRTContext	* const pContext,
	const unsigned int	dwScreenX,
	const unsigned int	dwScreenY)
{
#if !defined (DISABLE_PRINT3D)

	int				i;
	bool			bStatus;

	/* Set the aspect ratio, so we can chage it without updating textures or anything else */
	m_pReserved->fScreenScale[0] = (float)dwScreenX/640.0f;
	m_pReserved->fScreenScale[1] = (float)dwScreenY/480.0f;

	/* Check whether textures are already set up just in case */
	if (m_pReserved->bTexturesSet) return true;

	m_pReserved->bInitOK = false;

	/* Release textures if they haven't been already */
	if(m_pReserved->bTexturesReleased==false)
	{
		ReleaseTextures();
	}

	m_pReserved->bTexturesReleased = false;

	/*
		This is the window background texture
		Type 0 because the data comes in TexTool rectangular format.
	*/
	bStatus = UpLoad4444(*m_pReserved, 1, (unsigned char *)WindowBackground, 16, 0);
	if (!bStatus) return false;

	bStatus = UpLoad4444(*m_pReserved, 2, (unsigned char *)WindowPlainBackground, 16, 0);
	if (!bStatus) return false;

	bStatus = UpLoad4444(*m_pReserved, 3, (unsigned char *)WindowBackgroundOp, 16, 0);
	if (!bStatus) return false;

	bStatus = UpLoad4444(*m_pReserved, 4, (unsigned char *)WindowPlainBackgroundOp, 16, 0);
	if (!bStatus) return false;

	/*
		This is the texture with the fonts.
		Type 1 because there is only alpha component (RGB are white).
	*/
	bStatus = UpLoad4444(*m_pReserved, 0, (unsigned char *)PVRTPrint3DABC_Pixels, 256, 1);
	if (!bStatus) return false;

	/* INDEX BUFFERS */
	m_pReserved->pwFacesFont = (unsigned short*)malloc(MAX_RENDERABLE_LETTERS*2*3*sizeof(unsigned short));

	/* Load Icon textures */
	/* PVR Icon */
	glGenTextures(1, &m_pReserved->uTexturePVRLogo);
	glBindTexture(GL_TEXTURE_2D, m_pReserved->uTexturePVRLogo);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 128, 128, 0, GL_RGB, GL_UNSIGNED_SHORT_5_6_5, PVRTPrint3DPVRLogo + 11);

	/* IMG Icon */
	glGenTextures(1, &m_pReserved->uTextureIMGLogo);
	glBindTexture(GL_TEXTURE_2D, m_pReserved->uTextureIMGLogo);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 128, 128, 0, GL_RGB, GL_UNSIGNED_SHORT_5_6_5, PVRTPrint3DIMGLogo + 11);

	/* Vertex indices for letters */
	for (i=0; i < MAX_RENDERABLE_LETTERS; i++)
	{
		m_pReserved->pwFacesFont[i*6+0] = 0+i*4;
		m_pReserved->pwFacesFont[i*6+1] = 3+i*4;
		m_pReserved->pwFacesFont[i*6+2] = 1+i*4;

		m_pReserved->pwFacesFont[i*6+3] = 3+i*4;
		m_pReserved->pwFacesFont[i*6+4] = 0+i*4;
		m_pReserved->pwFacesFont[i*6+5] = 2+i*4;
	}

	m_pReserved->nVtxCacheMax = MIN_CACHED_VTX;
	m_pReserved->pVtxCache = (CUSTOMVERTEX*)malloc(m_pReserved->nVtxCacheMax * sizeof(*m_pReserved->pVtxCache));
	m_pReserved->nVtxCache = 0;

	/* Everything is OK */
	m_pReserved->bInitOK = true;
	m_pReserved->bTexturesSet = true;

	/* set all windows for an update */
	for (i=0; i<MAX_WINDOWS; i++)
		m_pReserved->pWin[i].bNeedUpdated = true;

	/* Return OK */
	return true;

#else
	return 1;
#endif
}

/*!***************************************************************************
 @Function			PVRTPrint3DReleaseTextures
 @Description		Deallocate the memory allocated in PVRTPrint3DSetTextures(...)
*****************************************************************************/
void CPVRTPrint3D::ReleaseTextures()
{
#if !defined (DISABLE_PRINT3D)

	/* Only release textures if they've been allocated */
	if (!m_pReserved->bTexturesSet) return;

	/* Release IndexBuffer */
	FREE(m_pReserved->pwFacesFont);
	FREE(m_pReserved->pPrint3dVtx);

	/* Delete textures */
	glDeleteTextures(5, m_pReserved->uTexture);
	glDeleteTextures(1, &m_pReserved->uTexturePVRLogo);
	glDeleteTextures(1, &m_pReserved->uTextureIMGLogo);

	m_pReserved->bTexturesSet = false;
	m_pReserved->bTexturesReleased = true;

	FREE(m_pReserved->pVtxCache);

#endif
}

/*!***************************************************************************
 @Function			PVRTPrint3D
 @Input				fPosX		Position of the text along X
 @Input				fPosY		Position of the text along Y
 @Input				fScale		Scale of the text
 @Input				Colour		Colour of the text
 @Input				Format		Format string for the text
 @Description		Display 3D text on screen.
					No window needs to be allocated to use this function.
					However, PVRTPrint3DSetTextures(...) must have been called
					beforehand.
					This function accepts formatting in the printf way.
*****************************************************************************/
void CPVRTPrint3D::Print3D(float fPosX, float fPosY, float fScale, int Colour, const char *Format, ...)
{
#if !defined (DISABLE_PRINT3D)

	va_list			args;
	static char	Text[MAX_LETTERS+1], sPreviousString[MAX_LETTERS+1];
	static float	XPosPrev, YPosPrev, fScalePrev;
	static int		ColourPrev;
	static unsigned int	nVertices;

	/* No textures! so... no window */
	if (!m_pReserved->bInitOK)
	{
		_RPT0(_CRT_WARN,"PVRTPrint3DDisplayWindow : You must call PVRTPrint3DSetTextures()\nbefore using this function!!!\n");
		return;
	}

	/* Reading the arguments to create our Text string */
	va_start(args,Format);
	vsprintf(Text, Format, args);		// Could use _vsnprintf but e.g. LinuxVP does not support it
	va_end(args);

	/* nothing to be drawn */
	if(*Text == 0)
		return;

	/* Adjust input parameters */
	fPosX *= 640.0f/100.0f;
	fPosY *= 480.0f/100.0f;

	/* We check if the string has been changed since last time */
	if(
		strcmp (sPreviousString, Text) != 0 ||
		fPosX != XPosPrev ||
		fPosY != YPosPrev ||
		fScale != fScalePrev ||
		Colour != ColourPrev ||
		m_pReserved->pPrint3dVtx == NULL)
	{
		/* copy strings */
		strcpy (sPreviousString, Text);
		XPosPrev = fPosX;
		YPosPrev = fPosY;
		fScalePrev = fScale;
		ColourPrev = Colour;

		/* Create Vertex Buffer (only if it doesn't exist) */
		if(m_pReserved->pPrint3dVtx==0)
		{
			m_pReserved->pPrint3dVtx = (CUSTOMVERTEX*)malloc(MAX_LETTERS*4*sizeof(CUSTOMVERTEX));
		}

		/* Fill up our buffer */
		nVertices = UpdateLine(*m_pReserved, 0, 0.0f, fPosX, fPosY, fScale, Colour, Text, m_pReserved->pPrint3dVtx);
	}

	// Draw the text
	DrawLineUP(*m_pReserved, m_pReserved->pPrint3dVtx, nVertices);

#endif
}
/*!***************************************************************************
 @Function			DisplayDefaultTitle
 @Input				sTitle				Title to display
 @Input				sDescription		Description to display
 @Input				uDisplayLogo		1 = Display the logo
 @Description		Creates a default title with predefined position and colours.
					It displays as well company logos when requested:
					0 = No logo
					1 = PowerVR logo
					2 = Img Tech logo
*****************************************************************************/
void CPVRTPrint3D::DisplayDefaultTitle(char *sTitle, char *sDescription, unsigned int uDisplayLogo)
{
#if !defined (DISABLE_PRINT3D)

	/* Display Title
	 */
	if(sTitle)
	{
		Print3D(0.0f, 1.0f, 1.2f,  0xFF00FFFF, sTitle);
	}

	/* Display Description
	 */
	if(sDescription)
	{
		Print3D(0.0f, 8.0f, 0.9f,  0xFFFFFFFF, sDescription);
	}

	m_pReserved->uLogoToDisplay = uDisplayLogo;

#endif
}
/*!***************************************************************************
 @Function			CreateDefaultWindow
 @Input				fPosX					Position X for the new window
 @Input				fPosY					Position Y for the new window
 @Input				nXSize_LettersPerLine
 @Input				sTitle					Title of the window
 @Input				sBody					Body text of the window
 @Return			Window handle
 @Description		Creates a default window.
					If Title is NULL the main body will have just one line
					(for InfoWin).
*****************************************************************************/
unsigned int CPVRTPrint3D::CreateDefaultWindow(float fPosX, float fPosY, int nXSize_LettersPerLine, char *sTitle, char *sBody)
{
#if !defined (DISABLE_PRINT3D)

	unsigned int dwActualWin;
	unsigned int dwFlags = PRINT3D_ADJUST_SIZE_ALWAYS;
	unsigned int dwBodyTextColor, dwBodyBackgroundColor;

	/* If no text is specified, return an error */
	if(!sBody && !sTitle) return 0xFFFFFFFF;

	/* If no title is specified, body text colours are different */
	if(!sTitle)
	{
		dwBodyTextColor			= OPENGL_RGBA(0xFF, 0xFF, 0x30, 0xFF);
		dwBodyBackgroundColor	= OPENGL_RGBA(0x20, 0x20, 0xB0, 0xE0);
	}
	else
	{
		dwBodyTextColor			= OPENGL_RGBA(0xFF, 0xFF, 0xFF, 0xFF);
		dwBodyBackgroundColor	= OPENGL_RGBA(0x20, 0x30, 0xFF, 0xE0);
	}

	/* Set window flags depending on title and body text were specified */
	if(!sBody)		dwFlags |= PRINT3D_DEACTIVATE_WIN;
	if(!sTitle)		dwFlags |= PRINT3D_DEACTIVATE_TITLE;

	/* Create window */
	dwActualWin = InitWindow(nXSize_LettersPerLine, (sTitle==NULL) ? 1:50);

	/* Set window properties */
	SetWindow(dwActualWin, dwBodyBackgroundColor, dwBodyTextColor, 0.5f, fPosX, fPosY, 20.0f, 20.0f);

	/* Set title */
	if (sTitle)
		SetTitle(dwActualWin, OPENGL_RGBA(0x20, 0x20, 0xB0, 0xE0), 0.6f, OPENGL_RGBA(0xFF, 0xFF, 0x30, 0xFF), sTitle, OPENGL_RGBA(0xFF, 0xFF, 0x30, 0xFF), "");

	/* Set window text */
	if (sBody)
		SetText(dwActualWin, sBody);

	/* Set window flags */
	SetWindowFlags(dwActualWin, dwFlags);

	m_pReserved->pWin[dwActualWin].bNeedUpdated = true;

	/* Return window handle */
	return dwActualWin;

#else
	return 0;
#endif
}

/*!***************************************************************************
 @Function			InitWindow
 @Input				dwBufferSizeX		Buffer width
 @Input				dwBufferSizeY		Buffer height
 @Return			Window handle
 @Description		Allocate a buffer for a newly-created window and return its
					handle.
*****************************************************************************/
unsigned int CPVRTPrint3D::InitWindow(unsigned int dwBufferSizeX, unsigned int dwBufferSizeY)
{
#if !defined (DISABLE_PRINT3D)

	unsigned int		dwCurrentWin;

	/* Find the first available window */
	for (dwCurrentWin=1; dwCurrentWin<MAX_WINDOWS; dwCurrentWin++)
	{
		/* If this window available? */
		if (!(m_pReserved->pWin[dwCurrentWin].dwFlags & Print3D_WIN_EXIST))
		{
			/* Window available, exit loop */
			break;
		}
	}

	/* No more windows available? */
	if (dwCurrentWin == MAX_WINDOWS)
	{
		_RPT0(_CRT_WARN,"\nPVRTPrint3DCreateWindow WARNING: MAX_WINDOWS overflow.\n");
		return 0;
	}

	/* Set flags */
	m_pReserved->pWin[dwCurrentWin].dwFlags = Print3D_WIN_TITLE  | Print3D_WIN_EXIST | Print3D_WIN_ACTIVE;

	/* Text Buffer */
	m_pReserved->pWin[dwCurrentWin].dwBufferSizeX = dwBufferSizeX + 1;
	m_pReserved->pWin[dwCurrentWin].dwBufferSizeY = dwBufferSizeY;
	m_pReserved->pWin[dwCurrentWin].pTextBuffer  = (char *)calloc((dwBufferSizeX+2)*(dwBufferSizeY+2), sizeof(char));
	m_pReserved->pWin[dwCurrentWin].bTitleTextL  = (char *)calloc(MAX_LETTERS, sizeof(char));
	m_pReserved->pWin[dwCurrentWin].bTitleTextR  = (char *)calloc(MAX_LETTERS, sizeof(char));

	/* Memory allocation failed */
	if (!m_pReserved->pWin[dwCurrentWin].pTextBuffer || !m_pReserved->pWin[dwCurrentWin].bTitleTextL || !m_pReserved->pWin[dwCurrentWin].bTitleTextR)
	{
		_RPT0(_CRT_WARN,"\nPVRTPrint3DCreateWindow : No memory enough for Text Buffer.\n");
		return 0;
	}

	/* Title */
	m_pReserved->pWin[dwCurrentWin].fTitleFontSize	= 1.0f;
	m_pReserved->pWin[dwCurrentWin].dwTitleFontColorL = OPENGL_RGBA(0xFF, 0xFF, 0x00, 0xFF);
	m_pReserved->pWin[dwCurrentWin].dwTitleFontColorR = OPENGL_RGBA(0xFF, 0xFF, 0x00, 0xFF);
	m_pReserved->pWin[dwCurrentWin].dwTitleBaseColor  = OPENGL_RGBA(0x30, 0x30, 0xFF, 0xFF); /* Dark Blue */

	/* Window */
	m_pReserved->pWin[dwCurrentWin].fWinFontSize		= 0.5f;
	m_pReserved->pWin[dwCurrentWin].dwWinFontColor	= OPENGL_RGBA(0xFF, 0xFF, 0xFF, 0xFF);
	m_pReserved->pWin[dwCurrentWin].dwWinBaseColor	= OPENGL_RGBA(0x80, 0x80, 0xFF, 0xFF); /* Light Blue */
	m_pReserved->pWin[dwCurrentWin].fWinPos[0]		= 0.0f;
	m_pReserved->pWin[dwCurrentWin].fWinPos[1]		= 0.0f;
	m_pReserved->pWin[dwCurrentWin].fWinSize[0]		= 20.0f;
	m_pReserved->pWin[dwCurrentWin].fWinSize[1]		= 20.0f;
	m_pReserved->pWin[dwCurrentWin].fZPos		        = 0.0f;
	m_pReserved->pWin[dwCurrentWin].dwSort		    = 0;

	m_pReserved->pWin[dwCurrentWin].bNeedUpdated = true;

	dwCurrentWin++;

	/* Returning the handle */
	return (dwCurrentWin-1);

#else
	return 0;
#endif
}

/*!***************************************************************************
 @Function			DeleteWindow
 @Input				dwWin		Window handle
 @Description		Delete the window referenced by dwWin.
*****************************************************************************/
void CPVRTPrint3D::DeleteWindow(unsigned int dwWin)
{
#if !defined (DISABLE_PRINT3D)

	int i;

	/* Release VertexBuffer */
	FREE(m_pReserved->pWin[dwWin].pTitleVtxL);
	FREE(m_pReserved->pWin[dwWin].pTitleVtxR);
	FREE(m_pReserved->pWin[dwWin].pWindowVtxTitle);
	FREE(m_pReserved->pWin[dwWin].pWindowVtxText);
	for(i=0; i<255; i++)
		FREE(m_pReserved->pWin[dwWin].pLineVtx[i])

	/* Only delete window if it exists */
	if(m_pReserved->pWin[dwWin].dwFlags & Print3D_WIN_EXIST)
	{
		FREE(m_pReserved->pWin[dwWin].pTextBuffer);
		FREE(m_pReserved->pWin[dwWin].bTitleTextL);
		FREE(m_pReserved->pWin[dwWin].bTitleTextR);
	}

	/* Reset flags */
	m_pReserved->pWin[dwWin].dwFlags = 0;

#endif
}

/*!***************************************************************************
 @Function			DeleteAllWindows
 @Description		Delete all windows.
*****************************************************************************/
void CPVRTPrint3D::DeleteAllWindows()
{
#if !defined (DISABLE_PRINT3D)

	int unsigned i;

	for (i=0; i<MAX_WINDOWS; i++)
		DeleteWindow (i);

#endif
}

/*!***************************************************************************
 @Function			DisplayWindow
 @Input				dwWin
 @Description		Display window.
					This function MUST be called between a BeginScene/EndScene
					pair as it uses D3D render primitive calls.
					PVRTPrint3DSetTextures(...) must have been called beforehand.
*****************************************************************************/
void CPVRTPrint3D::DisplayWindow(unsigned int dwWin)
{
#if !defined (DISABLE_PRINT3D)

	unsigned int	i;
	float			fTitleSize = 0.0f;

	/* No textures! so... no window */
	if (!m_pReserved->bInitOK)
	{
		_RPT0(_CRT_WARN,"PVRTPrint3DDisplayWindow : You must call PVRTPrint3DSetTextures()\nbefore using this function!!!\n");
		return;
	}

	/* Update Vertex data only when needed */
	if(m_pReserved->pWin[dwWin].bNeedUpdated)
	{
		/* TITLE */
		if(m_pReserved->pWin[dwWin].dwFlags & Print3D_WIN_TITLE)
		{
			/* Set title size */
			if(m_pReserved->pWin[dwWin].fTitleFontSize < 0.0f)
				fTitleSize = 8.0f + 16.0f;
			else
				fTitleSize = m_pReserved->pWin[dwWin].fTitleFontSize * 23.5f + 16.0f;

			/* Title */
			UpdateTitleVertexBuffer(*m_pReserved, dwWin);

			/* Background */
			if (!(m_pReserved->pWin[dwWin].dwFlags & Print3D_FULL_TRANS))
			{
				/* Draw title background */
				UpdateBackgroundWindow(
					*m_pReserved, dwWin, m_pReserved->pWin[dwWin].dwTitleBaseColor,
					0.0f,
					m_pReserved->pWin[dwWin].fWinPos[0],
					m_pReserved->pWin[dwWin].fWinPos[1],
					m_pReserved->pWin[dwWin].fWinSize[0],
					fTitleSize, &m_pReserved->pWin[dwWin].pWindowVtxTitle);
			}
		}

		/* Main text */
		UpdateMainTextVertexBuffer(*m_pReserved, dwWin);

		UpdateBackgroundWindow(
			*m_pReserved, dwWin, m_pReserved->pWin[dwWin].dwWinBaseColor,
			0.0f,
			m_pReserved->pWin[dwWin].fWinPos[0],
			(m_pReserved->pWin[dwWin].fWinPos[1] + fTitleSize),
			m_pReserved->pWin[dwWin].fWinSize[0],
			m_pReserved->pWin[dwWin].fWinSize[1], &m_pReserved->pWin[dwWin].pWindowVtxText);

		/* Don't update until next change makes it needed */
		m_pReserved->pWin[dwWin].bNeedUpdated = false;
	}

	// Ensure any previously drawn text has been submitted before drawing the window.
	Flush();

	/* Save current render states */
	RenderStates(*m_pReserved, 0);

	/*
		DRAW TITLE
	*/
	if(m_pReserved->pWin[dwWin].dwFlags & Print3D_WIN_TITLE)
	{
		if (!(m_pReserved->pWin[dwWin].dwFlags & Print3D_FULL_TRANS))
		{
			DrawBackgroundWindowUP(*m_pReserved, dwWin, m_pReserved->pWin[dwWin].pWindowVtxTitle);
		}

		/* Left and Right text */
		DrawLineUP(*m_pReserved, m_pReserved->pWin[dwWin].pTitleVtxL, m_pReserved->pWin[dwWin].nTitleVerticesL);
		DrawLineUP(*m_pReserved, m_pReserved->pWin[dwWin].pTitleVtxR, m_pReserved->pWin[dwWin].nTitleVerticesR);
	}

	/*
		DRAW WINDOW
	*/
	if (m_pReserved->pWin[dwWin].dwFlags & Print3D_WIN_ACTIVE)
	{
		/* Background */
		if (!(m_pReserved->pWin[dwWin].dwFlags & Print3D_FULL_TRANS))
		{
			DrawBackgroundWindowUP(*m_pReserved, dwWin, m_pReserved->pWin[dwWin].pWindowVtxText);
		}

		/* Text, line by line */
		for (i=0; i<m_pReserved->pWin[dwWin].dwBufferSizeY; i++)
		{
			DrawLineUP(*m_pReserved, m_pReserved->pWin[dwWin].pLineVtx[i], m_pReserved->pWin[dwWin].nLineVertices[i]);
		}
	}

	/* Restore render states */
	RenderStates(*m_pReserved, 1);

#endif
}

/*!***************************************************************************
 @Function			SetText
 @Input				dwWin		Window handle
 @Input				Format		Format string
 @Description		Feed the text buffer of window referenced by dwWin.
					This function accepts formatting in the printf way.
*****************************************************************************/
void CPVRTPrint3D::SetText(unsigned int dwWin, const char *Format, ...)
{
#if !defined (DISABLE_PRINT3D)

	va_list			args;
	unsigned int			i;
	unsigned int			dwBufferSize, dwTotalLength = 0;
	unsigned int			dwPosBx, dwPosBy, dwSpcPos;
	char			bChar;
	unsigned int			dwCursorPos;
	static char	sText[MAX_LETTERS+1];

	/* If window doesn't exist then return from function straight away */
	if (!(m_pReserved->pWin[dwWin].dwFlags & Print3D_WIN_EXIST))
		return;
	// Empty the window buffer
	memset(m_pReserved->pWin[dwWin].pTextBuffer, 0, m_pReserved->pWin[dwWin].dwBufferSizeX * m_pReserved->pWin[dwWin].dwBufferSizeY * sizeof(char));

	/* Reading the arguments to create our Text string */
	va_start(args,Format);
	vsprintf(sText, Format, args);		// Could use _vsnprintf but e.g. LinuxVP does not support it
	va_end(args);

	dwCursorPos	= 0;

	m_pReserved->pWin[dwWin].bNeedUpdated = true;

	/* Compute buffer size */
	dwBufferSize = (m_pReserved->pWin[dwWin].dwBufferSizeX+1) * (m_pReserved->pWin[dwWin].dwBufferSizeY+1);

	/* Compute length */
	while(dwTotalLength < dwBufferSize && sText[dwTotalLength] != 0)
		dwTotalLength++;

	/* X and Y pointer position */
	dwPosBx = 0;
	dwPosBy = 0;

	/* Process each character */
	for (i=0; i<dwTotalLength; i++)
	{
		/* Get current character in string */
		bChar = sText[i];

		/* Space (for word wrap only) */
		if (bChar == ' ')
		{
			/* Looking for the next space (or return or end) */
			dwSpcPos = 1;
			while (1)
			{
				bChar = sText[i + dwSpcPos++];
				if(bChar==' ' || bChar==0x0A || bChar==0)
					break;
			}
			bChar = ' ';

			/*
				Humm, if this word is longer than the buffer don't move it.
				Otherwise check if it is at the end and create a return.
			*/
			if (dwSpcPos<m_pReserved->pWin[dwWin].dwBufferSizeX && (dwPosBx+dwSpcPos)>m_pReserved->pWin[dwWin].dwBufferSizeX)
			{
				/* Set NULL character */
				m_pReserved->pWin[dwWin].pTextBuffer[dwCursorPos++] = 0;

				dwPosBx = 0;
				dwPosBy++;

				/* Set new cursor position */
				dwCursorPos = dwPosBy * m_pReserved->pWin[dwWin].dwBufferSizeX;

				/* Don't go any further */
				continue;
			}
		}

		/* End of line */
		if (dwPosBx == (m_pReserved->pWin[dwWin].dwBufferSizeX-1))
		{
			m_pReserved->pWin[dwWin].pTextBuffer[dwCursorPos++] = 0;
			dwPosBx = 0;
			dwPosBy++;
		}

		/* Vertical Scroll */
		if (dwPosBy >= m_pReserved->pWin[dwWin].dwBufferSizeY)
		{
			memcpy(m_pReserved->pWin[dwWin].pTextBuffer,
				m_pReserved->pWin[dwWin].pTextBuffer + m_pReserved->pWin[dwWin].dwBufferSizeX,
				(m_pReserved->pWin[dwWin].dwBufferSizeX-1) * m_pReserved->pWin[dwWin].dwBufferSizeY);

			dwCursorPos -= m_pReserved->pWin[dwWin].dwBufferSizeX;

			dwPosBx = 0;
			dwPosBy--;
		}

		/* Return */
		if (bChar == 0x0A)
		{
			/* Set NULL character */
			m_pReserved->pWin[dwWin].pTextBuffer[dwCursorPos++] = 0;

			dwPosBx = 0;
			dwPosBy++;

			dwCursorPos = dwPosBy * m_pReserved->pWin[dwWin].dwBufferSizeX;

			/* Don't go any further */
			continue;
		}

		/* Storing our character */
		if (dwCursorPos<dwBufferSize)
		{
			m_pReserved->pWin[dwWin].pTextBuffer[dwCursorPos++] = bChar;
		}

		/* Increase position */
		dwPosBx++;
	}

	/* Automatic adjust of the window size */
	if (m_pReserved->pWin[dwWin].dwFlags & Print3D_ADJUST_SIZE)
	{
		AdjustWindowSize(dwWin, 0);
	}

#endif
}

/*!***************************************************************************
 @Function			PVRTPrint3DSetWindow
 @Input				dwWin			Window handle
 @Input				dwWinColor		Window colour
 @Input				dwFontColor		Font colour
 @Input				fFontSize		Font size
 @Input				fPosX			Window position X
 @Input				fPosY			Window position Y
 @Input				fSizeX			Window size X
 @Input				fSizeY			Window size Y
 @Description		Set attributes of window.
					Windows position and size are referred to a virtual screen
					of 100x100. (0,0) is the top-left corner and (100,100) the
					bottom-right corner.
					These values are the same for all resolutions.
*****************************************************************************/
void CPVRTPrint3D::SetWindow(unsigned int dwWin, unsigned int dwWinColor, unsigned int dwFontColor, float fFontSize,
						  float fPosX, float fPosY, float fSizeX, float fSizeY)
{
#if !defined (DISABLE_PRINT3D)

	/* Check if there is a real change */
	if(	m_pReserved->pWin[dwWin].fWinFontSize		!= fFontSize ||
		m_pReserved->pWin[dwWin].dwWinFontColor	!= dwFontColor ||
		m_pReserved->pWin[dwWin].dwWinBaseColor	!= dwWinColor ||
		m_pReserved->pWin[dwWin].fWinPos[0]		!= fPosX  * 640.0f/100.0f ||
		m_pReserved->pWin[dwWin].fWinPos[1]		!= fPosY  * 480.0f/100.0f ||
		m_pReserved->pWin[dwWin].fWinSize[0]		!= fSizeX * 640.0f/100.0f ||
		m_pReserved->pWin[dwWin].fWinSize[1]		!= fSizeY * 480.0f/100.0f)
	{
		/* Set window properties */
		m_pReserved->pWin[dwWin].fWinFontSize		= fFontSize;
		m_pReserved->pWin[dwWin].dwWinFontColor	= dwFontColor;
		m_pReserved->pWin[dwWin].dwWinBaseColor	= dwWinColor;
		m_pReserved->pWin[dwWin].fWinPos[0]		= fPosX  * 640.0f/100.0f;
		m_pReserved->pWin[dwWin].fWinPos[1]		= fPosY  * 480.0f/100.0f;
		m_pReserved->pWin[dwWin].fWinSize[0]		= fSizeX * 640.0f/100.0f;
		m_pReserved->pWin[dwWin].fWinSize[1]		= fSizeY * 480.0f/100.0f;

		m_pReserved->pWin[dwWin].bNeedUpdated = true;
	}

#endif
}

/*!***************************************************************************
 @Function			SetTitle
 @Input				dwWin				Window handle
 @Input				dwBackgroundColor	Background color
 @Input				fFontSize			Font size
 @Input				dwFontColorLeft
 @Input				sTitleLeft
 @Input				dwFontColorRight
 @Input				sTitleRight
 @Description		Set window title.
*****************************************************************************/
void CPVRTPrint3D::SetTitle(unsigned int dwWin, unsigned int dwBackgroundColor, float fFontSize,
						 unsigned int dwFontColorLeft, char *sTitleLeft,
						 unsigned int dwFontColorRight, char *sTitleRight)
{
#if !defined (DISABLE_PRINT3D)

	FREE(m_pReserved->pWin[dwWin].pTitleVtxL);
	FREE(m_pReserved->pWin[dwWin].pTitleVtxR);

	if(sTitleLeft)  memcpy(m_pReserved->pWin[dwWin].bTitleTextL, sTitleLeft , min(MAX_LETTERS-1, strlen(sTitleLeft )+1));
	if(sTitleRight) memcpy(m_pReserved->pWin[dwWin].bTitleTextR, sTitleRight, min(MAX_LETTERS-1, strlen(sTitleRight)+1));

	/* Set title properties */
	m_pReserved->pWin[dwWin].fTitleFontSize		= fFontSize;
	m_pReserved->pWin[dwWin].dwTitleFontColorL	= dwFontColorLeft;
	m_pReserved->pWin[dwWin].dwTitleFontColorR	= dwFontColorRight;
	m_pReserved->pWin[dwWin].dwTitleBaseColor		= dwBackgroundColor;
	m_pReserved->pWin[dwWin].fTextRMinPos			= GetLength(m_pReserved->pWin[dwWin].fTitleFontSize, m_pReserved->pWin[dwWin].bTitleTextL) + 10.0f;
	m_pReserved->pWin[dwWin].bNeedUpdated			= true;

#endif
}

/*!***************************************************************************
 @Function			PVRTPrint3DSetWindowFlags
 @Input				dwWin				Window handle
 @Input				dwFlags				Flags
 @Description		Set flags for window referenced by dwWin.
					A list of flag can be found at the top of this header.
*****************************************************************************/
void CPVRTPrint3D::SetWindowFlags(unsigned int dwWin, unsigned int dwFlags)
{
#if !defined (DISABLE_PRINT3D)

	/* Check if there is need of updating vertex buffers */
	if(	dwFlags & PRINT3D_ACTIVATE_TITLE ||
		dwFlags & PRINT3D_DEACTIVATE_TITLE ||
		dwFlags & PRINT3D_ADJUST_SIZE_ALWAYS)
		m_pReserved->pWin[dwWin].bNeedUpdated = true;

	/* Set window flags */
	if (dwFlags & PRINT3D_ACTIVATE_WIN)		m_pReserved->pWin[dwWin].dwFlags |= Print3D_WIN_ACTIVE;
	if (dwFlags & PRINT3D_DEACTIVATE_WIN)	m_pReserved->pWin[dwWin].dwFlags &= ~Print3D_WIN_ACTIVE;
	if (dwFlags & PRINT3D_ACTIVATE_TITLE)	m_pReserved->pWin[dwWin].dwFlags |= Print3D_WIN_TITLE;
	if (dwFlags & PRINT3D_DEACTIVATE_TITLE) m_pReserved->pWin[dwWin].dwFlags &= ~Print3D_WIN_TITLE;
	if (dwFlags & PRINT3D_FULL_OPAQUE)		m_pReserved->pWin[dwWin].dwFlags |= Print3D_FULL_OPAQUE;
	if (dwFlags & PRINT3D_FULL_TRANS)		m_pReserved->pWin[dwWin].dwFlags |= Print3D_FULL_TRANS;

	if (dwFlags & PRINT3D_ADJUST_SIZE_ALWAYS)
	{
		m_pReserved->pWin[dwWin].dwFlags |= Print3D_ADJUST_SIZE;
		AdjustWindowSize(dwWin, 0);
	}

	if (dwFlags & PRINT3D_NO_BORDER)	m_pReserved->pWin[dwWin].dwFlags |= Print3D_NO_BORDER;

#endif
}

/*!***************************************************************************
 @Function			AdjustWindowSize
 @Input				dwWin				Window handle
 @Input				dwMode				dwMode 0 = Both, dwMode 1 = X only,  dwMode 2 = Y only
 @Description		Calculates window size so that all text fits in the window.
*****************************************************************************/
void CPVRTPrint3D::AdjustWindowSize(unsigned int dwWin, unsigned int dwMode)
{
#if !defined (DISABLE_PRINT3D)

	int unsigned i;
	unsigned int dwPointer = 0;
	float fMax = 0.0f, fLength;

	if (dwMode==1 || dwMode==0)
	{
		/* Title horizontal Size */
		if(m_pReserved->pWin[dwWin].dwFlags & Print3D_WIN_TITLE)
		{
			fMax = GetLength(m_pReserved->pWin[dwWin].fTitleFontSize, m_pReserved->pWin[dwWin].bTitleTextL);

			if (m_pReserved->pWin[dwWin].bTitleTextR)
			{
				fMax += GetLength (m_pReserved->pWin[dwWin].fTitleFontSize, m_pReserved->pWin[dwWin].bTitleTextR) + 12.0f;
			}
		}

		/* Body horizontal size (line by line) */
		for (i=0; i<m_pReserved->pWin[dwWin].dwBufferSizeY; i++)
		{
			fLength = GetLength(m_pReserved->pWin[dwWin].fWinFontSize, (m_pReserved->pWin[dwWin].pTextBuffer + dwPointer));

			if (fLength > fMax) fMax = fLength;

			dwPointer += m_pReserved->pWin[dwWin].dwBufferSizeX;
		}

		m_pReserved->pWin[dwWin].fWinSize[0] = fMax - 2.0f + 16.0f;
	}

	/* Vertical Size */
	if(dwMode==0 || dwMode==2)
	{
		if(m_pReserved->pWin[dwWin].dwBufferSizeY < 2)
		{
			i = 0;
		}
		else
		{
			/* Looking for the last line */
			i=m_pReserved->pWin[dwWin].dwBufferSizeY;
			while(i)
			{
				--i;
				if (m_pReserved->pWin[dwWin].pTextBuffer[m_pReserved->pWin[dwWin].dwBufferSizeX * i])
					break;
			}
		}

		if (m_pReserved->pWin[dwWin].fWinFontSize>0)
			m_pReserved->pWin[dwWin].fWinSize[1] = (float)(i+1) * LINES_SPACING * m_pReserved->pWin[dwWin].fWinFontSize + 16.0f;
		else
			m_pReserved->pWin[dwWin].fWinSize[1] = ((float)(i+1) * 12.0f) + 16.0f;
	}

	m_pReserved->pWin[dwWin].bNeedUpdated = true;

#endif
}

/*!***************************************************************************
 @Function			GetSize
 @Output			pfWidth				Width of the string in pixels
 @Output			pfHeight			Height of the string in pixels
 @Input				fFontSize			Font size
 @Input				sString				String to take the size of
 @Description		Returns the size of a string in pixels.
*****************************************************************************/
void CPVRTPrint3D::GetSize(
	float		* const pfWidth,
	float		* const pfHeight,
	const float	fFontSize,
	const char	* sString)
{
#if !defined (DISABLE_PRINT3D)

	unsigned char Val;
	float fScale, fSize;

	if(sString == NULL) {
		if(pfWidth)
			*pfWidth = 0;
		if(pfHeight)
			*pfHeight = 0;
		return;
	}

	if(fFontSize > 0.0f) /* Arial font */
	{
		fScale = fFontSize;
		fSize  = 0.0f;

		Val = *sString++;
		while(Val)
		{
			if(Val==' ')
				Val = '0';

			if(Val>='0' && Val <= '9')
				Val = '0'; /* That's for fixing the number width */

			fSize += PVRTPrint3DSize_Bold[Val] * 40.0f * fScale ;

			/* these letters are too narrow due to a bug in the table */
			if(Val=='i' || Val == 'l' || Val == 'j')
				fSize += 0.4f* fScale;
			Val = *sString++;
		}

		if(pfHeight)
			*pfHeight = m_pReserved->fScreenScale[1] * fScale * 27.0f * (100.0f / 640.0f);
	}
	else /* System font */
	{
		fScale = 255.0f;
		fSize  = 0.0f;

		Val = *sString++;
		while (Val)
		{
			if(Val == ' ') {
				fSize += 5.0f;
				continue;
			}

			if(Val>='0' && Val <= '9')
				Val = '0'; /* That's for fixing the number width */

			fSize += PVRTPrint3DSize_System[Val]  * fScale * (100.0f / 640.0f);
			Val = *sString++;
		}

		if(pfHeight)
			*pfHeight = m_pReserved->fScreenScale[1] * 12.0f;
	}

	if(pfWidth)
		*pfWidth = fSize;

#endif
}

/*!***************************************************************************
 @Function			GetAspectRatio
 @Output			dwScreenX		Screen resolution X
 @Output			dwScreenY		Screen resolution Y
 @Description		Returns the current resolution used by Print3D
*****************************************************************************/
void CPVRTPrint3D::GetAspectRatio(unsigned int *dwScreenX, unsigned int *dwScreenY)
{
#if !defined (DISABLE_PRINT3D)

	*dwScreenX = (int)(640.0f * m_pReserved->fScreenScale[0]);
	*dwScreenY = (int)(480.0f * m_pReserved->fScreenScale[1]);

#endif
}

/*!***************************************************************************
 @Function			Flush
 @Description		Flushes all the print text commands
*****************************************************************************/
int CPVRTPrint3D::Flush()
{
#if !defined (DISABLE_PRINT3D)

	int		nTris, nVtx, nVtxBase, nTrisTot;

	_ASSERT((m_pReserved->nVtxCache % 4) == 0);
	_ASSERT(m_pReserved->nVtxCache <= m_pReserved->nVtxCacheMax);

	/* Save render states */
	RenderStates(*m_pReserved, 0);

	/* Set font texture */
	glBindTexture(GL_TEXTURE_2D, m_pReserved->uTexture[0]);

	/* Set blending mode */
	glEnable(GL_BLEND);

	nTrisTot = m_pReserved->nVtxCache >> 1;

	/*
		Render the text then. Might need several submissions.
	*/
	nVtxBase = 0;
	while(m_pReserved->nVtxCache)
	{
		nVtx	= min(m_pReserved->nVtxCache, 0xFFFC);
		nTris	= nVtx >> 1;

		_ASSERT(nTris <= (MAX_RENDERABLE_LETTERS*2));
		_ASSERT((nVtx % 4) == 0);

		/* Draw triangles */
		glVertexPointer(3,		VERTTYPEENUM,			sizeof(CUSTOMVERTEX), &m_pReserved->pVtxCache[nVtxBase].sx);
		glColorPointer(4,		GL_UNSIGNED_BYTE,	sizeof(CUSTOMVERTEX), &m_pReserved->pVtxCache[nVtxBase].color);
		glTexCoordPointer(2,	VERTTYPEENUM,			sizeof(CUSTOMVERTEX), &m_pReserved->pVtxCache[nVtxBase].tu);
		glDrawElements(GL_TRIANGLES, nTris * 3, GL_UNSIGNED_SHORT, m_pReserved->pwFacesFont);
		if (glGetError())
		{
			_RPT0(_CRT_WARN,"glDrawElements(GL_TRIANGLES, (VertexCount/2)*3, GL_UNSIGNED_SHORT, m_pReserved->pFacesFont); failed\n");
		}

		nVtxBase		+= nVtx;
		m_pReserved->nVtxCache	-= nVtx;
	}

	/* Draw a logo if requested */
#if defined(FORCE_NO_LOGO)
	/* Do nothing */

#elif defined(FORCE_PVR_LOGO)
    PVRTPrint3DDrawLogo(*m_pReserved, m_pReserved->uTexturePVRLogo, 1);	/* PVR to the right */

#elif defined(FORCE_IMG_LOGO)
	PVRTPrint3DDrawLogo(*m_pReserved, m_pReserved->uTextureIMGLogo, 1);	/* IMG to the right */

#elif defined(FORCE_ALL_LOGOS)
	PVRTPrint3DDrawLogo(*m_pReserved, m_pReserved->uTextureIMGLogo, -1); /* IMG to the left */
	PVRTPrint3DDrawLogo(*m_pReserved, m_pReserved->uTexturePVRLogo, 1);	/* PVR to the right */

#else
	/* User selected logos */
	switch (m_pReserved->uLogoToDisplay)
	{
		case NO_LOGO:
			break;
		default:
		case PVR_LOGO:
			PVRTPrint3DDrawLogo(*m_pReserved, m_pReserved->uTexturePVRLogo, 1);	/* PVR to the right */
			break;
		case IMG_LOGO:
			PVRTPrint3DDrawLogo(*m_pReserved, m_pReserved->uTextureIMGLogo, 1);	/* IMG to the right */
			break;
		case (PVR_LOGO | IMG_LOGO):
			PVRTPrint3DDrawLogo(*m_pReserved, m_pReserved->uTextureIMGLogo, -1); /* IMG to the left */
			PVRTPrint3DDrawLogo(*m_pReserved, m_pReserved->uTexturePVRLogo, 1);	/* PVR to the right */
			break;
	}
#endif

	/* Restore render states */
	RenderStates(*m_pReserved, 1);

	return nTrisTot;

#else
	return 0;
#endif
}

/*************************************************************
*					 PRIVATE FUNCTIONS						 *
**************************************************************/

/*!***************************************************************************
 @Function			UpdateBackgroundWindow
 @Return			true if succesful, false otherwise.
 @Description		Draw a generic rectangle (with or without border).
*****************************************************************************/
static bool UpdateBackgroundWindow(PVRTPrint3DGlob &Glob, unsigned int dwWin, unsigned int Color, float fZPos, float fPosX, float fPosY, float fSizeX, float fSizeY, CUSTOMVERTEX **ppVtx)
{
	int				i;
	CUSTOMVERTEX	*vBox;
	float			fU[] = { 0.0f, 0.0f, 6.0f, 6.0f, 10.0f,10.0f, 16.0f,16.0f,10.0f,16.0f,10.0f,16.0f,6.0f,6.0f,0.0f,0.0f};
	float			fV[] = { 0.0f, 6.0f, 0.0f, 6.0f, 0.0f, 6.0f, 0.0f, 6.0f, 10.0f, 10.0f, 16.0f,16.0f, 16.0f, 10.0f, 16.0f, 10.0f};

	/* Create our vertex buffers */
	if(*ppVtx==0)
	{
		*ppVtx = (CUSTOMVERTEX*)malloc(16*sizeof(CUSTOMVERTEX));
	}
	vBox = *ppVtx;


	/* Removing the border */
	fSizeX -= 16.0f ;
	fSizeY -= 16.0f ;

	/* Set Z position, color and texture coordinates in array */
	for (i=0; i<16; i++)
	{
		vBox[i].sz		= f2vt(fZPos);
		vBox[i].color	= Color;
		vBox[i].tu		= f2vt(fU[i]/16.0f);
		vBox[i].tv		= f2vt(1.0f - fV[i]/16.0f);
	}

	/* Set coordinates in array */
	vBox[0].sx = f2vt((fPosX + fU[0]) * Glob.fScreenScale[0]);
	vBox[0].sy = f2vt((fPosY + fV[0]) * Glob.fScreenScale[1]);

	vBox[1].sx = f2vt((fPosX + fU[1]) * Glob.fScreenScale[0]);
	vBox[1].sy = f2vt((fPosY + fV[1]) * Glob.fScreenScale[1]);

	vBox[2].sx = f2vt((fPosX + fU[2]) * Glob.fScreenScale[0]);
	vBox[2].sy = f2vt((fPosY + fV[2]) * Glob.fScreenScale[1]);

	vBox[3].sx = f2vt((fPosX + fU[3]) * Glob.fScreenScale[0]);
	vBox[3].sy = f2vt((fPosY + fV[3]) * Glob.fScreenScale[1]);

	vBox[4].sx = f2vt((fPosX + fU[4] + fSizeX) * Glob.fScreenScale[0]);
	vBox[4].sy = f2vt((fPosY + fV[4]) * Glob.fScreenScale[1]);

	vBox[5].sx = f2vt((fPosX + fU[5] + fSizeX) * Glob.fScreenScale[0]);
	vBox[5].sy = f2vt((fPosY + fV[5]) * Glob.fScreenScale[1]);

	vBox[6].sx = f2vt((fPosX + fU[6] + fSizeX) * Glob.fScreenScale[0]);
	vBox[6].sy = f2vt((fPosY + fV[6]) * Glob.fScreenScale[1]);

	vBox[7].sx = f2vt((fPosX + fU[7] + fSizeX) * Glob.fScreenScale[0]);
	vBox[7].sy = f2vt((fPosY + fV[7]) * Glob.fScreenScale[1]);

	vBox[8].sx = f2vt((fPosX + fU[8] + fSizeX) * Glob.fScreenScale[0]);
	vBox[8].sy = f2vt((fPosY + fV[8] + fSizeY) * Glob.fScreenScale[1]);

	vBox[9].sx = f2vt((fPosX + fU[9] + fSizeX) * Glob.fScreenScale[0]);
	vBox[9].sy = f2vt((fPosY + fV[9] + fSizeY) * Glob.fScreenScale[1]);

	vBox[10].sx = f2vt((fPosX + fU[10] + fSizeX) * Glob.fScreenScale[0]);
	vBox[10].sy = f2vt((fPosY + fV[10] + fSizeY) * Glob.fScreenScale[1]);

	vBox[11].sx = f2vt((fPosX + fU[11] + fSizeX) * Glob.fScreenScale[0]);
	vBox[11].sy = f2vt((fPosY + fV[11] + fSizeY) * Glob.fScreenScale[1]);

	vBox[12].sx = f2vt((fPosX + fU[12]) * Glob.fScreenScale[0]);
	vBox[12].sy = f2vt((fPosY + fV[12] + fSizeY) * Glob.fScreenScale[1]);

	vBox[13].sx = f2vt((fPosX + fU[13]) * Glob.fScreenScale[0]);
	vBox[13].sy = f2vt((fPosY + fV[13] + fSizeY) * Glob.fScreenScale[1]);

	vBox[14].sx = f2vt((fPosX + fU[14]) * Glob.fScreenScale[0]);
	vBox[14].sy = f2vt((fPosY + fV[14] + fSizeY) * Glob.fScreenScale[1]);

	vBox[15].sx = f2vt((fPosX + fU[15]) * Glob.fScreenScale[0]);
	vBox[15].sy = f2vt((fPosY + fV[15] + fSizeY) * Glob.fScreenScale[1]);

	/* No problem occured */
	return true;
}

/*!***************************************************************************
 @Function			UpLoad4444
 @Return			true if succesful, false otherwise.
 @Description		Reads texture data from *.dat and loads it in
					video memory.
*****************************************************************************/
static bool UpLoad4444(PVRTPrint3DGlob &Glob, unsigned int dwTexID, unsigned char *pSource, unsigned int nSize, unsigned int nMode)
{
	int				i, j;
	int				x=256, y=256;
	unsigned short	R, G, B, A;
	unsigned short	*p8888,  *pDestByte;
	unsigned char   *pSrcByte;

	/* Only square textures */
	x = nSize;
	y = nSize;

	glGenTextures(1, &Glob.uTexture[dwTexID]);

	/* Load texture from data */

	/* Format is 4444-packed, expand it into 8888 */
	if (nMode==0)
	{
		/* Allocate temporary memory */
		p8888 = (unsigned short *)malloc(nSize*nSize*sizeof(unsigned short));
		pDestByte = p8888;

		/* Set source pointer (after offset of 16) */
		pSrcByte = &pSource[16];

		/* Transfer data */
		for (i=0; i<y; i++)
		{
			for (j=0; j<x; j++)
			{
				/* Get all 4 colour channels (invert A) */
				G =   (*pSrcByte) & 0xF0;
				R = ( (*pSrcByte++) & 0x0F ) << 4;
				A =   (*pSrcByte) ^ 0xF0;
				B = ( (*pSrcByte++) & 0x0F ) << 4;

				/* Set them in 8888 data */
				*pDestByte++ = ((R&0xF0)<<8) | ((G&0xF0)<<4) | (B&0xF0) | (A&0xF0)>>4;
			}
		}
	}
	else
	{
		/* Set source pointer */
		pSrcByte = pSource;

		/* Allocate temporary memory */
		p8888 = (unsigned short *)malloc(nSize*nSize*sizeof(unsigned short));
		if (!p8888)
		{
			_RPT0(_CRT_WARN,"Not enough memory!\n");
			return false;
		}

		/* Set destination pointer */
		pDestByte = p8888;

		/* Transfer data */
		for (i=0; i<y; i++)
		{
			for (j=0; j<x; j++)
			{
				/* Get alpha channel */
				A = *pSrcByte++;

				/* Set them in 8888 data */
				R = 255;
				G = 255;
				B = 255;

				/* Set them in 8888 data */
				*pDestByte++ = ((R&0xF0)<<8) | ((G&0xF0)<<4) | (B&0xF0) | (A&0xF0)>>4;
			}
		}
	}

	/* Bind texture */
	glBindTexture(GL_TEXTURE_2D, Glob.uTexture[dwTexID]);

	/* Default settings: bilinear */
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	/* Now load texture */
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, x, y, 0, GL_RGBA, GL_UNSIGNED_SHORT_4_4_4_4, p8888);
	if (glGetError())
	{
		_RPT0(_CRT_WARN,"glTexImage2D() failed\n");
		free(p8888);
		return false;
	}

	/* Destroy temporary data */
	free(p8888);

	/* Return status : OK */
	return true;
}

/*!***************************************************************************
 @Function			UpdateLine
 @Description
*****************************************************************************/
static unsigned int UpdateLine(PVRTPrint3DGlob &Glob, unsigned int dwWin, float fZPos, float XPos, float YPos, float fScale, int Colour, const char *Text, CUSTOMVERTEX *pVertices)
{
	unsigned	i=0, VertexCount=0;
	unsigned	Val;
	float		XSize = 0.0f, XFixBug,	YSize = 0, TempSize;
	float		UPos,	VPos;
	float		USize,	VSize;
	float		fWinClipX[2],fWinClipY[2];
	float		fScaleX, fScaleY, fPreXPos;

	/* Nothing to update */
	if (Text==NULL) return 0;

	_ASSERT(Glob.pWin[dwWin].dwFlags & Print3D_WIN_EXIST || !dwWin);

	if (fScale>0)
	{
		fScaleX = Glob.fScreenScale[0] * fScale * 255.0f;
		fScaleY = Glob.fScreenScale[1] * fScale * 27.0f;
	}
	else
	{
		fScaleX = Glob.fScreenScale[0] * 255.0f;
		fScaleY = Glob.fScreenScale[1] * 12.0f;
	}

	XPos *= Glob.fScreenScale[0];
	YPos *= Glob.fScreenScale[1];

	fPreXPos = XPos;

	/*
		Calculating our margins
	*/
	if (dwWin)
	{
		fWinClipX[0] = (Glob.pWin[dwWin].fWinPos[0] + 6.0f) * Glob.fScreenScale[0];
		fWinClipX[1] = (Glob.pWin[dwWin].fWinPos[0] + Glob.pWin[dwWin].fWinSize[0] - 6.0f) * Glob.fScreenScale[0];

		fWinClipY[0] = (Glob.pWin[dwWin].fWinPos[1] + 6.0f) * Glob.fScreenScale[1];
		fWinClipY[1] = (Glob.pWin[dwWin].fWinPos[1] + Glob.pWin[dwWin].fWinSize[1]  + 9.0f) * Glob.fScreenScale[1];

		if(Glob.pWin[dwWin].dwFlags & Print3D_WIN_TITLE)
		{
			if (Glob.pWin[dwWin].fTitleFontSize>0)
			{
				fWinClipY[0] +=  Glob.pWin[dwWin].fTitleFontSize * 25.0f  * Glob.fScreenScale[1];
				fWinClipY[1] +=  Glob.pWin[dwWin].fTitleFontSize * 25.0f *  Glob.fScreenScale[1];
			}
			else
			{
				fWinClipY[0] +=  10.0f * Glob.fScreenScale[1];
				fWinClipY[1] +=  8.0f  * Glob.fScreenScale[1];
			}
		}
	}

	while (true)
	{
		Val = (int)Text[i++];

		/* End of the string */
		if (Val==0 || i>MAX_LETTERS) break;

		/* It is SPACE so don't draw and carry on... */
		if (Val==' ')
		{
			if (fScale>0)	XPos += 10.0f/255.0f * fScaleX;
			else			XPos += 5.0f * Glob.fScreenScale[0];
			continue;
		}

		/* It is RETURN so jump a line */
		if (Val==0x0A)
		{
			XPos = fPreXPos - XSize;
			YPos += YSize;
			continue;
		}

		/* If fScale is negative then select the small set of letters (System) */
		if (fScale < 0.0f)
		{
			XPos    += XSize;
			UPos    =  PVRTPrint3DU_System[Val];
			VPos    =  PVRTPrint3DV_System[Val] - 0.0001f; /* Some cards need this little bit */
			YSize   =  fScaleY;
			XSize   =  PVRTPrint3DSize_System[Val] * fScaleX;
			USize	=  PVRTPrint3DSize_System[Val];
			VSize	=  12.0f/255.0f;
		}
		else /* Big set of letters (Bold) */
		{
			XPos    += XSize;
			UPos    =  PVRTPrint3DU_Bold[Val];
			VPos    =  PVRTPrint3DV_Bold[Val] - 1.0f/230.0f;
			YSize   =  fScaleY;
			XSize   =  PVRTPrint3DSize_Bold[Val] * fScaleX;
			USize	=  PVRTPrint3DSize_Bold[Val];
			VSize	=  29.0f/255.0f;
		}

		/*
			CLIPPING
		*/
		XFixBug = XSize;

		if (0)//dwWin) /* for dwWin==0 (screen) no clipping */
		{
			/* Outside */
			if (XPos>fWinClipX[1]  ||  (YPos)>fWinClipY[1])
			{
				continue;
			}

			/* Clip X */
			if (XPos<fWinClipX[1] && XPos+XSize > fWinClipX[1])
			{
				TempSize = XSize;

				XSize = fWinClipX[1] - XPos;

				if (fScale < 0.0f)
					USize	=  PVRTPrint3DSize_System[Val] * (XSize/TempSize);
				else
					USize	=  PVRTPrint3DSize_Bold[Val] * (XSize/TempSize);
			}

			/*
				Clip Y
			*/
			if (YPos<fWinClipY[1] && YPos+YSize > fWinClipY[1])
			{
				TempSize = YSize;
				YSize = fWinClipY[1] - YPos;

				if(fScale < 0.0f)
				 	VSize	=  (YSize/TempSize)*12.0f/255.0f;
				else
					VSize	=  (YSize/TempSize)*28.0f/255.0f;
			}
		}


		/* Filling vertex data */
		pVertices[VertexCount+0].sx		= f2vt(XPos);
		pVertices[VertexCount+0].sy		= f2vt(YPos);
		pVertices[VertexCount+0].sz		= f2vt(fZPos);
		pVertices[VertexCount+0].tu		= f2vt(UPos);
		pVertices[VertexCount+0].tv		= f2vt(VPos);

		pVertices[VertexCount+1].sx		= f2vt(XPos+XSize);
		pVertices[VertexCount+1].sy		= f2vt(YPos);
		pVertices[VertexCount+1].sz		= f2vt(fZPos);
		pVertices[VertexCount+1].tu		= f2vt(UPos+USize);
		pVertices[VertexCount+1].tv		= f2vt(VPos);

		pVertices[VertexCount+2].sx		= f2vt(XPos);
		pVertices[VertexCount+2].sy		= f2vt(YPos+YSize);
		pVertices[VertexCount+2].sz		= f2vt(fZPos);
		pVertices[VertexCount+2].tu		= f2vt(UPos);
		pVertices[VertexCount+2].tv		= f2vt(VPos-VSize);

		pVertices[VertexCount+3].sx		= f2vt(XPos+XSize);
		pVertices[VertexCount+3].sy		= f2vt(YPos+YSize);
		pVertices[VertexCount+3].sz		= f2vt(fZPos);
		pVertices[VertexCount+3].tu		= f2vt(UPos+USize);
		pVertices[VertexCount+3].tv		= f2vt(VPos-VSize);

		pVertices[VertexCount+0].color	= Colour;
		pVertices[VertexCount+1].color	= Colour;
		pVertices[VertexCount+2].color	= Colour;
		pVertices[VertexCount+3].color	= Colour;

		VertexCount += 4;

		XSize = XFixBug;

		/* Fix number width */
		if (Val >='0' && Val <='9')
		{
			if (fScale < 0.0f)
				XSize = PVRTPrint3DSize_System[(int)'0'] * fScaleX;
			else
				XSize = PVRTPrint3DSize_Bold[(int)'0'] * fScaleX;
		}
	}

	return VertexCount;
}

/*!***************************************************************************
 @Function			DrawBackgroundWindow
 @Description
*****************************************************************************/
static void DrawBackgroundWindowUP(PVRTPrint3DGlob &Glob, unsigned int dwWin, CUSTOMVERTEX *pVtx)
{
	bool bIsOp;
	const unsigned short c_pwFacesWindow[] =
	{
		0,1,2, 2,1,3, 2,3,4, 4,3,5, 4,5,6, 6,5,7, 5,8,7, 7,8,9, 8,10,9, 9,10,11, 8,12,10, 8,13,12,
		13,14,12, 13,15,14, 13,3,15, 1,15,3, 3,13,5, 5,13,8
	};

	/* Is window opaque ? */
	bIsOp = (Glob.pWin[dwWin].dwFlags & Print3D_FULL_OPAQUE) ? true : false;

	/* Set the texture (with or without border) */
	if(Glob.pWin[dwWin].dwFlags & Print3D_NO_BORDER)
		glBindTexture(GL_TEXTURE_2D, Glob.uTexture[2 + (bIsOp*2)]);
	else
		glBindTexture(GL_TEXTURE_2D, Glob.uTexture[1 + (bIsOp*2)]);

	/* Is window opaque ? */
	if(bIsOp)
	{
		glDisable(GL_BLEND);
	}
	else
	{
		/* Set blending properties */
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	/* Set pointers */
	glVertexPointer(3,		VERTTYPEENUM,			sizeof(CUSTOMVERTEX), &pVtx[0].sx);
	glColorPointer(4,		GL_UNSIGNED_BYTE,		sizeof(CUSTOMVERTEX), &pVtx[0].color);
	glTexCoordPointer(2,	VERTTYPEENUM,			sizeof(CUSTOMVERTEX), &pVtx[0].tu);

	/* Draw triangles */
	glDrawElements(GL_TRIANGLES, 18*3, GL_UNSIGNED_SHORT, c_pwFacesWindow);
	if (glGetError())
	{
		_RPT0(_CRT_WARN,"glDrawElements(GL_TRIANGLES, 18*3, GL_UNSIGNED_SHORT, pFaces); failed\n");
	}

	/* Restore render states (need to be translucent to draw the text) */
}

/*!***************************************************************************
 @Function			DrawLineUP
 @Description		Draw a single line of text.
*****************************************************************************/
static void DrawLineUP(PVRTPrint3DGlob &Glob, CUSTOMVERTEX *pVtx, unsigned int nVertices)
{
	if(!nVertices)
		return;

	_ASSERT((nVertices % 4) == 0);
	_ASSERT((nVertices/4) < MAX_LETTERS);

	while(Glob.nVtxCache + (int)nVertices > Glob.nVtxCacheMax) {
		if(Glob.nVtxCache + nVertices > MAX_CACHED_VTX) {
			_RPT1(_CRT_WARN, "Print3D: Out of space to cache text! (More than %d vertices!)\n", MAX_CACHED_VTX);
			return;
		}

		Glob.nVtxCacheMax	= min(Glob.nVtxCacheMax * 2, MAX_CACHED_VTX);
		Glob.pVtxCache		= (CUSTOMVERTEX*)realloc(Glob.pVtxCache, Glob.nVtxCacheMax * sizeof(*Glob.pVtxCache));
		_ASSERT(Glob.pVtxCache);
		_RPT1(_CRT_WARN, "Print3D: TextCache increased to %d vertices.\n", Glob.nVtxCacheMax);
	}

	memcpy(&Glob.pVtxCache[Glob.nVtxCache], pVtx, nVertices * sizeof(*pVtx));
	Glob.nVtxCache += nVertices;
}

/*!***************************************************************************
 @Function			UpdateTitleVertexBuffer
 @Description
*****************************************************************************/
static void UpdateTitleVertexBuffer(PVRTPrint3DGlob &Glob, unsigned int dwWin)
{
	float fRPos;
	unsigned int dwLenL = 0, dwLenR = 0;

	/* Doesn't exist */
	if (!(Glob.pWin[dwWin].dwFlags & Print3D_WIN_EXIST) && dwWin)
		return;

	/* Allocate our buffers if needed */
	if(Glob.pWin[dwWin].pTitleVtxL==0 || Glob.pWin[dwWin].pTitleVtxR==0)
	{
		dwLenL = (unsigned int)strlen(Glob.pWin[dwWin].bTitleTextL);
		FREE(Glob.pWin[dwWin].pTitleVtxL);
		if(dwLenL)
			Glob.pWin[dwWin].pTitleVtxL = (CUSTOMVERTEX*)malloc(dwLenL*4*sizeof(CUSTOMVERTEX));

		dwLenR = Glob.pWin[dwWin].bTitleTextR ? (unsigned int)strlen(Glob.pWin[dwWin].bTitleTextR) : 0;
		FREE(Glob.pWin[dwWin].pTitleVtxR);
		if(dwLenR)
			Glob.pWin[dwWin].pTitleVtxR = (CUSTOMVERTEX*)malloc(dwLenR*4*sizeof(CUSTOMVERTEX));
	}

	/* Left title */
	if (dwLenL)
	{
		Glob.pWin[dwWin].nTitleVerticesL = UpdateLine(Glob, dwWin, 0.0f,
			(Glob.pWin[dwWin].fWinPos[0] + 6.0f),
			(Glob.pWin[dwWin].fWinPos[1] + 7.0f),
			Glob.pWin[dwWin].fTitleFontSize,
			Glob.pWin[dwWin].dwTitleFontColorL,
			Glob.pWin[dwWin].bTitleTextL,
			Glob.pWin[dwWin].pTitleVtxL);
	}
	else
	{
		Glob.pWin[dwWin].nTitleVerticesL = 0;
		Glob.pWin[dwWin].pTitleVtxL = NULL;
	}

	/* Right title */
	if (dwLenR)
	{
		/* Compute position */
		fRPos = GetLength(Glob.pWin[dwWin].fTitleFontSize,Glob.pWin[dwWin].bTitleTextR);

		fRPos = Glob.pWin[dwWin].fWinSize[0]  - fRPos - 6.0f;

		/* Check that we're not under minimum position */
		if(fRPos<Glob.pWin[dwWin].fTextRMinPos)
			fRPos = Glob.pWin[dwWin].fTextRMinPos;

		/* Add window position */
		fRPos += Glob.pWin[dwWin].fWinPos[0];

		/* Print text */
		Glob.pWin[dwWin].nTitleVerticesR = UpdateLine(Glob, dwWin, 0.0f,
			fRPos,
			Glob.pWin[dwWin].fWinPos[1] + 7.0f,
			Glob.pWin[dwWin].fTitleFontSize,
			Glob.pWin[dwWin].dwTitleFontColorR,
			Glob.pWin[dwWin].bTitleTextR,
			Glob.pWin[dwWin].pTitleVtxR);
	}
	else
	{
		Glob.pWin[dwWin].nTitleVerticesR = 0;
		Glob.pWin[dwWin].pTitleVtxR = NULL;
	}
}

/*!***************************************************************************
 @Function			UpdateMainTextVertexBuffer
 @Description
*****************************************************************************/
static void UpdateMainTextVertexBuffer(PVRTPrint3DGlob &Glob, unsigned int dwWin)
{
	int i;
	float		fNewPos, fTitleSize;
	unsigned int		dwPointer = 0, dwLen;

	/* Doesn't exist */
	if (!(Glob.pWin[dwWin].dwFlags & Print3D_WIN_EXIST) && dwWin) return;

	/* No text to update vertices */
	if(Glob.pWin[dwWin].pTextBuffer==NULL) return;

	/* Well, once we've got our text, allocate it to draw it later */
	/* Text, line by line */
	for (i=0; i<(int)Glob.pWin[dwWin].dwBufferSizeY; i++)
	{
		/* line length */
		dwLen = (unsigned int)strlen(&Glob.pWin[dwWin].pTextBuffer[dwPointer]);
		if(dwLen==0)
		{
			Glob.pWin[dwWin].nLineVertices[i] = 0;
			Glob.pWin[dwWin].pLineVtx[i] = NULL;
		}
		else
		{
			/* Create Vertex Buffer (one per line) */
			if (Glob.pWin[dwWin].pLineVtx[i]==0)
				Glob.pWin[dwWin].pLineVtx[i] = (CUSTOMVERTEX*)malloc(Glob.pWin[dwWin].dwBufferSizeX *4*sizeof(CUSTOMVERTEX));

			/* Compute new text position */
			fTitleSize = 0.0f;
			if(Glob.pWin[dwWin].fTitleFontSize < 0.0f)
			{
				/* New position for alternate font */
				if(Glob.pWin[dwWin].dwFlags & Print3D_WIN_TITLE)
					fTitleSize = 8.0f +16;
				fNewPos = fTitleSize + (float)(i * 12.0f);
			}
			else
			{
				/* New position for normal font */
				if(Glob.pWin[dwWin].dwFlags & Print3D_WIN_TITLE)
					fTitleSize = Glob.pWin[dwWin].fTitleFontSize * 23.5f + 16.0f;
				fNewPos = fTitleSize + (float)(i * Glob.pWin[dwWin].fWinFontSize) * LINES_SPACING;
			}

			/* Print window text */
			Glob.pWin[dwWin].nLineVertices[i] = UpdateLine(Glob, dwWin, 0.0f,
				(Glob.pWin[dwWin].fWinPos[0] + 6.0f),
				(Glob.pWin[dwWin].fWinPos[1] + 6.0f + fNewPos),
				Glob.pWin[dwWin].fWinFontSize, Glob.pWin[dwWin].dwWinFontColor,
				&Glob.pWin[dwWin].pTextBuffer[dwPointer],
				Glob.pWin[dwWin].pLineVtx[i]);
		}

		/* Increase pointer */
		dwPointer += Glob.pWin[dwWin].dwBufferSizeX;
	}
}

/*!***************************************************************************
 @Function			GetLength
 @Description		calculates the size in pixels.
*****************************************************************************/
static float GetLength(float fFontSize, char *sString)
{
	unsigned char Val;
	float fScale, fSize;

	if(sString==NULL) return 0.0f;

	if (fFontSize>=0) /* Arial font */
	{
		fScale = fFontSize * 255.0f;
		fSize  = 0.0f;

		Val = *sString++;
		while (Val)
		{
			if(Val==' ')
			{
				fSize += 10.0f * fFontSize;
			}
			else
			{
				if(Val>='0' && Val <= '9') Val = '0'; /* That's for fixing the number width */
				fSize += PVRTPrint3DSize_Bold[Val] * fScale ;
			}
			Val = *sString++;
		}
	}
	else /* System font */
	{
		fScale = 255.0f;
		fSize  = 0.0f;

		Val = *sString++;
		while (Val)
		{
			if (Val==' ')
			{
				fSize += 5.0f;
			}
			else
			{
				if(Val>='0' && Val <= '9') Val = '0'; /* That's for fixing the number width */
				fSize += PVRTPrint3DSize_System[Val]  * fScale;
			}
			Val = *sString++;
		}
	}

	return (fSize);
}
/*!***************************************************************************
 @Function			RenderStates
 @Description		calculates the size in pixels.
*****************************************************************************/
static void RenderStates(PVRTPrint3DGlob &Glob, int nAction)
{
#if 0	// OpenGL ES 1.0 does not support these
	static GLint		iMatrixMode, iFrontFace, iCullFaceMode;
	static GLboolean	bLighting, bCullFace, bFog, bDepthTest, bVertexProgram;
#endif
	PVRTMATRIX			Matrix;
	int					i;

	/* Saving or restoring states ? */
	switch (nAction)
	{
	case 0:
		/* Get previous render states */
		/* Save all attributes */
		/*glPushAttrib(GL_ALL_ATTRIB_BITS);*/

		/* Client states */
		/*glGetbooleanv(GL_VERTEX_ARRAY,			&bVertexPointerEnabled);*/
		/*glGetbooleanv(GL_COLOR_ARRAY,			&bColorPointerEnabled);*/
		/*glGetbooleanv(GL_TEXTURE_COORD_ARRAY,	&bTexCoorPointerEnabled);*/

#if 0	// OpenGL ES 1.0 does not support these
		bLighting = glIsEnabled(GL_LIGHTING);
		bCullFace = glIsEnabled(GL_CULL_FACE);
		bFog = glIsEnabled(GL_FOG);
		bDepthTest = glIsEnabled(GL_DEPTH_TEST);
		bVertexProgram = glIsEnabled(GL_VERTEX_PROGRAM_ARB);
		glGetIntegerv(GL_FRONT_FACE, &iFrontFace);
		glGetIntegerv(GL_CULL_FACE_MODE, &iCullFaceMode);

		/* Save matrices */
		glGetIntegerv(GL_MATRIX_MODE, &iMatrixMode);
#endif
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glMatrixMode(GL_PROJECTION);
		glPushMatrix();


		/******************************
		** SET PRINT3D RENDER STATES **
		******************************/

		/* Get viewport dimensions */
		/*glGetFloatv(GL_VIEWPORT, fViewport);*/

		/* Set matrix with viewport dimensions */
		for(i=0; i<16; i++)
		{
			Matrix.f[i]=0;
		}
		Matrix.f[0] =	f2vt(2.0f/(Glob.fScreenScale[0]*640.0f));
		Matrix.f[5] =	f2vt(-2.0f/(Glob.fScreenScale[1]*480.0f));
		Matrix.f[10] = f2vt(1.0f);
		Matrix.f[12] = f2vt(-1.0f);
		Matrix.f[13] = f2vt(1.0f);
		Matrix.f[15] = f2vt(1.0f);

		/* Set matrix mode so that screen coordinates can be specified */
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();

		if(Glob.fScreenScale[0]*640.0f<Glob.fScreenScale[1]*480.0f)
		{
			myglRotate(f2vt(90), f2vt(0), f2vt(0), f2vt(1));
		}

		glMatrixMode(GL_MODELVIEW);
		myglLoadMatrix(Matrix.f);

		/* Disable lighting */
		glDisable(GL_LIGHTING);

		/* Culling */
		glEnable(GL_CULL_FACE);
		glFrontFace(GL_CW);
		glCullFace(GL_FRONT);

		/* Set client states */
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_COLOR_ARRAY);

		glClientActiveTexture(GL_TEXTURE0);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);

		/* texture 	*/
		glActiveTexture(GL_TEXTURE1);
		glDisable(GL_TEXTURE_2D);
		glActiveTexture(GL_TEXTURE0);
		glEnable(GL_TEXTURE_2D);
		myglTexEnv(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);

		/* Blending mode */
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		/* Disable fog */
		glDisable(GL_FOG);

		/* Set Z compare properties */
		glDisable(GL_DEPTH_TEST);

		/* Disable vertex program */
		glDisable(GL_VERTEX_PROGRAM_ARB);

		break;

	case 1:
		/* Restore render states */
		/*if (!bVertexPointerEnabled)	*/	glDisableClientState(GL_VERTEX_ARRAY);
		/*if (!bColorPointerEnabled)	*/	glDisableClientState(GL_COLOR_ARRAY);
		/*if (!bTexCoorPointerEnabled)	*/	glDisableClientState(GL_TEXTURE_COORD_ARRAY);

		/* Restore matrix mode & matrix */
		glMatrixMode(GL_PROJECTION);
		glPopMatrix();
		glMatrixMode(GL_MODELVIEW);
		glPopMatrix();
#if 0	// OpenGL ES 1.0 does not support these
		glMatrixMode(iMatrixMode);

		/* Restore some values */
		if(bLighting)		glEnable(GL_LIGHTING);
		if(!bCullFace)		glDisable(GL_CULL_FACE);
		if(bFog)			glEnable(GL_FOG);
		if(bDepthTest)		glEnable(GL_DEPTH_TEST);
		if(bVertexProgram)	glEnable(GL_VERTEX_PROGRAM_ARB);

		glFrontFace(iFrontFace);
		glCullFace(iCullFaceMode);
#endif

		break;
	}
}

/****************************************************************************
** Local code
****************************************************************************/

/*!***************************************************************************
 @Function			PVRTPrint3DDrawLogo
 @Description		nPos = -1 to the left
					nPos = +1 to the right
*****************************************************************************/
#define LOGO_SIZE 0.3f
#define LOGO_SHIFT 0.05f

static void PVRTPrint3DDrawLogo(PVRTPrint3DGlob &Glob, GLuint tex, int nPos)
{
	static VERTTYPE	VerticesRight[] = {
			f2vt(1.0f-LOGO_SHIFT-LOGO_SIZE)	, f2vt(-1.0f+(LOGO_SIZE*(50.0f/64.0f))+LOGO_SHIFT)	, f2vt(0.5f),
			f2vt(1.0f-LOGO_SHIFT-LOGO_SIZE)	, f2vt(-1.0f+LOGO_SHIFT)							, f2vt(0.5f),
			f2vt(1.0f-LOGO_SHIFT)			, f2vt(-1.0f+(LOGO_SIZE*(50.0f/64.0f))+LOGO_SHIFT)	, f2vt(0.5f),
	 		f2vt(1.0f-LOGO_SHIFT)	 		, f2vt(-1.0f+LOGO_SHIFT)							, f2vt(0.5f)
		};

	static VERTTYPE	VerticesLeft[] = {
			f2vt(-1.0f+LOGO_SHIFT)			, f2vt(-1.0f+(LOGO_SIZE*(50.0f/64.0f))+LOGO_SHIFT)	, f2vt(0.5f),
			f2vt(-1.0f+LOGO_SHIFT)			, f2vt(-1.0f+LOGO_SHIFT)							, f2vt(0.5f),
			f2vt(-1.0f+LOGO_SHIFT+LOGO_SIZE), f2vt(-1.0f+(LOGO_SIZE*(50.0f/64.0f))+LOGO_SHIFT)	, f2vt(0.5f),
	 		f2vt(-1.0f+LOGO_SHIFT+LOGO_SIZE), f2vt(-1.0f+LOGO_SHIFT)							, f2vt(0.5f)
		};

	static VERTTYPE	Colours[] = {
			f2vt(1.0f), f2vt(1.0f), f2vt(1.0f), f2vt(0.75f),
			f2vt(1.0f), f2vt(1.0f), f2vt(1.0f), f2vt(0.75f),
			f2vt(1.0f), f2vt(1.0f), f2vt(1.0f), f2vt(0.75f),
	 		f2vt(1.0f), f2vt(1.0f), f2vt(1.0f), f2vt(0.75f)
		};

	static VERTTYPE	UVs[] = {
			f2vt(0.0f), f2vt(0.0f),
			f2vt(0.0f), f2vt(1.0f),
			f2vt(1.0f), f2vt(0.0f),
	 		f2vt(1.0f), f2vt(1.0f)
		};

	VERTTYPE *pVertices = ( (VERTTYPE*)&VerticesRight );
	VERTTYPE *pColours  = ( (VERTTYPE*)&Colours );
	VERTTYPE *pUV       = ( (VERTTYPE*)&UVs );

	/* Left hand side of the screen */
	if (nPos == -1)
	{
		pVertices = VerticesLeft;
	}

	/* Matrices
	 */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	if(Glob.fScreenScale[0]*640.0f<Glob.fScreenScale[1]*480.0f)
	{
		myglRotate(f2vt(90), f2vt(0), f2vt(0), f2vt(1));
	}

	/* Render states
	 */
	glActiveTexture(GL_TEXTURE0);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, tex);

	glDisable(GL_DEPTH_TEST);

	glEnable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	/* Vertices
	 */
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3,VERTTYPEENUM,0,pVertices);

	glEnableClientState(GL_COLOR_ARRAY);
	glColorPointer(4,VERTTYPEENUM,0,pColours);

	glClientActiveTexture(GL_TEXTURE0);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2,VERTTYPEENUM,0,pUV);

	glDrawArrays(GL_TRIANGLE_STRIP,0,4);

	glDisableClientState(GL_VERTEX_ARRAY);

	glDisableClientState(GL_COLOR_ARRAY);

	glClientActiveTexture(GL_TEXTURE0);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);

	/* Restore render states
	 */
	glDisable (GL_BLEND);
	glEnable(GL_DEPTH_TEST);

}

/*****************************************************************************
 End of file (PVRTPrint3D.cpp)
*****************************************************************************/
