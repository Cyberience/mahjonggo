/******************************************************************************

 @File         PVRTModel3DS.h

 @Title        

 @Copyright    Copyright (C) 2000 - 2006 by Imagination Technologies Limited.

 @Platform     ANSI compatible

 @Description  Functions to load 3DS files.

******************************************************************************/
#ifndef _PVRTMODEL3DS_H_
#define _PVRTMODEL3DS_H_

/*************
** Typedefs **
*************/

/*!***************************************************************************
 Mesh structure
*****************************************************************************/
typedef struct MeshTypeTAG {
	char		pszName[20];		/*!< Name of mesh */
    char		pszMaterial[20];	/*!< Name of material used for this mesh */
	int			nMaterialNumber;	/*!< Index of material used in this mesh */
	float		fCentre[3];			/*!< Mesh centre */
	float		fMinimum[3];		/*!< Bounding box's lower front corner */
	float		fMaximum[3];		/*!< Bounding box's upper back corner */
	int			nNumVertex;			/*!< Number of vertices in the mesh */
	int			nNumFaces;			/*!< Number of triangles in the mesh */
	float		*pVertex;			/*!< List of vertices (x0, y0, z0, x1, y1, z1, x2, etc...) */
	unsigned short		*pFaces;	/*!< List of triangles indices */
	float		*pNormals;			/*!< List of vertex normals (Nx0, Ny0, Nz0, Nx1, Ny1, Nz1, Nx2, etc...) */
	float		*pUV;				/*!< List of UV coordinate (u0, v0, u1, v1, u2, etc...) */
} MeshType;

/*!***************************************************************************
 Material structure
*****************************************************************************/
typedef struct MaterialTypeTAG {
       char		pszMatName[256];	/*!< Material name */
       char		pszMatFile[256];	/*!< Material file name (used if textured) */
	   char		pszMatOpaq[256];	/*!< ? */
	   int		nMatOpacity;		/*!< Material opacity (used with vertex alpha ?) */
       float	fMatAmbient[3];		/*!< Ambient RGB value for material */
       float	fMatDiffuse[3];		/*!< Diffuse RGB value for material */
       float	fMatSpecular[3];	/*!< Specular RGB value for material */
       float	fMatShininess;		/*!< Material shininess */
       float	fMatTransparency;	/*!< Material transparency */
       short	sMatShading;		/*!< Shading mode used with this material */
} MaterialType;

/*!***************************************************************************
 Light structure
*****************************************************************************/
typedef struct LightTypeTAG {
		float	fPosition[3];		/*!< Light position in World coordinates */
		float	fColour[3];			/*!< Light colour (0.0f -> 1.0f for each channel) */
} LightType;

/*!***************************************************************************
 Camera structure
*****************************************************************************/
typedef struct CameraTypeTAG {
		char	pszName[20];		/*!< Name of camera */
		float	fPosition[3];		/*!< Camera position */
		float	fTarget[3];			/*!< Camera looking point */
		float	fRoll;				/*!< Camera roll value */
		float	fFocalLength;		/*!< Camera focal length, in millimeters */
		float	fFOV;				/*!< Field of view */
} CameraType;

/*!***************************************************************************
 Keyframe position structure
*****************************************************************************/
typedef struct _POSITIONKEY {

	int			FrameNumber;
	float		fTension;
	float		fContinuity;
	float		fBias;
	PVRTVECTOR3	p;
} POSITIONKEY;

/*!***************************************************************************
 Keyframe rotation structure
*****************************************************************************/
typedef struct _ROTATIONKEY {

	int			FrameNumber;
	float		fTension;
	float		fContinuity;
	float		fBias;
	float		Angle;
	PVRTVECTOR3	r;
} ROTATIONKEY;

/*!***************************************************************************
 Keyframe scaling structure
*****************************************************************************/
typedef struct _SCALINGKEY {

	int			FrameNumber;
	float		fTension;
	float		fContinuity;
	float		fBias;
	PVRTVECTOR3	s;
} SCALINGKEY;

/*!***************************************************************************
 Node structure
*****************************************************************************/
typedef struct NodeTypeTAG {
	short		ParentIndex;	/*!< Parent node Id */
	short		Mesh;			/*!< Mesh corresponding to this Node Id */
	short		Camera;
	short		Target;
	float		Pivot[3];		/*!< Pivot for this mesh */

	/* Position */
	int			PositionKeys;
	POSITIONKEY	*pPosition;

	/* Rotation */
	int			RotationKeys;
	ROTATIONKEY	*pRotation;

	/* Scaling */
	int			ScalingKeys;
	SCALINGKEY	*pScaling;
} NodeType;


/*!***************************************************************************
 Object structure
*****************************************************************************/
typedef struct ObjectTypeTAG {

	/* Information related to all meshes */
	int				nTotalVertices;		/*!< Total number of vertices in object */
	int				nTotalFaces;		/*!< Total number of faces in object */
	float			fGroupCentre[3];	/*!< Centre of object */
	float			fGroupMinimum[3];	/*!< Bounding box's lower front corner */
	float			fGroupMaximum[3];	/*!< Bounding box's upper back corner */

	/* Meshes defined in the .3DS file */
	int				nNumMeshes;			/*!< Number of meshes composing the object */
	MeshType		*pMesh;				/*!< List of meshes in object */

	/* Materials defined in the .3DS file */
	int				nNumMaterials;		/*!< Number of materials used with object */
	MaterialType	*pMaterial;			/*!< List of materials used with object */

	/* Lights defined in the .3DS file */
	int				nNumLights;			/*!< Number of lights */
	LightType		*pLight;			/*!< List of lights */

	/* Cameras defined in the .3DS file */
	int				nNumCameras;		/*!< Number of cameras */
	CameraType		*pCamera;			/*!< List of cameras */

	/* This is used for animation only. Ignore if animation is not to be used */
	int				nNumFrames;			/*!< Number of frames */
	int				nNumNodes;			/*!< Number of nodes */
	NodeType		*pNode;				/*!< List of nodes */
} ObjectType;


/***************
** Prototypes **
***************/

/*!***************************************************************************
 @Function			PVRTModel3DSRead
 @Output			pObject				Object containing the loaded 3DS data
 @Input				Name3DSFile			Name of the 3DS file to load
 @Return			true or false
 @Description		Read a 3DS file and create an object structure from it.
					After use, the object structure must be destroyed with a
					call to PVRTModel3DSDestroy().
					The 3DS file can either be a resource or file.
					If stored in a resource, the resource identifier must be "TDS".
*****************************************************************************/
bool PVRTModel3DSRead(ObjectType *pObject, char *Name3DSFile);

/*!***************************************************************************
 @Function			PVRTModel3DSDestroy
 @Modified			pObject				Object containing the loaded 3DS data
 @Return			true or false
 @Description		Destroy an object allocated with PVRTModel3DSRead()
*****************************************************************************/
void PVRTModel3DSDestroy(ObjectType *pObject);

/*!***************************************************************************
 @Function			PVRTModel3DSDisplayInfo
 @Modified			pObject				Object containing the loaded 3DS data
 @Description		Display model data into debug output
*****************************************************************************/
void PVRTModel3DSDisplayInfo(ObjectType *pObject);

/*!***************************************************************************
 @Function			PVRTModel3DSScale
 @Modified			pObject				Object containing the loaded 3DS data
 @Input				Scale				Scale to apply
 @Description		Scale a model's vertices with a uniform scaling value
*****************************************************************************/
void PVRTModel3DSScale(ObjectType *pObject, const float Scale);

/*!***************************************************************************
 @Function			PVRTModel3DSGetAnimationMatrix
 @Input				pObject				Object containing the loaded 3DS data
 @Input				nMesh				Mesh number
 @Input				fFrameNumber		Frame number
 @Output			pAnimationMatrix	Animation matrix at this frame
 @Return			true or false
 @Description		Return animation matrix required to animate this mesh at the
					specified frame.
					Frame number is a floating point number as (linear)
					interpolation will be used to compute the required matrix at
					this frame.
*****************************************************************************/
bool PVRTModel3DSGetAnimationMatrix(ObjectType *pObject, short nMesh, float fFrameNumber, PVRTMATRIX *pAnimationMatrix);

/*!***************************************************************************
 @Function			PVRTModel3DSGetCameraAnimation
 @Output			pPosition			Camera position at this frame
 @Output			pTarget				Camera target at this frame
 @Input				pObject				Object containing the loaded 3DS data
 @Input				nCamera				Number of the camera
 @Input				fFrameNumber		Frame number
 @Return			true or false
 @Description		Return position and target required to animate this camera at the
					specified frame. Frame number is a floating point number as (linear)
					interpolation will be used to compute the required matrix at
					this frame.
*****************************************************************************/
bool PVRTModel3DSGetCameraAnimation(ObjectType *pObject, short nCamera, float fFrameNumber,	PVRTVECTOR3 *pPosition, PVRTVECTOR3 *pTarget);


#endif

/*****************************************************************************
 End of file (PVRTModel3DS.h)
*****************************************************************************/
