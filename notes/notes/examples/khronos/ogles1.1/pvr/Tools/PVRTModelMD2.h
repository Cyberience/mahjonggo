/******************************************************************************

 @File         PVRTModelMD2.h

 @Title        

 @Copyright    Copyright (C) 2005 - 2006 by Imagination Technologies Limited.

 @Platform     ANSI compatible

 @Description  Header to load MD2 files. Based on MD2 Tutorial by David Henry -
               tfc_duke@hotmail.com Converted, fixes and some optimisations for
               usage with OGLES

******************************************************************************/
#ifndef		__MD2_H
#define		__MD2_H

#ifndef IW_USE_SYSTEM_STDLIB
	#define IW_USE_SYSTEM_STDLIB
#endif

#include <new>

/****************************************************************************
** Defines
****************************************************************************/

// number of precalculated normals
#define NUMVERTEXNORMALS		162

// precalculated normal vectors
#define SHADEDOT_QUANT			16

// magic number "IDP2" or 844121161
#define MD2_IDENT				(('2'<<24) + ('P'<<16) + ('D'<<8) + 'I')

// model version
#define	MD2_VERSION				8

// maximum number of vertices for a MD2 model
#define MAX_MD2_VERTS			2048

typedef float vec3_t[3];

/****************************************************************************
** Structures
****************************************************************************/

/*!***************************************************************************
 md2 header
*****************************************************************************/
typedef struct
{
	int		ident;				/*!< magic number. must be equal to "IPD2" */
	int		version;			/*!< md2 version. must be equal to 8 */

	int		skinwidth;			/*!< width of the texture */
	int		skinheight;			/*!< height of the texture */
	int		framesize;			/*!< size of one frame in bytes */

	int		num_skins;			/*!< number of textures */
	int		num_xyz;			/*!< number of vertices */
	int		num_st;				/*!< number of texture coordinates */
	int		num_tris;			/*!< number of triangles */
	int		num_glcmds;			/*!< number of opengl commands */
	int		num_frames;			/*!< total number of frames */

	int		ofs_skins;			/*!< offset to skin names (64 bytes each) */
	int		ofs_st;				/*!< offset to s-t texture coordinates */
	int		ofs_tris;			/*!< offset to triangles */
	int		ofs_frames;			/*!< offset to frame data */
	int		ofs_glcmds;			/*!< offset to opengl commands */
	int		ofs_end;			/*!< offset to the end of file */

} md2_t;

/*!***************************************************************************
 vertex
*****************************************************************************/
typedef struct
{
	unsigned char	v[3];				/*!< compressed vertex' (x, y, z) coordinates */
	unsigned char	lightnormalindex;	/*!< index to a normal vector for the lighting */

} vertex_t;

/*!***************************************************************************
 frame
*****************************************************************************/
typedef struct
{
	float		scale[3];		/*!< scale values */
	float		translate[3];	/*!< translation vector */
	char		name[16];		/*!< frame name */
	vertex_t	verts[1];		/*!< first vertex of this frame */

} frame_t;

/*!***************************************************************************
 animation
*****************************************************************************/
typedef struct
{
	int		first_frame;			/*!< first frame of the animation */
	int		last_frame;				/*!< number of frames */
	int		fps;					/*!< number of frames per second */

} anim_t;

/*!***************************************************************************
 status animation
*****************************************************************************/
typedef struct
{
	int		startframe;				/*!< first frame */
	int		endframe;				/*!< last frame */
	int		fps;					/*!< frame per second for this animation */

	float	curr_time;				/*!< current time */
	float	old_time;				/*!< old time */
	float	interpol;				/*!< percent of interpolation */

	int		type;					/*!< animation type */

	int		curr_frame;				/*!< current frame */
	int		next_frame;				/*!< next frame */

} animState_t;

/*!***************************************************************************
 Main Data Storage for Float Case
*****************************************************************************/
typedef struct
{
	unsigned char	*pColorArray;			/*!< Pointer to the Color Buffer (4 elements per vertex) */
	float			*pTexCoordArray;		/*!< Pointer to the TexCoord Buffer (3 elements per vertex) */
	float			*pNormalArray;			/*!< Pointer to the Normal Buffer (3 elements per vertex) */
	float			*pPositionArray;		/*!< Pointer to the Position Buffer (3 elements per vertex) */
	int				NumberOfFans;			/*!< Number of Fans. */
	int				NumberOfStrips;			/*!< Number of Strips. */
	int				Offset2Strips;			/*!< Offset between fan and strip vertices/normals/etc. */
	int				*pFanLengths;			/*!< Number of indices for each fan. */
	int				*pStripLengths;			/*!< Number of indices for each strip. */
} MyFloatDrawArray;

/*!***************************************************************************
 Main Data Storage for Fixed Point Case
*****************************************************************************/
typedef struct
{
	unsigned char	*pColorArray;			/*!< Pointer to the Color Buffer (4 elements per vertex) */
	int				*pTexCoordArray;		/*!< Pointer to the TexCoord Buffer (3 elements per vertex) */
	int				*pNormalArray;			/*!< Pointer to the Normal Buffer (3 elements per vertex) */
	int				*pPositionArray;		/*!< Pointer to the Position Buffer (3 elements per vertex) */
	int				NumberOfFans;			/*!< Number of Fans. */
	int				NumberOfStrips;			/*!< Number of Strips. */
	int				Offset2Strips;			/*!< Offset between fan and strip vertices/normals/etc. */
	int				*pFanLengths;			/*!< Number of indices for each fan. */
	int				*pStripLengths;			/*!< Number of indices for each strip. */
} MyFixedDrawArray;

/****************************************************************************
** Enumeration
****************************************************************************/

/*!***************************************************************************
 animation list
*****************************************************************************/
typedef enum {
	STAND,
	RUN,
	ATTACK,
	PAIN_A,
	PAIN_B,
	PAIN_C,
	JUMP,
	FLIP,
	SALUTE,
	FALLBACK,
	WAVE,
	POINTING,
	CROUCH_STAND,
	CROUCH_WALK,
	CROUCH_ATTACK,
	CROUCH_PAIN,
	CROUCH_DEATH,
	DEATH_FALLBACK,
	DEATH_FALLFORWARD,
	DEATH_FALLBACKSLOW,
	BOOM,

	MAX_ANIMATIONS

} animType_t;

/*!***************************************************************************
 CMD2Model - MD2 model class object.
*****************************************************************************/
class CMD2Model
{
public:
	/*!***********************************************************************
	 @Function			Constructor
	 @Description		Init all Class Data
	*************************************************************************/
	CMD2Model( void );
	/*!***********************************************************************
	@Function			Destructor
	@Description		Free all allocated Resources
	*************************************************************************/
	~CMD2Model( void );

	// functions
	/*!***********************************************************************
	 @Function		LoadModelFloatFromFile
	 @Input			filename		File to load from
	 @Return		True if OK, False if there was a problem
	 @Description	Loads the specified ".MD2" file from disk into Float Format
	*************************************************************************/
	bool	LoadModelFloatFromFile( const char *filename );

	/*!***********************************************************************
	 @Function		LoadModelFixedFromFile
	 @Input			filename		File to load from
	 @Return		True if OK, False if there was a problem
	 @Description	Loads the specified ".MD2" file from disk into Fixed Format
	*************************************************************************/
	bool	LoadModelFixedFromFile( const char *filename );

	/*!***********************************************************************
	 @Function		LoadModelFloatFromHeader
	 @Input			headername		Header to load the model from
	 @Return		True if OK, False if there was a problem
	 @Description	Loads the specified ".MD2" file from disk into Float Format
	*************************************************************************/
	bool	LoadModelFloatFromHeader( const char *headername );

	/*!***********************************************************************
	 @Function		LoadModelFixedFromHeader
	 @Input			headername		Header to load the model from
	 @Return		True if OK, False if there was a problem
	 @Description	Loads the specified ".MD2" file from disk into Fixed Format
	*************************************************************************/
	bool	LoadModelFixedFromHeader( const char *headername );

	/*!***********************************************************************
	 @Function		SetAnim
	 @Input			type		One of the animType_t Enumerations
	 @Description	Selects the current Animation to playback.
	*************************************************************************/
	void	SetAnim( int type );

	/*!***********************************************************************
	 @Function		ScaleModel
	 @Input			s		Scale to apply
	 @Description	Sets the scale used when generating the Model Data.
	*************************************************************************/
	void	ScaleModel( float s );

	/*!***********************************************************************
	 @Function		GetNumberOfVertices
	 @Return		Number of actual vertices
	 @Description	Returns number of vertices in buffer.
	*************************************************************************/
	int		GetNumberOfVertices();

	/*!***********************************************************************
	 @Function		GetModelDataPntrsFloat
	 @Output		pPosition		Vertex array
	 @Output		pTexCoord		Texture coordinates array
	 @Output		pNormal			Normal array
	 @Output		pColor			Color array
	 @Output		NumberOfFans	Number of fans
	 @Output		NumberOfStrips	Number of strips
	 @Output		Offset2Strips	Offset in bytes to strips
	 @Output		pFanLengths		Lengths of the fans
	 @Output		pStripLengths	Lengths of the strips
	 @Input			time			Time in the animation to get the mesh at
	 @Description	Outputs a Vertex Position Array and optionally Normal,
					TexCoord and Color Arrays which can be used to render
					the MD2 Model. In addition the number of Fans and Strips
					and their respective lengths (Array) are reported. Also
					provide is the offset to locate the Strips. In the Arrays
					Fans are stored first and Strips are stored after them.
					This is the Float Format version. Do not use on objects
					loaded in Fixed Format !
	*************************************************************************/
	void	GetModelDataPntrsFloat(float time, float **pPosition, float **pTexCoord, float **pNormal, unsigned char **pColor, int *NumberOfFans, int *NumberOfStrips, int *Offset2Strips, int **pFanLengths, int **pStripLengths);

	/*!***********************************************************************
	 @Function		GetModelDataPntrsFixed
	 @Output		pPosition		Vertex array
	 @Output		pTexCoord		Texture coordinates array
	 @Output		pNormal			Normal array
	 @Output		pColor			Color array
	 @Output		NumberOfFans	Number of fans
	 @Output		NumberOfStrips	Number of strips
	 @Output		Offset2Strips	Offset in bytes to strips
	 @Output		pFanLengths		Lengths of the fans
	 @Output		pStripLengths	Lengths of the strips
	 @Input			time			Time in the animation to get the mesh at
	 @Description	Outputs a Vertex Position Array and optionally Normal,
					TexCoord and Color Arrays which can be used to render
					the MD2 Model. In addition the number of Fans and Strips
					and their respective lengths (Array) are reported. Also
					provide is the offset to locate the Strips. In the Arrays
					Fans are stored first and Strips are stored after them.
					This is the Fixed Format version. Do not use on objects
					loaded in Float Format !
	*************************************************************************/
	void	GetModelDataPntrsFixed(float time, int   **pPosition, int   **pTexCoord, int   **pNormal, unsigned char **pColor, int *NumberOfFans, int *NumberOfStrips, int *Offset2Strips, int **pFanLengths, int **pStripLengths);

	/*!***********************************************************************
	 @Function		GenerateTriangleList
	 @Return		Index List
	 @Description	Outputs a triangle index list to allow for single
					draw call to draw the whole object. Unsigned Short data format returned.
	*************************************************************************/
	unsigned short* GenerateTriangleList( void );

private:	// These are tool functions  used by the Public Functions
	void	Animate( float time );						/*!< Update Animation Params */
	void	ProcessLighting( void );					/*!< Update Lighting Data */
	void	InterpolatePosition( vec3_t *vertlist);		/*!< Interpolate the Position based on Animation */
	void	InterpolateNormal( vec3_t *normlist );		/*!< Interpolate the Normal based on Animation */
	bool	InitMyArraysFloat( void );					/*!< Init the Arrays for usage - Memory Allocation */
	bool	InitMyArraysFixed( void );					/*!< Init the Arrays for usage - Memory Allocation */
	void	DestroyMyArrays( void );					/*!< Destroy the Array data - Memory De-Allocation */

public:
	// member variables - these are Look-up Tables
	static vec3_t	anorms[ NUMVERTEXNORMALS ];
	static float	anorms_dots[ SHADEDOT_QUANT ][256];

	static anim_t	animlist[21];		/*!< animation list */
	int				TriangleCount;


private:
	// some private class variables
	int					num_frames;				/*!< number of frames */
	int					num_xyz;				/*!< number of vertices */
	int					num_glcmds;				/*!< number of opengl commands */

	vec3_t				*m_vertices;			/*!< vertex array */
	int					*m_glcmds;				/*!< opengl command array */
	int					*m_lightnormals;		/*!< normal index array */

	unsigned int		m_texid;				/*!< texture id */
	animState_t			m_anim;					/*!< animation */
	float				m_scale;				/*!< scale value */

	MyFloatDrawArray	MyFloatFansAndStrips;	/*!< Single Buffer containing Fans and then Strips Data */
	MyFixedDrawArray	MyFixedFansAndStrips;	/*!< Single Buffer containing Fans and then Strips Data */
	unsigned short*		Indices;				/*!< Triangle Index List */

	float				oldtime;				/*!< Old time, decide if we need to update the geometry data */

	bool				ObjectIsFloat;			/*!< Or is it Fixed ? */

	int					MyVertexCount;
};


#endif	// __MD2_H
