/******************************************************************************

 @File         PVRTModelMD2.cpp

 @Title        

 @Copyright    Copyright (C) 2005 - 2006 by Imagination Technologies Limited.

 @Platform     ANSI compatible

 @Description  Code to load MD2 files. Based on MD2 Tutorial by David Henry -
               tfc_duke@hotmail.com Converted, fixes and some optimisations for
               usage with OGLES

******************************************************************************/

#include 	"PVRTModelMD2.h"

#include "s3eFile.h"

#include 	<math.h>
#include	<string.h>
#include	<stdio.h>


// Float to Fixed Stuff
#define fix_scale (1<<16)
#define float2fix(f) ((int)((f)*(float)fix_scale))

// precalculated normal vectors
vec3_t	CMD2Model::anorms[ NUMVERTEXNORMALS ] = {
#include	"PVRTModelMD2Anorms.h"
};

// precalculated dot product results
float	CMD2Model::anorms_dots[ SHADEDOT_QUANT ][256] = {
#include	"PVRTModelMD2Anormtab.h"
};

static float	*shadedots = CMD2Model::anorms_dots[0];
static vec3_t	lcolor;

vec3_t			g_lightcolor	= { 1.0, 1.0, 1.0 };
int				g_ambientlight	= 32;
float			g_shadelight	= 128;
float			g_angle			= 0.0;

/*!***************************************************************************
 @Function			Constructor
 @Description		Init all Class Data
*****************************************************************************/
CMD2Model::CMD2Model( void )
{
	/* Init all values to defaults */
	memset(this, 0, sizeof(*this));
	m_scale	= 1.0;
	oldtime	= -999.92f;
	SetAnim( 0 );
}

/*!***************************************************************************
 @Function			Destructor
 @Description		Free all allocated Resources
*****************************************************************************/
CMD2Model::~CMD2Model( void )
{
	delete [] m_vertices;
	delete [] m_glcmds;
	delete [] m_lightnormals;

	DestroyMyArrays();
}

/*!***************************************************************************
 @Function			LoadModelFloatFromFile
 @Input				filename		File to load from
 @Return			True if OK, False if there was a problem
 @Description		Loads the specified ".MD2" file from disk into Float Format
*****************************************************************************/
bool CMD2Model::LoadModelFloatFromFile( const char *filename )
{
	FILE			*file = NULL;			// declare a FILE pointer
	md2_t			header;					// md2 header
	char			*buffer = NULL;			// buffer storing frame data
	frame_t			*frame = NULL;			// temporary variable
	vec3_t			*ptrverts = NULL;		// pointer on m_vertices
	int				*ptrnormals = NULL;		// pointer on m_lightnormals

	/* open a binary file for reading */
	file = (FILE *)s3eFileOpen(filename, "rb"); 
	if(file==NULL)
	{
		/* Failed to open file */
		return false;
	}
	
	/* Keep track of the fact that this object is floating point */
	ObjectIsFloat=true;
	
	/* Read Header */
	int readcount = (int)s3eFileRead((char *)&header,1,sizeof( md2_t ), (s3eFile *)file);
	if(readcount != sizeof( md2_t ))
	{
		/* Failed to read all data from file */
		return false;
	}

	/* Verify that this is a MD2 file by checking for the ident and the version number */
	if( (header.ident != MD2_IDENT) && (header.version != MD2_VERSION) )
	{
		// this is not a MD2 model
		s3eFileClose((s3eFile *)file);
		return false;
	}
  
	/* initialize member variables */
	num_frames	= header.num_frames;
	num_xyz		= header.num_xyz;
	num_glcmds	= header.num_glcmds;

	/* allocate memory */
	m_vertices		= new vec3_t[ num_xyz * num_frames ];
	m_glcmds		= new int[ num_glcmds ];
	m_lightnormals	= new int[ num_xyz * num_frames ];
	buffer			= new char[ num_frames * header.framesize ];

	if (m_vertices== NULL || m_glcmds == NULL || m_lightnormals == NULL || buffer == NULL)
	{
		/* Memory Allocation Failed */
		return false;
	}

	/* Read MD2 Data into structures */
	/* Read frame data... */
	if(s3eFileSeek((s3eFile *)file,header.ofs_frames,(s3eFileSeekOrigin)SEEK_SET)!=0)
	{
		/* Failed to Seek the correct position in the file */
		return false;
	}

	readcount = (int)s3eFileRead((void*)buffer,1,num_frames * header.framesize,(s3eFile *)file);
	
	if(readcount != num_frames * header.framesize)
	{
		/* Failed to read all data from file */
		return false;	
	}

	/* Read opengl commands */
	if(s3eFileSeek((s3eFile *)file,header.ofs_glcmds,(s3eFileSeekOrigin)SEEK_SET)!=0)
	{
		/* Failed to Seek the correct position in the file */
		return false;
	}

	readcount = (int)s3eFileRead((void*)m_glcmds,1,num_glcmds * sizeof( int ),(s3eFile *)file);
	if (readcount != num_glcmds * ((int)sizeof( int )))
	{
		/* Failed to read all data from file */
		return false;
	}

	/* Vertex array initialization */
	for( int j = 0; j < num_frames; j++ )
	{
		/* Adjust pointers */
		frame		= (frame_t *)&buffer[ header.framesize * j ];
		ptrverts	= &m_vertices[ num_xyz * j ];
		ptrnormals	= &m_lightnormals[ num_xyz * j ];

		/* Store data */
		for( int i = 0; i < num_xyz; i++ )
		{
			ptrverts[i][0] = (frame->verts[i].v[0] * frame->scale[0]) + frame->translate[0];
			ptrverts[i][1] = (frame->verts[i].v[1] * frame->scale[1]) + frame->translate[1];
			ptrverts[i][2] = (frame->verts[i].v[2] * frame->scale[2]) + frame->translate[2];

			ptrnormals[i] = frame->verts[i].lightnormalindex;
		}
	}

	/* free buffer's memory */
	delete [] buffer;

	/* Close the file */
	s3eFileClose((s3eFile *)file);

	/* Now that we have read the data store into Float arrays for future usage */
	if(!InitMyArraysFloat())
	{
		/* Something failed */
		return false;
	}

	return true;
}

/*!***************************************************************************
 @Function		LoadModelFixedFromFile
 @Input			filename		File to load from
 @Return		True if OK, False if there was a problem
 @Description	Loads the specified ".MD2" file from disk into Fixed Format
*****************************************************************************/
bool CMD2Model::LoadModelFixedFromFile( const char *filename )
{
	FILE			*file		= NULL;			// declare a FILE pointer
	md2_t			header;						// md2 header
	char			*buffer		= NULL;			// buffer storing frame data
	frame_t			*frame		= NULL;			// temporary variable
	vec3_t			*ptrverts	= NULL;			// pointer on m_vertices
	int				*ptrnormals	= NULL;			// pointer on m_lightnormals

	/* open a binary file for reading */
	file = (FILE *)s3eFileOpen(filename, "rb"); 
	if(file==NULL)
	{
		/* Failed to open file */
		return false;
	}

	/* Keep track of the fact that we are dealing with Fixed Point and not Float data */
	ObjectIsFloat=false;

	/* Read MD2 Header */
	int readcount = (int)s3eFileRead((char *)&header,1,sizeof( md2_t ),(s3eFile *)file);
	if(readcount != sizeof( md2_t ))
	{
		/* Failed to read all data from file */
		return false;
	}

	/* Verify that this is a MD2 file by checking for the ident and the version number */
	if( (header.ident != MD2_IDENT) && (header.version != MD2_VERSION) )
	{
		// this is not a MD2 model
		s3eFileClose((s3eFile *)file);
		return false;
	}

	// initialize member variables
	num_frames	= header.num_frames;
	num_xyz		= header.num_xyz;
	num_glcmds	= header.num_glcmds;

	// allocate memory
	m_vertices		= new vec3_t[ num_xyz * num_frames ];
	m_glcmds		= new int[ num_glcmds ];
	m_lightnormals	= new int[ num_xyz * num_frames ];
	buffer			= new char[ num_frames * header.framesize ];

	if (m_vertices== NULL || m_glcmds == NULL || m_lightnormals == NULL || buffer == NULL)
	{
		/* Memory Allocation Failed */
		return false;
	}

	/* Reading file data */
	/* read frame data... */
	if(s3eFileSeek((s3eFile *)file,header.ofs_frames,(s3eFileSeekOrigin)SEEK_SET)!=0)
	{
		/* Failed to Seek the correct position in the file */
		return false;
	}
	
	readcount = (int)s3eFileRead((void*)buffer,1,num_frames * header.framesize,(s3eFile *)file);
	if(readcount != num_frames * header.framesize)
	{
		/* Failed to read all data from file */
		return false;
	}

	/* read opengl commands... */
	if(s3eFileSeek((s3eFile *)file,header.ofs_glcmds,(s3eFileSeekOrigin)SEEK_SET)!=0)
	{
		/* Failed to Seek the correct position in the file */
		return false;
	}
	readcount = (int)s3eFileRead((void*)m_glcmds,1,num_glcmds * sizeof( int ),(s3eFile *)file);
	if (readcount != num_glcmds * ((int)sizeof( int )))
	{
		/* Failed to read all data from file */
		return false;
	}

	/* vertex array initialization */
	for( int j = 0; j < num_frames; j++ )
	{
		/* adjust pointers */
		frame		= (frame_t *)&buffer[ header.framesize * j ];
		ptrverts	= &m_vertices[ num_xyz * j ];
		ptrnormals	= &m_lightnormals[ num_xyz * j ];

		/* Read and Store Data */
		for( int i = 0; i < num_xyz; i++ )
		{
			ptrverts[i][0] = (frame->verts[i].v[0] * frame->scale[0]) + frame->translate[0];
			ptrverts[i][1] = (frame->verts[i].v[1] * frame->scale[1]) + frame->translate[1];
			ptrverts[i][2] = (frame->verts[i].v[2] * frame->scale[2]) + frame->translate[2];

			ptrnormals[i] = frame->verts[i].lightnormalindex;
		}
	}

	// free buffer's memory
	delete [] buffer;

	// close the file
	s3eFileClose((s3eFile *)file);

	/* Generate Fixed Point Buffers for future usage */
	if(!InitMyArraysFixed())
	{
		/* Something went wrong */
		return false;
	}

	return true;
}

/*!***************************************************************************
 @Function			LoadModelFloatFromHeader
 @Input				headername		Header to load the model from
 @Return			True if OK, False if there was a problem
 @Description		Loads the specified ".MD2" file from disk into Float Format
*****************************************************************************/
bool CMD2Model::LoadModelFloatFromHeader( const char *headername )
{
	md2_t			header;			// md2 header
	char			*buffer;		// buffer storing frame data
	frame_t			*frame;			// temporary variable
	vec3_t			*ptrverts;		// pointer on m_vertices
	int				*ptrnormals;	// pointer on m_lightnormals

	/* Keep track of the fact that we are dealing with Float data */
	ObjectIsFloat=true;

	/* Read header */
	memcpy((char *)&header, (char *)headername, sizeof( md2_t ));

	/* Verify that this is a MD2 file by checking for the ident and the version number */
	if( (header.ident != MD2_IDENT) && (header.version != MD2_VERSION) )
	{
		// this is not a MD2 model
		return false;
	}

	// initialize member variables
	num_frames	= header.num_frames;
	num_xyz		= header.num_xyz;
	num_glcmds	= header.num_glcmds;

	// allocate memory
	m_vertices		= new vec3_t[ num_xyz * num_frames ];
	m_glcmds		= new int[ num_glcmds ];
	m_lightnormals	= new int[ num_xyz * num_frames ];
	buffer			= new char[ num_frames * header.framesize ];

	if (m_vertices== NULL || m_glcmds == NULL || m_lightnormals == NULL || buffer == NULL)
	{
		/* Memory Allocation Failed */
		return false;
	}

	/* Reading file data */
	// read frame data...
	memcpy((char *)buffer, &(((char *)headername)[header.ofs_frames]), num_frames * header.framesize);

	// read opengl commands...
	memcpy((char *)m_glcmds, &(((char *)headername)[header.ofs_glcmds]), num_glcmds * sizeof( int ));

	// vertex array initialization
	for( int j = 0; j < num_frames; j++ )
	{
		// adjust pointers
		frame		= (frame_t *)&buffer[ header.framesize * j ];
		ptrverts	= &m_vertices[ num_xyz * j ];
		ptrnormals	= &m_lightnormals[ num_xyz * j ];

		/* Store Data */
		for( int i = 0; i < num_xyz; i++ )
		{
			ptrverts[i][0] = (frame->verts[i].v[0] * frame->scale[0]) + frame->translate[0];
			ptrverts[i][1] = (frame->verts[i].v[1] * frame->scale[1]) + frame->translate[1];
			ptrverts[i][2] = (frame->verts[i].v[2] * frame->scale[2]) + frame->translate[2];

			ptrnormals[i] = frame->verts[i].lightnormalindex;
		}
	}

	// free buffer's memory
	delete [] buffer;

	/* When we have the data generate buffers for future usage */
	InitMyArraysFloat();

	return true;
}

// Fixed Version
/*!***************************************************************************
 @Function			LoadModelFixedFromHeader
 @Input				headername		Header to load the model from
 @Return			True if OK, False if there was a problem
 @Description		Loads the specified ".MD2" file from disk into Fixed Format
*****************************************************************************/
bool CMD2Model::LoadModelFixedFromHeader( const char *headername )
{
	md2_t			header;			// md2 header
	char			*buffer;		// buffer storing frame data
	frame_t			*frame;			// temporary variable
	vec3_t			*ptrverts;		// pointer on m_vertices
	int				*ptrnormals;	// pointer on m_lightnormals

	/* Keep track of the fact that we are dealing with Fixed Point data and not Floating point */
	ObjectIsFloat=false;

	/* Read header */
	memcpy((char *)&header, (char *)headername, sizeof( md2_t ));

	/* Verify that this is a MD2 file by checking for the ident and the version number */
	if( (header.ident != MD2_IDENT) && (header.version != MD2_VERSION) )
	{
		// this is not a MD2 model
		return false;
	}

	// initialize member variables
	num_frames	= header.num_frames;
	num_xyz		= header.num_xyz;
	num_glcmds	= header.num_glcmds;


	// allocate memory
	m_vertices		= new vec3_t[ num_xyz * num_frames ];
	m_glcmds		= new int[ num_glcmds ];
	m_lightnormals	= new int[ num_xyz * num_frames ];
	buffer			= new char[ num_frames * header.framesize ];

	if (m_vertices== NULL || m_glcmds == NULL || m_lightnormals == NULL || buffer == NULL)
	{
		/* Memory Allocation Failed */
		return false;
	}

	/* Reading file data */
	// read frame data...
	memcpy((char *)buffer, &(((char *)headername)[header.ofs_frames]), num_frames * header.framesize);

	// read opengl commands...
	memcpy((char *)m_glcmds, &(((char *)headername)[header.ofs_glcmds]), num_glcmds * sizeof( int ));

	// vertex array initialization
	for( int j = 0; j < num_frames; j++ )
	{
		// adjust pointers
		frame		= (frame_t *)&buffer[ header.framesize * j ];
		ptrverts	= &m_vertices[ num_xyz * j ];
		ptrnormals	= &m_lightnormals[ num_xyz * j ];

		/* Store Data */
		for( int i = 0; i < num_xyz; i++ )
		{
			ptrverts[i][0] = (frame->verts[i].v[0] * frame->scale[0]) + frame->translate[0];
			ptrverts[i][1] = (frame->verts[i].v[1] * frame->scale[1]) + frame->translate[1];
			ptrverts[i][2] = (frame->verts[i].v[2] * frame->scale[2]) + frame->translate[2];

			ptrnormals[i] = frame->verts[i].lightnormalindex;
		}
	}

	// free buffer's memory
	delete [] buffer;

	/* When we have the data generate buffers for future usage */
	InitMyArraysFixed();

	return true;
}

/*!***************************************************************************
 @Function			Animate
 @Input				Time
 @Description		Calculate the current frame, next frame and interpolation percent.
*****************************************************************************/
void CMD2Model::Animate( float time )
{
	m_anim.curr_time = time;

	// calculate current and next frames
	if( m_anim.curr_time - m_anim.old_time > (1.0 / m_anim.fps) )
	{
		m_anim.curr_frame = m_anim.next_frame;
		m_anim.next_frame++;

		if( m_anim.next_frame > m_anim.endframe )
			m_anim.next_frame = m_anim.startframe;

		m_anim.old_time = m_anim.curr_time;
	}

	// prevent having a current/next frame greater
	// than the total number of frames...
	if( m_anim.curr_frame > (num_frames - 1) )
		m_anim.curr_frame = 0;

	if( m_anim.next_frame > (num_frames - 1) )
		m_anim.next_frame = 0;

	m_anim.interpol = m_anim.fps * (m_anim.curr_time - m_anim.old_time);
}

/*****************************************************************************
 @Function			ProcessLighting
 @Description		Process all lighting calculus.
*****************************************************************************/
void CMD2Model::ProcessLighting( void )
{
	float lightvar = (float)((g_shadelight + g_ambientlight)/256.0);

	lcolor[0] = g_lightcolor[0] * lightvar;
	lcolor[1] = g_lightcolor[1] * lightvar;
	lcolor[2] = g_lightcolor[2] * lightvar;

	shadedots = anorms_dots[ ((int)(g_angle * (SHADEDOT_QUANT / 360.0))) & (SHADEDOT_QUANT - 1) ];
}

/*!***************************************************************************
 @Function			GetNumberOfVertices
 @Output			Number of vertices
 @Description		Return the number of vertices
*****************************************************************************/
int	CMD2Model::GetNumberOfVertices( void )
{
	return MyVertexCount;
}

/*!***************************************************************************
 @Function			ScaleModel
 @Input				Scale
 @Description		Set Global Object Scale for the MD2 model
*****************************************************************************/
void CMD2Model::ScaleModel( float s )
{
	m_scale = s;
}

/*!***************************************************************************
 @Function			InterpolatePosition
 @Input				Vertex List
 @Description		Interpolate and scale vertices using the current and the next frame data.
*****************************************************************************/
void CMD2Model::InterpolatePosition( vec3_t *vertlist)
{
	vec3_t	*curr_v;	// pointer to current frame vertices
	vec3_t	*next_v;	// pointer to next frame vertices

	int i;

	// create current frame and next frame's vertex list
	// from the whole vertex list
	curr_v = &m_vertices[ num_xyz * m_anim.curr_frame ];
	next_v = &m_vertices[ num_xyz * m_anim.next_frame ];

	// interpolate and scale vertices to avoid ugly animation
	for( i = 0; i < num_xyz ; i++ )
	{
		vertlist[i][0] = (curr_v[i][0] + m_anim.interpol * (next_v[i][0] - curr_v[i][0])) * m_scale;
		vertlist[i][1] = (curr_v[i][1] + m_anim.interpol * (next_v[i][1] - curr_v[i][1])) * m_scale;
		vertlist[i][2] = (curr_v[i][2] + m_anim.interpol * (next_v[i][2] - curr_v[i][2])) * m_scale;
	}
}

/*!***************************************************************************
 @Function			InterpolateNormal
 @Input				Normal List
 @Description		Interpolate and scale normals from the current and the next frame.
*****************************************************************************/
void CMD2Model::InterpolateNormal( vec3_t *normlist )
{
	int	*curr_n;	// pointer to current frame normals
	int	*next_n;	// pointer to next frame normals

	int i;

	// create current frame and next frame's normal list
	// from the whole normal list
	curr_n = &m_lightnormals[ num_xyz * m_anim.curr_frame ];
	next_n = &m_lightnormals[ num_xyz * m_anim.next_frame ];

	// interpolate and scale normals to avoid ugly animation
	for( i = 0; i < num_xyz ; i++ )
	{
		float x = anorms[curr_n[i]][0];
		float y = anorms[curr_n[i]][1];
		float z = anorms[curr_n[i]][2];

		float nx = anorms[next_n[i]][0];
		float ny = anorms[next_n[i]][1];
		float nz = anorms[next_n[i]][2];

		normlist[i][0] = (x + m_anim.interpol * (nx - x));
		normlist[i][1] = (y + m_anim.interpol * (ny - y));
		normlist[i][2] = (z + m_anim.interpol * (nz - z));

		float f;

		f = 1.0f / (float)sqrt(normlist[i][0] * normlist[i][0] + normlist[i][1] * normlist[i][1] + normlist[i][2] * normlist[i][2]);

		normlist[i][0] = normlist[i][0] * f;
		normlist[i][1] = normlist[i][1] * f;
		normlist[i][2] = normlist[i][2] * f;
	}
}

// ----------------------------------------------
// initialize the 21 MD2 model animations.
// ----------------------------------------------
anim_t CMD2Model::animlist[ 21 ] =
{
	// first, last, fps

	{   0,  39,  9 },	// STAND
	{  40,  45, 10 },	// RUN
	{  46,  53, 10 },	// ATTACK
	{  54,  57,  7 },	// PAIN_A
	{  58,  61,  7 },	// PAIN_B
	{  62,  65,  7 },	// PAIN_C
	{  66,  71,  7 },	// JUMP
	{  72,  83,  7 },	// FLIP
	{  84,  94,  7 },	// SALUTE
	{  95, 111, 10 },	// FALLBACK
	{ 112, 122,  7 },	// WAVE
	{ 123, 134,  6 },	// POINT
	{ 135, 153, 10 },	// CROUCH_STAND
	{ 154, 159,  7 },	// CROUCH_WALK
	{ 160, 168, 10 },	// CROUCH_ATTACK
	{ 196, 172,  7 },	// CROUCH_PAIN
	{ 173, 177,  5 },	// CROUCH_DEATH
	{ 178, 183,  7 },	// DEATH_FALLBACK
	{ 184, 189,  7 },	// DEATH_FALLFORWARD
	{ 190, 197,  7 },	// DEATH_FALLBACKSLOW
	{ 198, 198,  5 },	// BOOM
};

/*!***************************************************************************
 @Function			SetAnim
 @Input				Required Aniation (Enum or number directly)
 @Description		Initialize m_anim from the specified animation.
*****************************************************************************/
void CMD2Model::SetAnim( int type )
{
	if( (type < 0) || (type > MAX_ANIMATIONS) )
		type = 0;

	m_anim.startframe	= animlist[ type ].first_frame;
	m_anim.endframe		= animlist[ type ].last_frame;
	m_anim.next_frame	= animlist[ type ].first_frame + 1;
	m_anim.fps			= animlist[ type ].fps;
	m_anim.type			= type;
}

/****************************************************************************
 @Function			InitMyArraysFloat
 @Output			Boolean all OK or Failure
 @Description		Init the Arrays for usage - Memory Allocation
*****************************************************************************/
bool CMD2Model::InitMyArraysFloat( void )		
{
	int vertexfancounter = 0;
	int vertexstripcounter = 0;
	int fancounter = 0;
	int stripcounter = 0;

	int	*ptricmds = m_glcmds;		// pointer on gl commands

	// Step through the object to find number of strips and fans and total number of vertices in each case

	while( int i = *(ptricmds++) )
	{
		if( i < 0 )
		{
			i = -i;
			vertexfancounter+=i;	// Add number of Vertices in this fan
			fancounter++;			// Add one to fan counter
		}
		else
		{
			vertexstripcounter+=i;	// Add number of Vertices in this strip
			stripcounter++;			// Add one to the strip counter
		}

		for( /* nothing */; i > 0; i--, ptricmds += 3 )
		{
			// Do nothing for now - this routing is just counting to determine array sizes;
		}
	}

	// Now that we know the size : allocate memory

	// Allocate Memory for Fans And Strips Buffers

	MyFloatFansAndStrips.pColorArray = new unsigned char[4*(vertexfancounter+vertexstripcounter)];
	MyFloatFansAndStrips.pTexCoordArray = new float[2*(vertexfancounter+vertexstripcounter)];
	MyFloatFansAndStrips.pNormalArray = new float[3*(vertexfancounter+vertexstripcounter)];
	MyFloatFansAndStrips.pPositionArray = new float[3*(vertexfancounter+vertexstripcounter)];
	MyFloatFansAndStrips.NumberOfFans = fancounter;
	MyFloatFansAndStrips.NumberOfStrips = stripcounter;
	MyFloatFansAndStrips.Offset2Strips = vertexfancounter;
	MyFloatFansAndStrips.pFanLengths = new int[fancounter];
	MyFloatFansAndStrips.pStripLengths = new int[stripcounter];

	if(MyFloatFansAndStrips.pColorArray == NULL || MyFloatFansAndStrips.pTexCoordArray == NULL ||
		MyFloatFansAndStrips.pNormalArray == NULL || MyFloatFansAndStrips.pPositionArray == NULL ||
		MyFloatFansAndStrips.pFanLengths == NULL || MyFloatFansAndStrips.pStripLengths == NULL)
	{
		/* Memory Allocation failed */
		return false;
	}

	MyVertexCount=vertexfancounter+vertexstripcounter;

	// Fill in the strip and fan texcoords since these are static

	ptricmds = m_glcmds;		// Reset pointer to gl commands

	fancounter=0;
	stripcounter=0;

	vertexfancounter=0;
	vertexstripcounter=0;

	while( int i = *(ptricmds++) )
	{
		if( i < 0 )
		{
			i = -i;
			MyFloatFansAndStrips.pFanLengths[fancounter]=i;

			fancounter++;			// Add one to fan counter

			for( /* nothing */; i > 0; i--, ptricmds += 3 )
			{

				MyFloatFansAndStrips.pTexCoordArray[vertexfancounter*2+0] = ((float *)ptricmds)[0];
				MyFloatFansAndStrips.pTexCoordArray[vertexfancounter*2+1] = -1.0f*((float *)ptricmds)[1];

				vertexfancounter++;
			}


		}
		else
		{
			MyFloatFansAndStrips.pStripLengths[stripcounter]=i;
			stripcounter++;			// Add one to the strip counter

			for( /* nothing */; i > 0; i--, ptricmds += 3 )
			{
				MyFloatFansAndStrips.pTexCoordArray[(MyFloatFansAndStrips.Offset2Strips*2)+(vertexstripcounter*2)+0] = ((float *)ptricmds)[0];
				MyFloatFansAndStrips.pTexCoordArray[(MyFloatFansAndStrips.Offset2Strips*2)+(vertexstripcounter*2)+1] = -1.0f*((float *)ptricmds)[1];

				vertexstripcounter++;
			}
		}
	}

	// Other data is dynamic due to animation so only fill in when drawing a model

	return true;
}

/*!***************************************************************************
 @Function			InitMyArraysFixed
 @Output			Boolean all OK or Failure
 @Description		Init the Arrays for usage - Memory Allocation
*****************************************************************************/
bool CMD2Model::InitMyArraysFixed( void )
{
	int vertexfancounter = 0;
	int vertexstripcounter = 0;
	int fancounter = 0;
	int stripcounter = 0;

	int	*ptricmds = m_glcmds;		// pointer on gl commands

	// Step through the object to find number of strips and fans and total number of vertices in each case

	while( int i = *(ptricmds++) )
	{
		if( i < 0 )
		{
			i = -i;
			vertexfancounter+=i;	// Add number of Vertices in this fan
			fancounter++;			// Add one to fan counter
		}
		else
		{
			vertexstripcounter+=i;	// Add number of Vertices in this strip
			stripcounter++;			// Add one to the strip counter
		}

		for( /* nothing */; i > 0; i--, ptricmds += 3 )
		{
			// Do nothing for now - just counting to determine array sizes;
		}
	}

	// Now that we know the size : allocate memory

	// Allocate Memory for Fans And Strips Buffers

	MyFixedFansAndStrips.pColorArray = new unsigned char[4*(vertexfancounter+vertexstripcounter)];
	MyFixedFansAndStrips.pTexCoordArray = new int[2*(vertexfancounter+vertexstripcounter)];
	MyFixedFansAndStrips.pNormalArray = new int[3*(vertexfancounter+vertexstripcounter)];
	MyFixedFansAndStrips.pPositionArray = new int[3*(vertexfancounter+vertexstripcounter)];
	MyFixedFansAndStrips.NumberOfFans = fancounter;
	MyFixedFansAndStrips.NumberOfStrips = stripcounter;
	MyFixedFansAndStrips.Offset2Strips = vertexfancounter;
	MyFixedFansAndStrips.pFanLengths = new int[fancounter];
	MyFixedFansAndStrips.pStripLengths = new int[stripcounter];

	if(MyFixedFansAndStrips.pColorArray == NULL || MyFixedFansAndStrips.pTexCoordArray == NULL ||
		MyFixedFansAndStrips.pNormalArray == NULL || MyFixedFansAndStrips.pPositionArray == NULL ||
		MyFixedFansAndStrips.pFanLengths == NULL || MyFixedFansAndStrips.pStripLengths == NULL)
	{
		/* Memory Allocation failed */
		return false;
	}

	MyVertexCount=vertexfancounter+vertexstripcounter;

	// Fill in the strip and fan texture coords since these are static

	ptricmds = m_glcmds;		// Reset pointer to gl commands

	fancounter=0;
	stripcounter=0;

	vertexfancounter=0;
	vertexstripcounter=0;

	while( int i = *(ptricmds++) )
	{
		if( i < 0 )
		{
			i = -i;
			MyFixedFansAndStrips.pFanLengths[fancounter]=i;

			fancounter++;			// Add one to fan counter

			for( /* nothing */; i > 0; i--, ptricmds += 3 )
			{

				MyFixedFansAndStrips.pTexCoordArray[vertexfancounter*2+0] = float2fix( ((float *)ptricmds)[0] );
				MyFixedFansAndStrips.pTexCoordArray[vertexfancounter*2+1] = float2fix( -1.0f*((float *)ptricmds)[1] );

				vertexfancounter++;
			}


		}
		else
		{
			MyFixedFansAndStrips.pStripLengths[stripcounter]=i;
			stripcounter++;			// Add one to the strip counter

			for( /* nothing */; i > 0; i--, ptricmds += 3 )
			{
				MyFixedFansAndStrips.pTexCoordArray[(MyFixedFansAndStrips.Offset2Strips*2)+(vertexstripcounter*2)+0] = float2fix( ((float *)ptricmds)[0] );
				MyFixedFansAndStrips.pTexCoordArray[(MyFixedFansAndStrips.Offset2Strips*2)+(vertexstripcounter*2)+1] = float2fix( -1.0f*((float *)ptricmds)[1] );

				vertexstripcounter++;
			}
		}
	}

	// Other data is dynamic due to animation so only fill in when drawing a model

	return true;
}

/*!***************************************************************************
 @Function			DestroyMyArrays
 @Description		Free the memory allocated to store data
*****************************************************************************/
void CMD2Model::DestroyMyArrays( void )	
{
	if (ObjectIsFloat)
	{
		delete [] MyFloatFansAndStrips.pColorArray;
		delete [] MyFloatFansAndStrips.pTexCoordArray;
		delete [] MyFloatFansAndStrips.pNormalArray;
		delete [] MyFloatFansAndStrips.pPositionArray;
		delete [] MyFloatFansAndStrips.pFanLengths;
		delete [] MyFloatFansAndStrips.pStripLengths;
	}
	else
	{
		delete [] MyFixedFansAndStrips.pColorArray;
		delete [] MyFixedFansAndStrips.pTexCoordArray;
		delete [] MyFixedFansAndStrips.pNormalArray;
		delete [] MyFixedFansAndStrips.pPositionArray;
		delete [] MyFixedFansAndStrips.pFanLengths;
		delete [] MyFixedFansAndStrips.pStripLengths;
	}
	if(Indices!=NULL)
	{
		delete [] Indices;
	}
}

/*!***************************************************************************
 @Function			GetModelDataPntrsFloat
 @Output			NumberOfFans
 @Output			NumberOfStrips
 @Output			Offset2Strips
 @Output			pFanLengths
 @Output			pStripLengths
 @Modified			pPosition
 @Modified			pTexCoord
 @Modified			pNormal
 @Modified			pColor
 @Input				time
 @Description		Returns the data required for rendering
*****************************************************************************/
void CMD2Model::GetModelDataPntrsFloat(float time, float **pPosition , float **pTexCoord, float **pNormal, unsigned char **pColor, int *NumberOfFans, int *NumberOfStrips, int *Offset2Strips, int **pFanLengths, int **pStripLengths)
{
	static vec3_t	vertlist[ MAX_MD2_VERTS ];	// interpolated vertices
	static vec3_t	normlist[ MAX_MD2_VERTS ];	// interpolated normals
	int				*ptricmds = m_glcmds;		// pointer on gl commands
	int				*old_ptricmds;

	int vertexfancounter = 0;
	int vertexstripcounter = 0;

	int oldvertexfancounter = 0;
	int oldvertexstripcounter = 0;

	int j;

	bool bColorEnable		= pColor == NULL ? false:true;
	bool bNormalEnable		= pNormal == NULL ? false:true;
	bool bTexCoordEnable	= pTexCoord == NULL ? false:true;

	// Update Animation based on Time if required
	// Animation = calculate current frame and next frame

	if(( time > 0.0 ) && (time!=oldtime))
	{
		Animate( time );
	}

	if (1)  // Placeholder for possible future performance update - e.g. time and options same as previously requested
	{

		// process lighting only when color is enable
		if ((bColorEnable))
		{
			ProcessLighting();
		}

		// interpolate vertex always required since we always return the vertex data

		InterpolatePosition( vertlist);

		// interpolate normal data not always required

		if ((bNormalEnable))
		{
			InterpolatePosition( normlist );
		}

		// Store all Vertices in our Arrays
		while( int i = *(ptricmds++) )
		{
			if( i < 0 )
			{
				// FANS

				i = -i;

				j=i;

				oldvertexfancounter=vertexfancounter;
				old_ptricmds=ptricmds;

				if (bColorEnable)
				{
					for( /* nothing */; i > 0; i--, ptricmds += 3 )
					{
						float l = shadedots[ m_lightnormals[ ptricmds[2] ] ];

						// set the lighting color

						MyFloatFansAndStrips.pColorArray[vertexfancounter*4+0]=(unsigned char)(l * lcolor[0] * 255.0f);
						MyFloatFansAndStrips.pColorArray[vertexfancounter*4+1]=(unsigned char)(l * lcolor[1] * 255.0f);
						MyFloatFansAndStrips.pColorArray[vertexfancounter*4+2]=(unsigned char)(l * lcolor[2] * 255.0f);
						MyFloatFansAndStrips.pColorArray[vertexfancounter*4+3]=255;

						vertexfancounter++;
					}
				}

				i=j;
				vertexfancounter=oldvertexfancounter;
				ptricmds=old_ptricmds;

				if (bNormalEnable)
				{
					for( /* nothing */; i > 0; i--, ptricmds += 3 )
					{
						MyFloatFansAndStrips.pNormalArray[vertexfancounter*3+0]=normlist[ ptricmds[2] ] [0];
						MyFloatFansAndStrips.pNormalArray[vertexfancounter*3+1]=normlist[ ptricmds[2] ] [1];
						MyFloatFansAndStrips.pNormalArray[vertexfancounter*3+2]=normlist[ ptricmds[2] ] [2];

						vertexfancounter++;
					}
				}

				i=j;
				vertexfancounter=oldvertexfancounter;
				ptricmds=old_ptricmds;

				// ALWAYS do the position

				for( /* nothing */; i > 0; i--, ptricmds += 3 )
				{
					MyFloatFansAndStrips.pPositionArray[vertexfancounter*3+0]=vertlist[ ptricmds[2] ] [0];
					MyFloatFansAndStrips.pPositionArray[vertexfancounter*3+1]=vertlist[ ptricmds[2] ] [1];
					MyFloatFansAndStrips.pPositionArray[vertexfancounter*3+2]=vertlist[ ptricmds[2] ] [2];

					vertexfancounter++;
				}
			}
			else
			{
				// Strip

				j=i;
				oldvertexstripcounter=vertexstripcounter;
				old_ptricmds=ptricmds;

				if (bColorEnable)
				{
					for( /* nothing */; i > 0; i--, ptricmds += 3 )
					{
						float l = shadedots[ m_lightnormals[ ptricmds[2] ] ];

						// set the lighting color

						MyFloatFansAndStrips.pColorArray[(MyFloatFansAndStrips.Offset2Strips*4)+(vertexstripcounter*4)+0]=(unsigned char)(l * lcolor[0] * 255.0f);
						MyFloatFansAndStrips.pColorArray[(MyFloatFansAndStrips.Offset2Strips*4)+(vertexstripcounter*4)+1]=(unsigned char)(l * lcolor[1] * 255.0f);
						MyFloatFansAndStrips.pColorArray[(MyFloatFansAndStrips.Offset2Strips*4)+(vertexstripcounter*4)+2]=(unsigned char)(l * lcolor[2] * 255.0f);
						MyFloatFansAndStrips.pColorArray[(MyFloatFansAndStrips.Offset2Strips*4)+(vertexstripcounter*4)+3]=255;

						vertexstripcounter++;
					}
				}

				i=j;
				vertexstripcounter=oldvertexstripcounter;
				ptricmds=old_ptricmds;

				if (bNormalEnable)
				{
					for( /* nothing */; i > 0; i--, ptricmds += 3 )
					{
						MyFloatFansAndStrips.pNormalArray[(MyFloatFansAndStrips.Offset2Strips*3)+(vertexstripcounter*3)+0]=normlist[ ptricmds[2] ] [0];
						MyFloatFansAndStrips.pNormalArray[(MyFloatFansAndStrips.Offset2Strips*3)+(vertexstripcounter*3)+1]=normlist[ ptricmds[2] ] [1];
						MyFloatFansAndStrips.pNormalArray[(MyFloatFansAndStrips.Offset2Strips*3)+(vertexstripcounter*3)+2]=normlist[ ptricmds[2] ] [2];

						vertexstripcounter++;
					}
				}

				i=j;
				vertexstripcounter=oldvertexstripcounter;
				ptricmds=old_ptricmds;

				for( /* nothing */; i > 0; i--, ptricmds += 3 )
				{
					MyFloatFansAndStrips.pPositionArray[(MyFloatFansAndStrips.Offset2Strips*3)+(vertexstripcounter*3)+0]=vertlist[ ptricmds[2] ] [0];
					MyFloatFansAndStrips.pPositionArray[(MyFloatFansAndStrips.Offset2Strips*3)+(vertexstripcounter*3)+1]=vertlist[ ptricmds[2] ] [1];
					MyFloatFansAndStrips.pPositionArray[(MyFloatFansAndStrips.Offset2Strips*3)+(vertexstripcounter*3)+2]=vertlist[ ptricmds[2] ] [2];

					vertexstripcounter++;
				}
			}
		}
	}

	oldtime=time; // keep track of the currently created time to avoid regenerating the same data


	// Assign Output Values

	*pPosition		= MyFloatFansAndStrips.pPositionArray;

	if (bTexCoordEnable)
		*pTexCoord		= MyFloatFansAndStrips.pTexCoordArray;

	if (bNormalEnable)
		*pNormal		= MyFloatFansAndStrips.pNormalArray;

	if (bColorEnable)
		*pColor			= MyFloatFansAndStrips.pColorArray;

	*NumberOfFans	= MyFloatFansAndStrips.NumberOfFans;
	*NumberOfStrips	= MyFloatFansAndStrips.NumberOfStrips;
	*Offset2Strips	= MyFloatFansAndStrips.Offset2Strips;
	*pFanLengths	= MyFloatFansAndStrips.pFanLengths;
	*pStripLengths	= MyFloatFansAndStrips.pStripLengths;
}

/*!***************************************************************************
 @Function			GetModelDataPntrsFixed
 @Output			NumberOfFans
 @Output			NumberOfStrips
 @Output			Offset2Strips
 @Output			pFanLengths
 @Output			pStripLengths
 @Modified			pPosition
 @Modified			pTexCoord
 @Modified			pNormal
 @Modified			pColor
 @Input				time
 @Description		Returns the data required for rendering
*****************************************************************************/
void CMD2Model::GetModelDataPntrsFixed(float time, int **pPosition , int **pTexCoord, int **pNormal, unsigned char **pColor, int *NumberOfFans, int *NumberOfStrips, int *Offset2Strips, int **pFanLengths, int **pStripLengths)
{
	static vec3_t	vertlist[ MAX_MD2_VERTS ];	// interpolated vertices
	static vec3_t	normlist[ MAX_MD2_VERTS ];	// interpolated normals
	int				*ptricmds = m_glcmds;		// pointer on gl commands
	int				*old_ptricmds;

	int vertexfancounter = 0;
	int vertexstripcounter = 0;

	int oldvertexfancounter = 0;
	int oldvertexstripcounter = 0;

	int j;
//	int currentstripindex = 0;
//	int currentfanindex = 0;

	bool bColorEnable		= pColor == NULL ? false:true;
	bool bNormalEnable		= pNormal == NULL ? false:true;
	bool bTexCoordEnable	= pTexCoord == NULL ? false:true;

	// Update Animation based on Time if required
	// Animation = calculate current frame and next frame

	if(( time > 0.0 ) && (time!=oldtime))
	{
		Animate( time );
	}

	if (1) // Placeholder for possible future performance update - e.g. time and options same as previously requested
	{

		// process lighting only when color is enable
		if ((bColorEnable))
		{
			ProcessLighting();
		}

		// interpolate vertex always required since we always return the vertex data

		InterpolatePosition( vertlist);
		
		// interpolate normal data not always required

		if ((bNormalEnable))
		{
			InterpolatePosition( normlist );
		}

		// Store all Vertices in our Arrays
		while( int i = *(ptricmds++) )
		{
			if( i < 0 )
			{
				// FANS

				i = -i;

				j=i;

				oldvertexfancounter=vertexfancounter;
				old_ptricmds=ptricmds;

				if (bColorEnable)
				{
					for( /* nothing */; i > 0; i--, ptricmds += 3 )
					{
						float l = shadedots[ m_lightnormals[ ptricmds[2] ] ];

						// set the lighting color

						MyFixedFansAndStrips.pColorArray[vertexfancounter*4+0]=(unsigned char)(l * lcolor[0] * 255.0f);
						MyFixedFansAndStrips.pColorArray[vertexfancounter*4+1]=(unsigned char)(l * lcolor[1] * 255.0f);
						MyFixedFansAndStrips.pColorArray[vertexfancounter*4+2]=(unsigned char)(l * lcolor[2] * 255.0f);
						MyFixedFansAndStrips.pColorArray[vertexfancounter*4+3]=255;

						vertexfancounter++;
					}
				}

				i=j;
				vertexfancounter=oldvertexfancounter;
				ptricmds=old_ptricmds;

				if (bNormalEnable)
				{
					for( /* nothing */; i > 0; i--, ptricmds += 3 )
					{
						MyFixedFansAndStrips.pNormalArray[vertexfancounter*3+0]=float2fix( normlist[ ptricmds[2] ] [0] );
						MyFixedFansAndStrips.pNormalArray[vertexfancounter*3+1]=float2fix( normlist[ ptricmds[2] ] [1] );
						MyFixedFansAndStrips.pNormalArray[vertexfancounter*3+2]=float2fix( normlist[ ptricmds[2] ] [2] );

						vertexfancounter++;
					}
				}

				i=j;
				vertexfancounter=oldvertexfancounter;
				ptricmds=old_ptricmds;

				// ALWAYS do the position

				for( /* nothing */; i > 0; i--, ptricmds += 3 )
				{
					MyFixedFansAndStrips.pPositionArray[vertexfancounter*3+0]=float2fix( vertlist[ ptricmds[2] ] [0] );
					MyFixedFansAndStrips.pPositionArray[vertexfancounter*3+1]=float2fix( vertlist[ ptricmds[2] ] [1] );
					MyFixedFansAndStrips.pPositionArray[vertexfancounter*3+2]=float2fix( vertlist[ ptricmds[2] ] [2] );

					vertexfancounter++;
				}
			}
			else
			{
				// Strip

				j=i;
				oldvertexstripcounter=vertexstripcounter;
				old_ptricmds=ptricmds;

				if (bColorEnable)
				{
					for( /* nothing */; i > 0; i--, ptricmds += 3 )
					{
						float l = shadedots[ m_lightnormals[ ptricmds[2] ] ];

						// set the lighting color

						MyFixedFansAndStrips.pColorArray[(MyFixedFansAndStrips.Offset2Strips*4)+(vertexstripcounter*4)+0]=(unsigned char)(l * lcolor[0] * 255.0f);
						MyFixedFansAndStrips.pColorArray[(MyFixedFansAndStrips.Offset2Strips*4)+(vertexstripcounter*4)+1]=(unsigned char)(l * lcolor[1] * 255.0f);
						MyFixedFansAndStrips.pColorArray[(MyFixedFansAndStrips.Offset2Strips*4)+(vertexstripcounter*4)+2]=(unsigned char)(l * lcolor[2] * 255.0f);
						MyFixedFansAndStrips.pColorArray[(MyFixedFansAndStrips.Offset2Strips*4)+(vertexstripcounter*4)+3]=255;

						vertexstripcounter++;
					}
				}

				i=j;
				vertexstripcounter=oldvertexstripcounter;
				ptricmds=old_ptricmds;

				if (bNormalEnable)
				{
					for( /* nothing */; i > 0; i--, ptricmds += 3 )
					{
						MyFixedFansAndStrips.pNormalArray[(MyFixedFansAndStrips.Offset2Strips*3)+(vertexstripcounter*3)+0]=float2fix( normlist[ ptricmds[2] ] [0] );
						MyFixedFansAndStrips.pNormalArray[(MyFixedFansAndStrips.Offset2Strips*3)+(vertexstripcounter*3)+1]=float2fix( normlist[ ptricmds[2] ] [1] );
						MyFixedFansAndStrips.pNormalArray[(MyFixedFansAndStrips.Offset2Strips*3)+(vertexstripcounter*3)+2]=float2fix( normlist[ ptricmds[2] ] [2] );

						vertexstripcounter++;
					}
				}

				i=j;
				vertexstripcounter=oldvertexstripcounter;
				ptricmds=old_ptricmds;

				for( /* nothing */; i > 0; i--, ptricmds += 3 )
				{
					MyFixedFansAndStrips.pPositionArray[(MyFixedFansAndStrips.Offset2Strips*3)+(vertexstripcounter*3)+0]=float2fix( vertlist[ ptricmds[2] ] [0] );
					MyFixedFansAndStrips.pPositionArray[(MyFixedFansAndStrips.Offset2Strips*3)+(vertexstripcounter*3)+1]=float2fix( vertlist[ ptricmds[2] ] [1] );
					MyFixedFansAndStrips.pPositionArray[(MyFixedFansAndStrips.Offset2Strips*3)+(vertexstripcounter*3)+2]=float2fix( vertlist[ ptricmds[2] ] [2] );

					vertexstripcounter++;
				}
			}
		}
	}

	oldtime=time; // keep track of the currently created time to avoid regenerating the same data


	// Assign Output Values

	*pPosition		= MyFixedFansAndStrips.pPositionArray;

	if (bTexCoordEnable)
		*pTexCoord		= MyFixedFansAndStrips.pTexCoordArray;

	if (bNormalEnable)
		*pNormal		= MyFixedFansAndStrips.pNormalArray;

	if (bColorEnable)
		*pColor			= MyFixedFansAndStrips.pColorArray;

	*NumberOfFans	= MyFixedFansAndStrips.NumberOfFans;
	*NumberOfStrips	= MyFixedFansAndStrips.NumberOfStrips;
	*Offset2Strips	= MyFixedFansAndStrips.Offset2Strips;
	*pFanLengths	= MyFixedFansAndStrips.pFanLengths;
	*pStripLengths	= MyFixedFansAndStrips.pStripLengths;
}

/*!***************************************************************************
 @Function			GenerateTriangleList
 @Output			Triangle List
 @Description		Generate Indexed Triangle List Indices
*****************************************************************************/
unsigned short* CMD2Model::GenerateTriangleList( void )		
{
	int vertexfancounter = 0;
	int vertexfancounter2 = 0;
	int vertexstripcounter = 0;
	int fancounter = 0;
	int stripcounter = 0;
	int indexposition = 0;
	int sourceindexposition = 0;
	int stripsourceindexposition = 0;
	int CurrentFanMasterVertex;
	int OneButLastVertex;
	int LastVertex;
	int Tmp;
	int flip;

	int	*ptricmds = m_glcmds;		// pointer on gl commands

	// Step through the object to find number of strips and fans and total number of vertices in each case

	while( int i = *(ptricmds++) )
	{
		if( i < 0 )
		{
			i = -i;
			vertexfancounter+=(i-2);	// Number of Triangles is Number of Vertices - 2
			vertexfancounter2+=i;
			fancounter++;				// Add one to fan counter
		}
		else
		{
			vertexstripcounter+=(i-2);	// Number of Triangles is Number of Vertices - 2
			stripcounter++;				// Add one to the strip counter
		}

		for( /* nothing */; i > 0; i--, ptricmds += 3 )
		{
			// Do nothing for now - just counting how many indices there will be;
		}
	}

	int MyOffset2Strips = vertexfancounter2;

	// Now that we know the size : allocate memory
	// Allocate Memory for Index Buffer = 3 indices per triangle

	Indices = new unsigned short[3*(vertexfancounter+vertexstripcounter)];

	TriangleCount=vertexfancounter+vertexstripcounter;


	if(Indices == NULL)
	{
		/* Memory Allocation failed */
		return NULL;
	}

	// Fill in the strip and fan index data

	ptricmds = m_glcmds;		// Reset pointer to gl commands

	fancounter=0;
	stripcounter=0;

	vertexfancounter=0;
	vertexstripcounter=0;

	while( int i = *(ptricmds++) )
	{
		if( i < 0 )
		{
			i = -i;

			Indices[indexposition]=sourceindexposition;
			CurrentFanMasterVertex=sourceindexposition;
			indexposition++;
			sourceindexposition++;

			Indices[indexposition]=sourceindexposition;
			indexposition++;
			sourceindexposition++;

			LastVertex=sourceindexposition;			
			Indices[indexposition]=sourceindexposition;
			indexposition++;
			sourceindexposition++;

			for( /* nothing */; (i-3) > 0; i--, ptricmds += 3 )
			{
				Indices[indexposition]=CurrentFanMasterVertex;
				indexposition++;

				Indices[indexposition]=LastVertex;
				indexposition++;

				Indices[indexposition]=sourceindexposition;
				LastVertex=sourceindexposition;	
				indexposition++;

				sourceindexposition++;

				vertexfancounter++;
			}

			ptricmds+=9;
		}
		else
		{
			Indices[indexposition]=stripsourceindexposition+MyOffset2Strips;
			CurrentFanMasterVertex=stripsourceindexposition+MyOffset2Strips;
			indexposition++;
			stripsourceindexposition++;

			OneButLastVertex=stripsourceindexposition+MyOffset2Strips;	
			Indices[indexposition]=stripsourceindexposition+MyOffset2Strips;
			indexposition++;
			stripsourceindexposition++;

			LastVertex=stripsourceindexposition+MyOffset2Strips;			
			Indices[indexposition]=stripsourceindexposition+MyOffset2Strips;
			indexposition++;
			stripsourceindexposition++;

			flip=1;

			for( /* nothing */; (i-3) > 0; i--, ptricmds += 3 )
			{
				Indices[indexposition]=OneButLastVertex;
				indexposition++;

				Indices[indexposition]=LastVertex;
				indexposition++;

				Indices[indexposition]=stripsourceindexposition+MyOffset2Strips;
				indexposition++;

				OneButLastVertex=LastVertex;	
				LastVertex=stripsourceindexposition+MyOffset2Strips;				
				
				stripsourceindexposition++;

				vertexfancounter++;

				if (flip)
				{
					flip=0;
					Tmp=Indices[indexposition-1];
					Indices[indexposition-1]=Indices[indexposition-3];
					Indices[indexposition-3]=Tmp;
				}
				else
				{
					flip=1;
				}
			}

			ptricmds+=9;
		}
	}

	return Indices;
}
