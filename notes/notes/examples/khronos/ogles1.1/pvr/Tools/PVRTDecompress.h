/******************************************************************************

 @File         PVRTDecompress.h

 @Title        

 @Copyright    Copyright (C) 2000 - 2006 by Imagination Technologies Limited.

 @Platform     ANSI compatible

 @Description  PVRTC Texture Decompression.

******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif

void PVRTCDecompress(const void *pCompressedData,
				const int Do2bitMode,
				const int XDim,
				const int YDim,
				unsigned char* pResultImage);
				
#ifdef __cplusplus
}
#endif

/*
// END OF FILE
*/

