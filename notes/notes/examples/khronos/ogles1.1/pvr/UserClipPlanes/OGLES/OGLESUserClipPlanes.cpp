/******************************************************************************

 @File         OGLESUserClipPlanes.cpp

 @Title        User Defined Clip Planes

 @Copyright    Copyright (C) 2004 - 2006 by Imagination Technologies Limited.

 @Platform     Independant

 @Description  Shows how to use multiple user defined clip planes using a PowerVR
               OGLES extension.

******************************************************************************/
#define _USE_MATH_DEFINES
#include <math.h>

#include <stdlib.h>
#include "PVRShell.h"
#include "OGLESTools.h"

/* 3D data */
#ifdef OGLESLITE
#include "sphere_fixed.h"
#else
#include "sphere_float.h"
#endif

/* Textures */
#include "Granite.h"

/*************************
		Defines
*************************/
#ifndef PI
#define PI 3.14159f
#endif

/*************************
	   Demo Class
*************************/
class OGLESUserClipPlanes : public PVRShell{

	CPVRTPrint3D 	AppPrint3D;
	CPVRTglesExt	glExtensions;
	CPVRTPODScene	g_sScene;

	VERTTYPE		LightPosition[4];
	GLuint			texName;
	long			nFrame;
	int				nClipPlanes;
	bool			bClipPlaneSupported;

#ifdef GL_OES_VERSION_1_1
#ifdef OGLESLITE
#define ClipPlane glClipPlanex
#else
#define ClipPlane glClipPlanef
#endif
#else
#ifdef OGLESLITE
	CPVRTglesExt::PFNGLCLIPPLANEXIMG ClipPlane;
#else
	CPVRTglesExt::PFNGLCLIPPLANEFIMG ClipPlane;
#endif
#endif

public:
	OGLESUserClipPlanes(){
		nFrame = 0L;

		LightPosition[0]= f2vt(-1.0f);
		LightPosition[1]= f2vt(1.0f);
		LightPosition[2]= f2vt(1.0f);
		LightPosition[3]= f2vt(0.0f);
	}

	virtual bool InitApplication();
	virtual bool InitView();
	virtual bool ReleaseView();
	virtual bool QuitApplication();
	virtual bool RenderScene();

	void DrawSphere();
	void RenderPrimitive(VERTTYPE *pVertex, VERTTYPE *pNormals, VERTTYPE *pUVs, int nNumIndex, unsigned short *pIDX, GLenum mode);
	void SetupUserClipPlanes();
	void DisableClipPlanes();
};
/*******************************************************************************
 * Function Name  : InitApplication
 * Returns        : TRUE if no error occured
 * Description    : App
 *
 *******************************************************************************/

bool OGLESUserClipPlanes::InitApplication()
{
#ifdef OGLESLITE
	g_sScene.ReadFromMemory(c_SPHERE_FIXED_H);
#else
	g_sScene.ReadFromMemory(c_SPHERE_FLOAT_H);
#endif
	return true;
}

/*******************************************************************************
 * Function Name  : QuitApplication
 * Returns        : TRUE if no error occured
 * Description    : App
 *
 *******************************************************************************/
bool OGLESUserClipPlanes::QuitApplication()
{
	g_sScene.Destroy();
	return true;
}

/*******************************************************************************
 * Function Name  : InitView
 * Returns        : TRUE if no error occured
 * Description    : App
 *
 *******************************************************************************/
bool OGLESUserClipPlanes::InitView()
{
	PVRTMATRIX	MyPerspMatrix;
	int			err;
	float		width  = (float)PVRShellGet(prefWidth);
	float		height = (float)PVRShellGet(prefHeight);
	VERTTYPE	fValue[4];

	/* Initialize Extensions */
	glExtensions.Init();

	/* Detects if we are using OpenGL ES 1.1 or if the extension exists */
	bClipPlaneSupported = false;
#ifdef GL_OES_VERSION_1_1
	bClipPlaneSupported = true;
#endif
	if (!bClipPlaneSupported)
	{
		/* Look for the IMG clip plane extension */
		bool bExtensionExist = glExtensions.IsGLExtensionSupported("GL_IMG_user_clip_planes");
		bClipPlaneSupported = bClipPlaneSupported || bExtensionExist;
		if (bExtensionExist)
		{
			/* Extension exists, retrieve entry points */
#ifndef GL_OES_VERSION_1_1
#ifdef OGLESLITE
			ClipPlane = glExtensions.glClipPlanexIMG;
#else
			ClipPlane = glExtensions.glClipPlanefIMG;
#endif
#endif
		}
	}

	/* Retrieve max number of clip planes */
	if (bClipPlaneSupported)
	{
		glGetIntegerv(GL_MAX_CLIP_PLANES, &nClipPlanes);
	}
	if (nClipPlanes==0) bClipPlaneSupported = false;

	SPVRTContext Context;

	/* Setup textures */
	err = AppPrint3D.SetTextures(&Context,PVRShellGet(prefWidth),PVRShellGet(prefHeight));
	if(err == false)
	{
		PVRShellOutputDebug ("ERROR: Cannot initialise Print3D\n");
		return false;
	}

	/* Load textures */
	if (!PVRTLoadTextureFromPointer((void*)Granite, &texName)) return false;
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	/* Perspective matrix */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if(PVRShellGet(prefIsRotated) && PVRShellGet(prefFullScreen))
	{
		myglRotate(f2vt(90), f2vt(0), f2vt(0), f2vt(1));
		width =  (float)PVRShellGet(prefHeight);
		height = (float)PVRShellGet(prefWidth);
	}

	PVRTMatrixPerspectiveFovRH(MyPerspMatrix, f2vt(20.0f*(PI/180.0f)), f2vt((float)width/(float)height), f2vt(10.0f), f2vt(1200.0f));
	myglMultMatrix(MyPerspMatrix.f);

	/* Modelview matrix */
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	/* Setup culling */
	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);

	// Setup single light
	glEnable(GL_LIGHTING);

	/* Light 0 (White directional light) */
	fValue[0]=f2vt(0.4f); fValue[1]=f2vt(0.4f); fValue[2]=f2vt(0.4f); fValue[3]=f2vt(1.0f);
	myglLightv(GL_LIGHT0, GL_AMBIENT, fValue);
	fValue[0]=f2vt(1.0f); fValue[1]=f2vt(1.0f); fValue[2]=f2vt(1.0f); fValue[3]=f2vt(1.0f);
	myglLightv(GL_LIGHT0, GL_DIFFUSE, fValue);
	fValue[0]=f2vt(1.0f); fValue[1]=f2vt(1.0f); fValue[2]=f2vt(1.0f); fValue[3]=f2vt(1.0f);
	myglLightv(GL_LIGHT0, GL_SPECULAR, fValue);
	myglLightv(GL_LIGHT0, GL_POSITION, LightPosition);
	glEnable(GL_LIGHT0);
	VERTTYPE ambient_light[4] = {f2vt(0.8f), f2vt(0.8f), f2vt(0.8f), f2vt(1.0f)};
	myglLightModelv(GL_LIGHT_MODEL_AMBIENT, ambient_light);

	/* Setup all materials */
	VERTTYPE objectMatAmb[] = {f2vt(0.1f), f2vt(0.1f), f2vt(0.1f), f2vt(0.3f)};
	VERTTYPE objectMatDiff[] = {f2vt(0.5f), f2vt(0.5f), f2vt(0.5f), f2vt(0.3f)};
	VERTTYPE objectMatSpec[] = {f2vt(1.0f), f2vt(1.0f), f2vt(1.0f), f2vt(0.3f)};
	myglMaterialv(GL_FRONT_AND_BACK, GL_AMBIENT, objectMatAmb);
	myglMaterialv(GL_FRONT_AND_BACK, GL_DIFFUSE, objectMatDiff);
	myglMaterialv(GL_FRONT_AND_BACK, GL_SPECULAR, objectMatSpec);
	myglMaterial(GL_FRONT_AND_BACK, GL_SHININESS, f2vt(10));	// Nice and shiny so we don't get aliasing from the 1/2 angle

	return true;
}

/*******************************************************************************
 * Function Name  : ReleaseView
 * Returns        :
 * Description    : App
 *
 *******************************************************************************/
bool OGLESUserClipPlanes::ReleaseView()
{
	// release textures
	PVRTReleaseTexture(texName);
	AppPrint3D.ReleaseTextures();
	return true;
}

/*******************************************************************************
 * Function Name  : RenderScene
 * Description    : Main function of the program
 *******************************************************************************/
bool OGLESUserClipPlanes::RenderScene()
{
	// Set Vieweport size
	glViewport(0, 0, PVRShellGet(prefWidth), PVRShellGet(prefHeight));

	// Clear the buffers
	glEnable(GL_DEPTH_TEST);

	myglClearColor(f2vt(0.0f), f2vt(0.0f), f2vt(0.0f), f2vt(0.0f));
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Lighting
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	// Texturing
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texName);
	glActiveTexture(GL_TEXTURE0);
	glDisable(GL_BLEND);
	myglTexEnv(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	// Transformations
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	myglTranslate(f2vt(0.0f), f2vt(0.0f), f2vt(-500.0f));
	myglRotate(f2vt((float)nFrame/5.0f),f2vt(0),f2vt(1),f2vt(0));

	// Draw sphere with user clip planes
	SetupUserClipPlanes();
	glDisable(GL_CULL_FACE);
	DrawSphere();
	glDisable(GL_TEXTURE_2D);
	DisableClipPlanes();

	/* Display info text */
	if (bClipPlaneSupported)
	{
		AppPrint3D.DisplayDefaultTitle("UserClipPlanes", "User defined clip planes", PVR_LOGO);
	}
	else
	{
		AppPrint3D.DisplayDefaultTitle("UserClipPlanes", "User clip planes are not available", PVR_LOGO);
	}
	AppPrint3D.Flush();

	/* Increase frame number */
	nFrame++;

	return true;
}

/*!****************************************************************************
 @Function		DrawSphere
 @Description	Draw the rotating sphere
******************************************************************************/
void OGLESUserClipPlanes::DrawSphere()
{
	/* Enable States */
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	/* Set Data Pointers */
	SPODMesh* mesh = g_sScene.pMesh;

	// Used to display non interleaved geometry
	glVertexPointer(3, VERTTYPEENUM, mesh->sVertex.nStride, mesh->sVertex.pData);
	glNormalPointer(VERTTYPEENUM, mesh->sNormals.nStride, mesh->sNormals.pData);
	glTexCoordPointer(2, VERTTYPEENUM, mesh->psUVW->nStride, mesh->psUVW->pData);

	/* Draw */
	glDrawElements(GL_TRIANGLES, mesh->nNumFaces*3, GL_UNSIGNED_SHORT, mesh->sFaces.pData);

	/* Disable States */
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}

/*!****************************************************************************
 @Function		RenderPrimitive
 @Input			pVertex pNormals pUVs nFirst nStripLength mode
 @Modified		None.
 @Output		None.
 @Return		None.
 @Description	Render the central object
******************************************************************************/
void OGLESUserClipPlanes::RenderPrimitive(VERTTYPE *pVertex, VERTTYPE *pNormals, VERTTYPE *pUVs, int nNumIndex, unsigned short *pIDX, GLenum mode)
{
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3,VERTTYPEENUM,0,pVertex);

	glEnableClientState(GL_NORMAL_ARRAY);
	glNormalPointer(VERTTYPEENUM,0,pNormals);

	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2,VERTTYPEENUM,0,pUVs);

	glDrawElements(mode,nNumIndex,GL_UNSIGNED_SHORT,pIDX);

	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}

/*!****************************************************************************
 @Function		SetupUserClipPlanes
 @Description	Setup the user clip planes
******************************************************************************/
void OGLESUserClipPlanes::SetupUserClipPlanes()
{
	if (!bClipPlaneSupported) return;
	VERTTYPE ofs = f2vt(((float)sin(-nFrame / 50.0f) * 10));

	if (nClipPlanes < 1) return;
	VERTTYPE equation0[] = {f2vt(1), 0, f2vt(-1), f2vt(65)+ofs};
	ClipPlane( GL_CLIP_PLANE0, equation0 );
	glEnable( GL_CLIP_PLANE0 );

	if (nClipPlanes < 2) return;
	VERTTYPE equation1[] = {f2vt(-1), 0, f2vt(-1), f2vt(65)+ofs};
	ClipPlane( GL_CLIP_PLANE1, equation1 );
	glEnable( GL_CLIP_PLANE1 );

	if (nClipPlanes < 3) return;
	VERTTYPE equation2[] = {f2vt(-1), 0, f2vt(1), f2vt(65)+ofs};
	ClipPlane( GL_CLIP_PLANE2, equation2 );
	glEnable( GL_CLIP_PLANE2 );

	if (nClipPlanes < 4) return;
	VERTTYPE equation3[] = {f2vt(1), 0, f2vt(1), f2vt(65)+ofs};
	ClipPlane( GL_CLIP_PLANE3, equation3 );
	glEnable( GL_CLIP_PLANE3 );

	if (nClipPlanes < 5) return;
	VERTTYPE equation4[] = {0, f2vt(1), 0, f2vt(40)+ofs};
	ClipPlane( GL_CLIP_PLANE4, equation4 );
	glEnable( GL_CLIP_PLANE4 );

	if (nClipPlanes < 6) return;
	VERTTYPE equation5[] = {0, f2vt(-1), 0, f2vt(40)+ofs};
	ClipPlane( GL_CLIP_PLANE5, equation5 );
	glEnable( GL_CLIP_PLANE5 );
}

/*!****************************************************************************
 @Function		DisableClipPlanes
 @Description	Disable all the user clip planes
******************************************************************************/
void OGLESUserClipPlanes::DisableClipPlanes()
{
	if (!bClipPlaneSupported) return;
	glDisable( GL_CLIP_PLANE0 );
	glDisable( GL_CLIP_PLANE1 );
	glDisable( GL_CLIP_PLANE2 );
	glDisable( GL_CLIP_PLANE3 );
	glDisable( GL_CLIP_PLANE4 );
	glDisable( GL_CLIP_PLANE5 );
}


/*******************************************************************************
 * Function Name  : NewDemo
 * Description    : Called by the Shell to initialize a new instance to the
 *					demo class.
 *******************************************************************************/
PVRShell* NewDemo()
{
	return new OGLESUserClipPlanes();
}

/*****************************************************************************
 End of file (OGLESUserClipPlanes.cpp)
*****************************************************************************/

