/******************************************************************************

 @File         OGLESEvilSkull.cpp

 @Title        EvilSkull

 @Copyright    Copyright (C) 2005 - 2006 by Imagination Technologies Limited.

 @Platform     Independant

 @Description  Shows Animation using Morphing between Key Frames in Software.
               Requires the PVRShell.

******************************************************************************/
#include "PVRShell.h"

#include <math.h>
#include <string.h>

// 3D Model: Skull Geometry Data
#include "OGLESEvilSkull_low.H"

// Textures
#include "Iris.h"		// Eyes
#include "Metal.h"		// Skull

#include "Fire02.h"		// Background
#include "Fire03.h"		// Background

#include "OGLESTools.h"

/****************************************************************************
 ** DEFINES                                                                **
 ****************************************************************************/
#ifndef PI
#define PI 3.14159f
#endif

// Geometry Software Processing Defines
#define NMBR_OF_VERTICES	skull5_NumVertex
#define	NMBR_OF_MORPHTRGTS	4

// Animation Define
#define EXPR_TIME			75.0f

#define NUM_TEXTURES		4

/****************************************************************************
 ** STRUCTURES                                                             **
 ****************************************************************************/

/* A structure which holds the object's data */
struct MyObject
{
	int				nNumberOfTriangles;		/* Number of Triangles */
	VERTTYPE		*pVertices;				/* Vertex coordinates */
	VERTTYPE		*pNormals;				/* Vertex normals */
	VERTTYPE		*pUV;				    /* UVs coordinates */
	unsigned short	*pTriangleList;			/* Triangle list */
};
typedef MyObject* lpMyObject;

/****************************************************************************
** Class: OGLESEvilSkull
****************************************************************************/
class OGLESEvilSkull : public PVRShell
{
    // Print 3D Class Object
	CPVRTPrint3D 	AppPrint3D;

	// vLightPosition
	PVRTVECTOR4	vLightPosition;

	PVRTVECTOR3 Eye, At, Up;
	PVRTMATRIX	MyLookMatrix;

	/* Objects */
	GLuint		pTexture[NUM_TEXTURES];
	MyObject	OGLObject[NUM_MESHES];

	/* Software processing buffers */

	VERTTYPE	MyMorphedVertices[NMBR_OF_VERTICES*3];
	float		fMyAVGVertices[NMBR_OF_VERTICES*3];
	float		fMyDiffVertices[NMBR_OF_VERTICES*3*4];

	/* Animation Params */

	float	fSkull_Weight[5];

	float fExprTbl[4][7];
	float fJawRotation[7];
	float fBackRotation[7];

	int nBaseAnim,nTgtAnim;

	/* Generic */
	int nFrame;

	/* Header Object to Lite Conversion */
	HeaderStruct_Mesh_Type** Meshes;

public:
	OGLESEvilSkull()
	{

		/* Setup base constants in contructor */

		/* Camera and Light details */

		vLightPosition.x=f2vt(-1.0f); vLightPosition.y=f2vt(1.0f); vLightPosition.z=f2vt(1.0f); vLightPosition.w=f2vt(0.0f);

		Eye.x = f2vt(0.0f);			Eye.y = f2vt(0.0f);			Eye.z = f2vt(300.0f);
		At.x  = f2vt(0.0f);			At.y  = f2vt(-30.0f);		At.z  = f2vt(0.0f);
		Up.x  = f2vt(0.0f);			Up.y  = f2vt(1.0f);			Up.z  = f2vt(0.0f);

		/* Animation Table */

		fSkull_Weight[0] = 0.0f;
		fSkull_Weight[1] = 1.0f;
		fSkull_Weight[2] = 0.0f;
		fSkull_Weight[3] = 0.0f;
		fSkull_Weight[4] = 0.0f;

		fExprTbl[0][0]=1.0f;	fExprTbl[1][0]=1.0f;	fExprTbl[2][0]=1.0f;	fExprTbl[3][0]=1.0f;
		fExprTbl[0][1]=0.0f;	fExprTbl[1][1]=0.0f;	fExprTbl[2][1]=0.0f;	fExprTbl[3][1]=1.0f;
		fExprTbl[0][2]=0.0f;	fExprTbl[1][2]=0.0f;	fExprTbl[2][2]=1.0f;	fExprTbl[3][2]=1.0f;
		fExprTbl[0][3]=1.0f;	fExprTbl[1][3]=0.0f;	fExprTbl[2][3]=1.0f;	fExprTbl[3][3]=0.0f;
		fExprTbl[0][4]=-1.0f;	fExprTbl[1][4]=0.0f;	fExprTbl[2][4]=0.0f;	fExprTbl[3][4]=0.0f;
		fExprTbl[0][5]=0.0f;	fExprTbl[1][5]=0.0f;	fExprTbl[2][5]=-1.0f;	fExprTbl[3][5]=0.0f;
		fExprTbl[0][6]=0.0f;	fExprTbl[1][6]=0.0f;	fExprTbl[2][6]=0.0f;	fExprTbl[3][6]=-1.0f;

		fJawRotation[0]=45.0f;
		fJawRotation[1]=25.0f;
		fJawRotation[2]=40.0f;
		fJawRotation[3]=20.0f;
		fJawRotation[4]=45.0f;
		fJawRotation[5]=25.0f;
		fJawRotation[6]=30.0f;

		fBackRotation[0]=0.0f;
		fBackRotation[1]=25.0f;
		fBackRotation[2]=40.0f;
		fBackRotation[3]=90.0f;
		fBackRotation[4]=125.0f;
		fBackRotation[5]=80.0f;
		fBackRotation[6]=30.0f;

		nBaseAnim = 0;
		nTgtAnim  = 1;

		/* Some start values */
		nFrame = 0;
	}

	/* PVRShell functions */
	virtual bool InitApplication();
	virtual bool InitView();
	virtual bool ReleaseView();
	virtual bool QuitApplication();
	virtual bool RenderScene();

	/****************************************************************************
	** Function Definitions
	****************************************************************************/
	void RenderSkull (GLuint pTexture);
	void RenderJaw (GLuint pTexture);
	void CreateObjectFromHeaderFile	(MyObject *pObject, int nObject);
	void CalculateMovement (int nType);
	void DrawQuad (float x,float y,float z,float Size, GLuint pTexture);
	void DrawDualTexQuad (float x,float y,float z,float Size, GLuint pTexture1, GLuint PTexture2);
	void doRenderScene();
};

/*******************************************************************************
 * Function Name  : InitApplication
 * Inputs		  : argc, *argv[], uWidth, uHeight
 * Returns        : true if no error occured
 * Description    : Code in InitApplication() will be called by the Shell ONCE per
 *					run, early on in the execution of the program.
 *					Used to initialize variables that are not dependant on the
 *					rendering context (e.g. external modules, loading meshes, etc.)
 *******************************************************************************/
bool OGLESEvilSkull::InitApplication()
{
	int i;

	Meshes = new HeaderStruct_Mesh_Type*[NUM_MESHES];
	for(i = 0; i < NUM_MESHES; i++)
		Meshes[i] = PVRTLoadHeaderObject(&Mesh[i]);

	/* Initialise Meshes */
	for (i=0; i<NUM_MESHES; i++)
	{
		CreateObjectFromHeaderFile(&OGLObject[i], i);
	}
	return true;
}

/*******************************************************************************
 * Function Name  : QuitApplication
 * Returns        : true if no error occured
 * Description    : Code in QuitApplication() will be called by the Shell ONCE per
 *					run, just before exiting the program.
 *******************************************************************************/
bool OGLESEvilSkull::QuitApplication()
{
	int i;

	/* Release Geometry */
	/* free allocated memory */
	for(i=0; i<NUM_MESHES; i++)
	{
		PVRTUnloadHeaderObject(Meshes[i]);
	}
	delete [] Meshes;
	return true;
}

/*******************************************************************************
 * Function Name  : InitView
 * Inputs		  : uWidth, uHeight
 * Returns        : true if no error occured
 * Description    : Code in InitView() will be called by the Shell upon a change
 *					in the rendering context.
 *					Used to initialize variables that are dependant on the rendering
 *					context (e.g. textures, vertex buffers, etc.)
 *******************************************************************************/
bool OGLESEvilSkull::InitView()
{
	VERTTYPE fVal[4];
	int i,j;
	PVRTMATRIX		MyPerspMatrix;
	SPVRTContext	Context;
	int				dwCurrentWidth;
	int				dwCurrentHeight;

	/* Initialize Print3D textures */
	if(!AppPrint3D.SetTextures(&Context,PVRShellGet(prefWidth),PVRShellGet(prefHeight)))
	{
		PVRShellOutputDebug ("ERROR: Cannot initialise Print3D\n");
		return false;
	}

	dwCurrentWidth = PVRShellGet(prefWidth);
	dwCurrentHeight = PVRShellGet(prefHeight);

	/***********************
	** LOAD TEXTURES     **
	***********************/
	if(!PVRTLoadTextureFromPointer((void*)Iris, &pTexture[0]))
		return false;
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if(!PVRTLoadTextureFromPointer((void*)Metal, &pTexture[1]))
		return false;
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if(!PVRTLoadTextureFromPointer((void*)Fire02, &pTexture[2]))
		return false;
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if(!PVRTLoadTextureFromPointer((void*)Fire03, &pTexture[3]))
		return false;
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	/******************************
	** GENERIC RENDER STATES     **
	*******************************/

	// The Type Of Depth Test To Do
	glDepthFunc(GL_LEQUAL);

	// Enables Depth Testing
	glEnable(GL_DEPTH_TEST);

	// Enables Smooth Color Shading
	glShadeModel(GL_SMOOTH);

	// Blending mode
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	/* Create perspective matrix */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if(PVRShellGet(prefIsRotated) && PVRShellGet(prefFullScreen))
	{
		myglRotate(f2vt(90), f2vt(0), f2vt(0), f2vt(1));
		dwCurrentWidth = PVRShellGet(prefHeight);
		dwCurrentHeight = PVRShellGet(prefWidth);
	}

	/* Culling */
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	PVRTMatrixPerspectiveFovRH(MyPerspMatrix, f2vt(70.0f*(3.14f/180.0f)), f2vt((float)dwCurrentWidth/(float)dwCurrentHeight), f2vt(10.0f), f2vt(10000.0f));
	myglMultMatrix(MyPerspMatrix.f);

	/* Create viewing matrix */
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	PVRTMatrixLookAtRH(MyLookMatrix, Eye, At, Up);
	myglMultMatrix(MyLookMatrix.f);

	/* Enable texturing */
	glEnable(GL_TEXTURE_2D);

	/* Lights (only one side lighting) */
	glEnable(GL_LIGHTING);

	/* Light 0 (White directional light) */
	fVal[0]=f2vt(0.2f); fVal[1]=f2vt(0.2f); fVal[2]=f2vt(0.2f); fVal[3]=f2vt(1.0f);
	myglLightv(GL_LIGHT0, GL_AMBIENT, fVal);

	fVal[0]=f2vt(1.0f); fVal[1]=f2vt(1.0f); fVal[2]=f2vt(1.0f); fVal[3]=f2vt(1.0f);
	myglLightv(GL_LIGHT0, GL_DIFFUSE, fVal);

	fVal[0]=f2vt(1.0f); fVal[1]=f2vt(1.0f); fVal[2]=f2vt(1.0f); fVal[3]=f2vt(1.0f);
	myglLightv(GL_LIGHT0, GL_SPECULAR, fVal);

	myglLightv(GL_LIGHT0, GL_POSITION, &vLightPosition.x);

	glEnable(GL_LIGHT0);

	glDisable(GL_LIGHTING);

	/* Calculate AVG Model for Morphing */
	for (i=0; i<NMBR_OF_VERTICES*3;i++)
	{
		fMyAVGVertices[i]=0;

		for (j=0; j<NMBR_OF_MORPHTRGTS;j++)
		{
			fMyAVGVertices[i]+=Mesh[j].pVertex[i]*0.25f; // Use Header Data Directly because it has to stay float
		}
	}

	/* Calculate Differences for Morphing */
	for (i=0; i<NMBR_OF_VERTICES*3;i++)
	{
		fMyDiffVertices[i*4+0]=fMyAVGVertices[i]-Mesh[0].pVertex[i];
		fMyDiffVertices[i*4+1]=fMyAVGVertices[i]-Mesh[1].pVertex[i];
		fMyDiffVertices[i*4+2]=fMyAVGVertices[i]-Mesh[2].pVertex[i];
		fMyDiffVertices[i*4+3]=fMyAVGVertices[i]-Mesh[3].pVertex[i];
	}

	return true;
}

/*******************************************************************************
 * Function Name  : ReleaseView
 * Returns        : Nothing
 * Description    : Code in ReleaseView() will be called by the Shell before
 *					changing to a new rendering context.
 *******************************************************************************/
bool OGLESEvilSkull::ReleaseView()
{
	int i;

	/* release all textures */
	for(i = 0; i < NUM_TEXTURES; i++)
	{
		PVRTReleaseTexture(pTexture[i]);
	}

	/* Release Print3D Textures */
	AppPrint3D.ReleaseTextures();
	return true;
}


/*******************************************************************************
 * Function Name  : RenderScene
 * Returns		  : true if no error occured
 * Description    : Main rendering loop function of the program. The shell will
 *					call this function every frame.
 *******************************************************************************/
bool OGLESEvilSkull::RenderScene()
{
	/* View Port */
	glViewport(0,0,PVRShellGet(prefWidth),PVRShellGet(prefHeight));

	/* Actual Render */
	doRenderScene();

	/* Display info text. */
	AppPrint3D.DisplayDefaultTitle("EvilSkull", "Morphing.", PVR_LOGO);
	AppPrint3D.Flush();

	return true;
}

/*******************************************************************************
 * Function Name  : doRenderScene
 * Returns		  : None
 * Description    : Actual Rendering
 *******************************************************************************/
void OGLESEvilSkull::doRenderScene()
{
	register int i;
	float fCurrentfJawRotation,fCurrentfBackRotation;
	float fFactor,fInvFactor;

	/* Update Skull Weights and Rotations using Animation Info */
	if (nFrame>EXPR_TIME)
	{
		nFrame=0;
		nBaseAnim=nTgtAnim;

		nTgtAnim++;

		if (nTgtAnim>6)
		{
			nTgtAnim=0;
		}
	}

	fFactor=float(nFrame)/EXPR_TIME;
	fInvFactor=1.0f-fFactor;

	fSkull_Weight[0] = (fExprTbl[0][nBaseAnim]*fInvFactor)+(fExprTbl[0][nTgtAnim]*fFactor);
	fSkull_Weight[1] = (fExprTbl[1][nBaseAnim]*fInvFactor)+(fExprTbl[1][nTgtAnim]*fFactor);
	fSkull_Weight[2] = (fExprTbl[2][nBaseAnim]*fInvFactor)+(fExprTbl[2][nTgtAnim]*fFactor);
	fSkull_Weight[3] = (fExprTbl[3][nBaseAnim]*fInvFactor)+(fExprTbl[3][nTgtAnim]*fFactor);

	fCurrentfJawRotation = fJawRotation[nBaseAnim]*fInvFactor+(fJawRotation[nTgtAnim]*fFactor);
	fCurrentfBackRotation = fBackRotation[nBaseAnim]*fInvFactor+(fBackRotation[nTgtAnim]*fFactor);

	/* Update Base Animation Value - FrameBased Animation for now */
	nFrame++;

	/* Update Skull Vertex Data using Animation Params */
	for (i=0; i<NMBR_OF_VERTICES*3;i++)
	{
		MyMorphedVertices[i]=f2vt(fMyAVGVertices[i] + (fMyDiffVertices[i*4+0] * fSkull_Weight[0]) \
												    + (fMyDiffVertices[i*4+1] * fSkull_Weight[1]) \
												    + (fMyDiffVertices[i*4+2] * fSkull_Weight[2]) \
												    + (fMyDiffVertices[i*4+3] * fSkull_Weight[3]) );
	}

	/* Buffer Clear */
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/* Render Skull and Jaw Opaque with Lighting */
	glDisable(GL_BLEND);		// Opaque = No Blending
	glEnable(GL_LIGHTING);		// Lighting On

	/* Render Animated Jaw - Rotation Only */
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	glLoadIdentity();

	myglMultMatrix(MyLookMatrix.f);

	myglTranslate(f2vt(0),f2vt(-50.0f),f2vt(-50.0f));

	myglRotate(f2vt(-fCurrentfJawRotation), f2vt(1.0f), f2vt(0.0f), f2vt(0.0f));
	myglRotate(f2vt(fCurrentfJawRotation) - f2vt(30.0f), f2vt(0), f2vt(1.0f), f2vt(-1.0f));

	RenderJaw (pTexture[1]);

	glPopMatrix();

	/* Render Morphed Skull */

	glPushMatrix();

	myglRotate(f2vt(fCurrentfJawRotation) - f2vt(30.0f), f2vt(0), f2vt(1.0f), f2vt(-1.0f));

	RenderSkull (pTexture[1]);

	/* Render Eyes and Background with Alpha Blending and No Lighting*/

	glEnable(GL_BLEND);			// Enable Alpha Blending
	glDisable(GL_LIGHTING);		// Disable Lighting

	/* Render Eyes using Skull Model Matrix */
	DrawQuad (-30.0f ,0.0f ,50.0f ,20.0f , pTexture[0]);
	DrawQuad ( 33.0f ,0.0f ,50.0f ,20.0f , pTexture[0]);
	glPopMatrix();

	/* Render Dual Texture Background with different base color, rotation, and texture rotation */

	glPushMatrix();

	glDisable(GL_BLEND);			// Disable Alpha Blending

	myglColor4(f2vt(0.7f+0.3f*((fSkull_Weight[0]))), f2vt(0.7f), f2vt(0.7f), f2vt(1.0f));	// Animated Base Color
	myglTranslate(f2vt(10.0f), f2vt(-50.0f), f2vt(0.0f));
	myglRotate(f2vt(fCurrentfBackRotation*4.0f),f2vt(0),f2vt(0),f2vt(-1.0f));	// Rotation of Quad

	/* Animated Texture Matrix */
	glActiveTexture(GL_TEXTURE0);
	glMatrixMode(GL_TEXTURE);
	glLoadIdentity();
	myglTranslate(f2vt(-0.5f), f2vt(-0.5f), f2vt(0.0f));
	myglRotate(f2vt(fCurrentfBackRotation*-8.0f), f2vt(0), f2vt(0), f2vt(-1.0f));
	myglTranslate(f2vt(-0.5f), f2vt(-0.5f), f2vt(0.0f));

	/* Draw Geometry */
	DrawDualTexQuad (0.0f ,0.0f ,-100.0f ,300.0f, pTexture[3], pTexture[2]);

	/* Disable Animated Texture Matrix */
	glActiveTexture(GL_TEXTURE0);
	glMatrixMode(GL_TEXTURE);
	glLoadIdentity();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	/* Reset Color */
	myglColor4(f2vt(1.0f), f2vt(1.0f), f2vt(1.0f), f2vt(1.0f));
}

/*******************************************************************************
 * Function Name  : CreateObjectFromHeaderFile
 * Input/Output	  :
 * Global Used    :
 * Description    : Function to initialise object from a .h file
 *******************************************************************************/

/* This is done so that we can modify the data (which we couldn't do
 if it was actually the static constant array from the header */

void OGLESEvilSkull::CreateObjectFromHeaderFile (MyObject *pObj, int nObject)
{
	/* Get model info */
	pObj->nNumberOfTriangles	= Meshes[nObject]->nNumFaces;

	/* Vertices */
	pObj->pVertices=(VERTTYPE*)Meshes[nObject]->pVertex;

	/* Normals */
	pObj->pNormals=(VERTTYPE*)Meshes[nObject]->pNormals;

	/* Get triangle list data */
	pObj->pTriangleList=(unsigned short *)Meshes[nObject]->pFaces;

	/* UVs */
	pObj->pUV = Meshes[nObject]->pUV;

}

/*******************************************************************************
 * Function Name  : RenderSkull
 * Input		  : Texture Pntr and Filter Mode
 * Returns        :
 * Global Used    :
 * Description    : Renders the Skull data using the Morphed Data Set.
 *******************************************************************************/
void OGLESEvilSkull::RenderSkull (GLuint pTexture)
{
	/* Enable texturing */
	glBindTexture(GL_TEXTURE_2D, pTexture);

	/* Enable and set vertices, normals and index data */
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer		(3, VERTTYPEENUM, 0, MyMorphedVertices);

	if(OGLObject[1].pNormals)
	{
		glEnableClientState(GL_NORMAL_ARRAY);
		glNormalPointer	(VERTTYPEENUM, 0, OGLObject[1].pNormals);
	}

	if(OGLObject[1].pUV)
	{
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glTexCoordPointer	(2, VERTTYPEENUM, 0, OGLObject[1].pUV);
	}

	/* Draw mesh */
	glDrawElements(GL_TRIANGLES, OGLObject[1].nNumberOfTriangles*3, GL_UNSIGNED_SHORT, OGLObject[2].pTriangleList);

	/* Make sure to disable the arrays */

	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);

}

/*******************************************************************************
 * Function Name  : RenderJaw
 * Input		  : Texture Pntr and Filter Mode
 * Returns        :
 * Global Used    :
 * Description    : Renders the Skull Jaw - uses direct data no morphing
 *******************************************************************************/
void OGLESEvilSkull::RenderJaw (GLuint pTexture)
{
	/* Bind correct texture */
	glBindTexture(GL_TEXTURE_2D, pTexture);

	/* Enable and set vertices, normals and index data */
	if(OGLObject[M_JAW].pVertices)
	{
		glEnableClientState(GL_VERTEX_ARRAY);
		glVertexPointer		(3, VERTTYPEENUM, 0, OGLObject[4].pVertices);
	}

	if(OGLObject[M_JAW].pNormals)
	{
		glEnableClientState(GL_NORMAL_ARRAY);
		glNormalPointer	(VERTTYPEENUM, 0, OGLObject[4].pNormals);
	}

	if(OGLObject[M_JAW].pUV)
	{
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glTexCoordPointer	(2, VERTTYPEENUM, 0, OGLObject[4].pUV);
	}

	/* Draw mesh */
	glDrawElements(GL_TRIANGLES, OGLObject[4].nNumberOfTriangles*3, GL_UNSIGNED_SHORT, OGLObject[4].pTriangleList);

	/* Make sure to disable the arrays */

	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);

}

/*******************************************************************************
 * Function Name  : DrawQuad
 * Input		  : Size, (x,y,z) and texture pntr
 * Returns        :
 * Global Used    :
 * Description    : Basic Draw Quad with Size in Location X, Y, Z.
 *******************************************************************************/
void OGLESEvilSkull::DrawQuad (float x,float y,float z,float Size, GLuint pTexture)
{
	/* Bind correct texture */
	glBindTexture(GL_TEXTURE_2D, pTexture);

	/* Vertex Data */
	VERTTYPE verts[] =		{	f2vt(x+Size), f2vt(y-Size), f2vt(z),
								f2vt(x+Size), f2vt(y+Size), f2vt(z),
								f2vt(x-Size), f2vt(y-Size), f2vt(z),
								f2vt(x-Size), f2vt(y+Size), f2vt(z)
							};

	VERTTYPE texcoords[] =	{	f2vt(0.0f), f2vt(1.0f),
								f2vt(0.0f), f2vt(0.0f),
								f2vt(1.0f), f2vt(1.0f),
								f2vt(1.0f), f2vt(0.0f)
							};

	/* Set Arrays - Only need Vertex Array and Tex Coord Array*/
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3,VERTTYPEENUM,0,verts);

	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2,VERTTYPEENUM,0,texcoords);

	/* Draw Strip */
	glDrawArrays(GL_TRIANGLE_STRIP,0,4);

	/* Disable Arrays */
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
}

/*******************************************************************************
 * Function Name  : DrawDualTexQuad
 * Input		  : Size, (x,y,z) and texture pntr
 * Returns        :
 * Global Used    :
 * Description    : Basic Draw Dual Textured Quad with Size in Location X, Y, Z.
 *******************************************************************************/
void OGLESEvilSkull::DrawDualTexQuad (float x,float y,float z,float Size, GLuint pTexture1, GLuint pTexture2)
{
	/* Set Texture and Texture Options */
	glBindTexture(GL_TEXTURE_2D, pTexture1);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, pTexture2);
	glEnable(GL_TEXTURE_2D);

	/* Vertex Data */
	VERTTYPE verts[] =		{	f2vt(x+Size), f2vt(y-Size), f2vt(z),
								f2vt(x+Size), f2vt(y+Size), f2vt(z),
								f2vt(x-Size), f2vt(y-Size), f2vt(z),
								f2vt(x-Size), f2vt(y+Size), f2vt(z)
							};

	VERTTYPE texcoords[] =	{	f2vt(0.0f), f2vt(1.0f),
								f2vt(0.0f), f2vt(0.0f),
								f2vt(1.0f), f2vt(1.0f),
								f2vt(1.0f), f2vt(0.0f)
							};

	/* Set Arrays - Only need Vertex Array and Tex Coord Arrays*/
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3,VERTTYPEENUM,0,verts);

    glClientActiveTexture(GL_TEXTURE0);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2,VERTTYPEENUM,0,texcoords);

	glClientActiveTexture(GL_TEXTURE1);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2,VERTTYPEENUM,0,texcoords);

	/* Draw Strip */
	glDrawArrays(GL_TRIANGLE_STRIP,0,4);

	/* Disable Arrays */
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);

	glClientActiveTexture(GL_TEXTURE0);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);

	glDisableClientState(GL_VERTEX_ARRAY);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);

	glActiveTexture(GL_TEXTURE0);

}

/*******************************************************************************
 * Function Name  : NewDemo
 * Description    : Called by the Shell to initialize a new instance to the
 *					demo class.
 *******************************************************************************/
PVRShell* NewDemo(){
	return new OGLESEvilSkull();
}

/*****************************************************************************
 End of file (OGLESEvilSkull.cpp)
*****************************************************************************/
