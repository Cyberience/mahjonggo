/******************************************************************************

 @File         OGLESIntroducingPVRTools.cpp

 @Title        Introducing the PVRTools

 @Copyright    Copyright (C) 2000 - 2006 by Imagination Technologies Limited.

 @Platform     Independant

 @Description  Shows how to use the tools to load textures and display text

******************************************************************************/
#include <GLES/egl.h>

#include "PVRShell.h"
#include "OGLESTools.h"

#include "image.h"

/*!****************************************************************************
 Class implementing the PVRShell functions.
******************************************************************************/
class OGLESIntroducingPVRTools : public PVRShell
{
	// Print3D class used to display text
	CPVRTPrint3D	m_Print3D;

	// Texture handle
	GLuint	m_uiTexture;

public:
	virtual bool InitApplication();
	virtual bool InitView();
	virtual bool ReleaseView();
	virtual bool QuitApplication();
	virtual bool RenderScene();
};


/*!****************************************************************************
 @Function		InitApplication
 @Return		bool		true if no error occured
 @Description	Code in InitApplication() will be called by PVRShell once per
				run, before the rendering context is created.
				Used to initialize variables that are not dependant on it
				(e.g. external modules, loading meshes, etc.)
				If the rendering context is lost, InitApplication() will
				not be called again.
******************************************************************************/
bool OGLESIntroducingPVRTools::InitApplication()
{
	return true;
}

/*!****************************************************************************
 @Function		QuitApplication
 @Return		bool		true if no error occured
 @Description	Code in QuitApplication() will be called by PVRShell once per
				run, just before exiting the program.
				If the rendering context is lost, QuitApplication() will
				not be called.
******************************************************************************/
bool OGLESIntroducingPVRTools::QuitApplication()
{
    return true;
}

/*!****************************************************************************
 @Function		InitView
 @Return		bool		true if no error occured
 @Description	Code in InitView() will be called by PVRShell upon
				initialization or after a change in the rendering context.
				Used to initialize variables that are dependant on the rendering
				context (e.g. textures, vertex buffers, etc.)
******************************************************************************/
bool OGLESIntroducingPVRTools::InitView()
{
	/*
		Initialize the textures used by Print3D.
		To properly display text, Print3D needs to now the viewport dimensions.
		We get them using the shell function PVRShellGet(prefWidth/prefHeight).
	*/
	if(!m_Print3D.SetTextures(0,PVRShellGet(prefWidth),PVRShellGet(prefHeight)))
	{
		PVRShellSet(prefExitMessage, "ERROR: Cannot initialise Print3D\n");
		return false;
	}

	// Sets the clear color
	myglClearColor(f2vt(0.6f), f2vt(0.8f), f2vt(1.0f), f2vt(1.0f));

	// Enables texturing
	glEnable(GL_TEXTURE_2D);

	/*
		Loads the texture using the tool function PVRTLoadTextureFromPointer.
		The first parameter is a pointer to the data in memory and the
		second parameter returns the resulting texture handle.
		We could also use PVRTLoadTextureFromPVR() to load the texture from an
		external .PVR file.
	*/
	if(!PVRTLoadTextureFromPointer((void*)image, &m_uiTexture))
	{
		PVRShellSet(prefExitMessage, "ERROR: Cannot load the texture\n");
		return false;
	}

	// The texture we loaded contains mipmap levels so we can interpolate between them
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// Calculates the projection matrix
	bool bRotate = PVRShellGet(prefIsRotated) && PVRShellGet(prefFullScreen);
	if (bRotate)
	{
		glMatrixMode(GL_PROJECTION);
		PVRTMATRIX mRotate;
		PVRTMatrixRotationZ(mRotate, f2vt(-90.0f * PVRTPIf / 180.0f));
		myglLoadMatrix(mRotate.f);
	}

	return true;
}

/*!****************************************************************************
 @Function		ReleaseView
 @Return		bool		true if no error occured
 @Description	Code in ReleaseView() will be called by PVRShell when the
				application quits or before a change in the rendering context.
******************************************************************************/
bool OGLESIntroducingPVRTools::ReleaseView()
{
	// Frees the texture
	PVRTReleaseTexture(m_uiTexture);

	// Release Print3D Textures 
	m_Print3D.ReleaseTextures();

	return true;
}

/*!****************************************************************************
 @Function		RenderScene
 @Return		bool		true if no error occured
 @Description	Main rendering loop function of the program. The shell will
				call this function every frame.
				eglSwapBuffers() will be performed by PVRShell automatically.
				PVRShell will also manage important OS events.
				Will also manage relevent OS events. The user has access to
				these events through an abstraction layer provided by PVRShell.
******************************************************************************/
bool OGLESIntroducingPVRTools::RenderScene()
{
	// Clears the color and depth buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Binds the loaded texture
	glBindTexture(GL_TEXTURE_2D, m_uiTexture);

	/*
		Draw a triangle.
		Please refer to the training course IntroducingPVRShell for a detailed explanation.
	*/

	// Pass the vertex data
	VERTTYPE afVertices[] = {f2vt(-0.4f),f2vt(-0.4f),f2vt(0.0f),  f2vt(0.4f),f2vt(-0.4f),f2vt(0.0f),  f2vt(0.0f),f2vt(0.4f),f2vt(0.0f)};
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3,VERTTYPEENUM,0,afVertices);

	// Pass the texture coordinates data
	VERTTYPE afTexCoord[] = {f2vt(0.0f),f2vt(0.0f),  f2vt(1.0f),f2vt(0.0f), f2vt(0.5f),f2vt(1.0f)};
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2,VERTTYPEENUM,0,afTexCoord);

	// Draws a non-indexed triangle array
	glDrawArrays(GL_TRIANGLES, 0, 3);

	/*
		Display some text.
		Print3D() function allows to draw text anywhere on the screen using any color.
		Param 1: Position of the text along X (from 0 to 100 scale independent)
		Param 2: Position of the text along Y (from 0 to 100 scale independent)
		Param 3: Scale of the text
		Param 4: Colour of the text (0xAABBGGRR format)
		Param 5: Formated string (uses the same sintax as printf)
	*/
	m_Print3D.Print3D(8.0f, 30.0f, 1.5f, 0xFFAA4040, "example");

	/*
		DisplayDefaultTitle() writes a title and description text on the top left of the screen.
		It can also display the PVR logo (PVR_LOGO), the IMG logo (IMG_LOGO) or both (PVR_LOGO | IMG_LOGO).
		Set this last parameter to NULL not to display the logos.
	*/
	m_Print3D.DisplayDefaultTitle("IntroducingPVRTools", "Description", PVR_LOGO);

	// Tells Print3D to do all the pending text rendering now
	m_Print3D.Flush();

	return true;
}

/*!****************************************************************************
 @Function		NewDemo
 @Return		PVRShell*		The demo supplied by the user
 @Description	This function must be implemented by the user of the shell.
				The user should return its PVRShell object defining the
				behaviour of the application.
******************************************************************************/
PVRShell* NewDemo()
{
	return new OGLESIntroducingPVRTools();
}

/******************************************************************************
 End of file (OGLESIntroducingPVRTools.cpp)
******************************************************************************/
