/******************************************************************************

 @File         OGLESIntroducingPOD.cpp

 @Title        Introducing the POD 3d file format

 @Copyright    Copyright (C) 2000 - 2006 by Imagination Technologies Limited.

 @Platform     Independant

 @Description  Shows how to load POD files and play the animation with basic
               lighting

******************************************************************************/
#include <string.h>
#include <GLES/egl.h>

#include "PVRShell.h"
#include "OGLESTools.h"

#include "tex_base.h"
#include "tex_arm.h"

/*
	We exported the scene from 3DSMax into two .h files.
	One for the Common (floating point) profile, the other one for CommonLite (fixed point) profile.
*/
#ifdef OGLESLITE
#include "scene_fixed.h"
#else
#include "scene_float.h"
#endif

/******************************************************************************
 Defines
******************************************************************************/

// Camera constants. Used for making the projection matrix
#define CAM_ASPECT	(1.333333333f)
#define CAM_NEAR	(4.0f)
#define CAM_FAR		(5000.0f)

#define DEMO_FRAME_RATE	(1.0f / 30.0f)

/*!****************************************************************************
 Class implementing the PVRShell functions.
******************************************************************************/
class OGLESIntroducingPOD : public PVRShell
{
	// Print3D class used to display text
	CPVRTPrint3D	m_Print3D;

	// Texture handle
	GLuint			m_uiTex_base, m_uiTex_arm;

	// 3D Model
	CPVRTPODScene	m_Scene;

	// Projection and Model View matrices
	PVRTMATRIX		m_mProjection, m_mView;

	// Array to lookup the textures for each material in the scene
	GLuint*			m_puiTextures;

	// Variables to handle the animation in a time-based manner
	int				m_iTimePrev;
	VERTTYPE		m_fFrame;

public:
	virtual bool InitApplication();
	virtual bool InitView();
	virtual bool ReleaseView();
	virtual bool QuitApplication();
	virtual bool RenderScene();

	void DrawMesh(SPODMesh* mesh);
};


/*!****************************************************************************
 @Function		InitApplication
 @Return		bool		true if no error occured
 @Description	Code in InitApplication() will be called by PVRShell once per
				run, before the rendering context is created.
				Used to initialize variables that are not dependant on it
				(e.g. external modules, loading meshes, etc.)
				If the rendering context is lost, InitApplication() will
				not be called again.
******************************************************************************/
bool OGLESIntroducingPOD::InitApplication()
{

	/*
		Loads the scene from the .h file into a CPVRTPODScene object.
		We could also export the scene into a binary .pod file and
		load it with ReadFromFile().
	*/
#ifdef OGLESLITE
	m_Scene.ReadFromMemory(c_SCENE_FIXED_H);
#else
	m_Scene.ReadFromMemory(c_SCENE_FLOAT_H);
#endif

	// The cameras are stored in the file. We check it contains at least one.
	if (m_Scene.nNumCamera == 0)
	{
		PVRShellSet(prefExitMessage, "ERROR: The scene does not contain a camera\n");
		return false;
	}

	// Initialize variables used for the animation
	m_fFrame = 0;
	m_iTimePrev = PVRShellGetTime();

	return true;
}

/*!****************************************************************************
 @Function		QuitApplication
 @Return		bool		true if no error occured
 @Description	Code in QuitApplication() will be called by PVRShell once per
				run, just before exiting the program.
				If the rendering context is lost, QuitApplication() will
				not be called.
******************************************************************************/
bool OGLESIntroducingPOD::QuitApplication()
{
	// Frees the memory allocated for the scene
	m_Scene.Destroy();

    return true;
}

/*!****************************************************************************
 @Function		InitView
 @Return		bool		true if no error occured
 @Description	Code in InitView() will be called by PVRShell upon
				initialization or after a change in the rendering context.
				Used to initialize variables that are dependant on the rendering
				context (e.g. textures, vertex buffers, etc.)
******************************************************************************/
bool OGLESIntroducingPOD::InitView()
{
	/*
		Initialize Print3D
	*/
	if(!m_Print3D.SetTextures(0,PVRShellGet(prefWidth),PVRShellGet(prefHeight)))
	{
		PVRShellSet(prefExitMessage, "ERROR: Cannot initialise Print3D\n");
		return false;
	}

	// Sets the clear color
	myglClearColor(f2vt(0.2f), f2vt(0.2f), f2vt(0.2f), f2vt(1.0f));

	// Enables texturing
	glEnable(GL_TEXTURE_2D);

	/*
		Loads the texture.
		For a more detailed explanation, see Texturing and IntroducingPVRTools
	*/
	if(!PVRTLoadTextureFromPointer((void*)tex_base, &m_uiTex_base))
	{
		PVRShellSet(prefExitMessage, "ERROR: Cannot load the texture\n");
		return false;
	}
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if(!PVRTLoadTextureFromPointer((void*)tex_arm, &m_uiTex_arm))
	{
		PVRShellSet(prefExitMessage, "ERROR: Cannot load the texture\n");
		return false;
	}
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// Enables lighting. See BasicTnL for a detailed explanation
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	/*
		Loads the light direction from the scene.
	*/
	// We check the scene contains at least one
	if (m_Scene.nNumLight == 0)
	{
		PVRShellSet(prefExitMessage, "ERROR: The scene does not contain a light\n");
		return false;
	}

	/*
		Initializes an array to lookup the textures
		for each materials in the scene.
	*/
	m_puiTextures = new GLuint[m_Scene.nNumMaterial];
	for (int i=0; i<(int)m_Scene.nNumMaterial; i++)
	{
		m_puiTextures[i] = 0;
		SPODMaterial* pMaterial = &m_Scene.pMaterial[i];
		if (!strcmp(pMaterial->pszName, "Mat_Base"))
		{
			m_puiTextures[i] = m_uiTex_base;
		}
		if (!strcmp(pMaterial->pszName, "Mat_Arm"))
		{
			m_puiTextures[i] = m_uiTex_arm;
		}
	}

	return true;
}

/*!****************************************************************************
 @Function		ReleaseView
 @Return		bool		true if no error occured
 @Description	Code in ReleaseView() will be called by PVRShell when the
				application quits or before a change in the rendering context.
******************************************************************************/
bool OGLESIntroducingPOD::ReleaseView()
{
	// Frees the texture lookup array
	delete [] m_puiTextures;

	// Frees the texture
	PVRTReleaseTexture(m_uiTex_arm);
	PVRTReleaseTexture(m_uiTex_base);

	// Release Print3D Textures
	m_Print3D.ReleaseTextures();

	return true;
}

/*!****************************************************************************
 @Function		RenderScene
 @Return		bool		true if no error occured
 @Description	Main rendering loop function of the program. The shell will
				call this function every frame.
				eglSwapBuffers() will be performed by PVRShell automatically.
				PVRShell will also manage important OS events.
				Will also manage relevent OS events. The user has access to
				these events through an abstraction layer provided by PVRShell.
******************************************************************************/
bool OGLESIntroducingPOD::RenderScene()
{
	// Clears the color and depth buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Enables lighting
	glEnable(GL_LIGHTING);

	/*
		Calculates the frame number to animate in a time-based manner.
		Uses the shell function PVRShellGetTime() to get the time in milliseconds.
	*/
	int iTime = PVRShellGetTime();
	int iDeltaTime = iTime - m_iTimePrev;
	m_iTimePrev	= iTime;
	m_fFrame	+= VERTTYPEMUL(f2vt(iDeltaTime), f2vt(DEMO_FRAME_RATE));
	if (m_fFrame > f2vt(m_Scene.nNumFrame-1))
		m_fFrame = 0;

	// Sets the scene animation to this frame
	m_Scene.SetFrame(m_fFrame);

	{
		PVRTVECTOR3	vFrom, vTo, vUp;
		VERTTYPE	fFOV;
		vUp.x = f2vt(0.0f);
		vUp.y = f2vt(1.0f);
		vUp.z = f2vt(0.0f);

		// We can get the camera position, target and field of view (fov) with GetCameraPos()
		fFOV = m_Scene.GetCameraPos( vFrom, vTo, 0);

		/*
			We can build the model view matrix from the camera position, target and an up vector.
			For this we usePVRTMatrixLookAtRH().
		*/
		PVRTMatrixLookAtRH(m_mView, vFrom, vTo, vUp);

		// Calculates the projection matrix
		bool bRotate = PVRShellGet(prefIsRotated) && PVRShellGet(prefFullScreen);
		PVRTMatrixPerspectiveFovRH(m_mProjection, fFOV, f2vt(CAM_ASPECT), f2vt(CAM_NEAR), f2vt(CAM_FAR), bRotate);

		// Loads the projection matrix
		glMatrixMode(GL_PROJECTION);
		myglLoadMatrix(m_mProjection.f);
	}

	// Specify the view matrix to OpenGL ES so we can specify the light in world space
	glMatrixMode(GL_MODELVIEW);
	myglLoadMatrix(m_mView.f);

	{
		// Reads the light direction from the scene.
		PVRTVECTOR4 vLightDirection;
		PVRTVECTOR3 vPos;
		m_Scene.GetLight(vPos, *(PVRTVECTOR3*)&vLightDirection, 0);
		vLightDirection.x = -vLightDirection.x;
		vLightDirection.y = -vLightDirection.y;
		vLightDirection.z = -vLightDirection.z;

		/*
			Sets the w component to 0, so when passing it to glLight(), it is
			considered as a directional light (as opposed to a spot light).
		*/
		vLightDirection.w = 0;

		// Specify the light direction in world space
		myglLightv(GL_LIGHT0, GL_POSITION, (VERTTYPE*)&vLightDirection);
	}

	// Enables the vertices, normals and texture coordinates arrays
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	/*
		A scene is composed of nodes. There are 3 types of nodes:
		- MeshNodes :
			references a mesh in the pMesh[].
			These nodes are at the beginning of the pNode[] array.
			And there are nNumMeshNode number of them.
			This way the .pod format can instantiate several times the same mesh
			with different attributes.
		- lights
		- cameras
		To draw a scene, you must go through all the MeshNodes and draw the referenced meshes.
	*/
	for (int i=0; i<(int)m_Scene.nNumMeshNode; i++)
	{
		SPODNode* pNode = &m_Scene.pNode[i];

		// Gets pMesh referenced by the pNode
		SPODMesh* pMesh = &m_Scene.pMesh[pNode->nIdx];

		// Gets the node model matrix
		PVRTMATRIX mWorld;
		m_Scene.GetWorldMatrix(mWorld, *pNode);

		// Multiply the view matrix by the model (mWorld) matrix to get the model-view matrix
		PVRTMATRIX mModelView;
		PVRTMatrixMultiply(mModelView, mWorld, m_mView);
		myglLoadMatrix(mModelView.f);

		// Loads the correct texture using our texture lookup table
		if (pNode->nIdxMaterial == -1)
		{
			// It has no pMaterial defined. Use blank texture (0)
			glBindTexture(GL_TEXTURE_2D, 0);
		}
		else
		{
			glBindTexture(GL_TEXTURE_2D, m_puiTextures[pNode->nIdxMaterial]);
		}

		/*
			Now that the model-view matrix is set and the materials ready,
			call another function to actually draw the mesh.
		*/
		DrawMesh(pMesh);
	}
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);

	// Displays the demo name using the tools. For a detailed explanation, see the training course IntroducingPVRTools
	m_Print3D.DisplayDefaultTitle("IntroducingPOD", "", PVR_LOGO);
	m_Print3D.Flush();

	return true;
}

/*!****************************************************************************
 @Function		DrawMesh
 @Input			mesh		The mesh to draw
 @Description	Draws a SPODMesh after the model view matrix has been set and
				the meterial prepared.
******************************************************************************/
void OGLESIntroducingPOD::DrawMesh(SPODMesh* pMesh)
{
	/*
		Now we give the vertex and texture coordinates data to OpenGL ES.
		The pMesh has been exported with the "Interleave Vectors" check box on,
		so all the data starts at the address pMesh->pInterleaved but with a different offset.
		Interleaved data makes better use of the cache and thus is faster on embedded devices.
	*/
	glVertexPointer(3, VERTTYPEENUM, pMesh->sVertex.nStride, pMesh->pInterleaved + (long)pMesh->sVertex.pData);
	glNormalPointer(VERTTYPEENUM, pMesh->sNormals.nStride, pMesh->pInterleaved + (long)pMesh->sNormals.pData);
	glTexCoordPointer(2, VERTTYPEENUM, pMesh->psUVW[0].nStride, pMesh->pInterleaved + (long)pMesh->psUVW[0].pData);

	/*
		The geometry can be exported in 4 ways:
		- Non-Indexed Triangle list
		- Indexed Triangle list
		- Non-Indexed Triangle strips
		- Indexed Triangle strips
	*/
	if(!pMesh->nNumStrips)
	{
		if(pMesh->sFaces.pData)
		{
			// Indexed Triangle list
			glDrawElements(GL_TRIANGLES, pMesh->nNumFaces*3, GL_UNSIGNED_SHORT, pMesh->sFaces.pData);
		}
		else
		{
			// Non-Indexed Triangle list
			glDrawArrays(GL_TRIANGLES, 0, pMesh->nNumFaces*3);
		}
	}
	else
	{
		if(pMesh->sFaces.pData)
		{
			// Indexed Triangle strips
			int offset = 0;
			for(int i = 0; i < (int)pMesh->nNumStrips; i++)
			{
				glDrawElements(GL_TRIANGLE_STRIP, pMesh->pnStripLength[i]+2, GL_UNSIGNED_SHORT, pMesh->sFaces.pData + offset*2);
				offset += pMesh->pnStripLength[i]+2;
			}
		}
		else
		{
			// Non-Indexed Triangle strips
			int offset = 0;
			for(int i = 0; i < (int)pMesh->nNumStrips; i++)
			{
				glDrawArrays(GL_TRIANGLE_STRIP, offset, pMesh->pnStripLength[i]+2);
				offset += pMesh->pnStripLength[i]+2;
			}
		}
	}
}

/*!****************************************************************************
 @Function		NewDemo
 @Return		PVRShell*		The demo supplied by the user
 @Description	This function must be implemented by the user of the shell.
				The user should return its PVRShell object defining the
				behaviour of the application.
******************************************************************************/
PVRShell* NewDemo()
{
	return new OGLESIntroducingPOD();
}

/******************************************************************************
 End of file (OGLESIntroducingPOD.cpp)
******************************************************************************/
