#--------------------------------------------------------------------------
# Name         : maketex.mak
# Title        : Makefile to build textures
# Author       : PowerVR
# Created      : 22nd July 2004
#
# Copyright    : 2004 by Imagination Technologies.  All rights reserved.
#              : No part of this software, either material or conceptual 
#              : may be copied or distributed, transmitted, transcribed,
#              : stored in a retrieval system or translated into any 
#              : human or computer language in any form by any means,
#              : electronic, mechanical, manual or other-wise, or 
#              : disclosed to third parties without the express written
#              : permission of VideoLogic Limited, Unit 8, HomePark
#              : Industrial Estate, King's Langley, Hertfordshire,
#              : WD4 8LZ, U.K.
#
# Description  : Makefile for demos in the PowerVR SDK
#
# Platform     :
#
# $Revision: 1.2 $
#--------------------------------------------------------------------------

#############################################################################
## Variables
#############################################################################

PVRTEXTURETOOLPATH = ..\..\..\Utilities\PVRTextureTool\PVRTextureTool\Build\WindowsPC\Release\PVRTextureTool.exe

#############################################################################
## Instructions
#############################################################################

all:	tex_base.h tex_arm.h

tex_base.h: ../Media/tex_base.bmp
	$(PVRTEXTURETOOLPATH) -m -h -fOGLPVRTC4 -i../Media/tex_base.bmp -otex_base.h

tex_arm.h: ../Media/tex_arm.bmp
	$(PVRTEXTURETOOLPATH) -m -h -fOGLPVRTC4 -i../Media/tex_arm.bmp -otex_arm.h
	
############################################################################
# End of file (maketex.mak)
############################################################################