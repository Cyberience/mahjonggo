/******************************************************************************

 @File         OGLESSkybox.cpp

 @Title        Skybox

 @Copyright    Copyright (C) 2005 - 2006 by Imagination Technologies Limited.

 @Platform     Independant

 @Description  Shows Skyboxes with PVRTC compression. In the case of OGLES Lite
               we have to convert the data to fixed point format. So we allocate
               extra buffers for the data.

******************************************************************************/
#include <math.h>
#include "PVRShell.h"
#include "OGLESTools.h"

#ifdef OGLESLITE
#include "balloon_fixed.h"
#else
#include "balloon.h"
#endif


/****************************************************************************
 ** DEFINES                                                                **
 ****************************************************************************/

/* Assuming a 4:3 aspect ratio: */
#define CAM_ASPECT	(1.333333333f)
#define CAM_NEAR	(4.0f)
#define CAM_FAR		(5000.0f)

#define SKYBOX_ZOOM			150.0f
#define SKYBOX_ADJUSTUVS	true

#ifndef PI
#define PI 3.14159f
#endif

/****************************************************************************
** Class: OGLESSkybox
****************************************************************************/
class OGLESSkybox : public PVRShell
{
	/* Texture IDs */
	GLuint balloonTex;
	GLuint skyboxTex[6];

	/* Print3D, Extension and POD Class Objects */
	CPVRTPrint3D 		gsMyPrint3D;
	CPVRTPODScene		g_sScene;
	CPVRTglesExt		g_PVRTglesExt;

	/* View and Projection Matrices */
	PVRTMATRIX	g_mView, g_mProj;

	/* Skybox */
	VERTTYPE* g_skyboxVertices;
	VERTTYPE* g_skyboxUVs;

	/* View Variables */
	VERTTYPE fViewAngle;
	VERTTYPE fViewDistance, fViewAmplitude, fViewAmplitudeAngle;
	VERTTYPE fViewUpDownAmplitude, fViewUpDownAngle;

	/* Vectors for calculating the view matrix and saving the camera position*/
	PVRTVECTOR3 vTo, vUp, vCameraPosition;

public:
	OGLESSkybox()
	{
		/* Init values to defaults */
		fViewAngle = PVRTPIOVERTWO;

		fViewDistance = f2vt(100.0f);
		fViewAmplitude = f2vt(60.0f);
		fViewAmplitudeAngle = f2vt(0.0f);

		fViewUpDownAmplitude = f2vt(50.0f);
		fViewUpDownAngle = f2vt(0.0f);

		vTo.x = f2vt(0);
		vTo.y = f2vt(0);
		vTo.z = f2vt(0);

		vUp.x = f2vt(0);
		vUp.y = f2vt(1);
		vUp.z = f2vt(0);

	}

	/* PVRShell functions */
	virtual bool InitApplication();
	virtual bool InitView();
	virtual bool ReleaseView();
	virtual bool QuitApplication();
	virtual bool RenderScene();

	/****************************************************************************
	** Function Definitions
	****************************************************************************/
	void CameraGetMatrix();
	void ComputeViewMatrix();
	void DrawSkybox();
	void DrawBalloon();
};


/*******************************************************************************
 * Function Name  : InitApplication
 * Inputs		  : argc, *argv[], uWidth, uHeight
 * Returns        : true if no error occured
 * Description    : Code in InitApplication() will be called by the Shell ONCE per
 *					run, early on in the execution of the program.
 *					Used to initialize variables that are not dependant on the
 *					rendering context (e.g. external modules, loading meshes, etc.)
 *******************************************************************************/
bool OGLESSkybox::InitApplication()
{
#ifdef OGLESLITE
	g_sScene.ReadFromMemory(c_BALLOON_FIXED_H);
#else
	g_sScene.ReadFromMemory(c_BALLOON_H);
#endif
	return true;
}


/*******************************************************************************
 * Function Name  : QuitApplication
 * Returns        : true if no error occured
 * Description    : Code in QuitApplication() will be called by the Shell ONCE per
 *					run, just before exiting the program.
 *******************************************************************************/
bool OGLESSkybox::QuitApplication()
{
	g_sScene.Destroy();
    return true;
}

/*******************************************************************************
 * Function Name  : InitView
 * Inputs		  : uWidth, uHeight
 * Returns        : true if no error occured
 * Description    : Code in InitView() will be called by the Shell upon a change
 *					in the rendering context.
 *					Used to initialize variables that are dependant on the rendering
 *					context (e.g. textures, vertex buffers, etc.)
 *******************************************************************************/
bool OGLESSkybox::InitView()
{
	bool	bErr;
	SPVRTContext TempContext;
	int		i;

	/* Gets the Data Path */
	const char	*dataPath;
	char		*filename = new char[2048];
	dataPath = (char*)PVRShellGet(prefDataPath);
	if (!dataPath)
	{
		PVRShellOutputDebug("**ERROR** Failed to provide a buffer large enough for OGLESGetDataPath.\n");
		return false;
	}

	/******************************
	** Create Textures           **
	*******************************/
	for (i=0; i<6; i++)
	{
		sprintf(filename, "%sskybox%d.pvr", dataPath, (i+1));
		if(!PVRTLoadTextureFromPVR(filename, &skyboxTex[i]))
		{
			PVRShellOutputDebug("**ERROR** Failed to load texture for skybox.\n");
		}
		myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}

	sprintf(filename, "%sballoon.pvr", dataPath);
	if(!PVRTLoadTextureFromPVR(filename, &balloonTex))
	{
		PVRShellOutputDebug("**ERROR** Failed to load texture for Background.\n");
	}
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	delete [] filename;

	/* Init Print3D to display text on screen */
	bErr = gsMyPrint3D.SetTextures(&TempContext,PVRShellGet(prefWidth),PVRShellGet(prefHeight));
	if (bErr == false)
	{
		PVRShellOutputDebug ("ERROR: Cannot initialise Print3D\n");
		return false;
	}

	/*********************/
	/* Create the skybox */
	/*********************/
	PVRTCreateSkybox( SKYBOX_ZOOM, SKYBOX_ADJUSTUVS, 512, &g_skyboxVertices, &g_skyboxUVs );

	/**********************
	** Projection Matrix **
	**********************/

	/* Projection */
	PVRTMatrixPerspectiveFovRH(g_mProj, f2vt(PI / 6), f2vt(CAM_ASPECT), f2vt(CAM_NEAR), f2vt(CAM_FAR));
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if(PVRShellGet(prefIsRotated) && PVRShellGet(prefFullScreen))
		myglRotate(f2vt(90),f2vt(0),f2vt(0),f2vt(1));

	myglMultMatrix(g_mProj.f);

	/******************************
	** GENERIC RENDER STATES     **
	******************************/

	/* The Type Of Depth Test To Do */
	glDepthFunc(GL_LEQUAL);

	/* Enables Depth Testing */
	glEnable(GL_DEPTH_TEST);

	/* Enables Smooth Color Shading */
	glShadeModel(GL_SMOOTH);

	/* Enable texturing */
	glEnable(GL_TEXTURE_2D);

	/* Define front faces */
	glFrontFace(GL_CW);

	/* Enables texture clamping */
	myglTexParameter( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
	myglTexParameter( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );

	/* Sets the clear color */
	myglClearColor(f2vt(0.5f), f2vt(0.5f), f2vt(0.5f), 0);

	/* Reset the model view matrix to position the light */
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	/* Setup ambiant light */
    glEnable(GL_LIGHTING);
	VERTTYPE lightGlobalAmbient[] = {f2vt(0.4f), f2vt(0.4f), f2vt(0.4f), f2vt(1.0f)};
    myglLightModelv(GL_LIGHT_MODEL_AMBIENT, lightGlobalAmbient);

	/* Setup a directional light source */
	VERTTYPE lightPosition[] = {f2vt(+0.7f), f2vt(+1.0f), f2vt(-0.2f), f2vt(0.0f)};
    VERTTYPE lightAmbient[]  = {f2vt(0.6f), f2vt(0.6f), f2vt(0.6f), f2vt(1.0f)};
    VERTTYPE lightDiffuse[]  = {f2vt(1.0f), f2vt(1.0f), f2vt(1.0f), f2vt(1.0f)};
    VERTTYPE lightSpecular[] = {f2vt(1.0f), f2vt(1.0f), f2vt(1.0f), f2vt(1.0f)};

    glEnable(GL_LIGHT0);
    myglLightv(GL_LIGHT0, GL_POSITION, lightPosition);
    myglLightv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
    myglLightv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
    myglLightv(GL_LIGHT0, GL_SPECULAR, lightSpecular);

	/* Setup the balloon material */
	VERTTYPE objectMatAmb[] = {f2vt(0.7f), f2vt(0.7f), f2vt(0.7f), f2vt(1.0f)};
	VERTTYPE objectMatDiff[] = {f2vt(1.0f), f2vt(1.0f), f2vt(1.0f), f2vt(1.0f)};
	VERTTYPE objectMatSpec[] = {f2vt(0.0f), f2vt(0.0f), f2vt(0.0f), f2vt(0.0f)};
	myglMaterialv(GL_FRONT_AND_BACK, GL_AMBIENT, objectMatAmb);
	myglMaterialv(GL_FRONT_AND_BACK, GL_DIFFUSE, objectMatDiff);
	myglMaterialv(GL_FRONT_AND_BACK, GL_SPECULAR, objectMatSpec);

	return true;
}

/*******************************************************************************
 * Function Name  : ReleaseView
 * Returns        : Nothing
 * Description    : Code in ReleaseView() will be called by the Shell before
 *					changing to a new rendering context.
 *******************************************************************************/
bool OGLESSkybox::ReleaseView()
{
	int i;

	/* Release all Textures */
	PVRTReleaseTexture(balloonTex);
	for (i = 0; i < 6; i++)
	{
		PVRTReleaseTexture(skyboxTex[i]);
	}

	/* Destroy the skybox */
	PVRTDestroySkybox( g_skyboxVertices, g_skyboxUVs );

	/* Release the Print3D textures and windows */
	gsMyPrint3D.DeleteAllWindows();
	gsMyPrint3D.ReleaseTextures();

	return true;
}


/*******************************************************************************
 * Function Name  : RenderScene
 * Returns		  : true if no error occured
 * Description    : Main rendering loop function of the program. The shell will
 *					call this function every frame.
 *******************************************************************************/
bool OGLESSkybox::RenderScene()
{
	/* Clear the depth and frame buffer */
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/* Set Z compare properties */
	glEnable(GL_DEPTH_TEST);

	/* Disable Blending*/
	glDisable(GL_BLEND);

	/* Calculate the model view matrix turning around the balloon */
	ComputeViewMatrix();

	/* Draw the skybox */
	DrawSkybox();

	/* Draw the balloon */
	DrawBalloon();

	/* Print text on screen */
	gsMyPrint3D.DisplayDefaultTitle("Skybox", "Skybox with PVRTC", PVR_LOGO);

	/* Flush all Print3D commands */
	gsMyPrint3D.Flush();

	/* Check for Problems */
	if(glGetError())
	{
		PVRShellOutputDebug("**GL_ERROR** Detected prior to Print3D call.\n");
	}

	return true;
}

/*******************************************************************************
 * Function Name  : ComputeViewMatrix
 * Description    : Calculate the view matrix turning around the balloon
 *******************************************************************************/
void OGLESSkybox::ComputeViewMatrix()
{
	PVRTVECTOR3 vFrom;

	/* Calculate the distance to balloon */
	VERTTYPE distance = fViewDistance + VERTTYPEMUL(fViewAmplitude, PVRTSIN(fViewAmplitudeAngle));
	distance = VERTTYPEDIV(distance, f2vt(5.0f));
	fViewAmplitudeAngle += f2vt(.004f);

	/* Calculate the vertical position of the camera */
	VERTTYPE updown = VERTTYPEMUL(fViewUpDownAmplitude, PVRTSIN(fViewUpDownAngle));
	updown = VERTTYPEDIV(updown, f2vt(5.0f));
	fViewUpDownAngle += f2vt(0.005f);

	/* Calculate the angle of the camera around the balloon */
	vFrom.x = VERTTYPEMUL(distance, PVRTCOS(fViewAngle));
	vFrom.y = updown;
	vFrom.z = VERTTYPEMUL(distance, PVRTSIN(fViewAngle));
	fViewAngle += f2vt(0.003f);

	/* Compute and set the matrix */
	PVRTMatrixLookAtRH(g_mView, vFrom, vTo, vUp);
	glMatrixMode(GL_MODELVIEW);
	myglLoadMatrix(g_mView.f);

	/* Remember the camera position to draw the skybox around it */
	vCameraPosition = vFrom;
}

/*******************************************************************************
 * Function Name  : DrawSkybox
 * Description    : Draws the skybox
 *******************************************************************************/
void OGLESSkybox::DrawSkybox()
{
	/* Only use the texture color */
	myglTexEnv(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

	/* Draw the skybox around the camera position */
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	myglTranslate(-vCameraPosition.x, -vCameraPosition.y, -vCameraPosition.z);

	/* Disable lighting */
	glDisable(GL_LIGHTING);

	/* Enable backface culling for skybox; need to ensure skybox faces are set up properly */
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	/* Enable States */
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	for (int i = 0; i<6; i++)
	{
		/* Set Data Pointers */
		glBindTexture(GL_TEXTURE_2D, skyboxTex[i]);
		glVertexPointer(3, VERTTYPEENUM, sizeof(VERTTYPE)*3, &g_skyboxVertices[i*4*3]);
		glTexCoordPointer(2, VERTTYPEENUM, sizeof(VERTTYPE)*2, &g_skyboxUVs[i*4*2]);
		/* Draw */
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	}

	/* Disable States */
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);

	glPopMatrix();
}

/*******************************************************************************
 * Function Name  : DrawBalloon
 * Description    : Draws the balloon
 *******************************************************************************/
void OGLESSkybox::DrawBalloon()
{
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	PVRTMATRIX worldMatrix;
	g_sScene.GetWorldMatrix(worldMatrix, g_sScene.pNode[0]);
	myglMultMatrix(worldMatrix.f);

	/* Modulate with vertex color */
	myglTexEnv(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	/* Enable lighting */
	glEnable(GL_LIGHTING);

	/* Bind the Texture */
	glBindTexture(GL_TEXTURE_2D, balloonTex);

	/* Enable back face culling */
	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);

	/* Enable States */
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	/* Set Data Pointers */
	SPODMesh* mesh = g_sScene.pMesh;

	// Used to display interleaved geometry
	glVertexPointer(3, VERTTYPEENUM, mesh->sVertex.nStride, mesh->pInterleaved + (long)mesh->sVertex.pData);
	glNormalPointer(VERTTYPEENUM, mesh->sNormals.nStride, mesh->pInterleaved + (long)mesh->sNormals.pData);
	glTexCoordPointer(2, VERTTYPEENUM, mesh->psUVW[0].nStride, mesh->pInterleaved + (long)mesh->psUVW[0].pData);

	/* Draw */
	glDrawElements(GL_TRIANGLES, mesh->nNumFaces*3, GL_UNSIGNED_SHORT, mesh->sFaces.pData);

	/* Disable States */
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);

	glPopMatrix();
}


/*******************************************************************************
 * Function Name  : CameraGetMatrix
 * Global Used    :
 * Description    : Function to setup camera position
 *
 *******************************************************************************/
void OGLESSkybox::CameraGetMatrix()
{
	PVRTVECTOR3	vFrom, vTo, vUp;
	VERTTYPE	fFOV;

	vUp.x = f2vt(0.0f);
	vUp.y = f2vt(1.0f);
	vUp.z = f2vt(0.0f);

	if(g_sScene.nNumCamera)
	{
		/* Get Camera data from POD Geometry File */
		fFOV = g_sScene.GetCameraPos(vFrom, vTo, 0);
		fFOV = VERTTYPEMUL(fFOV, f2vt(0.75f));		// Convert from horizontal FOV to vertical FOV (0.75 assumes a 4:3 aspect ratio)
	}
	else
	{
		fFOV = f2vt(PI / 6);
	}

	/* View */
	PVRTMatrixLookAtRH(g_mView, vFrom, vTo, vUp);

	/* Projection */
	PVRTMatrixPerspectiveFovRH(g_mProj, fFOV, f2vt(CAM_ASPECT), f2vt(CAM_NEAR), f2vt(CAM_FAR));
}

/*******************************************************************************
 * Function Name  : NewDemo
 * Description    : Called by the Shell to initialize a new instance to the
 *					demo class.
 *******************************************************************************/
PVRShell* NewDemo()
{
	return new OGLESSkybox();
}

/*****************************************************************************
 End of file (OGLESSkybox.cpp)
*****************************************************************************/
