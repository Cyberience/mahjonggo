/******************************************************************************

 @File         OGLESMouse.cpp

 @Title        OGLESMouse

 @Copyright    Copyright (C) 2006 - 2006 by Imagination Technologies Limited.

 @Platform     Independant

 @Description  Demonstrates cell-shading (cartoon style) using VertexProgram
               extension.

******************************************************************************/
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "PVRShell.h"
#include "OGLESTools.h"

// Geometry and Animation Data
#include "OGLESMouse_trilist.H"

// Precompiled Vertex Programs
#include "CSH_VGPARMVP.h"
#include "CSH_VGP.h"
#include "CSH_VGPLITE.h"

// Textures
#include "Toon.h"			// Special Texture to generate Toon Shading
#include "MouseToon.h"		// Mouse Base Texture
#include "WallToon.h"		// Wall Texture
#include "FloorToon.h"		// Floor Texture

/******************************************************************************
 Defines
******************************************************************************/

/*
	Enable the following line to force software processing of the UVs instead of VGP Hardware.
	Can also be used as a build option.
*/
// #define FORCE_SOFTWARE_PROC	1

#ifdef FORCE_SOFTWARE_PROC
	#define DO_NOT_USE_VERTEXPROG 1
#else
	#define DO_NOT_USE_VERTEXPROG 0
#endif

/*!****************************************************************************
 Class implementing the PVRShell functions.
******************************************************************************/
class OGLESMouse : public PVRShell
{
	// Texture IDs
	GLuint  guiTexturesMap[256];

	// Animation Related
	GLfloat gfAnimationFrame, gfAnimationIncr;

	// VGP Program ID
	GLuint guiVGPProg;

	// Camera
	PVRTVECTOR3	gsCameraPosition;
	PVRTVECTOR3	gsCameraPointing;
	PVRTVECTOR3	gsCameraUpVector;

	// Data Conversion
	HeaderStruct_Mesh_Type *gpsDemoMesh[NUM_MESHES];

	// Print3D class used to display text
	CPVRTPrint3D gMyPrint3D;

	// Extension Class Object
	CPVRTglesExt gPVRTglesExt;

	bool		m_bUseVertexProgram;
	PVRTVECTOR3	InvCamera;
	PVRTVECTOR3	InvLight;
	VERTTYPE	TempUVBuffer[2*head_NumVertex];	// Used for software processing - Head is most complex object

public:
	OGLESMouse()
	{
		// Initialisation to start values
		gfAnimationFrame = 1.0f;
		gfAnimationIncr  = 1.0f;

		gsCameraUpVector.x=f2vt(0.0f);		gsCameraUpVector.y=f2vt(1.0f);		gsCameraUpVector.z=f2vt(0.0f);
		gsCameraPosition.x=f2vt(-1000.0f);	gsCameraPosition.y=f2vt(800.0f);	gsCameraPosition.z=f2vt(300.0f);

		m_bUseVertexProgram = !DO_NOT_USE_VERTEXPROG;
	}

	virtual bool InitApplication();
	virtual bool InitView();
	virtual bool ReleaseView();
	virtual bool QuitApplication();
	virtual bool RenderScene();

	void RenderPrimitive(HeaderStruct_Mesh_Type *object);
	void SetAnimMatrix	(int nObject, unsigned FrameCount);
	bool InitVertexProgram(const void *pCSH, size_t nSzCSH);
};

/*!****************************************************************************
 @Function		InitApplication
 @Return		bool		true if no error occured
 @Description	Code in InitApplication() will be called by PVRShell once per
				run, before the rendering context is created.
				Used to initialize variables that are not dependant on it
				(e.g. external modules, loading meshes, etc.)
				If the rendering context is lost, InitApplication() will
				not be called again.
******************************************************************************/
bool OGLESMouse::InitApplication()
{
	/*
		Load Model and convert to fixed point if necessary.
		This load only happens once and it must not be repeated hence in InitApplication.
	*/

	for(int i=0; i<NUM_MESHES; i++)
	{
		gpsDemoMesh[i] = PVRTLoadHeaderObject(&Mesh[i]);
	}

	return true;
}

/*!****************************************************************************
 @Function		QuitApplication
 @Return		bool		true if no error occured
 @Description	Code in QuitApplication() will be called by PVRShell once per
				run, just before exiting the program.
				If the rendering context is lost, QuitApplication() will
				not be called.
******************************************************************************/
bool OGLESMouse::QuitApplication()
{
	// Release Model Data
	for(int i=0; i<NUM_MESHES; i++)
	{
		PVRTUnloadHeaderObject(gpsDemoMesh[i]);
	}

	return true;
}

/*!****************************************************************************
 @Function		InitView
 @Return		bool		true if no error occured
 @Description	Code in InitView() will be called by PVRShell upon
				initialization or after a change in the rendering context.
				Used to initialize variables that are dependant on the rendering
				context (e.g. textures, vertex buffers, etc.)
******************************************************************************/
bool OGLESMouse::InitView()
{
	PVRTMATRIX		PerspectiveMatrix;
	SPVRTContext	sTempContext;
	float			fWidth = (float)PVRShellGet(prefWidth);
	float			fHeight = (float)PVRShellGet(prefHeight);
	bool			bErr;
	VERTTYPE		fValue[4];

	// Init Print3D to display text on screen
	bErr = gMyPrint3D.SetTextures(&sTempContext,PVRShellGet(prefWidth),PVRShellGet(prefHeight));
	if (bErr == false)
	{
		PVRShellSet(prefExitMessage, "ERROR: Cannot initialize Print3D\n");
		return false;
	}

	/*
		Load the textures.
	*/

	// Special Toon Texture must be Point Sampled (Nearest)
	if (!PVRTLoadTextureFromPointer((void*)Toon, &guiTexturesMap[100]))
	{
		PVRShellSet(prefExitMessage, "ERROR: Cannot load Toon texture\n");
		return false;
	}
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	// Special Toon Texture Option to Clamp to Edge, no wrap around of tex coords.
	myglTexParameter(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
	myglTexParameter(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE);

	if (!PVRTLoadTextureFromPointer((void*)MouseToon, &guiTexturesMap[M_HEAD]))
	{
		PVRShellSet(prefExitMessage, "ERROR: Cannot load MouseToon texture\n");
		return false;
	}
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if (!PVRTLoadTextureFromPointer((void*)FloorToon, &guiTexturesMap[M_FLOOR]))
	{
		PVRShellSet(prefExitMessage, "ERROR: Cannot load FloorToon texture\n");
		return false;
	}
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if (!PVRTLoadTextureFromPointer((void*)WallToon, &guiTexturesMap[M_WALL]))
	{
		PVRShellSet(prefExitMessage, "ERROR: Cannot load WallToon texture\n");
		return false;
	}
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// All the other meshes (parts of the mouse) use the same texture as the head
	guiTexturesMap[M_HAND_LEFT] = guiTexturesMap[M_HAND_RIGHT] = guiTexturesMap[M_BODY] = guiTexturesMap[M_OBJECT02] = guiTexturesMap[M_OBJECT01] = guiTexturesMap[M_HEAD];

	// Setup back face culling
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);

	// Use depth testing and no blending
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);

	// Disables lighting. Lighting is done through the use of lightmaps
	glDisable(GL_LIGHTING);

	myglClearColor(f2vt(1), f2vt(1), f2vt(0), f2vt(1));

	// Check for the vertex program extension
	if(!CPVRTglesExt::IsGLExtensionSupported("GL_IMG_vertex_program"))
	{
		PVRShellOutputDebug("**Warning** Running this demo without GL_IMG_vertex_program\n");
		m_bUseVertexProgram = false;
	}

	/*
		Create the perspective matrix.
	*/
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if(PVRShellGet(prefIsRotated) && PVRShellGet(prefFullScreen))
	{
#if defined(UNDER_CE) && !defined(WIN32_PLATFORM_WFSP)
		if(m_bUseVertexProgram)
		{
			PVRTMATRIX m = {
				0, 1, 0, 0,
				1, 0, 0, 0,
				0, 0, 1, 0,
				0, 0, 0, 1
			};
			myglMultMatrix(m.f);
		}
#endif
		myglRotate(f2vt(90), f2vt(0), f2vt(0), f2vt(1));
		fWidth = (float)PVRShellGet(prefHeight);
		fHeight = (float)PVRShellGet(prefWidth);
	}
	PVRTMatrixPerspectiveFovRH(PerspectiveMatrix, f2vt(20.0f*(3.14f/180.0f)), f2vt(fWidth/fHeight), f2vt(800.0f), f2vt(2800.0f));
	myglMultMatrix(PerspectiveMatrix.f);

	/*
		Load the vertex programs.
	*/
	bErr = 1; // No error yet
	if(m_bUseVertexProgram)
	{
		// Extension is available. Init function calls
		gPVRTglesExt.Init();

		const char *pszRendererString;

		// Retrieve pointer to renderer string
		pszRendererString = (const char *) glGetString(GL_RENDERER);

		// PowerVR hardware present ?
		if (strstr(pszRendererString, "PowerVR"))
		{
			// VGP present ?
			if (strstr(pszRendererString, "VGPLite"))
			{
				// VGPLite present
				bErr = InitVertexProgram(vgp_CSH_VGPLITE,sizeof(vgp_CSH_VGPLITE));
			}
			else if (strstr(pszRendererString, "VGP"))
			{
				// VGP present
				bErr = InitVertexProgram(vgp_CSH_VGP,sizeof(vgp_CSH_VGP));
				if (!bErr)
				{
					// Try loading ARM VP Vertex Program instead
					bErr = InitVertexProgram(vgp_CSH_VGPARMVP,sizeof(vgp_CSH_VGPARMVP));
				}
			}
		}

		// Init Vertex Program
		if(!bErr)
		{
			PVRShellSet(prefExitMessage, "ERROR: Can't load vertex program\n");
			return false;
		}


		/*
			Set constants for the Vertex Program.
		*/

		// Setup Camera Position Constant
		fValue[0]=f2vt(0.0f);
		fValue[1]=f2vt(0.0f);
		fValue[2]=f2vt(0.0f);
		fValue[3]=f2vt(1.0f);
		gPVRTglesExt.myglProgramLocalParameter4v(GL_VERTEX_PROGRAM_ARB,0,fValue);  // Camera Pos

		// Setup Light Position Constant
		fValue[0]=f2vt(10.0f);
		fValue[1]=f2vt(0.0f);
		fValue[2]=f2vt(10.0f);
		fValue[3]=f2vt(1.0f);
		gPVRTglesExt.myglProgramLocalParameter4v(GL_VERTEX_PROGRAM_ARB,1,fValue);  // Light Pos
	}

	// Base Viewport Setup
	glViewport(0,0,PVRShellGet(prefWidth),PVRShellGet(prefHeight));

	return true;
}

/*!****************************************************************************
 @Function		ReleaseView
 @Return		bool		true if no error occured
 @Description	Code in ReleaseView() will be called by PVRShell when the
				application quits or before a change in the rendering context.
******************************************************************************/
bool OGLESMouse::ReleaseView()
{
	// Release Print3D Textures
	gMyPrint3D.ReleaseTextures();

	// Frees the texture
	for(int i=0; i<4; i++)
	{
		PVRTReleaseTexture(guiTexturesMap[i]);
	}

	// Release the vertex program if used
	if(m_bUseVertexProgram)
	{
		gPVRTglesExt.glDeleteProgramsARB(1, &guiVGPProg);
	}
	return true;
}

/*!****************************************************************************
 @Function		RenderScene
 @Return		bool		true if no error occured
 @Description	Main rendering loop function of the program. The shell will
				call this function every frame.
				eglSwapBuffers() will be performed by PVRShell automatically.
				PVRShell will also manage important OS events.
				Will also manage relevent OS events. The user has access to
				these events through an abstraction layer provided by PVRShell.
******************************************************************************/
bool OGLESMouse::RenderScene()
{
	// Clears the color buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Enable texturing
	glActiveTexture(GL_TEXTURE0);
	glEnable(GL_TEXTURE_2D);

	// Set camera target (following the mouse: Data from the 3DS file)
	gsCameraPointing.x = f2vt(*(CameraAnimation[0].Target+(int)gfAnimationFrame*3+0));
	gsCameraPointing.y = f2vt(200.0f);
	gsCameraPointing.z = f2vt(*(CameraAnimation[0].Target+(int)gfAnimationFrame*3+2));

	/*
		Draw the scene.
	*/

	// Use the vertex program if it's available
	if(m_bUseVertexProgram)
	{
		glEnable(GL_VERTEX_PROGRAM_ARB);
		gPVRTglesExt.glBindProgramARB(GL_VERTEX_PROGRAM_ARB, guiVGPProg);
	}

	// For All Meshes Do...
	for(int i=0; i<NUM_MESHES; i++)
	{
		// Bind correct Texture
		glBindTexture(GL_TEXTURE_2D,guiTexturesMap[i]);

		/*
			Second layer multitexture for the cartoon effect.
			This layer will draw the black halo around the mesh and generate the banded lighting
			using modulate blending.
			Dynamic UV mapping is calculated in the Vertex Program
		*/
		if (guiTexturesMap[i] == guiTexturesMap[M_HEAD]) // Only the mouse is multitextured
		{
			// Enable the second texture layer
			glActiveTexture(GL_TEXTURE1);
			glEnable(GL_TEXTURE_2D);

			// Sets the Correct Texture. Texture 100 is the special Toon Shading Texture
			glBindTexture(GL_TEXTURE_2D,guiTexturesMap[100]);

			// Back to the default (0) texture layer
			glActiveTexture(GL_TEXTURE0);

			// Use Texture Combine Mode
			myglTexEnv(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		}

        // Update the Animation Matrix
		SetAnimMatrix(i, (int)gfAnimationFrame);

		// Render the geometry
		RenderPrimitive(gpsDemoMesh[i]);

		/*
			If we were drawing the multitextured mouse,
			disable the second texture layer.
		*/
		if (guiTexturesMap[i] == guiTexturesMap[M_HEAD])
		{
			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D,0);
			glDisable(GL_TEXTURE_2D);
			glActiveTexture(GL_TEXTURE0);
		}
	}

	// Disable Vertex Program since its not used to display text
	if(m_bUseVertexProgram)
	{
		glDisable(GL_VERTEX_PROGRAM_ARB);
	}


	// Update mouse animation
	if(gfAnimationFrame>=NUM_FRAMES || gfAnimationFrame<=0)
	{
		gfAnimationIncr=-gfAnimationIncr;
	}
	gfAnimationFrame += gfAnimationIncr;

	/*
		Draw Text
	*/
	if (!m_bUseVertexProgram)
	{
		gMyPrint3D.Print3D(0.0f, 1.0f, 1.205f,  0xFF000000, "Mouse");
		gMyPrint3D.Print3D(0.0f, 8.0f, 0.905f,  0xFF000000, "Toon Shading");
		gMyPrint3D.DisplayDefaultTitle("Mouse", "Toon Shading", PVR_LOGO);
	}
	else
	{
		gMyPrint3D.Print3D(0.0f, 1.0f, 1.205f,  0xFF000000, "Mouse");
		gMyPrint3D.Print3D(0.0f, 8.0f, 0.905f,  0xFF000000, "VGP Toon Shading");
		gMyPrint3D.DisplayDefaultTitle("Mouse", "VGP Toon Shading", PVR_LOGO);
	}
	gMyPrint3D.Flush();

	return true;
}

/*!****************************************************************************
 @Function		InitVertexProgram
 @Input			pCSH		Vertex program data
 @Input			nSzCSH		Vertex program size
 @Return		bool		true is everything was ok, false if something went wrong
 @Description	Load a VGP program.
******************************************************************************/
bool OGLESMouse::InitVertexProgram(const void *pCSH, size_t nSzCSH)
{
	/* Load VGP code */
	gPVRTglesExt.glGenProgramsARB(1, &guiVGPProg);
	gPVRTglesExt.glBindProgramARB(GL_VERTEX_PROGRAM_ARB, guiVGPProg);
	gPVRTglesExt.glProgramStringARB(GL_VERTEX_PROGRAM_ARB, GL_PROGRAM_FORMAT_BINARY_IMG, nSzCSH, pCSH);

	/* Check for problems */
	if (glGetError()!=GL_NO_ERROR)
	{
		return false;
	}

	return true;
}

/*!****************************************************************************
 @Function		RenderPrimitive
 @Input			object		Mesh to render
 @Description	Draws a mesh.
******************************************************************************/
void OGLESMouse::RenderPrimitive(HeaderStruct_Mesh_Type *object)
{
	VERTTYPE *pVerticies = object->pVertex;
	VERTTYPE *pUV = object->pUV;
	VERTTYPE *pNormals = object->pNormals;
	unsigned short *pIndices = object->pFaces;

	// Set Data Pointers and enable states
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3,VERTTYPEENUM,0,pVerticies);

	glEnableClientState(GL_NORMAL_ARRAY);
	glNormalPointer(VERTTYPEENUM,(sizeof(float))*3,pNormals);

	glClientActiveTexture(GL_TEXTURE0);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2,VERTTYPEENUM,0,pUV);

	/*
		If texture programs are not available, does software processing of
		the second set of UVs to generate the Toon Tex Coords.
	*/
	if(!m_bUseVertexProgram)
	{
		for (unsigned int i=0; i<object->nNumVertex;i++)
		{
            TempUVBuffer[i*2+0]= VERTTYPEMUL(object->pNormals[i*3+0],InvCamera.x) +
								 VERTTYPEMUL(object->pNormals[i*3+1],InvCamera.y) +
								 VERTTYPEMUL(object->pNormals[i*3+2],InvCamera.z);
			TempUVBuffer[i*2+1]= VERTTYPEMUL(object->pNormals[i*3+0],InvLight.x) +
								 VERTTYPEMUL(object->pNormals[i*3+1],InvLight.y) +
								 VERTTYPEMUL(object->pNormals[i*3+2],InvLight.z);
		}

		glClientActiveTexture(GL_TEXTURE1);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glTexCoordPointer(2,VERTTYPEENUM,0,TempUVBuffer);
	}

	// Draw
	glDrawElements(GL_TRIANGLES, (object->nNumFaces)*3, GL_UNSIGNED_SHORT, pIndices);

	// Disable Active States
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);

	// Disable second layer texture coords if vertex programs were used
	if(!m_bUseVertexProgram)
	{
        glClientActiveTexture(GL_TEXTURE1);
        glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	}

	glClientActiveTexture(GL_TEXTURE0);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}

/*!****************************************************************************
 @Function		SetAnimMatrix
 @Input			object		Object to set the matrix for
 @Input			uFrameCount	Frame number to set the matrix for
 @Description	To set a animation from 3DStudio MAX, feed the transformation matrix
				with the fValues exported by the PVRexp plug-in. And setup VGP Constants.
******************************************************************************/
void OGLESMouse::SetAnimMatrix (int nObject, unsigned uFrameCount)
{
#ifdef STRUCT_ANIMATION_DEFINED

	/*
		If the scene file contains an animation, take it into account.
	*/
	PVRTMATRIX  Matrix, ViewMatrix, InvWorld, TempMatrix;
	PVRTVECTOR3 CameraVector,LightVector;
	PVRTVECTOR4 Result;
	VERTTYPE	fValue[4];

	// Sets the camera as the view matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	PVRTMatrixLookAtRH(ViewMatrix, gsCameraPosition, gsCameraPointing, gsCameraUpVector);
	myglMultMatrix(ViewMatrix.f);

	// If the frame is out of range return the identity matrix
	if (uFrameCount > Animation[nObject].nNumFrames)
	{
		return;
	}

	/*
		Every chunk has 12 floats so, for example, the data for frame 32 of the object 2 starts at
		(Animation[2].pData + 32*12 + 0) and carrys on for 12 floats.
		Note: M_14 = 0, M_24 = 0, M_34 = 0 and M_44 =1 are fixed fValues.
	*/
	uFrameCount = uFrameCount*12;

    Matrix.f[ 0] = f2vt(*(Animation[nObject].pData + uFrameCount + 0));
	Matrix.f[ 1] = f2vt(*(Animation[nObject].pData + uFrameCount + 1));
	Matrix.f[ 2] = f2vt(*(Animation[nObject].pData + uFrameCount + 2));
	Matrix.f[ 3] = f2vt(0.0f);

	Matrix.f[ 4] = f2vt(*(Animation[nObject].pData + uFrameCount + 3));
	Matrix.f[ 5] = f2vt(*(Animation[nObject].pData + uFrameCount + 4));
	Matrix.f[ 6] = f2vt(*(Animation[nObject].pData + uFrameCount + 5));
	Matrix.f[ 7] = f2vt(0.0f);

	Matrix.f[ 8] = f2vt(*(Animation[nObject].pData + uFrameCount + 6));
	Matrix.f[ 9] = f2vt(*(Animation[nObject].pData + uFrameCount + 7));
	Matrix.f[10] = f2vt(*(Animation[nObject].pData + uFrameCount + 8));
	Matrix.f[11] = f2vt(0.0f);

	Matrix.f[12] = f2vt(*(Animation[nObject].pData + uFrameCount + 9));
	Matrix.f[13] = f2vt(*(Animation[nObject].pData + uFrameCount + 10));
	Matrix.f[14] = f2vt(*(Animation[nObject].pData + uFrameCount + 11));
	Matrix.f[15] = f2vt(1.0f);

	myglMultMatrix(Matrix.f);

#else

	// If the scene does not have an animation, just use the camera matrix.
	glLoadIdentity();
	PVRTMatrixLookAtRH(ViewMatrix, gsCameraPosition, gsCameraPointing, gsCameraUpVector);
	myglMultMatrix(ViewMatrix.f);

#endif

	/*
		Setup the camera position in the VGP code.
	*/
	CameraVector.x = f2vt(0.0f);
	CameraVector.y = f2vt(0.0f);
	CameraVector.z = f2vt(10000.0f);
	PVRTMatrixMultiply(TempMatrix, Matrix, ViewMatrix);
	PVRTMatrixInverseEx(InvWorld,  TempMatrix);
	PVRTTransVec3TransformArray(&Result,sizeof(PVRTVECTOR4),&CameraVector,sizeof(PVRTVECTOR3),&InvWorld,1);
	PVRTMatrixVec3Normalize((PVRTVECTOR3&)Result, (PVRTVECTOR3&)Result);
	fValue[0]=Result.x;
	fValue[1]=Result.y;
	fValue[2]=Result.z;
	fValue[3]=Result.w;
	if(m_bUseVertexProgram)
	{
		gPVRTglesExt.myglProgramLocalParameter4v(GL_VERTEX_PROGRAM_ARB,0,fValue);  // Camera Position
	}
	else
	{
		InvCamera.x=fValue[0];
		InvCamera.y=fValue[1];
		InvCamera.z=fValue[2];
	}

	/*
		Setup the light position in the VGP code.
	*/
	LightVector.x = f2vt(1000.0f);
	LightVector.y = f2vt(0.0f);
	LightVector.z = f2vt(0.0f);
	PVRTTransVec3TransformArray(&Result,sizeof(PVRTVECTOR3),&LightVector,sizeof(PVRTVECTOR3),&InvWorld,1);
	PVRTMatrixVec3Normalize((PVRTVECTOR3&)Result, (PVRTVECTOR3&)Result);
	fValue[0]=Result.x;
	fValue[1]=Result.y;
	fValue[2]=Result.z;
	fValue[3]=Result.w;
	if(m_bUseVertexProgram)
	{
        gPVRTglesExt.myglProgramLocalParameter4v(GL_VERTEX_PROGRAM_ARB,1,fValue);  // Light Position
	}
	else
	{
		InvLight.x=fValue[0];
		InvLight.y=fValue[1];
		InvLight.z=fValue[2];
	}
}

/*!****************************************************************************
 @Function		NewDemo
 @Return		PVRShell*		The demo supplied by the user
 @Description	This function must be implemented by the user of the shell.
				The user should return its PVRShell object defining the
				behaviour of the application.
******************************************************************************/
PVRShell* NewDemo(){
	return new OGLESMouse();
}

/******************************************************************************
 End of file (OGLESMouse.cpp)
******************************************************************************/
