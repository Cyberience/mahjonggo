/******************************************************************************

 @File         OGLESLighting.cpp

 @Title        Lighting

 @Copyright    Copyright (C) 2004 - 2006 by Imagination Technologies Limited.

 @Platform     Independant

 @Description  Shows how to use multiple lights in OpenGL ES. Requires the
               PVRShell.

******************************************************************************/
#include <math.h>
#include <stdlib.h>
#include "PVRShell.h"
#include "OGLESTools.h"

/* Textures */
#include "LightTex.h"
#include "GRANITE.h"

/* Geometry */
#include "SimpleGeoTest.H"

/****************************************************************************
** Defines
****************************************************************************/
#define NUM_LIGHTS_IN_USE	(8)

/****************************************************************************
** Structures
****************************************************************************/
struct SLightVars
{
	PVRTVECTOR4		position;	// GL_LIGHT_POSITION
	PVRTVECTOR4		direction;	// GL_SPOT_DIRECTION
	PVRTVECTOR4		ambient;	// GL_AMBIENT
	PVRTVECTOR4		diffuse;	// GL_DIFFUSE
	PVRTVECTOR4		specular;	// GL_SPECULAR

	PVRTVECTOR3		vRotationStep;
	PVRTVECTOR3		vRotation;
	PVRTVECTOR3		vRotationCentre;
	PVRTVECTOR3		vPosition;
};


/****************************************************************************
** Class: PVRDemo
****************************************************************************/
class PVRDemo : public PVRShell
{
private:
	/* Print3D class */
	CPVRTPrint3D 	m_AppPrint3D;

	/* Mesh data */
	HeaderStruct_Mesh_Type* Meshes[NUM_MESHES];

	/* Texture names */
	GLuint texNameObject;
	GLuint texNameLight;

	/* Light properties */
	SLightVars m_psLightData[8];

	/* Number of frames */
	long m_nNumFrames;

public:
	PVRDemo()
	{
		m_nNumFrames = 0;
	}

	/* PVRShell functions */
	virtual bool InitApplication();
	virtual bool InitView();
	virtual bool ReleaseView();
	virtual bool QuitApplication();
	virtual bool RenderScene();

	/****************************************************************************
	** Function Definitions
	****************************************************************************/
	void RenderPrimitive(VERTTYPE *pVertex, VERTTYPE *pNormals, VERTTYPE *pUVs, int nNumIndex, unsigned short *pIDX, GLenum mode);
	void initLight(SLightVars *pLight);
	void stepLight(SLightVars *pLight);
	void renderLight(SLightVars *pLight);
};


/*******************************************************************************
 * Function Name  : InitApplication
 * Inputs		  : argc, *argv[], uWidth, uHeight
 * Returns        : true if no error occured
 * Description    : Code in InitApplication() will be called by the Shell ONCE per
 *					run, early on in the execution of the program.
 *					Used to initialize variables that are not dependant on the
 *					rendering context (e.g. external modules, loading meshes, etc.)
 *******************************************************************************/
bool PVRDemo::InitApplication()
{
	PVRShellSet(prefAppName, "OGLESLighting");

	/* Load all header objects */
	for(int i = 0; i < NUM_MESHES; i++)
	{
		Meshes[i] = PVRTLoadHeaderObject(&Mesh[i]);
	}
	return true;
}

/*******************************************************************************
 * Function Name  : QuitApplication
 * Returns        : true if no error occured
 * Description    : Code in QuitApplication() will be called by the Shell ONCE per
 *					run, just before exiting the program.
 *******************************************************************************/
bool PVRDemo::QuitApplication()
{
	for(int i = 0; i < NUM_MESHES; i++)
	{
		PVRTUnloadHeaderObject(Meshes[i]);
	}
	return true;
}

/*******************************************************************************
 * Function Name  : InitView
 * Inputs		  : uWidth, uHeight
 * Returns        : true if no error occured
 * Description    : Code in InitView() will be called by the Shell upon a change
 *					in the rendering context.
 *					Used to initialize variables that are dependant on the rendering
 *					context (e.g. textures, vertex buffers, etc.)
 *******************************************************************************/
bool PVRDemo::InitView()
{
	PVRTMATRIX	MyPerspMatrix;
	int			i;
	int			err;
	SPVRTContext Context;
	float		fWidth  = (float)PVRShellGet(prefWidth);
	float		fHeight = (float)PVRShellGet(prefHeight);

	/*
		Init Print3D
	*/
	err = m_AppPrint3D.SetTextures(&Context,PVRShellGet(prefWidth),PVRShellGet(prefHeight));
	if(err == false)
	{
		PVRShellOutputDebug ("ERROR: Cannot initialise Print3D\n");
		return false;
	}

	/* Load textures */
	if (!PVRTLoadTextureFromPointer((void*)GRANITE, &texNameObject))
		return false;
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if (!PVRTLoadTextureFromPointer((void*)LightTex, &texNameLight))
		return false;
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	/* Setup all materials */
	VERTTYPE objectMatAmb[]		= {f2vt(0.1f), f2vt(0.1f), f2vt(0.1f), f2vt(0.3f)};
	VERTTYPE objectMatDiff[]	= {f2vt(0.5f), f2vt(0.5f), f2vt(0.5f), f2vt(0.3f)};
	VERTTYPE objectMatSpec[]	= {f2vt(1.0f), f2vt(1.0f), f2vt(1.0f), f2vt(0.3f)};
	myglMaterialv(GL_FRONT_AND_BACK, GL_AMBIENT, objectMatAmb);
	myglMaterialv(GL_FRONT_AND_BACK, GL_DIFFUSE, objectMatDiff);
	myglMaterialv(GL_FRONT_AND_BACK, GL_SPECULAR, objectMatSpec);
	myglMaterial(GL_FRONT_AND_BACK, GL_SHININESS, f2vt(10));	// Nice and shiny so we don't get aliasing from the 1/2 angle

	/* Initialize all lights */
	srand(0);
	for(i = 0; i < 8; ++i)
	{
		initLight(&m_psLightData[i]);
	}

	/* Perspective matrix */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if(PVRShellGet(prefIsRotated) && PVRShellGet(prefFullScreen))
	{
		myglRotate(f2vt(90), f2vt(0), f2vt(0), f2vt(1));
		fWidth =  (float)PVRShellGet(prefHeight);
		fHeight = (float)PVRShellGet(prefWidth);
	}
	PVRTMatrixPerspectiveFovRH(MyPerspMatrix, f2vt(20.0f*(PVRTPIf/180.0f)), f2vt(fWidth/fHeight), f2vt(10.0f), f2vt(1200.0f));
	myglMultMatrix(MyPerspMatrix.f);

	/* Modelview matrix */
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	myglTranslate(f2vt(0.0f), f2vt(0.0f), f2vt(-500.0f));

	/* Setup culling */
	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CW);
	glCullFace(GL_FRONT);

	/* Enable texturing */
	glEnable(GL_TEXTURE_2D);

	return true;
}

/*******************************************************************************
 * Function Name  : ReleaseView
 * Returns        : Nothing
 * Description    : Code in ReleaseView() will be called by the Shell before
 *					changing to a new rendering context.
 *******************************************************************************/
bool PVRDemo::ReleaseView()
{
	/* Release textures */
	PVRTReleaseTexture(texNameObject);
	PVRTReleaseTexture(texNameLight);

	/* Release Print3D textures */
	m_AppPrint3D.ReleaseTextures();
	return true;
}

/*******************************************************************************
 * Function Name  : RenderScene
 * Returns		  : true if no error occured
 * Description    : Main rendering loop function of the program. The shell will
 *					call this function every frame.
 *******************************************************************************/
bool PVRDemo::RenderScene()
{
	unsigned int i;
	PVRTMATRIX		RotationMatrix;

	/* Set up viewport */
	glViewport(0, 0, PVRShellGet(prefWidth), PVRShellGet(prefHeight));

	/* Clear the buffers */
	glEnable(GL_DEPTH_TEST);
	myglClearColor(f2vt(0.0f), f2vt(0.0f), f2vt(0.0f), f2vt(0.0f));
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/* Lighting */

	/* Enable lighting (needs to be specified everyframe as Print3D will turn it off */
	glEnable(GL_LIGHTING);

	/* Increase number of frames */
	m_nNumFrames++;
	m_nNumFrames = m_nNumFrames % 3600;
	PVRTMatrixRotationY(RotationMatrix, f2vt((-m_nNumFrames*0.1f) * PVRTPIf/180.0f));

	/* Loop through all lights */
	for(i = 0; i < 8; ++i)
	{
		/* Only process lights that we are actually using */
		if (i < NUM_LIGHTS_IN_USE)
		{
			/* Transform light */
			stepLight(&m_psLightData[i]);

			/* Set light properties */
			myglLightv(GL_LIGHT0 + i, GL_POSITION, &m_psLightData[i].position.x);
			myglLightv(GL_LIGHT0 + i, GL_AMBIENT, &m_psLightData[i].ambient.x);
			myglLightv(GL_LIGHT0 + i, GL_DIFFUSE, &m_psLightData[i].diffuse.x);
			myglLightv(GL_LIGHT0 + i, GL_SPECULAR, &m_psLightData[i].specular.x);

			/* Enable light */
			glEnable(GL_LIGHT0 + i);
		}
		else
		{
			/* Disable remaining lights */
			glDisable(GL_LIGHT0 + i);
		}
	}

	/*************
	* Begin Scene
	*************/

	/* Set texture and texture environment */
	myglTexEnv(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glBindTexture(GL_TEXTURE_2D, texNameObject);

	/* Render geometry */

	/* Save matrix by pushing it on the stack */
	glPushMatrix();

	/* Add a small Y rotation to the model */
	myglMultMatrix(RotationMatrix.f);

	/* Loop through all meshes */
	for (i = 0; i < NUM_MESHES; i++)
	{
		/* Render mesh */
		RenderPrimitive(Meshes[i]->pVertex, Meshes[i]->pNormals, Meshes[i]->pUV, Meshes[i]->nNumFaces*3,
						Meshes[i]->pFaces, GL_TRIANGLES);
	}

	/* Restore matrix */
	glPopMatrix();

	// draw lights
	/* No lighting for lights */
	glDisable(GL_LIGHTING);

	/* Disable Z writes */
	glDepthMask(GL_FALSE);

	/* Set additive blending */
	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE,GL_ONE);

	/* Render all lights in use */
	for(i = 0; i < NUM_LIGHTS_IN_USE; ++i)
	{
		renderLight(&m_psLightData[i]);
	}

	/* Disable blending */
	glDisable(GL_BLEND);

	/* Restore Z writes */
	glDepthMask(GL_TRUE);

	/* Display info text. */
	m_AppPrint3D.DisplayDefaultTitle("OGLESLighting", "8 point lights.", PVR_LOGO);
	m_AppPrint3D.Flush();

	return true;
}

/*******************************************************************************
 * Function Name  : initLight
 * Inputs		  : *pLight
 * Description    : Initialize light structure
 *******************************************************************************/
void PVRDemo::initLight(SLightVars *pLight)
{
	/* Light ambient colour */
	pLight->ambient.x = f2vt(0.0);
	pLight->ambient.y = f2vt(0.0);
	pLight->ambient.z = f2vt(0.0);
	pLight->ambient.w = f2vt(1.0);

	/* Light Diffuse colour */
	double difFac = 0.4;
	pLight->diffuse.x = f2vt((float)( difFac * (rand()/(double)RAND_MAX) ) * 2.0f); //1.0;
	pLight->diffuse.y = f2vt((float)( difFac * (rand()/(double)RAND_MAX) ) * 2.0f); //1.0;
	pLight->diffuse.z = f2vt((float)( difFac * (rand()/(double)RAND_MAX) ) * 2.0f); //1.0;
	pLight->diffuse.w = f2vt((float)( 1.0 ));

	/* Light Specular colour */
	double specFac = 0.1;
	pLight->specular.x = f2vt((float)( specFac * (rand()/(double)RAND_MAX) ) * 2.0f); //1.0;
	pLight->specular.y = f2vt((float)( specFac * (rand()/(double)RAND_MAX) ) * 2.0f); //1.0;
	pLight->specular.z = f2vt((float)( specFac * (rand()/(double)RAND_MAX) ) * 2.0f); //1.0;
	pLight->specular.w = f2vt((float)( 1.0 ));

	/* Randomize some of the other parameters */
	float lightDist = 80.0f;
	pLight->vPosition.x = f2vt((float)((rand()/(double)RAND_MAX) * lightDist/2.0f ) + lightDist/2.0f);
	pLight->vPosition.y = f2vt((float)((rand()/(double)RAND_MAX) * lightDist/2.0f ) + lightDist/2.0f);
	pLight->vPosition.z = f2vt((float)((rand()/(double)RAND_MAX) * lightDist/2.0f ) + lightDist/2.0f);

	float rStep = 2;
	pLight->vRotationStep.x = f2vt((float)( rStep/2.0 - (rand()/(double)RAND_MAX) * rStep ));
	pLight->vRotationStep.y = f2vt((float)( rStep/2.0 - (rand()/(double)RAND_MAX) * rStep ));
	pLight->vRotationStep.z = f2vt((float)( rStep/2.0 - (rand()/(double)RAND_MAX) * rStep ));

	pLight->vRotation.x = f2vt(0.0f);
	pLight->vRotation.y = f2vt(0.0f);
	pLight->vRotation.z = f2vt(0.0f);

	pLight->vRotationCentre.x = f2vt(0.0f);
	pLight->vRotationCentre.y = f2vt(0.0f);
	pLight->vRotationCentre.z = f2vt(0.0f);
}

/*******************************************************************************
 * Function Name  : stepLight
 * Inputs		  : *pLight
 * Description    : Advance one step in the light rotation.
 *******************************************************************************/
void PVRDemo::stepLight(SLightVars *pLight)
{
	PVRTMATRIX RotationMatrix, RotationMatrixX, RotationMatrixY, RotationMatrixZ;

	/* Increase rotation angles */
	pLight->vRotation.x += pLight->vRotationStep.x;
	pLight->vRotation.y += pLight->vRotationStep.y;
	pLight->vRotation.z += pLight->vRotationStep.z;

	while(pLight->vRotation.x > f2vt(360.0f)) pLight->vRotation.x -= f2vt(360.0f);
	while(pLight->vRotation.y > f2vt(360.0f)) pLight->vRotation.y -= f2vt(360.0f);
	while(pLight->vRotation.z > f2vt(360.0f)) pLight->vRotation.z -= f2vt(360.0f);

	/* Create three rotations from rotation angles */
	PVRTMatrixRotationX(RotationMatrixX, VERTTYPEMUL(pLight->vRotation.x, f2vt(PVRTPIf/180.0f)));
	PVRTMatrixRotationY(RotationMatrixY, VERTTYPEMUL(pLight->vRotation.y, f2vt(PVRTPIf/180.0f)));
	PVRTMatrixRotationZ(RotationMatrixZ, VERTTYPEMUL(pLight->vRotation.z, f2vt(PVRTPIf/180.0f)));

	/* Build transformation matrix by concatenating all rotations */
	PVRTMatrixMultiply(RotationMatrix, RotationMatrixY, RotationMatrixZ);
	PVRTMatrixMultiply(RotationMatrix, RotationMatrixX, RotationMatrix);

	/* Transform light with transformation matrix, setting w to 1 to indicate point light */
	PVRTTransTransformArray((PVRTVECTOR3*)&pLight->position, &pLight->vPosition, 1, &RotationMatrix);
	pLight->position.w = f2vt(1.0f);
}

/*******************************************************************************
 * Function Name  : renderLight
 * Inputs		  : *pLight
 * Description    : Draw every light as a quad.
 *******************************************************************************/
void PVRDemo::renderLight(SLightVars *pLight)
{
	VERTTYPE	quad_verts[4*5];
	VERTTYPE	quad_uvs[2*5];
	VERTTYPE	fLightSize = f2vt(5.0f);

	/* Set quad vertices */
	quad_verts[0]  = pLight->position.x - fLightSize;
	quad_verts[1]  = pLight->position.y - fLightSize;
	quad_verts[2]  = pLight->position.z;

	quad_verts[3]  = pLight->position.x + fLightSize;
	quad_verts[4]  = pLight->position.y - fLightSize;
	quad_verts[5]  = pLight->position.z;

	quad_verts[6]  = pLight->position.x - fLightSize;
	quad_verts[7]  = pLight->position.y + fLightSize;
	quad_verts[8]  = pLight->position.z;

	quad_verts[9]  = pLight->position.x + fLightSize;
	quad_verts[10] = pLight->position.y + fLightSize;
	quad_verts[11] = pLight->position.z;

	/* Set Quad UVs */
	quad_uvs[0]   = f2vt(0);
	quad_uvs[1]   = f2vt(0);
	quad_uvs[2]   = f2vt(1);
	quad_uvs[3]   = f2vt(0);
	quad_uvs[4]   = f2vt(0);
	quad_uvs[5]   = f2vt(1);
	quad_uvs[6]   = f2vt(1);
	quad_uvs[7]   = f2vt(1);

	/* Set texture and texture environment */
	myglTexEnv(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glBindTexture(GL_TEXTURE_2D, texNameLight);

	/* Set vertex data */
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, VERTTYPEENUM, 0, quad_verts);

	/* Set texture coordinates data */
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2, VERTTYPEENUM, 0, quad_uvs);

	/* Set light colour 2x overbright for more contrast (will be modulated with texture) */
	myglColor4(VERTTYPEMUL(pLight->diffuse.x,f2vt(2.0f)), VERTTYPEMUL(pLight->diffuse.y,f2vt(2.0f)), VERTTYPEMUL(pLight->diffuse.z,f2vt(2.0f)), f2vt(1));

	/* Draw quad */
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	/* Disable client states */
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}

/*******************************************************************************
 * Function Name  : RenderPrimitive
 * Inputs		  : pVertex, pNormals, pUVs, nFirst, nStripLength, mode
 * Description    : Code to render an object
 *******************************************************************************/
void PVRDemo::RenderPrimitive(VERTTYPE *pVertex, VERTTYPE *pNormals, VERTTYPE *pUVs,
									int nNumIndex, unsigned short *pIDX, GLenum mode)
{
	/* Set vertex data */
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3,VERTTYPEENUM,0,pVertex);

	/* Set normal data */
	glEnableClientState(GL_NORMAL_ARRAY);
	glNormalPointer(VERTTYPEENUM,0,pNormals);

	/* Set texture coordinates data */
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2,VERTTYPEENUM,0,pUVs);

	/* Draw as indexed data */
	glDrawElements(mode, nNumIndex, GL_UNSIGNED_SHORT, pIDX);

	/* Disable client states */
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}

/*******************************************************************************
 * Function Name  : NewDemo
 * Description    : Called by the Shell to initialize a new instance to the
 *					demo class.
 *******************************************************************************/
PVRShell* NewDemo()
{
	return new PVRDemo;
}

/*****************************************************************************
 End of file (OGLESLighting.cpp)
*****************************************************************************/
