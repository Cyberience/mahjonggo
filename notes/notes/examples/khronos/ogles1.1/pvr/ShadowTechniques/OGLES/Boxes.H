/**********************************************************
 * This file has been created with PowerVR Exporter v3.3
 * Date: 22 November 2004   Time: 11:52
 * Original File: BoxScene.max 
 *
 * Options: 
 *
 *   Meshes        = ON
 *   Mesh Anim     = OFF
 *   Materials     = OFF
 *   Cameras       = OFF
 *   Camera Anim   = OFF
 *   Lights        = OFF
 *   Paths         = OFF
 *   Bones         = OFF
 *
 *   UVs           = ON
 *   Normals       = ON
 *   Colors        = OFF
 *   Materials     = OFF
 *
 *   Strips        = ON
 *   No Face-list  = OFF
 *   DWORD List    = OFF
 *   Packed Format = OFF
 *   OpenGL Format = ON
 *   Transform UVs = OFF
 *   Vertex Format = FLOAT
 *
 *   Patches       = OFF
 *   PatchTriToQuad= OFF
 *   Patch Mesh    = OFF
 *   Patch UVs     = OFF
 *
 **********************************************************/

#define STRUCT_MESH_DEFINED 

typedef struct { unsigned int      nNumVertex;
                 unsigned int      nNumFaces;
                 unsigned int      nNumStrips;
                 unsigned int      nFlags;
                 unsigned int      nMaterial;
                 float             fCenter[3];
                 float             *pVertex;
                 float             *pUV;
                 float             *pNormals;
                 float             *pPackedVertex;
                 unsigned int      *pVertexColor;
                 unsigned int      *pVertexMaterial;
                 unsigned short    *pFaces;
                 unsigned short    *pStrips;
                 unsigned short    *pStripLength;
                 struct
                 {
                     unsigned int  nType;
                     unsigned int  nNumPatches;
                     unsigned int  nNumVertices;
                     unsigned int  nNumSubdivisions;
                     float         *pControlPoints;
                     float         *pUVs;
                 } Patch;
               }   Struct_Mesh;


/**********************************************************
 * GENERAL DEFINITIONS
 **********************************************************/

#ifndef _NULL_
#define _NULL_ 0
#endif

#define NUM_MESHES     8
#define NUM_BONES	   0
#define NUM_MATERIALS  0
#define NUM_LIGHTS     0
#define NUM_CAMERAS    0
#define NUM_PATHS      0
#define NUM_FRAMES     0

/* MESH FLAGS */

#ifndef MF_MATERIAL
#define MF_MATERIAL        1
#define MF_UV              2
#define MF_NORMALS         4
#define MF_VERTEXCOLOR     8
#define MF_VERTEXMATERIAL  16
#define MF_STRIPS          32
#define MF_NOFACELIST      64

#define MF_NOPATCH         0
#define MF_RECTPATCH       1
#define MF_TRIPATCH        2
#define MF_MIXPATCH        3

#endif


/* MESHES */
#define M_BOX01  0
#define M_BOX02  1
#define M_BOX03  2
#define M_BOX04  3
#define M_BOX05  4
#define M_BOX06  5
#define M_BOX07  6
#define M_BOX08  7


/**********************************************************
 * MESHES DATA 
 **********************************************************/

/* MESH 0 */

#define box01_NumVertex  24
#define box01_NumFaces   12
#define box01_NumStrips  6
#define box01_Flags      0x00000126
#define box01_Material   0

float box01_Vertices[] = {
	4.7165f,-2.9985f,3.2971f, 3.3702f,-2.9985f,4.7760f, 4.8491f,-2.9985f,6.1224f, 6.1955f,-2.9985f,4.6434f, 4.7165f,-0.9985f,3.2971f, 
	6.1955f,-0.9985f,4.6434f, 4.8491f,-0.9985f,6.1224f, 3.3702f,-0.9985f,4.7760f, 6.1955f,-0.9985f,4.6434f, 6.1955f,-2.9985f,4.6434f, 
	4.8491f,-2.9985f,6.1224f, 4.8491f,-0.9985f,6.1224f, 4.7165f,-0.9985f,3.2971f, 4.7165f,-2.9985f,3.2971f, 6.1955f,-2.9985f,4.6434f, 
	6.1955f,-0.9985f,4.6434f, 3.3702f,-0.9985f,4.7760f, 3.3702f,-2.9985f,4.7760f, 4.7165f,-2.9985f,3.2971f, 4.7165f,-0.9985f,3.2971f, 
	4.8491f,-0.9985f,6.1224f, 4.8491f,-2.9985f,6.1224f, 3.3702f,-2.9985f,4.7760f, 3.3702f,-0.9985f,4.7760f, 
_NULL_ }; /* End of box01_Vertices */

float box01_UV[] = {
	-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,0.0000f,-0.0000f,1.0000f,1.0000f,1.0000f,-0.0000f,-0.0000f,0.0000f,0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,
	-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,-0.0000f,1.0000f,
_NULL_ }; /* End of box01_UV */

float box01_Normals[] = {
	0.0000f,-1.0000f,0.0000f, 0.0000f,-1.0000f,0.0000f, 0.0000f,-1.0000f,0.0000f, 0.0000f,-1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.7395f,0.0000f,0.6732f, 0.7395f,0.0000f,0.6732f, 0.7395f,0.0000f,0.6732f, 0.7395f,0.0000f,0.6732f, 0.6732f,0.0000f,-0.7395f, 0.6732f,0.0000f,-0.7395f, 0.6732f,0.0000f,-0.7395f, 
	0.6732f,0.0000f,-0.7395f, -0.7395f,0.0000f,-0.6732f, -0.7395f,0.0000f,-0.6732f, -0.7395f,0.0000f,-0.6732f, -0.7395f,0.0000f,-0.6732f, -0.6732f,0.0000f,0.7395f, -0.6732f,0.0000f,0.7395f, -0.6732f,0.0000f,0.7395f, -0.6732f,0.0000f,0.7395f, 
_NULL_ }; /* End of box01_Normals */

unsigned short box01_Faces[] = {
	0, 2, 1, 2, 0, 3, 4, 6, 5, 6, 4, 7, 8, 10, 9, 10, 8, 11, 12, 14, 13, 14, 12, 15, 16, 18, 17, 18, 16, 19, 
	20, 22, 21, 22, 20, 23, 
_NULL_ }; /* End of box01_Faces */

unsigned short box01_Strips[] = {
	23,22,20,21,19,18,16,17,15,14,12,13,11,10,8,9,7,6,4,5,3,2,0,1,
_NULL_ }; /* End of box01_pStrips */

unsigned short box01_StripLength[] = {
	2,2,2, 2,2,2, 
_NULL_ }; /* End of box01_pStripLength */


/* MESH 1 */

#define box02_NumVertex  24
#define box02_NumFaces   12
#define box02_NumStrips  6
#define box02_Flags      0x00000126
#define box02_Material   0

float box02_Vertices[] = {
	4.6050f,-0.9985f,3.9204f, 3.4333f,-0.9985f,5.5412f, 5.0542f,-0.9985f,6.7129f, 6.2259f,-0.9985f,5.0920f, 4.6050f,1.0015f,3.9204f, 
	6.2259f,1.0015f,5.0920f, 5.0542f,1.0015f,6.7129f, 3.4333f,1.0015f,5.5412f, 6.2259f,1.0015f,5.0920f, 6.2259f,-0.9985f,5.0920f, 
	5.0542f,-0.9985f,6.7129f, 5.0542f,1.0015f,6.7129f, 4.6050f,1.0015f,3.9204f, 4.6050f,-0.9985f,3.9204f, 6.2259f,-0.9985f,5.0920f, 
	6.2259f,1.0015f,5.0920f, 3.4333f,1.0015f,5.5412f, 3.4333f,-0.9985f,5.5412f, 4.6050f,-0.9985f,3.9204f, 4.6050f,1.0015f,3.9204f, 
	5.0542f,1.0015f,6.7129f, 5.0542f,-0.9985f,6.7129f, 3.4333f,-0.9985f,5.5412f, 3.4333f,1.0015f,5.5412f, 
_NULL_ }; /* End of box02_Vertices */

float box02_UV[] = {
	-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,0.0000f,-0.0000f,1.0000f,1.0000f,1.0000f,-0.0000f,-0.0000f,0.0000f,0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,
	-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,-0.0000f,1.0000f,
_NULL_ }; /* End of box02_UV */

float box02_Normals[] = {
	0.0000f,-1.0000f,0.0000f, 0.0000f,-1.0000f,0.0000f, 0.0000f,-1.0000f,0.0000f, 0.0000f,-1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.8104f,0.0000f,0.5858f, 0.8104f,0.0000f,0.5858f, 0.8104f,0.0000f,0.5858f, 0.8104f,0.0000f,0.5858f, 0.5858f,0.0000f,-0.8104f, 0.5858f,0.0000f,-0.8104f, 0.5858f,0.0000f,-0.8104f, 
	0.5858f,0.0000f,-0.8104f, -0.8104f,0.0000f,-0.5858f, -0.8104f,0.0000f,-0.5858f, -0.8104f,0.0000f,-0.5858f, -0.8104f,0.0000f,-0.5858f, -0.5858f,0.0000f,0.8104f, -0.5858f,0.0000f,0.8104f, -0.5858f,0.0000f,0.8104f, -0.5858f,0.0000f,0.8104f, 
_NULL_ }; /* End of box02_Normals */

unsigned short box02_Faces[] = {
	0, 2, 1, 2, 0, 3, 4, 6, 5, 6, 4, 7, 8, 10, 9, 10, 8, 11, 12, 14, 13, 14, 12, 15, 16, 18, 17, 18, 16, 19, 
	20, 22, 21, 22, 20, 23, 
_NULL_ }; /* End of box02_Faces */

unsigned short box02_Strips[] = {
	23,22,20,21,19,18,16,17,15,14,12,13,11,10,8,9,7,6,4,5,3,2,0,1,
_NULL_ }; /* End of box02_pStrips */

unsigned short box02_StripLength[] = {
	2,2,2, 2,2,2, 
_NULL_ }; /* End of box02_pStripLength */


/* MESH 2 */

#define box03_NumVertex  24
#define box03_NumFaces   12
#define box03_NumStrips  6
#define box03_Flags      0x00000126
#define box03_Material   0

float box03_Vertices[] = {
	3.0532f,-0.9992f,4.4980f, 1.0994f,-0.9992f,4.9258f, 1.5273f,-0.9992f,6.8795f, 3.4810f,-0.9992f,6.4517f, 3.0532f,1.0008f,4.4980f, 
	3.4810f,1.0008f,6.4517f, 1.5273f,1.0008f,6.8795f, 1.0994f,1.0008f,4.9258f, 3.4810f,1.0008f,6.4517f, 3.4810f,-0.9992f,6.4517f, 
	1.5273f,-0.9992f,6.8795f, 1.5273f,1.0008f,6.8795f, 3.0532f,1.0008f,4.4980f, 3.0532f,-0.9992f,4.4980f, 3.4810f,-0.9992f,6.4517f, 
	3.4810f,1.0008f,6.4517f, 1.0994f,1.0008f,4.9258f, 1.0994f,-0.9992f,4.9258f, 3.0532f,-0.9992f,4.4980f, 3.0532f,1.0008f,4.4980f, 
	1.5273f,1.0008f,6.8795f, 1.5273f,-0.9992f,6.8795f, 1.0994f,-0.9992f,4.9258f, 1.0994f,1.0008f,4.9258f, 
_NULL_ }; /* End of box03_Vertices */

float box03_UV[] = {
	-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,0.0000f,-0.0000f,1.0000f,1.0000f,1.0000f,-0.0000f,-0.0000f,0.0000f,0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,
	-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,-0.0000f,1.0000f,
_NULL_ }; /* End of box03_UV */

float box03_Normals[] = {
	0.0000f,-1.0000f,0.0000f, 0.0000f,-1.0000f,0.0000f, 0.0000f,-1.0000f,0.0000f, 0.0000f,-1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.2139f,0.0000f,0.9769f, 0.2139f,0.0000f,0.9769f, 0.2139f,0.0000f,0.9769f, 0.2139f,0.0000f,0.9769f, 0.9769f,0.0000f,-0.2139f, 0.9769f,0.0000f,-0.2139f, 0.9769f,0.0000f,-0.2139f, 
	0.9769f,0.0000f,-0.2139f, -0.2139f,0.0000f,-0.9769f, -0.2139f,0.0000f,-0.9769f, -0.2139f,0.0000f,-0.9769f, -0.2139f,0.0000f,-0.9769f, -0.9769f,0.0000f,0.2139f, -0.9769f,0.0000f,0.2139f, -0.9769f,0.0000f,0.2139f, -0.9769f,0.0000f,0.2139f, 
_NULL_ }; /* End of box03_Normals */

unsigned short box03_Faces[] = {
	0, 2, 1, 2, 0, 3, 4, 6, 5, 6, 4, 7, 8, 10, 9, 10, 8, 11, 12, 14, 13, 14, 12, 15, 16, 18, 17, 18, 16, 19, 
	20, 22, 21, 22, 20, 23, 
_NULL_ }; /* End of box03_Faces */

unsigned short box03_Strips[] = {
	23,22,20,21,19,18,16,17,15,14,12,13,11,10,8,9,7,6,4,5,3,2,0,1,
_NULL_ }; /* End of box03_pStrips */

unsigned short box03_StripLength[] = {
	2,2,2, 2,2,2, 
_NULL_ }; /* End of box03_pStripLength */


/* MESH 3 */

#define box04_NumVertex  24
#define box04_NumFaces   12
#define box04_NumStrips  6
#define box04_Flags      0x00000126
#define box04_Material   0

float box04_Vertices[] = {
	3.1912f,-2.9992f,3.9537f, 1.2001f,-2.9992f,4.1428f, 1.3892f,-2.9992f,6.1338f, 3.3803f,-2.9992f,5.9447f, 3.1912f,-0.9992f,3.9537f, 
	3.3803f,-0.9992f,5.9447f, 1.3892f,-0.9992f,6.1338f, 1.2001f,-0.9992f,4.1428f, 3.3803f,-0.9992f,5.9447f, 3.3803f,-2.9992f,5.9447f, 
	1.3892f,-2.9992f,6.1338f, 1.3892f,-0.9992f,6.1338f, 3.1912f,-0.9992f,3.9537f, 3.1912f,-2.9992f,3.9537f, 3.3803f,-2.9992f,5.9447f, 
	3.3803f,-0.9992f,5.9447f, 1.2001f,-0.9992f,4.1428f, 1.2001f,-2.9992f,4.1428f, 3.1912f,-2.9992f,3.9537f, 3.1912f,-0.9992f,3.9537f, 
	1.3892f,-0.9992f,6.1338f, 1.3892f,-2.9992f,6.1338f, 1.2001f,-2.9992f,4.1428f, 1.2001f,-0.9992f,4.1428f, 
_NULL_ }; /* End of box04_Vertices */

float box04_UV[] = {
	-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,0.0000f,-0.0000f,1.0000f,1.0000f,1.0000f,-0.0000f,-0.0000f,0.0000f,0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,
	-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,-0.0000f,1.0000f,
_NULL_ }; /* End of box04_UV */

float box04_Normals[] = {
	0.0000f,-1.0000f,0.0000f, 0.0000f,-1.0000f,0.0000f, 0.0000f,-1.0000f,0.0000f, 0.0000f,-1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.0945f,0.0000f,0.9955f, 0.0945f,0.0000f,0.9955f, 0.0945f,0.0000f,0.9955f, 0.0945f,0.0000f,0.9955f, 0.9955f,0.0000f,-0.0945f, 0.9955f,0.0000f,-0.0945f, 0.9955f,0.0000f,-0.0945f, 
	0.9955f,0.0000f,-0.0945f, -0.0945f,0.0000f,-0.9955f, -0.0945f,0.0000f,-0.9955f, -0.0945f,0.0000f,-0.9955f, -0.0945f,0.0000f,-0.9955f, -0.9955f,0.0000f,0.0945f, -0.9955f,0.0000f,0.0945f, -0.9955f,0.0000f,0.0945f, -0.9955f,0.0000f,0.0945f, 
_NULL_ }; /* End of box04_Normals */

unsigned short box04_Faces[] = {
	0, 2, 1, 2, 0, 3, 4, 6, 5, 6, 4, 7, 8, 10, 9, 10, 8, 11, 12, 14, 13, 14, 12, 15, 16, 18, 17, 18, 16, 19, 
	20, 22, 21, 22, 20, 23, 
_NULL_ }; /* End of box04_Faces */

unsigned short box04_Strips[] = {
	23,22,20,21,19,18,16,17,15,14,12,13,11,10,8,9,7,6,4,5,3,2,0,1,
_NULL_ }; /* End of box04_pStrips */

unsigned short box04_StripLength[] = {
	2,2,2, 2,2,2, 
_NULL_ }; /* End of box04_pStripLength */


/* MESH 4 */

#define box05_NumVertex  24
#define box05_NumFaces   12
#define box05_NumStrips  6
#define box05_Flags      0x00000126
#define box05_Material   0

float box05_Vertices[] = {
	-0.7132f,-0.9993f,3.6192f, -2.1434f,-0.9993f,5.0173f, -0.7453f,-0.9993f,6.4475f, 0.6849f,-0.9993f,5.0494f, -0.7132f,1.0007f,3.6192f, 
	0.6849f,1.0007f,5.0494f, -0.7453f,1.0007f,6.4475f, -2.1434f,1.0007f,5.0173f, 0.6849f,1.0007f,5.0494f, 0.6849f,-0.9993f,5.0494f, 
	-0.7453f,-0.9993f,6.4475f, -0.7453f,1.0007f,6.4475f, -0.7132f,1.0007f,3.6192f, -0.7132f,-0.9993f,3.6192f, 0.6849f,-0.9993f,5.0494f, 
	0.6849f,1.0007f,5.0494f, -2.1434f,1.0007f,5.0173f, -2.1434f,-0.9993f,5.0173f, -0.7132f,-0.9993f,3.6192f, -0.7132f,1.0007f,3.6192f, 
	-0.7453f,1.0007f,6.4475f, -0.7453f,-0.9993f,6.4475f, -2.1434f,-0.9993f,5.0173f, -2.1434f,1.0007f,5.0173f, 
_NULL_ }; /* End of box05_Vertices */

float box05_UV[] = {
	-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,0.0000f,-0.0000f,1.0000f,1.0000f,1.0000f,-0.0000f,-0.0000f,0.0000f,0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,
	-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,-0.0000f,1.0000f,
_NULL_ }; /* End of box05_UV */

float box05_Normals[] = {
	0.0000f,-1.0000f,0.0000f, 0.0000f,-1.0000f,0.0000f, 0.0000f,-1.0000f,0.0000f, 0.0000f,-1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.6990f,0.0000f,0.7151f, 0.6990f,0.0000f,0.7151f, 0.6990f,0.0000f,0.7151f, 0.6990f,0.0000f,0.7151f, 0.7151f,0.0000f,-0.6990f, 0.7151f,0.0000f,-0.6990f, 0.7151f,0.0000f,-0.6990f, 
	0.7151f,0.0000f,-0.6990f, -0.6990f,0.0000f,-0.7151f, -0.6990f,0.0000f,-0.7151f, -0.6990f,0.0000f,-0.7151f, -0.6990f,0.0000f,-0.7151f, -0.7151f,0.0000f,0.6990f, -0.7151f,0.0000f,0.6990f, -0.7151f,0.0000f,0.6990f, -0.7151f,0.0000f,0.6990f, 
_NULL_ }; /* End of box05_Normals */

unsigned short box05_Faces[] = {
	0, 2, 1, 2, 0, 3, 4, 6, 5, 6, 4, 7, 8, 10, 9, 10, 8, 11, 12, 14, 13, 14, 12, 15, 16, 18, 17, 18, 16, 19, 
	20, 22, 21, 22, 20, 23, 
_NULL_ }; /* End of box05_Faces */

unsigned short box05_Strips[] = {
	23,22,20,21,19,18,16,17,15,14,12,13,11,10,8,9,7,6,4,5,3,2,0,1,
_NULL_ }; /* End of box05_pStrips */

unsigned short box05_StripLength[] = {
	2,2,2, 2,2,2, 
_NULL_ }; /* End of box05_pStripLength */


/* MESH 5 */

#define box06_NumVertex  24
#define box06_NumFaces   12
#define box06_NumStrips  6
#define box06_Flags      0x00000126
#define box06_Material   0

float box06_Vertices[] = {
	-0.6688f,-2.9993f,2.8250f, -1.7337f,-2.9993f,4.5179f, -0.0408f,-2.9993f,5.5828f, 1.0241f,-2.9993f,3.8899f, -0.6688f,-0.9993f,2.8250f, 
	1.0241f,-0.9993f,3.8899f, -0.0408f,-0.9993f,5.5828f, -1.7337f,-0.9993f,4.5179f, 1.0241f,-0.9993f,3.8899f, 1.0241f,-2.9993f,3.8899f, 
	-0.0408f,-2.9993f,5.5828f, -0.0408f,-0.9993f,5.5828f, -0.6688f,-0.9993f,2.8250f, -0.6688f,-2.9993f,2.8250f, 1.0241f,-2.9993f,3.8899f, 
	1.0241f,-0.9993f,3.8899f, -1.7337f,-0.9993f,4.5179f, -1.7337f,-2.9993f,4.5179f, -0.6688f,-2.9993f,2.8250f, -0.6688f,-0.9993f,2.8250f, 
	-0.0408f,-0.9993f,5.5828f, -0.0408f,-2.9993f,5.5828f, -1.7337f,-2.9993f,4.5179f, -1.7337f,-0.9993f,4.5179f, 
_NULL_ }; /* End of box06_Vertices */

float box06_UV[] = {
	-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,0.0000f,-0.0000f,1.0000f,1.0000f,1.0000f,-0.0000f,-0.0000f,0.0000f,0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,
	-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,-0.0000f,1.0000f,
_NULL_ }; /* End of box06_UV */

float box06_Normals[] = {
	0.0000f,-1.0000f,0.0000f, 0.0000f,-1.0000f,0.0000f, 0.0000f,-1.0000f,0.0000f, 0.0000f,-1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.8465f,0.0000f,0.5325f, 0.8465f,0.0000f,0.5325f, 0.8465f,0.0000f,0.5325f, 0.8465f,0.0000f,0.5325f, 0.5325f,0.0000f,-0.8465f, 0.5325f,0.0000f,-0.8465f, 0.5325f,0.0000f,-0.8465f, 
	0.5325f,0.0000f,-0.8465f, -0.8465f,0.0000f,-0.5325f, -0.8465f,0.0000f,-0.5325f, -0.8465f,0.0000f,-0.5325f, -0.8465f,0.0000f,-0.5325f, -0.5325f,0.0000f,0.8465f, -0.5325f,0.0000f,0.8465f, -0.5325f,0.0000f,0.8465f, -0.5325f,0.0000f,0.8465f, 
_NULL_ }; /* End of box06_Normals */

unsigned short box06_Faces[] = {
	0, 2, 1, 2, 0, 3, 4, 6, 5, 6, 4, 7, 8, 10, 9, 10, 8, 11, 12, 14, 13, 14, 12, 15, 16, 18, 17, 18, 16, 19, 
	20, 22, 21, 22, 20, 23, 
_NULL_ }; /* End of box06_Faces */

unsigned short box06_Strips[] = {
	23,22,20,21,19,18,16,17,15,14,12,13,11,10,8,9,7,6,4,5,3,2,0,1,
_NULL_ }; /* End of box06_pStrips */

unsigned short box06_StripLength[] = {
	2,2,2, 2,2,2, 
_NULL_ }; /* End of box06_pStripLength */


/* MESH 6 */

#define box07_NumVertex  24
#define box07_NumFaces   12
#define box07_NumStrips  6
#define box07_Flags      0x00000126
#define box07_Material   0

float box07_Vertices[] = {
	1.2989f,-0.9992f,6.1992f, -0.6663f,-0.9992f,6.5709f, -0.2945f,-0.9992f,8.5360f, 1.6706f,-0.9992f,8.1643f, 1.2989f,1.0008f,6.1992f, 
	1.6706f,1.0008f,8.1643f, -0.2945f,1.0008f,8.5360f, -0.6663f,1.0008f,6.5709f, 1.6706f,1.0008f,8.1643f, 1.6706f,-0.9992f,8.1643f, 
	-0.2945f,-0.9992f,8.5360f, -0.2945f,1.0008f,8.5360f, 1.2989f,1.0008f,6.1992f, 1.2989f,-0.9992f,6.1992f, 1.6706f,-0.9992f,8.1643f, 
	1.6706f,1.0008f,8.1643f, -0.6663f,1.0008f,6.5709f, -0.6663f,-0.9992f,6.5709f, 1.2989f,-0.9992f,6.1992f, 1.2989f,1.0008f,6.1992f, 
	-0.2945f,1.0008f,8.5360f, -0.2945f,-0.9992f,8.5360f, -0.6663f,-0.9992f,6.5709f, -0.6663f,1.0008f,6.5709f, 
_NULL_ }; /* End of box07_Vertices */

float box07_UV[] = {
	-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,0.0000f,-0.0000f,1.0000f,1.0000f,1.0000f,-0.0000f,-0.0000f,0.0000f,0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,
	-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,-0.0000f,1.0000f,
_NULL_ }; /* End of box07_UV */

float box07_Normals[] = {
	0.0000f,-1.0000f,0.0000f, 0.0000f,-1.0000f,0.0000f, 0.0000f,-1.0000f,0.0000f, 0.0000f,-1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.1859f,0.0000f,0.9826f, 0.1859f,0.0000f,0.9826f, 0.1859f,0.0000f,0.9826f, 0.1859f,0.0000f,0.9826f, 0.9826f,0.0000f,-0.1859f, 0.9826f,0.0000f,-0.1859f, 0.9826f,0.0000f,-0.1859f, 
	0.9826f,0.0000f,-0.1859f, -0.1859f,0.0000f,-0.9826f, -0.1859f,0.0000f,-0.9826f, -0.1859f,0.0000f,-0.9826f, -0.1859f,0.0000f,-0.9826f, -0.9826f,0.0000f,0.1859f, -0.9826f,0.0000f,0.1859f, -0.9826f,0.0000f,0.1859f, -0.9826f,0.0000f,0.1859f, 
_NULL_ }; /* End of box07_Normals */

unsigned short box07_Faces[] = {
	0, 2, 1, 2, 0, 3, 4, 6, 5, 6, 4, 7, 8, 10, 9, 10, 8, 11, 12, 14, 13, 14, 12, 15, 16, 18, 17, 18, 16, 19, 
	20, 22, 21, 22, 20, 23, 
_NULL_ }; /* End of box07_Faces */

unsigned short box07_Strips[] = {
	23,22,20,21,19,18,16,17,15,14,12,13,11,10,8,9,7,6,4,5,3,2,0,1,
_NULL_ }; /* End of box07_pStrips */

unsigned short box07_StripLength[] = {
	2,2,2, 2,2,2, 
_NULL_ }; /* End of box07_pStripLength */


/* MESH 7 */

#define box08_NumVertex  24
#define box08_NumFaces   12
#define box08_NumStrips  6
#define box08_Flags      0x00000126
#define box08_Material   0

float box08_Vertices[] = {
	0.6728f,-3.0000f,6.4396f, -1.2169f,-3.0000f,7.0943f, -0.5622f,-3.0000f,8.9841f, 1.3276f,-3.0000f,8.3293f, 0.6728f,-1.0000f,6.4396f, 
	1.3276f,-1.0000f,8.3293f, -0.5622f,-1.0000f,8.9841f, -1.2169f,-1.0000f,7.0943f, 1.3276f,-1.0000f,8.3293f, 1.3276f,-3.0000f,8.3293f, 
	-0.5622f,-3.0000f,8.9841f, -0.5622f,-1.0000f,8.9841f, 0.6728f,-1.0000f,6.4396f, 0.6728f,-3.0000f,6.4396f, 1.3276f,-3.0000f,8.3293f, 
	1.3276f,-1.0000f,8.3293f, -1.2169f,-1.0000f,7.0943f, -1.2169f,-3.0000f,7.0943f, 0.6728f,-3.0000f,6.4396f, 0.6728f,-1.0000f,6.4396f, 
	-0.5622f,-1.0000f,8.9841f, -0.5622f,-3.0000f,8.9841f, -1.2169f,-3.0000f,7.0943f, -1.2169f,-1.0000f,7.0943f, 
_NULL_ }; /* End of box08_Vertices */

float box08_UV[] = {
	-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,0.0000f,-0.0000f,1.0000f,1.0000f,1.0000f,-0.0000f,-0.0000f,0.0000f,0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,
	-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,-0.0000f,1.0000f,1.0000f,1.0000f,1.0000f,0.0000f,-0.0000f,0.0000f,-0.0000f,1.0000f,
_NULL_ }; /* End of box08_UV */

float box08_Normals[] = {
	0.0000f,-1.0000f,0.0000f, 0.0000f,-1.0000f,0.0000f, 0.0000f,-1.0000f,0.0000f, 0.0000f,-1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.0000f,1.0000f,0.0000f, 0.3274f,0.0000f,0.9449f, 0.3274f,0.0000f,0.9449f, 0.3274f,0.0000f,0.9449f, 0.3274f,0.0000f,0.9449f, 0.9449f,0.0000f,-0.3274f, 0.9449f,0.0000f,-0.3274f, 0.9449f,0.0000f,-0.3274f, 
	0.9449f,0.0000f,-0.3274f, -0.3274f,0.0000f,-0.9449f, -0.3274f,0.0000f,-0.9449f, -0.3274f,0.0000f,-0.9449f, -0.3274f,0.0000f,-0.9449f, -0.9449f,0.0000f,0.3274f, -0.9449f,0.0000f,0.3274f, -0.9449f,0.0000f,0.3274f, -0.9449f,0.0000f,0.3274f, 
_NULL_ }; /* End of box08_Normals */

unsigned short box08_Faces[] = {
	0, 2, 1, 2, 0, 3, 4, 6, 5, 6, 4, 7, 8, 10, 9, 10, 8, 11, 12, 14, 13, 14, 12, 15, 16, 18, 17, 18, 16, 19, 
	20, 22, 21, 22, 20, 23, 
_NULL_ }; /* End of box08_Faces */

unsigned short box08_Strips[] = {
	23,22,20,21,19,18,16,17,15,14,12,13,11,10,8,9,7,6,4,5,3,2,0,1,
_NULL_ }; /* End of box08_pStrips */

unsigned short box08_StripLength[] = {
	2,2,2, 2,2,2, 
_NULL_ }; /* End of box08_pStripLength */



/**********************************************************
 * MESHES 
 **********************************************************/

Struct_Mesh 
   Mesh[NUM_MESHES] = {

/* MESH 0 */
	/* NumVertex      */ box01_NumVertex,
	/* NumFaces       */ box01_NumFaces,
	/* NumStrips      */ box01_NumStrips,
	/* Flags          */ box01_Flags,
	/* Material       */ box01_Material,
	/* Center         */  4.782834f, -2.998549f, -4.709728f,
	/* Vertices       */ box01_Vertices,
	/* UV             */ box01_UV,
	/* Normals        */ box01_Normals,
	/* Packed Vertices*/ (float *) _NULL_,
	/* VertexColor    */ (unsigned int *)_NULL_,
	/* VertexMaterial */ (unsigned int *)_NULL_,
	/* Faces          */ (unsigned short *)box01_Faces,
	/* Strips         */ (unsigned short *)box01_Strips,
	/* StripLength    */ (unsigned short *)box01_StripLength, 
	/* Patch type     */ 0,
	/* NumPatches     */ 0,
	/* NumPVertices   */ 0,
	/* NumSubdivisions*/ 0,
	/* ControlPoints  */ _NULL_,
	/* PatchUVs       */ _NULL_,

/* MESH 1 */
	/* NumVertex      */ box02_NumVertex,
	/* NumFaces       */ box02_NumFaces,
	/* NumStrips      */ box02_NumStrips,
	/* Flags          */ box02_Flags,
	/* Material       */ box02_Material,
	/* Center         */  4.829596f, -0.998549f, -5.316612f,
	/* Vertices       */ box02_Vertices,
	/* UV             */ box02_UV,
	/* Normals        */ box02_Normals,
	/* Packed Vertices*/ (float *) _NULL_,
	/* VertexColor    */ (unsigned int *)_NULL_,
	/* VertexMaterial */ (unsigned int *)_NULL_,
	/* Faces          */ (unsigned short *)box02_Faces,
	/* Strips         */ (unsigned short *)box02_Strips,
	/* StripLength    */ (unsigned short *)box02_StripLength, 
	/* Patch type     */ 0,
	/* NumPatches     */ 0,
	/* NumPVertices   */ 0,
	/* NumSubdivisions*/ 0,
	/* ControlPoints  */ _NULL_,
	/* PatchUVs       */ _NULL_,

/* MESH 2 */
	/* NumVertex      */ box03_NumVertex,
	/* NumFaces       */ box03_NumFaces,
	/* NumStrips      */ box03_NumStrips,
	/* Flags          */ box03_Flags,
	/* Material       */ box03_Material,
	/* Center         */  2.290208f, -0.999239f, -5.688769f,
	/* Vertices       */ box03_Vertices,
	/* UV             */ box03_UV,
	/* Normals        */ box03_Normals,
	/* Packed Vertices*/ (float *) _NULL_,
	/* VertexColor    */ (unsigned int *)_NULL_,
	/* VertexMaterial */ (unsigned int *)_NULL_,
	/* Faces          */ (unsigned short *)box03_Faces,
	/* Strips         */ (unsigned short *)box03_Strips,
	/* StripLength    */ (unsigned short *)box03_StripLength, 
	/* Patch type     */ 0,
	/* NumPatches     */ 0,
	/* NumPVertices   */ 0,
	/* NumSubdivisions*/ 0,
	/* ControlPoints  */ _NULL_,
	/* PatchUVs       */ _NULL_,

/* MESH 3 */
	/* NumVertex      */ box04_NumVertex,
	/* NumFaces       */ box04_NumFaces,
	/* NumStrips      */ box04_NumStrips,
	/* Flags          */ box04_Flags,
	/* Material       */ box04_Material,
	/* Center         */  2.290208f, -2.999239f, -5.043767f,
	/* Vertices       */ box04_Vertices,
	/* UV             */ box04_UV,
	/* Normals        */ box04_Normals,
	/* Packed Vertices*/ (float *) _NULL_,
	/* VertexColor    */ (unsigned int *)_NULL_,
	/* VertexMaterial */ (unsigned int *)_NULL_,
	/* Faces          */ (unsigned short *)box04_Faces,
	/* Strips         */ (unsigned short *)box04_Strips,
	/* StripLength    */ (unsigned short *)box04_StripLength, 
	/* Patch type     */ 0,
	/* NumPatches     */ 0,
	/* NumPVertices   */ 0,
	/* NumSubdivisions*/ 0,
	/* ControlPoints  */ _NULL_,
	/* PatchUVs       */ _NULL_,

/* MESH 4 */
	/* NumVertex      */ box05_NumVertex,
	/* NumFaces       */ box05_NumFaces,
	/* NumStrips      */ box05_NumStrips,
	/* Flags          */ box05_Flags,
	/* Material       */ box05_Material,
	/* Center         */  -0.729243f, -0.999287f, -5.033360f,
	/* Vertices       */ box05_Vertices,
	/* UV             */ box05_UV,
	/* Normals        */ box05_Normals,
	/* Packed Vertices*/ (float *) _NULL_,
	/* VertexColor    */ (unsigned int *)_NULL_,
	/* VertexMaterial */ (unsigned int *)_NULL_,
	/* Faces          */ (unsigned short *)box05_Faces,
	/* Strips         */ (unsigned short *)box05_Strips,
	/* StripLength    */ (unsigned short *)box05_StripLength, 
	/* Patch type     */ 0,
	/* NumPatches     */ 0,
	/* NumPVertices   */ 0,
	/* NumSubdivisions*/ 0,
	/* ControlPoints  */ _NULL_,
	/* PatchUVs       */ _NULL_,

/* MESH 5 */
	/* NumVertex      */ box06_NumVertex,
	/* NumFaces       */ box06_NumFaces,
	/* NumStrips      */ box06_NumStrips,
	/* Flags          */ box06_Flags,
	/* Material       */ box06_Material,
	/* Center         */  -0.354833f, -2.999286f, -4.203907f,
	/* Vertices       */ box06_Vertices,
	/* UV             */ box06_UV,
	/* Normals        */ box06_Normals,
	/* Packed Vertices*/ (float *) _NULL_,
	/* VertexColor    */ (unsigned int *)_NULL_,
	/* VertexMaterial */ (unsigned int *)_NULL_,
	/* Faces          */ (unsigned short *)box06_Faces,
	/* Strips         */ (unsigned short *)box06_Strips,
	/* StripLength    */ (unsigned short *)box06_StripLength, 
	/* Patch type     */ 0,
	/* NumPatches     */ 0,
	/* NumPVertices   */ 0,
	/* NumSubdivisions*/ 0,
	/* ControlPoints  */ _NULL_,
	/* PatchUVs       */ _NULL_,

/* MESH 6 */
	/* NumVertex      */ box07_NumVertex,
	/* NumFaces       */ box07_NumFaces,
	/* NumStrips      */ box07_NumStrips,
	/* Flags          */ box07_Flags,
	/* Material       */ box07_Material,
	/* Center         */  0.502161f, -0.999182f, -7.367607f,
	/* Vertices       */ box07_Vertices,
	/* UV             */ box07_UV,
	/* Normals        */ box07_Normals,
	/* Packed Vertices*/ (float *) _NULL_,
	/* VertexColor    */ (unsigned int *)_NULL_,
	/* VertexMaterial */ (unsigned int *)_NULL_,
	/* Faces          */ (unsigned short *)box07_Faces,
	/* Strips         */ (unsigned short *)box07_Strips,
	/* StripLength    */ (unsigned short *)box07_StripLength, 
	/* Patch type     */ 0,
	/* NumPatches     */ 0,
	/* NumPVertices   */ 0,
	/* NumSubdivisions*/ 0,
	/* ControlPoints  */ _NULL_,
	/* PatchUVs       */ _NULL_,

/* MESH 7 */
	/* NumVertex      */ box08_NumVertex,
	/* NumFaces       */ box08_NumFaces,
	/* NumStrips      */ box08_NumStrips,
	/* Flags          */ box08_Flags,
	/* Material       */ box08_Material,
	/* Center         */  0.055330f, -3.000000f, -7.711826f,
	/* Vertices       */ box08_Vertices,
	/* UV             */ box08_UV,
	/* Normals        */ box08_Normals,
	/* Packed Vertices*/ (float *) _NULL_,
	/* VertexColor    */ (unsigned int *)_NULL_,
	/* VertexMaterial */ (unsigned int *)_NULL_,
	/* Faces          */ (unsigned short *)box08_Faces,
	/* Strips         */ (unsigned short *)box08_Strips,
	/* StripLength    */ (unsigned short *)box08_StripLength, 
	/* Patch type     */ 0,
	/* NumPatches     */ 0,
	/* NumPVertices   */ 0,
	/* NumSubdivisions*/ 0,
	/* ControlPoints  */ _NULL_,
	/* PatchUVs       */ _NULL_

	}; /* End of Meshes */
