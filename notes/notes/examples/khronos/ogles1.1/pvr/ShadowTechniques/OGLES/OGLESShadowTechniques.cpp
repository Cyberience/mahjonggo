/******************************************************************************

 @File         OGLESShadowTechniques.cpp

 @Title        Demonstrates MBX shadow techniques

 @Copyright    Copyright (C) 2004 - 2006 by Imagination Technologies Limited.

 @Platform     Independant

 @Description  Requires the PVRShell.

******************************************************************************/
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "PVRShell.h"
#include "OGLESTools.h"

/* -- Monster Geometry and Textures -- */

#define LOADFROMHEADER

//#define MONSTER_ALTERNATIVE

#ifndef MONSTER_ALTERNATIVE
	#if defined LOADFROMHEADER
		#include "Monster1_Geo.h"
		#define MONSTER Monster1_Geo
	#else
		#define MONSTERFILENAME "./../../../Media/Monster1.md2"
	#endif

	#include "Monster1_Tex.h"
	#define MONSTERTEXTURE	Monster1_Tex
#else
	#if defined LOADFROMHEADER
		#include "Monster2_Geo.h"
		#define MONSTER Monster2_Geo
	#else
		#define MONSTERFILENAME "./../../../Media/Monster2.md2"
	#endif

	#include "Monster2_Tex.h"
	#define MONSTERTEXTURE	Monster2_Tex
#endif

/* -- Scene Geometry -- */

#include "Boxes.H"

/* -- Textures -- */

#include "Blob.h"
#include "Crate.h"
#include "Floor.h"
#include "LightSource.h"

/* -- Defines -- */

#ifndef PI
#define PI 3.14159f
#endif

#define OFFSETA f2vt(-10.0f)		// Polygon Offset Values - Might need to be tweaked for various HW
#define OFFSETB f2vt(-25.0f)		// Polygon Offset Values - These avoid ZFighting between floor and shadow

#define	FLOORSIZE	10.0f	// Used to generate the Floor Plane
#define FLOORHEIGHT	3.0f	// Used to position the Floor Plane

#define	CHARWIDTH	2.2f	// Used to draw the basic blob shadow

#define	LIGHTPOSSCALE	1.25f	// Light Position Scale used to position the light at correct distance

#define SELECTEDANIMATION		0		// 0 = STAND - 1 = RUN - 2 = SHOOT - 6 = JUMP - 7 = SCRATCH - Etc...
#define ANIMATIONSTEP			0.125f	// Animation Time Increase Per Frame - Higher = Faster Animation
#define ENABLE_GL_LIGHTING		0		// Toggle for OGL Lighting
#define ENABLE_TEXTURES			1		// Toggle Textures
#define ENABLE_OBJ_ROT			0		// Rotate the whole scene
#define ENABLE_LIGHT_ROT		1		// Animate the Light Position

/* -- Build Options to Force a certain shadow type -- */

//#define DRAW_ADV_BLOB_SHADOW		1		// Force Adv Blob Shadow
//#define DRAW_PROJ_SHADOW			1		// Force Projected Shadow
//#define DRAW_BASE_BLOB_SHADOW		1		// Force Basic Blob Shadow
//#define DRAW_RENDER2TEX_SHADOW		1		// Force Render To Texture Shadow

/* -- PBuffer Options -- */

#define PBUFFERSIZE				128		// Render To Texture Size

/* -- Geometry drawing option -- */

#define USE_FANS_N_STRIPS		0		// Use Indexed Triangle List to draw or use Fans/Strips to draw

/* -- Light Animation Params -- */

#define MAXANIMATIONSTEPS		7		// Number of Entries in the Animation Table

/* -- Number of frames for each render 2 texture shadow regeneration -- */

#define PBUFFERUPDATESKIP		2

/* Set Matrix Enums */

enum MatrixEnums
{
	CAMERA,
	LIGHT,
	PROJTEX,
	PROJGEOM,
	BILLBOARD
};

/* Shadow Modes */

enum ShadowModes
{
	BASEBLOBMODE,
	ADVANCEDBLOBMODE,
	PROJGEOMMODE,
	R2TEXMODE
};

/* Actual Source Code Starts Here */

class OGLESShadowTechniques : public PVRShell{

	/* -- Print3D Class Object -- */
	CPVRTPrint3D 	AppPrint3D;

	/* -- Textures -- */
	GLuint gMonsterMap;
	GLuint gFloorMap;
	GLuint gBlobMap;
	GLuint gLightSourceMap;
	GLuint gCrateMap;

	/* -- Extension Support -- */
	bool	gCombinersPresent;
	bool	gIMGTextureFFExtPresent;
	bool	gEnable_PBuffer_Usage;			// Do context swap or use main framebuffer to render to texture
	bool	gEnable_PBuffer_Bind_Texture;	// Enables the use of Open GL ES 1.1 Render to texture extension

	/* -- Meshes Objects-- */
	CMD2Model		MonsterObj;
	unsigned short*	MonsterTriangleList;

	/* -- Shadow Projection on Floor -- */
	PVRTVECTOR3	floorVertices[4];
	PVRTVECTOR4	floorPlane;
	PVRTMATRIX	floorShadow;

	/* -- Animation -- */
	int		gnObjRotCntr;
	float	gnLightRotCntr;
	bool	bAnimated;
	float	MyTime;
	int		gnLightDelta;

	int		gAnimTbl[8][3];
	float	fAnimationCounter;
	int		gnCurrentAnimation;
	ShadowModes		gnCurrentMode;

	unsigned long	OldTime;
	float			fFactor;
	float			fOldFactor;
	float			fOlderFactor;

	/* -- Camera -- */
	PVRTVECTOR3 MyFrom;
	PVRTVECTOR3 MyTo;
	PVRTVECTOR3 MyUp;

	PVRTMATRIX	MyRotMatrix,MyViewMatrix;
	PVRTMATRIX	MyRotViewMatrix;

	int		gnPBufferUpdateCounter;

	/* -- Lighting -- */
	PVRTVECTOR4 vCurrentLightPos;
	PVRTVECTOR4 lightcoloramb;
	PVRTVECTOR4 lightcolordiff;
	PVRTVECTOR4 lightcolorspec;
	VERTTYPE	shininess;

	/* Bounding Ellips Data */
	GLfloat Radius;
	GLfloat YMin;
	GLfloat YMax;

	/* -- Options -- */
	bool bColorEnable;
	bool bNormalEnable;
	bool bTexCoordEnable;

	/* -- PBuffer -- */
	EGLSurface MyPBufSurface;
	EGLDisplay CurrentDisplay;
	EGLContext CurrentContext;
	EGLSurface CurrentSurface;
	GLuint MyPBufferTexture;

	/* Header Object to Lite Conversion */
	HeaderStruct_Mesh_Type** Meshes;

	public:
		OGLESShadowTechniques()
	{
		/* -- globals -- */
		/* -- Textures -- */
		gBlobMap		= 0;
		gCrateMap		= 0;
		gFloorMap		= 0;
        gLightSourceMap = 0;
		gMonsterMap		= 0;

		/* -- Extension Support -- */
		gCombinersPresent = false;
		gIMGTextureFFExtPresent = false;

		/* -- Data for Shadow Projection on Floor -- */
		floorVertices[0].x = f2vt(-20.0f);	floorVertices[0].y = f2vt(-3.0f);	floorVertices[0].z = f2vt( 20.0f);
		floorVertices[1].x = f2vt( 20.0f);	floorVertices[1].y = f2vt(-3.0f);	floorVertices[1].z = f2vt( 20.0f);
		floorVertices[2].x = f2vt( 20.0f);	floorVertices[2].y = f2vt(-3.0f);	floorVertices[2].z = f2vt(-20.0f);
		floorVertices[3].x = f2vt(-20.0f);	floorVertices[3].y = f2vt(-3.0f);	floorVertices[3].z = f2vt(-20.0f);

		/* -- Animation -- */
		gnObjRotCntr = 0;
		gnLightRotCntr = -145;
		bAnimated = true;
		MyTime=1.0f;
		gnLightDelta=1;
		gnCurrentAnimation=0;
		fAnimationCounter=0;

		gnPBufferUpdateCounter=0;

		/* -- Animation Table -- */
		gAnimTbl[0][0] = -145;	gAnimTbl[0][1] =   45;	gAnimTbl[0][2] = BASEBLOBMODE;
		gAnimTbl[1][0] =   45;	gAnimTbl[1][1] = -145;	gAnimTbl[1][2] = ADVANCEDBLOBMODE;
		gAnimTbl[2][0] = -145;	gAnimTbl[2][1] =   45;	gAnimTbl[2][2] = PROJGEOMMODE;
		gAnimTbl[3][0] =   45;	gAnimTbl[3][1] =  100;	gAnimTbl[3][2] = PROJGEOMMODE;		// Transistion
		gAnimTbl[4][0] =  100;	gAnimTbl[4][1] =  250;	gAnimTbl[4][2] = R2TEXMODE;
		gAnimTbl[5][0] =  250;	gAnimTbl[5][1] =  100;	gAnimTbl[5][2] = R2TEXMODE;
		gAnimTbl[6][0] =  100;	gAnimTbl[6][1] =   45;	gAnimTbl[6][2] = PROJGEOMMODE;		// Transistion
		gAnimTbl[7][0] =   45;	gAnimTbl[7][1] = -145;	gAnimTbl[7][2] = PROJGEOMMODE;		// Transistion

		gnCurrentMode = ((ShadowModes)gAnimTbl[0][2]);

		/* -- Lighting -- */
		vCurrentLightPos.x = f2vt(10.0f);	vCurrentLightPos.y = f2vt(10.0f);	vCurrentLightPos.z = f2vt(-10.0f);	vCurrentLightPos.w = f2vt(0.0f);

		lightcoloramb.x = f2vt(0.1f);	lightcoloramb.y = f2vt(0.1f);	lightcoloramb.z = f2vt(0.1f);	lightcoloramb.w = f2vt(0.1f);
		lightcolordiff.x = f2vt(0.2f);	lightcolordiff.y = f2vt(0.2f);	lightcolordiff.z = f2vt(0.2f);	lightcolordiff.w = f2vt(1.0f);
		lightcolorspec.x = f2vt(0.2f);	lightcolorspec.y = f2vt(0.2f);	lightcolorspec.z = f2vt(0.2f);	lightcolorspec.w = f2vt(1.0f);

		shininess = f2vt(2.0f);

		Radius = 0;
		YMin = 0;
		YMax = 0;

		MyUp.x = f2vt(0.0f);
		MyUp.y = f2vt(1.0f);
		MyUp.z = f2vt(0.0f);

		/* Set options based on Defines */
		if (ENABLE_GL_LIGHTING)
		{
			bColorEnable		=	false;
			bNormalEnable		=	true;
		}
		else
		{
			bColorEnable		=	true;
			bNormalEnable		=	false;
		}

		if (ENABLE_TEXTURES)
		{
			bTexCoordEnable	=	true;
		}
		else
		{
			bTexCoordEnable	=	false;
		}

		/* PBuffer Init */
		MyPBufSurface = EGL_NO_SURFACE;
		CurrentDisplay	= EGL_NO_DISPLAY;
		CurrentContext	= EGL_NO_CONTEXT;
		CurrentSurface = EGL_NO_SURFACE;
	}

	virtual bool InitApplication();
	virtual bool InitView();
	virtual bool ReleaseView();
	virtual bool QuitApplication();
	virtual bool RenderScene();

	/****************************************************************************
	** Function Definitions
	****************************************************************************/
	void shadowMatrix(PVRTMATRIX &shadowMat, const PVRTVECTOR4 &groundplane, const PVRTVECTOR4 &lightpos);
	void findPlane(PVRTVECTOR4 &plane, const PVRTVECTOR3 &v0, const PVRTVECTOR3 &v1, const PVRTVECTOR3 &v2);
	void DrawFloor();
	void RenderMD2(CMD2Model *Model, float time, bool bColorEnable, bool bNormalEnable, bool bTexCoordEnable, bool bComputeBBox, unsigned short* Indices, bool bFanStripMode);
	void DrawBlob();
	void DrawBaseBlob();
	void GetBoundingEllips(VERTTYPE *Vertices, int nNumOfVertices);
	void DrawProjectedShadow();
	void DrawAdvancedBlobShadow();
	EGLConfig SelectEGLConfig();
	void DrawProjectedFloor();
	void DrawBillBoardQuad(
		const PVRTVECTOR3	&MyFrom,
		const PVRTVECTOR3	&MyTo,
		const VERTTYPE		x,
		const VERTTYPE		y,
		const VERTTYPE		z,
		const VERTTYPE		Size,
		const GLuint		pTexture);
	void DrawProjectedMeshes();
	void UpdateLightPosition();
	void SetupMatrices(MatrixEnums MatrixSelected);
	void DrawMeshes();
};

/*******************************************************************************
 * Function Name  : InitApplication
 * Returns        : TRUE if no error occured
 * Description    : App / Shell Interface
 *
 *******************************************************************************/

bool OGLESShadowTechniques::InitApplication()
{
	/* If OpenGL 1.1, use Render To Texture extension (eglBindTexImage) */
#ifdef GL_OES_VERSION_1_1
	gEnable_PBuffer_Bind_Texture = true;
#else
	gEnable_PBuffer_Bind_Texture = false;
#endif

	// Request PBuffer support
	PVRShellSet(prefPBufferContext, 1);

	/* Load Max Geometry */
	/* Header Objects */
	Meshes = new HeaderStruct_Mesh_Type*[NUM_MESHES];
	for(int i = 0; i < NUM_MESHES; i++)
	{
		Meshes[i] = PVRTLoadHeaderObject(&Mesh[i]);
	}

	return true;
}

/*******************************************************************************
 * Function Name  : QuitApplication
 * Returns        : true if no error occured
 * Description    : Code in QuitApplication() will be called by the Shell ONCE per
 *					run, just before exiting the program.
 *******************************************************************************/
bool OGLESShadowTechniques::QuitApplication()
{
	for(int i = 0; i < NUM_MESHES; i++)
	{
		PVRTUnloadHeaderObject(Meshes[i]);
	}
	delete [] Meshes;
	return true;
}

/*******************************************************************************
 * Function Name  : InitView
 * Returns        : TRUE if no error occured
 * Description    : App / Shell Interface
 *
 *******************************************************************************/
bool OGLESShadowTechniques::InitView()
{
	int err;
	SPVRTContext Context;

	/* Detects if PBuffer is available */
	gEnable_PBuffer_Usage = PVRShellGet(prefPBufferContext) ? true : false;

#if UNDER_CE == 420
	gEnable_PBuffer_Usage = false;
#endif

	/* If PBuffer is not available, do not use gEnable_PBuffer_Bind_Texture */
	if (!gEnable_PBuffer_Usage)
	{
		gEnable_PBuffer_Bind_Texture = false;
	}

	/* Init Print3D Textures */
	err = AppPrint3D.SetTextures(&Context,PVRShellGet(prefWidth),PVRShellGet(prefHeight));
	if(err == false)
	{
		PVRShellOutputDebug ("ERROR: Cannot initialise Print3D\n");
		return false;
	}

	if (!PVRTLoadTextureFromPointer((void*)MONSTERTEXTURE, &gMonsterMap)) return false;
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if (!PVRTLoadTextureFromPointer((void*)Floor, &gFloorMap)) return false;
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if (!PVRTLoadTextureFromPointer((void*)Blob, &gBlobMap)) return false;
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if (!PVRTLoadTextureFromPointer((void*)LightSource, &gLightSourceMap)) return false;
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if (!PVRTLoadTextureFromPointer((void*)Crate, &gCrateMap)) return false;
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	#if(OGLESLITE)
	{
		#if defined LOADFROMHEADER
			if(!MonsterObj.LoadModelFixedFromHeader((char*)MONSTER))
			{
				PVRShellOutputDebug("Failed to load Model !\n");
				return false;
			}
		#else
			if(!MonsterObj.LoadModelFixedFromFile(MONSTERFILENAME))
			{
				PVRShellOutputDebug("Failed to load Model !\n");
				return false;
			}
		#endif
	}
	#else
	{
		#if defined LOADFROMHEADER
			if(!MonsterObj.LoadModelFloatFromHeader((char*)MONSTER))
			{
				PVRShellOutputDebug("Failed to load Model !\n");
				return false;
			}
		#else
			if(!MonsterObj.LoadModelFloatFromFile(MONSTERFILENAME))
			{
				PVRShellOutputDebug("Failed to load Model !\n");
				return false;
			}
		#endif
	}
	#endif

	/* Set Selected Animation and Global Scale Factor */
	MonsterObj.SetAnim( SELECTEDANIMATION );
	MonsterObj.ScaleModel( 0.125f );

	/* Generate Indexed Triangle List data for rendering MD2 Model */
	MonsterTriangleList = MonsterObj.GenerateTriangleList();

	/* check extensions */
	gCombinersPresent = CPVRTglesExt::IsGLExtensionSupported("GL_ARB_texture_env_combine");
	gCombinersPresent &= CPVRTglesExt::IsGLExtensionSupported("GL_ARB_texture_env_dot3");
	if (gCombinersPresent)
		PVRShellOutputDebug("GL_ARB_texture_env_combine Supported\n");

	gIMGTextureFFExtPresent = CPVRTglesExt::IsGLExtensionSupported("GL_IMG_texture_env_enhanced_fixed_function");
	if (gIMGTextureFFExtPresent)
		PVRShellOutputDebug("GL_IMG_texture_env_enhanced_fixed_function Supported\n");

	if(!gCombinersPresent && !gIMGTextureFFExtPresent )
	{
		PVRShellOutputDebug("Can't run this demo without Combiners or IMG_Texture_Ext\n");
		return false; // Can't run this demo
	}

	/* opengl lighting initialization */
	glEnable ( GL_LIGHTING );
	glEnable ( GL_LIGHT0 );

	myglLightv( GL_LIGHT0, GL_POSITION, &vCurrentLightPos.x);
	myglLightv( GL_LIGHT0, GL_AMBIENT, &lightcoloramb.x);
	myglLightv( GL_LIGHT0, GL_DIFFUSE, &lightcolordiff.x);
	myglLightv( GL_LIGHT0, GL_SPECULAR, &lightcolorspec.x);

	myglMaterial(GL_FRONT_AND_BACK,GL_SHININESS, shininess);
	myglMaterialv(GL_FRONT_AND_BACK,GL_SPECULAR, &lightcolorspec.x);

	/* General OpenGL Setup */

	// smoothing polygons
	glShadeModel( GL_SMOOTH );

	// polygon offset for shadow to avoid ZFighting between the shadow and floor
	myglPolygonOffset(OFFSETA,OFFSETB);

	/* PBuffer Init Setup */


	if (gEnable_PBuffer_Usage)
	{
		EGLConfig eglConfig = SelectEGLConfig();
		EGLint list[9];

		list[0]=EGL_WIDTH;
		list[1]=PBUFFERSIZE;
		list[2]=EGL_HEIGHT;
		list[3]=PBUFFERSIZE;
		list[4]=EGL_NONE;

#ifdef GL_OES_VERSION_1_1
		if(gEnable_PBuffer_Bind_Texture)
		{
			list[4]=EGL_TEXTURE_TARGET;
			list[5]=EGL_TEXTURE_2D;
			list[6]=EGL_TEXTURE_FORMAT;
			list[7]=EGL_TEXTURE_RGB;
			list[8]=EGL_NONE;
		}
#endif // GL_OES_VERSION_1_1

		MyPBufSurface = eglCreatePbufferSurface(eglGetCurrentDisplay(), eglConfig, list);
		if(MyPBufSurface==EGL_NO_SURFACE)
		{
			PVRShellOutputDebug("InitView: Failed to create pbuffer!\n");
			return false;
		}
	}

	/* Get and Store current Display, Context and Surface - required to toggle between PBuffer and Main Render Surface */
	CurrentDisplay = eglGetCurrentDisplay();
	if(CurrentDisplay == EGL_NO_DISPLAY)
	{
		return false;
	}

	CurrentContext = eglGetCurrentContext();
	if (CurrentContext == EGL_NO_CONTEXT)
	{
		return false;
	}

	CurrentSurface = eglGetCurrentSurface(EGL_DRAW);
	if (CurrentSurface == EGL_NO_SURFACE)
	{
		return false;
	}

	/* Setup floor plane for projected shadow calculations. */
	findPlane(floorPlane, floorVertices[1], floorVertices[2], floorVertices[3]);

	// Create Texture ID
	glGenTextures(1,&MyPBufferTexture);

	// Bind and Enable Texture ID
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,MyPBufferTexture);
	glEnable(GL_TEXTURE_2D);

	// If Tex Params are not set glCopyTexImage2D will fail !
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	// Initialise the Texture
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,PBUFFERSIZE,PBUFFERSIZE,0,GL_RGB,GL_UNSIGNED_BYTE,0);

	/* Init Time related variables */
	OldTime = PVRShellGetTime();
	fFactor = 1.0f;
	fOldFactor = 1.0f;
	fOlderFactor = 1.0f;

	return true;
}

/*******************************************************************************
 * Function Name  : ReleaseView
 * Returns        : TRUE if no error occured
 * Description    : App / Shell Interface
 *
 *******************************************************************************/
bool OGLESShadowTechniques::ReleaseView()
{
	/* Release All Textures : Print3D, PBuffer, Loaded from Header */
	AppPrint3D.ReleaseTextures();

	glDeleteTextures(1,&MyPBufferTexture);

	PVRTReleaseTexture(gMonsterMap);
	PVRTReleaseTexture(gFloorMap);
	PVRTReleaseTexture(gBlobMap);
	PVRTReleaseTexture(gLightSourceMap);
	PVRTReleaseTexture(gCrateMap);

	if (gEnable_PBuffer_Usage)
	{
		eglDestroySurface(CurrentDisplay,MyPBufSurface);
	}
	return true;
}


/*******************************************************************************
 * Function Name  : RenderScene
 * Returns		  : TRUE if no error occured
 * Description    : Main function of the program
 *******************************************************************************/

bool OGLESShadowTechniques::RenderScene()
{
	unsigned long CurrTime;

	/* Update Light Position and Rotation Params */
	UpdateLightPosition();

	/* If required do Render 2 Texture */
	if((gnCurrentMode==R2TEXMODE) && ((gnPBufferUpdateCounter%PBUFFERUPDATESKIP)==0))
	{
		/* Setup PBuffer Render or alternatively use Back Buffer*/
		if (gEnable_PBuffer_Usage)
		{
#ifdef GL_OES_VERSION_1_1
			if(gEnable_PBuffer_Bind_Texture)
			{
				glBindTexture(GL_TEXTURE_2D,MyPBufferTexture);
				eglReleaseTexImage(CurrentDisplay, MyPBufSurface, EGL_BACK_BUFFER);
			}
#endif
			if (!eglMakeCurrent(CurrentDisplay, MyPBufSurface, MyPBufSurface, CurrentContext))
			{
				PVRShellOutputDebug("Unable to make the pbuffer context current");
				return false;
			}
		}

		/* Set Viewport and do a Clear */
		glViewport (0, 0, PBUFFERSIZE, PBUFFERSIZE);
		myglClearColor(f2vt(1), f2vt(1), f2vt(1), f2vt(1));
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		/* Setup Matrices to render from the point of view of the lightsource */
        SetupMatrices(LIGHT);

		glEnable(GL_DEPTH_TEST);

		// Disable Texture
		glActiveTexture(GL_TEXTURE0);
		glDisable(GL_TEXTURE_2D);

		// Set the Shadow Color and Alpha
		myglColor4(f2vt(0.25f), f2vt(0.25f), f2vt(0.25f), f2vt(0.0f));

		// Render the objects into the texture
		RenderMD2( &MonsterObj,bAnimated ? MyTime : 0.0f, false, false, false, false, MonsterTriangleList,USE_FANS_N_STRIPS );

#ifdef GL_OES_VERSION_1_1
		if(gEnable_PBuffer_Bind_Texture)
		{
			glBindTexture(GL_TEXTURE_2D, MyPBufferTexture);
			eglBindTexImage(CurrentDisplay, MyPBufSurface, EGL_BACK_BUFFER);
		}
		else
#endif
		{
			/* Make the copy using the pbuffer as source ... or backbuffer if pbuffer is disabled */
			glActiveTexture(GL_TEXTURE0);
			glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D,MyPBufferTexture);

			glCopyTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 0, 0, PBUFFERSIZE, PBUFFERSIZE, 0);
		}

		if (gEnable_PBuffer_Usage)
		{
			/* Switch back to the backbuffer for rendering etc. */
			if (!eglMakeCurrent(CurrentDisplay, CurrentSurface, CurrentSurface, CurrentContext))
			{
				PVRShellOutputDebug("Unable to make the main context current");
				return false;
			}
		}
	}

	/* Keep track of the PBuffer updates */
	gnPBufferUpdateCounter++;

	/* Setup and Clear for the backbuffer */
	glViewport(0,0,PVRShellGet(prefWidth),PVRShellGet(prefHeight));
	myglClearColor(f2vt(0), f2vt(0), f2vt(0), f2vt(1));
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/* Setup the Matrices to render from the Camera point of view */
	SetupMatrices(CAMERA);

	glEnable(GL_DEPTH_TEST);

	if (ENABLE_GL_LIGHTING)
	{
		glEnable(GL_LIGHTING);
	}

	if (ENABLE_TEXTURES)
	{
		glEnable(GL_TEXTURE_2D);
	}
	else
	{
		glDisable(GL_TEXTURE_2D);
	}

	myglColor4(f2vt(1.0f), f2vt(1.0f), f2vt(1.0f), f2vt(1.0f));

	/* Draw the Monster */
	/* Set Texture*/
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,gMonsterMap);

	/* Draw Geometry */
	RenderMD2(&MonsterObj,bAnimated ? MyTime : 0.0f, bColorEnable, bNormalEnable, bTexCoordEnable, (gnCurrentMode==ADVANCEDBLOBMODE) ? true:false, MonsterTriangleList,USE_FANS_N_STRIPS );

	/* Draw the Environment and Shadows */

	/* Draw the Floor */

	glDisable(GL_LIGHTING);

	if(gnCurrentMode==R2TEXMODE)
	{
		/* Set correct Texture */
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, MyPBufferTexture);

		/* Draw Geometry with Projected Shadow Texture Layer */

		/* Setup Second Layer for Texturing Floor */
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D,gFloorMap);
		glEnable(GL_TEXTURE_2D);

		/* Draw Floor Geometry Dual Textured */
        DrawProjectedFloor();

		/* Setup Second Layer for Texturing Crates */
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D,gCrateMap);
		glEnable(GL_TEXTURE_2D);

		/* Draw Crates Geometry Dual Textured */
		DrawProjectedMeshes();

		/* Disable Second Texture Layer */
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D,0);
		glDisable(GL_TEXTURE_2D);
	}
	else
	{
		/* Set correct Texture */
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, gFloorMap);

		/* Draw Geometry: Crates and Floor */
		DrawFloor();

		/* Set Correct Texture */
		glBindTexture(GL_TEXTURE_2D,gCrateMap);
		DrawMeshes();
	}

	/* Render Different Shadow Types */
	if (gnCurrentMode==ADVANCEDBLOBMODE)
	{
		/* Setup work for Projecting the Shadow */
		SetupMatrices(PROJGEOM);

		/* Draw Shadow*/
		DrawAdvancedBlobShadow();

	} else 	if (gnCurrentMode==PROJGEOMMODE)
	{
		/* Setup work for Projecting the Shadow */
		SetupMatrices(PROJGEOM);

		/* Draw Shadow */
		DrawProjectedShadow();

	} else 	if(gnCurrentMode==BASEBLOBMODE)
	{
		/* Draw Geometry */
		DrawBaseBlob();
	}

	/* Update Time for animation */

	/* Grab Current Time in ms */
	CurrTime = PVRShellGetTime();

	/* Calculate a Time Scale Factor using the Current and Old Time */
	fFactor = (((float)(CurrTime-OldTime))*0.03333f);

	/* Average out the Factor Change to avoid spike behaviour */
	fFactor = 0.3333f*(fOlderFactor+fOldFactor+fFactor);
	fOlderFactor = fOldFactor;
	fOldFactor = fFactor;

	/* Update the actual Animation Time used by the drawing routines */
	MyTime+=(ANIMATIONSTEP*fFactor);

	/* Store Current Time as Old Time for Next Frame */
	OldTime = CurrTime;

	/* Draw Light Source with Alpha Blending Enabled at the end - no lighting */

	if (ENABLE_GL_LIGHTING)
	{
		glDisable(GL_LIGHTING);
	}

	/* Setup Matrices to render from the point of view of the camera */
	SetupMatrices(CAMERA);

	/* Enable and Setup Blend Mode */
	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE,GL_ONE);

	/* Draw Billboard geometry */
	DrawBillBoardQuad(MyFrom, MyTo, VERTTYPEMUL(f2vt(0.75f),vCurrentLightPos.x), VERTTYPEMUL(f2vt(0.75f),vCurrentLightPos.y), VERTTYPEMUL(f2vt(0.75f),vCurrentLightPos.z), f2vt(1.0f), gLightSourceMap);

	/* Disable Blending*/
	glDisable(GL_BLEND);

	if (glGetError())
	{
		_RPT0(_CRT_WARN,"glError Tracker : Error prior to Print3D Call found !\n");
	}

	char strTechnique[1024];
	char strBufferUsage[1024];
	char strTotal[1024];
	/* Display info text. */
	switch(gnCurrentMode)
	{
		case BASEBLOBMODE:		strcpy(strTechnique, "Basic Static Blob");
								break;
		case ADVANCEDBLOBMODE:	strcpy(strTechnique, "Dynamic Blob");
								break;
		case PROJGEOMMODE:		strcpy(strTechnique, "Projected Geometry");
								break;
		case R2TEXMODE:			strcpy(strTechnique, "Projected Render");
								break;
	}
	if (gEnable_PBuffer_Bind_Texture) strcpy(strBufferUsage, "Texture");
	if (!gEnable_PBuffer_Bind_Texture && gEnable_PBuffer_Usage) strcpy(strBufferUsage, "PBuffer");
	if (!gEnable_PBuffer_Bind_Texture && !gEnable_PBuffer_Usage) strcpy(strBufferUsage, "Back Buffer");

	if (gnCurrentMode==R2TEXMODE)
	{
		sprintf(strTotal, "%s (%s)", strTechnique, strBufferUsage);
	}
	else
	{
		sprintf(strTotal, "%s", strTechnique);
	}
	AppPrint3D.DisplayDefaultTitle("ShadowTechniques", strTotal, PVR_LOGO);

	AppPrint3D.Flush();

	return true;
}

/*******************************************************************************
 * Function Name  : shadowMatrix
 * Inputs		  : GroundPlane Equation and Lightposition
 * Outputs		  : Matrix
 * Description    : Create a matrix that will project the desired shadow.
 *******************************************************************************/
void OGLESShadowTechniques::shadowMatrix(PVRTMATRIX &shadowMat, const PVRTVECTOR4 &groundplane, const PVRTVECTOR4 &lightpos)
{
	VERTTYPE dot;

	/* Find dot product between light position vector and ground plane normal. */
	dot =
		VERTTYPEMUL(groundplane.x, lightpos.x) +
		VERTTYPEMUL(groundplane.y, lightpos.y) +
		VERTTYPEMUL(groundplane.z, lightpos.z) +
		VERTTYPEMUL(groundplane.w, lightpos.w);

	shadowMat.f[ 0] = dot - VERTTYPEMUL(lightpos.x, groundplane.x);
	shadowMat.f[ 4] = 0   - VERTTYPEMUL(lightpos.x, groundplane.y);
	shadowMat.f[ 8] = 0   - VERTTYPEMUL(lightpos.x, groundplane.z);
	shadowMat.f[12] = 0   - VERTTYPEMUL(lightpos.x, groundplane.w);

	shadowMat.f[ 1] = 0   - VERTTYPEMUL(lightpos.y, groundplane.x);
	shadowMat.f[ 5] = dot - VERTTYPEMUL(lightpos.y, groundplane.y);
	shadowMat.f[ 9] = 0   - VERTTYPEMUL(lightpos.y, groundplane.z);
	shadowMat.f[13] = 0   - VERTTYPEMUL(lightpos.y, groundplane.w);

	shadowMat.f[ 2] = 0   - VERTTYPEMUL(lightpos.z, groundplane.x);
	shadowMat.f[ 6] = 0   - VERTTYPEMUL(lightpos.z, groundplane.y);
	shadowMat.f[10] = dot - VERTTYPEMUL(lightpos.z, groundplane.z);
	shadowMat.f[14] = 0   - VERTTYPEMUL(lightpos.z, groundplane.w);

	shadowMat.f[ 3] = 0   - VERTTYPEMUL(lightpos.w, groundplane.x);
	shadowMat.f[ 7] = 0   - VERTTYPEMUL(lightpos.w, groundplane.y);
	shadowMat.f[11] = 0   - VERTTYPEMUL(lightpos.w, groundplane.z);
	shadowMat.f[15] = dot - VERTTYPEMUL(lightpos.w, groundplane.w);
}

/*******************************************************************************
 * Function Name  : findPlane
 * Inputs		  : 3 Points
 * Outputs		  : Plane Equations
 * Description    : Find the plane equation given 3 points.
 *******************************************************************************/
void OGLESShadowTechniques::findPlane(PVRTVECTOR4 &plane, const PVRTVECTOR3 &v0, const PVRTVECTOR3 &v1, const PVRTVECTOR3 &v2)
{
  PVRTVECTOR3 vec0, vec1;

  /* Need 2 vectors to find cross product. */
  vec0.x = v1.x - v0.x;
  vec0.y = v1.y - v0.y;
  vec0.z = v1.z - v0.z;

  PVRTMatrixVec3Normalize(vec0, vec0);

  vec1.x = v2.x - v0.x;
  vec1.y = v2.y - v0.y;
  vec1.z = v2.z - v0.z;

  PVRTMatrixVec3Normalize(vec1, vec1);

  /* find cross product to get A, B, and C of plane equation */
  plane.x = VERTTYPEMUL(vec0.y, vec1.z) - VERTTYPEMUL(vec0.z, vec1.y);
  plane.y = -(VERTTYPEMUL(vec0.x, vec1.z) - VERTTYPEMUL(vec0.z, vec1.x));
  plane.z = VERTTYPEMUL(vec0.x, vec1.y) - VERTTYPEMUL(vec0.y, vec1.x);

  plane.w = -(VERTTYPEMUL(plane.x, v0.x) + VERTTYPEMUL(plane.y, v0.y) + VERTTYPEMUL(plane.z, v0.z));
}

/*******************************************************************************
 * Function Name  : RenderMD2
 * Inputs		  : Model data and a whole bunch of options (color, normals, texcoord, compute bounding box enables and render mode)
 * Description    : Draw an MD2 model using the options provided
 *******************************************************************************/
void OGLESShadowTechniques::RenderMD2(CMD2Model *Model, float time, bool bColorEnable, bool bNormalEnable, bool bTexCoordEnable, bool bComputeBBox, unsigned short* Indices, bool bFanStripMode)		// Render the model using DrawArrays - Fill Memory with Data and Render
{
	VERTTYPE *pPosition;
	VERTTYPE *pTexCoord;
	VERTTYPE *pNormal;
	unsigned char *pColor;
	int NumberOfFans;
	int NumberOfStrips;
	int Offset2Strips;
	int *pFanLengths;
	int *pStripLengths;

	int CurrentIndex = 0;
	int j;

	/* rotate the MD2 Model so its upright - model is stored in a non traditional orientation */
	glPushMatrix();
	myglRotate( f2vt(-90.0), f2vt(1.0), f2vt(0.0), f2vt(0.0) );
	myglRotate( f2vt(90.0), f2vt(0.0), f2vt(0.0), f2vt(1.0) );

	/* Generate and Grab the Data */
	#if(OGLESLITE)
	{
		Model->GetModelDataPntrsFixed(time, &pPosition, bTexCoordEnable ? &pTexCoord:NULL, bNormalEnable ? &pNormal:NULL, bColorEnable ? &pColor:NULL, &NumberOfFans, &NumberOfStrips, &Offset2Strips, &pFanLengths, &pStripLengths);
	}
	#else
	{
		Model->GetModelDataPntrsFloat(time, &pPosition, bTexCoordEnable ? &pTexCoord:NULL, bNormalEnable ? &pNormal:NULL, bColorEnable ? &pColor:NULL, &NumberOfFans, &NumberOfStrips, &Offset2Strips, &pFanLengths, &pStripLengths);
	}
	#endif

	if (bComputeBBox)
	{
		GetBoundingEllips(pPosition, Model->GetNumberOfVertices());
	}

	/* Setup some render states */
	glFrontFace( GL_CW );

	/* Enable BackFace Culling */
	glEnable( GL_CULL_FACE );
	glCullFace( GL_BACK );

	/* Draw the Model */
	/* Enable the Client States and set Data Pointers as required by the options */
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3,VERTTYPEENUM,0,pPosition);

	if (bColorEnable)
	{
		glEnableClientState(GL_COLOR_ARRAY);
		glColorPointer(4,GL_UNSIGNED_BYTE,0,pColor);
	}

	if (bNormalEnable)
	{
		glEnableClientState(GL_NORMAL_ARRAY);
		glNormalPointer(VERTTYPEENUM,0,pNormal);
	}

	if (bTexCoordEnable)
	{
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glTexCoordPointer(2,VERTTYPEENUM,0,pTexCoord);
	}

	/* Draw the object either using fans and strips or using indexed triangles */
	if (bFanStripMode)
	{
		for (j=0;j<NumberOfFans; j++)
		{
			glDrawArrays(GL_TRIANGLE_FAN,CurrentIndex,pFanLengths[j]);
			CurrentIndex+=pFanLengths[j];
		}

		/* Draw Strips */
		for (j=0;j<NumberOfStrips; j++)
		{
			glDrawArrays(GL_TRIANGLE_STRIP,CurrentIndex,pStripLengths[j]);
			CurrentIndex+=pStripLengths[j];
		}
	}
	else
	{
        glDrawElements(GL_TRIANGLES,Model->TriangleCount*3,GL_UNSIGNED_SHORT,Indices);
	}

	glDisableClientState(GL_VERTEX_ARRAY);

	if (bColorEnable)
	{
		glDisableClientState(GL_COLOR_ARRAY);
	}

	if (bNormalEnable)
	{
		glDisableClientState(GL_NORMAL_ARRAY);
	}

	if (bTexCoordEnable)
	{
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	}

	/* Recover state */
	glDisable( GL_CULL_FACE );
	glPopMatrix();

}

/*******************************************************************************
 * Function Name  : DrawFloor
 * Inputs		  : None
 * Description    : Draw Simple Floor Object
 *******************************************************************************/
void OGLESShadowTechniques::DrawFloor()
{
	static VERTTYPE	Vertices[] = {
			f2vt(-FLOORSIZE)	, f2vt(-FLOORHEIGHT)	, f2vt(-FLOORSIZE),
			f2vt(FLOORSIZE)		, f2vt(-FLOORHEIGHT)	, f2vt(-FLOORSIZE),
			f2vt(-FLOORSIZE)	, f2vt(-FLOORHEIGHT)	, f2vt(FLOORSIZE),
	 		f2vt(FLOORSIZE)		, f2vt(-FLOORHEIGHT)	, f2vt(FLOORSIZE)
		};

	static VERTTYPE	Colours[] = {
			f2vt(1.0f), f2vt(1.0f), f2vt(1.0f), f2vt(1.0f),
			f2vt(1.0f), f2vt(1.0f), f2vt(1.0f), f2vt(1.0f),
			f2vt(1.0f), f2vt(1.0f), f2vt(1.0f), f2vt(1.0f),
	 		f2vt(1.0f), f2vt(1.0f), f2vt(1.0f), f2vt(1.0f)
		};

	static VERTTYPE	UVs[] = {
			f2vt(0.0f), f2vt(0.0f),
			f2vt(1.0f), f2vt(0.0f),
			f2vt(0.0f), f2vt(1.0f),
	 		f2vt(1.0f), f2vt(1.0f)
		};

	VERTTYPE *pVertices = Vertices;
	VERTTYPE *pColours  = Colours;
	VERTTYPE *pUV       = UVs;

	/* Enable Client States and Set Data Pointers */
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3,VERTTYPEENUM,0,pVertices);

	glEnableClientState(GL_COLOR_ARRAY);
	glColorPointer(4,VERTTYPEENUM,0,pColours);

	glClientActiveTexture(GL_TEXTURE0);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2,VERTTYPEENUM,0,pUV);

	/* Draw Geometry */
	glDrawArrays(GL_TRIANGLE_STRIP,0,4);

	/* Disable Client States */
	glDisableClientState(GL_VERTEX_ARRAY);

	glDisableClientState(GL_COLOR_ARRAY);

	glClientActiveTexture(GL_TEXTURE0);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}

/*******************************************************************************
 * Function Name  : DrawProjectedFloor
 * Inputs		  : None
 * Description    : Draw Floor Object with projected texture layer
 *******************************************************************************/
void OGLESShadowTechniques::DrawProjectedFloor()
{
	static VERTTYPE	Vertices[] = {
			f2vt(-FLOORSIZE)	, f2vt(-FLOORHEIGHT)	, f2vt(-FLOORSIZE),
			f2vt(FLOORSIZE)		, f2vt(-FLOORHEIGHT)	, f2vt(-FLOORSIZE),
			f2vt(-FLOORSIZE)	, f2vt(-FLOORHEIGHT)	, f2vt(FLOORSIZE),
	 		f2vt(FLOORSIZE)		, f2vt(-FLOORHEIGHT)	, f2vt(FLOORSIZE)
		};

	static VERTTYPE	Colours[] = {
			f2vt(1.0f), f2vt(1.0f), f2vt(1.0f), f2vt(1.0f),
			f2vt(1.0f), f2vt(1.0f), f2vt(1.0f), f2vt(1.0f),
			f2vt(1.0f), f2vt(1.0f), f2vt(1.0f), f2vt(1.0f),
	 		f2vt(1.0f), f2vt(1.0f), f2vt(1.0f), f2vt(1.0f)
		};

	static VERTTYPE	UV[] = {
			f2vt(-FLOORSIZE)	, f2vt(-FLOORHEIGHT)	, f2vt(-FLOORSIZE), f2vt(1.0f),
			f2vt(FLOORSIZE)		, f2vt(-FLOORHEIGHT)	, f2vt(-FLOORSIZE), f2vt(1.0f),
			f2vt(-FLOORSIZE)	, f2vt(-FLOORHEIGHT)	, f2vt(FLOORSIZE), f2vt(1.0f),
	 		f2vt(FLOORSIZE)		, f2vt(-FLOORHEIGHT)	, f2vt(FLOORSIZE), f2vt(1.0f)
		};

	static VERTTYPE	UVSecond[] = {
			f2vt(0.0f), f2vt(0.0f),
			f2vt(1.0f), f2vt(0.0f),
			f2vt(0.0f), f2vt(1.0f),
	 		f2vt(1.0f), f2vt(1.0f)
		};

	VERTTYPE *pVertices = Vertices;
	VERTTYPE *pColours  = Colours;
	VERTTYPE *pUV		= UV;
	VERTTYPE *pUVSecond = UVSecond;

	/* Enable Client States and Set Data Pointers */
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3,VERTTYPEENUM,0,pVertices);

	glEnableClientState(GL_COLOR_ARRAY);
	glColorPointer(4,VERTTYPEENUM,0,pColours);

	glClientActiveTexture(GL_TEXTURE1);
	glActiveTexture(GL_TEXTURE1);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2,VERTTYPEENUM,0,pUVSecond);
	glMatrixMode(GL_TEXTURE);
	glLoadIdentity();

	glClientActiveTexture(GL_TEXTURE0);
	glActiveTexture(GL_TEXTURE0);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(4,VERTTYPEENUM,0,pUV);

	/* Setup Matrix for Projection */
	SetupMatrices(PROJTEX);

	/* Draw Geometry */
	glDrawArrays(GL_TRIANGLE_STRIP,0,4);

	/* Disable Client States */
	glDisableClientState(GL_VERTEX_ARRAY);

	glDisableClientState(GL_COLOR_ARRAY);

	glClientActiveTexture(GL_TEXTURE1);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);

	glClientActiveTexture(GL_TEXTURE0);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);

	/* Disable Texture Matrix */
	glMatrixMode(GL_TEXTURE);
	glLoadIdentity();

}

/*******************************************************************************
 * Function Name  : DrawProjectedMeshes
 * Inputs		  : None
 * Description    : Draw Objects with projected texture layer
 *******************************************************************************/
void OGLESShadowTechniques::DrawProjectedMeshes()
{
	for(int i = 0; i < NUM_MESHES; i++)
	{
		VERTTYPE *pVertices = Meshes[i]->pVertex;
		VERTTYPE *pUV		= Meshes[i]->pUV;
		VERTTYPE *pUVSecond = Meshes[i]->pVertex;

		/* Enable Client States and Setup Data Pointers */
		glEnableClientState(GL_VERTEX_ARRAY);
		glVertexPointer(3,VERTTYPEENUM,0,pVertices);

		glClientActiveTexture(GL_TEXTURE1);
		glActiveTexture(GL_TEXTURE1);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glTexCoordPointer(2,VERTTYPEENUM,0,pUV);
		glMatrixMode(GL_TEXTURE);
		glLoadIdentity();

		glClientActiveTexture(GL_TEXTURE0);
		glActiveTexture(GL_TEXTURE0);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glTexCoordPointer(3,VERTTYPEENUM,0,pUVSecond);

		/* Setup Texture Matrix for Projection */
		SetupMatrices(PROJTEX);

		/* Draw Geometry */
		glDrawElements(GL_TRIANGLES,Meshes[i]->nNumFaces*3,GL_UNSIGNED_SHORT,Meshes[i]->pFaces);

		/* Disable Client States */
		glDisableClientState(GL_VERTEX_ARRAY);

		glClientActiveTexture(GL_TEXTURE1);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);

		glClientActiveTexture(GL_TEXTURE0);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);

		/* Disable Texture Matrix */
		glMatrixMode(GL_TEXTURE);
		glLoadIdentity();
	}
}

/*******************************************************************************
 * Function Name  : DrawMeshes
 * Inputs		  : None
 * Description    : Draw Objects
 *******************************************************************************/
void OGLESShadowTechniques::DrawMeshes()
{
	for(int i = 0; i < NUM_MESHES; i++)
	{
		VERTTYPE *pVertices = Meshes[i]->pVertex;
		VERTTYPE *pUV		= Meshes[i]->pUV;

		/* Enable Client States and Setup Data Pointers */
		glEnableClientState(GL_VERTEX_ARRAY);
		glVertexPointer(3,VERTTYPEENUM,0,pVertices);

		glClientActiveTexture(GL_TEXTURE0);
		glActiveTexture(GL_TEXTURE0);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glTexCoordPointer(2,VERTTYPEENUM,0,pUV);
		glMatrixMode(GL_TEXTURE);
		glLoadIdentity();

		/* Draw Geometry */
		glDrawElements(GL_TRIANGLES,Meshes[i]->nNumFaces*3,GL_UNSIGNED_SHORT,Meshes[i]->pFaces);

		/* Disable Client States */
		glDisableClientState(GL_VERTEX_ARRAY);

		glClientActiveTexture(GL_TEXTURE0);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	}
}

/*******************************************************************************
 * Function Name  : DrawBlob
 * Inputs		  : None
 * Description    : Draw Blob Object
 *******************************************************************************/
void OGLESShadowTechniques::DrawBlob()
{
	/* Blob Data based on bounding box data and orientation based on position of the light */
	float lightdirX = -0.9f*(float)-PVRTFCOS(atan2(vt2f(vCurrentLightPos.z), vt2f(vCurrentLightPos.x)));
	float lightdirZ = -0.9f*(float)-PVRTFSIN(atan2(vt2f(vCurrentLightPos.z), vt2f(vCurrentLightPos.x)));

	float perplightdirX = (float)PVRTFCOS(atan2(vt2f(vCurrentLightPos.z), vt2f(vCurrentLightPos.x))+PI/2);
	float perplightdirZ = (float)PVRTFSIN(atan2(vt2f(vCurrentLightPos.z), vt2f(vCurrentLightPos.x))+PI/2);

	VERTTYPE	Vertices[] = {
			f2vt(Radius*(lightdirX-perplightdirX))	, f2vt(YMin)	, f2vt(Radius*(lightdirZ-perplightdirZ)),
			f2vt(Radius*(lightdirX+perplightdirX))	, f2vt(YMin)	, f2vt(Radius*(lightdirZ+perplightdirZ)),
			f2vt(Radius*(lightdirX-perplightdirX))	, f2vt(YMax)	, f2vt(Radius*(lightdirZ-perplightdirZ)),
	 		f2vt(Radius*(lightdirX+perplightdirX))	, f2vt(YMax)	, f2vt(Radius*(lightdirZ+perplightdirZ))
		};

	static VERTTYPE	UVs[] = {
			f2vt(0.0f), f2vt(0.0f),
			f2vt(1.0f), f2vt(0.0f),
			f2vt(0.0f), f2vt(1.0f),
	 		f2vt(1.0f), f2vt(1.0f)
		};

	VERTTYPE *pVertices = Vertices;
	VERTTYPE *pUV       = UVs;

	/* Enable Client States and Setup Data Pointers */
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3,VERTTYPEENUM,0,pVertices);

	glClientActiveTexture(GL_TEXTURE0);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2,VERTTYPEENUM,0,pUV);

	/* Draw Geometry */
	glDrawArrays(GL_TRIANGLE_STRIP,0,4);

	/* Disable Client States */
	glDisableClientState(GL_VERTEX_ARRAY);

	glClientActiveTexture(GL_TEXTURE0);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);

}

/*******************************************************************************
 * Function Name  : GetBoundingEllips
 * Inputs		  : Vertex List and Number of Vertices in the List
 * Description    : Calculate a bounding ellips based on the vertices in the list
 *******************************************************************************/
void OGLESShadowTechniques::GetBoundingEllips(VERTTYPE *Vertices, int nNumOfVertices)
{
	/* Allocate and Init some vars */
	int i;
	float CenterX = 0, CenterY = 0, CenterZ = 0;

	float XMinTmp=9999999.99f;
	float XMaxTmp=-9999999.99f;

	float YMinTmp=9999999.99f;
	float YMaxTmp=-9999999.99f;

	float ZMinTmp=9999999.99f;
	float ZMaxTmp=-9999999.99f;

	float OverallMinTmp=9999999.99f;
	float OverallMaxTmp=-9999999.99f;

	float tmpMax;

	float TempVtxZero, TempVtxOne, TempVtxTwo;

	/* Run through Vertices finding the required data */
	for (i=0; i<nNumOfVertices; i++)
	{
		TempVtxZero = vt2f(Vertices[i*3+0]);
		TempVtxOne  = vt2f(Vertices[i*3+1]);
		TempVtxTwo  = vt2f(Vertices[i*3+2]);

		CenterX+=TempVtxZero;
		CenterY+=TempVtxOne;
		CenterZ+=TempVtxTwo;


		if (TempVtxZero>XMaxTmp)
		{
			XMaxTmp=TempVtxZero;
		}

		if (TempVtxZero<XMinTmp)
		{
			XMinTmp=TempVtxZero;
		}

		if (TempVtxOne>YMaxTmp)
		{
			YMaxTmp=TempVtxOne;
		}

		if (TempVtxOne<YMinTmp)
		{
			YMinTmp=TempVtxOne;
		}

		if (TempVtxTwo>ZMaxTmp)
		{
			ZMaxTmp=TempVtxTwo;
		}

		if (TempVtxTwo<ZMinTmp)
		{
			ZMinTmp=TempVtxTwo;
		}

		tmpMax=max(TempVtxZero,TempVtxOne);
		tmpMax=max(tmpMax,TempVtxTwo);

		if (tmpMax>OverallMaxTmp)
		{
			OverallMaxTmp=tmpMax;
		}

		tmpMax=min(TempVtxZero,TempVtxOne);
		tmpMax=min(tmpMax,TempVtxTwo);

		if (tmpMax<OverallMinTmp)
		{
			OverallMinTmp=tmpMax;
		}
	}

	/* Store the results */
	Radius=(float)max((fabs(XMaxTmp)+fabs(XMinTmp))*0.5f,(fabs(YMaxTmp)+fabs(YMinTmp))*0.5f);
	YMin=ZMinTmp;
	YMax=ZMaxTmp;//fabs(ZMaxTmp)*2.0f;
}

/*******************************************************************************
 * Function Name  : DrawAdvancedBlobShadow
 * Inputs		  : None
 * Description    : Draw Advanced Bob Shadow - this is a dynamic shadow based on bounding info.
 *******************************************************************************/
void OGLESShadowTechniques::DrawAdvancedBlobShadow()
{
	// Enable Polygon offset to avoid ZFighting between floor and shadow
	glEnable(GL_POLYGON_OFFSET_FILL);

	// Enable Blending for Transparent Blob
	glEnable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	// Bind Blob Texture
	glActiveTexture(GL_TEXTURE0);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,gBlobMap);

	// Set Base Blend color to influence how transparent the shadow is
	myglColor4(f2vt(0.0f), f2vt(0.0f), f2vt(0.0f), f2vt(0.5f));

	// Draw the bounding box blob which gets flattened into the floor
	DrawBlob();

	// Disable blending
	glDisable (GL_BLEND);

	// Disable Polygon offset to avoid ZFighting between floor and shadow
	glDisable(GL_POLYGON_OFFSET_FILL);
}

/*******************************************************************************
 * Function Name  : DrawProjectedShadow
 * Inputs		  : None
 * Description    : Draw Projected Geometry Shadow
 *******************************************************************************/
void OGLESShadowTechniques::DrawProjectedShadow()
{
	// Enable Polygon offset to avoid ZFighting between floor and shadow
	glEnable(GL_POLYGON_OFFSET_FILL);

	// Disable Blending since alpha blend does not work with projection
	glDisable (GL_BLEND);

	// Disable Texture
	glDisable(GL_TEXTURE_2D);

	// Set the Shadow Color and Alpha
	myglColor4(f2vt(0.0f), f2vt(0.0f), f2vt(0.0f), f2vt(0.0f));

	// Render the objects which will be slammed into the floor plane
	RenderMD2(&MonsterObj,bAnimated ? MyTime : 0.0f, false, false, false, false, MonsterTriangleList, USE_FANS_N_STRIPS );

	// Disable Polygon offset to avoid ZFighting between floor and shadow
	glDisable(GL_POLYGON_OFFSET_FILL);
}

/*******************************************************************************
 * Function Name  : DrawBaseBlob
 * Inputs		  : None
 * Description    : Draw Static Blob Shadow underneath object
 *******************************************************************************/
void OGLESShadowTechniques::DrawBaseBlob()
{
	static VERTTYPE	Vertices[] = {
			f2vt(-CHARWIDTH)	, f2vt(-FLOORHEIGHT)	, f2vt(-CHARWIDTH),
			f2vt(CHARWIDTH)		, f2vt(-FLOORHEIGHT)	, f2vt(-CHARWIDTH),
			f2vt(-CHARWIDTH)	, f2vt(-FLOORHEIGHT)	, f2vt(CHARWIDTH),
	 		f2vt(CHARWIDTH)		, f2vt(-FLOORHEIGHT)	, f2vt(CHARWIDTH)
		};

	static VERTTYPE	UVs[] = {
			f2vt(0.0f), f2vt(0.0f),
			f2vt(1.0f), f2vt(0.0f),
			f2vt(0.0f), f2vt(1.0f),
	 		f2vt(1.0f), f2vt(1.0f)
		};

	// Enable Polygon offset to avoid ZFighting between floor and shadow
	glEnable(GL_POLYGON_OFFSET_FILL);

	// Enable Blending for Transparent Blob
	glEnable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// Setup Matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	myglLoadMatrix(MyRotViewMatrix.f);

	// Bind Blob Texture
	glActiveTexture(GL_TEXTURE0);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,gBlobMap);

	// Set Base Blend color to influence how transparent the shadow is
	myglColor4(f2vt(0.0f), f2vt(0.0f), f2vt(0.0f), f2vt(0.5f));

	// Draw Blob - in this case the object is "static" so blob position is "static" as well
	// In a Game the Blob position would be calculated from the Character Position.

	VERTTYPE *pVertices = ( (VERTTYPE*)&Vertices );
	VERTTYPE *pUV       = ( (VERTTYPE*)&UVs );

	/* Enable Client States and Setup Data Pointers */
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3,VERTTYPEENUM,0,pVertices);

	glClientActiveTexture(GL_TEXTURE0);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2,VERTTYPEENUM,0,pUV);

	/* Draw Geometry */
	glDrawArrays(GL_TRIANGLE_STRIP,0,4);

	/* Disable Client States */
	glDisableClientState(GL_VERTEX_ARRAY);

	glClientActiveTexture(GL_TEXTURE0);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);

	// Disable blending
	glDisable (GL_BLEND);

	// Disable Polygon offset to avoid ZFighting between floor and shadow
	glDisable(GL_POLYGON_OFFSET_FILL);
}

/*******************************************************************************
 * Function Name  : SelectEGLConfig
 * Inputs		  : Mode Selection
 * Description    : Finds an EGL config with required options based on Mode Requested - for PBuffer
 *******************************************************************************/
EGLConfig OGLESShadowTechniques::SelectEGLConfig()
{
    EGLint num_config;
    EGLint conflist[32];
    int i;
	EGLConfig  gEglConfig;

    i = 0;

	/* Setup required options : essential is the PBuffer Bit */
    conflist[i++] = EGL_CONFIG_CAVEAT;
    conflist[i++] = EGL_NONE;

	conflist[i++] = EGL_DEPTH_SIZE;
    conflist[i++] = 16;

	conflist[i++] = EGL_SURFACE_TYPE;
	conflist[i++] = EGL_PBUFFER_BIT ;

#ifdef GL_OES_VERSION_1_1
	if(gEnable_PBuffer_Bind_Texture)
	{
		conflist[i++] = EGL_BIND_TO_TEXTURE_RGB;
		conflist[i++] = EGL_TRUE;
	}
#endif // GL_OES_VERSION_1_1
	
	switch(PVRShellGet(prefFSAAMode))
	{
		case 1:
			conflist[i++] = EGL_SAMPLE_BUFFERS;
			conflist[i++] = 1;
			conflist[i++] = EGL_SAMPLES;
			conflist[i++] = 2;
		break;

		case 2:
			conflist[i++] = EGL_SAMPLE_BUFFERS;
			conflist[i++] = 1;
			conflist[i++] = EGL_SAMPLES;
			conflist[i++] = 4;
		break;

		default:
			conflist[i++] = EGL_SAMPLE_BUFFERS;
			conflist[i++] = 0;
	}

	conflist[i++] = EGL_NONE;

	/* Find and return the config */
    if (!eglChooseConfig(eglGetCurrentDisplay(), conflist, &gEglConfig, 1, &num_config) || num_config != 1) {
		return 0;
    }

#ifdef GL_OES_VERSION_1_1
	// Check whether EGL_BIND_TO_TEXTURE_RGB is really set for the chosen config.
	// This is a workaround for a bug in eglChooseConfig that only occurs when
	// render to texture is disabled in the driver.
	EGLBoolean bCanBindToTexture;
	eglGetConfigAttrib(eglGetCurrentDisplay(), gEglConfig, EGL_BIND_TO_TEXTURE_RGB, &bCanBindToTexture);
	if(!bCanBindToTexture)
	{
		gEnable_PBuffer_Bind_Texture = false;
	}
#endif

    return gEglConfig;
}

/*******************************************************************************
 * Function Name  : DrawBillBoardQuad
 * Input		  : Size, (x,y,z) and texture pntr, From and To camera vectors
 * Description    : Draw a Billboard in location X,Y,Z with a certain size and texture
 *******************************************************************************/
void OGLESShadowTechniques::DrawBillBoardQuad(
	const PVRTVECTOR3	&MyFrom,
	const PVRTVECTOR3	&MyTo,
	const VERTTYPE		x,
	const VERTTYPE		y,
	const VERTTYPE		z,
	const VERTTYPE		Size,
	const GLuint		pTexture)
{
	PVRTMATRIX bbmatrix;

	PVRTVECTOR3 LookAt,Right,Up;

	/* Find Matrix based on Camera Vectors such that the Billboard always faces the camera */
	LookAt.x = MyFrom.x - MyTo.x;
	LookAt.y = MyFrom.y - MyTo.y;
	LookAt.z = MyFrom.z - MyTo.z;

	Up.x = f2vt(0.0f);
	Up.y = f2vt(1.0f);
	Up.z = f2vt(0.0f);

	Right.x = f2vt(1.0f);
	Right.y = f2vt(0.0f);
	Right.z = f2vt(0.0f);

	PVRTMatrixVec3Normalize(LookAt, LookAt);
	PVRTMatrixVec3CrossProduct(Right, Up, LookAt);
	PVRTMatrixVec3CrossProduct(Up, Right, LookAt);

	bbmatrix.f[0]  = Right.x;
	bbmatrix.f[1]  = Right.y;
	bbmatrix.f[2]  = Right.z;
	bbmatrix.f[3]  = f2vt(0);
	bbmatrix.f[4]  = Up.x;
	bbmatrix.f[5]  = Up.y;
	bbmatrix.f[6]  = Up.z;
	bbmatrix.f[7]  = f2vt(0);
	bbmatrix.f[8]  = LookAt.x;
	bbmatrix.f[9]  = LookAt.y;
	bbmatrix.f[10] = LookAt.z;
	bbmatrix.f[11] = f2vt(0);
	bbmatrix.f[12] = x;
	bbmatrix.f[13] = y;
	bbmatrix.f[14] = z;
	bbmatrix.f[15] = f2vt(1);

	/* Set the special billboard matrix */
	glPushMatrix();
	myglMultMatrix(bbmatrix.f);

	/* Set Texture and Texture Options */
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, pTexture);
	glEnable(GL_TEXTURE_2D);

	/* Set Base Color to White */
	myglColor4(f2vt(1.0f), f2vt(1.0f), f2vt(1.0f), f2vt(1.0f));

	/* Vertex Data */
	VERTTYPE verts[] =		{	-Size, -Size, 0,
								-Size, +Size, 0,
								+Size, -Size, 0,
								+Size, +Size, 0
							};

	VERTTYPE texcoords[] =	{	f2vt(0.0f), f2vt(1.0f),
								f2vt(0.0f), f2vt(0.0f),
								f2vt(1.0f), f2vt(1.0f),
								f2vt(1.0f), f2vt(0.0f)
							};

	/* Set Arrays - Only need Vertex Array and Tex Coord Array*/
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3,VERTTYPEENUM,0,verts);

	glClientActiveTexture(GL_TEXTURE0);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2,VERTTYPEENUM,0,texcoords);

	/* Draw Strip */
	glDrawArrays(GL_TRIANGLE_STRIP,0,4);

	/* Disable Arrays */
	glClientActiveTexture(GL_TEXTURE0);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);

	glDisableClientState(GL_VERTEX_ARRAY);

	/* Recover previous Matrix - no more billboard stuff */
	glPopMatrix();

}

/*******************************************************************************
 * Function Name  : UpdateLightPosition
 * Input		  : None
 * Description    : Update the Light Position and select the current active shadow technique
 *******************************************************************************/
void OGLESShadowTechniques::UpdateLightPosition()
{
	float	gfAnimationBlend;
	float	gfInvAnimationBlend;

	int Range = abs(gAnimTbl[gnCurrentAnimation][1] - gAnimTbl[gnCurrentAnimation][0]);

	/* Determine Blend Factors for Animation Table */
	gfAnimationBlend = (((float)fAnimationCounter)/Range);
	gfInvAnimationBlend = 1.0f - gfAnimationBlend;

	/* Calculate current Animation Position */
	gnLightRotCntr = ((gfInvAnimationBlend * ((float)gAnimTbl[gnCurrentAnimation][0]) + gfAnimationBlend * ((float)gAnimTbl[gnCurrentAnimation][1])));

	/* Calculate Light Position from current Animation Position */
	if (ENABLE_LIGHT_ROT)
	{
		vCurrentLightPos.x = f2vt(LIGHTPOSSCALE * 5.35f * (float)PVRTFCOS(gnLightRotCntr/100.0f));
		vCurrentLightPos.y = f2vt(LIGHTPOSSCALE * 5.35f);
		vCurrentLightPos.z = f2vt(-LIGHTPOSSCALE * 5.35f * (float)PVRTFSIN(gnLightRotCntr/100.0f));
		vCurrentLightPos.w = f2vt(0.0f);
	}

	myglLightv( GL_LIGHT0, GL_POSITION, &vCurrentLightPos.x);

	/* Animation Update use the time based Factor calculated in the main render loop */
	fAnimationCounter=fAnimationCounter+fFactor;

	if (fAnimationCounter>Range)
	{
		fAnimationCounter=0;
		gnPBufferUpdateCounter=0;
		gnCurrentAnimation++;

		if (gnCurrentAnimation>MAXANIMATIONSTEPS)
		{
			gnCurrentAnimation=0;
		}

		/* Set current Shadow Mode */
		gnCurrentMode = ((ShadowModes)gAnimTbl[gnCurrentAnimation][2]);
	}

	/* Through Build option its possible to force a certain Shadow Type to always be used
	   Handy for debugging and to see where the techniques break down */

	#if defined DRAW_ADV_BLOB_SHADOW
		gnCurrentMode = ADVANCEDBLOBMODE;
	#endif

	#if defined DRAW_PROJ_SHADOW
		gnCurrentMode = PROJGEOMMODE;
	#endif

	#if defined DRAW_BASE_BLOB_SHADOW
		gnCurrentMode = BASEBLOBMODE;
	#endif

	#if defined DRAW_RENDER2TEX_SHADOW
		gnCurrentMode = R2TEXMODE;
	#endif
}

/*******************************************************************************
 * Function Name  : SetupMatrices
 * Input		  : Matrix Type
 * Description    : Setup the Matrix as requested
 *******************************************************************************/
void OGLESShadowTechniques::SetupMatrices(MatrixEnums MatrixSelected)
{
	static MatrixEnums OldEnum = BILLBOARD; /* Initial Safe Setup */

	float nearPlane, farPlane, fov, aspect;
	float top, bottom, left, right;

	MyTo.x = f2vt(0.0f);
	MyTo.y = f2vt(1.0f);
	MyTo.z = f2vt(0.0f);

	if (OldEnum==PROJGEOM)
	{
		/* Do a POP Operation first to removed the post process */
		glMatrixMode(GL_MODELVIEW);
		glPopMatrix();
	}

	if(MatrixSelected==LIGHT)
	{
		/* Setup Matrices for Render from Light Position */
		/* Projection */

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();

		/* Calculte Frustum call to match gluPerspective */
		nearPlane = 1.0f;
		farPlane = 50.0f;
		fov = 45;
		aspect = PBUFFERSIZE/PBUFFERSIZE;

		top = (float) PVRTFTAN(fov*PI/360.0) * nearPlane;
		bottom = -top;
		left = aspect * bottom;
		right = aspect * top;

		myglFrustum(f2vt(left), f2vt(right), f2vt(bottom), f2vt(top), f2vt(nearPlane), f2vt(farPlane));

		/* Model View Matrix */

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		PVRTMatrixRotationY(MyRotMatrix, f2vt((float)gnObjRotCntr/100.0f));

		MyFrom.x = vCurrentLightPos.x;
		MyFrom.y = vCurrentLightPos.y;
		MyFrom.z = vCurrentLightPos.z;

		PVRTMatrixLookAtRH(MyViewMatrix, MyFrom, MyTo, MyUp);
		PVRTMatrixMultiply(MyRotViewMatrix, MyRotMatrix, MyViewMatrix);

		myglLoadMatrix(MyRotViewMatrix.f);
	}
	else if (MatrixSelected==PROJTEX)
	{
		/* Setup Texture Matrix for Texture	Projection */

		glMatrixMode(GL_TEXTURE);
		glLoadIdentity();

		myglTranslate(f2vt(0.5f), f2vt(0.6f), f2vt(0.0f));
		myglScale(f2vt(0.15f), f2vt(0.15f), f2vt(1.0f));

		/*******/

		PVRTMatrixRotationY(MyRotMatrix, f2vt((float)gnObjRotCntr/100.0f));

		MyFrom.x = vCurrentLightPos.x;
		MyFrom.y = vCurrentLightPos.y;
		MyFrom.z = vCurrentLightPos.z;

		PVRTMatrixLookAtRH(MyViewMatrix, MyFrom, MyTo, MyUp);
		PVRTMatrixMultiply(MyRotViewMatrix, MyRotMatrix, MyViewMatrix);

		myglMultMatrix(MyRotViewMatrix.f);
	}
	else
	{
		/* Setup Default Camera Case */
		if (OldEnum!=CAMERA)
		{
			/* Do Setup */

			/* Projection */
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			if(PVRShellGet(prefIsRotated) && PVRShellGet(prefFullScreen))
				myglRotate(f2vt(90), f2vt(0), f2vt(0), f2vt(1));

			// Calculte Frustum call to match gluPerspective
			nearPlane = 10.0f;
			farPlane = 500.0f;
			fov = 30;
			aspect = 320.0f/240.0f;

			top = (float) PVRTFTAN(fov*PI/360.0) * nearPlane;
			bottom = -top;
			left = aspect * bottom;
			right = aspect * top;

			myglFrustum(f2vt(left), f2vt(right), f2vt(bottom), f2vt(top), f2vt(nearPlane), f2vt(farPlane));

			/* Model View Matrix */

			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();

			PVRTMatrixRotationY(MyRotMatrix, f2vt((float)gnObjRotCntr/100.0f));

			MyFrom.x = f2vt(10.0f);
			MyFrom.y = f2vt(10.0f);
			MyFrom.z = f2vt(-10.0f);

			MyTo.x = f2vt(0.0f);
			MyTo.y = f2vt(0.0f);
			MyTo.z = f2vt(0.0f);

			PVRTMatrixLookAtRH(MyViewMatrix, MyFrom, MyTo, MyUp);
			PVRTMatrixMultiply(MyRotViewMatrix, MyRotMatrix, MyViewMatrix);

			myglLoadMatrix(MyRotViewMatrix.f);
		}

		if (MatrixSelected==PROJGEOM)
		{
			/* Add on Post Process after a Push */
			glMatrixMode(GL_MODELVIEW);
			glPushMatrix();

			shadowMatrix(floorShadow, floorPlane, vCurrentLightPos);
			myglMultMatrix(floorShadow.f);
		}
	}

	/* Update Old Enum Var */
	OldEnum = MatrixSelected;
}

/*******************************************************************************
 * Function Name  : NewDemo
 * Description    : Called by the Shell to initialize a new instance to the
 *					demo class.
 *******************************************************************************/
PVRShell* NewDemo(){
	return new OGLESShadowTechniques();
}
/*****************************************************************************
 End of file (OGLESShadowTechniques.cpp)
*****************************************************************************/

