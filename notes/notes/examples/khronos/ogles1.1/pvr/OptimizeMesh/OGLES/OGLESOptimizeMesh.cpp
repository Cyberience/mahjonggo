/******************************************************************************

 @File         OGLESOptimizeMesh.cpp

 @Title        OptimizeMesh

 @Copyright    Copyright (C) 2005 - 2006 by Imagination Technologies Limited.

 @Platform     Independant

 @Description  Shows OptimizeMeshes with PVRTC compression. In the case of OGLES
               Lite we have to convert the data to fixed point format. So we
               allocate extra buffers for the data.

******************************************************************************/
#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "PVRShell.h"
#include "OGLESTools.h"
#include "model_texture.h"

#ifdef OGLESLITE
	#include "Sphere_fixed.h"
	#include "SphereOpt_fixed.h"
#else
	#include "Sphere_float.h"
	#include "SphereOpt_float.h"
#endif

/*****************************************************************************
** Build options
*****************************************************************************/

/*
	Define this for a demonstration of triangle stripping at load time.
	Normally, an app wouldn't strip meshes at load time - it takes too much
	time. This is purely for demonstration purposes.
*/
//#define ENABLE_LOAD_TIME_STRIP

/*****************************************************************************
** DEFINES
*****************************************************************************/

#define VIEW_DISTANCE		f2vt(35)

// Times in milliseconds
#define TIME_AUTO_SWITCH	(4000)
#define TIME_FPS_UPDATE		(500)

// Assuming a 4:3 aspect ratio:
#define CAM_ASPECT	f2vt(1.333333333f)
#define CAM_NEAR	f2vt(4.0f)
#define CAM_FAR		f2vt(5000.0f)


/****************************************************************************
** Constants
****************************************************************************/

// Vectors for calculating the view matrix
PVRTVECTOR3 c_vOrigin = { 0, 0 ,0 };
PVRTVECTOR3	c_vUp = { 0, 1, 0 };

/****************************************************************************
** Class: OGLESOptimizeMesh
****************************************************************************/
class OGLESOptimizeMesh : public PVRShell
{
	/* Print3D, Extension and POD Class Objects */
	CPVRTPrint3D 		m_sPrint3D;
	CPVRTPODScene		m_sModel;		// Model
	CPVRTPODScene		m_sModelOpt;	// Triangle optimized model

#ifdef ENABLE_LOAD_TIME_STRIP
	unsigned short		*m_pNewIdx;		// Optimized model's indices
#endif

	// Model texture
	GLuint		m_Texture;

	// View and Projection Matrices
	PVRTMATRIX	m_mView, m_mProj;
	VERTTYPE	m_fViewAngle;

	// Used to switch mode (not optimized / optimized) after a while
	unsigned long	m_uiSwitchTimeDiff;
	int				m_nPage;

	// Time variables
	unsigned long	m_uiLastTime, m_uiTimeDiff;
	
	// FPS variables
	unsigned long	m_uiFPSTimeDiff, m_uiFPSFrameCnt;
	float			m_fFPS;

#ifdef ENABLE_LOAD_TIME_STRIP
	// There is some processing to be done once only; this flags marks whether it has been done.
	int				m_nInit;
#endif

public:
	/* PVRShell functions */
	virtual bool InitApplication();
	virtual bool InitView();
	virtual bool ReleaseView();
	virtual bool QuitApplication();
	virtual bool RenderScene();

	/****************************************************************************
	** Function Definitions
	****************************************************************************/
	void CameraGetMatrix();
	void ComputeViewMatrix();
	void DrawModel( int iOptim );
	void CalculateAndDisplayFrameRate();
#ifdef ENABLE_LOAD_TIME_STRIP
	void StripMesh();
#endif
};

/*******************************************************************************
 * Function Name  : InitApplication
 * Inputs		  :
 * Returns        : true if no error occured
 * Description    : Code in InitApplication() will be called by the Shell ONCE per
 *					run, early on in the execution of the program.
 *					Used to initialize variables that are not dependant on the
 *					rendering context (e.g. external modules, loading meshes, etc.)
 *******************************************************************************/
bool OGLESOptimizeMesh::InitApplication()
{
	// Set some parameters in the Shell
	PVRShellSet(prefAppName, "OptimizeMesh");
	PVRShellSet(prefSwapInterval, 0);

	// Load the meshes
#ifdef OGLESLITE
	m_sModel.ReadFromMemory(c_SPHERE_FIXED_H);
	m_sModelOpt.ReadFromMemory(c_SPHEREOPT_FIXED_H);
#else
	m_sModel.ReadFromMemory(c_SPHERE_FLOAT_H);
	m_sModelOpt.ReadFromMemory(c_SPHEREOPT_FLOAT_H);
#endif

#ifdef ENABLE_LOAD_TIME_STRIP
	// Create a stripped version of the mesh at load time
	m_nInit = 2;
#endif

	// Init values to defaults
	m_nPage = 0;

	return true;
}


/*******************************************************************************
 * Function Name  : QuitApplication
 * Returns        : true if no error occured
 * Description    : Code in QuitApplication() will be called by the Shell ONCE per
 *					run, just before exiting the program.
 *******************************************************************************/
bool OGLESOptimizeMesh::QuitApplication()
{
	m_sModel.Destroy();
	m_sModelOpt.Destroy();
#ifdef ENABLE_LOAD_TIME_STRIP
	free(m_pNewIdx);
#endif
	return true;
}

/*******************************************************************************
 * Function Name  : InitView
 * Inputs		  :
 * Returns        : true if no error occured
 * Description    : Code in InitView() will be called by the Shell upon a change
 *					in the rendering context.
 *					Used to initialize variables that are dependant on the rendering
 *					context (e.g. textures, vertex buffers, etc.)
 *******************************************************************************/
bool OGLESOptimizeMesh::InitView()
{
	SPVRTContext TempContext;

	/* Init Print3D to display text on screen */
	if (!m_sPrint3D.SetTextures(&TempContext,PVRShellGet(prefWidth),PVRShellGet(prefHeight)))
	{
		PVRShellOutputDebug ("ERROR: Cannot initialise Print3D\n");
		return false;
	}

	/******************************
	** Create Textures           **
	*******************************/
	if(!PVRTLoadTextureFromPointer((void*)model_texture, &m_Texture))
	{
		PVRShellOutputDebug("**ERROR** Failed to load texture for Background.\n");
		return false;
	}

	/*********************************
	** View and Projection Matrices **
	*********************************/

	/* Get Camera info from POD file */
	CameraGetMatrix();

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if(PVRShellGet(prefIsRotated) && PVRShellGet(prefFullScreen))
		myglRotate(f2vt(90), f2vt(0), f2vt(0), f2vt(1));

	myglMultMatrix(m_mProj.f);

	/******************************
	** GENERIC RENDER STATES     **
	******************************/

	/* The Type Of Depth Test To Do */
	glDepthFunc(GL_LEQUAL);

	/* Enables Depth Testing */
	glEnable(GL_DEPTH_TEST);

	/* Enables Smooth Color Shading */
	glShadeModel(GL_SMOOTH);

	/* Define front faces */
	glFrontFace(GL_CW);

	/* Sets the clear color */
	myglClearColor(f2vt(0.5f), f2vt(0.5f), f2vt(0.5f), f2vt(0));

	/* Reset the model view matrix to position the light */
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	/* Setup timing variables */
	m_uiLastTime = PVRShellGetTime();
	m_uiFPSFrameCnt = 0;
	m_fFPS = 0;
	m_fViewAngle = f2vt(0.0f);
	m_uiSwitchTimeDiff = 0;

	return true;
}

/*******************************************************************************
 * Function Name  : ReleaseView
 * Returns        : Nothing
 * Description    : Code in ReleaseView() will be called by the Shell before
 *					changing to a new rendering context.
 *******************************************************************************/
bool OGLESOptimizeMesh::ReleaseView()
{
	/* Release all Textures */
	PVRTReleaseTexture(m_Texture);

	/* Release the Print3D textures and windows */
	m_sPrint3D.DeleteAllWindows();
	m_sPrint3D.ReleaseTextures();
	return true;
}

/*******************************************************************************
 * Function Name  : RenderScene
 * Returns		  : true if no error occured
 * Description    : Main rendering loop function of the program. The shell will
 *					call this function every frame.
 *******************************************************************************/
bool OGLESOptimizeMesh::RenderScene()
{
	unsigned long time;

	/* Clear the depth and frame buffer */
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

#ifdef ENABLE_LOAD_TIME_STRIP
	/*
		Show a message on-screen then generate the necessary data on the
		second frame.
	*/
	if(m_nInit)
	{
		--m_nInit;

		if(m_nInit)
		{
			m_sPrint3D.DisplayDefaultTitle("OptimizeMesh", "Generating data...", PVR_LOGO);
			m_sPrint3D.Flush();
			return true;
		}

		StripMesh();
	}
#endif

	/*
		Time
	*/
	time = PVRShellGetTime();
	m_uiTimeDiff = time - m_uiLastTime;
	m_uiLastTime = time;

	// FPS
	m_uiFPSFrameCnt++;
	m_uiFPSTimeDiff += m_uiTimeDiff;
	if(m_uiFPSTimeDiff >= TIME_FPS_UPDATE)
	{
		m_fFPS = m_uiFPSFrameCnt * 1000.0f / (float)m_uiFPSTimeDiff;
		m_uiFPSFrameCnt = 0;
		m_uiFPSTimeDiff = 0;
	}

	// Change mode when necessary
	m_uiSwitchTimeDiff += m_uiTimeDiff;
	if ((m_uiSwitchTimeDiff > TIME_AUTO_SWITCH) || PVRShellIsKeyPressed(PVRShellKeyNameACTION1))
	{
		m_uiSwitchTimeDiff = 0;
		++m_nPage;

#ifdef ENABLE_LOAD_TIME_STRIP
		if(m_nPage > 2)
#else
		if(m_nPage > 1)
#endif
		{
			m_nPage = 0;
		}
	}

	/* Calculate the model view matrix turning around the balloon */
	ComputeViewMatrix();

	/* Draw the model */
	DrawModel(m_nPage);

	/* Calculate the frame rate */
	CalculateAndDisplayFrameRate();

	/* Check for Problems */
	if(glGetError())
	{
		PVRShellOutputDebug("**GL_ERROR** detected.\n");
	}

	return true;
}

/*******************************************************************************
 * Function Name  : ComputeViewMatrix
 * Description    : Calculate the view matrix turning around the balloon
 *******************************************************************************/
void OGLESOptimizeMesh::ComputeViewMatrix()
{
	PVRTVECTOR3 vFrom;
	VERTTYPE factor;

	/* Calculate the angle of the camera around the balloon */
	vFrom.x = VERTTYPEMUL(VIEW_DISTANCE, PVRTCOS(m_fViewAngle));
	vFrom.y = f2vt(0.0f);
	vFrom.z = VERTTYPEMUL(VIEW_DISTANCE, PVRTSIN(m_fViewAngle));

	// Increase the rotation
	factor = f2vt(0.005f * (float)m_uiTimeDiff);
	m_fViewAngle += factor;

	// Ensure it doesn't grow huge and lose accuracy over time
	if(m_fViewAngle > PVRTPI)
		m_fViewAngle -= PVRTTWOPI;

	/* Compute and set the matrix */
	PVRTMatrixLookAtRH(m_mView, vFrom, c_vOrigin, c_vUp);
	glMatrixMode(GL_MODELVIEW);
	myglLoadMatrix(m_mView.f);
}

/*******************************************************************************
 * Function Name  : DrawModel
 * Inputs		  : iOptim
 * Description    : Draws the balloon
 *******************************************************************************/
void OGLESOptimizeMesh::DrawModel( int iOptim )
{
	SPODMesh		*mesh;
	unsigned short	*indices;

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	PVRTMATRIX worldMatrix;
	m_sModel.GetWorldMatrix(worldMatrix, m_sModel.pNode[0]);
	myglMultMatrix(worldMatrix.f);

	/* Enable back face culling */
//	glEnable(GL_CULL_FACE);
//	glCullFace(GL_FRONT);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, m_Texture);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	/* Enable States */
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	/* Set Data Pointers */
	switch(iOptim)
	{
	default:
		mesh	= m_sModel.pMesh;
		indices	= (unsigned short*) mesh->sFaces.pData;
		break;
	case 1:
		mesh	= m_sModelOpt.pMesh;
		indices	= (unsigned short*) mesh->sFaces.pData;
		break;
#ifdef ENABLE_LOAD_TIME_STRIP
	case 2:
		mesh	= m_sModel.pMesh;
		indices	= m_pNewIdx;
		break;
#endif
	}

	// Used to display interleaved geometry
	glVertexPointer(3, VERTTYPEENUM, mesh->sVertex.nStride, mesh->pInterleaved + (long)mesh->sVertex.pData);
	glTexCoordPointer(2, VERTTYPEENUM, mesh->psUVW->nStride, mesh->pInterleaved + (long)mesh->psUVW->pData);

	/* Draw */
	glDrawElements(GL_TRIANGLES, mesh->nNumFaces*3, GL_UNSIGNED_SHORT, indices);

	/* Disable States */
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);

	glPopMatrix();
}


/*******************************************************************************
 * Function Name  : CameraGetMatrix
 * Description    : Function to setup camera position
 *******************************************************************************/
void OGLESOptimizeMesh::CameraGetMatrix()
{
	PVRTVECTOR3	vFrom, vTo, vUp;
	VERTTYPE		fFOV;

	vUp.x = f2vt(0.0f);	vUp.y = f2vt(1.0f);	vUp.z = f2vt(0.0f);

	if(m_sModel.nNumCamera)
	{
		/* Get Camera data from POD Geometry File */
		fFOV = m_sModel.GetCameraPos(vFrom, vTo, 0);
		fFOV = VERTTYPEMUL(fFOV, f2vt(0.75f));		// Convert from horizontal FOV to vertical FOV (0.75 assumes a 4:3 aspect ratio)
	}
	else
	{
		fFOV = f2vt(PVRTPIf / 6);
	}

	/* View */
	PVRTMatrixLookAtRH(m_mView, vFrom, vTo, vUp);

	/* Projection */
	PVRTMatrixPerspectiveFovRH(m_mProj, fFOV, CAM_ASPECT, CAM_NEAR, CAM_FAR);

}

/*******************************************************************************
 * Function Name  : CalculateAndDisplayFrameRate
 * Description    : Computes and displays the on screen information
 *******************************************************************************/
void OGLESOptimizeMesh::CalculateAndDisplayFrameRate()
{
	char	pTitle[512];
	char	*pDesc;

#if !defined __GNUC__
	sprintf(pTitle, "Optimize Mesh %.1ffps", m_fFPS);
#endif

	/* Print text on screen */
	switch(m_nPage)
	{
	default:
		pDesc = "Indexed Triangle List: Unoptimized";
		break;
	case 1:
		pDesc = "Indexed Triangle List: Optimized (at export time)";
		break;
#ifdef ENABLE_LOAD_TIME_STRIP
	case 2:
		pDesc = "Indexed Triangle List: Optimized (at load time)";
		break;
#endif
	}
	m_sPrint3D.DisplayDefaultTitle(pTitle, pDesc, PVR_LOGO);

	/* Flush all Print3D commands */
	m_sPrint3D.Flush();
}

#ifdef ENABLE_LOAD_TIME_STRIP
/*******************************************************************************
 * Function Name  : StripMesh
 * Description    : Generates the stripped-at-load-time list.
 *******************************************************************************/
void OGLESOptimizeMesh::StripMesh()
{
	// Make a copy of the indices (we can't edit the constants in the header)
	m_pNewIdx = (unsigned short*)malloc(sizeof(unsigned short)*m_sModel.pMesh->nNumFaces*3);
	memcpy(m_pNewIdx, m_sModel.pMesh->sFaces.pData, sizeof(unsigned short)*m_sModel.pMesh->nNumFaces*3);

	PVRTTriStripList(m_pNewIdx, m_sModel.pMesh->nNumFaces);
}
#endif

/*******************************************************************************
 * Function Name  : NewDemo
 * Description    : Called by the Shell to initialize a new instance to the
 *					demo class.
 *******************************************************************************/
PVRShell* NewDemo()
{
	return new OGLESOptimizeMesh();
}

/*****************************************************************************
 End of file (OGLESOptimizeMesh.cpp)
*****************************************************************************/
