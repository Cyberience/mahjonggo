/******************************************************************************

 @File         OGLESVase.cpp

 @Title        Vase

 @Copyright    Copyright (C) 2005 - 2006 by Imagination Technologies Limited.

 @Platform     Independant

 @Description  Shows textured transparency and reflections. Requires the PVRShell.

******************************************************************************/
#include <math.h>
#include "PVRShell.h"
#include "OGLESTools.h"

/* Textures */
#include "BACK.h"
#include "Flora.h"
#include "REFLECT.h"

/* Geometry */
#include "Vase.h"

/****************************************************************************
** Class: OGLESVase
****************************************************************************/
class OGLESVase : public PVRShell
{
	/* Print3D class */
	CPVRTPrint3D 			AppPrint3D;

	/* Mesh pointers */
	HeaderStruct_Mesh_Type *glassMesh;
	HeaderStruct_Mesh_Type *silverMesh;

	/* Texture names */
	GLuint					backTexName;
	GLuint					floraTexName;
	GLuint					reflectTexName;

    /* Rotation variables */
	VERTTYPE				fXAng, fYAng;

public:
	OGLESVase()
	{
		/* Init values to defaults */
		fXAng = 0;
		fYAng = 0;
	}

	/* PVRShell functions */
	virtual bool InitApplication();
	virtual bool InitView();
	virtual bool ReleaseView();
	virtual bool QuitApplication();
	virtual bool RenderScene();

	/****************************************************************************
	** Function Definitions
	****************************************************************************/
	void RenderObject(HeaderStruct_Mesh_Type *mesh);
	void RenderReflectiveObject(HeaderStruct_Mesh_Type *mesh, PVRTMATRIX *pNormalTx);
	void RenderBackground();
};


/*******************************************************************************
 * Function Name  : InitApplication
 * Returns        : true if no error occured
 * Description    : Code in InitApplication() will be called by the Shell ONCE per
 *					run, early on in the execution of the program.
 *					Used to initialize variables that are not dependant on the
 *					rendering context (e.g. external modules, loading meshes, etc.)
 *******************************************************************************/
bool OGLESVase::InitApplication()
{
	/* Set Shell preferences */
	PVRShellSet(prefAppName, "VASE");

	/* Allocate header data */
	glassMesh	= PVRTLoadHeaderObject(&Mesh[M_GLASS]);
	silverMesh	= PVRTLoadHeaderObject(&Mesh[M_SILVER]);

	return true;
}

/*******************************************************************************
 * Function Name  : QuitApplication
 * Returns        : true if no error occured
 * Description    : Code in QuitApplication() will be called by the Shell ONCE per
 *					run, just before exiting the program.
 *******************************************************************************/
bool OGLESVase::QuitApplication()
{
    /* Release header data */
	PVRTUnloadHeaderObject(glassMesh);
	PVRTUnloadHeaderObject(silverMesh);
	return true;
}

/*******************************************************************************
 * Function Name  : InitView
 * Returns        : true if no error occured
 * Description    : Code in InitView() will be called by the Shell upon a change
 *					in the rendering context.
 *					Used to initialize variables that are dependant on the rendering
 *					context (e.g. textures, vertex buffers, etc.)
 *******************************************************************************/
bool OGLESVase::InitView()
{
	bool			err;
	SPVRTContext	Context;
	PVRTMATRIX		MyPerspMatrix;

	/* Initialize Print3D textures */
	err = AppPrint3D.SetTextures(&Context, PVRShellGet(prefWidth), PVRShellGet(prefHeight));
	if (err == false)
	{
		PVRShellOutputDebug ("ERROR: Cannot initialise Print3D\n");
		return false;
	}

	/* Load textures */
	if (!PVRTLoadTextureFromPointer((void*)BACK,&backTexName))
		return false;
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if (!PVRTLoadTextureFromPointer((void*)Flora,&floraTexName))
		return false;
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if (!PVRTLoadTextureFromPointer((void*)REFLECT,&reflectTexName))
		return false;
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	/* Calculate projection matrix */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (PVRShellGet(prefIsRotated) && PVRShellGet(prefFullScreen))
	{
		myglRotate(f2vt(90), f2vt(0), f2vt(0), f2vt(1));
	}
	PVRTMatrixPerspectiveFovRH(MyPerspMatrix, f2vt(35.0f*(3.14f/180.0f)), f2vt(4.0f/3.0f), f2vt(10.0f), f2vt(1200.0f));
	myglMultMatrix(MyPerspMatrix.f);

	/* Enable texturing */
	glEnable(GL_TEXTURE_2D);

	/* No problem occured */
	return true;
}

/*******************************************************************************
 * Function Name  : ReleaseView
 * Returns        : Nothing
 * Description    : Code in ReleaseView() will be called by the Shell before
 *					changing to a new rendering context.
 *******************************************************************************/
bool OGLESVase::ReleaseView()
{
	/* Release textures */
	PVRTReleaseTexture(backTexName);
	PVRTReleaseTexture(floraTexName);
	PVRTReleaseTexture(reflectTexName);

	/* Release Print3D textures */
	AppPrint3D.ReleaseTextures();
	return true;
}

/*******************************************************************************
 * Function Name  : RenderScene
 * Returns		  : true if no error occured
 * Description    : Main rendering loop function of the program. The shell will
 *					call this function every frame.
 *******************************************************************************/
bool OGLESVase::RenderScene()
{
	PVRTMATRIX RotationMatrix, TmpX, TmpY;

	/* Set up viewport */
	glViewport(0, 0, PVRShellGet(prefWidth), PVRShellGet(prefHeight));

	/* Increase rotation angles */
	fXAng += VERTTYPEDIV(PVRTPI, f2vt(100.0f));
	fYAng += VERTTYPEDIV(PVRTPI, f2vt(150.0f));

	if(fXAng >= PVRTPI)
		fXAng -= PVRTTWOPI;

	if(fYAng >= PVRTPI)
		fYAng -= PVRTTWOPI;

	/* Clear the buffers */
	glEnable(GL_DEPTH_TEST);
	myglClearColor(1,1,1,0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/* Draw the scene */

	/***************
	** Background **
	***************/
	/* No need for Z test */
	glDisable(GL_DEPTH_TEST);

	/* Set projection matrix to identity (with or without display rotation) for background */
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	if(PVRShellGet(prefIsRotated) && PVRShellGet(prefFullScreen))
		myglRotate(f2vt(90), f2vt(0), f2vt(0), f2vt(1));

	/* Set modelview matrix to identity for background */
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	/* Set background texture */
	glBindTexture(GL_TEXTURE_2D, backTexName);

	/* Set texture environment mode: use texture directly */
	myglTexEnv(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

	/* Render background */
	RenderBackground();


	/**********************
	** Reflective silver **
	**********************/

	/* Calculate rotation matrix */
	PVRTMatrixRotationX(TmpX, fXAng);
	PVRTMatrixRotationY(TmpY, fYAng);
	PVRTMatrixMultiply(RotationMatrix, TmpX, TmpY);

	/* Modelview matrix */
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	myglTranslate(f2vt(0.0f), f2vt(0.0f), f2vt(-200.0f));
	myglMultMatrix(RotationMatrix.f);

	/* Projection matrix */
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	/* Enable depth test */
	glEnable(GL_DEPTH_TEST);

	/* Render front faces only (model has reverse winding) */
	glCullFace(GL_FRONT);

	/* Set texture and texture env mode */
	glBindTexture(GL_TEXTURE_2D,reflectTexName);
	myglTexEnv(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

	/* Render reflective object */
	RenderReflectiveObject(silverMesh, &RotationMatrix);


	/*******************
	** See-thru glass **
	*******************/

	/* Don't update Z-buffer */
	glDepthMask(GL_FALSE);

	/* Enable alpha blending */
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

	/* Set texture and texture env mode */
	glBindTexture(GL_TEXTURE_2D, floraTexName);
	myglTexEnv(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

	/* Pass 1: only render back faces (model has reverse winding) */
	glFrontFace(GL_CW);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	RenderObject(glassMesh);

	/* Pass 2: only render front faces (model has reverse winding) */
	glCullFace(GL_FRONT);
	RenderObject(glassMesh);

	/* Restore Z-writes */
	glDepthMask(GL_TRUE);

	/* Disable alpha blending */
	glDisable(GL_BLEND);

	/* Display info text */
	AppPrint3D.DisplayDefaultTitle("Vase", "Translucency and reflections.", PVR_LOGO);
	AppPrint3D.Flush();

	/* No problem occured */
	return true;
}


/*******************************************************************************
 * Function Name  : RenderReflectiveObject
 * Inputs		  : *mesh, *pNormalTx
 * Description    : Code to render the reflective parts of the object
 *******************************************************************************/
void OGLESVase::RenderReflectiveObject(HeaderStruct_Mesh_Type *mesh, PVRTMATRIX *pNormalTx)
{
	VERTTYPE		*uv_transformed = new VERTTYPE[2 * mesh->nNumVertex];
	VERTTYPE		EnvMapMatrix[16];
	unsigned int	i;

	/* Calculate matrix for environment mapping: simple multiply by 0.5 */
	for (i=0; i<16; i++)
	{
		/* Convert matrix to fixed point */
		EnvMapMatrix[i] = VERTTYPEMUL(pNormalTx->f[i], f2vt(0.5f));
	}

	/* Calculate UVs for environment mapping */
	for (i = 0; i < mesh->nNumVertex; i++)
	{
		uv_transformed[2*i] =	VERTTYPEMUL(mesh->pNormals[3*i+0], EnvMapMatrix[0]) +
								VERTTYPEMUL(mesh->pNormals[3*i+1], EnvMapMatrix[4]) +
								VERTTYPEMUL(mesh->pNormals[3*i+2], EnvMapMatrix[8]) +
								f2vt(0.5f);

		uv_transformed[2*i+1] =	VERTTYPEMUL(mesh->pNormals[3*i+0], EnvMapMatrix[1]) +
								VERTTYPEMUL(mesh->pNormals[3*i+1], EnvMapMatrix[5]) +
								VERTTYPEMUL(mesh->pNormals[3*i+2], EnvMapMatrix[9]) +
								f2vt(0.5f);
	}

	/* Set vertex pointer */
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, VERTTYPEENUM, 0, mesh->pVertex);

	/* Set texcoord pointer */
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2, VERTTYPEENUM, 0, uv_transformed);

	/* Draw object */
	glDrawElements(GL_TRIANGLES, mesh->nNumFaces*3, GL_UNSIGNED_SHORT, mesh->pFaces);

	/* Restore client states */
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);

	/* Delete memory */
	delete[] uv_transformed;
}


/*******************************************************************************
 * Function Name  : RenderObject
 * Inputs		  : *mesh
 * Description    : Code to render an object
 *******************************************************************************/
void OGLESVase::RenderObject(HeaderStruct_Mesh_Type *mesh)
{
	/* Set vertex pointer */
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, VERTTYPEENUM, 0, mesh->pVertex);

	/* Set texcoord pointer */
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2, VERTTYPEENUM, 0, mesh->pUV);

	/* Draw object */
	glDrawElements(GL_TRIANGLES, mesh->nNumFaces*3, GL_UNSIGNED_SHORT, mesh->pFaces);

	/* Restore client states */
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}


/*******************************************************************************
 * Function Name  : RenderBackground
 * Description    : Code to render a quad background
 *******************************************************************************/
void OGLESVase::RenderBackground()
{
	VERTTYPE BkndVerts[] = {f2vt(-1),f2vt(-1),f2vt(1),
							f2vt(1),f2vt(-1),f2vt(1),
							f2vt(-1),f2vt(1),f2vt(1),
							f2vt(1),f2vt(1),f2vt(1)};
	VERTTYPE BkndUV[] =	   {f2vt(0.5f/256.0f),		f2vt(0.5f/256.0f),
							f2vt(1-0.5f/256.0f),	f2vt(0.5f/256.0f),
							f2vt(0.5f/256.0f),		f2vt(1-0.5f/256.0f),
							f2vt(1-0.5f/256.0f),	f2vt(1-0.5f/256.0f)};

	/* Set vertex pointer */
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, VERTTYPEENUM, 0, BkndVerts);

	/* Set texcoord pointer */
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2, VERTTYPEENUM, 0, BkndUV);

	/* Draw object */
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	/* Restore client states */
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}

/*******************************************************************************
 * Function Name  : NewDemo
 * Description    : Called by the Shell to initialize a new instance to the
 *					demo class.
 *******************************************************************************/
PVRShell* NewDemo()
{
	return new OGLESVase();
}

/*****************************************************************************
 End of file (OGLESVase.cpp)
*****************************************************************************/
