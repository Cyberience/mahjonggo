/******************************************************************************

 @File         OGLESPolyBump.cpp

 @Title        Demonstrates MBX's DOT3 lighting

 @Copyright    Copyright (C) 2005 - 2006 by Imagination Technologies Limited.

 @Platform     Independant

 @Description  Per-pixel lighting done using Dot3 bumpmapping. A very complex
               model has been computed in the normal map for Dot3, that will be
               applied to a very low polygon count model. The model used in this
               demo is one of the free models suplied by Crytek
               (http://www.crytek.com/polybump). Currently there are several
               companies supliying tools and plug-ins to compute these maps.

******************************************************************************/
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "PVRShell.h"
#include "OGLESTools.h"

/* Geometry */
#include "Polybump_head.h"

/* Textures */
#include "Head_clonespace.h"
#include "Head_diffuse.h"

/*************************
**		Demo code       **
*************************/
class OGLESPolybump : public PVRShell
{
	/* Print3D class */
	CPVRTPrint3D 	AppPrint3D;

	/* Texture names */
	GLuint gDiffuseMap;
	GLuint gCloneMap;

	/* Render width and height */
	unsigned int dwCurrentWidth;
	unsigned int dwCurrentHeight;

	/* Dynamic data */
	int				nFrame;
	PVRTVECTOR3		CameraFrom, CameraTo, CameraUp;
	PVRTMATRIX		mLookMatrix;
	PVRTMATRIX		MyPerspMatrix;

	/* Mesh pointers and other model data */
	HeaderStruct_Mesh_Type *theMesh;

	/* OGLES Implementation flags */
	int   gCombinersPresent;
	int   gIMGTextureFFExtPresent;

public:
	OGLESPolybump()
	{
		gDiffuseMap = 0;
		gCloneMap = 0;
		gCombinersPresent = 0;
		gIMGTextureFFExtPresent = 0;
		nFrame=0;

		/* Camera and Light details */
		CameraFrom.x = f2vt(0.0f);	CameraFrom.y = f2vt(105.0f);	CameraFrom.z = f2vt(-300.0f);
		CameraTo.x  = f2vt(0.0f);	CameraTo.y   = f2vt(105.0f);	CameraTo.z  = f2vt(0.0f);
		CameraUp.x  = f2vt(0.0f);	CameraUp.y   = f2vt(1.0f);		CameraUp.z  = f2vt(0.0f);
	}

	/* PVRShell functions */
	virtual bool InitApplication();
	virtual bool InitView();
	virtual bool ReleaseView();
	virtual bool QuitApplication();
	virtual bool RenderScene();

	/****************************************************************************
		** Function Definitions
	****************************************************************************/
	void RenderPrimitive(HeaderStruct_Mesh_Type *object);
	void CalculateDot3LightDirection(PVRTMATRIX *mWorldMatrix, PVRTVECTOR3 *LightVector);
};


/*******************************************************************************
 * Function Name  : InitApplication
 * Inputs		  : argc, *argv[], uWidth, uHeight
 * Returns        : true if no error occured
 * Description    : Code in InitApplication() will be called by the Shell ONCE per
 *					run, early on in the execution of the program.
 *					Used to initialize variables that are not dependant on the
 *					rendering context (e.g. external modules, loading meshes, etc.)
 *******************************************************************************/
bool OGLESPolybump::InitApplication()
{
	theMesh = PVRTLoadHeaderObject(&Mesh[0]);
	return true;
}

/*******************************************************************************
 * Function Name  : QuitApplication
 * Returns        : true if no error occured
 * Description    : Code in QuitApplication() will be called by the Shell ONCE per
 *					run, just before exiting the program.
 *******************************************************************************/
bool OGLESPolybump::QuitApplication()
{
	PVRTUnloadHeaderObject(theMesh);
	return true;
}

/*******************************************************************************
 * Function Name  : InitView
 * Inputs		  : uWidth, uHeight
 * Returns        : true if no error occured
 * Description    : Code in InitView() will be called by the Shell upon a change
 *					in the rendering context.
 *					Used to initialize variables that are dependant on the rendering
 *					context (e.g. textures, vertex buffers, etc.)
 *******************************************************************************/
bool OGLESPolybump::InitView()
{
	int				err;
	SPVRTContext	Context;

	dwCurrentWidth = PVRShellGet(prefWidth);
	dwCurrentHeight = PVRShellGet(prefHeight);

	/* Initialize Print3D textures */
	err = AppPrint3D.SetTextures(&Context, PVRShellGet(prefWidth), PVRShellGet(prefHeight));
	if(err == false)
	{
		PVRShellOutputDebug ("ERROR: Cannot initialise Print3D\n");
		return false;
	}

	/* Load Textures */
	err = !PVRTLoadTextureFromPointer((void*)Head_clonespace, &gCloneMap);
	if (err)
	{
		return false;
	}
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	err = !PVRTLoadTextureFromPointer((void*)Head_diffuse, &gDiffuseMap);
	if (err)
	{
		return false;
	}
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	/* Retrieve OpenGL ES driver version */
	int		nOGLESVersionMajor=1, nOGLESVersionMinor=0;
	const char	*pszVersionNumber;
	pszVersionNumber = strchr((const char *)glGetString(GL_VERSION), '.');
	if (pszVersionNumber)
	{
		nOGLESVersionMajor = pszVersionNumber[-1] - '0';
		nOGLESVersionMinor = pszVersionNumber[+1] - '0';
	}

	/* Check extensions */
	if (nOGLESVersionMajor>1 || (nOGLESVersionMajor==1 && nOGLESVersionMinor>0))
	{
		gCombinersPresent = true;
	}
	else
	{
        gCombinersPresent = CPVRTglesExt::IsGLExtensionSupported("GL_ARB_texture_env_combine");
	}

	gIMGTextureFFExtPresent = CPVRTglesExt::IsGLExtensionSupported("GL_IMG_texture_env_enhanced_fixed_function");

	if(!gCombinersPresent && !gIMGTextureFFExtPresent )
	{
		PVRShellOutputDebug("Can't run this demo without support for GL_ARB_texture_env_combine or GL_IMG_texture_env_enhanced_fixed_function\n");
		return false; // Can't run this demo
	}

	/* Create perspective matrix */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	if (PVRShellGet(prefIsRotated) && PVRShellGet(prefFullScreen))
	{
		myglRotate(f2vt(90), f2vt(0), f2vt(0), f2vt(1));
		dwCurrentWidth = PVRShellGet(prefHeight);
		dwCurrentHeight = PVRShellGet(prefWidth);
	}
	PVRTMatrixPerspectiveFovRH(MyPerspMatrix, f2vt(30.0f*(3.14f/180.0f)), f2vt(((float)dwCurrentWidth/(float)dwCurrentHeight)), f2vt(10.0f), f2vt(10000.0f));
	myglMultMatrix(MyPerspMatrix.f);

	/* Global render states */

	// setup culling
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);

	// setup ztests
	glEnable(GL_DEPTH_TEST);

	return true;
}

/*******************************************************************************
 * Function Name  : ReleaseView
 * Returns        : Nothing
 * Description    : Code in ReleaseView() will be called by the Shell before
 *					changing to a new rendering context.
 *******************************************************************************/
bool OGLESPolybump::ReleaseView()
{
	/* Release textures */
	AppPrint3D.ReleaseTextures();

	PVRTReleaseTexture(gCloneMap);
	PVRTReleaseTexture(gDiffuseMap);
	return true;
}


/*******************************************************************************
 * Function Name  : RenderScene
 * Returns		  : true if no error occured
 * Description    : Main rendering loop function of the program. The shell will
 *					call this function every frame.
 *******************************************************************************/
bool OGLESPolybump::RenderScene()
{
	PVRTVECTOR3	LightVector;
	PVRTMATRIX	mTempMatrix;

	/* Set clear */
	glViewport(0, 0, PVRShellGet(prefWidth), PVRShellGet(prefHeight));
	myglClearColor(f2vt(0), f2vt(0), f2vt(0), f2vt(1));
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/* Set Scene render states */
	glDisable(GL_LIGHTING);
	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);

	/* Setup texture blend modes */
	/* First layer (Dot3) */
	glActiveTexture(GL_TEXTURE0);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, gCloneMap);
	if(gCombinersPresent)
	{
		myglTexEnv(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE);
		myglTexEnv(GL_TEXTURE_ENV, GL_COMBINE_RGB, GL_DOT3_RGBA);
		myglTexEnv(GL_TEXTURE_ENV, GL_SOURCE0_RGB, GL_TEXTURE);
		myglTexEnv(GL_TEXTURE_ENV, GL_SOURCE1_RGB, GL_PREVIOUS);
	}
	else if(gIMGTextureFFExtPresent)
	{
		myglTexEnv(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DOT3_RGBA);
	}
	else
	{
		PVRShellOutputDebug("Error, neither combiners nor GL_IMG_texture_env_enhanced_fixed_function present\n");
		return false;
	}

	/* Second layer (modulate) */
	glActiveTexture(GL_TEXTURE1);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, gDiffuseMap);

	if (gCombinersPresent)
	{
		myglTexEnv(GL_TEXTURE_ENV, GL_COMBINE_RGB, GL_MODULATE);
		myglTexEnv(GL_TEXTURE_ENV, GL_SOURCE0_RGB, GL_TEXTURE);
		myglTexEnv(GL_TEXTURE_ENV, GL_SOURCE1_RGB, GL_PREVIOUS);
	}
	else if (gIMGTextureFFExtPresent)
	{
		myglTexEnv(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	}

	/* Create world matrix */
	PVRTMatrixIdentity(mLookMatrix);
	PVRTMatrixRotationY(mTempMatrix, PVRTSIN(VERTTYPEDIV(f2vt(nFrame), f2vt(300.0f))) - PVRTPIOVERTWO);
	PVRTMatrixMultiply(mLookMatrix, mLookMatrix, mTempMatrix);

	/* Directional white light (world space) */
	LightVector.x = PVRTSIN(VERTTYPEDIV(f2vt(nFrame), f2vt(40.0f)));
	LightVector.y = f2vt(0.0f);
	LightVector.z = -PVRTABS(PVRTCOS(VERTTYPEDIV(f2vt(nFrame), f2vt(40.0f))));

	/* Calculate Dot3 light direction */
	CalculateDot3LightDirection(&mLookMatrix, &LightVector);

	/* Create camera matrix and multiply to world matrix */
	PVRTMatrixLookAtRH(mTempMatrix, CameraFrom, CameraTo, CameraUp);
	PVRTMatrixMultiply(mLookMatrix, mLookMatrix, mTempMatrix);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	myglMultMatrix(mLookMatrix.f);


	/* Render mesh */
	RenderPrimitive(theMesh);

	/* Disable blend modes */
	glActiveTexture(GL_TEXTURE0);
	glDisable(GL_TEXTURE_2D);

	glActiveTexture(GL_TEXTURE1);
	glDisable(GL_TEXTURE_2D);


	/* Display info text */
	AppPrint3D.DisplayDefaultTitle("PolyBump", "DOT3 per-pixel lighting", PVR_LOGO);

	AppPrint3D.Flush();

	/* Increase frame counter */
	nFrame++;

	return true;
}


/*******************************************************************************
 * Function Name  : CalculateDot3LightDirection
 * Inputs		  : *mWorldMatrix
 * Outputs		  : *LightVector
 * Returns		  : true if no error occured
 * Description    : Set global colour for all vertices with the light direction
 *					used for Dot3. Because the object normals have been computed
 *					already in the normal map this value is the same for all vertices
 *					and it coincides with the light direction transformed with the
 *					inverse of the world matrix.
 *******************************************************************************/
void OGLESPolybump::CalculateDot3LightDirection(PVRTMATRIX *mWorldMatrix, PVRTVECTOR3 *LightVector)
{
	PVRTVECTOR3 LVector;
	PVRTMATRIX	mInvWorldMatrix;
	PVRTVECTOR3 Dot3LightPos;

	LVector.x= LightVector->x;
	LVector.y= LightVector->y;
	LVector.z= LightVector->z;

	/* Normalize light vector in case it is not */
	PVRTMatrixVec3Normalize(LVector, LVector);

	/* Invert world matrix */
	PVRTMatrixInverse(mInvWorldMatrix, *mWorldMatrix);

	/* Transform the light vector with inverse world matrix.
	   Note : Don't include Inverse._4X because LightVector = TO - FROM.
	   FROM is always (0, 0, 0), so transformed FROM would be equal to
	   (Inverse._41, Inverse._42, Inverse._43). Thus by doing TO - FROM, Inverse._4X
	   would be subtracted with themselves, hence yielding zero */
	/* NOTE: the order has been changed to match Polybump angles (x,z,y) */
	Dot3LightPos.x =	VERTTYPEMUL(LVector.x, mInvWorldMatrix.f[0]) +
						VERTTYPEMUL(LVector.y, mInvWorldMatrix.f[4]) +
						VERTTYPEMUL(LVector.z, mInvWorldMatrix.f[8]);

	Dot3LightPos.y =	VERTTYPEMUL(LVector.x, mInvWorldMatrix.f[1]) +
						VERTTYPEMUL(LVector.y, mInvWorldMatrix.f[5]) +
						VERTTYPEMUL(LVector.z, mInvWorldMatrix.f[9]);

	Dot3LightPos.z =	VERTTYPEMUL(LVector.x, mInvWorldMatrix.f[2]) +
						VERTTYPEMUL(LVector.y, mInvWorldMatrix.f[6]) +
						VERTTYPEMUL(LVector.z, mInvWorldMatrix.f[10]);

	/* Half shifting to have a value between 0.0f and 1.0f */
	Dot3LightPos.x = VERTTYPEMUL(Dot3LightPos.x, f2vt(0.5f)) + f2vt(0.5f);
	Dot3LightPos.y = VERTTYPEMUL(Dot3LightPos.y, f2vt(0.5f)) + f2vt(0.5f);
	Dot3LightPos.z = VERTTYPEMUL(Dot3LightPos.z, f2vt(0.5f)) + f2vt(0.5f);

	/* Set light direction as a colour
	 * (the colour ordering depend on how the normal map has been computed)
	 * red=y, green=z, blue=x */
	myglColor4(Dot3LightPos.y, Dot3LightPos.z, Dot3LightPos.x, 0);
}


/*******************************************************************************
 * Function Name  : RenderPrimitive
 * Inputs		  : *object
 * Returns		  : true if no error occured
 * Description    : Render the object using two layers multitexture
 *******************************************************************************/
void OGLESPolybump::RenderPrimitive(HeaderStruct_Mesh_Type *object)
{
	VERTTYPE *pVerticies = object->pVertex;
	VERTTYPE *pUV = object->pUV;

	/* Set vertex data */
	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, VERTTYPEENUM, 0, pVerticies);

	/* Set texture data */
	glClientActiveTexture(GL_TEXTURE0);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2, VERTTYPEENUM, 0, pUV);
	glClientActiveTexture(GL_TEXTURE1);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2, VERTTYPEENUM, 0, pUV);

	/* Draw mesh */
	glDrawElements(GL_TRIANGLES, object->nNumFaces*3, GL_UNSIGNED_SHORT, object->pFaces);

	/* Restore states */
	glDisableClientState(GL_VERTEX_ARRAY);
	glClientActiveTexture(GL_TEXTURE0);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glClientActiveTexture(GL_TEXTURE1);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}


/*******************************************************************************
 * Function Name  : NewDemo
 * Description    : Called by the Shell to initialize a new instance to the
 *					demo class.
 *******************************************************************************/
PVRShell* NewDemo()
{
	return new OGLESPolybump();
}


/*****************************************************************************
 End of file (OGLESPolybump.cpp)
*****************************************************************************/
