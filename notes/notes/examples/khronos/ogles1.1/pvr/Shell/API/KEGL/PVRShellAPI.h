/******************************************************************************

 @File         PVRShellAPI.h

 @Title        

 @Copyright    Copyright (C) 2005 - 2006 by Imagination Technologies Limited.

 @Platform     Independant

 @Description  Makes programming for 3D APIs easier by wrapping surface
               initialization, Texture allocation and other functions for use by a demo.

******************************************************************************/
#ifndef __PVRSHELLAPI_H_
#define __PVRSHELLAPI_H_

/****************************************************************************
** 3D API header files
****************************************************************************/
#include "GLES/egl.h"
/*!***************************************************************************
** Class: PVRShellInitAPI
** @Brief Class. Initialisation interface with specific API.
****************************************************************************/
class PVRShellInitAPI
{
public:
	EGLDisplay	gEglDisplay;
	EGLSurface	gEglWindow;
	EGLContext	gEglContext;
	EGLConfig	gEglConfig;
	EGLint		majorVersion, minorVersion;
	bool		powerManagementSupported;

public:
	EGLConfig SelectEGLConfiguration(const bool bZ, const bool bP, const int nFSAA);
};

#endif // __PVRSHELLAPI_H_

/*****************************************************************************
 End of file (PVRShellAPI.h)
*****************************************************************************/
