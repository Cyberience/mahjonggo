/******************************************************************************

 @File         PVRShellAPI.cpp

 @Title        

 @Copyright    Copyright (C) 2005 - 2006 by Imagination Technologies Limited.

 @Platform     Independant

 @Description  Makes programming for 3D APIs easier by wrapping surface
               initialization, Texture allocation and other functions for use by a demo.

******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "PVRShell.h"
#include "PVRShellAPI.h"
#include "PVRShellImpl.h"

#ifndef EGL_CONTEXT_LOST_IMG
#define EGL_CONTEXT_LOST_IMG						0x300E
#endif

/*****************************************************************************
	Declarations
*****************************************************************************/
#ifndef EGL_VERSION_1_1
static bool PVRShellIsExtensionSupported(EGLDisplay dpy, const char *extension);
#endif


/****************************************************************************
** Class: PVRShellInitAPI
****************************************************************************/

/*****************************************************************************
* Function Name  : ActivatePreferences
* Description    : Activates the user set preferences (like v-sync)
*****************************************************************************/
void PVRShellInit::ApiActivatePreferences()
{
#ifdef EGL_VERSION_1_1
	eglSwapInterval(gEglDisplay, m_pShell->m_pShellData->nSwapInterval);
#endif
}

/*****************************************************************************
* Function Name  : InitAPI
* Returns        : true for success
* Description    : Initialise the 3D API
*****************************************************************************/
bool PVRShellInit::ApiInitAPI()
{
	int					bDone;
	NativeDisplayType	ndt;

	gEglContext = 0;

	do
	{
		bDone = true;

		ndt = (NativeDisplayType)OsGetNativeDisplayType();
		gEglDisplay = eglGetDisplay(ndt);
		if (!eglInitialize(gEglDisplay, &majorVersion, &minorVersion))
		{
			m_pShell->PVRShellOutputDebug("PVRShell: Unable to initialise EGL");
			return false;
		}
		m_pShell->PVRShellOutputDebug("PVRShell: EGL %d.%d initialized\n", majorVersion, minorVersion);

		// Check Extension avaliablility after EGL initialization
		#ifndef EGL_VERSION_1_1
			powerManagementSupported = PVRShellIsExtensionSupported(gEglDisplay,"EGL_IMG_power_management");
		#else
			powerManagementSupported = true;
		#endif

		do
		{
			// bind OpenVG API if requested (but check EGL1.2 support first)
			if(m_pShell->m_pShellData->bNeedOpenVG)
			{
				#ifndef EGL_VERSION_1_2
					m_pShell->PVRShellOutputDebug("PVRShell: OpenVG not supported. Compile against EGL 1.2");
					return false;
				#else
					if(!eglBindAPI(EGL_OPENVG_API))
					{
						m_pShell->PVRShellOutputDebug("PVRShell: Failed to bind OpenVG API");
						return false;
					}
					m_pShell->PVRShellOutputDebug("PVRShell: OpenVG bound");
					// Find an EGL config
					gEglConfig = SelectEGLConfiguration(m_pShell->m_pShellData->bNeedZbuffer, m_pShell->m_pShellData->bNeedPbuffer, 0);
				#endif
			}
			else
			{
				#ifdef EGL_VERSION_1_2
				#ifdef GL_OES_VERSION_2_0
				if(!eglBindAPI(EGL_OPENGL_ES2_API))
				{
					m_pShell->PVRShellOutputDebug("PVRShell: Failed to bind OpenGL ES 2.0 API");
					return false;
				}
				#else
				if(!eglBindAPI(EGL_OPENGL_ES_API))
				{
					m_pShell->PVRShellOutputDebug("PVRShell: Failed to bind OpenGL ES 2.0 API");
					return false;
				}
				#endif
				#endif
				// Find an EGL config
				gEglConfig = SelectEGLConfiguration(m_pShell->m_pShellData->bNeedZbuffer, m_pShell->m_pShellData->bNeedPbuffer, m_pShell->m_pShellData->nFSAAMode);
			}

			// Destroy the context if we already created one
			if (gEglContext)
			{
				eglDestroyContext(gEglDisplay, gEglContext);
			}
			// Attempt to create a context
			gEglContext = eglCreateContext(gEglDisplay, gEglConfig, NULL, NULL);
			if (gEglContext == EGL_NO_CONTEXT)
			{
				if(m_pShell->m_pShellData->bNeedPbuffer)
				{
					// Disable P-buffer and try again
					m_pShell->m_pShellData->bNeedPbuffer = false;
				}
				else
				{
					m_pShell->PVRShellOutputDebug("PVRShell: Unable to create a context");
					return false;
				}
			}
		} while(gEglContext == EGL_NO_CONTEXT);


		if(m_pShell->m_pShellData->bNeedPixmap)
		{
			NativePixmapType npt = (NativePixmapType)OsGetNativePixmapType();
			m_pShell->PVRShellOutputDebug("InitAPI() Using pixmaps, about to create egl surface");
			gEglWindow = eglCreatePixmapSurface(gEglDisplay, gEglConfig, npt, NULL);
		}
		else
		{
			NativeWindowType nwt = (NativeWindowType)OsGetNativeWindowType();
// 			_ASSERT(GetForegroundWindow() == m_hWnd);
			gEglWindow = eglCreateWindowSurface(gEglDisplay, gEglConfig, nwt, NULL);
// 			_ASSERT(GetForegroundWindow() == m_hWnd);
		}

		if (gEglWindow == EGL_NO_SURFACE)
		{
			m_pShell->PVRShellOutputDebug("PVRShell: Unable to create surface");
			return false;
		}

		if (!eglMakeCurrent(gEglDisplay, gEglWindow, gEglWindow, gEglContext))
		{
			if((eglGetError() == EGL_CONTEXT_LOST_IMG) && powerManagementSupported)
			{
				bDone = false;
			}
			else
			{
				m_pShell->PVRShellOutputDebug("PVRShell: Unable to make context current");
				return false;
			}
		}
	} while(!bDone);

	/*
		Done - activate requested features
	*/
	ApiActivatePreferences();
	return true;
}

/*******************************************************************************
 * Function Name  : ReleaseAPI
 * Inputs		  : None
 * Returns        : Nothing
 * Description    : Clean up when we're done
 *******************************************************************************/
void PVRShellInit::ApiReleaseAPI()
{
	eglSwapBuffers(gEglDisplay, gEglWindow);
	eglMakeCurrent(gEglDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
	eglDestroyContext(gEglDisplay, gEglContext);
	eglDestroySurface(gEglDisplay, gEglWindow);
   	eglTerminate(gEglDisplay);
}

/*******************************************************************************
 * Function Name  : SelectEGLConfiguration
 * Inputs		  : bZ, bP
 * Returns        : EGLConfig
 * Description    : Find the config to use for EGL initialization
 *******************************************************************************/
EGLConfig PVRShellInitAPI::SelectEGLConfiguration(const bool bZ, const bool bP, const int nFSAA)
{
    EGLint		num_config;
    EGLint		conflist[32];
	EGLConfig	conf;
    int			i;

    i = 0;

	/* Select default configuration */
    conflist[i++] = EGL_LEVEL;
    conflist[i++] = 0;

    conflist[i++] = EGL_NATIVE_RENDERABLE;
    conflist[i++] = EGL_FALSE;

	if(bZ)
	{
		conflist[i++] = EGL_DEPTH_SIZE;
		conflist[i++] = 16;
	}

	conflist[i++] = EGL_SURFACE_TYPE;
	conflist[i] = EGL_WINDOW_BIT;

	if(bP)
	{
		conflist[i] |= EGL_PBUFFER_BIT;
	}
	i++;

	// Append number of number buffers depending on FSAA mode selected 
	switch(nFSAA)
	{
		case 1:
			conflist[i++] = EGL_SAMPLE_BUFFERS;
			conflist[i++] = 1;
			conflist[i++] = EGL_SAMPLES;
			conflist[i++] = 2;
		break;

		case 2:
			conflist[i++] = EGL_SAMPLE_BUFFERS;
			conflist[i++] = 1;
			conflist[i++] = EGL_SAMPLES;
			conflist[i++] = 4;
		break;

		default:
			conflist[i++] = EGL_SAMPLE_BUFFERS;
			conflist[i++] = 0;
	}

	/* Terminate the list with EGL_NONE */
	conflist[i++] = EGL_NONE;

	/* Return null config if config is not found */
    if (!eglChooseConfig(gEglDisplay, conflist, &conf, 1, &num_config) || num_config != 1)
	{
		return 0;
    }

	/* Return config index */
    return conf;
}

bool PVRShellInit::ApiScreenCaptureBuffer(int Width,int Height,unsigned char *pBuf)
{
	unsigned char	*pLines2;
	int				i,j;
	bool			bRet = true;

#ifdef EGL_VERSION_1_2
	if(eglQueryAPI() != EGL_OPENGL_ES_API)
	{
		m_pShell->PVRShellOutputDebug("PVRShell: Screen capture failed. OpenGL ES is not the current API");
		return false;
	}
#endif

	/* Allocate memory for line */
	pLines2=(unsigned char *)calloc(4*Width*Height, sizeof(unsigned char));
	if (!pLines2) return false;

	while (glGetError());
	/* Read line from frame buffer */
	glReadPixels(0, 0, Width, Height, GL_RGBA, GL_UNSIGNED_BYTE, pLines2);

	if(glGetError())
	{
		bRet = false;
	}
	else
	{
		/* Convert RGB to BGR in line */
		for (j=0,i=0; j<4*Width*Height; j+=4,i+=3)
		{
			pBuf[i]=pLines2[j+2];
			pBuf[i+1]=pLines2[j+1];
			pBuf[i+2]=pLines2[j];
		}
	}

	free(pLines2);
	return bRet;
}

void PVRShellInit::ApiRenderComplete()
{
	bool bRes;

	if(m_pShell->m_pShellData->bNeedPixmap)
	{
		/*
			"Clients rendering to single buffered surfaces (e.g. pixmap surfaces)
			should call eglWaitGL before accessing the native pixmap from the client."
		*/
		eglWaitGL();

		// Pixmap support: Copy the rendered pixmap to the display
		if(m_pShell->m_pShellData->bNeedPixmapDisableCopy)
		{
			bRes = true;
		}
		else
		{
			bRes = OsPixmapCopy();
		}
	}
	else
	{
		bRes = (eglSwapBuffers (gEglDisplay, gEglWindow) == EGL_TRUE);
	}

	if(!bRes)
	{
		// check for context loss
		if((eglGetError() == EGL_CONTEXT_LOST_IMG) && powerManagementSupported)
		{
			m_pShell->ReleaseView();

			OsDoReleaseAPI();
			if(ApiInitAPI())
			{
				m_pShell->InitView();
			}
		}
		else
		{
			m_pShell->PVRShellOutputDebug("eglSwapBuffers failed\n");
		}
	}
}

/****************************************************************************
** Get
****************************************************************************/
bool PVRShellInit::ApiGet(const prefNamePtrEnum prefName, void **pp)
{
	return false;
}

/****************************************************************************
** Local code
****************************************************************************/

#ifndef EGL_VERSION_1_1
// The recommended technique for querying OpenGL extensions;
// adapted from http://opengl.org/resources/features/OGLextensions/
static bool PVRShellIsExtensionSupported(EGLDisplay dpy, const char *extension)
{
    const char *extensions = NULL;
    const char *start;
    char *where, *terminator;

    /* Extension names should not have spaces. */
    where = (char *) strchr(extension, ' ');
    if (where || *extension == '\0')
        return false;

    extensions = eglQueryString(dpy,EGL_EXTENSIONS);

    /* It takes a bit of care to be fool-proof about parsing the
    OpenGL extensions string. Don't be fooled by sub-strings, etc. */
    start = extensions;
    for (;;)
	{
        where = (char *) strstr((const char *) start, extension);
        if (!where)
            break;
        terminator = where + strlen(extension);
        if (where == start || *(where - 1) == ' ')
            if (*terminator == ' ' || *terminator == '\0')
                return true;
        start = terminator;
    }
    return false;
}
#endif

/*****************************************************************************
 End of file (PVRShellAPI.cpp)
*****************************************************************************/
