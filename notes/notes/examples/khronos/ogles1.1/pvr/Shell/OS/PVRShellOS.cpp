/******************************************************************************

 @File         PVRShellOS.cpp

 @Title        

 @Copyright    Copyright (C) 2004 - 2006 by Imagination Technologies Limited.

 @Platform     Non-windowed support for any Linux

 @Description  Makes programming for 3D APIs easier by wrapping surface
               initialization, Texture allocation and other functions for use by a demo.

******************************************************************************/
#include "PVRShell.h"
#include "PVRShellAPI.h"
#include "PVRShellImpl.h"

#include "s3e.h"

#ifndef CONNAME
#define CONNAME "/dev/tty"
#endif

#ifndef FBNAME
#define FBNAME  "/dev/fb0"
#endif

/*!***************************************************************************
	Defines
*****************************************************************************/
#ifndef SHELL_DISPLAY_DIM_X
	#define SHELL_DISPLAY_DIM_X	240
#endif
#ifndef SHELL_DISPLAY_DIM_Y
	#define SHELL_DISPLAY_DIM_Y	320
#endif

/*!***************************************************************************
	Declarations
*****************************************************************************/

PVRShellKeyName	s_CurrentKey;
uint64	s_AppStartTime;

/*!***************************************************************************
	Class: PVRShellInit
*****************************************************************************/

/*
	OS functionality
*/

/************************************************************************/
/* s3e keyboard handling                                                */
/************************************************************************/
int32 Mys3eKeyboardCallback(void *systemData, void *userData)
{
    s3eKeyboardEvent *event = (s3eKeyboardEvent *)systemData;

    if (event->m_Pressed)
    {
        switch(event->m_Key)
        {
            case s3eKeyEsc:
            case s3eKeyRSK:
            case s3eKeyLSK:
                s_CurrentKey = PVRShellKeyNameQUIT;
                break;
            case s3eKeyAbsGameB:	s_CurrentKey = PVRShellKeyNameScreenshot;	break;
            case s3eKeyAbsOk:		s_CurrentKey = PVRShellKeyNameSELECT;	break;
            case s3eKeyAbsGameA:	s_CurrentKey = PVRShellKeyNameACTION1;	break;
            case s3eKeyAbsUp:		s_CurrentKey = PVRShellKeyNameUP;	break;
            case s3eKeyAbsDown:		s_CurrentKey = PVRShellKeyNameDOWN;	break;
            case s3eKeyAbsLeft:		s_CurrentKey = PVRShellKeyNameLEFT;	break;
            case s3eKeyAbsRight:	s_CurrentKey = PVRShellKeyNameRIGHT;	break;
        }
    }

	return S3E_RESULT_SUCCESS;
}

/*!***********************************************************************
 @Function		OsInit
 @description	Initialisation for OS-specific code.
*************************************************************************/
void PVRShellInit::OsInit()
{
	// register s3e keyboard callback
	s3eKeyboardRegister(S3E_KEYBOARD_KEY_EVENT, Mys3eKeyboardCallback, NULL);

	// store app start time
	s_AppStartTime = s3eTimerGetMs();

	SetDataPath("/");
}

/*!***********************************************************************
 @Function		OsInitOS
 @description	Saves instance handle and creates main window
				In this function, we save the instance handle in a global variable and
				create and display the main program window.
*************************************************************************/
bool PVRShellInit::OsInitOS()
{
	// Initialize global strings

	m_pShell->m_pShellData->bFullScreen = 1;
	m_pShell->m_pShellData->nShellDimX = s3eSurfaceGetInt(S3E_SURFACE_DEVICE_WIDTH);
	m_pShell->m_pShellData->nShellDimY = s3eSurfaceGetInt(S3E_SURFACE_DEVICE_HEIGHT);

	m_pShell->m_pShellData->bIsRotated = m_pShell->m_pShellData->nShellDimY > m_pShell->m_pShellData->nShellDimX;

	return true;
}

/*!***********************************************************************
 @Function		OsReleaseOS
 @description	Destroys main window
*************************************************************************/
void PVRShellInit::OsReleaseOS()
{
}

/*!***********************************************************************
 @Function		OsExit
 @description	Destroys main window
*************************************************************************/
void PVRShellInit::OsExit()
{
	/*
		Show the exit message to the user
	*/
	m_pShell->PVRShellOutputDebug((const char*)m_pShell->PVRShellGet(prefExitMessage));
}

/*!***********************************************************************
 @Function		OsDoInitAPI
 @Return		true on success
 @description	Perform GL initialization and bring up window / fullscreen
*************************************************************************/
bool PVRShellInit::OsDoInitAPI()
{
	// Pixmap support: create the pixmap
	if(m_pShell->m_pShellData->bNeedPixmap)
	{
	}

	if(!ApiInitAPI())
	{
		return false;
	}

	// Pixmap support: select the pixmap into a device context (DC) ready for blitting
	if(m_pShell->m_pShellData->bNeedPixmap)
	{
	}

	/* No problem occured */
	return true;
}

/*!***********************************************************************
 @Function		OsDoReleaseAPI
 @description	Clean up after we're done
*************************************************************************/
void PVRShellInit::OsDoReleaseAPI()
{
	ApiReleaseAPI();

	if(m_pShell->m_pShellData->bNeedPixmap)
	{
		// Pixmap support: free the pixmap
	}
}

/*!***********************************************************************
 @Function		OsRenderComplete
 @Returns		false when the app should quit
 @description	Main message loop / render loop
*************************************************************************/
void PVRShellInit::OsRenderComplete()
{
    if (s3eDeviceCheckQuitRequest())
    {
        nLastKeyPressed = PVRShellKeyNameQUIT;
        return;
    }
    nLastKeyPressed = s_CurrentKey;
}

/*!***********************************************************************
 @Function		OsPixmapCopy
 @Return		true if the copy succeeded
 @description	When using pixmaps, copy the render to the display
*************************************************************************/
bool PVRShellInit::OsPixmapCopy()
{
	return false;
}

/*!***********************************************************************
 @Function		OsGetNativeDisplayType
 @Return		The 'NativeDisplayType' for EGL
 @description	Called from InitAPI() to get the NativeDisplayType
*************************************************************************/
void *PVRShellInit::OsGetNativeDisplayType()
{
	return 0;
}

/*!***********************************************************************
 @Function		OsGetNativePixmapType
 @Return		The 'NativePixmapType' for EGL
 @description	Called from InitAPI() to get the NativePixmapType
*************************************************************************/
void *PVRShellInit::OsGetNativePixmapType()
{
	// Pixmap support: return the pixmap
	return 0;
}

/*!***********************************************************************
 @Function		OsGetNativeWindowType
 @Return		The 'NativeWindowType' for EGL
 @description	Called from InitAPI() to get the NativeWindowType
*************************************************************************/
void *PVRShellInit::OsGetNativeWindowType()
{
	return s3eGLGetNativeWindow();
}

/*!***********************************************************************
 @Function		OsGet
 @Input			prefName		value to retrieve
 @Output		pp				pointer to which to write the value
 @Description	Retrieves OS-specific data
*************************************************************************/
bool PVRShellInit::OsGet(const prefNamePtrEnum prefName, void **pp)
{
	return false;
}

/*!***********************************************************************
 @Function		OsDisplayDebugString
 @Input			str		string to output
 @Description	Prints a debug string
*************************************************************************/
void PVRShellInit::OsDisplayDebugString(char const * const str)
{
	s3eDebugOutputString(str);
}

/*!***********************************************************************
 @Function		OsGetTime
 @Return		Time in milliseconds since the beginning of the application
 @Description	Gets the time in milliseconds since the beginning of the application
*************************************************************************/
unsigned long PVRShellInit::OsGetTime()
{
	return (uint32)(s3eTimerGetMs() - s_AppStartTime);
}

/*****************************************************************************
 Class: PVRShellInitOS
*****************************************************************************/

/*****************************************************************************
 Global code
*****************************************************************************/

int main()
{
	PVRShellInit init;

	/*
		Create the demo, process the command line, create the OS initialiser.
	*/
	PVRShell *pDemo = NewDemo();
	if(!pDemo)
		return -1;

	init.Init(*pDemo);
	init.CommandLine(0, NULL);

	/*
		Initialise/run/shutdown
	*/
	while (init.Run())
	{
		s3eDeviceYield(0);
		s3eKeyboardUpdate();
	}

	delete pDemo;
	return 0;
}

/*****************************************************************************
 End of file (PVRShellOS.cpp)
*****************************************************************************/
