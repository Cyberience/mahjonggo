/******************************************************************************

 @File         PVRShell.cpp

 @Title        

 @Copyright    Copyright (C) 2005 - 2006 by Imagination Technologies Limited.

 @Platform     Independant

 @Description  Makes programming for 3D APIs easier by wrapping surface
               initialization, Texture allocation and other functions for use by a demo.

******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "PVRShell.h"
#include "PVRShellAPI.h"
#include "PVRShellImpl.h"

// This file simply defines a version string. It can be commented out.
#ifndef PVRSDK_VERSION
#define PVRSDK_VERSION "n.nn.nn.nnnn"
#endif

// Define to automatically stop the app after x frames
#ifndef PVRSHELL_QUIT_AFTER_FRAME 			// So that this can be re-specified at the build level
#define PVRSHELL_QUIT_AFTER_FRAME -1		// # of frames. If negative, run forever
#endif

// Define to automatically stop the app after x amount of time
#ifndef PVRSHELL_QUIT_AFTER_TIME			// So that this can be re-specified at the build level
#define PVRSHELL_QUIT_AFTER_TIME -1			// # of seconds. If negative, run forever
#endif

#define PVRSHELL_SCREENSHOT_NAME	"PVRShell"

#define PVRSHELL_DIR_SYM	'/'

/*****************************************************************************
** Prototypes
*****************************************************************************/
static bool StringCopy(char *&pszStr, const char * const pszSrc);

/****************************************************************************
** Class: PVRShell
****************************************************************************/

/*!***********************************************************************
@Function			PVRShell
@Description		Constructor
*************************************************************************/
PVRShell::PVRShell()
{
	m_pShellInit = NULL;
	m_pShellData = new PVRShellData;

	m_pShellData->nShellPosX=0;
	m_pShellData->nShellPosY=0;

	/* WinCE, PocketPC and linux builds go fullscreen by default */
#if defined(__linux__)
	m_pShellData->bFullScreen=true;
#else
	m_pShellData->bFullScreen=false;
#endif

	m_pShellData->nFSAAMode=0;
	m_pShellData->bIsRotated=false;

	m_pShellData->nDieAfterFrames=PVRSHELL_QUIT_AFTER_FRAME;
	m_pShellData->fDieAfterTime=PVRSHELL_QUIT_AFTER_TIME;

	m_pShellData->bNeedPbuffer = false;
	m_pShellData->bNeedPixmap = false;
	m_pShellData->bNeedPixmapDisableCopy = false;
	m_pShellData->bNeedZbuffer = true;
	m_pShellData->bLockableBackBuffer = false;
	m_pShellData->bSoftwareRender = false;
	m_pShellData->bNeedStencilBuffer = false;
	m_pShellData->bNeedOpenVG = false;

	m_pShellData->pszAppName = 0;
	m_pShellData->pszExitMessage = 0;

	m_pShellData->nSwapInterval = 1;
	m_pShellData->nInitRepeats = 0;

	// Internal Data
	m_pShellData->bShellPosWasDefault = true;
	m_pShellData->nShellCurFrameNum = 0;
}

/*!***********************************************************************
@Function			~PVRShell
@Description		Destructor
*************************************************************************/
PVRShell::~PVRShell()
{
	delete m_pShellData;
	m_pShellData = NULL;
}

// Allow user to set preferences from within InitApplication

bool PVRShell::PVRShellSet(const prefNameBoolEnum prefName, const bool value)
{
	switch(prefName)
	{
		case prefFullScreen:
			m_pShellData->bFullScreen = value;
			return true;

		case prefPBufferContext:
			m_pShellData->bNeedPbuffer = value;
			return true;

		case prefPixmapContext:
			m_pShellData->bNeedPixmap = value;
			return true;

		case prefPixmapDisableCopy:
			m_pShellData->bNeedPixmapDisableCopy = value;
			return true;

		case prefZbufferContext:
			m_pShellData->bNeedZbuffer = value;
			return true;

		case prefLockableBackBuffer:
			m_pShellData->bLockableBackBuffer = value;
			return true;

		case prefSoftwareRendering:
			m_pShellData->bSoftwareRender = value;
			return true;

		case prefStencilBufferContext:
			m_pShellData->bNeedStencilBuffer = value;
			return true;

		case prefOpenVGContext:
			m_pShellData->bNeedOpenVG = value;
			return true;

		default:
			break;
	}
	return false;
}

bool PVRShell::PVRShellGet(const prefNameBoolEnum prefName) const
{
	switch(prefName)
	{
		case prefFullScreen:	return m_pShellData->bFullScreen;
		case prefIsRotated:	return m_pShellData->bIsRotated;
		case prefPBufferContext:	return m_pShellData->bNeedPbuffer;
		case prefPixmapContext:	return m_pShellData->bNeedPixmap;
		case prefPixmapDisableCopy:	return m_pShellData->bNeedPixmapDisableCopy;
		case prefZbufferContext:	return m_pShellData->bNeedZbuffer;
		case prefLockableBackBuffer:	return m_pShellData->bLockableBackBuffer;
		case prefSoftwareRendering:	return m_pShellData->bSoftwareRender;
		case prefStencilBufferContext:	return m_pShellData->bNeedStencilBuffer;
		case prefOpenVGContext:	return m_pShellData->bNeedOpenVG;
		default:	return false;
	}
}

bool PVRShell::PVRShellSet(const prefNameFloatEnum prefName, const float value)
{
	switch(prefName)
	{
		case prefQuitAfterTime:
			m_pShellData->fDieAfterTime = value;
			return true;

		default:
			break;
	}
	return false;
}

float PVRShell::PVRShellGet(const prefNameFloatEnum prefName) const
{
	switch(prefName)
	{
		case prefQuitAfterTime:	return m_pShellData->fDieAfterTime;
		default:	return -1;
	}
}

bool PVRShell::PVRShellSet(const prefNameIntEnum prefName, const int value)
{
	switch(prefName)
	{
		case prefWidth:
			if(value > 0)
			{
				m_pShellData->nShellDimX = value;
				return true;
			}
			return false;

		case prefHeight:
			if(value > 0)
			{
				m_pShellData->nShellDimY = value;
				return true;
			}
			return false;

		case prefPositionX:
			m_pShellData->bShellPosWasDefault = false;
			m_pShellData->nShellPosX = value;
			return true;

		case prefPositionY:
			m_pShellData->bShellPosWasDefault = false;
			m_pShellData->nShellPosY = value;
			return true;

		case prefQuitAfterFrame:
			m_pShellData->nDieAfterFrames = value;
			return true;

		case prefSwapInterval:
#if defined(EGL_VERSION_1_0) && !defined(EGL_VERSION_1_1)
			return false;
#endif
			m_pShellData->nSwapInterval = value;
			return true;

		case prefInitRepeats:
			m_pShellData->nInitRepeats = value;
			return true;

		case prefFSAAMode:
			if(value >= 0 && value <= 2)
			{
				m_pShellData->nFSAAMode = value;
				return true;
			}
			return false;

		default:
			break;
	}
	return false;
}

int PVRShell::PVRShellGet(const prefNameIntEnum prefName) const
{
	switch(prefName)
	{
		case prefWidth:	return m_pShellData->nShellDimX;
		case prefHeight:	return m_pShellData->nShellDimY;
		case prefPositionX:	return m_pShellData->nShellPosX;
		case prefPositionY:	return m_pShellData->nShellPosY;
		case prefQuitAfterFrame:	return m_pShellData->nDieAfterFrames;
		case prefSwapInterval:	return m_pShellData->nSwapInterval;
		case prefInitRepeats:	return m_pShellData->nInitRepeats;
		case prefFSAAMode:	return m_pShellData->nFSAAMode;
		case prefCommandLineOptNum:	return m_pShellInit->m_CommandLine.m_nOptLen;
		default:	return -1;
	}
}

bool PVRShell::PVRShellSet(const prefNamePtrEnum prefName, const void * const ptrValue)
{
	return false;
}

void *PVRShell::PVRShellGet(const prefNamePtrEnum prefName) const
{
	switch(prefName)
	{
	case prefNativeWindowType:	return m_pShellInit->OsGetNativeWindowType();
	default:
		{
			void *p;

			if(m_pShellInit->ApiGet(prefName, &p))
				return p;
			if(m_pShellInit->OsGet(prefName, &p))
				return p;
			return NULL;
		}
	}
}

bool PVRShell::PVRShellSet(const prefNameConstPtrEnum prefName, const void * const ptrValue)
{
	switch(prefName)
	{
	case prefAppName:
		StringCopy(m_pShellData->pszAppName, (char*)ptrValue);
		return true;
	case prefExitMessage:
		StringCopy(m_pShellData->pszExitMessage, (char*)ptrValue);
		return true;
	default:
		break;
	}
	return false;
}

const void *PVRShell::PVRShellGet(const prefNameConstPtrEnum prefName) const
{
	switch(prefName)
	{
	case prefAppName:
		return m_pShellData->pszAppName;
	case prefExitMessage:
		return m_pShellData->pszExitMessage;
	case prefDataPath:
		return m_pShellInit->GetDataPath();
	case prefCommandLine:
		return m_pShellInit->m_CommandLine.m_psOrig;
	case prefCommandLineOpts:
		return m_pShellInit->m_CommandLine.m_pOpt;
	case prefVersion:
		return PVRSDK_VERSION;
	default:
		return 0;
	}
}

// Screen capture

bool PVRShell::PVRShellScreenCaptureBuffer(const int Width, const int Height, unsigned char **pLines)
{
	int Pitch;

	/* Compute line pitch */
	Pitch = 3*Width;
	if ( ((3*Width)%4)!=0 )
	{
		Pitch += 4-((3*Width)%4);
	}

	/* Allocate memory for line */
	*pLines=(unsigned char *)calloc(Pitch*Height, sizeof(unsigned char));
	if (!(*pLines)) return false;

	return m_pShellInit->ApiScreenCaptureBuffer(Width, Height, *pLines);
}

int PVRShell::PVRShellScreenSave(
	const char			* const fname,
	const unsigned char	* const pLines,
	char				* const ofname)
{
	FILE		*file = 0;
	const char	*pszDataPath;
	char		*pszFileName;
	int			nScreenshotCount;
	int			dwWidth	= m_pShellData->nShellDimX;
	int			dwHeight= m_pShellData->nShellDimY;
	int 		error;

	pszDataPath = (const char*)PVRShellGet(prefDataPath);
	pszFileName = (char*)malloc(strlen(pszDataPath) + 200);

	/* Look for the first file name that doesn't already exist */
	for(nScreenshotCount = 0; nScreenshotCount < 10000; ++nScreenshotCount)
	{
		sprintf(pszFileName, "%s\\%s%04d.bmp", pszDataPath, fname, nScreenshotCount);
		file = (FILE *)fopen(pszFileName,"r");
		if(!file)
			break;
		fclose(file);
	}

	/* If all files already exist, replace the first one */
	if (nScreenshotCount==10000)
	{
		sprintf(pszFileName, "%s\\%s0000.bmp", pszDataPath, fname);
		PVRShellOutputDebug("PVRShell: *WARNING* : Overwriting %s\n", pszFileName);
	}

	if(ofname)	// requested the output file name
	{
		strcpy(ofname, pszFileName);
	}

	error = PVRShellWriteBMPFile(pszFileName, dwWidth, dwHeight, pLines);
	FREE(pszFileName);
	if (error)
	{
		return 10*error+1;
	}
	else
	{
		// No problem occured
		return 0;
	}
}

#define BMPHEADERSIZE	(14)
#define BMPINFOSIZE		(40)

int PVRShell::PVRShellWriteBMPFile(
	const char			* const pszFilename,
	const unsigned long	uWidth,
	const unsigned long	uHeight,
	const void			* const pImageData)
{
	int			Result = 1;
	FILE*		fpDumpfile = 0;

	unsigned int		uBitsPerPixel = 24;
	unsigned long		uColours = 1 << 24;
	unsigned long		uImageSize = 0;

	fpDumpfile = (FILE *)fopen(pszFilename, "wb");

	uImageSize =  (uWidth * uHeight * 24) >> 3;

	if (fpDumpfile != 0)
	{
		unsigned long	BMPFileHeader[(BMPHEADERSIZE + BMPINFOSIZE + 7) >> 2];
		unsigned char*	pRealHeaderStart = 0;
		unsigned short*	pHeaderShort = 0;
		unsigned long*	pHeaderLong = 0;
		unsigned long	uBytesWritten = 0;

		/* Write BMP header structure */

		/* To make sure that 32-bit accesses are 32-bit aligned, we have to be a bit tricky */
 		pRealHeaderStart = ((unsigned char*)&BMPFileHeader) + 2;	/* Start on 16-bit alignment */
 		pRealHeaderStart[0]	= 'B';
 		pRealHeaderStart[1]	= 'M';

		pHeaderLong = (unsigned long*)&pRealHeaderStart[2];
		*(pHeaderLong++)	= BMPHEADERSIZE + BMPINFOSIZE + uImageSize;	/* File size */

		pHeaderShort = (unsigned short*)pHeaderLong;
		*(pHeaderShort++)	= 0;	/* Reserved */
		*(pHeaderShort++)	= 0;	/* Reserved */

		pHeaderLong = (unsigned long*)pHeaderShort;
		*(pHeaderLong++)	= BMPHEADERSIZE + BMPINFOSIZE;	/* Offset to image data from SOF */

		/* Append BMP info structure */
		*(pHeaderLong++)	= BMPINFOSIZE;		/* Bitmap info header size */
		*(pHeaderLong++)	= uWidth;
		*(pHeaderLong++)	= uHeight;

		pHeaderShort = (unsigned short*)pHeaderLong;
		*(pHeaderShort++)	= 1;				/* Colour planes */
		*(pHeaderShort++)	= uBitsPerPixel;	/* uBitsPerPixel */

		pHeaderLong = (unsigned long*)pHeaderShort;
		*(pHeaderLong++)	= 0;				/* No compression */
		*(pHeaderLong++)	= uImageSize;
		*(pHeaderLong++)	= 4096;				/* Arbitrary pixels per metre */
		*(pHeaderLong++)	= 4096;
		*(pHeaderLong++)	= uColours;			/* Number of colours */
		*(pHeaderLong)		= 0;				/* Important colours (?) */

		uBytesWritten = (unsigned long)fwrite(pRealHeaderStart, 1, BMPHEADERSIZE + BMPINFOSIZE, fpDumpfile);

		if (uBytesWritten == (BMPHEADERSIZE + BMPINFOSIZE))
		{
			uBytesWritten = (unsigned long)fwrite(pImageData, 1, uImageSize, fpDumpfile);

			if (uBytesWritten == uImageSize)
			{
				Result = 0;
			}
		}
		else
		{
			PVRShellOutputDebug("PVRShell: Wrote wrong number of bytes to BMP file.\n");
		}

		fclose(fpDumpfile);
	}
	else
	{
		PVRShellOutputDebug("PVRShell: Failed to open \"%s\" for writing screen dump.\n", pszFilename);
	}

	return Result;
}

void PVRShell::PVRShellOutputDebug(char const * const format, ...) const
{
	if (!format)
		return;
	
	va_list arg;
	char	buf[1024];

	va_start(arg, format);
	vsprintf(buf, format, arg);
	va_end(arg);

	/* Passes the data to a platform dependant function */
	m_pShellInit->OsDisplayDebugString(buf);
}

unsigned long PVRShell::PVRShellGetTime()
{
	/* Read timer from a platform dependant function */
	return m_pShellInit->OsGetTime();
}

bool PVRShell::PVRShellIsKeyPressed(const PVRShellKeyName key)
{
	if(!m_pShellInit)
		return false;
	return m_pShellInit->DoIsKeyPressed(key);
}

/****************************************************************************
** Class: PVRShellCommandLine
****************************************************************************/
PVRShellCommandLine::PVRShellCommandLine()
{
	memset(this, 0, sizeof(*this));
}

PVRShellCommandLine::~PVRShellCommandLine()
{
	delete [] m_psOrig;
	delete [] m_psSplit;
	FREE(m_pOpt);
}

void PVRShellCommandLine::Parse(const char *pStr)
{
	size_t		len;
	int			nIn, nOut;
	bool		bInQuotes;
	SCmdLineOpt	opt;

	// Take a copy of the original
	len = strlen(pStr)+1;
	m_psOrig = new char[len];
	strcpy(m_psOrig, pStr);

	// Take a copy to be edited
	m_psSplit = new char[len];

	// Break the command line into options
	bInQuotes = false;
	opt.pArg = NULL;
	opt.pVal = NULL;
	nIn = -1;
	nOut = 0;
	do
	{
		++nIn;
		if(pStr[nIn] == '"')
		{
			bInQuotes = !bInQuotes;
		}
		else
		{
			if(bInQuotes && pStr[nIn] != 0)
			{
				if(!opt.pArg)
					opt.pArg = &m_psSplit[nOut];

				m_psSplit[nOut++] = pStr[nIn];
			}
			else
			{
				switch(pStr[nIn])
				{
				case '=':
					m_psSplit[nOut++] = 0;
					opt.pVal = &m_psSplit[nOut];
					break;

				case ' ':
				case '\t':
				case '\0':
					m_psSplit[nOut++] = 0;
					if(opt.pArg || opt.pVal)
					{
						// Increase list length if necessary
						if(m_nOptLen == m_nOptMax)
							m_nOptMax = m_nOptMax * 2 + 1;
						m_pOpt = (SCmdLineOpt*)realloc(m_pOpt, m_nOptMax * sizeof(*m_pOpt));
						if(!m_pOpt)
							return;

						// Add option to list
						m_pOpt[m_nOptLen++] = opt;
						opt.pArg = NULL;
						opt.pVal = NULL;
					}
					break;

				default:
					if(!opt.pArg)
						opt.pArg = &m_psSplit[nOut];

					m_psSplit[nOut++] = pStr[nIn];
					break;
				}
			}
		}
	} while(pStr[nIn]);

	/*
		Debug output (works on Windows builds)
	*/
}

void PVRShellCommandLine::Apply(PVRShell &shell)
{
	int i;
	const char *arg, *val;

	for(i = 0; i < m_nOptLen; ++i)
	{
		arg = m_pOpt[i].pArg;
		val = m_pOpt[i].pVal;

		if(!arg)
			continue;

		if(val)
		{
			if(strcmp(arg, "-width") == 0)
			{
				shell.PVRShellSet(prefWidth, atoi(val));
			}
			else if(strcmp(arg, "-height") == 0)
			{
				shell.PVRShellSet(prefHeight, atoi(val));
			}
			else if(strcmp(arg, "-FSAAMode") == 0 || strcmp(arg, "-aa") == 0)
			{
				shell.PVRShellSet(prefFSAAMode, atoi(val));
			}
			else if(strcmp(arg, "-fullscreen") == 0)
			{
				shell.PVRShellSet(prefFullScreen, (atoi(val) != 0));
			}
			else if(strcmp(arg, "-sw") == 0)
			{
				shell.PVRShellSet(prefSoftwareRendering, (atoi(val) != 0));
			}
			else if(strcmp(arg, "-quitafterframe") == 0 || strcmp(arg, "-qaf") == 0)
			{
				shell.PVRShellSet(prefQuitAfterFrame, atoi(val));
			}
			else if(strcmp(arg, "-quitaftertime") == 0 || strcmp(arg, "-qat") == 0)
			{
				shell.PVRShellSet(prefQuitAfterTime, (float)atof(val));
			}
			else if(strcmp(arg, "-posx") == 0)
			{
				shell.PVRShellSet(prefPositionX, atoi(val));
			}
			else if(strcmp(arg, "-posy") == 0)
			{
				shell.PVRShellSet(prefPositionY, atoi(val));
			}
			else if(strcmp(arg, "-vsync") == 0)
			{
				shell.PVRShellSet(prefSwapInterval, atoi(val));
			}
		}
		else
		{
			if(strcmp(arg, "-version") == 0)
			{
				shell.PVRShellOutputDebug("Version: \"%s\"\n", shell.PVRShellGet(prefVersion));
			}
		}
	}
}

/****************************************************************************
** Class: PVRShellInit
****************************************************************************/
PVRShellInit::PVRShellInit()
{
	memset(this, 0, sizeof(*this));
}

PVRShellInit::~PVRShellInit()
{
	delete [] m_pDataPath;
	m_pDataPath = NULL;
}

void PVRShellInit::Init(PVRShell &Shell)
{
	m_pShell				= &Shell;
	m_pShell->m_pShellInit	= this;

	OsInit();

	gShellDone = false;
	m_eState = ePVRShellInitApp;
}

void PVRShellInit::CommandLine(char *str)
{
	m_CommandLine.Parse(str);
}

void PVRShellInit::CommandLine(int argc, char **argv)
{
	size_t	tot, len;
	char	*buf;
	int		i;

	tot = 0;
	for(i = 0; i < argc; ++i)
		tot += strlen(argv[i]);

	if(!tot)
	{
		CommandLine("");
		return;
	}

	// Add room for spaces and the \0
	tot += argc;

	buf = new char[tot];
	tot = 0;
	for(i = 0; i < argc; ++i)
	{
		len = strlen(argv[i]);
		strncpy(&buf[tot], argv[i], len);
		tot += len;
		buf[tot++] = ' ';
	}
	buf[tot-1] = 0;

	CommandLine(buf);

	delete [] buf;
}

/*****************************************************************************
* Function Name  : DoIsKeyPressed
* Inputs		 : key
* Returns        : Is the key pressed
* Description    : Check if a key is pressed
*****************************************************************************/
bool PVRShellInit::DoIsKeyPressed(const PVRShellKeyName key)
{
	if(key == nLastKeyPressed)
	{
		nLastKeyPressed = PVRShellKeyNameNull;
		return true;
	}
	else
	{
		return false;
	}
}

/*******************************************************************************
 * Function Name  : KeyPressed
 * Description    : Set the last set key
 *******************************************************************************/
void PVRShellInit::KeyPressed(PVRShellKeyName nKey)
{
	nLastKeyPressed = nKey;
}

/*****************************************************************************
* Function Name  : GetDataPath
* Inputs		 : size
* Returns        : the number of bytes copied, or 0 if the buffer is not large enough
* Description    : Gets the current data path (the user has allocated size bytes)
*****************************************************************************/
const char *PVRShellInit::GetDataPath() const
{
	return m_pDataPath;
}

/*******************************************************************************
 * Function Name  : SetAppName
 * Description    : Sets the default app name
 *******************************************************************************/
void PVRShellInit::SetAppName(const char * const str)
{
	const char *pName = strrchr(str, PVRSHELL_DIR_SYM);
	if(pName)
	{
		++pName;
	}
	else
	{
		pName = str;
	}
	m_pShell->PVRShellSet(prefAppName, pName);
}

/*******************************************************************************
 * Function Name  : SetDataPath
 * Description    : Sets the data path for GetDataPath()
 *******************************************************************************/
void PVRShellInit::SetDataPath(const char * const str)
{
	m_pDataPath = new char[strlen(str)+1];
	if(m_pDataPath)
	{
		strcpy(m_pDataPath, str);
		char* lastSlash = strrchr(m_pDataPath, PVRSHELL_DIR_SYM);
		lastSlash[1] = 0;
	}
}

/*****************************************************************************
* Function Name  : Run
* Returns        : false when the app should quit
* Description    : Main message loop / render loop
*****************************************************************************/
bool PVRShellInit::Run()
{
	static unsigned long StartTime = 0;

	switch(m_eState)
	{
	case ePVRShellInitApp:
		if(!m_pShell->InitApplication())
		{
			m_eState = ePVRShellExit;
			return true;
		}

	case ePVRShellInitInstance:
		m_CommandLine.Apply(*m_pShell);

		// Perform OS initialisation
		if (!OsInitOS())
		{
			m_pShell->PVRShellOutputDebug("InitOS failed!\n");
			m_eState = ePVRShellQuitApp;
			return true;
		}

		// Initialize the 3D API
		if(!OsDoInitAPI())
		{
			m_pShell->PVRShellOutputDebug("InitAPI failed!\n");
			m_eState = ePVRShellReleaseOS;
			gShellDone = true;
			return true;
		}

		// Initialise the app
		if(!m_pShell->InitView())
		{
			m_pShell->PVRShellOutputDebug("InitView failed!\n");
			m_eState = ePVRShellReleaseAPI;
			gShellDone = true;
			return true;
		}

		if (StartTime==0)
		{
			StartTime = OsGetTime();
		}

		m_eState = ePVRShellRender;
		return true;

	case ePVRShellRender:
		{
			// Main message loop:
			if(!m_pShell->RenderScene())
				break;

			ApiRenderComplete();
			OsRenderComplete();

#ifdef PVRSHELL_FPS_OUTPUT
			FpsUpdate();
#endif

			if(DoIsKeyPressed(PVRShellKeyNameScreenshot))
			{
				unsigned char *pBuf;
				m_pShell->PVRShellScreenCaptureBuffer(m_pShell->PVRShellGet(prefWidth), m_pShell->PVRShellGet(prefHeight), &pBuf);
				m_pShell->PVRShellScreenSave(PVRSHELL_SCREENSHOT_NAME, pBuf);
				FREE(pBuf);
			}

			if(DoIsKeyPressed(PVRShellKeyNameQUIT))
				gShellDone = true;

			if(gShellDone)
				break;

			/* Quit if maximum number of allowed frames is reached */
			if((m_pShell->m_pShellData->nDieAfterFrames>=0) && (m_pShell->m_pShellData->nShellCurFrameNum >= m_pShell->m_pShellData->nDieAfterFrames))
				break;

			/* Quit if maximum time is reached */
			if((m_pShell->m_pShellData->fDieAfterTime>=0.0f) && (((OsGetTime()-StartTime)*0.001f) >= m_pShell->m_pShellData->fDieAfterTime))
				break;

			m_pShell->m_pShellData->nShellCurFrameNum++;
			return true;
		}

	case ePVRShellReleaseView:
		m_pShell->ReleaseView();

	case ePVRShellReleaseAPI:
		OsDoReleaseAPI();

	case ePVRShellReleaseOS:
		OsReleaseOS();

		if(!gShellDone && m_pShell->m_pShellData->nInitRepeats)
		{
			--m_pShell->m_pShellData->nInitRepeats;
			m_eState = ePVRShellInitInstance;
			return true;
		}

	case ePVRShellQuitApp:
		// Final app tidy-up
		m_pShell->QuitApplication();

	case ePVRShellExit:
		OsExit();
		StringCopy(m_pShell->m_pShellData->pszAppName, 0);
		StringCopy(m_pShell->m_pShellData->pszExitMessage, 0);
		return false;
	}

	m_eState = (EPVRShellState)(m_eState + 1);
	return true;
}

#ifdef PVRSHELL_FPS_OUTPUT
/*****************************************************************************
* Function Name  : FpsUpdate
* Description    : Calculates a value for frames-per-second (FPS).
*****************************************************************************/
void PVRShellInit::FpsUpdate()
{
	int nTimeDelta, nTime;

	nTime = m_pShell->PVRShellGetTime();
	++m_nFpsFrameCnt;
	nTimeDelta = nTime - m_nFpsTimePrev;

	if(nTimeDelta >= PVRSHELL_FPS_OUTPUT)
	{
		float fFPS = 1000.0f * (float)m_nFpsFrameCnt / (float)nTimeDelta;

		m_pShell->PVRShellOutputDebug("PVRShell: frame %d, FPS %.1f.\n",
			m_pShell->m_pShellData->nShellCurFrameNum, fFPS);

		m_nFpsFrameCnt = 0;
		m_nFpsTimePrev = nTime;
	}
}
#endif

/****************************************************************************
** Local code
****************************************************************************/
static bool StringCopy(char *&pszStr, const char * const pszSrc)
{
	size_t len;

	FREE(pszStr);

	if(!pszSrc)
		return true;

	len = strlen(pszSrc)+1;
	pszStr = (char*)malloc(len);
	if(!pszStr)
		return false;

	strcpy(pszStr, pszSrc);
	return true;
}

/*****************************************************************************
 End of file (PVRShell.cpp)
*****************************************************************************/
