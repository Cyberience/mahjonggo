/******************************************************************************

 @File         PVRShellImpl.h

 @Title        

 @Copyright    Copyright (C) 2005 - 2006 by Imagination Technologies Limited.

 @Platform     Independant

 @Description  Makes programming for 3D APIs easier by wrapping surface
               initialization, Texture allocation and other functions for use by a demo.

******************************************************************************/
#ifndef __PVRSHELLIMPL_H_
#define __PVRSHELLIMPL_H_

/*****************************************************************************
** Build options
*****************************************************************************/

// Time, in milliseconds (ms) between reporting the FPS
//#define PVRSHELL_FPS_OUTPUT		(1000)

/*****************************************************************************
** Macros
*****************************************************************************/
#define FREE(X) { if(X) { free(X); (X)=0; } }

#ifndef _ASSERT
#define _ASSERT(X) /**/
#endif

/*****************************************************************************
** Defines
*****************************************************************************/
#define STR_WNDTITLE (" - Build ")

/*!****************************************************************************
 * @Struct PVRShellData
 * @Brief Holds PVRShell internal data.
 *****************************************************************************/
struct PVRShellData
{
	// Shell Interface Data
	char		*pszAppName;
	char		*pszExitMessage;
	int			nShellDimX;
	int			nShellDimY;
	int			nShellPosX;
	int			nShellPosY;
	bool		bFullScreen;
	bool		bNeedPbuffer;
	bool		bNeedZbuffer;
	bool		bNeedStencilBuffer;
	bool		bNeedPixmap;
	bool		bNeedPixmapDisableCopy;
	bool		bLockableBackBuffer;
	bool		bSoftwareRender;
	bool		bNeedOpenVG;
	int			nSwapInterval;
	int			nInitRepeats;
	int			nDieAfterFrames;
	float		fDieAfterTime;
	int			nFSAAMode;

	// Internal Data
	bool		bIsRotated;
	bool		bShellPosWasDefault;
	int			nShellCurFrameNum;
};

/*!****************************************************************************
 * @Class PVRShellCommandLine
 * @Brief Command-line interpreter
 *****************************************************************************/
class PVRShellCommandLine
{
public:
	char		*m_psOrig, *m_psSplit;
	SCmdLineOpt	*m_pOpt;
	int			m_nOptLen, m_nOptMax;

public:
	PVRShellCommandLine();
	~PVRShellCommandLine();

	void Parse(const char *pStr);
	void Apply(PVRShell &shell);
};

/*!****************************************************************************
 * @Enum  EPVRShellState
 * @Brief Current Shell state
*****************************************************************************/
enum EPVRShellState {
	ePVRShellInitApp,
	ePVRShellInitInstance,
	ePVRShellRender,
	ePVRShellReleaseView,
	ePVRShellReleaseAPI,
	ePVRShellReleaseOS,
	ePVRShellQuitApp,
	ePVRShellExit
};

/*!***************************************************************************
 * @Enum  EPVRShellState
 * @Brief Abstract API and OS code.
 ****************************************************************************/
class PVRShellInit : public PVRShellInitAPI
{
public:
	friend class PVRShell;
	friend class PVRShellInitAPI;

	PVRShell			*m_pShell;		/*!< Our PVRShell class */
	PVRShellCommandLine	m_CommandLine;	/*!< Our Commad-line class */

	bool		gShellDone;				/*!< Indicates that the application has finished */
	EPVRShellState	m_eState;			/*!< Current PVRShell state */

	// Key handling
	PVRShellKeyName	nLastKeyPressed;	/*!< Holds the last key pressed */

	// Data path
	char	*m_pDataPath;				/*!<Holds the path where the application will read the data from */

#ifdef PVRSHELL_FPS_OUTPUT
	// Frames per second (FPS)
	int		m_nFpsFrameCnt, m_nFpsTimePrev;
#endif

public:

    /*!***********************************************************************
	 @Function		PVRShellInit
	 @description	PVRShellInit Constructor
	*************************************************************************/
	PVRShellInit();

	/*!***********************************************************************
	 @Function		~PVRShellInit
	 @description	PVRShellInit Destructor
	*************************************************************************/
	~PVRShellInit();

	/*!***********************************************************************
	 @Function		Init
	 @description	PVRShell Initialisation.
	*************************************************************************/
	void Init(PVRShell &Shell);

	/*!***********************************************************************
	 @Function		CommandLine
	 @description	Receives the command-line from the application.
	*************************************************************************/
	void CommandLine(char *str);
	void CommandLine(int argc, char **argv);

	/*!***********************************************************************
	 @Function		DoIsKeyPressed
	 @description	Return 'true' if the specific key has been pressed.
	*************************************************************************/
	bool DoIsKeyPressed(const PVRShellKeyName key);

	/*!***********************************************************************
	 @Function		KeyPressed
	 @description	Used by the OS-specific code to tell the Shell that a key has been pressed.
	*************************************************************************/
	void KeyPressed(PVRShellKeyName key);

	/*!***********************************************************************
	 @Function		GetDataPath
	 @description	Used by the OS-specific code to tell the Shell where the actual application resides.
	*************************************************************************/
	const char	*GetDataPath() const;

	/*!******************************************************************************
	 @Function	  SetAppName
	 @Description Sets the default app name (to be displayed by the OS)
	*******************************************************************************/
	void SetAppName(const char * const str);

	/*!***********************************************************************
	 @Function		SetDataPath
	 @description	Set the path to where the application expects the data.
	*************************************************************************/
	void SetDataPath(const char * const str);

	/*!***********************************************************************
	 @Function		Run
	 @description	Called from the OS-specific code to perform the render.
					When this fucntion fails the application will quit.
	*************************************************************************/
	bool Run();

#ifdef PVRSHELL_FPS_OUTPUT
	/*!****************************************************************************
	@Function   FpsUpdate
	@Description    Calculates a value for frames-per-second (FPS).
	*****************************************************************************/
	void FpsUpdate();
#endif

	/*
		OS functionality
	*/

	/*!***********************************************************************
	 @Function		OsInit
	 @description	Initialisation for OS-specific code.
	*************************************************************************/
	void		OsInit();

	/*!***********************************************************************
	 @Function		OsInitOS
	 @description	Saves instance handle and creates main window
					In this function, we save the instance handle in a global variable and
					create and display the main program window.
	*************************************************************************/
	bool		OsInitOS();

	/*!***********************************************************************
	 @Function		OsReleaseOS
	 @description	Destroys main window
	*************************************************************************/
	void		OsReleaseOS();

	/*!***********************************************************************
	@Function		OsExit
	@description	Destroys main window
	*************************************************************************/
	void		OsExit();

	/*!***********************************************************************
	 @Function		OsDoInitAPI
	 @description	Perform GL initialization and bring up window / fullscreen
	*************************************************************************/
	bool		OsDoInitAPI();

	/*!***********************************************************************
	 @Function		OsDoReleaseAPI
	 @description	Clean up after we're done
	*************************************************************************/
	void		OsDoReleaseAPI();

	/*!***********************************************************************
	 @Function		OsRenderComplete
	 @description	Main message loop / render loop
	*************************************************************************/
	void		OsRenderComplete();

	/*!***********************************************************************
	 @Function		OsPixmapCopy
	 @description	When using pixmaps, copy the render to the display
	*************************************************************************/
	bool		OsPixmapCopy();

	/*!***********************************************************************
	 @Function		OsGetNativeDisplayType
	 @description	Called from InitAPI() to get the NativeDisplayType
	*************************************************************************/
	void		*OsGetNativeDisplayType();

	/*!***********************************************************************
	 @Function		OsGetNativePixmapType
	 @description	Called from InitAPI() to get the NativePixmapType
	*************************************************************************/
	void		*OsGetNativePixmapType();

	/*!***********************************************************************
	 @Function		OsGetNativeWindowType
	 @description	Called from InitAPI() to get the NativeWindowType
	*************************************************************************/
	void		*OsGetNativeWindowType();

	/*!***********************************************************************
	 @Function		OsGet
	 @Description	Retrieves OS-specific data
	*************************************************************************/
	bool		OsGet(const prefNamePtrEnum prefName, void **pp);

	/*!***********************************************************************
	 @Function		OsDisplayDebugString
	 @Description	Prints a debug string
	*************************************************************************/
	void OsDisplayDebugString(char const * const str);

	/*!***********************************************************************
	 @Function		OsGetTime
	 @Description	Gets the time in milliseconds since the beginning of the application
	*************************************************************************/
	unsigned long OsGetTime();

	/*
		API functionality
	*/
	/*!***********************************************************************
	 @Function		ApiInitAPI
	 @description	Initialisation for API-specific code.
	*************************************************************************/
	bool ApiInitAPI();

	/*!***********************************************************************
	 @Function		ApiReleaseAPI
	 @description	Releases all resources allocated by the API.
	*************************************************************************/
	void ApiReleaseAPI();

	/*!***********************************************************************
	 @Function		ApiScreenCaptureBuffer
	 @description	API-specific function to store the current content of the
					FrameBuffer into the memory allocated by the user.
	*************************************************************************/
	bool ApiScreenCaptureBuffer(int Width,int Height,unsigned char *pBuf);

	/*!***********************************************************************
	 @Function		ApiRenderComplete
	 @description	Perform API operations required after a frame has finished (e.g., flipping).
	*************************************************************************/
	void ApiRenderComplete();

	/*!***********************************************************************
	 @Function		ApiGet
	 @description	Get parameters which are specific of the API.
	*************************************************************************/
	bool ApiGet(const prefNamePtrEnum prefName, void **pp);

	/*!***********************************************************************
	 @Function		ApiActivatePreferences
	 @description	Run specific API code to perform the operations requested in preferences.
	*************************************************************************/
	void ApiActivatePreferences();
};

#endif /* __PVRSHELLIMPL_H_ */

/*****************************************************************************
 End of file (PVRShellImpl.h)
*****************************************************************************/
