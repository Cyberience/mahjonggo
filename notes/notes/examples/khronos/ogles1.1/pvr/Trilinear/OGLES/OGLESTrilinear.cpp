/******************************************************************************

 @File         OGLESTrilinear.cpp

 @Title        OGLES filtering modes

 @Copyright    Copyright (C) 2000 - 2006 by Imagination Technologies Limited.

 @Platform     Independant

 @Description  Shows the bilinear and trilinear filtering modes. A POD scene file
               is loaded and displayed 3 times with no filtering, bilinear
               filtering and trilinear filtering. See IntroducingPOD for
               explanations on the use of POD files.

******************************************************************************/
#include <GLES/egl.h>

#include <math.h>
#include <string.h>

#include "PVRShell.h"
#include "OGLESTools.h"

#include "o_back.h"
#include "o_tape.h"
#include "o_ball.h"
#include "o_info.h"

#ifdef OGLESLITE
#define MODEL_NAME "o_model_fixed.pod"
#else
#define MODEL_NAME "o_model_float.pod"
#endif

/******************************************************************************
 Defines
******************************************************************************/

// Camera constants. Used for making the projection matrix
#define CAM_ASPECT	(1.333333333f)
#define CAM_NEAR	(4.0f)
#define CAM_FAR		(5000.0f)

/*!****************************************************************************
 Class implementing the PVRShell functions.
******************************************************************************/
class OGLESTrilinear : public PVRShell
{
	// Print3D class used to display text
	CPVRTPrint3D	m_Print3D;

	// Projection and Model View matrices
	PVRTMATRIX		m_mProjection, m_mView;

	// Texture handles
	GLuint			m_uiTexBackground;
	GLuint			m_uiTexTape[3], m_uiTexBall[3], m_uiTexInfo[3];

	// Values used to look up the nodes by their names
	int				m_iNodeSphere, m_iNodeTape, m_iNodeBanner1, m_iNodeBanner2, m_iNodeBanner3, m_iNodeBanner4;

	// 3D Model
	CPVRTPODScene	m_Scene;

	// Enum to represent one of the three models displayed
	enum eTapePosition {Left, Middle, Right};

	// Used for animating the models
	float			m_fFrame;

public:
	virtual bool InitApplication();
	virtual bool InitView();
	virtual bool ReleaseView();
	virtual bool QuitApplication();
	virtual bool RenderScene();

	void DrawTape(eTapePosition ePosition);
	void DrawSphere(eTapePosition ePosition);
	void DrawBanner(eTapePosition ePosition);

	void DrawMesh(SPODMesh* pMesh);
	void GetModelMatrixFromPosition(PVRTMATRIX* mModel, eTapePosition ePosition);
	void GetSpherePosition(eTapePosition ePosition, VERTTYPE* fSpherePosY, VERTTYPE* fSpherePosZ);
	void ComputeTapeVertices(eTapePosition ePosition);
	bool LoadTextures();
};


/*!****************************************************************************
 @Function		InitApplication
 @Return		bool		true if no error occured
 @Description	Code in InitApplication() will be called by PVRShell once per
				run, before the rendering context is created.
				Used to initialize variables that are not dependant on it
				(e.g. external modules, loading meshes, etc.)
				If the rendering context is lost, InitApplication() will
				not be called again.
******************************************************************************/
bool OGLESTrilinear::InitApplication()
{
	/*
		Gets the Data Path.
	*/
	const char	*dataPath;
	char		*filename = new char[2048];
	dataPath = (char*)PVRShellGet(prefDataPath);
	if (!dataPath)
	{
		PVRShellSet(prefExitMessage, "ERROR: Failed to provide a buffer large enough for OGLESGetDataPath.\n");
		return false;
	}

	/*
		Loads the scene.
	*/
	sprintf(filename, "%s%s", dataPath, MODEL_NAME);
	if (!m_Scene.ReadFromFile(filename))
	{
		PVRShellSet(prefExitMessage, "ERROR: Couldn't load the .pod file\n");
		delete [] filename;
		return false;
	}
	delete [] filename;

	// Initialize the variable used for animation
	m_fFrame = 0;

	return true;
}

/*!****************************************************************************
 @Function		QuitApplication
 @Return		bool		true if no error occured
 @Description	Code in QuitApplication() will be called by PVRShell once per
				run, just before exiting the program.
				If the rendering context is lost, QuitApplication() will
				not be called.
******************************************************************************/
bool OGLESTrilinear::QuitApplication()
{
	// Destroys the scene
	m_Scene.Destroy();
    return true;
}

/*!****************************************************************************
 @Function		InitView
 @Return		bool		true if no error occured
 @Description	Code in InitView() will be called by PVRShell upon
				initialization or after a change in the rendering context.
				Used to initialize variables that are dependant on the rendering
				context (e.g. textures, vertex buffers, etc.)
******************************************************************************/
bool OGLESTrilinear::InitView()
{
	int i;
	// Initialize Print3D
	if(!m_Print3D.SetTextures(0,PVRShellGet(prefWidth),PVRShellGet(prefHeight)))
	{
		PVRShellSet(prefExitMessage, "ERROR: Cannot initialise Print3D\n");
		return false;
	}

	// Enables texturing
	glEnable(GL_TEXTURE_2D);

	/*
		Loads the camera.
		See IntroducingPOD for more explications.
	*/
	if (m_Scene.nNumCamera == 0)
	{
		PVRShellSet(prefExitMessage, "ERROR: The scene does not contain a camera\n");
		return false;
	}
	PVRTVECTOR3	vFrom, vTo, vUp;
	VERTTYPE	fFOV;
	vUp.x = f2vt(0.0f);
	vUp.y = f2vt(1.0f);
	vUp.z = f2vt(0.0f);
	fFOV = m_Scene.GetCameraPos(vFrom, vTo, 0);
	PVRTMatrixLookAtRH(m_mView, vFrom, vTo, vUp);

	// Calculates the projection matrix
	bool bRotate = PVRShellGet(prefIsRotated) && PVRShellGet(prefFullScreen);
	PVRTMatrixPerspectiveFovRH(m_mProjection, fFOV, f2vt(CAM_ASPECT), f2vt(CAM_NEAR), f2vt(CAM_FAR), bRotate);

	// Loads the projection matrix
	glMatrixMode(GL_PROJECTION);
	myglLoadMatrix(m_mProjection.f);
	glMatrixMode(GL_MODELVIEW);

	// Loads the textures
	if (!LoadTextures())
	{
		PVRShellSet(prefExitMessage, "ERROR: Cannot load the textures\n");
		return false;
	}

	/*
		Saves the indices of the nodes,
		according to their names in 3D Studio.
		Used for easily accessing the nodes we want later.
	*/
	for (i=0; i<(int)m_Scene.nNumNode; i++)
	{
		SPODNode* pNode = &m_Scene.pNode[i];
		if (!strcmp(pNode->pszName, "Sphere"))
			m_iNodeSphere = i;
		if (!strcmp(pNode->pszName, "Tape"))
			m_iNodeTape = i;
		if (!strcmp(pNode->pszName, "Banner1"))
			m_iNodeBanner2 = i;
		if (!strcmp(pNode->pszName, "Banner2"))
			m_iNodeBanner1 = i;
		if (!strcmp(pNode->pszName, "Banner3"))
			m_iNodeBanner3 = i;
		if (!strcmp(pNode->pszName, "Banner4"))
			m_iNodeBanner4 = i;
	}
	
	return true;
}

/*!****************************************************************************
 @Function		ReleaseView
 @Return		bool		true if no error occured
 @Description	Code in ReleaseView() will be called by PVRShell when the
				application quits or before a change in the rendering context.
******************************************************************************/
bool OGLESTrilinear::ReleaseView()
{
	int i;

	// Frees the texture
	for (i=2; i>=0; i--)
	{
		PVRTReleaseTexture(m_uiTexInfo[i]);
		PVRTReleaseTexture(m_uiTexBall[i]);
		PVRTReleaseTexture(m_uiTexTape[i]);
	}
	PVRTReleaseTexture(m_uiTexBackground);

	// Release Print3D Textures
	m_Print3D.ReleaseTextures();

	return true;
}

/*!****************************************************************************
 @Function		RenderScene
 @Return		bool		true if no error occured
 @Description	Main rendering loop function of the program. The shell will
				call this function every frame.
				eglSwapBuffers() will be performed by PVRShell automatically.
				PVRShell will also manage important OS events.
				Will also manage relevent OS events. The user has access to
				these events through an abstraction layer provided by PVRShell.
******************************************************************************/
bool OGLESTrilinear::RenderScene()
{
//	PVRShellOutputDebug("frame = %d\n", Interface_GetFrameNumber());
	m_fFrame++;

	// Clears the color buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Use PVRTools to draw a background image
	PVRTMiscDrawBackground(m_uiTexBackground, PVRShellGet(prefIsRotated) && PVRShellGet(prefFullScreen));

	// Enables the z-buffer
	glEnable(GL_DEPTH_TEST);

	// Sets the projection matrix
	glMatrixMode(GL_PROJECTION);
	myglLoadMatrix(m_mProjection.f);
	glMatrixMode(GL_MODELVIEW);

	// Draws the model without filtering
	DrawTape(Left);
	DrawSphere(Left);
	DrawBanner(Left);

	// Draws the model with mipmap filtering
	DrawTape(Right);
	DrawSphere(Right);
	DrawBanner(Right);

	// Draws the model with trilinear filtering
	DrawTape(Middle);
	DrawSphere(Middle);
	DrawBanner(Middle);

	// Displays the demo name using the tools. For a detailed explanation, see the training course IntroducingPVRTools
	m_Print3D.DisplayDefaultTitle("Trilinear", "Texture filter comparison.", PVR_LOGO);
	m_Print3D.Flush();

	return true;
}

/*!****************************************************************************
 @Function		DrawTape
 @Input			ePosition		Position of the tape to draw
 @Description	Draws one of the 3 waving tapes.
******************************************************************************/
void OGLESTrilinear::DrawTape(eTapePosition ePosition)
{
	// Gets the tape node and mesh
	SPODNode* pNode = &m_Scene.pNode[m_iNodeTape];
	SPODMesh* pMesh = &m_Scene.pMesh[pNode->nIdx];

	// Disables blending and back face culling
	glDisable(GL_BLEND);
	glDisable(GL_CULL_FACE);

	// Sets the tape texture with the correct filtering mode
	glBindTexture(GL_TEXTURE_2D, m_uiTexTape[ePosition]);

	// Recalculates the waving tapes vertices
	ComputeTapeVertices(ePosition);

	/*
		Gets the tape model matrix from its position.
		And then calculates the model-view matrix.
	*/
	PVRTMATRIX mModel, mModelView;
	GetModelMatrixFromPosition(&mModel, ePosition);
	PVRTMatrixMultiply(mModelView, mModel, m_mView);
	myglLoadMatrix(mModelView.f);

	// Draw the tape
	DrawMesh(pMesh);
}

/*!****************************************************************************
 @Function		DrawSphere
 @Input			ePosition		Position of the sphere to draw
 @Description	Draws one of the 3 spheres.
******************************************************************************/
void OGLESTrilinear::DrawSphere(eTapePosition ePosition)
{
	// Gets the sphere node and mesh
	SPODNode* pNode = &m_Scene.pNode[m_iNodeSphere];
	SPODMesh* pMesh = &m_Scene.pMesh[pNode->nIdx];

	// Enables transparency using blending
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// Sets the sphere texture with the correct filtering mode
	glBindTexture(GL_TEXTURE_2D, m_uiTexBall[ePosition]);

	/*
		Gets the sphere model matrix from its position.
		Then finds the sphere specific model matrix.
		And finally calculates the model-view matrix.
	*/
	PVRTMATRIX mModel, mSpecificModel, mModelView, mScale, mTranslate, mRotateX, mRotateY, mTemp;
	VERTTYPE fSpherePosY, fSpherePosZ;
	GetSpherePosition(ePosition, &fSpherePosY, &fSpherePosZ);
	GetModelMatrixFromPosition(&mModel, ePosition);

	// Calculates the sphere specific transformation matrices
	PVRTMatrixScaling(mScale, f2vt(0.9f), f2vt(0.9f), f2vt(0.9f));
	PVRTMatrixTranslation(mTranslate, f2vt(0.0f), fSpherePosY, fSpherePosZ);
	PVRTMatrixRotationX(mRotateX, f2vt(m_fFrame/50.0f));
	PVRTMatrixRotationY(mRotateY, f2vt(m_fFrame/50.0f));

	// Compose all those matrices to get the model-view matrix
	PVRTMatrixMultiply(mSpecificModel, mRotateY, mRotateX);
	PVRTMatrixMultiply(mTemp, mSpecificModel, mTranslate);
	PVRTMatrixMultiply(mSpecificModel, mTemp, mScale);
	PVRTMatrixMultiply(mTemp, mSpecificModel, mModel);
	PVRTMatrixMultiply(mModelView, mTemp, m_mView);

	// Loads it in OpenGL ES
	myglLoadMatrix(mModelView.f);

	/*
		To display properly transparency using blending, the geometry must be
		drawn from back to front. If it is not, the z-test will prevent the
		faces behind to be drawn at all.
		So we use back face culling to first draw the front faces of the
		sphere then the faces behind.
	*/
	glFrontFace(GL_CW); glCullFace(GL_BACK); glEnable(GL_CULL_FACE);
	DrawMesh(pMesh);
	glFrontFace(GL_CCW); glCullFace(GL_BACK); glEnable(GL_CULL_FACE);
	DrawMesh(pMesh);
}

/*!****************************************************************************
 @Function		DrawBanner
 @Input			ePosition		Position of the banner to draw
 @Description	Draws one of the 3 banners (descriptive text on top of
				each model).
******************************************************************************/
void OGLESTrilinear::DrawBanner(eTapePosition ePosition)
{
	/*
		Gets the banner node and mesh.
		Unlike the tape or sphere, the model contains the three banners
		seperately so each can have its own set of texture coordinates.
	*/
	SPODNode* pNode = 0;
	switch (ePosition)
	{
		case Left:
			pNode = &m_Scene.pNode[m_iNodeBanner1];
			break;
		case Middle:
			pNode = &m_Scene.pNode[m_iNodeBanner2];
			break;
		case Right:
			pNode = &m_Scene.pNode[m_iNodeBanner3];
			break;
	}
	SPODMesh* pMesh = &m_Scene.pMesh[pNode->nIdx];

	// Enables transparency using blending
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// Sets the banner texture with the correct filtering mode
	glBindTexture(GL_TEXTURE_2D, m_uiTexInfo[ePosition]);

	/*
		Gets the banner model matrix from its position.
		Then finds the banner specific model matrix.
		And finally calculates the model-view matrix.
	*/
	PVRTMATRIX mModel, mSpecificModel, mModelView, mTemp;
	VERTTYPE fSpherePosY, fSpherePosZ;
	GetSpherePosition(ePosition, &fSpherePosY, &fSpherePosZ);
	GetModelMatrixFromPosition(&mModel, ePosition);

	// Calculates the sphere specific transformation matrices
	PVRTMatrixTranslation(mSpecificModel, f2vt(0.0f), fSpherePosY, fSpherePosZ);

	// Compose all those matrices to get the model-view matrix
	PVRTMatrixMultiply(mTemp, mSpecificModel, mModel);
	PVRTMatrixMultiply(mModelView, mTemp, m_mView);

	// Loads it in OpenGL ES
	myglLoadMatrix(mModelView.f);

	// Draw the banner
	DrawMesh(pMesh);
}

/*!****************************************************************************
 @Function		DrawMesh
 @Input			mesh		The mesh to draw
 @Description	Draws a SPODMesh after the model view matrix has been set and
				the meterial prepared.
******************************************************************************/
void OGLESTrilinear::DrawMesh(SPODMesh* pMesh)
{
	// Enables the vertices and texture coordinates arrays
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	// Give the vertex and texture coordinates data to OpenGL ES
	glVertexPointer(3, VERTTYPEENUM, pMesh->sVertex.nStride, pMesh->pInterleaved + (long)pMesh->sVertex.pData);
	glTexCoordPointer(2, VERTTYPEENUM, pMesh->psUVW[0].nStride, pMesh->pInterleaved + (long)pMesh->psUVW[0].pData);

	// Draw Indexed Triangle list
	glDrawElements(GL_TRIANGLES, pMesh->nNumFaces*3, GL_UNSIGNED_SHORT, pMesh->sFaces.pData);
}

/*!****************************************************************************
 @Function		GetModelMatrixFromPosition
 @Input			ePosition		Position of the model (left, middle or right)
 @Output		mModel			Returned mode matrix to use
 @Description	Returns the model matrix to use (to place to model to the
				left, middle or right).
******************************************************************************/
void OGLESTrilinear::GetModelMatrixFromPosition(PVRTMATRIX* mModel, eTapePosition ePosition)
{
	switch (ePosition)
	{
		case Left:
			PVRTMatrixTranslation(*mModel, f2vt(-110),f2vt(0),f2vt(0));
			break;
		case Middle:
			PVRTMatrixIdentity(*mModel);
			break;
		case Right:
			PVRTMatrixTranslation(*mModel, f2vt(+110),f2vt(0),f2vt(0));
			break;
	}
}

/*!****************************************************************************
 @Function		GetSpherePosition
 @Input			ePosition		Position of the model (left, middle or right)
 @Output		fSpherePosY		Returned position along Y
 @Output		fSpherePosZ		Returned position along Z
 @Description	Returns the sphere position at a given time. Used to make the
				spheres go back and forth on the tape.
******************************************************************************/
void OGLESTrilinear::GetSpherePosition(eTapePosition ePosition, VERTTYPE* fSpherePosY, VERTTYPE* fSpherePosZ)
{
	VERTTYPE Offset = f2vt(m_fFrame/20.0f);
	if (ePosition == Middle)
	{
		*fSpherePosZ = f2vt(700.0f * (float)PVRTFSIN(m_fFrame/100.0f) - 700.0f);
		float fAngle = vt2f(VERTTYPEMUL(*fSpherePosZ, f2vt(1.0f/100.0f)) + Offset);
		*fSpherePosY = VERTTYPEMUL(f2vt((float)PVRTFSIN(fAngle)), f2vt(15.0f));
	}
	else
	{
		*fSpherePosZ = f2vt(600.0f * (float)PVRTFSIN(m_fFrame/100.0f) - 700.0f);
		float fAngle = vt2f(VERTTYPEMUL(*fSpherePosZ, f2vt(1.0f/100.0f)) + Offset);
		*fSpherePosY = VERTTYPEMUL(f2vt((float)PVRTFCOS(fAngle)), f2vt(15.0f));
	}
}

/*!****************************************************************************
 @Function		ComputeTapeVertices
 @Input			ePosition		Position of the tape (left, middle or right)
 @Description	Recalculate the vertices of a given tape. Used to make the
				tapes move along waves.
******************************************************************************/
void OGLESTrilinear::ComputeTapeVertices(eTapePosition ePosition)
{
	int i;
	VERTTYPE Offset = f2vt(m_fFrame/20.0f);
	SPODMesh* pMesh = &m_Scene.pMesh[m_Scene.pNode[m_iNodeTape].nIdx];
	unsigned char* pfY = pMesh->pInterleaved + (long)pMesh->sVertex.pData + sizeof(VERTTYPE) * 1;
	unsigned char* pfZ = pMesh->pInterleaved + (long)pMesh->sVertex.pData + sizeof(VERTTYPE) * 2;

	for (i=0; i<(int)pMesh->nNumVertex; i++)
	{
		float fAngle = vt2f(VERTTYPEMUL(*(VERTTYPE*)pfZ, f2vt(1.0f/100.0f)) + Offset);
		if (ePosition==Middle)
			*(VERTTYPE*)pfY = VERTTYPEMUL(f2vt((float)PVRTFSIN(fAngle)), f2vt(15.0f)) - f2vt(30.0f);
		else
			*(VERTTYPE*)pfY = VERTTYPEMUL(f2vt((float)PVRTFCOS(fAngle)), f2vt(15.0f)) - f2vt(30.0f);
		pfY += pMesh->sVertex.nStride;
		pfZ += pMesh->sVertex.nStride;
	}
}

/*!****************************************************************************
 @Function		LoadTextures
 @Return		bool			true for success, false for failure
 @Description	Loads the textures.
******************************************************************************/
bool OGLESTrilinear::LoadTextures()
{
	/*
		Loads the textures.
		For a detailed explanation see the Texturing training course.
	*/
	if(!PVRTLoadTextureFromPointer((void*)o_back, &m_uiTexBackground)) return false;
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	/*
		We need to load the tape, ball and info textures 3 times with
		different filtering modes.
		That is because changing the filtering mode every frame for
		these textures would make the drivers upload the textures every frame,
		so it would be very slow.
	*/
	/*
		Loads the textures used for nearest filtering mode.
	*/
	if(!PVRTLoadTextureFromPointer((void*)o_tape, &m_uiTexTape[Left])) return false;
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if(!PVRTLoadTextureFromPointer((void*)o_ball, &m_uiTexBall[Left])) return false;
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if(!PVRTLoadTextureFromPointer((void*)o_info, &m_uiTexInfo[Left])) return false;
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	/*
		Loads the textures used for bilinear filtering mode.
	*/
	if(!PVRTLoadTextureFromPointer((void*)o_tape, &m_uiTexTape[Right])) return false;
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if(!PVRTLoadTextureFromPointer((void*)o_ball, &m_uiTexBall[Right])) return false;
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if(!PVRTLoadTextureFromPointer((void*)o_info, &m_uiTexInfo[Right])) return false;
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	/*
		Loads the textures used for trilinear filtering mode.
	*/
	if(!PVRTLoadTextureFromPointer((void*)o_tape, &m_uiTexTape[Middle])) return false;
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if(!PVRTLoadTextureFromPointer((void*)o_ball, &m_uiTexBall[Middle])) return false;
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if(!PVRTLoadTextureFromPointer((void*)o_info, &m_uiTexInfo[Middle])) return false;
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	myglTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	return true;
}

/*!****************************************************************************
 @Function		NewDemo
 @Return		PVRShell*		The demo supplied by the user
 @Description	This function must be implemented by the user of the shell.
				The user should return its PVRShell object defining the
				behaviour of the application.
******************************************************************************/
PVRShell* NewDemo()
{
	return new OGLESTrilinear();
}

/******************************************************************************
 End of file (OGLESTrilinear.cpp)
******************************************************************************/
