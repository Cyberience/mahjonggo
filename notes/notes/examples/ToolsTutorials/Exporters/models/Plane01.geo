// Source file: exportersexample.max
CIwModel
{
	name "Plane01"
	CMesh
	{
		name "Plane01"
		baseName ""
		scale 1.0
		CVerts
		{
			numVerts 4
			v {-112.26054,0,92.72031}
			v {116.09196,0,92.72031}
			v {-112.26054,0,-111.11111}
			v {116.09196,0,-111.11111}
		}
		CVertNorms
		{
			numVertNorms 1
			vn {0,1,0}
		}
		CUVs
		{
			setID 0
			numUVs 4
			uv {0.00250,0.99750}
			uv {4.99750,0.99750}
			uv {0.00250,-3.99750}
			uv {4.99750,-3.99750}
		}
		CSurface
		{
			material "floor"
			CQuads
			{
				numQuads 1
				q {3,0,3,-1,-1} {1,0,1,-1,-1} {0,0,0,-1,-1} {2,0,2,-1,-1}
			}
		}
	}
}
