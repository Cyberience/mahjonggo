/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
// Includes
#include "IwGx.h"
#include "IwUI.h"
//--------------------------------------------------------------------------
class CCounter
{
public:
    CCounter():m_Count(0),m_Label(NULL)
    {
        IW_UI_CREATE_VIEW_SLOT1(this, "CCounter", CCounter, OnClick, CIwUIElement*)
    }

    void OnClick(CIwUIElement*)
    {
        // Handle the click here
        m_Count++;

        // Create new label text
        char buf[64];
        sprintf(buf,"%d",m_Count);

        // Set label text
        m_Label->SetCaption(buf);
    }

public:
    int         m_Count;
    CIwUILabel* m_Label;
};

CCounter* g_Counter = NULL;

//--------------------------------------------------------------------------
void ExampleInit()
{
    IwGxInit();
    IwUIInit();

    //Instantiate the view and controller singletons.
    new CIwUIController;
    new CIwUIView;

    // Instantiate class to deal with events
    g_Counter = new CCounter;

    // Load the UITutorial UI
    IwGetResManager()->LoadGroup("UI.group");

    // Add the built page to the view
    CIwUIElement* pPage = CIwUIElement::CreateFromResource("page");
    IwGetUIView()->AddElement(pPage);

    //Find the counter label
    g_Counter->m_Label = (CIwUILabel*)pPage->GetChildNamed("Label");
}
//--------------------------------------------------------------------------
void ExampleShutDown()
{
    delete g_Counter;

    delete IwGetUIController();
    delete IwGetUIView();

    IwUITerminate();
    IwGxTerminate();
}
//--------------------------------------------------------------------------
bool ExampleUpdate()
{
    IwGetUIController()->Update();
    IwGetUIView()->Update(25);

    return true;
}
//--------------------------------------------------------------------------
void ExampleRender()
{
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    IwGetUIView()->Render();

    IwGxFlush();
    IwGxSwapBuffers();
}
