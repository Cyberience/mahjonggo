/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
// UITutorial main file
//--------------------------------------------------------------------------

#include "s3e.h"

// Externs for functions which examples must implement
void ExampleInit();
void ExampleShutDown();
void ExampleRender();
bool ExampleUpdate();

//--------------------------------------------------------------------------
// Main global function
//--------------------------------------------------------------------------
int main()
{
#ifdef EXAMPLE_DEBUG_ONLY
    // Test for Debug only examples
#endif

    // Example main loop
    ExampleInit();
    while (1)
    {
        s3eDeviceYield(0);
        s3eKeyboardUpdate();
        bool result = ExampleUpdate();
        if (
            (result == false) ||
            (s3eKeyboardGetState(s3eKeyEsc) & S3E_KEY_STATE_DOWN)
            ||
            (s3eKeyboardGetState(s3eKeyLSK) & S3E_KEY_STATE_DOWN)
            ||
            (s3eDeviceCheckQuitRequest())
            )
            break;
        ExampleRender();
        s3eSurfaceShow();
    }
    ExampleShutDown();
    return 0;
}
