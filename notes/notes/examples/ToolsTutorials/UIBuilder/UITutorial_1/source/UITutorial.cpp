/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
#include "s3e.h"
#include <memory.h>
//--------------------------------------------------------------------------
void ExampleInit()
{}
//--------------------------------------------------------------------------
void ExampleShutDown()
{}
//--------------------------------------------------------------------------
bool ExampleUpdate()
{
    return true;
}
//--------------------------------------------------------------------------
// The following function clears the screen and outputs the
// phrase "Hello World". It uses The s3eDebugPrint() function
// to print the phrase.
//--------------------------------------------------------------------------
void ExampleRender()
{
    // Get pointer to the screen surface
    // (pixel depth is 2 bytes by default)
    uint16* screen = (uint16*)s3eSurfacePtr();
    int height = s3eSurfaceGetInt(S3E_SURFACE_HEIGHT);
    int width = s3eSurfaceGetInt(S3E_SURFACE_WIDTH);
    int pitch = s3eSurfaceGetInt(S3E_SURFACE_PITCH);

    // Clear screen to white
    for (int i=0; i < height; i++)
    {
        memset((char*)screen + pitch * i, 255, (width * 2));
    }
    // Print Hello World
    s3eDebugPrint(10, 20, "`x000000Hello World", 0);
}
