// Source file: animatedUVs_ScrollAnim.max
CIwModel
{
	name "scrollAnimPlane"
	CMesh
	{
		name "scrollAnimPlane"
		scale 1.0
		CVerts
		{
			numVerts 6
			v {-69.27184,-21.54392,-0.00000}
			v {66.37064,-21.54392,-0.00000}
			v {-69.27184,24.32752,0.00000}
			v {66.37064,24.32752,0.00000}
			v {-98.85058,0.27087,-0.00000}
			v {98.85058,-0.00017,-0.00000}
		}
		CVertNorms
		{
			numVertNorms 1
			vn {0,1,0}
		}
		CUVs
		{
			setID 0
			numUVs 6
			uv {0,0}
			uv {0.01048,0}
			uv {0,-1}
			uv {0.01048,-1}
			uv {0,-0.50357}
			uv {0.01048,-0.50000}
		}
		CSurface
		{
			material "01_Default"
			CQuads
			{
				numQuads 1
				q {0,0,0,-1,-1} {3,0,3,-1,-1} {5,0,5,-1,-1} {1,0,1,-1,-1}
			}
			CTris
			{
				numTris 2
				t {3,0,3,-1,-1} {0,0,0,-1,-1} {4,0,4,-1,-1}
				t {3,0,3,-1,-1} {4,0,4,-1,-1} {2,0,2,-1,-1}
			}
		}
	}
}
