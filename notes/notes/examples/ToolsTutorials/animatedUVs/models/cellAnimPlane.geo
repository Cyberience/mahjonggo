// Source file: animatedUVs_CellAnim.max
CIwModel
{
	name "cellAnimPlane"
	CMesh
	{
		name "cellAnimPlane"
		scale 1.0
		CVerts
		{
			numVerts 4
			v {-247.91960,-101.76503,-0.00000}
			v {252.08040,-101.76503,-0.00000}
			v {-247.91960,98.23497,0.00000}
			v {252.08040,98.23497,0.00000}
		}
		CVertNorms
		{
			numVertNorms 1
			vn {0,1,0}
		}
		CUVs
		{
			setID 0
			numUVs 4
			uv {-0.00187,-0.86981}
			uv {0.32286,-0.86981}
			uv {-0.00187,-1}
			uv {0.32286,-1}
		}
		CSurface
		{
			material "numbersMat"
			CQuads
			{
				numQuads 1
				q {3,0,3,-1,-1} {1,0,1,-1,-1} {0,0,0,-1,-1} {2,0,2,-1,-1}
			}
		}
	}
}
