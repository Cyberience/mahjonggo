#category: Graphics
IwGraphicsScalablePipeline
==========================

IwGraphics Scalable Pipeline Example

IwGx provides extensive support for assets that render using the resources
available on the device.

This example demonstrates the rendering of a scene using a minimal number of
duplicated assets and other automatic scaling.

Of particular interest is:
 - Using IwGx to automatically display the same content with different
   effects in different renderers
 - Use of IwGxGetHwType() to dynamically scale client-side effects

The example has custom shaders that add a specular normal mapping with a
reflection of the Logo on the TV screen.
IwGxSurface is used to produce a bloom effect.
