/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
//-----------------------------------------------------------------------------
// ExampleIwGraphicsScalablePipeline
//-----------------------------------------------------------------------------

/**
 * @page ExampleIwGraphicsScalablePipeline IwGraphics Scalable Pipeline Example
 *
 * IwGx provides extensive support for assets that render using the resources
 * available on the device.
 *
 * This example demonstrates the rendering of a scene using a minimal number of
 * duplicated assets and other automatic scaling.
 *
 * Of particular interest is:
 *  - Using IwGx to automatically display the same content with different
 *    effects in different renderers
 *  - Use of IwGxGetHwType() to dynamically scale client-side effects
 *
 * Use the windows simulator GL configuration menu to change which renderer is
 * available.
 *
 * The example can be deployed to various devices and will render in one of
 * two modes:
 *
 * If an OpenGL ES 1.x renderer is available it will render using OpenGL ES
 * 1.x. This includes an example of using EFFECT_NORMAL_MAPPING_SPECULAR to do
 * specular normal mapping using the OpenGL ES 1.x fixed function pipeline.
 *
 * If OpenGL ES 2.x is available it will use that. Custom shaders add a
 * specular normal mapping with a reflection of the Logo on the TV screen.
 * IwGxSurface is used to produce a bloom effect.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwGraphicsScalablePipelineImage.png
 *
 * @include IwGraphicsScalablePipeline.cpp
 */

#include "IwGraphics.h"
#include "ExamplesMain.h"

// Angles
CIwFVec3    s_Angles;
float       s_Zoom;

CIwFMat s_BikeMat;

// Model resources
CIwResList* s_BikeModelList;
CIwResList* s_EnvModelList;

// Handles to shaders and surfaces
CIwGxShaderTechnique* s_BikeShader;
CIwGxShaderTechnique* s_PostProcessShader;
CIwGxShaderTechnique* s_BloomBufferShader;
CIwGxShaderTechnique* s_BlurBufferShader;

enum CSurfaceID
{
    OFFSCREEN_SCENE, //Main scene (full size buffer)
    BLOOM_BUFFER,    //Peak-extracted scene (small size buffer)
    BLUR_BUFFER_1,   //horizontally blurred bloom (small size buffer)
    BLUR_BUFFER_2,   //fully blurred bloom (small size buffer)

    SURFACE_COUNT,
    DISABLED,
};
CIwGxSurface* s_Surface[SURFACE_COUNT];

const char* s_SurfaceNames[SURFACE_COUNT+2] =
{
    "Scene",
    "Peak Extracted",
    "Bloom Horizontal Blur",
    "Bloom Fully Blurred",
    "Fully composed scene",
    "Post process disabled",
};

int s_DrawSurface = DISABLED; //start with surfaces disabled

// Relative size of the bloom buffer in GLES2
// Increasing this number will increase the spread of the bloom at the expense of accuracy
#define BLOOM_DIM 4

//-----------------------------------------------------------------------------
void ResizeSurfaces()
{
    int height = IwGxGetScreenHeight();
    int width = IwGxGetScreenWidth();

    for (uint32 i = 0; i < SURFACE_COUNT; i++)
    {
        if (i)
            s_Surface[i]->RecreateSurface(width/BLOOM_DIM, height/BLOOM_DIM);
        else
            s_Surface[i]->RecreateSurface(width, height);
    }
}
//-----------------------------------------------------------------------------
void RenderPass(CIwGxSurface* src, CIwGxSurface* dst, CIwGxShaderTechnique* shader)
{
    //Select the bloom buffer target as the current surface
    dst->MakeCurrent();

    //Render the frame through the shader
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    CIwMaterial* pMat = IW_GX_ALLOC_MATERIAL();
    pMat->SetTexture(src->GetTexture());
    pMat->SetShaderTechnique(shader);
    IwGxSetMaterial(pMat);
    CIwSVec2 wh((int16)IwGxGetDisplayWidth(), (int16)IwGxGetDisplayHeight());
    CIwSVec2 xy(0,0);
    CIwFVec2 uvxy(0, 0);
    CIwFVec2 uvwh(s_Surface[0]->GetClientUVExtentFloat());
    IwGxDrawRectScreenSpace(&xy, &wh, &uvxy, &uvwh);


    //no need to flush, will be flushed implicitly when next surface is made current
}
//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    IwGxInit();
    IwGraphicsInit();

    // Set screen clear colour
    IwGxSetColClear(0x40, 0x40, 0x40, 0x00);

    // Set near and far planes
    IwGxSetFarZNearZ(0x7fff, 0x10*10);

    // Initialise angles
    s_Angles = CIwFVec3(PI * 1.96f, PI * 14.5f, 0);
    s_Zoom = 0.5f;

    // Parse the GROUP file, which will load the model, its materials and textures
    IwGetResManager()->LoadGroup("IwGraphicsScalablePipeline.group");

    // A group file is a resource bundle containing a number of resource types.
    // This example uses one group for the bike and turntable and one group for the environment.
    s_BikeModelList = IwGetResManager()->GetGroupNamed("bike")->GetListNamed("CIwModel");
    s_EnvModelList = IwGetResManager()->GetGroupNamed("env")->GetListNamed("CIwModel");

    // Retrieve a handle to the shader for dynamic parameter editing
    s_BikeShader = (CIwGxShaderTechnique*)IwGetResManager()->GetResNamed("normal_specular_tv", "CIwGxShaderTechnique");
    s_PostProcessShader = (CIwGxShaderTechnique*)IwGetResManager()->GetResNamed("postprocess", "CIwGxShaderTechnique");
    s_BloomBufferShader = (CIwGxShaderTechnique*)IwGetResManager()->GetResNamed("bloombuffer", "CIwGxShaderTechnique");
    s_BlurBufferShader = (CIwGxShaderTechnique*)IwGetResManager()->GetResNamed("blurbuffer", "CIwGxShaderTechnique");

    // Initialise the transform for the bike and turntable
    s_BikeMat = CIwFMat::g_Identity;

    // Set up the view matrix
    CIwFMat view;
    view.SetRotZ(PI);
    view.t.y =  0x80*10;
    view.t.z = -0x180*10;
    IwGxSetViewMatrix(&view);

    AddButton("Zoom in: 1", 0, 70, 70, 30, s3eKey1);
    AddButton("Zoom out: 2", 0, 110, 70, 30, s3eKey2);

    //Create render targets for GL2
    if (IwGxGetHWType() == IW_GX_HWTYPE_GL2 )
    {
        AddButton("Surface stage: 3", 0, 150, 100, 30, s3eKey3);
        s_DrawSurface = SURFACE_COUNT; //start from full composited output

        int height = IwGxGetScreenHeight();
        int width = IwGxGetScreenWidth();

        for (uint32 i = 0; i < SURFACE_COUNT; i++)
        {
            s_Surface[i] = new CIwGxSurface();
            if (i)
                s_Surface[i]->CreateSurface(NULL, width/BLOOM_DIM, height/BLOOM_DIM, CIwGxSurface::EXACT_MATCH_F);
            else
                s_Surface[i]->CreateSurface(NULL, width, height, CIwGxSurface::EXACT_MATCH_F);
        }

        IwGxRegister(IW_GX_SCREENSIZE, ResizeSurfaces);
    }

}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    for (uint32 i = 0; i < SURFACE_COUNT; i++)
    {
        delete s_Surface[i];
    }

    // Terminate
    IwGraphicsTerminate();
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // Update angles from the arrow keys
    if (CheckCursorState() == EXCURSOR_LEFT )
        s_Angles.y -= PI / 32.0f;
    if (CheckCursorState() == EXCURSOR_RIGHT )
        s_Angles.y += PI / 32.0f;
    if (CheckCursorState() == EXCURSOR_UP )
        s_Angles.x -= PI / 32.0f;
    if (CheckCursorState() == EXCURSOR_DOWN )
        s_Angles.x += PI / 32.0f;

    if (CheckButton("Zoom in: 1") & S3E_KEY_STATE_DOWN )
        s_Zoom -= 0.05f;
    if (CheckButton("Zoom out: 2") & S3E_KEY_STATE_DOWN )
        s_Zoom += 0.05f;
    if (CheckButton("Surface stage: 3") & S3E_KEY_STATE_PRESSED )
        s_DrawSurface = (s_DrawSurface + 1) % (SURFACE_COUNT + 2);

    // Restrict the zoom level to know safe values.
    s_Zoom = MAX(0.125f, MIN(s_Zoom, 0.25f));

    // Rotate the bike at a constant rate.
    CIwFMat rot;
    rot.SetRotY(PI / 1024.0f);
    s_BikeMat.PostMult(rot);
    s_BikeMat.Normalise();

    //Set clear colour to black
    IwGxSetColClear(0,0,0,0);

    // Update IwGx state time stamp
    IwGxTickUpdate();
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Build view matrix rotation from angles
    CIwFMat view;
    view.SetRotZ(PI);
    view.t = CIwFVec3(0, 0x80*10, -0x480*10) * s_Zoom;

    CIwFMat rotX, rotY;
    rotX.SetRotX(s_Angles.x);
    rotY.SetRotY(s_Angles.y);
    view *= rotX * rotY;

    view.t += CIwVec3(-0x200, 0x20*10, 0);

    //Restrict y to be inside the room
    view.t.y = MAX(0, MIN(view.t.y, 151*10));

    //Pass the view matrix to gx
    IwGxSetViewMatrix(&view);

    if (s_DrawSurface != DISABLED)
    {
        s_Surface[OFFSCREEN_SCENE]->MakeCurrent();
    }

    // Set field of view - we maintain a fixed horizontal field of view
    IwGxSetPerspMul((float)IwGxGetDisplayWidth()*0xa0/320);

    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);


    IwGxFogOn();
    IwGxSetFogFarZNearZ(800*10, 450*10);

    //Configure the bike's shader

    //GetParam returns the uniform parameter object, which can then be set to control the
    //vertex/fragment shader

    //Set the size of the TV Screen
    s_BikeShader->GetParam("inTVScale")->Set(0.0005f);

    //Build a basis for the animated TV screen
    CIwFMat tvMat;
    tvMat.SetRotY(-PI/4.0f);
    tvMat.t = CIwVec3(-2000, 500, 1100);

    //Parameters which are set in worldspace will be transformed by the framework to the
    //appropriate local coordinate space. In this case, they will be passed to the shader
    //in the bike's model space, so they can be compared directly to vertices and normals
    //in the vertex shader.
    s_BikeShader->GetParam("inTVPos")->SetWorldSpace(tvMat.t);
    CIwGxShaderUniform* orient = s_BikeShader->GetParam("inTVOrient");
    orient->SetWorldSpaceDirection(tvMat.RowX(), 0);
    orient->SetWorldSpaceDirection(tvMat.RowY(), 1);
    orient->SetWorldSpaceDirection(tvMat.RowZ(), 2);

    //Configure lighting environment
    //Note that in OpenGL ES 2.0, the shader consumes these values passed from the framework.
    IwGxLightingAmbient(true);
    IwGxLightingEmissive(false);
    IwGxLightingDiffuse(true);
    IwGxLightingSpecular(true);

    //Set an ambient term
    IwGxSetLightType(1, IW_GX_LIGHT_AMBIENT);
    CIwColour c;
    c.Set(0x80,0x80,0x80,0xff);
    IwGxSetLightCol(1, &c);

    //Set a single directional light
    IwGxSetLightType(0, IW_GX_LIGHT_DIFFUSE);
    CIwFVec3 dir = CIwFVec3(1,-1, 1);
    dir.Normalise();
    IwGxSetLightDirn(0, &dir);

    //-------------------------------------------------------------------------
    // Render the models
    //-------------------------------------------------------------------------

    //Render the environment using the identity matrix
    IwGxSetModelMatrix(&CIwFMat::g_Identity);

    uint32 i;
    for (i = 0; i < s_EnvModelList->m_Resources.GetSize(); i++)
    {
        CIwModel* pModel = (CIwModel*)s_EnvModelList->m_Resources[i];
        pModel->Render();
    }


    //Render the bike and turntable using the rotating matrix
    IwGxSetModelMatrix(&s_BikeMat);

    for (i = 0; i < s_BikeModelList->m_Resources.GetSize(); i++)
    {
        CIwModel* pModel = (CIwModel*)s_BikeModelList->m_Resources[i];
        pModel->Render();
    }

    if (s_DrawSurface != DISABLED)
    {
        IwGxFlush();

        //Render a smaller version of the frame buffer,
        //transforming into a fake HDR version using the bloombuffer shader
        RenderPass(s_Surface[OFFSCREEN_SCENE], s_Surface[BLOOM_BUFFER], s_BloomBufferShader);

        //Blur the bloom buffer to give spread
        const float BLUR_SPREAD_X = 0.007f;
        const float BLUR_SPREAD_Y = BLUR_SPREAD_X * s_Surface[0]->GetClientWidth() / s_Surface[0]->GetClientHeight();

        CIwGxShaderUniform* blurDir = s_BlurBufferShader->GetParam("inBlurStep");
        float dirs[3] = { BLUR_SPREAD_X, 0.0f, BLUR_SPREAD_Y };
        blurDir->Set(CIwGxShaderUniform::VEC2, 0, dirs);

        RenderPass(s_Surface[BLOOM_BUFFER], s_Surface[BLUR_BUFFER_1], s_BlurBufferShader);
        IwGxFlush(); //need to submit before changing the shader parameter (it is not an instanceParam)

        blurDir->Set(CIwGxShaderUniform::VEC2, 0, dirs+1);
        RenderPass(s_Surface[BLUR_BUFFER_1], s_Surface[BLUR_BUFFER_2], s_BlurBufferShader);

        //Now composite the frame buffer and the bloom buffer using the postprocess shader

        //Select the back buffer as the current surface
        CIwGxSurface::MakeDisplayCurrent();

        //Render the buffers through the shader
        IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

        CIwMaterial* pMat = IW_GX_ALLOC_MATERIAL();

        if (s_DrawSurface == SURFACE_COUNT)
        {
            //Select the original surface and the bloom surface into the material
            pMat->SetTexture(s_Surface[OFFSCREEN_SCENE]->GetTexture());
            pMat->SetTexture(s_Surface[BLUR_BUFFER_2]->GetTexture(), 1);

            pMat->SetShaderTechnique(s_PostProcessShader);
        }
        else
        {
            IwGxLightingOff();
            pMat->SetTexture(s_Surface[s_DrawSurface]->GetTexture());
            pMat->SetColAmbient(0xffffffff);
        }
        IwGxSetMaterial(pMat);
        CIwSVec2 xy(0,0);
        CIwSVec2 wh((int16)IwGxGetScreenWidth(), (int16)IwGxGetScreenHeight());
        CIwFVec2 uvxy(0, 0);
        IwGxDrawRectScreenSpace(&xy, &wh, &uvxy, &s_Surface[OFFSCREEN_SCENE]->GetClientUVExtentFloat());

        IwGxPrintString( 2, 40, "Viewing Surface:" );
        IwGxPrintString( 2, 50, s_SurfaceNames[s_DrawSurface] );
    }

    // Rendering the frame buffer will have removed the example framework buttons,
    // So render them again
    RenderButtons();
    RenderSoftkeys();
    RenderCursorskeys();

    // End drawing
    IwGxFlush();

    // Swap buffers
    IwGxSwapBuffers();
}
