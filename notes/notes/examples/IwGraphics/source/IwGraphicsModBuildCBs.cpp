/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
//-----------------------------------------------------------------------------
// ExampleIwGraphicsModelBuilder
//-----------------------------------------------------------------------------

/**
 * @page ExampleIwGraphicsModelBuilderCallbacks IwGraphics Model Builder Callbacks Example
 * The following example demonstrates conditioning and extending models with
 * model builder callbacks.
 *
 * The main functions used to achieve this are:
 *  - SetPreBuildFn()
 *  - SetPostBuildFn()
 *
 * When a model is compiled to a binary resource ModelBuilder provides two
 * callback hooks for client programs to alter the model and add their own
 * annotations while the model is in a readable format.
 *
 * PreBuildFn - this function is called after the GEO has been loaded into the
 * model builder, but before the binary representation is build. Typically this
 * can be used to transform the model data. In this example, PreBuildFn is
 * attached to a function that warps the loaded geometry.
 *
 * PostBuildFn - this function is called after the GEO has been converted to
 * binary representation, but before it is serialised. This callback is
 * typically used to store additional information about the model which would
 * be difficult, expensive or impossible to extract from the binary version. In
 * this example it stores the vertex indices of faces in a set called
 * "walkable" in a new separate resource CHalo. It then renders them,
 * transformed, in the scene.
 *
 * A "userString" is supplied to both callbacks which can be specified in the
 * group file to control the behaviour of the callback functions.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwGraphicsModBuildCBsImage.png
 *
 * @include IwGraphicsModBuildCBs.cpp
 */

#include "IwGraphics.h"
#include "IwGx.h"
#include "IwGxPrint.h"
#include "IwModel.h"
#include "IwModelBuilder.h"
#include "IwResManager.h"
#include "IwTextParserITX.h"
#include "s3eKeyboard.h"
#include "IwModelExt.h"
#include "ExamplesMain.h"

// Angles
CIwFVec3    s_Angles;

// Local matrix
CIwFMat     s_ModelMatrix;

// Model resource
CIwModel*   s_Model;

// Custom resource
class CHalo* s_Halo;

// Timer
uint32      s_Timer;

//--------------------------------------------------------------------------------
CIwMenu* DebugCreateMainMenu()
{
    IW_CALLSTACK("DebugCreateMainMenu")

    CIwMenu* pMenu = new CIwMenu;

#ifdef IW_DEBUG
    pMenu->SetXY(0, 50);
    pMenu->AddItem(new CIwMenuItemResManager);
    pMenu->AddItem(new CIwMenuItemGraphics);
    pMenu->AddItem(new CIwMenuItemGx);
#endif
    return pMenu;
}

//-----------------------------------------------------------------------------
// CHalo is a custom resource that is constructed from a geo at ModelParseClose time.
// It is then used to generate some procedural geometry based on a sub set of the model's faces
//-----------------------------------------------------------------------------
class CHalo : public CIwResource
{
public:
    IW_MANAGED_DECLARE(CHalo);

    CHalo() : m_pModel(NULL) {};

#ifdef IW_BUILD_RESOURCES
    void AddFace(CIwFace* pFace, CIwModel* pModel);
    void Finish();
#endif

    // Standard CIwResource interface
    void Serialise();
    void Resolve();
    void Render();

    // helper function for looking up verts from CIwModel's vertex block
    CIwFVec3& GetVert(int32 i)
    {
        return ((CIwFVec3*)m_pModel->GetVerts())[m_Points[i]];
    }

private:
    // The model this halo is based on
    CIwModel* m_pModel;
    // A list of points. Each triplet is one triangle.
    CIwArray<uint16> m_Points;
    // A list of weights based on each point's position
    CIwArray<float> m_Weights;
};
//-----------------------------------------------------------------------------
IW_MANAGED_IMPLEMENT(CHalo);
IW_CLASS_FACTORY(CHalo);
//-----------------------------------------------------------------------------
#ifdef IW_BUILD_RESOURCES
void CHalo::AddFace(CIwFace* pFace, CIwModel* pModel)
{
    // Remember the model
    IwAssert(GRAPHICS, !m_pModel || m_pModel == pModel);
    m_pModel = pModel;

    // Model build info contains information about how the data in the model was re-arranged.
    // Verts may be reordered for cache performance, etc.
    CIwModelBuildInfoMap& map = m_pModel->GetModelBuildInfo().GetMap(IW_MB_STREAM_VERTS);

    // If this face is a triangle...
    if (pFace->GetNumPoints() == 3 )
    {
        for (uint32 i = 0; i < 3; i++)
        {
            CIwArray<uint16> deps;
            map.GetDependents(pFace->GetVertID(i), deps);
            m_Points.push_back(deps[0]);
        }
    }
    else
    {
        // Only support quads
        IwAssert(GRAPHICS, pFace->GetNumPoints() == 4);

        // Add the quad as two triangles
        uint32 i;
        for (i = 0; i < 3; i++)
        {
            CIwArray<uint16> deps;
            map.GetDependents(pFace->GetVertID(i), deps);
            m_Points.push_back(deps[0]);
        }
        for (i = 0; i < 4; i == 0 ? i += 2 : i++)
        {
            CIwArray<uint16> deps;
            map.GetDependents(pFace->GetVertID(i), deps);
            m_Points.push_back(deps[0]);
        }
    }
}
//-----------------------------------------------------------------------------
void CHalo::Finish()
{
    // Calculate centre
    CIwFVec3 centre = CIwFVec3(0, 0, 0);
    uint32 i;
    for (i = 0; i < m_Points.size(); i++)
    {
        centre += GetVert(i);
    }
    centre = centre / float(m_Points.size());

    // Calculate weight based on distance from centre
    float farthestPoint = 0;
    for (i = 0; i < m_Points.size(); i++)
    {
        farthestPoint = MAX(farthestPoint, (GetVert(i) - centre).GetLength());
    }

    m_Weights.resize(m_Points.size());
    for (i = 0; i < m_Points.size(); i++)
    {
        m_Weights[i] = MAX(farthestPoint - (GetVert(i) - centre).GetLength() - 40, 0) / farthestPoint;
    }

}
#endif
//-----------------------------------------------------------------------------
void CHalo::Serialise()
{
    CIwManaged::Serialise();

    IwSerialiseManagedHash(&m_pModel);
    m_Points.SerialiseHeader();
    IwSerialiseUInt16(m_Points[0], m_Points.size());
    m_Weights.SerialiseHeader();
    IwSerialiseFloat(m_Weights[0], m_Points.size());
}
//-----------------------------------------------------------------------------
void CHalo::Resolve()
{
    CIwManaged::Resolve();

    IwResolveManagedHash(&m_pModel, IW_GRAPHICS_RESTYPE_MODEL);
}
//-----------------------------------------------------------------------------
void CHalo::Render()
{
    // Create material in gx data cache
    CIwMaterial* pMat = IW_GX_ALLOC_MATERIAL();

    pMat->SetAlphaMode(CIwMaterial::ADD);
    pMat->SetZDepthOfs(-1000);
    pMat->SetCullMode(CIwMaterial::CULL_NONE);

    IwGxLightingOff();

    // Create storage for verts and colours in gx data cache
    CIwFVec3 *pVerts = IW_GX_ALLOC(CIwFVec3, m_Points.size());
    CIwColour *pCols = IW_GX_ALLOC(CIwColour, m_Points.size());

    // Generate target point
    CIwFVec3 target(0, 0x2000, 0x1000);
    CIwFMat mat;
    mat.SetRotY(float(2 * PI * sin(s_Timer / 20.0f)));
    target = mat.RotateVec(target);

    // Construct this frame's verts/colours by copying and transforming the model's verts
    // We generate two streams: one of colour and one of verts.
    for (uint32 i = 0; i < m_Points.size(); i++)
    {
        float weight = m_Weights[i];
        // Blend original position and target position based on weight
        pVerts[i] = target * weight + GetVert(i) * (1 - weight);

        // Ramp opacity based on inverse weight
        float col = (1 - weight);
        uint8 col2 = (uint8)MAX(0, col * col * col * 0xff - 0x80);
        pCols[i].Set(col2, col2, col2);
    }

    // Set up for render...
    IwGxSetMaterial(pMat);
    IwGxSetVertStreamModelSpace(pVerts, m_Points.size());
    IwGxSetNormStream(NULL);
    IwGxSetColStream(pCols, m_Points.size());

    // And render
    IwGxDrawPrims(IW_GX_TRI_LIST, NULL, m_Points.size());
}
//-----------------------------------------------------------------------------
#ifdef IW_BUILD_RESOURCES
void ConditionModel(const char* pUserString)
{
    //check user string to test whether to affect this asset
    if (strcmp(pUserString, "level") != 0 )
        return;


    // Loop over model builder verts...
    uint32 i;
    for (i = 0; i < IwGetModelBuilder()->GetNumVerts(); i++)
    {
        CIwFVec3& v = IwGetModelBuilder()->GetVert(i);
        // Applying a transformation (linear stretch based on y value)
        v *= 0.5f + (v.y + 10.0f) * 0.001f;
    }
}
//-----------------------------------------------------------------------------
void BuildHalo(const char* pUserString, CIwModel* pModel)
{
    //check user string to test whether to affect this asset
    if (strcmp(pUserString, "level") != 0 )
        return;

    // Create a new halo resource and name it after its model partner
    CHalo* pHalo = new CHalo;
    pHalo->SetName(pModel->DebugGetName());

    IW_MANAGED_LIST_ITERATE(pModel->m_Exts, itE)
    {
        // Only interested in face selection sets
        if (strcmp((*itE)->GetClassName(), "CIwModelExtSelSetFace") != 0)
            continue;

        const char* name = (*itE)->DebugGetName();

        if (!name) continue;
        CIwModelExtSelSetFace* pSet = (CIwModelExtSelSetFace*)(*itE);

        if (!strcmp(name, "walkable"))
        {
            for (uint32 i = 0; i < pSet->m_FaceIDs.size(); i++)
            {
                pHalo->AddFace(IwGetModelBuilder()->GetFace(pSet->m_FaceIDs[i]), pModel);
            }

            // We'll transform the set into a halo resource, so there's no need to keep it in the model binary
            delete pSet;
            pModel->m_Exts.RemoveSlow(pSet);
            itE--;  // So we don't miss a set!
        }
    }

    // Now that all faces have been added to the halo do some final calculations
    pHalo->Finish();

    // Add the halo resource to IwResManager - it will be serialised as part of the current group.
    IwGetResManager()->AddRes("CHalo", pHalo);
}
#endif


void CreateButtonsUI(int w, int h)
{
    // Create the UI layout
    AddButton("Zoom in: 1", 0, 70, 70, 30, s3eKey1);
    AddButton("Zoom out: 2", 0, 110, 70, 30, s3eKey2);
}

void SurfaceChangedCallback()
{
    DeleteButtons();
    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    IwGxRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
    IwGxInit();
    IwGraphicsInit();

    IW_CLASS_REGISTER(CHalo);

    // Set screen clear colour
    IwGxSetColClear(0x00, 0x00, 0xff, 0x00);

    // Set field of view
    IwGxSetPerspMul(0xa0);

    // Set near and far planes
    IwGxSetFarZNearZ(0x3000, 0x10);

    // Initialise angles
    s_Angles = CIwFVec3(PI / 4, PI, 0);

    // If configured to support HW, then build model for HW
#ifdef IW_BUILD_RESOURCES
    // Load the resource templates
    IwGetTextParserITX()->ParseFile("ModBuild_templates.itx");

    // Register the model builder callbacks
    IwGetModelBuilder()->SetPreBuildFn(&ConditionModel);
    IwGetModelBuilder()->SetPostBuildFn(&BuildHalo);
#endif

    // Parse the GROUP file, which will load the model, its materials and textures
    IwGetResManager()->LoadGroup("ModBuildCallbacks.group");

    // Get a pointer to the named group
    CIwResGroup* pGroup = IwGetResManager()->GetGroupNamed("Example");

    // Get and store pointers to the model resources
    s_Model = (CIwModel*)pGroup->GetResNamed("lava", IW_GRAPHICS_RESTYPE_MODEL);
    s_Halo = (CHalo*)pGroup->GetResNamed("lava", "CHalo");

    // Set up the view matrix
    CIwFMat view;
    view.SetRotZ(PI);
    view.t.y =  128.0;
    view.t.z = -2048.0;
    IwGxSetViewMatrix(&view);

    //-------------------------------------------------------------------------
    // Set up scene lighting
    //-------------------------------------------------------------------------
    // Set single ambient light
    IwGxSetLightType(0, IW_GX_LIGHT_AMBIENT);
    CIwColour colA = {0x40, 0x40, 0x40, 0xff};
    IwGxSetLightCol(0, &colA);

    // Set single diffuse light
    IwGxSetLightType(1, IW_GX_LIGHT_DIFFUSE);
    CIwColour colD = {0xc0, 0xc0, 0xc0, 0xff};
    IwGxSetLightCol(1, &colD);
    CIwFVec3 dd(1, 1, 1);
    IwGxSetLightDirn(1, &dd);

    // Set up the menu manager
    new CIwMenuManager;
    IwGetMenuManager()->SetTextCallback(IwGxPrintMenuCallback);
    IwGetMenuManager()->SetMainMenuFn(DebugCreateMainMenu);

    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    delete IwGetMenuManager();

    // Terminate
    IwGraphicsTerminate();
    IwGxTerminate();
    IwGxUnRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // Reset metrics
#ifdef IW_DEBUG
    IwGraphicsMetricsReset();
    IwGxMetricsReset();
#endif
    // Update angles from the arrow keys
    if (CheckCursorState() == EXCURSOR_LEFT)
        s_Angles.y -= 0.05f;
    if (CheckCursorState() == EXCURSOR_RIGHT)
        s_Angles.y += 0.05f;
    if (CheckCursorState() == EXCURSOR_UP)
        s_Angles.x -= 0.05f;
    if (CheckCursorState() == EXCURSOR_DOWN)
        s_Angles.x += 0.05f;

    // Move camera in/out
    if (CheckButton("Zoom out: 2") & S3E_KEY_STATE_DOWN)
        s_ModelMatrix.t.z += 50.0;
    if (CheckButton("Zoom in: 1") & S3E_KEY_STATE_DOWN)
        s_ModelMatrix.t.z -= 50.0;

#ifdef IW_DEBUG
    // Toggle flag to display chunks in debug colours
    if (s3eKeyboardGetState(s3eKeyD) & S3E_KEY_STATE_PRESSED)
    {
        if (IwGraphicsGetDebugFlags() & IW_GRAPHICS_DEBUG_MODEL_COLOUR_CHUNKS_F)
            IwGraphicsClearDebugFlags(IW_GRAPHICS_DEBUG_MODEL_COLOUR_CHUNKS_F);
        else
            IwGraphicsSetDebugFlags(IW_GRAPHICS_DEBUG_MODEL_COLOUR_CHUNKS_F);
    }
#endif

    // Build model matrix rotation from angles
    CIwFMat rotX, rotY;
    rotX.SetRotX(s_Angles.x);
    rotY.SetRotY(s_Angles.y);
    s_ModelMatrix.CopyRot(rotY * rotX);

    // Update timer
    s_Timer++;

    // Update menu manager
    IwGetMenuManager()->Update();

    // Update IwGx state time stamp
    IwGxTickUpdate();
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Start with full lighting
    IwGxLightingOn();

    //-------------------------------------------------------------------------
    // Render a background poly
    //-------------------------------------------------------------------------
    CIwSVec2* pVec = AllocClientScreenRectangle();
    CIwMaterial* pMat = IW_GX_ALLOC_MATERIAL();
    pMat->Init();
    pMat->SetColAmbient(0, 0, 255, 255);
    IwGxSetMaterial(pMat);
    IwGxSetVertStreamScreenSpace(pVec, 4);
    IwGxSetScreenSpaceSlot(-1);
    IwGxDrawPrims(IW_GX_QUAD_STRIP, NULL, 4);
    IwGxSetScreenSpaceSlot(0);

    //-------------------------------------------------------------------------
    // Render the main model and the halo
    //-------------------------------------------------------------------------
    IwGxSetModelMatrix(&s_ModelMatrix);
    s_Model->Render();
    s_Halo->Render();

    // Paint the cursor keys buttons
    IwGxPrintString(2, IwGxGetScreenHeight() - 60, "Rotate Model: Arrow Keys");
    RenderCursorskeys();

    // End drawing
    IwGxFlush();

    // Render menu manager
    IwGetMenuManager()->Render();
    IwGxFlush();

    // Swap buffers
    IwGxSwapBuffers();
}
