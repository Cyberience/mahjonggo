/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
//-----------------------------------------------------------------------------
// ExampleIwGraphicsModelRayIntersection
//-----------------------------------------------------------------------------

/**
 * @page ExampleIwGraphicsModelRayIntersection IwGraphics Model Ray Intersection Example
 * The following example builds on IwGraphics Model Builder Callbacks example to demonstrate
 * using custom conditioning to create a collision object that allows rays to be intersected
 * with the model.
 *
 * The main functions used to achieve this are:
 * - SetPostBuildFn()
 *
 * Since the format of a model can change significantly between renderers it is not easy to
 * walk primitives to test for intersection. Studio's custom conditioning can be used to
 * store the primitive information in a easily walkable custom format.
 *
 * This example constructs an object that includes indices into the model's vertex information
 * and pre-calculates face normals for use with IwGeom's intersection functions.
 *
 * The following graphic illustrates the example output.
 *
 * @image html IwGraphicsModelRayIntersectionImage.png
 *
 * @include IwGraphicsModelRayIntersection.cpp
 */

#include "IwGraphics.h"
#include "IwGx.h"
#include "IwGxPrint.h"
#include "IwModel.h"
#include "IwModelBuilder.h"
#include "IwResManager.h"
#include "IwTextParserITX.h"
#include "s3eKeyboard.h"
#include "IwModelExt.h"
#include "ExamplesMain.h"

// Angles
CIwFVec3    s_Angles;

// Local matrix
CIwFMat     s_ModelMatrix;

// Model resource
CIwModel*   s_Model;

// Custom resource
class CCollision* s_Collision;

//--------------------------------------------------------------------------------
CIwMenu* DebugCreateMainMenu()
{
    IW_CALLSTACK("DebugCreateMainMenu")

    CIwMenu* pMenu = new CIwMenu;

#ifdef IW_DEBUG
    pMenu->SetXY(0, 50);
    pMenu->AddItem(new CIwMenuItemResManager);
    pMenu->AddItem(new CIwMenuItemGraphics);
    pMenu->AddItem(new CIwMenuItemGx);
#endif
    return pMenu;
}

//-----------------------------------------------------------------------------
// CCollision is a custom resource that is constructed from a geo at ModelParseClose time.
// It is then used to generate some procedural geometry based on a sub set of the model's faces
//-----------------------------------------------------------------------------
class CCollision : public CIwResource
{
public:
    IW_MANAGED_DECLARE(CCollision);

    CCollision() : m_pModel(NULL) {};

#ifdef IW_BUILD_RESOURCES
    void AddFace(CIwFace* pFace, CIwModel* pModel);
#endif

    // Standard CIwResource interface
    void Serialise();
    void Resolve();
    int32 GetFaceUnderCursor();

    // helper function for looking up verts from CIwModel's vertex block
    CIwFVec3& GetVert(int32 i)
    {
        return ((CIwFVec3*)m_pModel->GetVerts())[m_Points[i]];
    }

private:
    // The model this collision is based on
    CIwModel* m_pModel;
    // A list of points. Each triplet is one triangle.
    CIwArray<uint16> m_Points;
    CIwArray<CIwFVec3> m_Norms;
};
//-----------------------------------------------------------------------------
IW_MANAGED_IMPLEMENT(CCollision);
IW_CLASS_FACTORY(CCollision);
//-----------------------------------------------------------------------------
#ifdef IW_BUILD_RESOURCES

void CCollision::AddFace(CIwFace* pFace, CIwModel* pModel)
{
    // Remember the model
    IwAssert(GRAPHICS, !m_pModel || m_pModel == pModel);
    m_pModel = pModel;

    // Model build info contains information about how the data in the model was re-arranged.
    // Verts may be reordered for cache performance, etc.
    CIwModelBuildInfoMap& map = m_pModel->GetModelBuildInfo().GetMap(IW_MB_STREAM_VERTS);

    // If this face is a triangle...
    if (pFace->GetNumPoints() == 3 )
    {
        for (uint32 i = 0; i < 3; i++)
        {
            CIwArray<uint16> deps;
            map.GetDependents(pFace->GetVertID(i), deps);
            m_Points.push_back(deps[0]);
        }
    }
    else
    {
        // Only support quads
        IwAssert(GRAPHICS, pFace->GetNumPoints() == 4);

        // Add the quad as two triangles
        uint32 i;
        for (i = 0; i < 3; i++)
        {
            CIwArray<uint16> deps;
            map.GetDependents(pFace->GetVertID(i), deps);
            m_Points.push_back(deps[0]);
        }
        for (i = 0; i < 4; i == 0 ? i += 2 : i++)
        {
            CIwArray<uint16> deps;
            map.GetDependents(pFace->GetVertID(i), deps);
            m_Points.push_back(deps[0]);
        }
    }
}
#endif
//-----------------------------------------------------------------------------
void CCollision::Serialise()
{
    CIwManaged::Serialise();

    IwSerialiseManagedHash(&m_pModel);
    m_Points.SerialiseHeader();
    IwSerialiseUInt16(m_Points[0], m_Points.size());
}
//-----------------------------------------------------------------------------
void CCollision::Resolve()
{
    CIwManaged::Resolve();

    IwResolveManagedHash(&m_pModel, IW_GRAPHICS_RESTYPE_MODEL);

    //Build face normals (done on resolve to save disk space)
    for (uint32 i = 0; i < m_Points.size(); i += 3)
    {
        CIwFVec3 v1 = (CIwFVec3)GetVert(i);
        CIwFVec3 v2 = (CIwFVec3)GetVert(i+1);
        CIwFVec3 v3 = (CIwFVec3)GetVert(i+2);

        CIwFVec3 cross = (CIwFVec3)(v2 - v1).Cross(v3 - v1);
        if (cross != CIwFVec3::g_Zero)
            cross.Normalise();
        m_Norms.push_back(cross);
    }
}
//-----------------------------------------------------------------------------
int32 CCollision::GetFaceUnderCursor()
{
    //Calculate pos/dir of cursor from camera
    CIwFVec3 pos = IwGxGetViewMatrix().t;
    CIwFVec3 dir((float)s3ePointerGetX(), (float)s3ePointerGetY(), IwGxGetPerspMul());
    dir.x -= IwGxGetScreenWidth()/2;
    dir.y -= IwGxGetScreenHeight()/2;

    //Extend to the far plane
    dir *= IwGxGetFarZ()/IwGxGetPerspMul();

    //Transform pos/dir into model space
    dir = IwGxGetViewMatrix().RotateVec(dir);
    dir = s_ModelMatrix.TransposeRotateVec(dir);

    //Use more accurate normalise
    dir.Normalise();

    // Scale to touch far plane
    dir *= IwGxGetFarZ();

    pos = s_ModelMatrix.TransposeTransformVec(pos);

    //find first face intersection
    float minf = 10000000; //nearest intersection distance
    uint32 nearest = 0; //nearest intersection index

    for (uint32 i = 0; i < m_Points.size(); i += 3)
    {
        CIwFVec3 v1 = (CIwFVec3)GetVert(i);
        CIwFVec3 v2 = (CIwFVec3)GetVert(i+1);
        CIwFVec3 v3 = (CIwFVec3)GetVert(i+2);

        float f = 0;
        if (IwIntersectLineTriNorm(pos, dir, v1, v2, v3, m_Norms[i/3], f) )
        {
            if (f < minf)
            {
                minf = f;
                nearest = i;
            }
        }
    }

    if (minf != 10000000)
    {
        return nearest;
    }

    return -1;
}
//-----------------------------------------------------------------------------
#ifdef IW_BUILD_RESOURCES
void BuildCollision(const char* pUserString, CIwModel* pModel)
{
    //check user string to test whether to affect this asset
    if (strcmp(pUserString, "level") != 0 )
        return;

    // Create a new collision resource and name it after its model partner
    CCollision* pCollision = new CCollision;
    pCollision->SetName(pModel->DebugGetName());

    for (uint32 i = 0; i < IwGetModelBuilder()->GetNumFaces(); i++)
    {
        pCollision->AddFace(IwGetModelBuilder()->GetFace(i), pModel);
    }

    // Add the collision resource to IwResManager - it will be serialised as part of the current group.
    IwGetResManager()->AddRes("CCollision", pCollision);
}
#endif


void CreateButtonsUI(int w, int h)
{
    // Create the UI layout
    AddButton("Zoom in: 1", 0, 70, 70, 30, s3eKey1);
    AddButton("Zoom out: 2", 0, 110, 70, 30, s3eKey2);
}

void SurfaceChangedCallback()
{
    DeleteButtons();
    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    IwGxRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
    IwGxInit();
    IwGraphicsInit();

    IW_CLASS_REGISTER(CCollision);

    // Set screen clear colour
    IwGxSetColClear(0x00, 0x00, 0xff, 0x00);

    // Set field of view
    IwGxSetPerspMul(0xa0);

    // Set near and far planes
    IwGxSetFarZNearZ(0x2000, 0x10);

    // Initialise angles
    s_Angles = CIwFVec3(PI / 4.0f, PI, 0);

    // If configured to support HW, then build model for HW
#ifdef IW_BUILD_RESOURCES
    // Load the resource templates
    IwGetTextParserITX()->ParseFile("ModBuild_templates.itx");

    // Register the model builder callbacks
    IwGetModelBuilder()->SetPostBuildFn(&BuildCollision);
#endif

    // Parse the GROUP file, which will load the model, its materials and textures
    IwGetResManager()->LoadGroup("ModelRayIntersection.group");

    // Get a pointer to the named group
    CIwResGroup* pGroup = IwGetResManager()->GetGroupNamed("Example");

    // Get and store pointers to the model resources
    s_Model = (CIwModel*)pGroup->GetResNamed("lava", IW_GRAPHICS_RESTYPE_MODEL);
    s_Collision = (CCollision*)pGroup->GetResNamed("lava", "CCollision");

    // Set up the view matrix
    CIwFMat view;
    view.SetRotZ(PI);
    view.t.y =  0x80;
    view.t.z = -0x800;
    IwGxSetViewMatrix(&view);

    //-------------------------------------------------------------------------
    // Set up scene lighting
    //-------------------------------------------------------------------------
    // Set single ambient light
    IwGxSetLightType(0, IW_GX_LIGHT_AMBIENT);
    CIwColour colA = {0x40, 0x40, 0x40, 0xff};
    IwGxSetLightCol(0, &colA);

    // Set single diffuse light
    IwGxSetLightType(1, IW_GX_LIGHT_DIFFUSE);
    CIwColour colD = {0xc0, 0xc0, 0xc0, 0xff};
    IwGxSetLightCol(1, &colD);
    CIwFVec3 dd(0.577f, 0.577f, 0.577f);
    IwGxSetLightDirn(1, &dd);

    // Set up the menu manager
    new CIwMenuManager;
    IwGetMenuManager()->SetTextCallback(IwGxPrintMenuCallback);
    IwGetMenuManager()->SetMainMenuFn(DebugCreateMainMenu);

    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    delete IwGetMenuManager();

    // Terminate
    IwGraphicsTerminate();
    IwGxTerminate();
    IwGxUnRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // Reset metrics
#ifdef IW_DEBUG
    IwGraphicsMetricsReset();
    IwGxMetricsReset();
#endif
    // Update angles from the arrow keys
    if (CheckCursorState() == EXCURSOR_LEFT )
        s_Angles.y -= PI / 32.0f;
    if (CheckCursorState() == EXCURSOR_RIGHT )
        s_Angles.y += PI / 32.0f;
    if (CheckCursorState() == EXCURSOR_UP )
        s_Angles.x -= PI / 32.0f;
    if (CheckCursorState() == EXCURSOR_DOWN )
        s_Angles.x += PI / 32.0f;

    // Move camera in/out
    if (CheckButton("Zoom out: 2") & S3E_KEY_STATE_DOWN)
        s_ModelMatrix.t.z += 0x40;
    if (CheckButton("Zoom in: 1") & S3E_KEY_STATE_DOWN)
        s_ModelMatrix.t.z -= 0x40;

#ifdef IW_DEBUG
    // Toggle flag to display chunks in debug colours
    if (s3eKeyboardGetState(s3eKeyD) & S3E_KEY_STATE_PRESSED)
    {
        if (IwGraphicsGetDebugFlags() & IW_GRAPHICS_DEBUG_MODEL_COLOUR_CHUNKS_F)
            IwGraphicsClearDebugFlags(IW_GRAPHICS_DEBUG_MODEL_COLOUR_CHUNKS_F);
        else
            IwGraphicsSetDebugFlags(IW_GRAPHICS_DEBUG_MODEL_COLOUR_CHUNKS_F);
    }
#endif

    // Build model matrix rotation from angles
    CIwFMat rotX, rotY;
    rotX.SetRotX(s_Angles.x);
    rotY.SetRotY(s_Angles.y);
    s_ModelMatrix.CopyRot(rotY * rotX);

    // Update menu manager
    IwGetMenuManager()->Update();

    // Update IwGx state time stamp
    IwGxTickUpdate();
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Start with full lighting
    IwGxLightingOn();

    //-------------------------------------------------------------------------
    // Render a background poly
    //-------------------------------------------------------------------------
    CIwSVec2* pVec = AllocClientScreenRectangle();
    CIwMaterial* pMat = IW_GX_ALLOC_MATERIAL();
    pMat->Init();
    pMat->SetColAmbient(0, 0, 255, 255);
    IwGxSetMaterial(pMat);
    IwGxSetVertStreamScreenSpace(pVec, 4);
    IwGxSetScreenSpaceSlot(-1);
    IwGxDrawPrims(IW_GX_QUAD_STRIP, NULL, 4);
    IwGxSetScreenSpaceSlot(0);

    //-------------------------------------------------------------------------
    // Render the main model
    //-------------------------------------------------------------------------
    IwGxSetModelMatrix(&s_ModelMatrix);

    s_Model->Render();

    //outline the face under the cursor
    int32 faceID = s_Collision->GetFaceUnderCursor();
    if (faceID != -1)
    {
        CIwMaterial* pMat = IW_GX_ALLOC_MATERIAL();
        pMat->SetColAmbient(0xff0000ff);
        pMat->SetCullMode(CIwMaterial::CULL_NONE);
        IwGxSetMaterial(pMat);

        CIwFVec3* verts = IW_GX_ALLOC(CIwFVec3, 3);
        verts[0] = s_Collision->GetVert(faceID);
        verts[1] = s_Collision->GetVert(faceID+1);
        verts[2] = s_Collision->GetVert(faceID+2);
        IwGxSetVertStreamModelSpace(verts, 3);
        IwGxDrawPrims(IW_GX_TRI_LIST, NULL, 3);
    }

    // Paint the cursor keys buttons
    IwGxPrintString(2, IwGxGetScreenHeight() - 60, "Rotate Model: Arrow Keys");
    RenderCursorskeys();

    // End drawing
    IwGxFlush();

    // Render menu manager
    IwGetMenuManager()->Render();
    IwGxFlush();

    // Swap buffers
    IwGxSwapBuffers();
}
