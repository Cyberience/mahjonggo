/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

//-----------------------------------------------------------------------------
// ExampleIwUIBasicButtonSlotSignal
//-----------------------------------------------------------------------------

/**
 * @page ExampleIwUIBasicButtonSlotSignal IwUI Basic Button Slot Signal Example
 * The following example demonstrates attaching a slot to a button's signal.
 *
 * @image html IwUIBasicButtonSlotSignalImage.png
 *
 * @include IwUIBasicButtonSlotSignal.cpp
 *
 */

#include "IwUI.h"

int s_ClickCounter = 0;
class CCommandHandler* s_CommandHandler = NULL;

//-----------------------------------------------------------------------------
//This class encapsulates the creation and destruction as well as harbors the slot
class CCommandHandler
{
public:
    CCommandHandler()
    {
        //Registration of the slot
        IW_UI_CREATE_VIEW_SLOT1(this, "CCommandHandler", CCommandHandler,
            OnClickFunction, CIwUIElement*)
    }

    ~CCommandHandler()
    {
        //Destruction of the slot
        IW_UI_DESTROY_VIEW_SLOTS(this)
    }

private:
    //Called in reaction to a ButtonCLick signal
    void OnClickFunction(CIwUIElement* element)
    {
        char captionString[64];
        sprintf(captionString, "I have been\n clicked %i times",++s_ClickCounter);
        element->SetProperty("caption", CIwPropertyString(captionString));
    }
};
//-----------------------------------------------------------------------------
void ExampleInit()
{
    //Initialise the IwUI module
    IwUIInit();

    //Instantiate the view and controller singletons.
    //IwUI will not instantiate these itself, since they can be subclassed to add functionality.
    new CIwUIController;
    new CIwUIView;

    //Create a new Commandhandler instance BEFORE the button is loaded
    s_CommandHandler = new CCommandHandler();

    //Load group which contains the ui specifications
    IwGetResManager()->LoadGroup("IwUIBasicButtonSlotSignal.group");

    //Load button
    CIwUIButton* pButton = (CIwUIButton*)  CIwUIElement::CreateFromResource("EventButton");

    //Add the button to the UIView singleton
    IwGetUIView()->AddElement(pButton);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    delete s_CommandHandler;
    delete IwGetUIController();
    delete IwGetUIView();

    //Terminate the IwUI module
    IwUITerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    //Update the controller (this will generate control events etc.)
    IwGetUIController()->Update();

    //Update the view (this will do animations etc.) The example framework has
    //a fixed framerate of 20fps, so we pass that duration to the update function.
    IwGetUIView()->Update(1000/20);

    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    //Render the UI
    IwGetUIView()->Render();

    //Flush IwGx
    IwGxFlush();

    //Display the rendered frame
    IwGxSwapBuffers();
}
