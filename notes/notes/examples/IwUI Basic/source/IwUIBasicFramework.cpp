/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

//-----------------------------------------------------------------------------
// ExampleIwUIBasicFramework
//-----------------------------------------------------------------------------

/**
 * @page ExampleIwUIBasicFramework IwUI Basic Framework Example
 * The following example demonstrates the simplest IwUI application.
 *
 * @image html IwUIBasicFrameworkImage.png
 *
 * @include IwUIBasicFramework.cpp
 *
 */

#include "IwUI.h"

int main()
{
    // Initialize IwUI, create default singletons
    IwUIInit();
    new CIwUIView;
    new CIwUIController;

    // Create an element and add it at the view's top level
    CIwUIElement* pElement = new CIwUIElement;
    IwGetUIView()->AddElement(pElement);

    // Process (one logic, one render) until device quit
    const int32 frametime = 25;
    while (!s3eDeviceCheckQuitRequest())
    {
        // Clear frame buffer
        IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

        // Update ui singletons
        IwGetUIController()->Update();
        IwGetUIView()->Update(frametime);

        // Render ui
        IwGetUIView()->Render();

        // Show
        IwGxFlush();
        IwGxSwapBuffers();

        // Device interaction
        s3eDeviceYield();
        s3eKeyboardUpdate();
        s3ePointerUpdate();
    }

    // Shutdown IwUI
    delete IwGetUIView();
    delete IwGetUIController();
    IwUITerminate();

    return 0;
}
