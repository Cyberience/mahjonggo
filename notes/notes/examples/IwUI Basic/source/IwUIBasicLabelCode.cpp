/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

//-----------------------------------------------------------------------------
// ExampleIwUIBasicLabelCode
//-----------------------------------------------------------------------------

/**
 * @page ExampleIwUIBasicLabelCode IwUI Basic Label Code Example
 * The following example demonstrates creating a label from code.
 *
 * @image html IwUIBasicLabelCodeImage.png
 *
 * @include IwUIBasicLabelCode.cpp
 *
 */

#include "IwUI.h"

void ExampleInit()
{
    //Initialise the IwUI module
    IwUIInit();

    //Instantiate the view and controller singletons.
    //IwUI will not instantiate these itself, since they can be subclassed to add functionality.
    new CIwUIController;
    new CIwUIView;

    //Create a label to say "Hello World"
    CIwUILabel* pLabel = new CIwUILabel;

    //Load the group containing the example font
    IwGetResManager()->LoadGroup("IwUIBasicLabelCode.group");

    //Get the example font and set it to be the current font
    CIwGxFont* pFont = (CIwGxFont*)
        IwGetResManager()->GetResNamed("times", "CIwGxFont");
    pLabel->SetFont(pFont);

    //Set position and allow label to resize according to contents
    pLabel->SetPosAbsolute  ( CIwVec2(50,50));
    pLabel->SetSizeToContent (true);

    //Set text colour to blue
    CIwColour* LabelColour = new CIwColour();
    LabelColour->Set (0,0,255);
    pLabel->SetTextColour (*LabelColour);
    delete LabelColour;

    //Set the text to be displayed
    pLabel->SetCaption("Hello World");

    //Add the label to the UIView singleton
    IwGetUIView()->AddElement(pLabel);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    delete IwGetUIController();
    delete IwGetUIView();

    //Terminate the IwUI module
    IwUITerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    //Update the controller (this will generate control events etc.)
    IwGetUIController()->Update();

    //Update the view (this will do animations etc.) The example framework has
    //a fixed framerate of 20fps, so we pass that duration to the update function.
    IwGetUIView()->Update(1000/20);

    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    //Render the UI
    IwGetUIView()->Render();

    //Flush IwGx
    IwGxFlush();

    //Display the rendered frame
    IwGxSwapBuffers();
}
