/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

//-----------------------------------------------------------------------------
// ExampleIwUIBasicButtonEvents
//-----------------------------------------------------------------------------

/**
 * @page ExampleIwUIBasicButtonEvents IwUI Basic Button Events Example
 * The following example demonstrates attaching an event handler to a button.
 *
 * @image html IwUIBasicButtonEventsImage.png
 *
 * @include IwUIBasicButtonEvents.cpp
 *
 */

#include "IwUI.h"

CIwUIButton* s_Button = NULL;
int s_ClickCounter = 0;

//-----------------------------------------------------------------------------
class MyButtonEventHandler : public CIwUIElementEventHandler
{
public:
    //HandleEvent is passed up the element hierarchy
    virtual bool HandleEvent(CIwEvent* pEvent)
    {
        if (pEvent->GetID() == IWUI_EVENT_BUTTON)
        {
            char captionString[0x40];
            sprintf(captionString, "I have been\n clicked %i times",++s_ClickCounter);
            s_Button->SetCaption(captionString);

            //return true, as event has been handled
            return true;
        }

        return false;
    }

    //FilterEvent is passed down the element hierarchy
    virtual bool FilterEvent(CIwEvent* pEvent)
    {
        //return false, since filtering is not necessary
        return false;
    }
};

//-----------------------------------------------------------------------------
void ExampleInit()
{
    //Initialise the IwUI module
    IwUIInit();

    //Instantiate the view and controller singletons.
    //IwUI will not instantiate these itself, since they can be subclassed to add functionality.
    new CIwUIController;
    new CIwUIView;

    //Load group which contains the ui specifications
    IwGetResManager()->LoadGroup("IwUIBasicButtonEvents.group");

    //Load button
    s_Button = (CIwUIButton*) CIwUIElement::CreateFromResource("EventButton");

    //Attach eventhandler to button
    s_Button->AddEventHandler(new MyButtonEventHandler);

    //Add the label to the UIView singleton
    IwGetUIView()->AddElement(s_Button);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    delete IwGetUIController();
    delete IwGetUIView();

    //Terminate the IwUI module
    IwUITerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    //Update the controller (this will generate control events etc.)
    IwGetUIController()->Update();

    //Update the view (this will do animations etc.) The example framework has
    //a fixed framerate of 20fps, so we pass that duration to the update function.
    IwGetUIView()->Update(1000/20);

    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    //Render the UI
    IwGetUIView()->Render();

    //Flush IwGx
    IwGxFlush();

    //Display the rendered frame
    IwGxSwapBuffers();
}
