/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

//-----------------------------------------------------------------------------
// ExampleIwUIBasicTextInput
//-----------------------------------------------------------------------------

/**
 * @page ExampleIwUIBasicTextInput IwUI Basic Text Input Example
 * The example uses a soft keyboard to enter a string into a IwUITextField
 * The label below the text field is updated via signal/slots functionality
 * once editing of the text field is completed via the
 * CCommandHandler::UpdateLabelCaption() function, which is registered as a
 * slot and linked to the text field in main.ui with the line
 * OnTextFieldComplete UpdateLabelCaption
 *
 * @image html IwUIBasicTextInputImage.png
 *
 * @include IwUIBasicTextInput.cpp
 *
 */

#include "IwUI.h"

class CCommandHandler
{
public:
    CCommandHandler()
    {
        //Create slot for OnTextFieldComplete event
        IW_UI_CREATE_VIEW_SLOT2(this, "CCommandHandler", CCommandHandler,
            UpdateLabelCaption, CIwUIElement*, bool)
    }

    ~CCommandHandler()
    {
        IW_UI_DESTROY_VIEW_SLOTS(this)
    }

    void UpdateLabelCaption(CIwUIElement* pTextField, bool acceptedNotCancelled)
    {
        //Retrieve the value from the CIwUITextField
        const char* pCaption = IwSafeCast<CIwUITextField*>(pTextField)->GetCaption();

        //Copy the retrieved textstring onto the label
        CIwUILabel* pLabel = IwSafeCast<CIwUILabel*>(IwGetUIView()->GetChildNamed("Label"));
        pLabel->SetCaption(pCaption);
    }
} *s_CommandHandler;

//-----------------------------------------------------------------------------
void ExampleInit()
{
    //Initialise the IwUI module
    IwUIInit();

    //Instantiate the view and controller singletons.
    //IwUI will not instantiate these itself, since they can be subclassed to add functionality.
    new CIwUIController;
    new CIwUIView;

    //Create text input singleton
    new CIwUITextInput;

    //Create command handler for the OnTextFieldComplete event
    s_CommandHandler = new CCommandHandler;

    //Load group which contains the ui specifications
    IwGetResManager()->LoadGroup("IwUIBasicTextInput.group");

    //Create softkeyboard
    IwGetUITextInput()->CreateSoftKeyboard();

    //Choose type of soft keyboard
    IwGetUITextInput()->SetEditorMode(CIwUITextInput::eInlineKeyboard);

    //Add ui structure to UIView singleton
    CIwUIElement* pMain = CIwUIElement::CreateFromResource("Main");
    IwGetUIView()->AddElementToLayout(pMain);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    delete s_CommandHandler;
    delete IwGetUITextInput();
    delete IwGetUIController();
    delete IwGetUIView();

    //Terminate the IwUI module
    IwUITerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    //Update the controller (this will generate control events etc.)
    IwGetUIController()->Update();

    //Update the view (this will do animations etc.) The example framework has
    //a fixed framerate of 20fps, so we pass that duration to the update function.
    IwGetUIView()->Update(1000/20);

    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    //Render the UI
    IwGetUIView()->Render();

    //Flush IwGx
    IwGxFlush();

    //Render Debugging outlines of the element hierarchy
//    IwUIDebugRender(NULL, IW_UI_DEBUG_LAYOUT_OUTLINE_F | IW_UI_DEBUG_ELEMENT_OUTLINE_F);

    //Display the rendered frame
    IwGxSwapBuffers();
}
