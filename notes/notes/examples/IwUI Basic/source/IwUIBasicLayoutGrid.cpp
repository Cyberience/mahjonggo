/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

//-----------------------------------------------------------------------------
// ExampleIwUIBasicLayoutGrid
//-----------------------------------------------------------------------------

/**
 * @page ExampleIwUIBasicLayoutGrid IwUI Basic Layout Grid Example
 * The following example demonstrates the grid layout.
 *
 * @image html IwUIBasicLayoutGridImage.png
 *
 * @include IwUIBasicLayoutGrid.cpp
 *
 */

#include "IwUI.h"

//-----------------------------------------------------------------------------
void ExampleInit()
{
    //Initialise the IwUI module
    IwUIInit();

    //Instantiate the view and controller singletons.
    //IwUI will not instantiate these itself, since they can be subclassed to add functionality.
    new CIwUIController;
    new CIwUIView;

    //Load group which contains the ui specifications
    IwGetResManager()->LoadGroup("IwUIBasicLayoutGrid.group");

    //Load root element
    CIwUIElement* pRoot =  CIwUIElement::CreateFromResource("RootElement");

    //Add the root element and therefore the whole element hierarchy to the UIView singleton
    IwGetUIView()->AddElementToLayout(pRoot);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    delete IwGetUIController();
    delete IwGetUIView();

    //Terminate the IwUI module
    IwUITerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    //Update the controller (this will generate control events etc.)
    IwGetUIController()->Update();

    //Update the view (this will do animations etc.) The example framework has
    //a fixed framerate of 20fps, so we pass that duration to the update function.
    IwGetUIView()->Update(1000/20);

    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    //Render the UI
    IwGetUIView()->Render();

    //Flush IwGx
    IwGxFlush();

    //Render Debugging outlines of the element hierarchy
    IwUIDebugRender(NULL, IW_UI_DEBUG_LAYOUT_OUTLINE_F | IW_UI_DEBUG_ELEMENT_OUTLINE_F);

    //Display the rendered frame
    IwGxSwapBuffers();
}
