/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

//-----------------------------------------------------------------------------
// ExampleIwUIBasicLabelGroup
//-----------------------------------------------------------------------------

/**
 * @page ExampleIwUIBasicLabelGroup IwUI Basic Label Group Example
 * The following example demonstrates creating a label from a resource.
 *
 * @image html IwUIBasicLabelGroupImage.png
 *
 * @include IwUIBasicLabelGroup.cpp
 *
 */

#include "IwUI.h"

void ExampleInit()
{
    //Initialise the IwUI module
    IwUIInit();

    //Instantiate the view and controller singletons.
    //IwUI will not instantiate these itself, since they can be subclassed to add functionality.
    new CIwUIController;
    new CIwUIView;

    //Load ui group which contains the specification for the label
    //as well as the font
    IwGetResManager()->LoadGroup("IwUIBasicLabelGroup.group");

    //Load label
    CIwUILabel* pLabel = (CIwUILabel*)  CIwUIElement::CreateFromResource("Hello_World_label");

    //Add the label to the UIView singleton
    IwGetUIView()->AddElement(pLabel);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    delete IwGetUIController();
    delete IwGetUIView();

    //Terminate the IwUI module
    IwUITerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    //Update the controller (this will generate control events etc.)
    IwGetUIController()->Update();

    //Update the view (this will do animations etc.) The example framework has
    //a fixed framerate of 20fps, so we pass that duration to the update function.
    IwGetUIView()->Update(1000/20);

    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    //Render the UI
    IwGetUIView()->Render();

    //Flush IwGx
    IwGxFlush();

    //Display the rendered frame
    IwGxSwapBuffers();
}
