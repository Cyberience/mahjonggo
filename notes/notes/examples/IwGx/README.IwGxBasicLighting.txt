#category: Graphics
IwGxBasicLighting
=================

The following example demonstrates how to display a single
textured spinning cube, with ambient and diffuse scene
lighting. The example allows you to change the lighting
values using the debug menu.
