/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
// ExampleIwGxCIwTexture
//-----------------------------------------------------------------------------

/**
 * @page ExampleIwGxCIwTexture IwGx CIwTexture Example
 *
 * The following example loads an image file and demonstrates texture operations.
 *
 * The main classes used to achieve this are:
 *  - CIwTexture
 *  - CIwImage
 *
 * The main functions used to achieve this are:
 *  - CIwTexture::LoadFromFile()
 *  - CIwTexture::Upload()
 *  - CIwTexture::CopyFromBuffer()
 *  - CIwTexture::SetImage()
 *  - CIwTexture::ExtractSubTexture()
 *  - CIwTexture::ChangePalette()
 *  - CIwTexture::SetModifiable()
 *
 * By pressing s3eKey1 you can cycle through the list of texture operations this
 * example demonstrates. The operations demonstrated are as follows:
 *  - <b>LoadFromFile</b>: loads a new texture from file if one does not already
 *    exist using the LoadFromFile() function and then the
 *    Upload() function is called to put the texture in VRAM.
 *  - <b>CopyFromBuffer</b>: creates a colour palette and texel set which are
 *    used by the CopyFromBuffer() function to create an image of size 0x4 x 0x4.
 *    The Upload() function is called to put the created texture in VRAM.
 *  - <b>CopyFromImage</b>: opens an image file and reads it. It then creates a
 *    new texture and copies the the texture from the loaded image. The Upload()
 *    function is used to load the texture into VRAM.
 *  - <b>SetImage</b>: creates a CIwImage object, opens a texture file and
 *    reads the texture into the CIwImage object. A texture object is created and
 *    the SetImage() function is used to transfer ownership of the image data
 *    to the newly created texture object. The Upload() function is then used to
 *    load the texture into VRAM.
 *  - <b>ExtractSubTexture</b>: creates a CIwTexture object and loads a texture from
 *    file using the LoadFromFile function. It then uses the ExtractSubTexture()
 *    function to create a new subtexture from the existing texture. The texture
 *    then loaded into VRAM using the Upload() function.
 *  - <b>ChangePalette</b>: creates a static palette and then loads a texture
 *    from file using the LoadFromFile() function. The ChangePalette() function is
 *    then used to change the palette used by the texture by supplying the new
 *    palette and the data format.
 *  - <b>ChangeTexels</b>: sets a texture palette and a set of 8 texels. The 4x4
 *    texture displayed using the CopyFromBuffer() function. The textures is
 *    constructed by changing the texel settings for each render loop,
 *    which results in a different section of the palette being used.
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwGxCIwTextureImage.png
 *
 * @include IwGxCIwTexture.cpp
 */

#include "s3e.h"
#include "IwGL.h"
#include "IwGx.h"
#include "IwGxPrint.h"
#include "IwMaterial.h"
#include "IwTexture.h"
#include "ExamplesMain.h"

// Main texture for test
// This should be uploaded by end of test update function
CIwTexture* g_Texture = NULL;

enum TEXTURE_TESTS
{
    LOADFROMFILE,
    COPYFROMBUFFER,
    COPYFROMIMAGE,
    SETIMAGE,
    EXTRACTSUBTEXTURE,
    CHANGEPALETTE,
    CHANGETEXELS,
    LOADCOMPRESSED,
    MAX_TEXTURE_TESTS,
};

const char* s_TestNames[]=
{
    "LoadFromFile",
    "CopyFromBuffer",
    "CopyFromImage",
    "SetImage",
    "ExtractSubTexture",
    "ChangePalette",
    "ChangeTexels",
    "LoadCompressed",
};

//-----------------------------------------------------------------------------
// The following function loads a new texture from file if one does not already
// exist using the LoadFromFile() function and then the
// Upload() function is called to put the texture in VRAM.
//-----------------------------------------------------------------------------
void Update_LoadFromFile()
{
    // Skip update if texture already exists
    if (g_Texture) return;

    // Load texture from File
    g_Texture = new CIwTexture;

    g_Texture->LoadFromFile("textures/testTexture_4bit.bmp");

    g_Texture->Upload(); // Upload the result
}

//-----------------------------------------------------------------------------
// The following function creates a colour palette and texel set which are
// used by the CopyFromBuffer() function to create an image of size 0x4 x 0x4.
// The Upload() function is called to put the created texture in VRAM.
//-----------------------------------------------------------------------------
void Update_CopyFromBuffer()
{
    // Skip update if texture already exists
    if (g_Texture) return;

    //Copy from buffer
    uint8 palette[16 * 3] =
    {
        0x00, 0x00, 0x00,
        0x00, 0x00, 0x40,
        0x00, 0x00, 0x80,
        0x00, 0x00, 0xc0,
        0x00, 0x80, 0x00,
        0x00, 0x80, 0x40,
        0x00, 0x80, 0x80,
        0x00, 0x80, 0xc0,
        0x80, 0x80, 0x00,
        0x80, 0x80, 0x40,
        0x80, 0x80, 0x80,
        0x80, 0x80, 0xc0,
        0x80, 0x00, 0x00,
        0x80, 0x00, 0x40,
        0x80, 0x00, 0x80,
        0x80, 0x00, 0xc0,
    };
    uint8 texels[4 * 2] =
    {
        0x10, 0x32,
        0x54, 0x76,
        0x98, 0xba,
        0xdc, 0xfe,
    };

    g_Texture = new CIwTexture;

    g_Texture->CopyFromBuffer(0x4, 0x4, CIwImage::PALETTE4_RGB_888, 2, texels, (uint16*)palette);

    g_Texture->Upload(); // Upload the result
}

//-----------------------------------------------------------------------------
// The following function opens an image file and reads it. It then creates a
// new texture and copies the the texture from the loaded image. The Upload()
// function is used to load the texture into VRAM.
//-----------------------------------------------------------------------------
void Update_CopyFromImage()
{
    // Skip update if texture already exists
    if (g_Texture) return;

    // Copy from Image
    // Load an image directly then copy all the data to a new texture
    CIwImage image;
    s3eFile* pFile = s3eFileOpen("textures/testTexture_4bit.bmp", "rb");
    if (pFile)
    {
        image.ReadFile(pFile);
        s3eFileClose(pFile);

        // Create a new empty texture
        g_Texture = new CIwTexture;

        // Copy image data
        g_Texture->CopyFromImage(&image);

        // Upload the result
        g_Texture->Upload();
    }
}

//-----------------------------------------------------------------------------
// The following function creates a CIwImage object, opens a texture file and
// reads the texture into the CIwImage object. A texture object is created and
// the SetImage() function is used to transfer ownership of the image data
// to the newly created texture object. The Upload() function is then used to
// load the texture into VRAM.
//-----------------------------------------------------------------------------
void Update_SetImage()
{
    // Skip update if texture already exists
    if (g_Texture) return;

    // Copy from Image
    // Load an image directly then transfer buffers to a new texture
    // Ownership of buffers is transfered rather than copied, a faster operation than CopyFromImage.
    CIwImage image;
    s3eFile* pFile = s3eFileOpen("textures/testTexture_4bit.bmp", "rb");
    if (pFile)
    {
        image.ReadFile(pFile);
        s3eFileClose(pFile);

        // Create a new empty texture
        g_Texture = new CIwTexture;

        // Set image data
        // This transfers ownership of pImage's buffers to g_Texture
        g_Texture->SetImage(&image);

        // Upload the result
        g_Texture->Upload();
    }
}

//-----------------------------------------------------------------------------
// The following function creates a CIwTexture object and loads a texture from
// file using the LoadFromFile function. It then uses the ExtractSubTexture()
// function to create a new subtexture from the existing texture. The texture
// then loaded into VRAM using the Upload() function.
//-----------------------------------------------------------------------------
void Update_ExtractSubTexture()
{
    // Skip update if texture already exists
    if (g_Texture) return;

    // Load original texture
    CIwTexture whole_texture;
    whole_texture.LoadFromFile("textures/testTexture_4bit.bmp");

    // Extract Sub Texture - This will return a new texture with the reduced dimensions specified
    // eg: Numbers given will extract image of the number four from the test texture
    g_Texture = whole_texture.ExtractSubTexture(12, 50, 32, 32, "NumberFour");

    g_Texture->Upload(); // Upload the result
}

//-----------------------------------------------------------------------------
// The following function creates a static palette and then loads a texture
// from file using the LoadFromFile() function. The ChangePalette() function is
// then used to change the palette used by the texture by supplying the new
// palette and the data format.
//-----------------------------------------------------------------------------
void Update_ChangePalette()
{
    // Static palette data
    static uint16 pal[16] =
    {
        0x0000, 0x1111, 0x2222, 0x3333,
        0x4444, 0x5555, 0x6666, 0x7777,
        0x8888, 0x9999, 0xaaaa, 0xbbbb,
        0xcccc, 0xdddd, 0xeeee, 0xffff,
    };
    static uint16 pal_change(0);
    pal_change = (pal_change+1)%16;

    // Load texture first time only
    if (!g_Texture)
    {
        // Load texture from File
        g_Texture = new CIwTexture;
        g_Texture->LoadFromFile("textures/testTexture_4bit.bmp");

        // Palettes can be changed pre-upload
        // A copy of the new palette is made into the texture
        g_Texture->ChangePalette(pal, CIwImage::PALETTE4_RGBA_5551);

        // We need to keep the data for changing the palette
        g_Texture->SetModifiable(true);

        // Upload the result
        g_Texture->Upload();
    }
    else
    {
        // Post-Upload Change
        // Change palette entries with random effect
        pal[pal_change] = (uint16)(IwRand()%0xFFFF)|0x1;

        // Change for an animated palette
        g_Texture->ChangePalette(pal, CIwImage::PALETTE4_RGBA_5551 );
    }
}

//-----------------------------------------------------------------------------
// The following function sets a texture palette of 16 colours and a set of 8
// texels. The 4x4 texture displayed using the CopyFromBuffer() function. The
// textures is constructed by changing the texel settings for each
// render loop, which results in a different section of the palette being used.
//-----------------------------------------------------------------------------
void Update_ChangeTexels()
{
    // Static Data
    static uint8 palette[16 * 3] =
    {
        0x00, 0x00, 0x00,
        0x00, 0x00, 0x40,
        0x00, 0x00, 0x80,
        0x00, 0x00, 0xc0,
        0x00, 0x80, 0x00,
        0x00, 0x80, 0x40,
        0x00, 0x80, 0x80,
        0x00, 0x80, 0xc0,
        0x80, 0x80, 0x00,
        0x80, 0x80, 0x40,
        0x80, 0x80, 0x80,
        0x80, 0x80, 0xc0,
        0x80, 0x00, 0x00,
        0x80, 0x00, 0x40,
        0x80, 0x00, 0x80,
        0x80, 0x00, 0xc0,
    };

    // 4*4 image with 4bits per pixel
    static uint8 texels[4 * 2] =
    {
        0x10, 0x32,
        0x54, 0x76,
        0x98, 0xba,
        0xdc, 0xfe,
    };

    static uint16 texel_change(0);
    texel_change = (texel_change+1)%8;

    // Load texture first time only
    if (!g_Texture)
    {
        // Create new texture and copy data from buffers
        g_Texture = new CIwTexture;
        g_Texture->CopyFromBuffer(0x4, 0x4, CIwImage::PALETTE4_RGB_888, 2, texels, (uint16*)palette);

        // Texels can be changed pre-upload using a format given by g_Texture->GetFormat()
        // A copy of the new texel data is made into the texture
        g_Texture->ChangeTexels(texels);

        // We need to keep the data for changing the palette
        g_Texture->SetModifiable(true);

        // Upload the result
        g_Texture->Upload();
    }
    else
    {
        // In Software texels can be changed after upload - (Not in hardware)
        // Texel format for post-upload change is given by g_Texture->GetFormatSW()

        // Change texel entries with random effect
        texels[texel_change] = (uint8)(IwRand()%0xFF);

        // Change for an animated palette
        g_Texture->ChangeTexels(texels, CIwImage::PALETTE4_RGB_888);
    }
}

//-----------------------------------------------------------------------------
// The following function manually loads a new compressed texture from file and
// sets it on a texture using the CIwGxCompressedTextureBlock structure and
// then the Upload() function is called to put the texture in VRAM.
//-----------------------------------------------------------------------------
void Update_LoadCompressed()
{
    // Skip update if texture already exists
    if (g_Texture) return;

    g_Texture = new CIwTexture;

    // Load from File
    uint32 size = (uint32)s3eFileGetFileInt("textures/testTexture.pvr", S3E_FILE_SIZE);
    uint8* pTexBuffer = new uint8[size];

    s3eFile* fp = s3eFileOpen("textures/testTexture.pvr", "rb");
    s3eFileRead(pTexBuffer, 1, size, fp);
    s3eFileClose(fp);

    uint32* ptr = (uint32*)pTexBuffer;
    uint8* ptr2[12];
    uint32 size2[12];
    ptr2[0] = pTexBuffer + 52 + ptr[12];

    // Calculate pointers and sizes for each MIP level (Note: DXT1 is made up of 4x4 block each of which is 8 bytes)
    int h = ptr[6];
    int w = ptr[7];
    for (int i = 0; i < (int)ptr[11]; i++)
    {
        if (h < 4) h = 4;
        if (w < 4) w = 4;
        size2[i] = (w / 4) * (h / 4) * 8;
        h /= 2;
        w /= 2;
    }
    for (int i = 1; i < 7; i++)
        ptr2[i] = ptr2[i-1] + size2[i-1];

    // Create compressed texture block
    CIwGxCompressedTextureBlock* pTexBlock = CIwGxCompressedTextureBlock::Create(CIwImage::DXT1, GL_COMPRESSED_RGB_S3TC_DXT1_EXT, ptr[7], ptr[6], ptr[11], ptr2, size2);

    // No longer need buffer - texture block holds required data now.
    delete[] pTexBuffer;

    // Load image data using compressed texture block.
    CIwImage img;
    img.SetCompressed(pTexBlock);
    g_Texture->CopyFromImage(&img);

    g_Texture->Upload(); // Upload the result
}

typedef void(*UpdateFN)(void);

UpdateFN g_TestUpdateFunctions[]=
{
    Update_LoadFromFile,
    Update_CopyFromBuffer,
    Update_CopyFromImage,
    Update_SetImage,
    Update_ExtractSubTexture,
    Update_ChangePalette,
    Update_ChangeTexels,
    Update_LoadCompressed,
};

uint16 g_CurrentTest = 0;

#define min(_a,_b) ((_a)<(_b)?(_a):(_b))


void CreateButtonsUI(int w, int h)
{
    // Create button
    AddButton("Press 1 for next test", 2,h-50,150,30, s3eKey1);
}

void SurfaceChangedCallback()
{
    DeleteButtons();
    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}

//-----------------------------------------------------------------------------
// The following function sets the screen colour and turns off the lighting.
//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    IwGxRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
    IwGxInit();

    // Turn all lighting off
    IwGxLightingOff();

    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}
//-----------------------------------------------------------------------------
// The following function deletes g_texture if it still exists and termiates
// IwGx functionality.
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    if (g_Texture)
        delete g_Texture;

    // Terminate
    IwGxTerminate();
    IwGxUnRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
}
//-----------------------------------------------------------------------------
// The following function cycles through the list of tests that are available
// when s3eKey1 is pressed.
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    if (CheckButton("Press 1 for next test") & S3E_KEY_STATE_PRESSED)
    {
        g_CurrentTest = (g_CurrentTest+1)%MAX_TEXTURE_TESTS;

        // Clear old assets
        if (g_Texture)
            delete g_Texture;
        g_Texture = NULL;
    }

    // Update current test
    g_TestUpdateFunctions[g_CurrentTest]();

    return true;
}
//-----------------------------------------------------------------------------
// The following function displays the updated texture.
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    if (g_Texture)
    {
        // Allocate a material from the IwGx global cache
        CIwMaterial* pMat = IW_GX_ALLOC_MATERIAL();
        pMat->SetModulateMode(CIwMaterial::MODULATE_NONE);

        // Use Texture on Material
        pMat->SetTexture(g_Texture);

        // Set this as the active material
        IwGxSetMaterial(pMat);

        // Set up screenspace vertex coords
        int16 size = (int16)min(IwGxGetScreenHeight(),IwGxGetScreenWidth()) / 3;
        int16 x1 = (int16)IwGxGetScreenWidth()/2 - size;
        int16 x2 = (int16)IwGxGetScreenWidth()/2 + size;
        int16 y1 = (int16)IwGxGetScreenHeight()/2 - size;
        int16 y2 = (int16)IwGxGetScreenHeight()/2 + size;

        static CIwSVec2 xy3[4];
        xy3[0].x = x1, xy3[0].y = y1;
        xy3[1].x = x1, xy3[1].y = y2;
        xy3[2].x = x2, xy3[2].y = y2;
        xy3[3].x = x2, xy3[3].y = y1;
        IwGxSetVertStreamScreenSpace(xy3, 4);

        static CIwFVec2 uvs[4] =
        {
            CIwFVec2(0, 0),
            CIwFVec2(0, 1),
            CIwFVec2(1, 1),
            CIwFVec2(1, 0),
        };
        IwGxSetUVStream(uvs);

        // Draw single triangle
        IwGxDrawPrims(IW_GX_QUAD_LIST, NULL, 4);
    }

    // Help Text
    IwGxPrintString(2,IwGxGetScreenHeight()-20, s_TestNames[g_CurrentTest], 0);

    // Flush and swap
    IwGxFlush();
    IwGxSwapBuffers();
}
