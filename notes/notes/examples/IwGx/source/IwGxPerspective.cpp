/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */

/**
 * @page ExampleIwGxPerspective IwGx Perspective Example
 *
 * The following example demonstrates how to set a perspective matrix
 * rather than use the nearZ, farZ, and perspMul values
 *
 * The main functions used to achieve this are:
 *  - IwGxSetPerpectiveMatrix()
 *  - IwGxSetPerspMul()
 *  - IwGxSetFarZNearZ()
 *
 * Press 8 to use perspective set by IwGxSetFarZNearZ/IwGxSetPerspMul
 * Press 4 to use a normal perspective matrix
 * Press 2 to use a skewed perspective matrix
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwGxPerspectiveImage.png
 *
 * @include IwGxPerspective.cpp
 */

#include "IwGx.h"
#include "IwGxPrint.h"
#include "IwMaterial.h"
#include "IwMenu.h"
#include "IwTexture.h"
#include "ExamplesMain.h"
#include "IwGL.h"

// Texture object
CIwTexture* s_Texture = NULL;

// Vertex data
const float s = 0x80;
CIwFVec3    s_Verts[24] =
{
    CIwFVec3(-s, -s, -s),
    CIwFVec3( s, -s, -s),
    CIwFVec3( s,  s, -s),
    CIwFVec3(-s,  s, -s),

    CIwFVec3( s, -s, -s),
    CIwFVec3( s, -s,  s),
    CIwFVec3( s,  s,  s),
    CIwFVec3( s,  s, -s),

    CIwFVec3( s, -s,  s),
    CIwFVec3(-s, -s,  s),
    CIwFVec3(-s,  s,  s),
    CIwFVec3( s,  s,  s),

    CIwFVec3(-s, -s,  s),
    CIwFVec3(-s, -s, -s),
    CIwFVec3(-s,  s, -s),
    CIwFVec3(-s,  s,  s),

    CIwFVec3(-s, -s, -s),
    CIwFVec3( s, -s, -s),
    CIwFVec3( s, -s,  s),
    CIwFVec3(-s, -s,  s),

    CIwFVec3( s,  s, -s),
    CIwFVec3(-s,  s, -s),
    CIwFVec3(-s,  s,  s),
    CIwFVec3( s,  s,  s),
};

// Normal data
const float n = 0.577f;
CIwFVec3    s_Norms[24] =
{
    CIwFVec3(-n, -n, -n),
    CIwFVec3( n, -n, -n),
    CIwFVec3( n,  n, -n),
    CIwFVec3(-n,  n, -n),

    CIwFVec3( n, -n, -n),
    CIwFVec3( n, -n,  n),
    CIwFVec3( n,  n,  n),
    CIwFVec3( n,  n, -n),

    CIwFVec3( n, -n,  n),
    CIwFVec3(-n, -n,  n),
    CIwFVec3(-n,  n,  n),
    CIwFVec3( n,  n,  n),

    CIwFVec3(-n, -n,  n),
    CIwFVec3(-n, -n, -n),
    CIwFVec3(-n,  n, -n),
    CIwFVec3(-n,  n,  n),

    CIwFVec3(-n, -n, -n),
    CIwFVec3( n, -n, -n),
    CIwFVec3( n, -n,  n),
    CIwFVec3(-n, -n,  n),

    CIwFVec3( n,  n, -n),
    CIwFVec3(-n,  n, -n),
    CIwFVec3(-n,  n,  n),
    CIwFVec3( n,  n,  n),
};

// UV data
CIwFVec2    s_UVs[24] =
{
    CIwFVec2(0,   0),
    CIwFVec2(1,   0),
    CIwFVec2(1,   1),
    CIwFVec2(0,   1),

    CIwFVec2(0,   0),
    CIwFVec2(1,   0),
    CIwFVec2(1,   1),
    CIwFVec2(0,   1),

    CIwFVec2(0,   0),
    CIwFVec2(1,   0),
    CIwFVec2(1,   1),
    CIwFVec2(0,   1),

    CIwFVec2(0,   0),
    CIwFVec2(1,   0),
    CIwFVec2(1,   1),
    CIwFVec2(0,   1),

    CIwFVec2(0,   0),
    CIwFVec2(1,   0),
    CIwFVec2(1,   1),
    CIwFVec2(0,   1),

    CIwFVec2(0,   0),
    CIwFVec2(1,   0),
    CIwFVec2(1,   1),
    CIwFVec2(0,   1),
};

// Index stream for textured material
uint16      s_QuadList[24] =
{
    0, 3, 2, 1,
    4, 7, 6, 5,
    8, 11, 10, 9,
    12, 15, 14, 13,
    16, 17, 18, 19,
    20, 21, 22, 23,
};

// Angles
CIwFVec3     s_Angles;

// Local matrix
CIwFMat       s_ModelMatrix;

// Materials
CIwMaterial *s_MaterialBG;
CIwMaterial *s_MaterialCube;

// Update timer
uint32       s_Timer = 0;

// Ambient scene light colour
CIwColour    s_ColSceneAmb = {0x20, 0x20, 0x20};

// Diffuse scene light colour
CIwColour    s_ColSceneDiff = {0xf0, 0xf0, 0xf0};

// The following function creates a new material and loads a new texture to be
// used by the spinning cube. It also sets diffuse and ambient lighting for the
// scene using the IwGxSetLightType() function. The IwGxSetLightDirn() is used
// to set the direction of the Diffuse lighting.
void ExampleInit()
{
    // Initialise
    IwGxInit();

    AddButton("Persp Mul: 8", 0, 30, 100, 30, s3eKey1);
    AddButton("Persp Mat 1: 4", 0, 70, 100, 30, s3eKey2);
    AddButton("Persp Mat 2: 2", 0, 110, 100, 30, s3eKey3);

    // set perspective matrix
    float persp[16] = {1,0,0,0, 0,1,0,0, 0,0,1,-1, 0,0,0,0};

    persp[0] = 1.0f;
    persp[5] = 0.6f;
    persp[10] = -1.0f;
    persp[14] = -32.0f;

    IwGxSetPerspectiveMatrix(persp);

    // Initialise angles
    s_Angles = CIwFVec3::g_Zero;

    // Initialise materials
    s_MaterialBG = new CIwMaterial;

    s_MaterialCube = new CIwMaterial;
    s_Texture = new CIwTexture;
    s_Texture->LoadFromFile("./textures/testTexture.bmp");
    s_Texture->Upload();
    s_MaterialCube->SetTexture(s_Texture);

    // Set the view matrix along the -ve z axis
    CIwFMat view = CIwFMat::g_Identity;
    view.t.z = -0x200;
    IwGxSetViewMatrix(&view);

    // Set up scene lighting
    // Set single ambient light
    IwGxSetLightType(0, IW_GX_LIGHT_AMBIENT);

    // Set single diffuse light
    IwGxSetLightType(1, IW_GX_LIGHT_DIFFUSE);
    CIwFVec3 dd(0.577f, 0.577f, 0.577f);
    IwGxSetLightDirn(1, &dd);

    // Set up the menu manager
    new CIwMenuManager;
    IwGetMenuManager()->SetTextCallback(IwGxPrintMenuCallback);
}

// The following function destroys the various classes that have been created
// within the example.
void ExampleShutDown()
{
    delete s_Texture;
    delete IwGetMenuManager();

    delete s_MaterialBG;
    delete s_MaterialCube;

    // Terminate
    IwGxTerminate();
}

// The following function updates the view and the lighting values.
bool ExampleUpdate()
{
    if (CheckButton("Persp Mul: 8") == S3E_KEY_STATE_DOWN)
    {
        // Clear perspective matrix
        IwGxSetPerspectiveMatrix(NULL);

        // Set field of view
        IwGxSetPerspMul(0xa0);

        // Set near and far planes
        IwGxSetFarZNearZ(0x1000, 0x10);
    }
    if (CheckButton("Persp Mat 1: 4") == S3E_KEY_STATE_DOWN)
    {
        // Set normal perspective matrix;
        float persp[16] = {1,0,0,0, 0,1,0,0, 0,0,1,-1, 0,0,0,0};

        persp[0] = 1.0f;
        persp[5] = 0.6f;
        persp[10] = -1.0f;
        persp[14] = -32.0f;

        IwGxSetPerspectiveMatrix(persp);
    }
    if (CheckButton("Persp Mat 2: 2") == S3E_KEY_STATE_DOWN)
    {
        // Set crazy perspective matrix
        float persp[16] = {1,0,0,0, 0,1,0,0, 0,0,1,-1, 0,0,0,0};

        persp[0] = 0.4f;
        persp[5] = 1.533333f;
        persp[10] = -1.0f;
        persp[14] = -32.0f;

        IwGxSetPerspectiveMatrix(persp);
    }

    // Update angles
    s_Angles.x += IW_ANGLE_TO_RADIANS(0x10);
    s_Angles.y += IW_ANGLE_TO_RADIANS(0x24);
    s_Angles.z += IW_ANGLE_TO_RADIANS(0x38);

    // Build model matrix from angles
    CIwFMat rotX, rotY, rotZ;
    rotX.SetRotX(s_Angles.x);
    rotY.SetRotY(s_Angles.y);
    rotZ.SetRotZ(s_Angles.z);
    s_ModelMatrix = rotX * rotY * rotZ;

    // Update timer
    s_Timer++;

    // Update menu manager
    IwGetMenuManager()->Update();

    // Update scene light colours from user colours
    // Set single ambient light
    IwGxSetLightCol(0, &s_ColSceneAmb);
    IwGxSetLightCol(1, &s_ColSceneDiff);

    return true;
}

// The following function renders the spinning cube and any debug menu that has
// selected.
void ExampleRender()
{
    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Render cube
    // Use all lighting
    IwGxLightingOn();

    // Set the model matrix
    IwGxSetModelMatrix(&s_ModelMatrix);

    // Set vertex stream
    IwGxSetVertStreamModelSpace(s_Verts, 24);

    // Set normal stream
    IwGxSetNormStream(s_Norms, 24);

    // Clear colour stream
    IwGxSetColStream(NULL);

    // Set UV stream
    IwGxSetUVStream(s_UVs);

    // Set material
    IwGxSetMaterial(s_MaterialCube);

    // Draw 6 quads
    IwGxDrawPrims(IW_GX_QUAD_LIST, s_QuadList, 24);

    // End drawing
    IwGxFlush();

    // Render menu manager
    IwGetMenuManager()->Render();
    IwGxFlush();

    if (s3eGLGetInt(S3E_GL_VERSION) < 0x200)
        IwGxPrintString(2, 2, "IwGxSetPerpectiveMatrix does not work in < GLES 2.0");

    // Swap buffers
    IwGxSwapBuffers();
}
