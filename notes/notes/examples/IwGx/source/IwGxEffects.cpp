/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
// ExampleIwGxEffects
//-----------------------------------------------------------------------------

/**
 * @page ExampleIwGxEffects IwGx Effects Example
 *
 * The following example demonstrates how materials can use multipass
 * effects.
 *
 * This example is designed to be run in HW rasterisation; multipass effects
 * will only be visible in this mode. In addition, normal mapping will only be
 * visible if the underlying GL drivers support the required extensions.
 *
 * @include IwGxEffects.cpp
 */

#include "s3e.h"
#include "IwGx.h"
#include "IwGxPrint.h"
#include "IwMaterial.h"
#include "IwMenu.h"
#include "IwTexture.h"
#include "ExamplesMain.h"

// Texture objects
CIwTexture* s_Textures[4];

// Torus constants
#define TORUS_NUM_RINGS 24
#define TORUS_NUM_SPANS 12
#define TORUS_NUM_VERTS ((TORUS_NUM_RINGS+1)*(TORUS_NUM_SPANS+1))
#define TORUS_NUM_QUADS ((TORUS_NUM_RINGS  )*(TORUS_NUM_SPANS  ))
#define TORUS_NUM_INDICES (TORUS_NUM_QUADS*4)

// Vertex stream
const float r = 0x50;
const float p = 0xa0;
CIwFVec3    s_Verts[TORUS_NUM_VERTS];

// UV stream
CIwFVec2        s_UVs_0[TORUS_NUM_VERTS]; // Primary UV set   - For diffuse colour
CIwFVec2        s_UVs_1[TORUS_NUM_VERTS]; // Secondary UV set - For effects

// Normal Stream
CIwFVec3    s_Norms[TORUS_NUM_VERTS];

// Tangent and BiTangent Streams
CIwFVec3    s_Tangents[TORUS_NUM_VERTS];
CIwFVec3    s_BiTangents[TORUS_NUM_VERTS];

// Index streams
uint16      s_QuadList[TORUS_NUM_INDICES];

// Angles
CIwFVec3    s_Angles;

// Local matrix
CIwFMat      s_ModelMatrix;

// Materials
CIwMaterial* s_MaterialRef = NULL;

// Ambient scene light colour
CIwColour   s_ColSceneAmb = {0x20, 0x20, 0x20};

// Diffuse scene light colour
CIwColour   s_ColSceneDiff = {0xfb, 0xfb, 0xfb};

// 50% grey for bump colour
CIwColour   s_ColSceneGrey = {0x80, 0x80, 0x80};

// Effect Modes
enum TESTEFFECTS
{
    TEST_NO_2ND_TEXTURE,
    TEST_DEFAULT,
    TEST_REFLECTION,
    TEST_BUMP,
    TEST_SPECULAR,
    NUM_TESTS
};

const char* s_TestNames[]=
{
    "(No 2nd Texture)",
    "EFFECT_DEFAULT",
    "EFFECT_REFLECTION_MAPPING",
    "EFFECT_NORMAL_MAPPING",
    "EFFECT_NORMAL_MAPPING_SPECULAR",
};

enum TESTBLEND
{
    TEST_MODULATE,
    TEST_ADD,
    TEST_REPLACE,
    NUM_BLEND_MODES,
};

const char* s_BlendNames[]=
{
    "BLEND_MODULATE",
    "BLEND_ADD",
    "BLEND_REPLACE",
};

int32 g_EffectMode = TEST_REFLECTION;
int32 g_BlendMode = TEST_ADD;

void BuildTorus()
{
    uint32 v(0);
    uint32 ind(0);
    iwfixed i,j;
    // Build Verts
    for (j=0;j<=TORUS_NUM_RINGS;j++)
    {
        float ring_angle((2.0f*PI*j)/TORUS_NUM_RINGS);
        CIwFVec3 ring_vector( (float)cos(ring_angle), 0, (float)sin(ring_angle) ); // Unit vector towards current ring
        CIwFVec3 ring_pos = ring_vector*p;
        for (i=0;i<=TORUS_NUM_SPANS;i++)
        {
            float span_angle((2.0f*PI*i)/TORUS_NUM_SPANS);

            s_Norms[v]    = ring_vector * (float)cos(span_angle) + CIwFVec3(0,(float)sin(span_angle),0);
            s_BiTangents[v] = ring_vector * (float)sin(span_angle) - CIwFVec3(0,(float)cos(span_angle),0);
            s_Tangents[v] = CIwFVec3(ring_vector.z,0,-ring_vector.x);

            s_Verts[v] = ring_pos + s_Norms[v] * r;

            s_UVs_0[v] = CIwFVec2((float)i/(TORUS_NUM_SPANS+1), (float)j/(TORUS_NUM_RINGS+1));
            s_UVs_1[v] = CIwFVec2((float)(4*i)/(TORUS_NUM_SPANS+1), (float)(4*j)/(TORUS_NUM_RINGS+1));
            v++;
        }
    }
    // Build Quads
    for (j=0;j<TORUS_NUM_RINGS;j++)
    {
        for (i=0;i<TORUS_NUM_SPANS;i++)
        {
            s_QuadList[ind++] = (uint16)(i  +(j  )*(TORUS_NUM_SPANS+1));
            s_QuadList[ind++] = (uint16)(i+1+(j  )*(TORUS_NUM_SPANS+1));
            s_QuadList[ind++] = (uint16)(i+1+(j+1)*(TORUS_NUM_SPANS+1));
            s_QuadList[ind++] = (uint16)(i  +(j+1)*(TORUS_NUM_SPANS+1));
        }
    }
}

//--------------------------------------------------------------------------------
CIwMenu* DebugCreateMainMenu()
{
    IW_CALLSTACK("DebugCreateMainMenu")

    CIwMenu* pMenu = new CIwMenu;

#ifdef IW_DEBUG
    pMenu->AddItem(new CIwMenuItemGx);
#endif
    return pMenu;
}

//-----------------------------------------------------------------------------
void SetupTestMaterial()
{
    // Common setup code - all effects share the same first texture
    s_MaterialRef->SetTexture(s_Textures[0],0);
        s_MaterialRef->SetEffectPreset(CIwMaterial::EFFECT_DEFAULT);
    s_MaterialRef->SetColSpecular(0, 0, 0);


    // Set Blend Mode (Only used when there is a 2nd texture)
    if (g_BlendMode == TEST_MODULATE)
    {
        s_MaterialRef->SetBlendMode(CIwMaterial::BLEND_MODULATE);
    }
    else
    if (g_BlendMode == TEST_ADD)
    {
        s_MaterialRef->SetBlendMode(CIwMaterial::BLEND_ADD);
    }
    else
    if (g_BlendMode == TEST_REPLACE)
    {
        s_MaterialRef->SetBlendMode(CIwMaterial::BLEND_REPLACE);
    }

    // Set Effect mode
    if (g_EffectMode == TEST_NO_2ND_TEXTURE)
    {
        // Don't set 2nd texture
        s_MaterialRef->SetTexture(NULL,1);
    }
    else
    if (g_EffectMode == TEST_DEFAULT)
    {
        // Set 2nd Texture - This can be used for overlay, shadowmap, lightmap, detail textures etc.
        s_MaterialRef->SetTexture(s_Textures[2],1);
    }
    else
    if (g_EffectMode == TEST_REFLECTION)
    {
        // Set Reflection map
        s_MaterialRef->SetTexture(s_Textures[1],1); // Set a glossy specular texture
        s_MaterialRef->SetEffectPreset(CIwMaterial::EFFECT_REFLECTION_MAPPING);
    }
    else
    if (g_EffectMode == TEST_BUMP)
    {
        s_MaterialRef->SetTexture(s_Textures[3],1); // Set a bump map
        s_MaterialRef->SetEffectPreset(CIwMaterial::EFFECT_NORMAL_MAPPING);
    }
    else
    if (g_EffectMode == TEST_SPECULAR)
    {
        s_MaterialRef->SetTexture(s_Textures[3],1); // Set a bump map
        s_MaterialRef->SetEffectPreset(CIwMaterial::EFFECT_NORMAL_MAPPING_SPECULAR);
        s_MaterialRef->SetColSpecular(0xff, 0xff, 0xff);
    }
}


void CreateButtonsUI(int w, int h)
{
    // Create the UI layout
    int32 text_y(h-80);
    AddButton("Press 1 for next Effect", 2, text_y, w, 30, s3eKey1);
    text_y += 32;
    AddButton("Press 2 for next Blend Mode", 2, text_y, w, 30, s3eKey2);
}

void SurfaceChangedCallback()
{
    DeleteButtons();
    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}

//-----------------------------------------------------------------------------
// The following function sets the lighting for the example, loads textures for
// the material, and sets up the menu manager for the scene.
//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    IwGxRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
    IwGxInit();

    // Set field of view
    IwGxSetPerspMul(0xa0);

    // Set near and far planes
    IwGxSetFarZNearZ(0x400, 0x10);

    // Initialise angles
    s_Angles = CIwSVec3::g_Zero;

    //-------------------------------------------------------------------------
    // Set up scene lighting
    //-------------------------------------------------------------------------
    IwGxLightingOn();

    // Set single ambient light
    IwGxSetLightType(0, IW_GX_LIGHT_AMBIENT);

    // Set single diffuse light
    IwGxSetLightType(1, IW_GX_LIGHT_DIFFUSE);
    CIwFVec3 dd(0.577f, 0.577f, 0.577f);
    IwGxSetLightDirn(1, &dd);

    IwGxSetLightCol(0, &s_ColSceneAmb);
    IwGxSetLightCol(1, &s_ColSceneDiff);

    // Build Torus
    BuildTorus();

    // Initialise material to with two textures
    s_Textures[0] = new CIwTexture;
    s_Textures[0]->LoadFromFile("./textures/testTexture_blue.bmp");
    s_Textures[0]->Upload();
    s_Textures[1] = new CIwTexture;
    s_Textures[1]->LoadFromFile("./textures/testTexture_shine.bmp");
    s_Textures[1]->Upload();
    s_Textures[2] = new CIwTexture;
    s_Textures[2]->LoadFromFile("./textures/testTexture_lightmap.bmp");
    s_Textures[2]->Upload();
    s_Textures[3] = new CIwTexture;
    s_Textures[3]->LoadFromFile("./textures/testTexture_bumpmap.bmp");
    s_Textures[3]->Upload();

    s_MaterialRef = new CIwMaterial;

    // Set up the material to use an effect
    SetupTestMaterial();

    // Set the view matrix along the -ve z axis
    CIwFMat view = CIwFMat::g_Identity;
    view.t.z = -0x200;
    IwGxSetViewMatrix(&view);

    // Set up the menu manager
    new CIwMenuManager;
    IwGetMenuManager()->SetTextCallback(IwGxPrintMenuCallback);
    IwGetMenuManager()->SetMainMenuFn(DebugCreateMainMenu);

    // Help Text
    CreateButtonsUI(IwGxGetScreenWidth(), IwGxGetScreenHeight());
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    delete s_Textures[0];
    delete s_Textures[1];
    delete s_Textures[2];
    delete s_Textures[3];
    delete s_MaterialRef;
    delete IwGetMenuManager();

    // Terminate
    IwGxTerminate();
    IwGxUnRegister(IW_GX_SCREENSIZE, SurfaceChangedCallback);
}
//-----------------------------------------------------------------------------
// The following function tests the state of the keyboard and based on this
// sets the blend mode or the effect mode.
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // Get Keyboard input
    if (CheckButton("Press 1 for next Effect") & S3E_KEY_STATE_PRESSED)
    {
        g_EffectMode = (g_EffectMode+1)%NUM_TESTS;

        // Choose normal blend mode for this effect
        switch(g_EffectMode)
        {
        case TEST_REFLECTION:
            g_BlendMode = TEST_ADD;
            break;
        default:
            g_BlendMode = TEST_MODULATE;
            break;
        }

        SetupTestMaterial();
    }

    if (CheckButton("Press 2 for next Blend Mode") & S3E_KEY_STATE_PRESSED)
    {
        g_BlendMode = (g_BlendMode+1)%NUM_BLEND_MODES;
        SetupTestMaterial();
    }

    // Update angles
    s_Angles.x += 3.0f * PI / 1024.0f;
    s_Angles.y += 6.0f * PI / 1024.0f;
    s_Angles.z += 9.0f * PI / 1024.0f;

    // Build model matrix rotation from angles
    CIwFMat rotX, rotY, rotZ;
    rotX.SetRotX(s_Angles.x);
    rotY.SetRotY(s_Angles.y);
    rotZ.SetRotZ(s_Angles.z);
    s_ModelMatrix.CopyRot(rotX * rotY * rotZ);

    // Update menu manager
    IwGetMenuManager()->Update();

    // Update IwGx state time stamp
    IwGxTickUpdate();
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    //-------------------------------------------------------------------------
    // Render torus shape with effect
    //-------------------------------------------------------------------------
    IwGxSetMaterial(s_MaterialRef);

    IwGxSetModelMatrix(&s_ModelMatrix);
    IwGxSetVertStream(s_Verts, TORUS_NUM_VERTS);
    IwGxSetNormStream(s_Norms, TORUS_NUM_VERTS);
    IwGxSetColStream(NULL);
    IwGxSetUVStream(s_UVs_0,0);
    IwGxSetUVStream(s_UVs_1,1);
    IwGxSetTangentStream(s_Tangents);
    IwGxSetBiTangentStream(s_BiTangents);
    IwGxDrawPrims(IW_GX_QUAD_LIST, s_QuadList, TORUS_NUM_INDICES);

    IwGxPrintString(2,IwGxGetScreenHeight()-10, s_TestNames[g_EffectMode], 0);
    IwGxPrintString(2,IwGxGetScreenHeight()-20, s_BlendNames[g_BlendMode], 0);

    // Render menu manager
    IwGetMenuManager()->Render();

    // Flush and swap
    IwGxFlush();
    IwGxSwapBuffers();
}
