/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
//-----------------------------------------------------------------------------
// ExampleIwGxPolygonDisplay
//-----------------------------------------------------------------------------

/**
 * @page ExampleIwGxPolygonDisplay IwGx Polygon Display Example
 *
 * This example demonstrates how to draw a single polygon on screen.
 *
 * The main functions required to achieve this are:
 *  - IwGxGetScreenWidth()
 *  - IwGxGetScreenHeight()
 *  - IwGxSetMaterial()
 *  - IwGxSetVertStreamScreenSpace()
 *  - IwGxSetColStream()
 *  - IwGxDrawPrims()
 *  - IwGxFlush()
 *  - IwGxSwapBuffers()
 *
 * The three main actions performed in this example are:
 *  -# Set a material to be used when drawing.
 *     This is done using the IwGxSetMaterial(), and this material will
 *     be used for all drawing until a new material is set.
 *  -# Take in a vertex stream of three vertices.
 *     This is done using IwGxSetVertStreamScreenSpace().
 *  -# Draw the polygon.
 *     This is done using the IwGxDrawPrims() function.
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwGxPolygonDisplayImage.png
 *
 * @include IwGxPolygonDisplay.cpp
 */


#include "IwGx.h"
#include "IwMaterial.h"

//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    IwGxInit();

    // Turn all lighting off
    IwGxLightingOff();
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    // Terminate
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Allocate a material from the IwGx global cache
    CIwMaterial* pMat = IW_GX_ALLOC_MATERIAL();

    // Set this as the active material
    IwGxSetMaterial(pMat);

    // Set up screenspace vertex coords
    int16 x1 = (int16)IwGxGetScreenWidth() * 1 / 4;
    int16 x2 = (int16)IwGxGetScreenWidth() * 2 / 4;
    int16 x3 = (int16)IwGxGetScreenWidth() * 3 / 4;
    int16 y1 = (int16)IwGxGetScreenHeight() * 1 / 4;
    int16 y3 = (int16)IwGxGetScreenHeight() * 3 / 4;

    static CIwSVec2 xy3[3];
    xy3[0].x = x2, xy3[0].y = y1;
    xy3[1].x = x1, xy3[1].y = y3;
    xy3[2].x = x3, xy3[2].y = y3;
    IwGxSetVertStreamScreenSpace(xy3, 3);

    // Set up vertex colours
    static CIwColour cols[4] =
    {
        {0xff, 0x00, 0x00},
        {0x00, 0xff, 0x00},
        {0x00, 0x00, 0xff},
    };
    IwGxSetColStream(cols, 3);

    // Draw single triangle
    IwGxDrawPrims(IW_GX_TRI_LIST, NULL, 3);

    // End drawing
    IwGxFlush();

    // Swap buffers
    IwGxSwapBuffers();
}
