/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
// ExampleIwGxCameraBasic
//-----------------------------------------------------------------------------

/**
 * @page ExampleIwGxCameraBasic IwGx CameraBasic Example
 *
 * This example is a clone of s3eCameraBasic example, employing IwGx for
 * rendering camera frames.
 * Also at can be viewed as a preceding step to IwGxAugmentedReality example.
 *
 * This example demonstrates the use of the s3eCamera API with IwGx, feeding
 * the output of the camera directly onto a quad as a texture. This method is
 * described fully in the IwGxDynamicTexture example. The camera output format
 * used is chosen to be RGB565_CONVERTED. This format is available on all
 * platforms, although it may be slower than using a native format on a given
 * platform. IwGx can accept textures of this format so no further processing
 * is required.
 *
 * @include IwGxCameraBasic.cpp
 *
 */

#include "s3e.h"
#include "s3eCamera.h"

#include "ExamplesMain.h"

#include "IwGx.h"
#include "IwGxPrint.h"
#include "IwMaterial.h"
#include "IwTexture.h"

enum CameraState
{
    kStarted,
    kStopped,
    kUnSupported
};

static CameraState g_Status = kUnSupported;
static Button* g_ButtonStartLarge = 0;
static Button* g_ButtonStartMedium = 0;
static Button* g_ButtonStartSmall = 0;
static Button* g_ButtonStop = 0;
static s3eBool g_CameraSupported = S3E_FALSE;
static s3eCameraStreamingSizeHint g_CameraSizeHint = S3E_CAMERA_STREAMING_SIZE_HINT_LARGEST;
static bool g_StartCameraOnResume = false;
// Block app from flooding user with error messages when Out-of-memory occurred.
static s3eBool g_OutOfMem = S3E_FALSE;

static CIwTexture* g_CameraFrameTexture = NULL;
static s3eCameraFrameRotation g_FrameRotation = S3E_CAMERA_FRAME_ROT90;

// Start Windows Phone 8 camera on application resume
int32 HandleResume(void* sysData, void* userData)
{
    if (g_StartCameraOnResume)
    {
        g_StartCameraOnResume = false;
        if (s3eCameraStart(g_CameraSizeHint, S3E_CAMERA_PIXEL_TYPE_RGB565_CONVERTED) == S3E_RESULT_SUCCESS)
            g_Status = kStarted;
    }

    return 0;
}

int32 HandleSuspend(void* sysData, void* userData)
{
    if (g_Status == kStarted)
    {
        s3eCameraStop();
        g_StartCameraOnResume = true;
    }

    return 0;
}

//-----------------------------------------------------------------------------
// Camera callback
//
// Copy the capture frame buffer into a texture ready for rendering.
// This function will be called during s3eDeviceYield() so cannot conflict with
// Render calls.
//-----------------------------------------------------------------------------
static int32 cameraUpdate(void* systemData, void*)
{
    if (g_OutOfMem)
        return 0;

    // Stray camera callbacks can be received even after s3eCameraStop() was called!
    if (!g_CameraFrameTexture)
        return 0;

    s3eCameraFrameData *data = (s3eCameraFrameData*)systemData;

    // Optimize copying of camera frame data to the texture when frame size was not changed.
    if (int(g_CameraFrameTexture->GetWidth())  == data->m_Width &&
        int(g_CameraFrameTexture->GetHeight()) == data->m_Height)
    {
        memcpy(g_CameraFrameTexture->GetTexels(), data->m_Data, data->m_Height * data->m_Pitch);
    }
    else if (!g_OutOfMem)
    {
        g_CameraFrameTexture->CopyFromBuffer(data->m_Width, data->m_Height, CIwImage::RGB_565, data->m_Pitch, (uint8*)data->m_Data, NULL);
        g_CameraFrameTexture->Upload();

        g_OutOfMem = (s3eMemoryGetError() == S3E_MEMORY_ERR_NO_MEM);
    }
    g_FrameRotation = data->m_Rotation;

    return 0;
}

static int32 cameraStop(void* systemData, void*)
{
    g_Status = kStopped;
    return 0;
}

void ButtonCallbackStart(struct Button* button)
{
    if (g_Status != kStopped)
    {
        return;
    }

    if (button == g_ButtonStartSmall)
    {
        g_CameraSizeHint = S3E_CAMERA_STREAMING_SIZE_HINT_SMALLEST;
    }
    else if (button == g_ButtonStartMedium)
    {
        g_CameraSizeHint = S3E_CAMERA_STREAMING_SIZE_HINT_MEDIUM;
    }
    else if (button == g_ButtonStartLarge)
    {
        g_CameraSizeHint = S3E_CAMERA_STREAMING_SIZE_HINT_LARGEST;
    }

    if (s3eCameraStart(g_CameraSizeHint, S3E_CAMERA_PIXEL_TYPE_RGB565_CONVERTED) == S3E_RESULT_SUCCESS)
    {
        g_Status = kStarted;
    }

    // Reset g_OutOfMem just in case there is enough memory for smaller resolution.
    g_OutOfMem = S3E_FALSE;
}

void ButtonCallbackStop(struct Button* button)
{
    if (s3eCameraStop() == S3E_RESULT_SUCCESS)
    {
        g_Status = kStopped;
    }
}

void ExampleInit()
{
    g_ButtonStartLarge  = NewButton("Start (Large)",  ButtonCallbackStart);
    g_ButtonStartMedium = NewButton("Start (Medium)", ButtonCallbackStart);
    g_ButtonStartSmall  = NewButton("Start (Small)",  ButtonCallbackStart);
    g_ButtonStop        = NewButton("Stop",           ButtonCallbackStop);

    g_ButtonStartLarge->m_Enabled  = false;
    g_ButtonStartMedium->m_Enabled = false;
    g_ButtonStartSmall->m_Enabled  = false;
    g_ButtonStop->m_Enabled        = false;

    g_CameraSupported = s3eCameraAvailable();

    if (g_CameraSupported)
    {
        if (S3E_RESULT_ERROR == s3eCameraRegister(S3E_CAMERA_UPDATE_STREAMING, cameraUpdate, 0))
        {
            AppendMessage("Could not register frame receiver callback");
        }
        if (S3E_RESULT_ERROR == s3eCameraRegister(S3E_CAMERA_STOP_STREAMING, cameraStop, 0))
        {
            AppendMessage("Could not register stop camera callback");
        }
        g_Status = kStopped;
    }
    else
    {
        AppendMessage("s3eCamera not available");
    }

    // Handle suspend/resume on Windows Phone 8 to start/stop
    // camera depending on application state
    if (s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WP8)
    {
        s3eDeviceRegister(S3E_DEVICE_PAUSE, HandleSuspend, NULL);
        s3eDeviceRegister(S3E_DEVICE_UNPAUSE, HandleResume, NULL);
    }

    g_CameraFrameTexture = new CIwTexture();
    g_CameraFrameTexture->SetMipMapping(false);
    g_CameraFrameTexture->SetModifiable(true);

    // Turn all lighting off
    IwGxLightingOff();
}

//-----------------------------------------------------------------------------
// The following function deletes textures if they exist and terminates
// IwGx and s3e API functionality.
//-----------------------------------------------------------------------------
void ExampleTerm()
{
    // stop API services
    if (s3eCameraAvailable())
    {
        s3eCameraStop();
        s3eCameraUnRegister(S3E_CAMERA_UPDATE_STREAMING, cameraUpdate);
        s3eCameraUnRegister(S3E_CAMERA_STOP_STREAMING, cameraStop);
    }

    // Release texture
    delete g_CameraFrameTexture;
    g_CameraFrameTexture = NULL;

    // Unregister handlers
    if (s3eDeviceGetInt(S3E_DEVICE_OS) == S3E_OS_ID_WP8)
    {
        s3eDeviceUnRegister(S3E_DEVICE_PAUSE, HandleSuspend);
        s3eDeviceUnRegister(S3E_DEVICE_UNPAUSE, HandleResume);
    }
}

//-----------------------------------------------------------------------------

bool ExampleUpdate()
{
    if (g_CameraSupported)
    {
        g_ButtonStartLarge->m_Enabled = (g_Status == kStopped);
        g_ButtonStartMedium->m_Enabled = (g_Status == kStopped);
        g_ButtonStartSmall->m_Enabled = (g_Status == kStopped);
        g_ButtonStop->m_Enabled = (g_Status == kStarted);
    }

    return true;
}

//-----------------------------------------------------------------------------
void CameraFrameRender()
{
    // Refresh dynamic texture
    g_CameraFrameTexture->ChangeTexels(g_CameraFrameTexture->GetTexels(), CIwImage::RGB_565);
    // Allocate a material from the IwGx global cache
    CIwMaterial* pMat = IW_GX_ALLOC_MATERIAL();
    pMat->SetModulateMode(CIwMaterial::MODULATE_NONE);

    // Use Texture on Material
    pMat->SetTexture(g_CameraFrameTexture);

    // Set this as the active material
    IwGxSetMaterial(pMat);

    // Draw behind the view space
    IwGxSetScreenSpaceSlot(-1);

    // Set up screenspace vertex coords
    // (We are drawing camera image over whole screen)
    const int16 x1 = 0;
    const int16 x2 = (int16)IwGxGetScreenWidth();
    const int16 y1 = 0;
    const int16 y2 = (int16)IwGxGetScreenHeight();

    static CIwSVec2 xy3[4];
    xy3[0].x = x1, xy3[0].y = y1;
    xy3[1].x = x1, xy3[1].y = y2;
    xy3[2].x = x2, xy3[2].y = y2;
    xy3[3].x = x2, xy3[3].y = y1;

    // Camera image must not rotate with surface
    static CIwFVec2 normal_uvs[4] =
    {
        CIwFVec2(0, 0),
        CIwFVec2(0, 1),
        CIwFVec2(1, 1),
        CIwFVec2(1, 0),
    };

    // rotate UV coordinates based on rotation
    // (image must rotate against rotation of surface,
    // assuming that the camera image is NOT auto rotated)
    static CIwFVec2 uvs[4];
    for (int j=0; j<4; j++)
    {
        uvs[j] = normal_uvs[(4 - (int)g_FrameRotation + j ) % 4];
    }

    IwGxSetUVStream(uvs);
    IwGxSetVertStreamScreenSpace(xy3, 4);

    // Draw single quad in screen space for camera image
    IwGxDrawPrims(IW_GX_QUAD_LIST, NULL, 4);
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Display status info
    int32 camStatus;
    const int textHeight = s3eDebugGetInt(S3E_DEBUG_FONT_SIZE_HEIGHT);

    int ypos = GetYBelowButtons() + textHeight * 2;

    if (g_Status == kStarted)
    {
        camStatus = s3eCameraGetInt(S3E_CAMERA_STATUS);
        if (camStatus == S3E_CAMERA_IDLE)
            IwGxPrintString(10, ypos, "`x666666Camera is idle", 0);
        else if (camStatus == S3E_CAMERA_STREAMING)
            IwGxPrintString(10, ypos, "`x666666Camera is streaming", 0);
        else if (camStatus == S3E_CAMERA_FAILED)
            IwGxPrintString(10, ypos, "`x666666Error occurred during capture", 0);
        else if (camStatus == S3E_CAMERA_SUSPENDED)
            IwGxPrintString(10, ypos, "`x666666Camera is paused due to device suspend", 0);
        else if (camStatus == S3E_CAMERA_RESTARTING)
            IwGxPrintString(10, ypos, "`x666666Camera is being restarted after a device suspend", 0);

        if (g_OutOfMem)
            IwGxPrintString(10, ypos += textHeight * 2, "`x666666Out of Memory occurred. Cannot render camera frame", 0);
    }
    else if (g_Status == kStopped)
        IwGxPrintString(10, ypos, "`x666666Camera Stopped", 0);
    else
        IwGxPrintString(10, ypos, "`x666666The camera is not available", 0);

    // CIwTexture::UPLOADED_F flag indicates camera frame was uploaded in cameraUpdate()
    if (g_Status == kStarted && (g_CameraFrameTexture->GetFlags() & CIwTexture::UPLOADED_F) && !g_OutOfMem)
    {
        CameraFrameRender();
    }
}
