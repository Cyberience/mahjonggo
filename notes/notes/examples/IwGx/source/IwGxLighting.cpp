/*
 * This file is part of the Marmalade SDK Code Samples.
 *
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This source code is intended only as a supplement to the Marmalade SDK.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 */
// ExampleIwGxLighting
//-----------------------------------------------------------------------------

/**
 * @page ExampleIwGxLighting IwGx Lighting Example
 *
 * IwGx uses the same lighting model as OpenGL ES.
 * The lighting for a material is calculated using the following formula:
 *
 * <i>C_v = M_e + (M_a * S_a) + (M_d * S_d * Falloff) + (M_s * S_s * SpecularFalloff)</i>
 *
 * Where:
 *  - <i>C_v</i> is the calculated vertex colour.
 *  - <i>M_e</i> is the material emissive colour.
 *  - <i>M_a</i> is the material ambient colour (ambient response).
 *  - <i>M_d</i> is the material diffuse colour (diffuse response).
 *  - <i>M_s</i> is the material specular colour (specular response).
 *  - <i>S_a</i> is the scene ambient colour (colour of single ambient light).
 *  - <i>S_d</i> is the scene diffuse colour (colour of single diffuse light).
 *  - <i>S_s</i> is the scene specular colour (specular colour of single diffuse light).
 *  - <i>Falloff</i> is the dot product falloff calculated from vertex normal and single diffuse light direction.
 *  - <i>SpecularFalloff</i> is the dot product falloff calculated from the
 *    half vector between the camera direction and the single diffuse light direction.
 *
 * Lighting is set using colour values. All possible lighting values are
 * combined using the above formula and then applied to each vertex. If a
 * colour stream is also supplied, then the supplied colour replaces both M_a
 * and M_d at each vertex.
 *
 * @note For a detailed description of how to implement Lighting Response
 * in your application, see the @ref lighting "Lighting" section of the
 * <i>IwGx API Documentation</i>
 *
 * This example demonstrates use of the lighting model. An ambient scene
 * light and diffuse scene light are created. The diffuse light points at
 * 90 degrees to each axis. Each of the four cubes uses its own
 * dynamically-modified material, and prior to rendering, enables or disables
 * some components of the lighting equation as follows:
 *  - Cube 0 (top-left): enables emissive lighting only, and ramps its material emissive colour from black to white.
 *  - Cube 1 (top-right): enables ambient lighting only, and ramps its material ambient response from black to cyan.
 *  - Cube 2 (bottom-left): enables diffuse lighting only, and ramps its material diffuse response from black to magenta.
 *  - Cube 3 (bottom-right): enables all lighting, and ramps its material emissive colour from black to blue.
 *
 * The example also demonstrates use of the debug menu manager: Hit F1 to
 * toggle up the menu manager. Use up, down, right and left to navigate the
 * menus. Use NumPad+ and NumPad- to modify values. Hold down SHIFT or CONTROL
 * to modify a value in steps of 10 and 100 respectively.
 *
 * Try modifying the ambient and diffuse colours. Notice how the cube colours
 * change, and think about how the lighting model is working.
 *
 * The following graphics illustrates the example output.
 *
 * @image html IwGxLightingImage.png
 *
 * @include IwGxLighting.cpp
 */

#include "IwGx.h"
#include "IwGxPrint.h"
#include "IwMaterial.h"
#include "IwMenu.h"

#include "ExamplesMain.h"

// Vertex data
const float s = 0x80;
CIwFVec3    s_Verts[8] =
{
    CIwFVec3(-s, -s, -s),
    CIwFVec3( s, -s, -s),
    CIwFVec3( s,  s, -s),
    CIwFVec3(-s,  s, -s),
    CIwFVec3(-s, -s,  s),
    CIwFVec3( s, -s,  s),
    CIwFVec3( s,  s,  s),
    CIwFVec3(-s,  s,  s),
};

// Normal data
const float n = 0.577f;
CIwFVec3    s_Norms[8] =
{
    CIwFVec3(-n, -n, -n),
    CIwFVec3( n, -n, -n),
    CIwFVec3( n,  n, -n),
    CIwFVec3(-n,  n, -n),
    CIwFVec3(-n, -n,  n),
    CIwFVec3( n, -n,  n),
    CIwFVec3( n,  n,  n),
    CIwFVec3(-n,  n,  n),
};

// Index stream
uint16      s_TriStrip[22] =
{
    1, 2, 5, 6, 4, 7, 0, 3, 1, 2,
    2, 4, // degenerates
    4, 0, 5, 1,
    1, 3, // degenerates
    3, 7, 2, 6,
};

// Angles
CIwFVec3     s_Angles;

// Local matrix
CIwFMat      s_ModelMatrix;

// Materials
CIwMaterial *s_MaterialBG;
CIwMaterial *s_MaterialCubes[4];

// Update timer
uint32       s_Timer = 0;

// Ambient scene light colour
CIwColour    s_ColSceneAmb = {0x80, 0x80, 0x80};

// Diffuse scene light colour
CIwColour    s_ColSceneDiff = {0x80, 0x80, 0x80};

//--------------------------------------------------------------------------------
CIwMenu* DebugCreateMainMenu()
{
    IW_CALLSTACK("DebugCreateMainMenu")

    CIwMenu* pMenu = new CIwMenu;

#ifdef IW_DEBUG
    pMenu->AddItem(new CIwMenuItemGx);
    pMenu->AddItem(new CIwMenuItemEditUInt8("ColSceneAmb.r", &s_ColSceneAmb.r));
    pMenu->AddItem(new CIwMenuItemEditUInt8("ColSceneAmb.g", &s_ColSceneAmb.g));
    pMenu->AddItem(new CIwMenuItemEditUInt8("ColSceneAmb.b", &s_ColSceneAmb.b));
    pMenu->AddItem(new CIwMenuItemEditUInt8("ColSceneDiff.r", &s_ColSceneDiff.r));
    pMenu->AddItem(new CIwMenuItemEditUInt8("ColSceneDiff.g", &s_ColSceneDiff.g));
    pMenu->AddItem(new CIwMenuItemEditUInt8("ColSceneDiff.b", &s_ColSceneDiff.b));
#endif
    return pMenu;
}
//-----------------------------------------------------------------------------
void ExampleInit()
{
    // Initialise
    IwGxInit();

    // Set screen clear colour
    IwGxSetColClear(0x40, 0x40, 0x40, 0x00);

    // Set field of view
    IwGxSetPerspMul(0xa0);

    // Set near and far planes
    IwGxSetFarZNearZ(0x1000, 0x10);

    // Initialise angles
    s_Angles = CIwFVec3::g_Zero;

    // Set the view matrix along the -ve z axis
    CIwFMat view = CIwFMat::g_Identity;
    view.t.z = -0x400;
    IwGxSetViewMatrix(&view);

    s_MaterialBG = new CIwMaterial();
    s_MaterialCubes[0] = new CIwMaterial();
    s_MaterialCubes[1] = new CIwMaterial();
    s_MaterialCubes[2] = new CIwMaterial();
    s_MaterialCubes[3] = new CIwMaterial();

    // Note: If ambient light is not enabled, the material ambient is added without being modulated by an ambient light value
    // So set to black to remove all ambient contribution from these cubes
    s_MaterialCubes[0]->SetColAmbient(0x00000000);
    s_MaterialCubes[2]->SetColAmbient(0x00000000);

    //-------------------------------------------------------------------------
    // Set up scene lighting
    //-------------------------------------------------------------------------
    // Set single ambient light
    IwGxSetLightType(0, IW_GX_LIGHT_AMBIENT);
    IwGxSetLightCol(0, 0x80, 0x80, 0x80);

    // Set single diffuse light
    IwGxSetLightType(1, IW_GX_LIGHT_DIFFUSE);
    IwGxSetLightCol(1, 0x80, 0x80, 0x80);
    CIwFVec3 dd(0.577f, 0.577f, 0.577f);
    IwGxSetLightDirn(1, &dd);

    // Set up the menu manager
    new CIwMenuManager;
    IwGetMenuManager()->SetTextCallback(IwGxPrintMenuCallback);
    IwGetMenuManager()->SetMainMenuFn(DebugCreateMainMenu);
}
//-----------------------------------------------------------------------------
void ExampleShutDown()
{
    delete s_MaterialBG;
    delete s_MaterialCubes[0];
    delete s_MaterialCubes[1];
    delete s_MaterialCubes[2];
    delete s_MaterialCubes[3];

    delete IwGetMenuManager();

    // Terminate
    IwGxTerminate();
}
//-----------------------------------------------------------------------------
bool ExampleUpdate()
{
    // Update angles
    s_Angles.x += 1.0f * PI / 128.0f;
    s_Angles.y += 9.0f * PI / 512.0f;
    s_Angles.z += 7.0f * PI / 256.0f;

    // Build model matrix from angles
    s_ModelMatrix.SetRotY(s_Angles.y);

    // Update timer
    s_Timer++;

    // Update menu manager
    IwGetMenuManager()->Update();

    // Update scene light colours from user colours
    // Set single ambient light
    IwGxSetLightCol(0, &s_ColSceneAmb);
    IwGxSetLightCol(1, &s_ColSceneDiff);
    return true;
}
//-----------------------------------------------------------------------------
void ExampleRender()
{
    // Clear the screen
    IwGxClear(IW_GX_COLOUR_BUFFER_F | IW_GX_DEPTH_BUFFER_F);

    // Start with no lighting
    IwGxLightingOff();

    //-------------------------------------------------------------------------
    // Render background
    //-------------------------------------------------------------------------
    // Set active material
    IwGxSetMaterial(s_MaterialBG);

    // Set colour stream
    static CIwColour bgCols[4] =
    {
        {0xff, 0x00, 0x00},
        {0x00, 0xff, 0x00},
        {0x00, 0x00, 0xff},
        {0x00, 0xff, 0xff},
    };
    IwGxSetColStream(bgCols, 4);

    // Set the (screenspace) vertex stream
    CIwSVec2* pCoords = AllocClientScreenRectangle();
    IwGxSetScreenSpaceOrg(&CIwSVec2::g_Zero);
    IwGxSetScreenSpaceSlot(-1);
    IwGxSetVertStreamScreenSpace(pCoords, 4);

    // Draw the textured primitives
    IwGxDrawPrims(IW_GX_QUAD_STRIP, NULL, 4);

    //-------------------------------------------------------------------------
    // Render cubes
    //-------------------------------------------------------------------------
    // Clear colour stream
    IwGxSetColStream(NULL);

    // Set normal stream once
    IwGxSetNormStream(s_Norms, 8);

    //-------------------------------------------------------------------------
    // Cube 0 (top-left)... use emissive only, ramp emissive colour from black to white
    //-------------------------------------------------------------------------
    s_MaterialCubes[0]->SetColEmissive(((s_Timer << 2) & 0xff) * 0x010101);
    IwGxSetMaterial(s_MaterialCubes[0]);
    IwGxLightingEmissive(true);

    s_ModelMatrix.t.x = -0x100;
    s_ModelMatrix.t.y = -0x0a0;
    IwGxSetModelMatrix(&s_ModelMatrix);
    IwGxSetVertStreamModelSpace(s_Verts, 16);
    IwGxDrawPrims(IW_GX_TRI_STRIP, s_TriStrip, 22);

    //-------------------------------------------------------------------------
    // Cube 1 (top-right)... use ambient only, ramp ambient response from black to cyan
    //-------------------------------------------------------------------------
    s_MaterialCubes[1]->SetColAmbient(((s_Timer << 2) & 0xff) * 0x010100);
    IwGxSetMaterial(s_MaterialCubes[1]);
    IwGxLightingEmissive(false);
    IwGxLightingAmbient(true);

    s_ModelMatrix.t.x =  0x100;
    s_ModelMatrix.t.y = -0x0a0;
    IwGxSetModelMatrix(&s_ModelMatrix);
    IwGxSetVertStreamModelSpace(s_Verts, 16);
    IwGxDrawPrims(IW_GX_TRI_STRIP, s_TriStrip, 22);

    //-------------------------------------------------------------------------
    // Cube 2 (bottom-left)... use diffuse and specular, ramp diffuse response from black to magenta
    //-------------------------------------------------------------------------
    s_MaterialCubes[2]->SetColDiffuse(((s_Timer << 2) & 0xff) * 0x010001);
    s_MaterialCubes[2]->SetColSpecular(0xff, 0xff, 0xff);
    s_MaterialCubes[2]->SetSpecularPower(2);
    IwGxSetMaterial(s_MaterialCubes[2]);
    IwGxLightingAmbient(false);
    IwGxLightingDiffuse(true);
    IwGxLightingSpecular(true);

    s_ModelMatrix.t.x = -0x100;
    s_ModelMatrix.t.y =  0x100;
    IwGxSetModelMatrix(&s_ModelMatrix);
    IwGxSetVertStreamModelSpace(s_Verts, 16);
    IwGxDrawPrims(IW_GX_TRI_STRIP, s_TriStrip, 22);

    //-------------------------------------------------------------------------
    // Cube 3 (bottom-right)... use emissive, ambient and diffuse. Ramp emissive from black to blue
    //-------------------------------------------------------------------------
    s_MaterialCubes[3]->SetColEmissive(((s_Timer << 2) & 0xff) * 0x010000);
    s_MaterialCubes[3]->SetColAmbient(0x404040);
    s_MaterialCubes[3]->SetColDiffuse(0xc0c0c0);
    IwGxSetMaterial(s_MaterialCubes[3]);
    IwGxLightingOn();

    s_ModelMatrix.t.x =  0x100;
    s_ModelMatrix.t.y =  0x100;
    IwGxSetModelMatrix(&s_ModelMatrix);
    IwGxSetVertStreamModelSpace(s_Verts, 16);
    IwGxDrawPrims(IW_GX_TRI_STRIP, s_TriStrip, 22);

    // End drawing
    IwGxFlush();

    // Render menu manager
    IwGetMenuManager()->Render();
    IwGxFlush();

    // Swap buffers
    IwGxSwapBuffers();
}
