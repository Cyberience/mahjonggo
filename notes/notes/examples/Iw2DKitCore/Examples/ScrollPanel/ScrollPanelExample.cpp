/*
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This document is protected by copyright, and contains information
 * proprietary to Marmalade.
 *
 * This file consists of source code released by Marmalade under
 * the terms of the accompanying End User License Agreement (EULA).
 * Please do not use this program/source code before you have read the
 * EULA and have agreed to be bound by its terms.
 */

#include "GameManager.h"
#include "SceneImporter.h"
#include "Button.h"
#include "Label.h"
#include "ScrollPanel.h"

// The 2D engine lives in the Iw2DEngine namespace
M2DKIT_USE_NAMESPACE
M2DKITCORE_USE_NAMESPACE
M2DKITENGINE_USE_NAMESPACE

// The main game manager
static CGameManager* g_Game;
static shared_ptr<CRootNode> g_RootNode;
static shared_ptr<CLabel> g_ButtonLabel;
static shared_ptr<CSprite> g_Sprite;
static shared_ptr<CScrollPanel> g_ScrollPanel;

bool g_SpriteAttached = false;

// The scenes unique Id
static int s_SceneId = -1;

const char* kButtonPath = "Scene.Button";
const char* kButtonLabelPath = "Scene.Button.Label";
const char* kSpritePath = "Scene.Sprite";
const char* kScrollPanelPath = "Scene.ScrollPanel";

void ButtonReleasedCallback(CEventArgs* args)
{
    g_SpriteAttached = !g_SpriteAttached;
    if (g_SpriteAttached)
    {
        g_RootNode->RemoveChild(g_Sprite);
        g_ScrollPanel->AddChild(g_Sprite);
        g_ButtonLabel->SetText("Remove Sprite from ScrollPanel");
    }
    else
    {
        g_ScrollPanel->RemoveChild(g_Sprite);
        g_RootNode->AddChild(g_Sprite);
        g_ButtonLabel->SetText("Add Sprite to ScrollPanel");
    }
}

int main()
{
    // Create and initialise the game manager
    g_Game = new CGameManager();
    g_Game->Init();

    // Import the scene and its associated resources
    CSceneContainer* sc = g_Game->GetSceneContainer();
    const int zIndex = 0;
    sc->LoadSceneFromDisk("Scene.json", "Scene.resources", zIndex, &s_SceneId);

    //Create a scope to make the shared pointers go out of scope before we're unloading the scene.
    {
        // Attach pressed and released event callbacks to button
        shared_ptr<CButton> btn = sc->GetNode<CButton>(s_SceneId, kButtonPath);
        IwAssertMsg(2DENGINE, btn != 0, ("%s not found", kButtonPath));
        btn->SubscribeEvent(BUTTON_EVENT_RELEASED, ButtonReleasedCallback);

        g_RootNode = sc->GetScene(s_SceneId)->GetRootNode();
        IwAssertMsg(2DENGINE, g_RootNode != 0, ("Root node not found"));

        g_ButtonLabel = sc->GetNode<CLabel>(s_SceneId, kButtonLabelPath);
        IwAssertMsg(2DENGINE, g_ButtonLabel != 0, ("%s not found", kButtonLabelPath));

        g_Sprite = sc->GetNode<CSprite>(s_SceneId, kSpritePath);
        IwAssertMsg(2DENGINE, g_Sprite != 0, ("%s not found", kSpritePath));

        g_ScrollPanel = sc->GetNode<CScrollPanel>(s_SceneId, kScrollPanelPath);
        IwAssertMsg(2DENGINE, g_ScrollPanel != 0, ("%s not found", kScrollPanelPath));

        // Run the game
        g_Game->Run();
    }

    g_RootNode.reset();
    g_ButtonLabel.reset();
    g_Sprite.reset();
    g_ScrollPanel.reset();

    sc->UnloadScene(s_SceneId);
    // Destroy the game manager
    delete g_Game;

    return 0;
}
