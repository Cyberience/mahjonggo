/*
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This document is protected by copyright, and contains information
 * proprietary to Marmalade.
 *
 * This file consists of source code released by Marmalade under
 * the terms of the accompanying End User License Agreement (EULA).
 * Please do not use this program/source code before you have read the
 * EULA and have agreed to be bound by its terms.
 */

#include "GameManager.h"
#include "GameManagerEvents.h"
#include "SceneImporter.h"
#include "Sprite.h"

// The 2D engine lives in the Iw2DEngine namespace
M2DKIT_USE_NAMESPACE
M2DKITCORE_USE_NAMESPACE
M2DKITENGINE_USE_NAMESPACE

// The main game manager
CGameManager* g_Game;

// The scenes unique Id
static int s_SceneId = -1;


void CreateGame()
{
    // Get the scene container
    CSceneContainer* sc = g_Game->GetSceneContainer();

    // Create a scene
    sc->CreateScene("Scene", 0, 1024.0f, 768.0f, &s_SceneId);
    CScene* scene = sc->GetScene(s_SceneId);
    
    // Create sprites
    CSpriteCreationParams params;
    params.m_Name = "Sprite1";
    params.m_Dimensions = CIwFVec2(128.0f, 128.0f);
    params.m_Position = CIwFVec2(256.f, 128.0f);
    params.m_Pivot = CIwFVec2(0.5f, 0.5f);
    params.m_SpriteFrame = scene->CreateSpriteFrame("assets/barrel.png", "barrel1", CIwRect32(0, 0, 0, 0));
    sc->CreateNode<CSprite>(params, s_SceneId, "Scene");

    params.m_Name = "Sprite2";
    params.m_Position = CIwFVec2(128.0f, 128.0f);
    sc->CreateNode<CSprite>(params, s_SceneId, "Scene");

    params.m_Name = "Sprite3";
    params.m_Position = CIwFVec2(512.0f, 512.0f);
    sc->CreateNode<CSprite>(params, s_SceneId, "Scene");
}

void UpdateGame(CEventArgs* args)
{
    // Get the scene container
    CSceneContainer* sc = g_Game->GetSceneContainer();

    // Get the sprites scene
    CScene* scene = sc->GetScene(s_SceneId);

    // Move sprite 1
    TSpritePtr sprite1 = dynamic_pointer_cast<CSprite>(scene->FindNodeByPath("Scene.sprite1"));
    CIwFVec2 pos = sprite1->GetPosition() + CIwFVec2(1.0f, 0.0f);
    if (pos.x > 1024.0f) pos.x -= 1024.0f;
    sprite1->SetPosition(pos);

    // Scale sprite 2
    TSpritePtr sprite2 = dynamic_pointer_cast<CSprite>(scene->FindNodeByPath("Scene.sprite2"));
    CIwFVec2 scale = sprite2->GetScale() + CIwFVec2(0.01f, 0.01f);
    if (scale.x > 2.0f) scale = CIwFVec2(1.0f, 1.0f);
    sprite2->SetScale(scale);

    // Rotate sprite 3
    TSpritePtr sprite3 = dynamic_pointer_cast<CSprite>(scene->FindNodeByPath("Scene.sprite3"));
    sprite3->SetRotation(sprite3->GetRotation() + 1.0f);
}

int main()
{
    // Create and initialise the game manager
    g_Game = new CGameManager();
    g_Game->Init();

    // Create the game
    CreateGame();

    // Subscribe to the game managers update event so we can run our own game logic
    g_Game->SubscribeEvent(GAMEMANAGER_EVENT_UPDATE, UpdateGame);

    // Run the game
    g_Game->Run();

    // Destroy the game manager
    delete g_Game;

    return 0;
}
