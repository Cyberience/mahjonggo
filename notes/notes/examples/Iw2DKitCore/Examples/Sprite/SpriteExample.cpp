/*
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This document is protected by copyright, and contains information
 * proprietary to Marmalade.
 *
 * This file consists of source code released by Marmalade under
 * the terms of the accompanying End User License Agreement (EULA).
 * Please do not use this program/source code before you have read the
 * EULA and have agreed to be bound by its terms.
 */

#include "GameManager.h"
#include "SceneImporter.h"
#include "Button.h"
#include "Label.h"

// The 2D engine lives in the Iw2DEngine namespace
M2DKIT_USE_NAMESPACE
M2DKITCORE_USE_NAMESPACE
M2DKITENGINE_USE_NAMESPACE

// The main game manager
static CGameManager* g_Game;
static shared_ptr<CSprite> g_Sprite1;
static shared_ptr<CSprite> g_Sprite2;

// The scenes unique Id
static int s_SceneId = -1;

const char* kButtonPath = "Scene.Button";
const char* kSprite1Path = "Scene.Sprite1";
const char* kSprite2Path = "Scene.Sprite2";

void ButtonReleasedCallback(CEventArgs* args)
{
    TSpriteFramePtr frame1 = g_Sprite1->GetSpriteFrame();
    TSpriteFramePtr frame2 = g_Sprite2->GetSpriteFrame();
    g_Sprite1->SetSpriteFrame(frame2);
    g_Sprite2->SetSpriteFrame(frame1);
}

int main()
{
    // Create and initialise the game manager
    g_Game = new CGameManager();
    g_Game->Init();

    // Import the scene and its associated resources
    CSceneContainer* sc = g_Game->GetSceneContainer();
    const int zIndex = 0;
    sc->LoadSceneFromDisk("Scene.json", "Scene.resources", zIndex, &s_SceneId);

    //Create a scope to make the shared pointers go out of scope before we're unloading the scene.
    {
        // Attach pressed and released event callbacks to button
        shared_ptr<CButton> btn = sc->GetNode<CButton>(s_SceneId, kButtonPath);
        IwAssertMsg(2DENGINE, btn != 0, ("%s not found", kButtonPath));
        btn->SubscribeEvent(BUTTON_EVENT_RELEASED, ButtonReleasedCallback);

        g_Sprite1 = sc->GetNode<CSprite>(s_SceneId, kSprite1Path);
        IwAssertMsg(2DENGINE, g_Sprite1 != 0, ("%s not found", kSprite1Path));

        g_Sprite2 = sc->GetNode<CSprite>(s_SceneId, kSprite2Path);
        IwAssertMsg(2DENGINE, g_Sprite2 != 0, ("%s not found", kSprite2Path));

        // Run the game
        g_Game->Run();
    }

    g_Sprite1.reset();
    g_Sprite2.reset();

    sc->UnloadScene(s_SceneId);
    // Destroy the game manager
    delete g_Game;

    return 0;
}
