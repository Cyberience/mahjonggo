/*
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This document is protected by copyright, and contains information
 * proprietary to Marmalade.
 *
 * This file consists of source code released by Marmalade under
 * the terms of the accompanying End User License Agreement (EULA).
 * Please do not use this program/source code before you have read the
 * EULA and have agreed to be bound by its terms.
 */

#include "GameManager.h"
#include "SceneImporter.h"
#include "Button.h"
#include "Label.h"

// The 2D engine lives in the Iw2DEngine namespace
M2DKIT_USE_NAMESPACE
M2DKITCORE_USE_NAMESPACE
M2DKITENGINE_USE_NAMESPACE

// The main game manager
static CGameManager* g_Game;
static CSceneContainer* g_SceneContainer;
static int g_SceneId = -1;

static shared_ptr<CLabel> g_Label;

struct Info
{
    const char* m_Description;
    IwGxFontAlignVer m_AlignVer;
    IwGxFontAlignHor m_AlignHor;
    CIwColour m_Colour;
};

std::map<CButton*, Info> g_ButtonMap;

void ButtonReleasedCallback(CEventArgs* args);

void AddButton(const char* path, const char* description, IwGxFontAlignVer alignVer, IwGxFontAlignHor alignHor)
{
    shared_ptr<CButton> btn = g_SceneContainer->GetNode<CButton>(g_SceneId, path);
    IwAssertMsg(2DENGINE, btn != 0, ("%s not found", path));
    btn->SubscribeEvent(BUTTON_EVENT_RELEASED, ButtonReleasedCallback);

    Info info;
    info.m_Description = description;
    info.m_AlignVer = alignVer;
    info.m_AlignHor = alignHor;

    g_ButtonMap.insert(std::make_pair(btn.get(), info));
}

void ButtonReleasedCallback(CEventArgs* args)
{
    CButtonEventReleasedArgs* _args = (CButtonEventReleasedArgs*)args;
    CButton* button = (CButton*)_args->m_Source;
    const Info& info = g_ButtonMap[button];

    g_Label->SetText(info.m_Description);
    g_Label->SetAlignV(info.m_AlignVer);
    g_Label->SetAlignH(info.m_AlignHor);
}

const char* kLabelPath = "Scene.Panel.TextPanel.Label";

int main()
{
    // Create and initialise the game manager
    g_Game = new CGameManager();
    g_Game->Init();

    // Import the scene and its associated resources
    g_SceneContainer = g_Game->GetSceneContainer();
    const int zIndex = 0;
    g_SceneContainer->LoadSceneFromDisk("Scene.json", "Scene.resources", zIndex, &g_SceneId);

    //Create a scope to make the shared pointers go out of scope before we're unloading the scene.
    {
        AddButton("Scene.Panel.ButtonTopLeft",      "Top Left",      IW_GX_FONT_ALIGN_TOP, IW_GX_FONT_ALIGN_LEFT);
        AddButton("Scene.Panel.ButtonTopCenter",    "Top Center",    IW_GX_FONT_ALIGN_TOP, IW_GX_FONT_ALIGN_CENTRE);
        AddButton("Scene.Panel.ButtonTopRight",     "Top Right",     IW_GX_FONT_ALIGN_TOP, IW_GX_FONT_ALIGN_RIGHT);
        AddButton("Scene.Panel.ButtonCenterLeft",   "Center Left",   IW_GX_FONT_ALIGN_MIDDLE, IW_GX_FONT_ALIGN_LEFT);
        AddButton("Scene.Panel.ButtonCenterCenter", "Center Center", IW_GX_FONT_ALIGN_MIDDLE, IW_GX_FONT_ALIGN_CENTRE);
        AddButton("Scene.Panel.ButtonCenterRight",  "Center Right",  IW_GX_FONT_ALIGN_MIDDLE, IW_GX_FONT_ALIGN_RIGHT);
        AddButton("Scene.Panel.ButtonBottomLeft",   "Bottom Left",   IW_GX_FONT_ALIGN_BOTTOM, IW_GX_FONT_ALIGN_LEFT);
        AddButton("Scene.Panel.ButtonBottomCenter", "Bottom Center", IW_GX_FONT_ALIGN_BOTTOM, IW_GX_FONT_ALIGN_CENTRE);
        AddButton("Scene.Panel.ButtonBottomRight",  "Bottom Right",  IW_GX_FONT_ALIGN_BOTTOM, IW_GX_FONT_ALIGN_RIGHT);

        g_Label = g_SceneContainer->GetNode<CLabel>(g_SceneId, kLabelPath);
        IwAssertMsg(2DENGINE, g_Label != 0, ("%s not found", kLabelPath));

        // Run the game
        g_Game->Run();
    }

    g_Label.reset();

    g_ButtonMap.clear();

    g_SceneContainer->UnloadScene(g_SceneId);
    // Destroy the game manager
    delete g_Game;

    return 0;
}
