#category: 2D Kit

This example shows how to maintain Label node.
There are shown following features:
1. Text
    It's shown how to set Label's text.
2. Alignment
    It's shown how set Label's horizontal and vertical alignment.
