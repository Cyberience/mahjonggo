/*
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This document is protected by copyright, and contains information
 * proprietary to Marmalade.
 *
 * This file consists of source code released by Marmalade under
 * the terms of the accompanying End User License Agreement (EULA).
 * Please do not use this program/source code before you have read the
 * EULA and have agreed to be bound by its terms.
 */

#include "GameManager.h"
#include "SceneImporter.h"

// The 2D engine lives in the Iw2DEngine namespace
M2DKITCORE_USE_NAMESPACE
M2DKITENGINE_USE_NAMESPACE

// The main game manager
CGameManager* g_pGame;

std::vector<int>* g_SceneIds;

int main()
{
    // Create and initialise the game manager
    g_pGame = new CGameManager();
    g_pGame->Init();

    // Import the scene and its associated resources
    // Note that the scene ID must be unique for each loaded scene as it will be used to refer to the 
    // scene when looking for nodes
    CSceneContainer* sc = g_pGame->GetSceneContainer();
    g_SceneIds = new std::vector<int>();

    char scenePath[1024];
    char resourcesPath[1024];

    bool result = true;
    for (int n = 1; result; ++n)
    {
        const int zIndex = n;
        int sceneId = -1;
        sprintf(scenePath, "Scene%d.json", n);
        sprintf(resourcesPath, "Scene%d.resources", n);

        result = sc->LoadSceneFromDisk(scenePath, resourcesPath, zIndex, &sceneId);
        if (result)
        {
            (*g_SceneIds).push_back(sceneId);
        }
    }

    // Set scaling mode
    for (size_t i = 0; i < (*g_SceneIds).size(); ++i)
    {
        sc->SetSceneFitToScreenMode((*g_SceneIds)[i], VirtualResolution::FitToScreenModeWidthOrHeight);
    }

    // Run scene
    g_pGame->Run();

    for (size_t i = 0; i < (*g_SceneIds).size(); ++i)
    {
        sc->UnloadScene((*g_SceneIds)[i]);
    }

    delete g_SceneIds;
    // Destroy and clean-up game
    delete g_pGame;

    return 0;
}
