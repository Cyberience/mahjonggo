/*
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This document is protected by copyright, and contains information
 * proprietary to Marmalade.
 *
 * This file consists of source code released by Marmalade under
 * the terms of the accompanying End User License Agreement (EULA).
 * Please do not use this program/source code before you have read the
 * EULA and have agreed to be bound by its terms.
 */

#include "GameManager.h"
#include "SceneImporter.h"
#include "Button.h"
#include "ProgressBar.h"
#include "Label.h"

// The 2D engine lives in the Iw2DEngine namespace
M2DKIT_USE_NAMESPACE
M2DKITCORE_USE_NAMESPACE
M2DKITENGINE_USE_NAMESPACE

// The main game manager
static CGameManager* g_Game;
static CSceneContainer* g_SceneContainer;
static int g_SceneId = -1;

static shared_ptr<CButton> g_ButtonDec;
static shared_ptr<CButton> g_ButtonInc;
static shared_ptr<CProgressBar> g_ProgressBar;
static shared_ptr<CLabel> g_Label;

void ButtonReleasedCallback(CEventArgs* args)
{
    CButtonEventReleasedArgs* _args = (CButtonEventReleasedArgs*)args;
    CButton* button = (CButton*)_args->m_Source;

    float progress = 0.0f;
    const float step = 0.05f;

    if (g_ButtonDec.get() == button)
        progress = g_ProgressBar->GetProgress() - step;
    else if (g_ButtonInc.get() == button)
        progress = g_ProgressBar->GetProgress() + step;

    g_ProgressBar->SetProgress(progress);

    char buf[256]; sprintf(buf, "%d %%", (int)(100 * g_ProgressBar->GetProgress()));
    g_Label->SetText(buf);
}

const char* kButtonIncPath = "Scene.ButtonInc";
const char* kButtonDecPath = "Scene.ButtonDec";
const char* kProgressBarPath = "Scene.ProgressBarBg.ProgressBar";
const char* kLabelPath = "Scene.ProgressBarBg.Label";

int main()
{
    // Create and initialise the game manager
    g_Game = new CGameManager();
    g_Game->Init();

    // Import the scene and its associated resources
    g_SceneContainer = g_Game->GetSceneContainer();
    const int zIndex = 0;
    g_SceneContainer->LoadSceneFromDisk("Scene.json", "Scene.resources", zIndex, &g_SceneId);

    //Create a scope to make the shared pointers go out of scope before we're unloading the scene.
    {
        g_ButtonDec = g_SceneContainer->GetNode<CButton>(g_SceneId, kButtonDecPath);
        IwAssertMsg(2DENGINE, g_ButtonDec != 0, ("%s not found", kButtonDecPath));
        g_ButtonDec->SubscribeEvent(BUTTON_EVENT_RELEASED, ButtonReleasedCallback);

        g_ButtonInc = g_SceneContainer->GetNode<CButton>(g_SceneId, kButtonIncPath);
        IwAssertMsg(2DENGINE, g_ButtonInc != 0, ("%s not found", kButtonIncPath));
        g_ButtonInc->SubscribeEvent(BUTTON_EVENT_RELEASED, ButtonReleasedCallback);

        g_ProgressBar = g_SceneContainer->GetNode<CProgressBar>(g_SceneId, kProgressBarPath);
        IwAssertMsg(2DENGINE, g_ProgressBar != 0, ("%s not found", kProgressBarPath));

        g_Label = g_SceneContainer->GetNode<CLabel>(g_SceneId, kLabelPath);
        IwAssertMsg(2DENGINE, g_Label != 0, ("%s not found", kLabelPath));

        // Run the game
        g_Game->Run();
    }

    g_ButtonDec.reset();
    g_ButtonInc.reset();
    g_ProgressBar.reset();
    g_Label.reset();

    g_SceneContainer->UnloadScene(g_SceneId);
    // Destroy the game manager
    delete g_Game;

    return 0;
}
