#category: 2D Kit

This example shows how Z-index affects node's draw order.
There are shown following cases:
1. Sibling nodes.
    The sibling nodes draws in order to theirs Z-indexes ascending.
2. Parent and child.
    The child node draws above it's parent node if it's Z-index is more or equal than 0.
    Otherwise, it draws below it's parent node.

Drag a corresponding slider to change node's Z-index.
