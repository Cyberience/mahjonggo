/*
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This document is protected by copyright, and contains information
 * proprietary to Marmalade.
 *
 * This file consists of source code released by Marmalade under
 * the terms of the accompanying End User License Agreement (EULA).
 * Please do not use this program/source code before you have read the
 * EULA and have agreed to be bound by its terms.
 */

#include "GameManager.h"
#include "SceneImporter.h"
#include "Checkbox.h"
#include "Label.h"
#include "Slider.h"
#include "SliderEvents.h"

// The 2D engine lives in the Iw2DEngine namespace
M2DKIT_USE_NAMESPACE
M2DKITCORE_USE_NAMESPACE
M2DKITENGINE_USE_NAMESPACE

// The main game manager
static CGameManager* g_Game;
static CSceneContainer* g_SceneContainer;
static int g_SceneId = -1;

struct Info
{
    CSprite* m_Sprite;
    CLabel*  m_Label;
};

static std::map<CSlider*, Info> g_Infos;

void SliderValueChangedCallback(CEventArgs* args)
{
    CSliderValueChangedArgs* _args = (CSliderValueChangedArgs*)args;
    CSlider* slider = (CSlider*)_args->m_Source;
    float zIndex = _args->m_NewValue - 10.0f;

    Info& info = g_Infos[slider];

    char buf[256]; sprintf(buf, "%s: Z = %.2f", info.m_Sprite->GetName().GetString().c_str(), zIndex);
    info.m_Label->SetText(buf);
    info.m_Sprite->SetZIndex(zIndex);
}

void AddInfo(const char* sliderPath, const char* spritePath, const char* labelPath)
{
    CSlider* slider = g_SceneContainer->GetNode<CSlider>(g_SceneId, sliderPath).get();
    IwAssertMsg(2DENGINE, slider != 0, ("%s not found", sliderPath));
    slider->SetMinValue(0.0f);
    slider->SetMaxValue(20.0f);
    slider->SetValue(10.0f);
    slider->SubscribeEvent(SLIDER_EVENT_VALUE_CHANGED, SliderValueChangedCallback);

    CSprite* sprite = g_SceneContainer->GetNode<CSprite>(g_SceneId, spritePath).get();
    IwAssertMsg(2DENGINE, sprite != 0, ("%s not found", spritePath));

    CLabel* label = g_SceneContainer->GetNode<CLabel>(g_SceneId, labelPath).get();
    IwAssertMsg(2DENGINE, label != 0, ("%s not found", labelPath));

    Info info;
    info.m_Sprite = sprite;
    info.m_Label = label;

    g_Infos.insert(std::make_pair(slider, info));
}

int main()
{
    // Create and initialise the game manager
    g_Game = new CGameManager();
    g_Game->Init();

    // Import the scene and its associated resources
    g_SceneContainer = g_Game->GetSceneContainer();
    const int zIndex = 0;
    g_SceneContainer->LoadSceneFromDisk("Scene.json", "Scene.resources", zIndex, &g_SceneId);

    //Create a scope to make the shared pointers go out of scope before we're unloading the scene.
    {
        AddInfo("Scene.Sibling1Slider", "Scene.Sibling 1", "Scene.Sibling 1.Label");
        AddInfo("Scene.Sibling2Slider", "Scene.Sibling 2", "Scene.Sibling 2.Label");
        AddInfo("Scene.ParentSlider", "Scene.Parent", "Scene.Parent.Label");
        AddInfo("Scene.ChildSlider", "Scene.Parent.Child", "Scene.Parent.Child.Label");

        // Run the game
        g_Game->Run();
    }

    g_Infos.clear();

    g_SceneContainer->UnloadScene(g_SceneId);
    // Destroy the game manager
    delete g_Game;

    return 0;
}
