/*
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This document is protected by copyright, and contains information
 * proprietary to Marmalade.
 *
 * This file consists of source code released by Marmalade under
 * the terms of the accompanying End User License Agreement (EULA).
 * Please do not use this program/source code before you have read the
 * EULA and have agreed to be bound by its terms.
 */

#include "GameManager.h"
#include "Sprite.h"
#include "SceneImporter.h"
#include "Tween.h"
#include "AnimationSchema.h"
#include "AnimationInstance.h"

#include <shared_ptr.hpp>

// The 2D engine lives in the Iw2DEngine namespace
M2DKIT_USE_NAMESPACE
M2DKITCORE_USE_NAMESPACE
M2DKITENGINE_USE_NAMESPACE

// The main game manager
CGameManager* g_pGame;

void SetupScene()
{
    // Import scene
    const int zIndex = 0;
    int sceneId = -1;
    g_pGame->GetSceneContainer()->LoadSceneFromDisk("scene.json", "scene.resources", zIndex, &sceneId);
    g_pGame->GetSceneContainer()->SetSceneFitToScreenMode(sceneId, VirtualResolution::FitToScreenModeWidthOrHeight);

    // Find Barrel1 sprite
    shared_ptr<CSprite> sprite1 = g_pGame->GetSceneContainer()->GetNode<CSprite>(sceneId, "Scene.FieldObjects.Barrel1");

    CScene* scene = g_pGame->GetSceneContainer()->GetScene(sceneId);
    TAnimationSchemaPtr rotationSchema = scene->CreateAnimationSchema("RotationAnim");

    CAnimationSchemaNode& root = rotationSchema->GetRoot();
    shared_ptr<CAnimationTrack<float> > rotationTrack = root.RequestNewAnimationTrack<float>("Rotation");

    CKeyFrame<float> rotationKeyFrame1(0.0f, 0.0f, ExpIn, 2.0f, AbsoluteKeyFrameType); //timeInMilliseconds, val, easing, easingValue, type
    rotationTrack->GetKeyFrames().push_back(rotationKeyFrame1);

    CKeyFrame<float> rotationKeyFrame2(2.0f, 180.0f, ExpOut, 2.0f, AbsoluteKeyFrameType);
    rotationTrack->GetKeyFrames().push_back(rotationKeyFrame2);

    CKeyFrame<float> rotationKeyFrame3(4.0f, 0.0f, ExpIn, 2.0f, AbsoluteKeyFrameType);
    rotationTrack->GetKeyFrames().push_back(rotationKeyFrame3);

    sprite1->GetAnimationContainer().AddAnimation(rotationSchema)->SetRepeatCount(0);

    // Find MenuButton sprite
    shared_ptr<CSprite> sprite2 = g_pGame->GetSceneContainer()->GetNode<CSprite>(sceneId, "Scene.MenuButton");

    TAnimationSchemaPtr scaleSchema = scene->CreateAnimationSchema("ScaleAnim");

    CAnimationSchemaNode& scaleRoot = scaleSchema->GetRoot();
    shared_ptr<CAnimationTrack<CIwFVec2> > scaleTrack = scaleRoot.RequestNewAnimationTrack<CIwFVec2>("Scale");

    CKeyFrame<CIwFVec2> scaleKeyFrame1(0.0f, CIwFVec2(1.0f, 1.0f), Linear, 1.0f, AbsoluteKeyFrameType);
    scaleTrack->GetKeyFrames().push_back(scaleKeyFrame1);

    CKeyFrame<CIwFVec2> scaleKeyFrame2(0.5f, CIwFVec2(1.3f, 1.4f), Linear, 1.0f, AbsoluteKeyFrameType);
    scaleTrack->GetKeyFrames().push_back(scaleKeyFrame2);

    CKeyFrame<CIwFVec2> scaleKeyFrame3(1.0f, CIwFVec2(1.4f, 1.3f), Linear, 1.0f, AbsoluteKeyFrameType);
    scaleTrack->GetKeyFrames().push_back(scaleKeyFrame3);

    CKeyFrame<CIwFVec2> scaleKeyFrame4(1.5f, CIwFVec2(1.0f, 1.0f), Linear, 1.0f, AbsoluteKeyFrameType);
    scaleTrack->GetKeyFrames().push_back(scaleKeyFrame4);

    sprite2->GetAnimationContainer().AddAnimation(scaleSchema)->SetRepeatCount(0);
}

int main()
{
    g_pGame = new CGameManager();
    g_pGame->Init();

    SetupScene();

    g_pGame->Run();
    delete g_pGame;

    return 0;
}
