/*
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This document is protected by copyright, and contains information
 * proprietary to Marmalade.
 *
 * This file consists of source code released by Marmalade under
 * the terms of the accompanying End User License Agreement (EULA).
 * Please do not use this program/source code before you have read the
 * EULA and have agreed to be bound by its terms.
 */

#include "GameManager.h"
#include "SceneImporter.h"

// The 2D engine lives in the Iw2DEngine namespace
M2DKITCORE_USE_NAMESPACE
M2DKITENGINE_USE_NAMESPACE

// The main game manager
CGameManager* g_pGame;

int main()
{
    // Create and initialise the game manager
    g_pGame = new CGameManager();
    g_pGame->Init();

    // Import the scene and its associated resources
    // Note that the scene ID must be unique for each loaded scene as it will be used to refer to the 
    // scene when looking for nodes
    CSceneContainer* sc = g_pGame->GetSceneContainer();
    const int zIndex = 0;
    int sceneId = -1;
    sc->LoadSceneFromDisk("scene.json", "scene.resources", zIndex, &sceneId);

    // Set scaling mode
    sc->SetSceneFitToScreenMode(sceneId, VirtualResolution::FitToScreenModeExpand);

    // Run scene
    g_pGame->Run();

    //Unload scene
    sc->UnloadScene(sceneId);

    // Destroy and clean-up game
    delete g_pGame;

    return 0;
}