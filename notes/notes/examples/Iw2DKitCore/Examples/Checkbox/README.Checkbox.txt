#category: 2D Kit

This example shows how to maintain Checkbox node.
There are shown following features:
1. Enable
    It's shown how to enable/disable checkbox.
2. Events
    It's shown how to subscribe checkbox changed event and how to react it.
