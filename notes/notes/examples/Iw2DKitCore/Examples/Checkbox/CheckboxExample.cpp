/*
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This document is protected by copyright, and contains information
 * proprietary to Marmalade.
 *
 * This file consists of source code released by Marmalade under
 * the terms of the accompanying End User License Agreement (EULA).
 * Please do not use this program/source code before you have read the
 * EULA and have agreed to be bound by its terms.
 */

#include "GameManager.h"
#include "SceneImporter.h"
#include "Button.h"
#include "Checkbox.h"
#include "Label.h"

// The 2D engine lives in the Iw2DEngine namespace
M2DKIT_USE_NAMESPACE
M2DKITCORE_USE_NAMESPACE
M2DKITENGINE_USE_NAMESPACE

// The main game manager
static CGameManager* g_Game;
static shared_ptr<CLabel> g_Label;
static shared_ptr<CButton> g_SlaveButton;
static shared_ptr<CCheckbox> g_SlaveCheckbox;
static shared_ptr<CLabel> g_SlaveCheckboxLabel;
static shared_ptr<CLabel> g_SlaveButtonLabel;

// The scenes unique Id
static int s_SceneId = -1;

const char* kCheckboxPath = "Scene.Checkbox";
const char* kLabelPath = "Scene.Checkbox.Label";
const char* kSlaveButtonPath = "Scene.SlaveButton";
const char* kSlaveCheckboxPath = "Scene.SlaveCheckbox";
const char* kSlaveButtonLabelPath = "Scene.SlaveButton.Label";
const char* kSlaveCheckboxLabelPath = "Scene.SlaveCheckbox.Label";

void CheckboxChangedCallback(CEventArgs* args)
{
    CCheckboxEventCheckChangedArgs* _args = (CCheckboxEventCheckChangedArgs*)args;
    CCheckbox* checkbox = (CCheckbox*)_args->m_Source;

    if (_args->m_Checked)
    {
        g_Label->SetText("Disable widgets");
        g_SlaveButton->SetEnabled(true);
        g_SlaveCheckbox->SetEnabled(true);
        g_SlaveCheckboxLabel->SetText("Checkbox is enabled");
        g_SlaveButtonLabel->SetText("Button is enabled");
    }
    else
    {
        g_Label->SetText("Enable widgets");
        g_SlaveButton->SetEnabled(false);
        g_SlaveCheckbox->SetEnabled(false);
        g_SlaveCheckboxLabel->SetText("Checkbox is disabled");
        g_SlaveButtonLabel->SetText("Button is disabled");
    }
}

int main()
{
    // Create and initialise the game manager
    g_Game = new CGameManager();
    g_Game->Init();

    // Import the scene and its associated resources
    CSceneContainer* sc = g_Game->GetSceneContainer();
    const int zIndex = 0;
    sc->LoadSceneFromDisk("Scene.json", "Scene.resources", zIndex, &s_SceneId);

    //Create a scope to make the shared pointers go out of scope before we're unloading the scene.
    {
        // Attach check changed event callback to checkbox
        shared_ptr<CCheckbox> cb = sc->GetNode<CCheckbox>(s_SceneId, kCheckboxPath);
        IwAssertMsg(2DENGINE, cb != 0, ("%s not found", kCheckboxPath));
        cb->SubscribeEvent(CHECKBOX_EVENT_CHECK_CHANGED, CheckboxChangedCallback);

        g_Label = sc->GetNode<CLabel>(s_SceneId, kLabelPath);
        IwAssertMsg(2DENGINE, g_Label != 0, ("%s not found", kLabelPath));

        g_SlaveButton = sc->GetNode<CButton>(s_SceneId, kSlaveButtonPath);
        IwAssertMsg(2DENGINE, g_SlaveButton != 0, ("%s not found", kSlaveButtonPath));

        g_SlaveCheckbox = sc->GetNode<CCheckbox>(s_SceneId, kSlaveCheckboxPath);
        IwAssertMsg(2DENGINE, g_SlaveCheckbox != 0, ("%s not found", kSlaveCheckboxPath));

        g_SlaveButtonLabel = sc->GetNode<CLabel>(s_SceneId, kSlaveButtonLabelPath);
        IwAssertMsg(2DENGINE, g_SlaveButtonLabel != 0, ("%s not found", kSlaveButtonLabelPath));

        g_SlaveCheckboxLabel = sc->GetNode<CLabel>(s_SceneId, kSlaveCheckboxLabelPath);
        IwAssertMsg(2DENGINE, g_SlaveCheckboxLabel != 0, ("%s not found", kSlaveCheckboxLabelPath));

        // Run the game
        g_Game->Run();
    }

    g_Label.reset();
    g_SlaveButton.reset();
    g_SlaveCheckbox.reset();
    g_SlaveButtonLabel.reset();
    g_SlaveCheckboxLabel.reset();

    sc->UnloadScene(s_SceneId);
    // Destroy the game manager
    delete g_Game;

    return 0;
}
