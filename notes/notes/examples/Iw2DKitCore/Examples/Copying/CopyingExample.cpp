/*
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This document is protected by copyright, and contains information
 * proprietary to Marmalade.
 *
 * This file consists of source code released by Marmalade under
 * the terms of the accompanying End User License Agreement (EULA).
 * Please do not use this program/source code before you have read the
 * EULA and have agreed to be bound by its terms.
 */

#include "GameManager.h"
#include "SceneImporter.h"
#include "Button.h"
#include "Label.h"
#include "Slider.h"
#include "SliderEvents.h"

// The 2D engine lives in the Iw2DEngine namespace
M2DKIT_USE_NAMESPACE
M2DKITCORE_USE_NAMESPACE
M2DKITENGINE_USE_NAMESPACE

// The main game manager
static CGameManager* g_Game;
static CSceneContainer* g_SceneContainer;
static int g_SceneId = -1;

const int g_MaxRows = 2;
const int g_MaxCols = 5;

static int g_CurRow = 0;
static int g_CurCol = 0;

shared_ptr<CButton> g_CopyButton;
shared_ptr<CButton> g_DeleteButton;
shared_ptr<CSprite> g_Sprite;

shared_ptr<CSprite> g_CopiedSprites[g_MaxRows * g_MaxCols];

void CopyButtonReleasedCallback(CEventArgs* args)
{
    g_CurRow++;
    if (g_CurRow >= g_MaxRows)
    {
        g_CurRow = 0;
        g_CurCol++;
    }

    shared_ptr<CSprite> copiedSprite = g_Sprite->Copy();
    g_Sprite->GetParent()->AddChild(copiedSprite);

    copiedSprite->SetPosition(g_Sprite->GetPosition() + CIwFVec2(g_CurCol * 150.0f, -g_CurRow * 210.0f));
    ((CLabel*)copiedSprite->FindChildByName("Label").get())->SetText("copy");

    g_CopiedSprites[g_MaxRows * g_CurCol + g_CurRow] = copiedSprite;

    g_DeleteButton->SetEnabled(true);
    if ((g_CurRow == g_MaxRows - 1) && (g_CurCol == g_MaxCols - 1))
    {
        g_CopyButton->SetEnabled(false);
    }
}

void DeleteButtonReleasedCallback(CEventArgs* args)
{
    shared_ptr<CSprite> copiedSprite = g_CopiedSprites[g_MaxRows * g_CurCol + g_CurRow];
    copiedSprite->GetParent()->RemoveChild(copiedSprite);
    g_CopiedSprites[g_MaxRows * g_CurCol + g_CurRow] = shared_ptr<CSprite>();

    g_CurRow--;
    if (g_CurRow < 0)
    {
        g_CurRow = g_MaxRows - 1;
        g_CurCol--;
    }

    g_CopyButton->SetEnabled(true);
    if ((g_CurRow == 0) && (g_CurCol == 0))
    {
        g_DeleteButton->SetEnabled(false);
    }
}

const char* kCopyButtonPath = "Scene.CopyButton";
const char* kDeleteButtonPath = "Scene.DeleteButton";
const char* kSpritePath = "Scene.Sprite";

int main()
{
    // Create and initialise the game manager
    g_Game = new CGameManager();
    g_Game->Init();

    // Import the scene and its associated resources
    g_SceneContainer = g_Game->GetSceneContainer();
    const int zIndex = 0;
    g_SceneContainer->LoadSceneFromDisk("Scene.json", "Scene.resources", zIndex, &g_SceneId);

    //Create a scope to make the shared pointers go out of scope before we're unloading the scene.
    {
        g_CopyButton = g_SceneContainer->GetNode<CButton>(g_SceneId, kCopyButtonPath);
        IwAssertMsg(2DENGINE, g_CopyButton != 0, ("%s not found", kCopyButtonPath));
        g_CopyButton->SubscribeEvent(BUTTON_EVENT_RELEASED, CopyButtonReleasedCallback);

        g_DeleteButton = g_SceneContainer->GetNode<CButton>(g_SceneId, kDeleteButtonPath);
        IwAssertMsg(2DENGINE, g_DeleteButton != 0, ("%s not found", kDeleteButtonPath));
        g_DeleteButton->SubscribeEvent(BUTTON_EVENT_RELEASED, DeleteButtonReleasedCallback);

        g_Sprite = g_SceneContainer->GetNode<CSprite>(g_SceneId, kSpritePath);
        IwAssertMsg(2DENGINE, g_Sprite != 0, ("%s not found", kSpritePath));

        // Run the game
        g_Game->Run();
    }

    g_CopyButton.reset();
    g_DeleteButton.reset();
    g_Sprite.reset();

    for (int i = 0; i < g_MaxCols * g_MaxRows; ++i)
        g_CopiedSprites[i] = shared_ptr<CSprite>();


    g_SceneContainer->UnloadScene(g_SceneId);
    // Destroy the game manager
    delete g_Game;

    return 0;
}
