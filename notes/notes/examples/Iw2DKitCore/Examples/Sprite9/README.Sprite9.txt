#category: 2D Kit

This example shows how to maintain Sprite9 nodes in code.
It's shown how Scale9 node draws with different sizes.

Drag a corresponding slider to change Scale9's width or height.
