/*
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This document is protected by copyright, and contains information
 * proprietary to Marmalade.
 *
 * This file consists of source code released by Marmalade under
 * the terms of the accompanying End User License Agreement (EULA).
 * Please do not use this program/source code before you have read the
 * EULA and have agreed to be bound by its terms.
 */

#include "GameManager.h"
#include "SceneImporter.h"
#include "Slider.h"
#include "Label.h"
#include "Sprite9.h"
#include "SliderEvents.h"

// The 2D engine lives in the Iw2DEngine namespace
M2DKIT_USE_NAMESPACE
M2DKITCORE_USE_NAMESPACE
M2DKITENGINE_USE_NAMESPACE

// The main game manager
static CGameManager* g_Game;
static CSceneContainer* g_SceneContainer;
static int g_SceneId = -1;

CSprite9* g_Sprite9;
CSlider* g_WidthSlider;
CSlider* g_HeightSlider;

const char* kSprite9Path = "Scene.Sprite9";
const float kSizeMinValue = 60.0f;
const float kSizeMaxValue = 400.0f;
const float kSizeDefaultValue = kSizeMinValue;

void SliderValueChangedCallback(CEventArgs* args)
{
    CSliderValueChangedArgs* _args = (CSliderValueChangedArgs*)args;
    CSlider* slider = (CSlider*)_args->m_Source;

    if (slider == g_WidthSlider)
    {
        g_Sprite9->SetSize(_args->m_NewValue, g_Sprite9->GetSize().y);
    }
    else if (slider == g_HeightSlider)
    {
        g_Sprite9->SetSize(g_Sprite9->GetSize().x, _args->m_NewValue);
    }
}

CSlider* AddSlider(const char* sliderPath, float sliderMinValue, float sliderMaxValue, float sliderValue, const char* labelPath)
{
    CSlider* slider = g_SceneContainer->GetNode<CSlider>(g_SceneId, sliderPath).get();
    IwAssertMsg(2DENGINE, slider != 0, ("%s not found", sliderPath));
    slider->SetMinValue(sliderMinValue);
    slider->SetMaxValue(sliderMaxValue);
    slider->SetValue(sliderValue);
    slider->SubscribeEvent(SLIDER_EVENT_VALUE_CHANGED, SliderValueChangedCallback);

    return slider;
}

int main()
{
    // Create and initialise the game manager
    g_Game = new CGameManager();
    g_Game->Init();

    // Import the scene and its associated resources
    g_SceneContainer = g_Game->GetSceneContainer();
    const int zIndex = 0;
    g_SceneContainer->LoadSceneFromDisk("Scene.json", "Scene.resources", zIndex, &g_SceneId);
    CScene* scene = g_SceneContainer->GetScene(g_SceneId);

    //Create a scope to make the shared pointers go out of scope before we're unloading the scene.
    {
        g_WidthSlider = AddSlider("Scene.WidthSlider", kSizeMinValue, kSizeMaxValue, kSizeDefaultValue, "Scene.Labels.Width");
        g_HeightSlider = AddSlider("Scene.HeightSlider", kSizeMinValue, kSizeMaxValue, kSizeDefaultValue, "Scene.Labels.Height");

        g_Sprite9 = g_SceneContainer->GetNode<CSprite9>(g_SceneId, kSprite9Path).get();
        IwAssertMsg(2DENGINE, g_Sprite9 != 0, ("%s not found", kSprite9Path));

        // Run the game
        g_Game->Run();
    }

    g_SceneContainer->UnloadScene(g_SceneId);
    // Destroy the game manager
    delete g_Game;

    return 0;
}
