/*
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This document is protected by copyright, and contains information
 * proprietary to Marmalade.
 *
 * This file consists of source code released by Marmalade under
 * the terms of the accompanying End User License Agreement (EULA).
 * Please do not use this program/source code before you have read the
 * EULA and have agreed to be bound by its terms.
 */

#include "GameManager.h"
#include "SceneImporter.h"
#include "Checkbox.h"
#include "Button.h"
#include "ProgressBar.h"
#include "Grid.h"
#include "ScrollPanel.h"
#include "Slider.h"

// The 2D engine lives in the Iw2DEngine namespace
M2DKIT_USE_NAMESPACE
M2DKITCORE_USE_NAMESPACE
M2DKITENGINE_USE_NAMESPACE

// The main game manager
CGameManager* g_Game;

// The scenes unique Id
static int s_SceneId = -1;

const char* kSliderPath = "Scene.slider1";
const char* kTestGridPath = "Scene.TestGrid";
const char* kScenePath = "Scene";
const char* kPanelPath = "Scene.panel1";
const char* kPanelCopyName = "panel1_copy";
const char* kPanelContentPath = "Scene.panel1.content";
const char* kCheckBoxPath = "Scene.Checkbox1";
const char* kButtonPath = "Scene.Button1";

void ButtonPressedCallback(CEventArgs* args)
{
    CButtonEventPressedArgs* _args = (CButtonEventPressedArgs*)args;
    CButton* button = (CButton*)_args->m_Source;
    s3eDebugTracePrintf("Button pressed %s\n", button->GetName().GetString().c_str());
}
void ButtonReleasedCallback(CEventArgs* args)
{
    CButtonEventReleasedArgs* _args = (CButtonEventReleasedArgs*)args;
    CButton* button = (CButton*)_args->m_Source;
    s3eDebugTracePrintf("Button released %s\n", button->GetName().GetString().c_str());
}

void CheckboxChangedCallback(CEventArgs* args)
{
    CCheckboxEventCheckChangedArgs* _args = (CCheckboxEventCheckChangedArgs*)args;
    CCheckbox* checkbox = (CCheckbox*)_args->m_Source;

    if (_args->m_Checked)
    {
        s3eDebugTracePrintf("Checkbox checked %s\n", checkbox->GetName().GetString().c_str());
    }
    else
    {
        s3eDebugTracePrintf("Checkbox unchecked %s\n", checkbox->GetName().GetString().c_str());
    }
}

void CreateGrid()
{
    CSceneContainer* sc = g_Game->GetSceneContainer();
    CScene* scene = sc->GetScene(s_SceneId);

    // Set upgrid paramaters
    CGridCreationParams params;
    params.m_Name = "TestGrid";
    params.m_Id = "TestGrid";
    params.m_Position = CIwFVec2(350.0f, 250.0f);
    params.m_Dimensions = CIwFVec2(300.0f, 200.0f);
    params.m_Visible = true;
    params.m_SpriteFrame = scene->FindSpriteFrameFromName("/assets/panel.png");
    params.m_ZIndex = 1000;

    // Create the grid object and add it to the scene
    shared_ptr<CGrid> grid = sc->CreateNode<CGrid>(params, s_SceneId, "Scene");

    // Create a sprite and add it to the grid
    CSpriteCreationParams firstElementParams;
    firstElementParams.m_Name = "FirstSprite";
    firstElementParams.m_Id = "FirstSprite";
    firstElementParams.m_Dimensions = CIwFVec2(150.0f, 100.0f);
    firstElementParams.m_Visible = true;
    firstElementParams.m_SpriteFrame = scene->FindSpriteFrameFromName("/assets/cb_un.png");
    firstElementParams.m_Margin.m_Left = 10.0f;
    firstElementParams.m_Margin.m_Right = 10.0f;
    firstElementParams.m_Margin.m_Top = 10.0f;
    firstElementParams.m_Margin.m_Bottom = 10.0f;
    shared_ptr<CSprite> firstElement = sc->CreateNode<CSprite>(firstElementParams, s_SceneId, kTestGridPath);

    // Create a second sprite and add it to the grid
    CSpriteCreationParams secondElementParams;
    secondElementParams.m_Name = "SecondSprite";
    secondElementParams.m_Id = "SecondSprite";
    secondElementParams.m_Dimensions = CIwFVec2(150.0f, 100.0f);
    secondElementParams.m_Visible = true;
    secondElementParams.m_SpriteFrame = scene->FindSpriteFrameFromName("/assets/cb_un_1.png");
    secondElementParams.m_Margin.m_Left = 10.0f;
    secondElementParams.m_Margin.m_Right = 10.0f;
    secondElementParams.m_Margin.m_Top = 10.0f;
    secondElementParams.m_Margin.m_Bottom = 10.0f;
    shared_ptr<CSprite> secondElement = sc->CreateNode<CSprite>(secondElementParams, s_SceneId, kTestGridPath);

    // Scale children to fit to grid size
    grid->SetScaleToFit(true);

    // Add two columsnt to the grid
    grid->AddColumnDefinition(CColumnDefinition(200.0f));
    grid->AddColumnDefinition(CColumnDefinition(100.0f));

    // Add two rows to the grid
    grid->AddRowDefinition(CRowDefinition(100.0f));
    grid->AddRowDefinition(CRowDefinition(100.0f));

    // Set the grid row and column of the first sprite
    grid->SetRow(firstElement, 0);
    grid->SetColumn(firstElement, 1);

    // Set the grid row and column of the second sprite
    grid->SetRow(secondElement, 0);
    grid->SetColumn(secondElement, 0);
}

void CreatePanel()
{
    CSceneContainer* sc = g_Game->GetSceneContainer();
    CScene* scene = sc->GetScene(s_SceneId);

    // Create a scroll panel
    CScrollPanelCreationParams params;
    params.m_Name = "panel1";
    params.m_Id = "panel1";
    params.m_Position = CIwFVec2(700, 200);
    params.m_SpriteFrame = scene->FindSpriteFrameFromName("/assets/panel.png");
    params.m_Dimensions = CIwFVec2(200, 200);
    params.m_SizeAnchorType = SizeAnchoring::WidthHeight;
    params.m_ClipBorder = UI::CRect(10, 10, 10, 10);
    params.m_HorizontalScroll = true;
    params.m_HorizontalScrollMargin = UI::CMargin(10, 10, 2, 2);
    params.m_HorizontalScrollWidth = 4;
    params.m_VerticalScroll = true;
    params.m_VerticalScrollMargin = UI::CMargin(2, 2, 10, 10);
    params.m_VerticalScrollWidth = 4;

    shared_ptr<CScrollPanel> p = sc->CreateNode<CScrollPanel>(params, s_SceneId, kScenePath);

    p->SetCachedParentSize(CIwFVec2(1024, 768));

    CNodeCreationParams c_params;
    c_params.m_Name = "content";
    c_params.m_Id = "content";
    c_params.m_Position = CIwFVec2(0.0f, 0.0f);
    c_params.m_Dimensions = CIwFVec2(400, 400);
    c_params.m_ConsumeTouch = Node::ConsumeAndPassOn;
    sc->CreateNode<CNode>(c_params, s_SceneId, kPanelPath);

    // Create panel children
    for (int t = 0; t < 3; t++)
    {
        CSpriteCreationParams s_params;
        s_params.m_Name = "child";
        s_params.m_Position = CIwFVec2((t + 1) * 80.0f, (t + 1) * 80.0f);
        s_params.m_Dimensions = CIwFVec2(60, 60);
        s_params.m_SpriteFrame = scene->FindSpriteFrameFromName("/assets/cb_un.png");
        s_params.m_ConsumeTouch = Node::ConsumeAndPassOn;
        sc->CreateNode<CSprite>(s_params, s_SceneId, kPanelContentPath);
    }
}


void CreateSlider()
{
    CSceneContainer* sc = g_Game->GetSceneContainer();
    CScene* scene = sc->GetScene(s_SceneId);

    // Create a slider
    CSliderCreationParams params;
    params.m_Name = "slider1";
    params.m_Id = "slider1";
    params.m_Position = CIwFVec2(700, 500);
    params.m_SliderBackground = scene->FindSpriteFrameFromName("/assets/slider.png");
    params.m_SliderProgressBar = scene->FindSpriteFrameFromName("/assets/slider_progress.png");
    params.m_SliderNubNormal = scene->FindSpriteFrameFromName("/assets/slider_nub.png");
    params.m_SliderNubPressed = scene->FindSpriteFrameFromName("/assets/slider_nub_pressed.png");
    params.m_SliderNubDisabled = scene->FindSpriteFrameFromName("/assets/slider_nub_gray.png");
    params.m_Dimensions = CIwFVec2(243, 40);
    params.m_Rotation = 45;
    shared_ptr<CSlider> slider = sc->CreateNode<CSlider>(params, s_SceneId, kScenePath);
}

void CopyPanel()
{
    CScene* scene = g_Game->GetSceneContainer()->GetScene(s_SceneId);
    TNodePtr panelCopy = scene->FindNodeByPath(kPanelPath)->Copy();//->GetNode<CNode>(s_SceneId, kPanelPath)->Copy();
    panelCopy->SetName(kPanelCopyName);
    panelCopy->SetPosition(CIwFVec2(50, 50));
    scene->GetRootNode()->AddChild(panelCopy);
}

int main()
{
    // Create and initialise the game manager
    g_Game = new CGameManager();
    g_Game->Init();

    // Import the scene and its associated resources
    CSceneContainer* sc = g_Game->GetSceneContainer();
    const int zIndex = 0;
    sc->LoadSceneFromDisk("Scene.json", "Scene.resources", zIndex, &s_SceneId);

    //Create a scope to make the shared pointers go out of scope before we're unloading the scene.
    {
        // Attach pressed and released event callbacks to button
        shared_ptr<CButton> btn = sc->GetNode<CButton>(s_SceneId, kButtonPath);
        IwAssertMsg(2DENGINE, btn != 0, ("%s not found", kButtonPath));
        btn->SubscribeEvent(BUTTON_EVENT_PRESSED, ButtonPressedCallback);
        btn->SubscribeEvent(BUTTON_EVENT_RELEASED, ButtonReleasedCallback);
        
        // Attach check changed event callback to checkbox
        shared_ptr<CCheckbox> cb = sc->GetNode<CCheckbox>(s_SceneId, kCheckBoxPath);
        IwAssertMsg(2DENGINE, cb != 0, ("%s not found", kCheckBoxPath));
        cb->SubscribeEvent(CHECKBOX_EVENT_CHECK_CHANGED, CheckboxChangedCallback);

        //// Create a grid
        CreateGrid();

        //// Create a scroll panel
        CreatePanel();

        //// Copy scroll panel
        CopyPanel();

        //// Create a slider
        CreateSlider();

        // Run the game
        g_Game->Run();

    }

    sc->UnloadScene(s_SceneId);
    // Destroy the game manager
    delete g_Game;

    return 0;
}
