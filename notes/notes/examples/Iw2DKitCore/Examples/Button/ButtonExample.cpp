/*
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This document is protected by copyright, and contains information
 * proprietary to Marmalade.
 *
 * This file consists of source code released by Marmalade under
 * the terms of the accompanying End User License Agreement (EULA).
 * Please do not use this program/source code before you have read the
 * EULA and have agreed to be bound by its terms.
 */

#include "GameManager.h"
#include "SceneImporter.h"
#include "Button.h"
#include "Label.h"

// The 2D engine lives in the Iw2DEngine namespace
M2DKIT_USE_NAMESPACE
M2DKITCORE_USE_NAMESPACE
M2DKITENGINE_USE_NAMESPACE

// The main game manager
static CGameManager* g_Game;
static shared_ptr<CLabel> g_Label;

// The scenes unique Id
static int s_SceneId = -1;

const char* kButton1Path = "Scene.Button 1";
const char* kButton2Path = "Scene.Button 2";
const char* kLabelPath = "Scene.Label";

void ButtonPressedCallback(CEventArgs* args)
{
    CButtonEventPressedArgs* _args = (CButtonEventPressedArgs*)args;
    CButton* button = (CButton*)_args->m_Source;
    const char* buttonName = button->GetName().GetString().c_str();

    char buf[256];
    sprintf(buf, "%s pressed", buttonName);
    g_Label->SetText(buf);
}
void ButtonReleasedCallback(CEventArgs* args)
{
    CButtonEventReleasedArgs* _args = (CButtonEventReleasedArgs*)args;
    CButton* button = (CButton*)_args->m_Source;
    const char* buttonName = button->GetName().GetString().c_str();

    char buf[256];
    sprintf(buf, "%s released", buttonName);
    g_Label->SetText(buf);
}

int main()
{
    // Create and initialise the game manager
    g_Game = new CGameManager();
    g_Game->Init();

    // Import the scene and its associated resources
    CSceneContainer* sc = g_Game->GetSceneContainer();
    const int zIndex = 0;
    sc->LoadSceneFromDisk("Scene.json", "Scene.resources", zIndex, &s_SceneId);

    //Create a scope to make the shared pointers go out of scope before we're unloading the scene.
    {
        // Attach pressed and released event callbacks to button
        shared_ptr<CButton> btn = sc->GetNode<CButton>(s_SceneId, kButton1Path);
        IwAssertMsg(2DENGINE, btn != 0, ("%s not found", kButton1Path));
        btn->SubscribeEvent(BUTTON_EVENT_PRESSED, ButtonPressedCallback);
        btn->SubscribeEvent(BUTTON_EVENT_RELEASED, ButtonReleasedCallback);

        btn = sc->GetNode<CButton>(s_SceneId, kButton2Path);
        IwAssertMsg(2DENGINE, btn != 0, ("%s not found", kButton2Path));
        btn->SubscribeEvent(BUTTON_EVENT_PRESSED, ButtonPressedCallback);
        btn->SubscribeEvent(BUTTON_EVENT_RELEASED, ButtonReleasedCallback);

        g_Label = sc->GetNode<CLabel>(s_SceneId, kLabelPath);
        IwAssertMsg(2DENGINE, g_Label != 0, ("%s not found", kLabelPath));

        // Run the game
        g_Game->Run();
    }

    g_Label.reset();

    sc->UnloadScene(s_SceneId);
    // Destroy the game manager
    delete g_Game;

    return 0;
}
