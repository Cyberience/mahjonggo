#category: 2D Kit

This example shows how to maintain Slider node.
There are shown following features:
1. Enable
    It's shown how to enable/disable slider.
    Check/uncheck checkbox to enable/disable slider.
2. Callbacks
    It's shown how to subscribe slider change value callback and how to react it.
