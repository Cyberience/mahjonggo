/*
 * (C) 2001-2012 Marmalade. All Rights Reserved.
 *
 * This document is protected by copyright, and contains information
 * proprietary to Marmalade.
 *
 * This file consists of source code released by Marmalade under
 * the terms of the accompanying End User License Agreement (EULA).
 * Please do not use this program/source code before you have read the
 * EULA and have agreed to be bound by its terms.
 */

#include "GameManager.h"
#include "SceneImporter.h"
#include "Checkbox.h"
#include "Label.h"
#include "Slider.h"
#include "SliderEvents.h"

// The 2D engine lives in the Iw2DEngine namespace
M2DKIT_USE_NAMESPACE
M2DKITCORE_USE_NAMESPACE
M2DKITENGINE_USE_NAMESPACE

// The main game manager
static CGameManager* g_Game;
static CSceneContainer* g_SceneContainer;
static int g_SceneId = -1;

static shared_ptr<CSlider> g_Slider;
static shared_ptr<CLabel> g_CheckboxLabel;
static shared_ptr<CLabel> g_SliderLabel;

void SliderValueChangedCallback(CEventArgs* args)
{
    CSliderValueChangedArgs* _args = (CSliderValueChangedArgs*)args;

    char buf[256]; sprintf(buf, "Slider value: %.2f", _args->m_NewValue);
    g_SliderLabel->SetText(buf);
}

void CheckboxChangedCallback(CEventArgs* args)
{
    CCheckboxEventCheckChangedArgs* _args = (CCheckboxEventCheckChangedArgs*)args;

    if (_args->m_Checked)
    {
        g_Slider->SetEnabled(true);
        g_CheckboxLabel->SetText("Disable slider");
    }
    else
    {
        g_Slider->SetEnabled(false);
        g_CheckboxLabel->SetText("Enable slider");
    }
}

const char* kCheckboxPath = "Scene.Checkbox";
const char* kSliderPath = "Scene.Slider";
const char* kCheckboxLabelPath = "Scene.Checkbox.Label";
const char* kSliderLabelPath = "Scene.Slider.Label";

int main()
{
    // Create and initialise the game manager
    g_Game = new CGameManager();
    g_Game->Init();

    // Import the scene and its associated resources
    g_SceneContainer = g_Game->GetSceneContainer();
    const int zIndex = 0;
    g_SceneContainer->LoadSceneFromDisk("Scene.json", "Scene.resources", zIndex, &g_SceneId);

    //Create a scope to make the shared pointers go out of scope before we're unloading the scene.
    {
        shared_ptr<CCheckbox> cb = g_SceneContainer->GetNode<CCheckbox>(g_SceneId, kCheckboxPath);
        IwAssertMsg(2DENGINE, cb != 0, ("%s not found", kCheckboxPath));
        cb->SubscribeEvent(CHECKBOX_EVENT_CHECK_CHANGED, CheckboxChangedCallback);

        g_Slider = g_SceneContainer->GetNode<CSlider>(g_SceneId, kSliderPath);
        IwAssertMsg(2DENGINE, g_Slider != 0, ("%s not found", kSliderPath));
        g_Slider->SubscribeEvent(SLIDER_EVENT_VALUE_CHANGED, SliderValueChangedCallback);

        g_CheckboxLabel = g_SceneContainer->GetNode<CLabel>(g_SceneId, kCheckboxLabelPath);
        IwAssertMsg(2DENGINE, g_CheckboxLabel != 0, ("%s not found", kCheckboxLabelPath));

        g_SliderLabel = g_SceneContainer->GetNode<CLabel>(g_SceneId, kSliderLabelPath);
        IwAssertMsg(2DENGINE, g_SliderLabel != 0, ("%s not found", kSliderLabelPath));

        // Run the game
        g_Game->Run();
    }

    g_Slider.reset();
    g_CheckboxLabel.reset();
    g_SliderLabel.reset();

    g_SceneContainer->UnloadScene(g_SceneId);
    // Destroy the game manager
    delete g_Game;

    return 0;
}
