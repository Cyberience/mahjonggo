--require("mobdebug").start()  --this line is to enable the ZeroBrain Debugger needs to b 
-- This is the Main entry point to the application

--Connect to the Web site for REST comms.
commscheck()

-- Sets debug to on if the code is not running on a mobile device.
debug = (application:getDeviceInfo() == "Windows" or application:getDeviceInfo() == "Mac OS")  --This will set debug if not run on a mobile device

scene=sceneManager.new()  		--Global page control 
scene:loadPage()		  		--Executes the Loadpage method in the scene object
profile = playerprofile.new()	--now the scenese are loded and app control is done, set up a player profile to manage the states
