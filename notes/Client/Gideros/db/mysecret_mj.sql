-- phpMyAdmin SQL Dump
-- version 3.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 22, 2012 at 09:51 AM
-- Server version: 5.0.92
-- PHP Version: 5.2.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mysecret_mj`
--

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE IF NOT EXISTS `chat` (
  `cid` int(11) NOT NULL auto_increment COMMENT 'counter',
  `from` int(11) NOT NULL COMMENT 'user uid',
  `to` int(11) NOT NULL COMMENT 'user uid',
  `type` varchar(10) collate utf8_bin NOT NULL COMMENT 'room or table',
  `type_id` int(11) NOT NULL COMMENT 'if type is a room, then its room id, if type is a table it is table id',
  `system` tinyint(4) NOT NULL default '0',
  `time` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `message` varchar(200) collate utf8_bin NOT NULL COMMENT 'The Message',
  PRIMARY KEY  (`cid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Chat table, keep track of last chat id in users tables' AUTO_INCREMENT=410 ;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`cid`, `from`, `to`, `type`, `type_id`, `system`, `time`, `message`) VALUES
(395, 42, 0, 'room', 2, 0, '2012-01-03 11:28:39', 'yep and this is in chrome on a mac'),
(394, 41, 0, 'room', 2, 0, '2012-01-03 11:28:06', 'ahaa , so chat is now room specikfic'),
(393, 42, 0, 'room', 2, 0, '2012-01-03 11:25:33', 'ok'),
(392, 41, 0, 'room', 2, 0, '2012-01-03 11:25:15', 'test room 2'),
(391, 41, 0, 'room', 1, 0, '2012-01-03 11:23:00', 'test room'),
(390, 41, 0, 'hall', 0, 0, '2012-01-03 11:22:23', 'test hall'),
(396, 41, 0, 'room', 1, 0, '2012-01-03 11:28:44', 'test room 1'),
(397, 42, 0, 'hall', 0, 0, '2012-01-03 11:29:26', 'this is the hall'),
(398, 42, 0, 'hall', 0, 0, '2012-01-03 12:10:25', 'safari version in PC ok'),
(399, 42, 0, 'room', 2, 0, '2012-01-03 12:47:56', 'room2 from mac safari'),
(400, 41, 0, 'hall', 0, 0, '2012-01-03 13:41:23', 'wonder why doors dont open on safari windows?'),
(401, 41, 0, 'hall', 0, 0, '2012-01-05 14:07:35', 'hmm'),
(402, 41, 0, 'hall', 0, 0, '2012-01-05 14:07:39', 'hall works'),
(403, 41, 0, 'room', 1, 0, '2012-01-05 14:07:57', 'room works'),
(404, 42, 0, 'room', 1, 0, '2012-01-05 14:12:24', '3 tables'),
(405, 41, 0, 'room', 1, 0, '2012-01-05 14:22:09', 'yu[p'),
(406, 41, 0, 'room', 1, 0, '2012-01-05 14:22:12', 'yup'),
(407, 42, 0, 'hall', 0, 0, '2012-01-17 01:47:03', 'hi'),
(408, 42, 0, 'room', 2, 0, '2012-01-27 07:18:17', 'hhhh'),
(409, 41, 0, 'room', 4, 0, '2012-01-30 04:52:02', 'animate a bit slow, should move faster no?');

-- --------------------------------------------------------

--
-- Table structure for table `notify`
--

CREATE TABLE IF NOT EXISTS `notify` (
  `email` varchar(60) NOT NULL,
  `request` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `notified` timestamp NOT NULL default '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

CREATE TABLE IF NOT EXISTS `purchases` (
  `pid` int(11) NOT NULL auto_increment COMMENT 'purchase id',
  `uid` int(11) NOT NULL COMMENT 'User id for the purchase',
  `type` varchar(20) collate utf8_bin NOT NULL COMMENT 'apple pp free etc',
  `time` timestamp NOT NULL default '0000-00-00 00:00:00' on update CURRENT_TIMESTAMP COMMENT 'time stamp',
  `msg` varchar(50) collate utf8_bin NOT NULL COMMENT 'the message',
  PRIMARY KEY  (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE IF NOT EXISTS `rooms` (
  `rid` int(11) NOT NULL auto_increment COMMENT 'auto id',
  `game` varchar(40) collate utf8_bin NOT NULL COMMENT 'game, such as hkmj, twmj,cluedo what ever.',
  `URL` varchar(60) collate utf8_bin NOT NULL COMMENT 'URL of the room entry',
  `rules` varchar(200) collate utf8_bin NOT NULL COMMENT 'some rules as paramters for the game',
  `tables` int(11) NOT NULL,
  `status` varchar(60) collate utf8_bin NOT NULL,
  `level` int(11) NOT NULL default '0',
  `limits` varchar(10) collate utf8_bin NOT NULL COMMENT 'Min Max bet limits',
  PRIMARY KEY  (`rid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=7 ;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`rid`, `game`, `URL`, `rules`, `tables`, `status`, `level`, `limits`) VALUES
(1, 'Mahjong Poker', 'http://mj.live.hk.com/?p=room&pid=1', 'Hong Kong', 10, 'Open', 0, '$1/$100'),
(2, 'MahJong Poker', 'http://mj.live.hk.com/?p=room&pid=2', 'Guanzhao', 30, 'Open', 2, ''),
(3, 'MahJong Poker', 'http://mj.live.hk.com/?p=room&pid=3', 'Guanzhao', 30, 'Open', 3, '$100/$500'),
(4, 'MahJong Poker', 'http://mj.live.hk.com/?p=room&pid=4', 'Guanzhao', 30, 'Closed', 4, '$200/$1000'),
(5, 'MahJong Poker', 'http://mj.live.hk.com/?p=room&pid=5', 'Guanzhao', 30, 'Open', 5, '$500/$10,0'),
(6, 'MahJong Poker', 'http://mj.live.hk.com/?p=room&pid=6', 'Guanzhao', 30, 'Open', 6, '$10K/$100K');

-- --------------------------------------------------------

--
-- Table structure for table `system_alerts`
--

CREATE TABLE IF NOT EXISTS `system_alerts` (
  `alert_ID` int(11) NOT NULL auto_increment,
  `text` varchar(200) NOT NULL,
  `expiry` datetime NOT NULL,
  PRIMARY KEY  (`alert_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `system_alerts`
--

INSERT INTO `system_alerts` (`alert_ID`, `text`, `expiry`) VALUES
(3, 'some text', '2011-10-23 04:03:12'),
(4, 'some text1', '2011-10-23 04:06:28'),
(5, 'some text 2', '2011-10-23 04:07:46'),
(6, 'ALERT !!! exploding in 5 mns', '2011-10-23 04:26:08'),
(7, 'ALERT !!! exploding', '2011-10-23 05:02:12'),
(8, 'ALERT !!! website exploding!', '2011-10-23 07:02:24'),
(9, 'ALERT !!! website exploding!', '2011-10-23 19:28:32'),
(10, 'hey', '2011-10-23 20:19:05');

-- --------------------------------------------------------

--
-- Table structure for table `tables`
--

CREATE TABLE IF NOT EXISTS `tables` (
  `tid` int(11) NOT NULL auto_increment COMMENT 'table id',
  `Pack` varchar(500) collate utf8_bin NOT NULL COMMENT 'Pack of Cards that sre left after the deal [B:443432344,M:444444444,C:214443349,W:4444,F:1111] eg W:nsew, F:flowers.',
  `pot` int(11) NOT NULL COMMENT 'Total Money in the pot',
  `turn` int(11) NOT NULL COMMENT 'whos turn is it. one of the ID from the players',
  `countdown` int(11) NOT NULL COMMENT 'turn count down start, set the time and then all clients can count down. 0 is forfit. default stands. player time out is in user table.',
  `seats` int(11) NOT NULL COMMENT 'Number of Seats on this Table, 4, 6, or 8',
  `players` varchar(32) collate utf8_bin NOT NULL COMMENT 'players id in position. eg: 01,,,12.  seats is max, if more here, trunk, explode and implode',
  PRIMARY KEY  (`tid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table for game control' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `temp_users`
--

CREATE TABLE IF NOT EXISTS `temp_users` (
  `vcode` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `ip_address` varchar(20) NOT NULL,
  `date` timestamp NOT NULL default CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `uid` int(11) NOT NULL auto_increment COMMENT 'user auto id',
  `id` varchar(60) NOT NULL,
  `password` varchar(40) NOT NULL COMMENT 'What else but the fukin password? but remember no password for the ipad.',
  `platform` varchar(50) NOT NULL default '0                                     ',
  `name` varchar(40) NOT NULL default '0',
  `sys` varchar(40) NOT NULL default '0',
  `local` varchar(20) NOT NULL default '0',
  `ip` varchar(20) NOT NULL default '0',
  `last_update` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `wallet` int(11) NOT NULL default '0',
  `mj_played` int(11) NOT NULL default '0',
  `mj_won` int(11) NOT NULL default '0',
  `mj_draw` int(11) NOT NULL default '0',
  `mj_rank` text,
  `mj_level` int(11) NOT NULL default '0',
  `last_cid` int(11) NOT NULL default '0' COMMENT 'last Chat ID',
  `last_pid` int(11) NOT NULL default '0' COMMENT 'Last Prchase ID',
  `authkey` varchar(80) NOT NULL default '0',
  `status` tinyint(4) NOT NULL default '1',
  `session` varchar(100) NOT NULL default '0',
  PRIMARY KEY  (`uid`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=64 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`uid`, `id`, `password`, `platform`, `name`, `sys`, `local`, `ip`, `last_update`, `wallet`, `mj_played`, `mj_won`, `mj_draw`, `mj_rank`, `mj_level`, `last_cid`, `last_pid`, `authkey`, `status`, `session`) VALUES
(42, 'tipton@mysecretroom.com', '7c6a61c68ef8b9b6b061b28c348bc1ed7921cb53', '0                                     ', 'MusicMan', '0', '0', '219.78.203.93', '2012-02-04 10:40:53', 746, 0, 0, 0, NULL, 0, 0, 0, '0', 1, ''),
(41, 'david@badbod.com', '5e319a168b66ab8224cc19b1d1cd0d9d2ee7715e', '0                                     ', 'badbod', '0', '0', '110.171.24.88', '2012-01-31 11:46:41', 60, 0, 0, 0, NULL, 0, 0, 0, '0', 1, ''),
(44, 'badbod@badbod.com', '8af970f725db46cedbde0f3f800921ca54bae746', '0                                     ', 'MadDog', '0', '0', '110.171.24.88', '2011-10-11 14:15:15', 0, 0, 0, 0, NULL, 0, 0, 0, '0', 1, ''),
(45, 'meany@badbod.com', '5e319a168b66ab8224cc19b1d1cd0d9d2ee7715e', '0                                     ', 'YoMamma', '0', '0', '110.171.24.88', '2011-10-11 14:15:34', 0, 0, 0, 0, NULL, 0, 0, 0, '0', 1, ''),
(46, 'mangler@badbod.com', '5e319a168b66ab8224cc19b1d1cd0d9d2ee7715e', '0                                     ', 'Mangler', '0', '0', '110.171.24.88', '2011-10-11 14:14:58', 0, 0, 0, 0, NULL, 0, 0, 0, '0', 1, ''),
(49, 'megaman@badbod.com', '5e319a168b66ab8224cc19b1d1cd0d9d2ee7715e', '0                                     ', '0', '0', '0', '110.171.24.88', '2011-10-11 14:36:25', 0, 0, 0, 0, NULL, 0, 0, 0, '0', 1, ''),
(62, 'superic@n-gallery.com.hk', '511d2f6aee3201c79d9afe43926ea6466ba8e404', '0                                     ', '0', '0', '0', '119.237.241.184', '2012-01-06 09:56:55', 0, 0, 0, 0, NULL, 0, 0, 0, '0', 1, '0'),
(50, 'mjtest@badbod.com', '5e319a168b66ab8224cc19b1d1cd0d9d2ee7715e', '0                                     ', 'Guest', '0', '0', '93.89.92.116', '2011-10-30 05:03:36', 0, 0, 0, 0, NULL, 0, 0, 0, '0', 1, '0'),
(51, 'bburgess@keysystemsintl.com', '22e2d4d538227834257b3213bad22a591b46337b', '0                                     ', '0', '0', '0', '218.103.220.108', '2012-01-30 12:51:52', 749, 51, 49, 2, NULL, 0, 0, 0, '0', 1, '0'),
(63, 'bburgess66@hotmail.com', '', '0                                     ', '0', '0', '0', '0', '2012-02-01 04:19:17', 0, 0, 0, 0, '9999/9999', 0, 0, 0, '0', 1, '0');
