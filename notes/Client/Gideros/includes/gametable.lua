gametable = Core.class(Sprite)

function gametable:init()
	self:background() --Adds the background and animates it.
	self:addChild(playingtable.new()) --adds a menu, yet to be done, possibly top right corner cog, with sprite overlay
end

function gametable:background()
	local background1 = imageload.new("images/HongKong.jpg")
	local boatR = imageload.new("images/layer2-boat.png")
	self:addChild(background1)

	mcBG=MovieClip.new{
						{1,2000, background1,{x={1,-(background1:getWidth()-application:getLogicalHeight()),"linear"}}},
						{2000,4000, background1,{x={-(background1:getWidth()-application:getLogicalHeight()),1,"linear"}}}
	}
	mcBG:setGotoAction(4000, 1)

	self:addChild(boatR)
	mcBoat=MovieClip.new{
						{1,2, 		boatR,{y=background1:getHeight()*.85},{x=-50}},
						{1,1000, 	boatR,{x={-50,(application:getLogicalHeight()+50),"linear"}}},
						{1000,1001, boatR,{scaleX={1,-1}}},
						{1001,2000, boatR,{x={(application:getLogicalHeight()+100),-50,"linear"}}},
						{2000,2001, boatR,{scaleX={-1,1}}},
						}
	mcBoat:setGotoAction(2001, 1) --sets the animation to repeat.
end

--=====Table Draw=======---
playingtable = Core.class(Sprite)

function playingtable:init()  --Place the nice clean table on the screen
	local imgtable = imageload.new("images/BigTable-896.png")
	self:addChild(imgtable)
	--put a movie clip here to zoom into the table to normal size
	self:setPosition(55,155)
end