door = Core.class(Sprite)

function door:init(door,frame,room,anim,x,y,parent)
	self.parent = parent
	self:setPosition(x,y)
	self.door = door
	self.room = room
	self.activedoor=0
	self.doorstate = "closed"
	self.frameimg = imageload.new(frame)
	self.roomimg = imageload.new(room)
	self.dooranim = self:anim(anim)
	self.swingopen = self:open(self.dooranim)
	self.swingclose = self:close(self.dooranim)
	self.swingopen:stop()
	self.swingclose:stop()
	
	self.roomimg:setPosition(45,58)
	self.roomimg:setScale(.90,.96)
	self.swingopen:setPosition(44,57)
	self.swingopen:setScale(1,1.01)

	self.swingclose:setPosition(44,57)
	self.swingclose:setScale(1,1.01)

	
	self.roomimg:setVisible(false) --change this when a door opens
	self.swingopen:setVisible(false)
	self.swingclose:setVisible(false)

	self:addChild(self.frameimg)
	self:addChild(self.roomimg)
	self:addChild(self.swingopen)
	self:addChild(self.swingclose)

	self.closeend = function()	
						self.parent.doorslamfx:play()
						self.roomimg:setVisible(false)
						self.swingclose:setVisible(false)
						self.activedoor = 0
						self.doorstate = "closed"
						self.swingclose:removeEventListener(Event.COMPLETE,self.closeend)
					end

	self:addEventListener(Event.TOUCHES_BEGIN, 	function(t,e) 
													if t:hitTestPoint(e.touch.x, e.touch.y) then
														if self.doorstate == "closed" then
															self.parent.dooropenfx:play()
															self.activedoor = self.door
															self.doorstate = "open"
															self.roomimg:setVisible(true)  --show the room
															self.swingopen:setVisible(true) --Show opening door image
															self.swingopen:gotoAndPlay(1) --Start the animation
															collectgarbage()  --need this to stop memory leak, for fuck sake :(
															print ("memory used 1:"..collectgarbage("count")) 

														end -- if
														e:stopPropagation()
													end
												end, self)
	self:addEventListener(Event.TOUCHES_END, function(t,e) 
													if t:hitTestPoint(e.touch.x, e.touch.y) and (self.activedoor == self.door) then
														if self.doorstate == "open" then
															self.parent.doorclosefx:play()
															self.doorstate = "closing"
															self.swingclose:setVisible(true)
															self.swingopen:setVisible(false)
															self.swingclose:addEventListener(Event.COMPLETE, self.closeend, self.swingclose)
															self.swingclose:gotoAndPlay(1)
															e:stopPropagation()
														end --if
														print("Touch End Door:"..self.door)
														gamestate.room = self.door
														scene:loadPage("room")
													end 
												end, self)
end

function door:anim(dooranim)
local texture = Texture.new(dooranim)
retval = {}
local pos,width,height=0,119,190
for loop = 1,9 do
	pos=loop-1
	retval[loop] =  Bitmap.new(TextureRegion.new(texture, (pos*width), 0, width, height))
end
	return retval
end

function door:open(doorarray)
	local retval = MovieClip.new{
		{1 , 	3 ,doorarray[1] },	
		{3 , 	5 ,doorarray[2]},	
		{5 , 	7, doorarray[3]},	
		{7 , 	9, doorarray[4]},
		{9, 	11,doorarray[5]},
		{11, 	13,doorarray[6]},
		{13, 	15,doorarray[7]},
		{15, 	17,doorarray[8]},
		{17, 	19,doorarray[9]}
	}
return retval
end

function door:close(doorarray)
	local retval = MovieClip.new{
		{1 , 	3 , doorarray[9] },	
		{3 , 	5 , doorarray[8]},	
		{5 , 	7 , doorarray[7]},	
		{7 , 	9 , doorarray[6]},
		{9, 	11, doorarray[5]},
		{11, 	13, doorarray[4]},
		{13, 	15, doorarray[3]},
		{15, 	17, doorarray[2]},
		{17, 	19, doorarray[1]}
	}
return retval
end

hall = Core.class(Sprite)

function hall:init()
	self:background() --Adds the background and animates it.
	self:menu() --adds the Menu to the left, and floats it in.
	self:doors() -- add the doors
end

function hall:doors()
	self.dooropenfx = Sound.new("audio/door_creak_open.wav")
	self.doorclosefx = Sound.new("audio/door_creak_close.wav")
	self.doorslamfx = Sound.new("audio/door_creak_closed.wav")

	self:addChild(door.new(1,"images/door1.png","images/room1.jpg","images/doortype1.png",300,40 ,self) )
	self:addChild(door.new(2,"images/door2.png","images/room2.jpg","images/doortype2.png",550,40 ,self) )
	self:addChild(door.new(3,"images/door2.png","images/room3.jpg","images/doortype2.png",800,40 ,self) )
	self:addChild(door.new(4,"images/door2.png","images/room4.jpg","images/doortype2.png",300,380,self) )
	self:addChild(door.new(5,"images/door1.png","images/room5.jpg","images/doortype1.png",550,380,self) )
	self:addChild(door.new(6,"images/door1.png","images/room6.jpg","images/doortype1.png",800,380,self) )
end



function hall:roomobject(roomtype,x,y)
	local rooms = {"images/room1.jpg","images/room2.jpg","images/room3.jpg","images/room4.jpg","images/room5.jpg","images/room6.jpg"}
	local retroom = Sprite.new()
	retroom:addChild(imageload.new(rooms[roomtype]))
	retroom:setPosition(x,y)
	retroom:setScale(.90,.96)
	return retroom
end


function hall:menu()
	local menu = Sprite.new()
	menu:setPosition(0,10)
	menu:addChild(imageload.new("images/panel_left.png"))
	--put the avatar here.
print(profile.detail["name"])
	--avatar frame
	local avframe = imageload.new("images/avatarframe2.png")
	avframe:setPosition(35,60)
	menu:addChild(avframe)

	--slide in the menu with mc
	self:addChild(menu)
	mcMenu=MovieClip.new{
						{1,30, menu,{x={-250,0,"outCubic"}}}
	}
end

function hall:background()
	local background1 = imageload.new("images/HongKong.jpg")
	local boatR = imageload.new("images/layer2-boat.png")
	self:addChild(background1)

	mcBG=MovieClip.new{
						{1,2000, background1,{x={1,-(background1:getWidth()-application:getLogicalHeight()),"linear"}}},
						{2000,4000, background1,{x={-(background1:getWidth()-application:getLogicalHeight()),1,"linear"}}}
	}
	mcBG:setGotoAction(4000, 1)

	self:addChild(boatR)
	mcBoat=MovieClip.new{
						{1,2, 		boatR,{y=background1:getHeight()*.85},{x=-50}},
						{1,1000, 	boatR,{x={-50,(application:getLogicalHeight()+50),"linear"}}},
						{1000,1001, boatR,{scaleX={1,-1}}},
						{1001,2000, boatR,{x={(application:getLogicalHeight()+100),-50,"linear"}}},
						{2000,2001, boatR,{scaleX={-1,1}}},
						}
	mcBoat:setGotoAction(2001, 1)
end
