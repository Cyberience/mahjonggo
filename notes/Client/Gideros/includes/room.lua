room = Core.class(Sprite)

--[[ToDo
1: Wallpaper back drop required.
2: add a carosell of tables with players on them.
3: Select the Table to go to the playing table
]]--


function room:init()
	self:background() --Adds the background and animates it.
	self:addChild(playingtable.new()) --adds a menu, yet to be done, possibly top right corner cog, with sprite overlay
end

function room:background()
	local background1 = imageload.new("images/HongKong.jpg")
	self:addChild(background1)

	mcBG=MovieClip.new{
						{1,2000, background1,{x={1,-(background1:getWidth()-application:getLogicalHeight()),"linear"}}},
						{2000,4000, background1,{x={-(background1:getWidth()-application:getLogicalHeight()),1,"linear"}}}
	}
	mcBG:setGotoAction(4000, 1)

end

--=====Tables Draw=======---
playingtable = Core.class(Sprite)

function playingtable:init()  --Place the nice clean table on the screen
	local imgtable = imageload.new("images/BigTable-896.png")
	self:addChild(imgtable)
	--put a movie clip here to zoom into the table to normal size
	self:setPosition(55,155)
end