--Manage the scene
sceneManager = Core.class()
function sceneManager:init()
	self.scene = nil
end

function sceneManager:loadPage(p_page)
	collectgarbage()
	print(collectgarbage("count"))
	
	if self.scene then self.scene:removeFromParent() end
	
	if p_page == "hall" then  --the main menu screen.
		self.scene = hall.new()
	elseif p_page== "room" then
		self.scene = room.new()
	elseif p_page== "gametable" then
		self.scene = gametable.new()
	else  --welcome screen
		self.scene = loading.new()
	end
	stage:addChild(self.scene)
end

-- Load images here,
imageload = Core.class(Sprite)

function imageload:init(p_image)
 self:addChild(Bitmap.new(Texture.new(p_image)))
 self:setScale(1,1)
end

function DecToBase(IN,BASE)
    local B,K,OUT,D=BASE or 10,"0123456789ABCDEFGHIJKLMNOPQRSTUVW",""
    while IN>0 do
        IN,D=math.floor(IN/B),math.mod(IN,B)+1
        OUT=string.sub(K,D,D)..OUT
    end
    return OUT
end

playerprofile = Core.class()

function playerprofile:init()
	self.detail = {}
	if not exists("|D|profile.tbl") then
		self.detail.email		="";
		self.detail.password 	="";
		self.detail.name		="Guest";
		self.detail.avatar		=0;
		self.detail.score		=0;
		self.detail.hiscore		=0;	
		self.detail.globalscore	=0;
		self.detail.gold		=0;
		self:save(self.detail)
	else 
		self.detail = self:load()
	end --if
print("Profile Name:"..self.detail.name)
end

function playerprofile:save()
	table.save(self.detail, "|D|profile.tbl")
end

function playerprofile:load()
	return table.load("|D|profile.tbl")
end


function table.copy(t)
  local t2 = {}
  for k,v in pairs(t) do
    t2[k] = v
  end
  return t2
end

--[[
	Save Table to File
	Load Table from File
	v 1.0
	
	Only Saves Tables, Numbers and Strings
	Insides Table References are saved
	Does not save Userdata, Metatables, Functions and indices of these
	----------------------------------------------------
	table.save( table , filename )
	
	on failure: returns an error msg
	
	----------------------------------------------------
	table.load( filename or stringtable )
	
	Loads a table that has been saved via the table.save function
	
	on success: returns a previously saved table
	on failure: returns as second argument an error msg
	----------------------------------------------------
]]--
do
	-- declare local variables
	--// exportstring( string )
	--// returns a "Lua" portable version of the string
	local function exportstring( s )
		return string.format("%q", s)
	end

	--// The Save Function
	function table.save(  tbl,filename )
		local charS,charE = "   ","\n"
		local file,err = io.open( filename, "wb" )
		if err then return err end

		-- initiate variables for save procedure
		local tables,lookup = { tbl },{ [tbl] = 1 }
		file:write( "return {"..charE )

		for idx,t in ipairs( tables ) do
			file:write( "-- Table: {"..idx.."}"..charE )
			file:write( "{"..charE )
			local thandled = {}

			for i,v in ipairs( t ) do
				thandled[i] = true
				local stype = type( v )
				-- only handle value
				if stype == "table" then
					if not lookup[v] then
						table.insert( tables, v )
						lookup[v] = #tables
					end
					file:write( charS.."{"..lookup[v].."},"..charE )
				elseif stype == "string" then
					file:write(  charS..exportstring( v )..","..charE )
				elseif stype == "number" then
					file:write(  charS..tostring( v )..","..charE )
				end
			end

			for i,v in pairs( t ) do
				-- escape handled values
				if (not thandled[i]) then
				
					local str = ""
					local stype = type( i )
					-- handle index
					if stype == "table" then
						if not lookup[i] then
							table.insert( tables,i )
							lookup[i] = #tables
						end
						str = charS.."[{"..lookup[i].."}]="
					elseif stype == "string" then
						str = charS.."["..exportstring( i ).."]="
					elseif stype == "number" then
						str = charS.."["..tostring( i ).."]="
					end
				
					if str ~= "" then
						stype = type( v )
						-- handle value
						if stype == "table" then
							if not lookup[v] then
								table.insert( tables,v )
								lookup[v] = #tables
							end
							file:write( str.."{"..lookup[v].."},"..charE )
						elseif stype == "string" then
							file:write( str..exportstring( v )..","..charE )
						elseif stype == "number" then
							file:write( str..tostring( v )..","..charE )
						end
					end
				end
			end
			file:write( "},"..charE )
		end
		file:write( "}" )
		file:close()
	end
	
	--// The Load Function
	function table.load( sfile )
		local ftables,err = loadfile( sfile )
		if err then return _,err end
		local tables = ftables()
		for idx = 1,#tables do
			local tolinki = {}
			for i,v in pairs( tables[idx] ) do
				if type( v ) == "table" then
					tables[idx][i] = tables[v[1]]
				end
				if type( i ) == "table" and tables[i[1]] then
					table.insert( tolinki,{ i,tables[i[1]] } )
				end
			end
			-- link indices
			for _,v in ipairs( tolinki ) do
				tables[idx][v[2]],tables[idx][v[1]] =  tables[idx][v[1]],nil
			end
		end
		return tables[1]
	end
-- close do
end

--Check a file exists
function exists(file)  --Does the file Exist?
    local f = io.open(file, "rb")
    if f == nil then
        return false
    end
    f:close()
    return true
end

function commscheck()
loader = UrlLoader.new("http://mj.live.hk.com")
loader:addEventListener(Event.COMPLETE, function() print("COMPLETE") end)
loader:addEventListener(Event.ERROR, function() print("ERROR") end)
end

