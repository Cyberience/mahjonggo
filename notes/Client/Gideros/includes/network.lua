--[[

]]

NWclass = Core.class(UrlLoader)

local nw 		= nil
local rawdata	= nil

function NWclass:init()
	--no need to do anything here .. yet
end

function NWclass.open(url,method,headers,body)
	nw	= UrlLoader.new(url,method,headers,body)
	print(nw)
	return nw
end

function NWclass.close()
	nw.close()
end

function NWclass.printNW()
	print(nw)
end

function NWclass.onComplete(event)
    rawdata = (event.data)
end

function NWclass.onError()
	print(event.error)
end

function NWclass.onProgress(event)
    print("progress: " .. event.bytesLoaded .. " of " .. event.bytesTotal)
end
