
-- Menu Bar routines
MenuBarClass = Core.class(Sprite)

function MenuBarClass:init()
--build the menu bar for all screens, the mast navigator
	--self:addChild(icons:get(10))
	self.ready = false
	self:addChild(Bitmap.new(Texture.new("images/icons/nav_bg.png")))
	
	self.menubar = {MenuItemClass.new("btn_home.png"		,"Home"			,"btn_home2.png"			),
					MenuItemClass.new("btn_novel.png"		,"Book" 		,"btn_novel2.png"			),
					MenuItemClass.new("btn_serial.png"		,"Serialize"	,"btn_serial.png"		),
					MenuItemClass.new("btn_column2.png"		,"Column" 		,"btn_column2.png"		),
					MenuItemClass.new("btn_video.png"		,"Video" 		,"btn_video2.png"			),
					MenuItemClass.new("btn_starauthor.png"	,"Star Author" 	,"btn_starauthor2.png"	),
					MenuItemClass.new("btn_acct.png"		,"Account" 		,"btn_acct2.png"			),
					MenuItemClass.new("btn_bkshelf.png"		,"Bookshelf"	,"btn_bkshelf2.png"		),
					MenuItemClass.new("btn_search.png"		,"Search" 		,"btn_search2.png"		)}
	--place on screen 
	for i, menuitem in pairs(self.menubar) do
		self:addChild( menuitem )
		menuitem:setPosition((menuitem.width*i*1.1)-menuitem.width ,2 )
   end
end


MenuItemClass = Core.class(Sprite)

function MenuItemClass:init(p_SpriteDn,p_title,p_SpriteUp)
	self.up 	= self:sprite(p_SpriteDn)
	self.down 	= self:sprite(p_SpriteUp)
	self.width = self.up:getWidth()
	self.title = p_title
	addLabel(self.up, p_title ,.52,.8,2,"0xA9A9A9")   --sprit to add to, text, scale, x, y, font, colour
	addLabel(self.down, p_title ,.52,.8,2,"0x008000")   --sprit to add to, text, scale, x, y, font, colour

	self:addChild(self.up)
	self:addEventListener(Event.TOUCHES_BEGIN, onMenuBarTouchDown, self)
	--self:addEventListener(Event.TOUCHES_MOVE, onMenuAvatarTouchMove, gridmap[col][row].sprite)
	self:addEventListener(Event.TOUCHES_END, onMenuBarTouchUp, self)
end


function MenuItemClass:sprite(p_icon)
	local retval = Bitmap.new(Texture.new("images/icons/"..p_icon))
	return retval
end

function onMenuBarTouchDown(self, event)
	if self:hitTestPoint(event.touch.x, event.touch.y) then
		self.isFocus = true
		self.x,self.y = event.touch.x, event.touch.y
		self:removeChild(self.up)
		self:addChild(self.down)
		event:stopPropagation()
	end
end

function onMenuBarTouchUp(self, event)
	if self:hitTestPoint(event.touch.x, event.touch.y) then  --make sure this is inside this sprite
		self:removeChild(self.down)
		self:addChild(self.up)
		if (math.abs(event.touch.x - self.x) < self:getWidth() and math.abs(event.touch.y-self.y) < self:getHeight()) then
			loadPage(self.title)
		end
		event:stopPropagation()  --Prevents the event running down the event tree
	end
end