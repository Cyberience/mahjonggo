-- db.lua
--[[ Psuedo Code 
1-check the DB is in |D|
2-if not, copy from |R| to |D|
1-Open DB
2-If DB Fail, Maybe-- restore from default db
3-Run once Timer Event to retreive Author update, 
4-Attach completion even to Author update download and import data when triggered
5-During the update, we should start a video sprite, rotating icon. (il find one)
Rules:
Rule 1: You can not open a DB file read and write in the deployment folders on the tablet, especially Android
	SO: 1:Check DB exists in the Document Folder.
		2: Not exist, Copy the deployed files to the document folder.
		3: for clean up, delete files in the document folder, should force restore to installed version when opening a file or DB.
Rule 2: Keep the DB stuff local to the object, and pass back arrays of the selection
]]

local req				= NWclass.new()
local requestRawData	= nil
local authorsReq		= nil
local uuidClass			= require("uuid")
local swObj				= require("switch")				

require("lsqlite3")

DBclass 				= Core.class()

local db				= nil

function DBclass:init()
	if not exists("db/Top100.db3") then		--show an alert and end the app
		local alertDlg		= AlertDialog.new("Error !!", "Could not load the data\nPower-off (then turn back on) your device to retry\nYou may have to reinstall", "OK")
		local function onComplete(event)
			print(event.buttonIndex, event.buttonText)
			--according to the Gideros API docs .. the following goes for the call to 'Application:exit' below ..
			--"Terminates the application. Although this function is available to all platforms, it should be used on Android only."
			--but does not say why, or what to do for iOS .. whatta shock!
			Application:exit()
		end
		alertDlg:addEventListener(Event.COMPLETE, onComplete)
		alertDlg:show()
	else
		if not exists("|D|Top100.db3") then
			copy("db/Top100.db3","|D|Top100.db3")
		end
		db	= self:open("|D|Top100.db3")
	end
end

function DBclass:open(dbFile)
	return sqlite3.open(dbFile)
end

function DBclass:close()
--DB:close()  
--[[Closing is somthing I am not sure if we can do, so in this case, 
	we may need to find a clean way to recover a DB crash, incase 
	during an update, the app is closed and corrupts the DB. 
	Closing here, makes no sense as it is user driven, and 
	while in memory, access should always be available, so the DB 
	is like the stage, globably available, and not open and closed 
	on demand.]]
	--db.close()
end

function DBclass:printDB()
	print(db)
end

function DBclass:populateDBfromXML(xmlData)
	
end

function DBclass:createUUID()
	return uuidClass.getUUID()
end

function DBclass:sendSyncAuthorsPost()
	local headers			= {
		["Content-Type"]	= "application/x-www-form-urlencoded",
		["User-Agent"]		= "Top 100 Channel mobile app"
	}

	local strUserUUID		= "00000000-0000-0000-0000-000000000001"
	local postData			= "uuid="..strUserUUID
	
	authorsReq				= req.open("http://keysystemsintl.com:58001/Top100/syncAuthors.php", UrlLoader.POST, headers, postData)
	
	authorsReq:addEventListener(Event.COMPLETE, onRequestComplete)
	authorsReq:addEventListener(Event.ERROR, onRequestError)
	authorsReq:addEventListener(Event.PROGRESS, onRequestProgress)

	print("request for syncAuthors sent")
	print(authorsReq)
end

function DBclass:sendSyncAuthors()
	local headers			= {
		["Content-Type"]	= "text/plain",
		["User-Agent"]		= "Top 100 Channel mobile app"
	}
	
	authorsReq				= req.open("http://keysystemsintl.com:58001/Top100/syncAuthors.php?uid=1", UrlLoader.GET, headers)
	
	authorsReq:addEventListener(Event.COMPLETE, onRequestComplete)
	authorsReq:addEventListener(Event.ERROR, onRequestError)
	authorsReq:addEventListener(Event.PROGRESS, onRequestProgress)

	print("request for syncAuthors sent")
	print(authorsReq)
end

function DBclass:getTopAuthors(p_rank)
	local sql				=	"SELECT	* "
	sql = sql				..	"FROM Authors "
	if (p_rank) then
		sql = sql 			..	"WHERE rank = "..p_rank
	end
	
	local retval = {}
	for authorId, firstName, lastName, midName, profileImageUrl, biography, votes, rank, subscribers, subscribers in db:urows(sql) do
		table.insert(retval, {["id"]=authorId, ["fname"]=firstName, ["lname"]=lastName, ["mname"]=midName, ["image"]=profileImageUrl, ["votes"]=votes, ["rank"]=rank, ["subscribers"]=subscribers} )
	end
	return retval
end

function DBclass:getAuthors(p_id)
	local sql				=	"SELECT	* "
	sql = sql				..	"FROM Authors "
	if (p_rank) then
		sql = sql 			..	"WHERE authorId = "..p_id
	end
	
	local retval = {}
	for authorId, firstName, lastName, midName, profileImageUrl, biography, votes, rank, subscribers, subscribers in db:urows(sql) do
		table.insert(retval, {["id"]=authorId, ["fname"]=firstName, ["lname"]=lastName, ["mname"]=midName, ["image"]=profileImageUrl, ["bio"]=biography, ["votes"]=votes, ["rank"]=rank, ["subscribers"]=subscribers} )
	end
	return retval
end

function DBclass:getAuthorById(authorId)

end

function DBclass:getAuthorByName(fName, mName, lName)

end


--utilities
function parseRawData(stream)			--this is determine the type of stream (Authors, Books, Columns, etc)
	local strChk		= string.sub(stream,1,7)
--[[  not working  .. fails on 'case'
	switch(strChk,
		case("AUTHSNC", syncLocalAuthors(string.sub(stream, 8))),
		case("AUTHUPD", updServerAuthors(string.sub(stream, 8))),
		case("BOOKSNC", syncLocalBooks(string.sub(stream, 8))),
		case("CLMNSNC", syncLocalColumns(string.sub(stream, 8))),
		default( function() print("switch has fallen thru to 'default'") end)
	)
--]]
	local sw		= swObj:switch()
	sw:case('AUTHSNC', function()
							syncLocalAuthors(string.sub(stream, 8))
					   end)
	sw:case('AUTHUPD', function()
							updServerAuthors(hasSucceeded)
					   end)
	sw:case('BOOKSNC', function()
							syncLocalBooks(string.sub(stream, 8))
					   end)
	sw:case('CLMNSNC', function()
							syncLocalColumns(string.sub(stream, 8))
					   end)
	sw:default(function() print("in the switch default") end)
	sw:test(strChk)
--[[	
	if(strChk == 'AUTHSNC') then		--this is an attempt to synchronize the Authors with the server
		syncLocalAuthors(string.sub(stream, 8))
	end
	
	if(strChk == 'AUTHUPD') then		--this is an attempt to tell the server that Authors has had sync attempt on Authors
		updServerAuthors(hasSucceeded)
	end
	
	if(strChk == 'BOOKSNC') then		--this is an attempt to synchronize the Books with the server
		syncLocalBooks(string.sub(stream, 8))
	end
	
	if(strChk == 'CLMNSNC') then		--this is an attempt to synchronize the Columns with the server
		syncLocalColumns(string.sub(stream, 8))
	end
--]]
end

function syncLocalAuthors(data)
	print("attempting to sync Authors with data passed: "..data)
	--'data' is a list of the current Top 100 Author IDs on the server,
	--then all data for any current author with changed info since the last sync,
	--separated by '^_^'

	--now need to parse all this into the local device database
	
end

function updServerAuthors(hasSucceeded)
	print("attempting to update server Authors sync info (user, success flag and date last synced)")
end

function syncLocalBooks(data)
	print("attempting to sync Books with data passed: "..data)
end

function syncLocalColumns(data)
	print("attempting to sync Columns with data passed: "..data)
end


--NW request events
function onRequestComplete(event)
    requestRawData = (event.data)
	print("onRequestComplete")
	print(requestRawData)
	parseRawData(requestRawData)
	
	authorsReq:removeEventListener(Event.COMPLETE, onRequestComplete)
	authorsReq:removeEventListener(Event.ERROR, onRequestError)
	authorsReq:removeEventListener(Event.PROGRESS, onRequestProgress)
end

function onRequestError()
	print('UnKnownError in DB line 232 this should not be called')
	
	authorsReq:removeEventListener(Event.COMPLETE, onRequestComplete)
	authorsReq:removeEventListener(Event.ERROR, onRequestError)
	authorsReq:removeEventListener(Event.PROGRESS, onRequestProgress)
end

function onRequestProgress(event)
    --print("http request progress: " .. event.bytesLoaded .. " of " .. event.bytesTotal)
end
