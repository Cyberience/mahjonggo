loading = Core.class(Sprite)

function loading:init()
	self:addChild(imageload.new("images/loadpage.jpg"))
	local frames=self:loadframes()
	local loader = Sprite.new()
	
	loader:addChild(frames[1])
	loader:setScale(1.5,1.5)
	loader:setPosition((application:getContentWidth()*.5)-(loader:getWidth()*.5),application:getContentHeight()*.75)  --sets centre location

	self:addChild(loader)
	loader:addChild(self:timein(frames))
end


function loading:timein(p_frames)
	local lag = (debug) and 2 or 20

	self.tilein = MovieClip.new{
					{1*lag, 2*lag, p_frames[2],{scale = {.8,1,"outElastic"}} },
					{2*lag, 3*lag, p_frames[3],{scale = {.8,1,"outElastic"}} },
					{3*lag, 4*lag, p_frames[4],{scale = {.8,1,"outElastic"}} },
					{4*lag, 5*lag, p_frames[5],{scale = {.8,1,"outElastic"}} },
					{5*lag, 6*lag, p_frames[6],{scale = {.8,1,"outElastic"}} },
					{6*lag, 7*lag, p_frames[7],{scale = {.8,1,"outElastic"}} },
					{7*lag, 8*lag, p_frames[8],{scale = {.8,1,"outElastic"}} },
					{8*lag, 9*lag, p_frames[9],{scale = {.8,1,"outElastic"}} },
					{9*lag, 10*lag, p_frames[10],{scale = {.8,1,"outElastic"}} }}
	self.tilein:addEventListener(Event.COMPLETE, function() scene:loadPage("hall") end )
	return self.tilein
end

function loading:loadframes()
local retval = {}
table.insert(retval,clips.new("images/loading.jpg", 1, 1, 24*9, 42))
for i = 1, 9 do
	table.insert(retval,clips.new("images/loading.jpg", 1+(24*i)-24, 43, 24, 42))
end

return retval
end

-- Load images here,
clips = Core.class(Sprite)

function clips:init(p_image,x,y,w,h)
 self:addChild(Bitmap.new(TextureRegion.new(Texture.new(p_image), x,y,w,h)))
 self:setPosition(x,1)
end