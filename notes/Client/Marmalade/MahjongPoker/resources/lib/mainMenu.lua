--[[
Main Menu
--]]

-- Create a scene to contain the main menu
menuScene = director:createScene()
-- UI
local playText

-- New game event handler, called when the user taps the New Game button
function newGame(event)
	-- Switch to game scene
	switchToScene("game")
end

-- Create menu background
local background = director:createSprite(director.displayCenterX, director.displayCenterY, "images/loadpage.jpg")
background.xAnchor = 0.5
background.yAnchor = 0.5
-- Fit background to screen size
background.xScale = 1 
background.yScale = 1 

-- Create Tile start
local tileBack = director:createSprite(
  {
   x=director.displayCenterX, y=director.displayCenterY,
   xAnchor=0.5, yAnchor=1,
   xScale = 1.5, yScale = 1.4,
   source="images/loading.png",
   uvRect = {0,0, 1,.5} ,
   } )


local timeInAtlas = director:createAtlas( {   width = 32,   height = 64,   numFrames = 16,   textureName = "images/loading.png"   } )
local timeInAnim = director:createAnimation( {   start = 9,   count = 1,   atlas = timeInAtlas,   delay = 1,   } )
local timeInSprite = director:createSprite( {
   x=director.displayCenterX, y=director.displayCenterY,
   xAnchor=0.5, yAnchor=1,
   xScale = 1.5, yScale = 1.5,
   source=timeInAnim,
   } )
--]]


--after animation, go to Menu screen
timeInSprite:addEventListener("touch", newGame)
