--[[/*
 * (C) 2012-2013 Marmalade.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */--]]

--[[
/**
Global object for pointer services.
After starting the service add a listener to the system events named "pointerButton" and "pointerMoved"
that is triggered when the pointer data change.
*/
--]]
pointer = {}

--[[
    /**
    Starts the pointer service. 
    @return true on success, false otherwise.
    */
--]]
function pointer:start()	
	if(quick.QPointer:start()) then
		system:addEventListener("update",pointer)
		return true
	end
	return false
end

--[[
    /**
    Stops the pointer service.
    */
--]]
function pointer:stop()
	system:removeEventListener("update",pointer)
	return quick.QPointer:stop()
end

--[[
    /**
    If the service has started this method is called on every update by the system.
    It queries the pointer values and if any change is detected, the corresponding following events are triggered:
    triggeredEvent 
    {
        event.name = "pointerButton"
        event.id = number
        event.pressed = bool
        event.x = number
        event.y = number
        event.pressure = number
    }
    
    triggeredEvent 
    {
        event.name = "pointerMoved" 
        event.id = number
        event.pressed = bool
        event.x = number
        event.y = number
        event.pressure = number
    }
    @param event the system update event
    */
--]]
function pointer:update(event)
	quick.QPointer:update()
end

--[[
    /**
    Query the state of the button number.
    @return an integer describing the state of the given button.
    */
--]]
function pointer:getPointerButtonState(buttonId)
	return quick.QPointer:getPointerButtonState()
end

--[[
    /**
    Query the x value of the pointer.
    @return an integer describing the current x value.
    */
--]]
function pointer:getPointerX()
    return quick.QPointer:getPointerX()
end

--[[
    /**
    Query the y value of the pointer.
    @return an integer describing the current y value.
    */
--]]
function pointer:getPointerY()
    return quick.QPointer:getPointerY()
end

--[[
    /**
    Query the pressure value of the pointer.
    @return an integer describing the current pressure value, -1 if it's not available.
    */
--]]
function pointer:getPointerPressure()
    return quick.QPointer:getPointerPressure()
end

--[[
    /**
    Calls s3e querying for service started integer property.
    @return true if the service is started, false otherwise.
    */
--]]
function pointer:hasStarted()
	return quick.QPointer:hasStarted()
end
