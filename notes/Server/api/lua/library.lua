function DecToBase(IN,BASE)
    local B,K,OUT,D=BASE or 10,"0123456789ABCDEFGHIJKLMNOPQRSTUVW",""
    while IN>0 do
        IN,D=math.floor(IN/B),math.mod(IN,B)+1
        OUT=string.sub(K,D,D)..OUT
    end
    return (OUT:len()>0) and OUT or "0"
end

function table.copy(t)
  local t2 = {}
  for k,v in pairs(t) do
    t2[k] = v
  end
  return t2
end