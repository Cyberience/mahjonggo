--check the hand
function checkhand(hand)
	local masterhand = migrate(hand)  --sort hand
	local test, retval = true,0
	
	--false at any point will mean a failed hand...
	test, retvalues = validhand(masterhand['b'],"S")
	if test then 
		masterhand = addvalues(masterhand, retvalues)
		test, retvalues = validhand(masterhand['c'],"S") 
	end
	if test then 
		masterhand = addvalues(masterhand, retvalues)
		test, retvalues = validhand(masterhand['m'],"S") 
	end
	if test then 
		masterhand = addvalues(masterhand, retvalues)
		test, retvalues = validhand(masterhand['W'],"W") 
	end
	if test then 
		masterhand = addvalues(masterhand, retvalues)
		test, retvalues = validhand(masterhand['D'],"D") 
	end
	if test then  --final check,
		masterhand = addvalues(masterhand, retvalues) 
		retval = masterhand.points.stre + masterhand.points.trip + masterhand.points.pair
	end
	print(hand)
	print("Stre:"..masterhand.points.stre)
	print("trip:"..masterhand.points.trip)
	print("Pair:"..masterhand.points.pair)
	print("Wind:"..masterhand.points.wind)
	print("Drag:"..masterhand.points.drag)
	return retval
end --function

function validhand(suit,type)

	local count, control, iteration, found, colv, worklen, retval, base,sets, workhand,chkstr, points,maxpoints,wind,drag = 0,"", -1, false, 0, 0, false, 3,5, "","",{stre=0, trip=0, pair=0, wind=0, drag=0},{stre=0, trip=0, pair=0, wind=0, drag=0},{"e","s","w","n"},{"r","b","g"}
	if suit:len() > 1 then  -- no point doing this if there is no tiles or a stand alone tile.
		sets = ((suit:len()-2)/3)+1 --columns  so 14 tiles is 4 sets plus 1 pair.
		worklen = suit:len()
		while (control:len() < sets+1 and not retval) do
			iteration = iteration + 1
			workhand = suit  --reset the hand
			control = DecToBase(iteration,base)  --counts up making is max 2 in each column
			control = string.rep("0",sets-control:len())..control
			if control:len() > sets then break end
			for icol = 1 , sets do --go through the sets, there is 4 sets of 3, plus 1 pair. Always
				colv = tonumber(control:sub(icol,icol))
				found = false  --reset to not found
				if type=="S" then
					for icard = 1, 9 do --walk through the string suits
						if colv == 0 and worklen > 2 and not found then --check for a street, can only be sequence of 3,
							chkstr = tostring(icard)..tostring(icard+1)..tostring(icard+2)
							workhand, count = string.gsub(workhand,chkstr,"",1)  --we have a streets replace only 1 occurance
							points.stre = points.stre + count
						elseif colv == 1 and worklen > 2 and not found then -- could be a 3 of a kind
							chkstr = string.rep(icard,3)
							workhand, count = string.gsub(workhand,chkstr,"",1)  --we have a triple replace all occurances
							points.trip = points.trip + count
						elseif colv == 2 and worklen > 1 and not found then -- check for a pair
							chkstr = string.rep(icard,2)
							tmpstr = workhand
							workhand, count = string.gsub(workhand,chkstr,"",1)  --we have pairs replace all occurances can only be 1 pair, or all pairs,
							if count == 1 or count == 7 then
								points.pair = points.pair + count
							else workhand = tmpstr end --reset the hand if the pairs are not 1 or 7 sets.
						end
						if (count>0) then --stops the checking of a second hand if one is found on an earlier number.
							found = true 
--							workhand = string.gsub(workhand,"0","") -- clean up by removing the 0, we could cut them as the string length would throw us out.
							worklen = workhand:len()
							count = 0 --reset
						else  
							found = false 
						end
					end --icard
				elseif type=="W" then --checks e,s,w,n pairs and triples
					for iloop = 1,#wind do  --check for sets of 3. and exit if found or reached 4 which means none
						if colv == 1 and worklen > 2 and not found then -- could be a 3 of a kind
							chkstr = string.rep(wind[iloop],3)
							workhand, count = string.gsub(workhand,chkstr,"",1)  --we have a triple replace first occurance
						elseif colv == 2 and worklen > 1 and not found then -- check for a pair
							chkstr = string.rep(wind[iloop],2)
							workhand, count = string.gsub(workhand,chkstr,"0",1)  --we have a triple replace first occurance
						end --if
						if count > 0 then
							points.trip = points.trip + count
							points.drag = points.drag + count
							worklen = workhand:len()
							count = 0 --reset
							break
						end --if
					end --for
				elseif type=="D" then --checks dragons red,gree,blue
					for iloop = 1,#drag do  --check for sets of 3. and exit if found or reached 4 which means none
						if colv == 1 and worklen > 2 and not found then -- could be a 3 of a kind
							chkstr = string.rep(drag[iloop],3)
							workhand, count = string.gsub(workhand,chkstr,"",1)  --we have a triple replace first occurances
						elseif colv == 2 and worklen > 1 and not found then -- check for a pair
							chkstr = string.rep(drag[iloop],2)
							workhand, count = string.gsub(workhand,chkstr,"",1)  --we have a pair replace first occurances
						end --if
						if count > 0 then
							points.trip = points.trip + count
							points.drag = points.drag + count
							worklen = workhand:len()
							count = 0 --reset
							break
						end --if
					end --for
				else --must be a special check then
				end --type check
			end --for icols
			if worklen == 0 then -- a perfect hand, might need to continue to see if there is a higher valid hand!! ***
				if points.stre+points.trip+points.pair > maxpoints.stre+maxpoints.trip+maxpoints.pair then
					maxpoints.stre,maxpoints.trip,maxpoints.pair = points.stre,points.trip,points.pair 
				end
				retval = true
			else --reset working hand for the next combination
				workhand = suit
				worklen = workhand:len()
				points.stre,points.trip,points.pair, points.wind, points.drag = 0,0,0,0,0-- reset the scores

			end
		end -- for interations
	elseif suit:len() == 1 then  retval = false  --Guaranteed Failure, cos any single card is a totally invalid hand
	else retval = true   --empty hand is a valid state, must be no tiles in this suit
	end
return true,maxpoints
end

function validspecials(suit) --winds and dragons can only be 1 pair or sets of 3 or 4,
	local found, points, retval, points = false, 0, true, {stre=0, trip=0, pair=0}
	if suit:len() > 1 then
	
	end
	return retval,points
end

--sum up
function addvalues(oval,nval)
		oval.points.stre = oval.points.stre + nval.stre
		oval.points.trip = oval.points.trip + nval.trip
		oval.points.pair = oval.points.pair + nval.pair
		oval.points.wind = oval.points.pair + nval.wind
		oval.points.drag = oval.points.pair + nval.drag
	return oval
end

-- Split the hands to the various suits.....
function migrate(hand) --this function will take the hand, and place the peices into the known working structure,
	local masterhand = { b='',c='',m='',W='',D='', points={stre=0,trip=0,quad=0,pair=0} }
	local hand = string.match(hand, "[^|]+")  --get the hand, exlcude declared, to delimit the string, use gmatch
	for loop = 1 , hand:len() , 2 do  --step every 2 through the pairs in 2 upto the declared had set.
		if string.match("bcmWD",hand:sub(loop ,loop)) then --if the char is b c m W od D, append the value.
			masterhand[hand:sub(loop ,loop)] = masterhand[hand:sub(loop ,loop)]..hand:sub(loop+1 ,loop+1)
		end --if, should be no failures here.
	end -- loop1 ]]
	--this may be a good place to scor the declared hands.
return masterhand
end


