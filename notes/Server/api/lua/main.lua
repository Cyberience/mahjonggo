-- If its the Linux module, un rem the next lines
--  hand = arg[1] or ""
--  print(checkhand(hand))
-- above is required for linux stand alone hand check

-- testing code
hand = ""
--hand = "E:b1b9c1c9m1m9WeWsWwWnDrDbDgXx"  --13 orphans
hand = "E:b1b1b2b3b4b7b7b7WsWsWsWnWnWn" -- "b1b1b1b1b2b3b4b4b5b5b5b9b9b9"
--hand = "E:WeWeWeWsWsWsc7c7|WnWnWn|WwWwWw"
	-- or	= "b1b1b2b3b4:c7c7c7:WsWsWs:WnWnWn"
	-- or 	= "b1b1c7c7c7WsWsWsWnWnWn:b2b3b4"
	-- or	= "c1c2c3b4b5b6m7m8m9DrDrDrDbDbDb"
	-- or	= "b1b9c1c9m1m9WeWsWwWnDrDbDg"  --special 13 orfans
	
	-- notes: 	c = circles, b = bamboo, m = millions
	--			W = winds, s = south, n = north, e = east, w = west
	--			D = dragons b = Blue dragon, r = Red Dragon, g = Green dragon 
--[[note: 
	get clean pattern, like this->  111223334455
	Start Count, 1 to 154.
	0000
	||||- 123, 234, 345, 456, 567, 678, 789
	|||-- 111. 222, 333, 444, 555, 666, 777, 888, 999, nnn, sss, eee, www, rrr, bbb, ggg
	||--- 1111, 2222, 3333, 4444, 5555, 6666, 7777, 8888, 9999
	|---- 11, 22, 33, 44, 55, 66, 77, 88, 99, nn, ss, ee, ww, rr, bb, gg
	]]
--_,_,a=string.find(hand, '([^:]*)')
--print(a)

--b=tonumber("10",'2')
--print(b)
--local c = DecToBase(156,4)
--print(c)
--print(math.ceil(math.pow(14/3,4)))

function DecToBase(IN,BASE)
    local B,K,OUT,D=BASE or 10,"0123456789ABCDEFGHIJKLMNOPQRSTUVW",""
    while IN>0 do
        IN,D=math.floor(IN/B),math.fmod(IN,B)+1
        OUT=string.sub(K,D,D)..OUT
    end
    return (OUT:len()>0) and OUT or "0"
end

--check the hand
function checkhand(hand)
		local masterhand = migrate(hand)  --sort hand
		local test, retval = true,0
	--false at any point will mean a failed hand...
	test, retvalues = validspecials(hand)  --raw hand for speed

	if  (not test) then  --if a valid special, dont check anything else, just go straight to the add values
		test, retvalues = validhand(masterhand.hand['b'],"b")
		if test then 
			masterhand = addvalues(masterhand, retvalues)
			test, retvalues = validhand(masterhand.hand['c'],"c")
		end
		if test then 
			masterhand = addvalues(masterhand, retvalues)
			test, retvalues = validhand(masterhand.hand['m'],"m") 
		end
		if test then 
			masterhand = addvalues(masterhand, retvalues)
			test, retvalues = validhand(masterhand.hand['W'],"W") 
		end
		if test then 
			masterhand = addvalues(masterhand, retvalues)
			test, retvalues = validhand(masterhand.hand['D'],"D") 
		end
	end

	if test then  --final check,
		masterhand = addvalues(masterhand, retvalues)
		masterhand = declarecheck(masterhand)
		retval = masterhand.points:total()   --return total points.
	end
	--[[
	print(hand)
	print("Stre:"..masterhand.points.stre)
	print("trip:"..masterhand.points.trip)
	print("Pair:"..masterhand.points.pair)
	print("Wind:"..masterhand.points.wind)
	print("Drag:"..masterhand.points.drag)
	print("Faan:"..masterhand.points.faan)
	print("Spec:"..masterhand.points.spec)
	--]]
	return retval
end --function

function validhand(suit,type)
	local count, control, iteration, found, colv, worklen, retval, base,sets, workhand, chkstr, wind, drag = 0,"", -1, false, 0, 0, false, 3,5, "","",{"e","s","w","n"},{"r","b","g"}
	local points=scores() --set structure
	local maxpoints=scores() --set structure
	if suit:len() > 1 then  -- no point doing this if there is no tiles or a stand alone tile.
		sets = ((suit:len()-2)/3)+1 --columns  so 14 tiles is 4 sets plus 1 pair.
		worklen = suit:len()
		while (control:len() < sets+1 and not retval) do
			iteration = iteration + 1
			workhand = suit  --reset the hand
			control = DecToBase(iteration,base)  --counts up making is max 2 in each column
			control = string.rep("0",sets-control:len())..control
			if control:len() > sets then break end
			for icol = 1 , sets do --go through the sets, there is 4 sets of 3, plus 1 pair. Always
				colv = tonumber(control:sub(icol,icol))
				found = false  --reset to not found
				if string.match("bcm",type) then  --for hands
					for icard = 1, 9 do --walk through the string suits
						if colv == 0 and worklen > 2 and not found then --check for a street, can only be sequence of 3,
							chkstr = tostring(icard)..tostring(icard+1)..tostring(icard+2)
							workhand, count = string.gsub(workhand,chkstr,"",1)  --we have a streets replace only 1 occurance
							points.stre = points.stre + count
						elseif colv == 1 and worklen > 2 and not found then -- could be a 3 of a kind
							chkstr = string.rep(icard,3)
							workhand, count = string.gsub(workhand,chkstr,"",1)  --we have a triple replace all occurances
							points.trip = points.trip + count
						elseif colv == 2 and worklen > 1 and not found then -- check for a pair
							chkstr = string.rep(icard,2)
							tmpstr = workhand
							workhand, count = string.gsub(workhand,chkstr,"",1)  --we have pairs replace all occurances can only be 1 pair, or all pairs,
							if count == 1 then
								points.pair = points.pair + count
							else workhand = tmpstr end --reset the hand if there is no pairs are not 1.
						end
						if (count>0) then --stops the checking of a second hand if one is found on an earlier number.
							found = true 
							worklen = workhand:len()
							if type == "b" then
								points.bamb = points.bamb + 1
							elseif type == "c" then
								points.circ = points.circ + 1
							elseif type == "m" then
								points.mill = points.mill + 1
							end
							count = 0 --reset
						else  
							found = false 
						end
					end --icard
				elseif type=="W" then --checks e,s,w,n pairs and triples
					for iloop = 1,#wind do  --check for sets of 3. and exit if found or reached 4 which means none
						if colv == 1 and worklen > 2 and not found then -- could be a 3 of a kind
							chkstr = string.rep(wind[iloop],3)
							workhand, count = string.gsub(workhand,chkstr,"",1)  --we have a triple replace first occurance
						elseif colv == 2 and worklen > 1 and not found then -- check for a pair
							chkstr = string.rep(wind[iloop],2)
							workhand, count = string.gsub(workhand,chkstr,"0",1)  --we have a triple replace first occurance
						end --if
print("winds:"..count)
						if count > 0 then
							points.trip = points.trip + count
							points.wind = points.wind + count
							worklen = workhand:len()
							count = 0 --reset
							break
						end --if
					end --for
				elseif type=="D" then --checks dragons red,gree,blue
					for iloop = 1,#drag do  --check for sets of 3. and exit if found or reached 4 which means none
						if colv == 1 and worklen > 2 and not found then -- could be a 3 of a kind
							chkstr = string.rep(drag[iloop],3)
							workhand, count = string.gsub(workhand,chkstr,"",1)  --we have a triple replace first occurances
						elseif colv == 2 and worklen > 1 and not found then -- check for a pair
							chkstr = string.rep(drag[iloop],2)
							workhand, count = string.gsub(workhand,chkstr,"",1)  --we have a pair replace first occurances
						end --if
						if count > 0 then
							points.trip = points.trip + count
							points.drag = points.drag + count
							worklen = workhand:len()
							count = 0 --reset
							break
						end --if
					end --for
				else --must be a special check then
				end --type check
			end --for icols
			if worklen == 0 then -- a perfect hand, might need to continue to see if there is a higher valid hand!! ***
				if points:summary() > maxpoints:summary() then
					maxpoints.stre=maxpoints.stre+points.stre;  
					maxpoints.trip=maxpoints.trip+points.trip;  
					maxpoints.pair=maxpoints.pair+points.pair;
					maxpoints.circ=maxpoints.circ+points.circ;
					maxpoints.bamb=maxpoints.bamb+points.bamb;
					maxpoints.mill=maxpoints.mill+points.mill;
					maxpoints.wind=maxpoints.wind+points.wind;
					maxpoints.drag=maxpoints.drag+points.drag;
				end
				retval = true
			else --reset working hand for the next combination
				workhand = suit
				worklen = workhand:len()
				points.stre=0;  
				points.trip=0;  
				points.pair=0;
				points.circ=0;
				points.bamb=0;
				points.mill=0;
				points.wind=0;
				points.drag=0;
			end
		end -- for interations
	elseif suit:len() == 1 then  retval = false  --Guaranteed Failure, cos any single card is a totally invalid hand
	else retval = true   --empty hand is a valid state, must be no tiles in this suit
	end
return true,maxpoints
end

function validspecials(suit,type) --winds and dragons can only be 1 pair or sets of 3 or 4,
	local workhand, retval, declares = "", false, 0
	local points=scores()
	local specials ={	orphans 	= {"b1","b9","c1","c9","m1","m9","We","Ws","Ww","Wn","Dr","Db","Dg"},
						bigwinds 	= {"We","Ws","Ww","Wn"}
					}
	if suit:len() > 13 then
		for index, data in pairs(specials) do
			workhand = suit

			if index=="orphans" and not retval then
				for i,v in ipairs(specials.orphans) do
					workhand, count = string.gsub(workhand,v,"",1) 
				end
				if workhand:len() < 5 then --a winner
					retval = true
					points.faan = 8
					points.spec = "Orphans"
				end
			elseif index=="bigwinds" and not retval then
				for i,v in ipairs(specials.bigwinds) do
					workhand, declares = string.gsub(workhand,"|","") --remove the declare delimters and count them.
					workhand, count = string.gsub(workhand,v,"")
				end
				if workhand:sub(1,2) == workhand:sub(3,5) then --a remaining pair
					retval = true
					points.faan = 8 + declares
					points.spec = "Big4Winds"
				end
			end --add one to check for a full set of pairs.
		end --specials
	end
	return retval,points
end

function declarecheck(masterhand)
	local val = 0
	for index, data in pairs(masterhand.declares) do
		if data:len() > 0 and data:len() < 5 then --score 1 eith 3 or 4, street or sets
			val=1
		elseif data:len() > 4 and data:len() < 9 then --scores must be 2 sets
			val=2
		elseif data:len() > 8 and data:len() < 13 then --scores 3
			val=3
		elseif data:len() > 12 then --must be 4 then.
			val=4
		else val = 0 -- only none then!!
		end
		if string.match("bcm",index) then --add value to the street count
			masterhand.points.trip = masterhand.points.trip + val
		elseif string.match("W",index) then --do the wind calculation
			masterhand.points.wind = masterhand.points.wind + val
			masterhand.points.stre = masterhand.points.stre + val
			if string.match(masterhand.hand.W..data,masterhand.seat:lower()) then -- if there is a seat string...
				masterhand.points.mult = 2
			end
		elseif string.match("D",index) then --do the dragon
			masterhand.points.drag = masterhand.points.drag+val
			masterhand.points.mult = masterhand.points.mult+val
		end --if
	end
	return masterhand
end

--sum up
function addvalues(oval,nval)
		oval.points.stre = oval.points.stre + nval.stre
		oval.points.trip = oval.points.trip + nval.trip
		oval.points.pair = oval.points.pair + nval.pair
		oval.points.bamb = oval.points.bamb + nval.bamb
		oval.points.circ = oval.points.circ + nval.circ
		oval.points.mill = oval.points.mill + nval.mill
		oval.points.wind = oval.points.wind + nval.wind
		oval.points.drag = oval.points.drag + nval.drag
		oval.points.faan = oval.points.faan + nval.faan
		oval.points.spec = nval.spec
	return oval
end

-- Split the hands to the various suits.....
function migrate(hand) --this function will take the hand, and place the peices into the known working structure,
	local track = 0  --to track the row, if no declares, then no second field
	local masterhand = { seat="", hand={b='',c='',m='',W='',D=''}, declares={b='',c='',m='',W='',D=''}, points=scores() }
	masterhand.seat=string.match(hand, "[^:]+") or ""  --before the :
	hand = string.match(hand, ":(.*)" ) or hand --strip the seat out of the hand 
	string.gsub(hand,"[^|]+",function(hand)
		track=track+1
		for loop = 1 , hand:len() , 2 do  --step every 2 through the pairs in 2 upto the declared had set.
			if string.match("bcmWD",hand:sub(loop ,loop)) then --if the char is b c m W od D, append the value.
				if track==1 then  --for the main hand before the | (pipe)
					masterhand.hand[hand:sub(loop ,loop)] = masterhand.hand[hand:sub(loop ,loop)]..hand:sub(loop+1 ,loop+1)
				else --for the declares.
					masterhand.declares[hand:sub(loop ,loop)] = masterhand.declares[hand:sub(loop ,loop)]..hand:sub(loop+1 ,loop+1)
				end
			end --if, should be no failures here.
		end -- loop1 ]]
	end)
return masterhand
end

function scores()
	return {
  stre=0;  --only in hand
  trip=0;  --includes declared streets
  pair=0;
  circ=0;
  bamb=0;
  mill=0;
  wind=0;
  drag=0;
  faan=0;
  mult=1;  --multiplier
  spec="";
  summary=function(self)  --this function is for running calculations only....Final value is the total()
	calc=self:total()
	if calc==0 then
		sum=self.stre+self.trip+self.pair+self.circ+self.bamb+self.mill+self.wind+self.drag
	else sum = calc end
	
	return sum
	end;
  total= function(self) --this will consolidate the scores 
	--[[Note:Here is some basic scoring:
			Complete hand-0 fan
			4 Chows in a hand - 1 additional fan
			4 Pungs and/or Kongs in a hand - 3 additional fan
			1 or 2 Dragon Pungs or Kongs - 1 additional fan each
			A pair of Dragon Pung and another pair of Dragon - 4 additional fan
			Pungs/Kongs of Winds that matches the round or seat - 1 additional fan
			No Flowers/Season tile - 1 additional fan
			Flowers/Season tile that matches seat value - 1 additional fan each
			All Flowers/Season of a particular color - 2 additional fan
			Win by self-drawn - 1 additional fan
	--]]
		local sumtotal = 0
		if  (self.wind+self.drag) == 5 then -- Pure Honor hand = Limit
			sumtotal = 99
		elseif self.circ == 5 or self.bamb == 5 or self.mill == 5 then -- Pure hand = 6
			sumtotal = 6
		elseif self.wind == 4 then --blessings = 6;
			sumtotal = 6
		elseif self.drag == 3 and self.trip > 2 then --3 Great schollars = 6
			sumtotal = 6
		elseif self.drag == 3 and self.trip == 2 then --3 Lesser schollars = 3 (2 triples and a pair)
			sumtotal = 3
		elseif (self.circ == 4 or self.bamb == 4 or self.mill==4) and (self.wind+self.drag) == 1 then --Mixed pure hand = 3
			sumtotal = 3
		elseif self.stre + self.trip + self.pair > 4 then --all chows = 1
			sumtotal = 1
		end
		sumtotal = sumtotal * self.mult
		
print("(street:"..self.stre..")(Triple:"..self.trip..")(Pairs-:"..self.pair..")(Circle:"..self.circ..")(Bamboo:"..self.bamb..")(Millio:"..self.mill..")")		
print("(Winds-:"..self.wind..")(Dragon:"..self.drag..")(Fans--:"..self.faan..")(Multip:"..self.mult..")(Specia:"..self.spec..")(Total-:"..sumtotal..")")
		return sumtotal
	end
	}
end


print(checkhand(hand))