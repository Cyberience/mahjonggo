<?php
$passcode = "dAS7M2qXT";

if(isset($_GET["action"]) && isset($_GET["pass"]) && $_GET["pass"] == $passcode)
{
	include("./mysql.php");
	include("./funcs.php");
	$debug = false;
	$q = new mysql($debug);
	$_GET = $q->safe($_GET);
	
	if(strtolower($_GET["action"]) == "insert" && isset($_GET["app"]) && isset($_GET["game"]) && isset($_GET["username"]) && isset($_GET["score"]))
	{
		$app = $q->select_byvar("ID", "apps", "where name = '".$_GET["app"]."' LIMIT 1");
		if(!is_array($app))
		{
			$q->insert("apps", "name = '".$_GET["app"]."'");
			$app = $q->select_byvar("ID", "apps", "where name = '".$_GET["app"]."' LIMIT 1");
		}
		unset($_GET["app"]);
		$game = $q->select_byvar("ID", "games", "where appID = '".$app["ID"]."' and name = '".$_GET["game"]."' LIMIT 1");
		if(!is_array($game))
		{
			$q->insert("games", "appID = '".$app["ID"]."', name = '".$_GET["game"]."'");
			$game = $q->select_byvar("ID", "games", "where appID = '".$app["ID"]."' and name = '".$_GET["game"]."' LIMIT 1");
		}
		unset($_GET["game"]);
		$_GET["gameID"] = $game["ID"];
		if(!isset($_GET["time"]))
		{
			$_GET["time"] = time();
		}
		$score = $q->select_byvar("*", "score", "where gameID = '".$_GET["gameID"]."' and username = '".$_GET["username"]."' LIMIT 1");
		if(!is_array($score))
		{
			$q->insert_array("score", $_GET, array("action"));
			$score = (int)$_GET["score"];
		}
		else
		{
			if((int)$score["score"] >= (int)$_GET["score"])
			{
				$score = (int)$score["score"];
			}
			else
			{
				$q->update("score", "score = '".$_GET["score"]."'", "WHERE ID = '".$score["ID"]."'");
				$score = (int)$_GET["score"];
			}
		}
		$place = $q->select_byvar("count(ID) as cnt", "score", "where score > ".$score);
		out_one($place["cnt"]+1, "place");
	}
	else if(strtolower($_GET["action"]) == "top" && isset($_GET["app"]) && isset($_GET["game"]))
	{
		$lim = (isset($_GET["limit"])) ? $_GET["limit"] : 10;
		$start = (isset($_GET["start"])) ? $_GET["start"] : 0;
		$app = $q->select_byvar("ID", "apps", "where name = '".$_GET["app"]."' LIMIT 1");
		if(is_array($app))
		{
			$game = $q->select_byvar("ID", "games", "where appID = '".$app["ID"]."' and name = '".$_GET["game"]."' LIMIT 1");
			if(is_array($game))
			{
				$result = $q->result("username, comment, time, score", "score", "where gameID = '".$game["ID"]."' order by score desc LIMIT ".$start.", ".$lim);
				out_result($result, "result", true);
			}
			else
			{
				out_one("no result", "error");
			}
		}
		else
		{
			out_one("no result", "error");
		}
	}
}
?>