<?php
date_default_timezone_set("Europe/Helsinki");
function out_result($result, $tag = "result", $count = false){
	global $q;
	$arr = array();
	$cnt = 1;
	if($result != 0)
	{
		while($row = $q->fetch($result))
		{
			if($count)
			{
				$row["place"] = $cnt++;
			}
			$arr[] = $row;
		}
	}
	if(isset($_GET["type"]) && $_GET["type"] == "json")
	{
		echo json_encode($arr);
	}
	else
	{
		$xml = simplexml_load_string("<?xml version='1.0' encoding='utf-8'?><data />");
		foreach($arr as $ind => $val)
		{
			$node = $xml->addChild($tag);
			foreach($val as $key => $value)
			{
				//$value = htmlentities($value);
				$node->addChild($key,$value);
			}
		}
		header('Content-Type:text/xml; charset=UTF-8');
		echo $xml->asXML();
	}
}
function out_one($val, $tag = "error"){
	if(isset($_GET["type"]) && $_GET["type"] == "json")
	{
		echo  json_encode(array($tag => $val));
	}
	else
	{
		header('Content-Type:text/xml; charset=UTF-8');
		$xml = simplexml_load_string("<?xml version='1.0' encoding='utf-8'?><".$tag.">".$val."</".$tag.">");
		echo $xml->asXML();
	}
}
function out_arr($arr){
	if(isset($_GET["type"]) && $_GET["type"] == "json")
	{
		echo json_encode($arr);
	}
	else
	{
		$xml = simplexml_load_string("<?xml version='1.0' encoding='utf-8'?><data />");
		foreach($arr as $key => $val)
		{
			$xml->addChild($key,$val);
		}
		header('Content-Type:text/xml; charset=UTF-8');
		echo $xml->asXML();
	}
}
?>