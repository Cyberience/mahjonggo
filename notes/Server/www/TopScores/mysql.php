<?php
class mysql
{
	private $log;
	private $collect = true;
	private $pass = false;
	private $explain = false;
	
	function __construct($debug = false, $explain = false) {
		$this->debug = $debug;
		$this->explain = $explain;
		$this->cnt = 0;
		$this->host = "localhost";
		$this->db = "dbname";
		$this->user = "root";
		$this->pass = "password";
		$this->serverlink = mysql_connect($this->host, $this->user, $this->pass) or die (mysql_error());
		if($this->debug)
		{
			mysql_select_db($this->db) or die (mysql_error());
			mysql_query('SET NAMES UTF8') or die (mysql_error());
		}
		else
		{
			mysql_select_db($this->db);
			mysql_query('SET NAMES UTF8');
		}
   }
	
	public function make_log($logs){
		$this->log = $logs;
	}

	public function get_con(){
		return $this->serverlink;
	}
	
    public function result($what ="*", $where, $condition = "", $errdata = array()) {
        $query = "select ".$what." from `".$where."` ".$condition;
		$result = $this->exec($query, $errdata);
		if(mysql_num_rows($result) > 0)
		{
			return $result;
		}
		else return 0;
	}
	
	public function get_types($table, $exclude = array(), $condition = "") {
		$result = $this->exec("DESCRIBE ".$table);
		$arr = array();
		if($result != 0)
		{
			$val = $this->select_byvar("*", $table, $condition);
			while($row = $this->fetch($result))
			{
				if(!in_array($row['Field'], $exclude))
				{
					foreach($row as $key => $value)
					{
						if($key != 'Field')
						{
							if($key == 'Type')
							{
								if(strpos($value, "(") === false)
								{
									$arr[$row['Field']][strtolower($key)] =  substr($value, 0);
								}
								else
								{
									$arr[$row['Field']][strtolower($key)] =  substr($value, 0, strpos($value, "("));
								}
								if(strpos($value, "(") == false)
								{
									$arr[$row['Field']]['max_len'] =  '';
								}
								else
								{
									$arr[$row['Field']]['max_len'] =  substr($value, strpos($value, "(") + 1, (strpos($value, ")") - strpos($value, "(")) -1);
								}
							}
							else if($key == 'Null')
							{
								if($value == 'YES')
								{
									$arr[$row['Field']]['not_null'] = 0;
								}
								else
								{
									$arr[$row['Field']]['not_null'] = 1;
								}
							}
							else if($key == 'Default')
							{
								$arr[$row['Field']]['def'] =  $value;
							}
							else
							{
								$arr[$row['Field']][strtolower($key)] =  $value;
							}
							if(is_array($val))
							{
								$arr[$row['Field']]['value'] = $val[$row['Field']];
							}
							else
							{
								$arr[$row['Field']]['value'] = '';
							}
						}
					}
				}
			}
			
			return $arr;
		}
		else
		{
			return 0;
		}
	}
	
	public function count($what = "*", $where, $condition = "", $errdata = array()) {
		$query = "select count(".$what.") as cnt from ".$where." ".$condition;
		$result = $this->exec($query, $errdata);
		if(mysql_num_rows($result) > 0)
		{
			$arr = mysql_fetch_assoc($result);
			return $arr["cnt"];
		}
		else return 0;
    }
	
	public function count_result($result) {
		
		$count = mysql_num_rows($result);
		return $count;
    }
	
	public function fetch($result, $strip = true) {
		if($arr = mysql_fetch_assoc($result))
		{
			if($strip)
			{
				foreach ($arr as $key => $value)
				{
					$arr[$key] = $this->strip($value);
				}
			}
			return $arr;
		}
		else
		{
			return 0;
		}
    }
	
	public function select_query($what ="*", $where, $condition = "") {
        $query = "select ".$what." from ".$where." ".$condition;
		return $query;
    }
	
	public function insert($where, $what, $errdata = array()) {
        $query = "insert into `".$where."` set ".$what;
		$this->exec($query, $errdata);
    }
	
	public function update($where, $what, $condition = "", $errdata = array()) {
        $query = "update `".$where."` set ".$what." ".$condition;
		$result = $this->exec($query, $errdata);
		
    }
	
	public function delete($what, $condition = "", $errdata = array()) {
        $query = "delete from ".$what." ".$condition;
		$result = $this->exec($query, $errdata);
		
    }
	
	public function insert_array($where, $arr, $exclude = array(), $errdata = array()) {

		$state = 0;
        $query = "insert into `".$where."` set ";
		foreach ($arr as $key => $value)
		{
			if(!in_array($key,$exclude))
			{
				if($state == 0)
				{
					$query = $query."`".$key."` = '".$value."'";
					$state++;
				}
				else	
				{
					$query = $query.", `".$key."` = '".$value."'";
				}
			}
		}
		$result = $this->exec($query, $errdata);
    }
	
	public function update_array($where, $arr, $exclude = array(), $condition = "", $errdata = array()) {
		$state = 0;
        $query = "update `".$where."` set ";
		foreach ($arr as $key => $value)
		{
			if(!in_array($key,$exclude))
			{
				if($state == 0)
				{
					$query = $query."`".$key."` = '".$value."'";
					$state++;
				}
				else	
				{
					$query = $query.", `".$key."` = '".$value."'";
				}
			}
		}
		$query = $query." ".$condition;
		$result = $this->exec($query, $errdata);
    }
	
	public function delete_file($what ="*", $where, $condition = "", $del = false, $errdata = array()) {

        $query = "select ".$what." from ".$where." ".$condition;
		$result = $this->exec($query, $errdata);
		if(mysql_num_rows($result) > 0)
		{
			while($row = mysql_fetch_assoc($result))
			{
				foreach ($row as $key => $value)
				{
					if (file_exists($value)) {unlink($value);}
				}
			}
			
		}
		if($del)
		{
			$query = "delete from ".$where." ".$condition;
			$this->exec($query, $errdata);
		}
    }
	
	public function select_byvar($what ="*", $where, $condition = "", $errdata = array()) {
        $query = "select ".$what." from `".$where."` ".$condition;
		$result = $this->exec($query, $errdata);
		if($result != 0 && mysql_num_rows($result) > 0)
		{
			$arr = mysql_fetch_assoc($result);
			foreach ($arr as $key => $value)
			{
				$arr[$key] = $this->strip($value);
			}
			
			return $arr;
		}
		else return 0;
    }
		
	public function check($what, $where, $arr, $exclude = array(), $errdata = array()) {
		$state = 0;
        $query = "select count(".$what.") from ".$where." where ";
		foreach ($arr as $key => $value)
		{
			if(!in_array($key,$exclude))
			{
				if($state == 0)
				{
					$query = $query.$key." = '".$value."'";
					$state++;
				}
				else	
				{
					$query = $query." and ".$key." = '".$value."'";
				}
			}
		}
		$result = $this->exec($query, $errdata);
		if(mysql_num_rows($result) > 0)
		{
			
			return mysql_num_rows($result);
		}
		else
		{
			return 0;
		}
    }
	
	public function select_byarr($what, $where, $arr, $exclude = array(), $errdata = array()) {
		$state = 0;
        $query = "select ".$what." from `".$where."` where ";
		foreach ($arr as $key => $value)
		{
			if(!in_array($key,$exclude))
			{
				if($state == 0)
				{
					$query = $query.$key." = '".$value."'";
					$state++;
				}
				else	
				{
					$query = $query." and ".$key." = '".$value."'";
				}
			}
		}
		$result = $this->exec($query, $errdata);
		if(mysql_num_rows($result) > 0)
		{
			$arr = mysql_fetch_assoc($result);
			foreach ($arr as $key => $value)
			{
				$arr[$key] = $this->strip($value);
			}
			
			return $arr;
		}
		else
		{
			return 0;
		}
    }
	
	public function error_mysql($type, $err, $file, $line, $context = ""){
		$arr["type"] = $type;
		$arr["time"] = time();
		$arr["error_text"] = $err;
		$arr["script_name"] = $_SERVER["SCRIPT_NAME"];
		$arr["request_uri"] = $_SERVER["REQUEST_URI"];
		$arr["filename"] = $file;
		$arr["line"] = $line;
		$arr["context"] = $context;
		$arr = $this->safe($arr);
		$this->pass = true;
		$this->insert_array("error_log", $arr);
		//echo "Sql error of <b>".$_SERVER["SCRIPT_NAME"]."</b>. Call to `` in ".__FILE__." on line: ".__LINE__."<br>".mysql_error()."<br><b>SQL:</b><br>$query<br>";
	}
	
	public function error_php($errno, $errstr, $errfile, $errline){
		$arr["type"] = $errno;
		$arr["time"] = time();
		$arr["error_text"] = $errstr;
		$arr["script_name"] = $_SERVER["SCRIPT_NAME"];
		$arr["request_uri"] = $_SERVER["REQUEST_URI"];
		$arr["filename"] = $errfile;
		$arr["line"] = $errline;
		$arr = $this->safe($arr);
		$this->pass = true;
		$this->insert_array("error_log", $arr);
		/* Don't execute PHP internal error handler */
		return true;
	}
	
	public function exec($query, $errdata = array()) {
		$this->cnt = $this->cnt + 1;
		if($this->debug == true)
		{
			echo "<p>".($this->cnt).") ".$query."<br/>";
			$result = mysql_query($query, $this->serverlink) or die (mysql_error());
			if($this->explain && strtolower(substr($query, 0, 6)) == "select")
			{
				$test = mysql_query("EXPLAIN ".$query, $this->serverlink) or die (mysql_error());
				$rez = $this->fetch($test);
				echo "<table border='1' cellpadding='5'>";
				$r1 = "<tr style='font-weight: bold;'>";
				$r2 = "<tr>";
				foreach($rez as $key => $val)
				{
					$r1 .= "<td>".$key."</td>";
					if($val != "")
					{
						$r2 .= "<td>".$val."</td>";
					}
					else
					{
						$r2 .= "<td>&nbsp;</td>";
					}
				}
				echo $r1."</tr>";
				echo $r2."</tr>";
				echo "</table>";
			}
			echo "</p>";
		}
		else if($this->collect && !$this->pass && !empty($errdata))
		{
			$result = mysql_query($query, $this->serverlink) or ($this->error_mysql("mysql", mysql_error(), $errdata["file"], $errdata["line"], $query));
		}
		else
		{
			if($this->pass)
			{
				$this->pass = false;
			}
			$result = mysql_query($query, $this->serverlink);
		}
		if($this->log == true && $logtext != '')
		{
			$this->insert("logs", $logtext);
		}
		return $result;
	}
	
	public function get_errdata($file, $line){
		$err = array();
		$err["line"] = $line;
		$err["file"] = $file;
		return $err;
	}
	
	public function safe($arr, $html = false) {
		if(!empty($arr))
		{
			foreach ($arr as $key => $value)
			{
				if(is_array($arr[$key]))
				{
					$arr[$key] = $this->safe($arr[$key], $html);
				}
				else
				{
					$arr[$key] = $this->strip($value);
					if($html)
					{
						$arr[$key] = mysql_real_escape_string($arr[$key]);
					}
					else
					{
						$arr[$key] = mysql_real_escape_string(nl2br(htmlspecialchars($arr[$key], ENT_QUOTES)));
					}
				}
			}
		}
		return $arr;
    }
	
	public function strip($val) {
		$arr = json_decode($val, true);
		if(is_array($arr) && !empty($arr))
		{
			return str_replace("\u", "\\u", $val);
		}
		if (TRUE == function_exists('get_magic_quotes_gpc') && 1 == get_magic_quotes_gpc())
		{
			$mqs = strtolower(ini_get('magic_quotes_sybase'));
			
			if (empty($mqs) || 'off' == $mqs)
			{
				$val = stripslashes($val);
			}
			else
			{
				$val =  str_replace("''", "'", $val);
			}
		}
		return $val;
	}
	
	function __destruct(){
	
		mysql_close($this->serverlink);
	}
	
}
?>