package main

import (
    "database/sql"
    "fmt"
    "log"
    "math/rand"
    "os"
    "runtime"
    "time"
)

// --------------------------------------------------
//    ___  _  _            ___       _      _
//   | __|(_)| | ___  ___ | __|__ __(_) ___| |_  ___
//   | _| | || |/ -_)|___|| _| \ \ /| |(_-<|  _|(_-<
//   |_|  |_||_|\___|     |___|/_\_\|_|/__/ \__|/__/
func exists(filePath string) (exists bool) {
    _,err := os.Stat(filePath)
    if err != nil {
        exists = false
    } else {
        exists = true
    }
    return
}

// ----------------------------------------------------------------------------
//      ____                  __                   ____
//     / __ \____ _____  ____/ /___  ____ ___     / __ \____ _____  ____ ____
//    / /_/ / __ `/ __ \/ __  / __ \/ __ `__ \   / /_/ / __ `/ __ \/ __ `/ _ \
//   / _, _/ /_/ / / / / /_/ / /_/ / / / / / /  / _, _/ /_/ / / / / /_/ /  __/
//  /_/ |_|\__,_/_/ /_/\__,_/\____/_/ /_/ /_/  /_/ |_|\__,_/_/ /_/\__, /\___/
//                                                               /____/
func randomRange(min int, max int) (randomRange int){
    rand.Seed(time.Now().UnixNano())
    randomRange = rand.Intn(max-min)+min
    return
}


/* -------------------------------------
 *     |  \/  |     | |
 *     | \  / |_   _| |     ___   __ _
 *     | |\/| | | | | |    / _ \ / _` |
 *     | |  | | |_| | |___| (_) | (_| |
 *     |_|  |_|\__, |______\___/ \__, |
 *              __/ |             __/ |
 *             |___/             |___/  */
func myLog(msg ...interface{}) {
    defer func() { r := recover(); if r != nil { fmt.Print("Error detected logging:", r) } }()
    if conf.DEBUG >0 {
        fmt.Println(msg)
        logfile, err := os.OpenFile(conf.LOGDIR+"/"+conf.AppName+".log", os.O_RDWR | os.O_CREATE | os.O_APPEND,0666)
        if !checkErr(err) {
            log.SetOutput(logfile)
            log.Println(msg)
        }
        defer logfile.Close()
    }
}

/* -------------------------------------
 *     |  \/  |     | |
 *     | \  / |_   _| |     ___   __ _
 *     | |\/| | | | | |    / _ \ / _` |
 *     | |  | | |_| | |___| (_) | (_| |
 *     |_|  |_|\__, |______\___/ \__, |
 *              __/ |             __/ |
 *             |___/             |___/  */
func myDebugLog(msg ...interface{}) {
    defer func() { r := recover(); if r != nil { fmt.Print("Error detected logging:", r) } }()
    if conf.DEBUG > 1 {
        fmt.Println(msg)
        logfile, err := os.OpenFile(conf.LOGDIR+"/"+conf.AppName+".log", os.O_RDWR | os.O_CREATE | os.O_APPEND,0666)
        if !checkErr(err) {
            log.SetOutput(logfile)
            log.Println(msg)
        }
        defer logfile.Close()
    }
}

// -------------------------------------------------
//     ___ _           _     ___
//    / __| |_  ___ __| |__ | __|_ _ _ _ ___ _ _
//   | (__| ' \/ -_) _| / / | _|| '_| '_/ _ \ '_|
//    \___|_||_\___\__|_\_\ |___|_| |_| \___/_|
func checkErr(err error) (isErr bool){
    defer func() { r := recover(); if r != nil { log.Print("Error detected:", r) } }()
    isErr = false
    if err != nil {
        isErr = true
        a,b,c,_ := runtime.Caller(1)
        myLog("Error", err, " in ","Process ID:",a , "In Module:",b, "Line:",c)  //Return Error object
    }
    return
}

// -------------------------------------------------
//     ___ _           _  DB ___
//    / __| |_  ___ __| |__ | __|_ _ _ _ ___ _ _
//   | (__| ' \/ -_) _| / / | _|| '_| '_/ _ \ '_|
//    \___|_||_\___\__|_\_\ |___|_| |_| \___/_|
func checkDbErr(err error,db *sql.DB) (isErr bool){
    defer func() { r := recover(); if r != nil { log.Print("DB Error detected:", r) } }()
    isErr = false
    if err != nil {
        isErr = true
        a,b,c,_ := runtime.Caller(1)
        myLog("DB Error:",db.Ping(),err,"Process ID:",a , "In Module:",b, "Line:",c)  //Return the Database Error object
    }
    return
}