package main

import (
    "fmt"
    "github.com/valyala/fasthttp"
)

func main(){
    handler := requestHandler
    //checkErr(fasthttp.ListenAndServe(conf.DNS+":"+string(conf.PORT), handler))
    checkErr(fasthttp.ListenAndServe("127.0.0.1:8000", handler))
}

//   -----------------------------------------------------------------------------
//    _____                            _     _    _                 _ _
//   |  __ \                          | |   | |  | |               | | |
//   | |__) |___  __ _ _   _  ___  ___| |_  | |__| | __ _ _ __   __| | | ___ _ __
//   |  _  // _ \/ _` | | | |/ _ \/ __| __| |  __  |/ _` | '_ \ / _` | |/ _ \ '__|
//   | | \ \  __/ (_| | |_| |  __/\__ \ |_  | |  | | (_| | | | | (_| | |  __/ |
//   |_|  \_\___|\__, |\__,_|\___||___/\__| |_|  |_|\__,_|_| |_|\__,_|_|\___|_|
//                  | |
//                  |_|
func requestHandler(ctx *fasthttp.RequestCtx) {
    switch string(ctx.Path()){
    case "/api/test":
        fmt.Fprintf(ctx, "Request method is %q\n", ctx.Method())
        fmt.Fprintf(ctx, "RequestURI is %q\n", ctx.RequestURI())
        fmt.Fprintf(ctx, "Requested path is %q\n", ctx.Path())
        fmt.Fprintf(ctx, "Host is %q\n", ctx.Host())
        fmt.Fprintf(ctx, "Query string is %q\n", ctx.QueryArgs())
        fmt.Fprintf(ctx, "User-Agent is %q\n", ctx.UserAgent())
        fmt.Fprintf(ctx, "Connection has been established at %s\n", ctx.ConnTime())
        fmt.Fprintf(ctx, "Request has been started at %s\n", ctx.Time())
        fmt.Fprintf(ctx, "Serial request number for the current connection is %d\n", ctx.ConnRequestNum())
        fmt.Fprintf(ctx, "Your ip is %q\n\n", ctx.RemoteIP())
    case "/api/status":
        //responce, _ := json.MarshalIndent(status,"","    ")
        //fmt.Fprintf(ctx,"Status returns\n")
        //fmt.Fprintf(ctx,string(responce))
    case "/api/config":
    case "/api/hand":
    case "/api/checkhand":
        //responce, _ := json.MarshalIndent(conf,"","    ")
        //fmt.Fprintf(ctx,"Configs\n")
        //fmt.Fprintf(ctx,string(responce))
    default:
        ctx.Error("Not a valid command", fasthttp.StatusBadRequest)
        fmt.Fprintf(ctx,"\nstatus -> returns the accumilations of the engine")
        fmt.Fprintf(ctx,"\nconnections -> returns the database connection details")
    }

}


func checkHand(){
    
}