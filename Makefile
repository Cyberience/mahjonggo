mjp: clean import build

import:
	go get -u github.com/valyala/fasthttp
	go get github.com/mattn/go-sqlite3
	go get -u github.com/go-sql-driver/mysql
	go get -u github.com/lib/pq
	go get -u github.com/joho/godotenv
	go get -u github.com/sirupsen/logrus

clean:
	rm -f mjp


compile:
	echo "Compiling for every OS and Platform"
	GOOS=freebsd GOARCH=386 go build -o bin/main-freebsd-386 main.go
	GOOS=linux GOARCH=386 go build -o bin/main-linux-386 main.go
	GOOS=windows GOARCH=386 go build -o bin/main-windows-386 main.go

build:
	go build -o mjp

all: clean import compile build